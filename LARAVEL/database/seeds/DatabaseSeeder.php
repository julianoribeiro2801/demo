<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();

		   User::create( [
             'email' => 'ruiz@7cliques.com.br' ,
             'password' => Hash::make( 'cli007' ) ,
             'name' => 'Adriano Ruiz' ,
             'role' => 'admin' 
            ] );
    }
}
