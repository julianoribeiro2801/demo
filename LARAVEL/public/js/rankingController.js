app.controller('rankingController', function($http, $scope, $timeout) {
        
        $scope.ranking = {};
	$scope.musculacaos = [];
	$scope.ginasticas =[];
	$scope.natacaos = [];
	$scope.crosss = [];
	$scope.corridas = [];
	$scope.ciclismos = [];
        $scope.notificacao = {};
	$http({
		url: '../admin/api/conquistas/getRanking/1',
		method: "GET"
	}).then(function(response) {
		$scope.musculacaos = response.data.musculacaos;
	}, function(response) {
		console.log('Opsss... Algo deu errado!');
	});

	$http({
		url: '../admin/api/conquistas/getRanking/2',
		method: "GET"
	}).then(function(response) {
		$scope.ginasticas = response.data.musculacaos;
	}, function(response) {
		console.log('Opsss... Algo deu errado!');
	});
	$http({
		url: '../admin/api/conquistas/getRanking/3',
		method: "GET"
	}).then(function(response) {
		$scope.natacaos = response.data.musculacaos;
	}, function(response) {
		console.log('Opsss... Algo deu errado!');
	});
	$http({
		url: '../admin/api/conquistas/getRanking/4',
		method: "GET"
	}).then(function(response) {
               $scope.crosss = response.data.musculacaos;

	}, function(response) {
		console.log('Opsss... Algo deu errado!');
	});
	$http({
		url: '../admin/api/conquistas/getRanking/5',
		method: "GET"
	}).then(function(response) {
               $scope.corridas = response.data.musculacaos;

	}, function(response) {
		console.log('Opsss... Algo deu errado!');
	});
	$http({
		url: '../admin/api/conquistas/getRanking/6',
		method: "GET"
	}).then(function(response) {
               $scope.ciclismos = response.data.musculacaos;

	}, function(response) {
		console.log('Opsss... Algo deu errado!');
	});
    
    
	$scope.exibirDenuncias = function(conquista_id, aluno_id) {
		// LISTA CIDADE
		$scope.denuncias = [];
		$http({
        		url: '../admin/api/conquistas/getDenuncias',
			data: {
				idconquista: conquista_id,
				idaluno: aluno_id
                                
				},
			method: "GET"
		}).then(function(response) {
			
                        $scope.denuncias = response.data.denuncias;
                        $scope.denuncias.dtdenu = $filter('date')(response.data.denuncias.dtdenuncia, 'dd/MM/yyyy');
                        
		}, function(response) {
			console.log('Opsss... Algo deu errado!');
		});
	};        
	$scope.notificar = function(conquista_id, aluno_id) {
		// LISTA CIDADE
		$scope.notificacao.idaluno = aluno_id;
		$scope.notificacao.idconquista = conquista_id;
                
	};        
        
});