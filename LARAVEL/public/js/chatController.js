
app.controller('chatController', function ($http, $scope, $timeout) {
    
    

    $http.defaults.headers.common.user = $('meta[name="user_logued"]').attr('content');
    $http.defaults.headers.common.unity = $('meta[name="unidade"]').attr('content');

    $scope.loading = false;
    $scope.cvempresa = {};
    $scope.alunos = {};
    $scope.unity = {};
    $scope.messages = [];
    $scope.message = {};
    $scope.mensagem = '';
    $scope.totalMsg=0;

    $scope.aluno=0;
    $scope.alunonome='';

    $scope.exibetudo='N';
    
    
    $scope.logued= $('meta[name="user_logued"]').attr('content');
    $scope.receiver='';    
    

    $http({
        url: '/api/chat/unity',
        method: "GET"
    }).then(function (response) {


        $scope.unity = response.data.unity;


    }, function (response) {
        console.log('Opsss... Algo deu errado!');
    });



    $scope.pegaMensagens = function (id,nome) {
        
        $scope.aluno=id;
        $scope.alunonome=nome;
        $scope.getMessages(id,nome);
        $scope.setReadAll(); 
    };
    
    
    $scope.exibirTudo = function (){
        
        
      if ($scope.exibetudo == 'S'){
           $scope.exibetudo = 'N';
           
       } else {
           $scope.exibetudo ='S';
       }
       
       $scope.getMessages($scope.aluno,$scope.alunonome);
       

    };
    
    

    $scope.getMessages = function (id,nome) {
        


        $scope.conversando_com = nome;
        $scope.receiver=id;
  
        var url ='';
        if ($scope.exibetudo=='S'){
            url= '/api/chat/person/' + id + '/messagesAll/';
        } else {
            url= '/api/chat/person/' + id + '/messages/';
        }
        $http({
            url: url,
            method: "GET"
        }).then(function (response) {
           $scope.messages = response.data.resposta;
           console.log('dados messages',$scope.messages);
//           $scope.messages = response.data.resposta.data;
           setTimeout(function () {  $scope.rolaTela(); }, 100);
        }, function (response) {
            console.log('Opsss... Algo deu errado!');
        });
        
    };

    setInterval(function () {
        $http({
            url: '/api/chat/searchMessages/' + $scope.totalMsg,
            method: "GET"
        }).then(function (response) {
            
            if(response.data[0].naolidas != response.data[0].quant){
                $scope.totalMsg = response.data;
                $http({
                    url: '/api/chat/unity',
                    method: "GET"
                }).then(function (response) {
                    $scope.unity = response.data.unity;
                }, function (response) {
                    console.log('Opsss... Algo deu errado!');
                });                
                $http({
                        url: '/api/chat/unity/atualizarListaChat/' + $http.defaults.headers.common.user + '/' + $http.defaults.headers.common.unity ,
                        method: "get"
                    }).then(function (response) {
                    $scope.unity = response.data.unity;
                }, function (response) {
                    console.log('Opsss... Algo deu errado!');
                });                   
               $scope.totalMsg=response.data[0].naolidas;
               
              $scope.getMessages($scope.aluno,$scope.alunonome);
            }
               //  alert('xx' + $scope.totalMsg + ' yy ' + response.data[0].naolidas);
            if ($scope.totalMsg !== response.data[0].naolidas) {
               

                
                $scope.totalMsg = response.data;
                $http({
                    url: '/api/chat/unity',
                    method: "GET"
                }).then(function (response) {


                    $scope.unity = response.data.unity;


                }, function (response) {
                    console.log('Opsss... Algo deu errado!');
                });
        ////////////////////verifica lista achat

            }


        }, function (response) {
            console.log('Opsss... Algo deu errado!');
        });
        
        

        
        

    }, 10000);
    
    
    $scope.storeMessage = function (mensagem) {


         
        $http({
            url: '/api/chat/messages',
            data: {
              person: $scope.receiver,
              message: mensagem
            },
            method: "POST"
        }).then(function (response) {
            
           $scope.mensagem='';
            
           //$scope.messages = response.data.resposta.data;
           $scope.getMessages($scope.receiver);
        }, function (response) {
            console.log('Opsss... Algo deu errado!');
        });
        
        
            
        $http({
            url: '../api/prescricao/enviaPush',
            data: {
              person: $scope.receiver,
              message: mensagem
            },
            method: "POST"
        }).then(function (response) {
            
           $scope.getMessages($scope.receiver);
           
        }, function (response) {
            console.log('Opsss... Algo deu errado!');
        });
        
        

            
        
        
    };    
    
    $scope.rolaTela = function () {
       // var altura = $( ".chat-discussion" ).height();
        //altura = parseInt(altura)+100;
         //$('.chat-discussion').animate({ scrollTop: altura }, 'slow');
          $('.chat-discussion').scrollTop($('.chat-discussion')[0].scrollHeight);

    };

    $scope.setReadAll = function () {
        $http({
            url: '/api/chat/messages/setRead',
            data: {
              person: $scope.receiver,
              user: $scope.logued
            },
            method: "POST"
        }).then(function (response) {
            
           //$scope.mensagem='';
            $http({
                url: '/api/chat/unity/atualizarListaChat/' + $http.defaults.headers.common.user + '/' + $http.defaults.headers.common.unity,
                method: "get"
            }).then(function (response) {
                $scope.unity = response.data.unity;
            }, function (response) {
                console.log('Opsss... Algo deu errado!');
            });
             
        
           $scope.getMessages($scope.receiver);
           
        }, function (response) {
            console.log('Opsss... Algo deu errado!');
        });
        
           
        
        
        
    
    
    };    
    
    $scope.leTecla = function(keyEvent) {
        console.log(keyEvent);
        if (keyEvent.which === 13) {
            //alert('I am an alert');
            $scope.storeMessage($scope.mensagem);
        }
    
    }    ;

}); // fim controler alunoController
