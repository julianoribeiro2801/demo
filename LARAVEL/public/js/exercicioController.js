app.factory('ExercicioService', function($http, $q, $timeout, $window) {
    var config = {
        api: '/api/exercicio'
    };
    $http.defaults.headers.common.user = $('meta[name="user_id"]').attr('content') || 1;
    $http.defaults.headers.common.unidade = 1;
    $http.defaults.headers.common._token = $('meta[name="csrf_token"]').attr('content');
    var services = {
        register: register,
        show: show,
        destroy: destroy,
        update: update,
        all: all
    };

    function all() {
        return $http.get(config.api);
    }

    function register($data) {
        return $http.post(config.api, $data);
    }

    function show($id) {
        return $http.get(config.api + '/exercicio/' + $id);
    }

    function destroy($id) {
        return $http.delete(config.api + '/' + $id);
    }

    function update($id, $data) {
        return $http.put(config.api + '/' + $id, $data);
    }
    return services;
});
app.controller('exercicioController', function($http, $scope, ExercicioService, $timeout) {
    $http.defaults.headers.common.user = $('meta[name="user_logued"]').attr('content');
    $http.defaults.headers.common.unity = $('meta[name="unidade"]').attr('content');
    $scope.loading = false;
    $scope.unidade = 1;
    $scope.user = 1;
    $scope.exercicio = {};
    $scope.exercicio.dados = {};
    $scope.exercicios = [];
    $scope.grupos = [];
    $scope.grupo = {};
    $scope.editing = false;
    $scope.showData = false;
    $scope.errors = {};
    $scope.video = 'N';
    $scope.texto = 'Exibir vídeo';
    $scope.toDate = function(date) {
        return new Date(date);
    };
    // PEGA TODOS OS EXERCICIOS
    getExercicios();
    $scope.add = function() {
        var f = document.getElementById('file').files[0];
        console.log('f', f);
    }
    $scope.exibeVideo = function() {
        if ($scope.video == 'S') {
            $scope.video = 'N';
            $scope.texto = 'Exibir vídeo';
        } else {
            $scope.video = 'S';
            $scope.texto = 'Ocultar vídeo';
        }
    };
    $scope.edit = function($exercicio) {
        $scope.editing = $exercicio.id;
        if ($scope.showData === false) {
            $scope.showData = true;
        } else {
            $scope.showData = false;
        }
    };
    $scope.select = function($exercicio) {
        if ($http.defaults.headers.common.unity == 2) {
            $exercicio.idunidade = $http.defaults.headers.common.unity;
        }
        if ($exercicio.idunidade == '0') {
            swal("Bloqueado!", "Exercício padrão. Não pode ser alterado!", "error");
        } else {
            $scope.exercicio = {};
            ExercicioService.show($exercicio.id).then(function(response) {
                $scope.exercicio = response.data;
                $('#exercicios_edit').modal('show');
                $timeout(function() {
                    $scope.$apply();
                });
            }, function(response) {
                alert('Erro ao buscar informações do exercicio!');
            });
        }
    };
    // // quando seleciona a imagem executa automatico
    //        $scope.uploadFile = function(files) {
    //            alert('ashjsakjhsajk');
    //            var fd = new FormData();
    //            //Take the first selected file
    //            fd.append("image", files[0]);
    //            console.log(files);
    //            // var uploadUrl = '/admin/category/uploadFile';
    //            // $http.post(uploadUrl, fd, {
    //            //     withCredentials: true,
    //            //     headers: {'Content-Type': undefined },
    //            //     transformRequest: angular.identity
    //            // }).then(function (response) {
    //            //   console.log('nome image',response.data.image_name);
    //            //   if(response.data.status == 'success'){
    //            //     $scope.dados.image = response.data.image_name
    //            //   } else {
    //            //     $scope.dados.image = '';
    //            //   }
    //            // }, function (response) {
    //            //    toastr.error('Opsss... Algo deu errado!', 'Error'); 
    //            // });
    //        };
    $scope.gerarMiniatura = function($exercicio) {
        $http({
            url: '../../admin/prescricao/gerarMiniatura',
            data: $scope.exercicio,
            method: "POST"
        }).then(function(response) {
            toastr.success('Exercicio gravado com sucesso!!');
            getExercicios();
        }, function(response) {
            console.log('Opsss... Algo deu errado!');
        });
    };
    $scope.deleteExercicio = function(exercicio_id) {
        if (exercicio_id !== undefined) {
            swal({
                title: "Tem certeza?",
                text: "Tem certeza que deseja deletar esse exercicio?",
                icon: "warning",
                buttons: true,
                buttons: {
                    cancel: {
                        text: "Cancelar",
                        value: null,
                        visible: true,
                        className: "",
                        closeModal: true,
                    },
                    confirm: {
                        text: "Sim, tenho certeza!",
                        value: true,
                        visible: true,
                        className: "",
                        closeModal: true
                    }
                },
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    // aqui chama o ajax
                    $http({
                        url: '../prescricao/deleteExercicio/' + exercicio_id,
                        method: "POST"
                    }).then(function(response) {
                        getExercicios();
                        swal("Deletado!", "Exercício deletado com sucesso!", "success");
                    });
                } else {
                    // swal("Your imaginary file is safe!");
                }
            });
            // fim swal
            // 
        } else {
            swal("Opsss!", "Selecione um local para deletar!", "error");
        }
    };
    $scope.register = function() {
        $scope.loading = true;
        $scope.errors = {};
        ExercicioService.register($scope.exercicio).then(function(response) {
            $timeout(function() {
                $scope.$apply();
            });
            swal("Cadastrado!", "Exercício cadastrado com sucesso!", "success");
            $scope.loading = false;
            getExercicios();
            $('#m_exercicios').modal('hide');
        }, function(response) {
            $scope.loading = false;
            $scope.errors = response.data;
            alert('Erro ao cadastrar novo exercicio!');
        });
    };
    $scope.update = function() {
        $scope.loading = true;
        ExercicioService.update($scope.exercicio.id, $scope.exercicio).then(function(response) {
            $timeout(function() {
                $scope.$apply();
            });
            swal("Atualizado!", "Exercício atualizado com sucesso!", "success");
            getExercicios();
            $scope.loading = false;
            $('#exercicios_edit').modal('hide');
        }, function(response) {
            alert('Ocorreu um erro ao atualizar os dados do exercicio!');
        });
    };
    $scope.addExercicio = function($exercicio) {
        $scope.exercicio = {};
        $scope.exercicio = $exercicio;
        $http({
            url: '../../admin/prescricao/insere',
            data: $scope.exercicio,
            method: "POST"
        }).then(function(response) {
            toastr.success('Exercicio gravado com sucesso!!');
            getExercicios();
        }, function(response) {
            console.log('Opsss... Algo deu errado!');
        });
    };
    $scope.updateExercicio = function($exercicio) {
        $scope.exercicio = {};
        $scope.exercicio = $exercicio;
        $http({
            url: '../../admin/prescricao/atualiza',
            data: $scope.exercicio,
            method: "POST"
        }).then(function(response) {
            toastr.success('Exercicio atualizado com sucesso!!');
            getExercicios();
        }, function(response) {
            console.log('Opsss... Algo deu errado!');
        });
    };

    function getExercicios() {
        $http({
            url: '../prescricao/getExercicios',
            method: "GET"
        }).then(function(response) {
            $scope.exercicios = response.data.exercicios;
            $scope.grupos = response.data.gruposmusculares;
        }, function(response) {
            console.log('Opsss... Algo deu errado!');
        });
    }
    $scope.searchExercicio = '';
    // getExerciciosBusca
    $scope.getExerciciosBusca = function() {
        var texto = $scope.searchExercicio;
        if (texto.length > 2) {
            $http({
                url: '/admin/prescricao/getExerciciosBusca/' + $scope.searchExercicio,
                method: "GET"
            }).then(function(response) {
                $scope.exercicios = response.data.exercicios;
                $scope.grupos = response.data.gruposmusculares;
            }, function(response) {
                console.log('Opsss... Algo deu errado!');
            });
        }
        if (texto.length == 0) {
            getExercicios();
        }
    };
    $scope.paginateUrl = function(urlpage) {
        $http({
            url: urlpage,
            method: "GET"
        }).then(function(response) {
            $scope.exercicios = response.data.exercicios;
            $scope.grupos = response.data.gruposmusculares;
        }, function(response) {
            console.log('Opsss... Algo deu errado!');
        });
    };
});