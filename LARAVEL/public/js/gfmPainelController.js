// INICIO DA CONTROLER GFM PAINEL CONTROLLER
app.controller('gfmPainelController', function ($http, gfmService, $scope, $timeout) {

    $scope.aulasdia = [];
    $scope.programas = [];
    $scope.listareservas = [];
    $scope.listareservasdados = [];
    $scope.matriculas = [];
    $scope.clients = [];


    /* essa nao precisa estender */

    $scope.diahoje = formatarDataExtenso();


    getAulasDiaSemana();

    function formatarDataExtenso() {


        var diasSemana = ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'];
//var meses = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'];

        var dtx = new Date();
        var dia = dtx.getDate();
        var dias = dtx.getDay();
        var mes = dtx.getMonth();
        var ano = dtx.getYear();


        var diaSemana = diasSemana[dias];

        return diaSemana;
    }

    $scope.setDiasemana = function (diasemana) {

        $scope.diahoje = $scope.diasemana();
        // alert( $scope.diahoje);
        $scope.dia_semana = diasemana;
    };

    /* essa nao precisa estender */
    $scope.setDiasemana();
    
    
    $scope.reservaDel = function (idaula, id, indice) {
   
        swal({
   title: "Tem certeza?",
            text: "Deseja realmente cancelar a reserva?",
  icon: "warning",
  buttons: true,
  buttons: {
      cancel: {
        text: "Cancelar",
        value: null,
        visible: true,
        className: "",
        closeModal: true,
      },
      confirm: {
        text: "Sim, tenho certeza!",
        value: true,
        visible: true,
        className: "",
        closeModal: true
      }},
  dangerMode: true,

}).then((willDelete) => {
  if (willDelete) {
    // aqui chama o ajax
    $scope.listareservas.splice(indice, 1);        
            //$scope.statusBotoesGfm(true);
            $http({
                url: '../api/reservaDelPainel/' + idaula + '/' + parseInt(id),
                method: "GET"
            }).then(function (response) {
                
                $scope.getListareserva(idaula,'','Não');
                //$scope.getAula(idaula);
            }, function (response) {
                console.log('Opsss... Algo deu errado!');
            });
  } else {
    // swal("Your imaginary file is safe!");
  }
});
// fim swal
// 
        
      
        
    };    

    function getAulasDiaSemana() {
        $http({
            url: '../admin/api/painel/getAulasDiaSemana',
            method: "GET"
        }).then(function (response) {
            $scope.aulasdia = response.data.gfms;
            $scope.diasemana_select = response.data.diasemana;
        }, function (response) {
            console.log('Opsss... Algo deu errado!');
        });
    }

    getAulasDiaSemana();




    // LISTA TODOS OS CLIENTES
    // com auto completar
    $scope.getClients = function ($user) {
        gfmService.getClients().then(function (response) {
            $scope.clients = response.data;
            console.log(response.data)
            // ativa o auto completar
            setTimeout(function () {
                $(".chosen-select").chosen({width: "100%"});
            }, 500);
        }, function (response) {
        });
    }


    $scope.getAula = function (id) {
        $http({
            url: '../api/getAula/' + id + '/0/Ter',
            method: "GET"
        }).then(function (response) {
            $scope.qt_vagas = response.data.n_reservas;
            $scope.dt_aula = response.data.dtaula;
            $scope.capacidade = response.data.capacidade;
            $scope.matriculados = response.data.matriculados;
            $scope.vacancia = $scope.capacidade - $scope.matriculados;


        }, function (response) {
            console.log('Opsss... Algo deu errado!');
        });

    };


    $scope.reservaAddNovo = function (id) {
        
        

        $http({
            url: '/admin/api/gfm/reservaAddPainel/' + id,
            data: $scope.listareservas,
            method: "POST"
        }).then(function (response) {

            swal("Ok!", "Reservas gravadas com sucesso", "success");
        });

    };

    $scope.getListareserva = function ($id, $dtaula, $privado) {


        if ($privado == 'Não') {
            $http({
                url: '../api/getListareservaPainel/' + $id,
                method: "GET"
            }).then(function (response) {
                $scope.listareservas = response.data.reservas;
                $scope.listareservasdados = response.data.reservasdados;
                $scope.qt_alunos = response.data.reservasdados[0].capacidade - response.data.reservasdados[0].n_reservas;
                $scope.qt_vagas = response.data.reservasdados[0].capacidade - $scope.qt_alunos;
//                $scope.qt_vagas = response.data.reservasdados[0].n_reservas;
                $scope.capacidade = response.data.reservasdados[0].capacidade;
                $scope.dt_aula = response.data.reservasdados[0].dtaula;

            });
        } else {
            $http({
                url: '../api/getListamatricula/' + $id + '/' + $dtaula,
                method: "GET"
            }).then(function (response) {
                $scope.matriculas = response.data.matriculas;
                $scope.vagas = response.data.gfm.capacidade;
                $scope.sobra = response.data.gfm.capacidade - response.data.matriculados;
            });
        }
    };

    $scope.matriculaAdd = function (aula, aluno) {
        $http({
            url: '../api/matriculaAdd/' + aula + '/' + aluno,
            method: "GET"
        }).then(function (response) {
            if (response.data.cod !== '0') {
                swal("Opsss!", response.data.text + "!", "error");
            } else {
                swal("Ok!", response.data.text, "success");
            }
            $scope.getListareserva(aula);
            $scope.getListaespera(aula);
            $scope.getAula(aula);
            // $scope.listareservas = response.data.gfm;
        });
    };

    $scope.addAlunoAula = function (aula) {

        $scope.inserted = {
            id: 0,
            name: 'ANÔNIMO',
            registro:0,
            cont:$scope.listareservas.length + 1

        };
       
       
       
       
        $scope.listareservas.unshift($scope.inserted);
        //$scope.listareservas.reverse();
        $scope.qt_vagas = $scope.capacidade - $scope.listareservas.length;

        // ativa o auto completar
        
       
        setTimeout(function () {
            $(".chosen-select").chosen({width: "100%"});
        }, 200);

    };


    $scope.addAlunoMatricula = function (indice) {

        $scope.inserted = {
            idaluno: 0,
            name: 'ANÔNIMO'

        };

        $scope.matriculas.push($scope.inserted);
        $scope.matriculas.reverse();
        $scope.sobra = $scope.vagas - $scope.matriculas.length;

        // ativa o auto completar
        setTimeout(function () {
            $(".chosen-select").chosen({width: "100%"});
        }, 200);
    };


    $scope.getListamatricula = function ($id, $dataaula) {

        $http({
            url: '../api/getListamatricula/' + $id,
            method: "GET"
        }).then(function (response) {
            $scope.matriculas = response.data.matriculas;
            $scope.vagas = response.data.gfm.capacidade;
            $scope.sobra = response.data.gfm.capacidade - response.data.matriculados;

        });
    };

    $scope.getListaespera = function ($id) {

        $http({
            url: '../api/getListaespera/' + $id,
            method: "GET"
        }).then(function (response) {
            $scope.listaesperas = response.data.espera;


        });
    };

    $scope.getAulasPassadas = function (id, professor, aula, horario, privado) {

        $scope.professor = professor;
        $scope.aula = aula;
        $scope.horario = horario;
        $scope.aulaSelect = id;
        $scope.privado = privado;

        $scope.getClients();

        $scope.getListamatricula(id, '2018-01-01');
        $scope.getListaespera(id);

        if (privado == 'Não') {
            $("#addCliente").modal('toggle');
            $scope.getListareserva(id, '', privado);
            $scope.getClients();
        } else {
            $("#addClientePrivate").modal('toggle');
            //$scope.getListamatricula(id, $scope.aulasant[$scope.aulasant.length - 1].dtaula);
            // $scope.getListamatricula(id, $scope.aulasant[$scope.aulasant.length - 1].dtaula);
        }
    };


});