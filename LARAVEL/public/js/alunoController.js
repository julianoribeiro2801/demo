
app.factory('AlunoService', function($http, $q, $timeout, $window) {
    var config = {
        api: '/api/cliente'
    };
    $http.defaults.headers.common.user = $('meta[name="user_logued"]').attr('content');
    $http.defaults.headers.common.unidade = $('meta[name="unidade"]').attr('content');
    $http.defaults.headers.common._token = $('meta[name="csrf_token"]').attr('content');
    var services = {
        register: register,
        show: show,
        convert: convert,
        update: update,
        atualizar: atualizar,
        deleteCli: deleteCli,
        all: all,
        search: search,
        paginate: paginate,
        crm: crm,
        getRefreshClientPassword: getRefreshClientPassword,
        getCore: getCore,
        addObjective: addObjective,
        addContact: addContact
    };

    function all() {
        return $http.get(config.api);
    }

    function paginate(url) {
        console.log(url);
        return $http.get(url);
    }

    function register($data) {
        return $http.post(config.api, $data);
    }

    function show($id) {
        return $http.get(config.api + '/client/' + $id);
    }

    function search($query) {
      
        return $http.get(config.api + '/search/' + $query);

    }

    function convert($id,$motivo) {
        return $http.patch(config.api + '/' + $id + '/' + $motivo);
    }

    function deleteCli($id) {
        return $http.patch(config.api + '/' + $id);
    }

    function update($id, $data) {
        return $http.put(config.api + '/' + $id, $data);
    }

    function atualizar($id) {
        return $http.get(config.api + '/' + $id);
    }

    function getCore() {
        return $http.get('/api/core');
    }

    function getRefreshClientPassword($id) {
        return $http.put(config.api + '/refresh/password/' + $id);
    }

    function crm($cliente) {
        return $http.get(config.api + '/crm/' + $cliente);
    }

    function addObjective($objective) {
        return $http.post('/api/objectives', {
            nmobjetivo: $objective
        });
    }

    function addContact(data) {
        return $http.post('/api/contact', data);
    }
    return services;
})

app.controller('alunoController', function($http, $scope, AlunoService, $timeout, $rootScope, Upload) {
  
    $http.defaults.headers.common.user = $('meta[name="user_logued"]').attr('content');
    $http.defaults.headers.common.unity = $('meta[name="unidade"]').attr('content');
  
    $scope.logued = $('meta[name="user_logued"]').attr('content');
  
    $scope.crm = [];
    $scope.editing = false;
    $scope.showData = false;
    $scope.person = {};
    $scope.persons = [];
    $scope.query;
    $scope.searching = false
    $scope.errors = {};
    $scope.contact = {};
    $scope.messages = [];
    $scope.page = 1;
    var keys;
    var key;
  
    $scope.init = bindEventsOnStart()
  

    $scope.rolaTela = function() {
        // var altura = $( ".chat-discussion" ).height();
        //$('.chat-discussion').animate({ scrollTop: altura }, 'slow');
        $('.chat-discussion').scrollTop($('.chat-discussion')[0].scrollHeight);
    };


    getImportacao();
    

                function getImportacao(){

                        var url = '';
                        url = '/api/importaClientesEvo';
                        $http({
                        url: url,
                                method: "GET"
                        }).then(function(response) {
                            
                }, function(response) {
                console.log('Opsss... Algo deu errado!');
                });
                }

    // $scope.validarDependente = function(id_user, email) {

    //     $http({
    //         url: '../../api/validaDepenteAluno/' +id_user+'/'+ email,
    //         method: "GET"
    //     }).then(function(response) {
    //         console.log(response.data);
    //         $scope.depentente = response.data;
    //         if ($scope.depentente != "") {
    //             // sou dependete de alguem
             
    //             var span = document.createElement("span");

    //             span.innerHTML='O <b>Heitor Schawb </b> está com o mesmo e-mail do cliente <b>Karsten Schawb</b>. <br> Deseja tornar o <b>Heitor Schawb </b> um depentente do cliente <b>Karsten Schawb </b>?'
    //             swal({
    //                  title: "ATENÇÃO!",
    //               content: span,
    //               icon: "warning",
    //               buttons: true,
    //               buttons: {
    //                   cancel: {
    //                     text: "Cancelar",
    //                     value: null,
    //                     visible: true,
    //                     className: "",
    //                     closeModal: true,
    //                   },
    //                   confirm: {
    //                     text: "Sim Desejo!",
    //                     value: true,
    //                     visible: true,
    //                     className: "",
    //                     closeModal: true
    //                   }},
    //               dangerMode: true,

    //             }).then((confirmar) => {
    //               if (confirmar) {
    //                 // aqui chama o ajax
    //                  // $http({
    //                  //        url: '../../api/excluirAluno/' + $client.id,
    //                  //        method: "GET"
    //                  //    }).then(function(response) {
    //                  //        $scope.redirect();
    //                  //        swal("Excluído!", "Cliente excluído com sucesso!", "success");
    //                  //    });
    //               } else {
    //                 // swal("Your imaginary file is safe!");
    //               }
    //             });



    //             // fim do if dependete
    //             // 
    //             // 
    //         } else {
    //             // Não sou dependente 
    //             alert('Não sou dependente');
    //         }
    //     });


         

    // }




    // getMessages
    
    
    $scope.tipoCliente = function($client) {
        var cat=0;
        if($client.idcategoria==1){
            cat=2;
        }else{
            cat=1;
        }

        swal({
            title: "Tem certeza?",
            text: "Tem certeza que deseja atualizar o status desse cliente?",
            icon: "warning",
            buttons: true,
            buttons: {
                cancel: {
                    text: "Cancelar",
                    value: null,
                    visible: true,
                    className: "",
                    closeModal: true,
                },
                confirm: {
                    text: "Sim, tenho certeza!",
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: true
                }
            },
            dangerMode: true,
        }).then((isConfirm) => {
            if (isConfirm) {
                $http({
                    url: '../../api/clientePremium/' + $client.id + '/' + cat,
                    data: {
                        id: $client.id
                    },
                    method: "POST"
                }).then(function(response) {
                    swal("Sucesso!", response.data.text, "success");
                }, function(response) {
                    console.log('Opsss... Algo deu errado!');
                });
            } else {
                $scope.getBlacklists();
            }
        });
        // fim swal
        // 
    }      
    

    /*$scope.tipoCliente = function($client) {
        // $scope.client = {};
        if ($client.id !== undefined) {

           swal({
              title: "Tem certeza?",
              text: 'Deseja realmente alterar a categoria desse cliente',
              icon: "warning",
              buttons: true,
              buttons: {
                  cancel: {
                    text: "Cancelar",
                    value: null,
                    visible: true,
                    className: "",
                    closeModal: true,
                  },
                  confirm: {
                    text: "Sim, tenho certeza!",
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: true
                  }},
              dangerMode: true,

            }).then((confirmar) => {
              if (confirmar) {
                // aqui chama o ajax
                $http({
                    url: '../../api/vincularDepenteAluno/' + client.id '/' +  ,
                    method: "GET"
                }).then(function(response) {
                    $scope.redirect();
                    swal("Excluído!", "Cliente excluído com sucesso!", "success");
                });
               
              } else {
                // swal("Your imaginary file is safe!");
              }
            });

        } else {
            swal("Deletar!", "Selecione um aluno para excluir!", "error");
        }
       
    };    */
    
    
    $scope.getMessages = function(id) {
        $scope.receiver = id;
        var url = '';
        url = '/api/chat/person/' + id + '/messagesAllCRM';
        $http({
            url: url,
            method: "GET"
        }).then(function(response) {
            $scope.messages = response.data.resposta;
            console.log('dados messages chat', $scope.messages);
            setTimeout(function() {
                $scope.rolaTela();
            }, 400);
        }, function(response) {
            console.log('Opsss... Algo deu errado!');
        });
    };

    //storeMessage. Envia mensage e push do chat
    $scope.storeMessage = function(mensagem) {
        $http({
            url: '/api/chat/messages',
            data: {
                person: $scope.receiver,
                message: mensagem
            },
            method: "POST"
        }).then(function(response) {
            $scope.mensagem = '';
            //$scope.messages = response.data.resposta.data;
            $scope.getMessages($scope.receiver);
        }, function(response) {
            console.log('Opsss... Algo deu errado!');
        });
        $http({
            url: '../api/prescricao/enviaPush',
            data: {
                person: $scope.receiver,
                message: mensagem
            },
            method: "POST"
        }).then(function(response) {
            $scope.getMessages($scope.receiver);
        }, function(response) {
            console.log('Opsss... Algo deu errado!');
        });
    };
    // getRotaPost
    $scope.getRotaPost = function(client_id) {
        $http({
            url: '../../admin/prescricao/nova/' + client_id,
            method: "POST"
        }).then(function(response) {}, function(response) {
            console.log('Opsss... Algo deu errado na busca do cliente!');
        });
    };
    $scope.abreCalendario = function() {
        console.log('teste');
        $timeout(function() {
            $('#data_4 .input-group.date').datepicker({
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                calendarWeeks: true,
                startDate: 'today',
                autoclose: true
            });
        }, 100);
    }

    function bindEventsOnStart() {
        _getInitalData()
    }

    function _getInitalData() {
        keys = _getJsonFromUrl()
        key = keys.user ? keys.user : null;
        _getCore()
        _getClients(key);
    }

    function getpageindex() {
        var page = window.location.hash.substr(1);
        console.log(page);
        if (page.length > 0) {
            $scope.page = ((page - 1) * 50);
        } else {
            $scope.page = 0;
        }
    }

    function _getCore() {
        AlunoService.getCore().then(function(response) {
            $scope.coreData = response.data;
            $timeout(function() {
                $scope.$apply();
            }, 600)
        })
    }

    function _getClients(client_id, paginate) {
        $scope.searching = true;
        var client_id = client_id || null;
        var paginate = paginate || null;
        if (paginate) {
            return AlunoService.paginate(paginate).then(dataSuccess).catch(error)
        }
        AlunoService.all().then(dataSuccess).catch(error)

        function dataSuccess(response) {
            $scope.persons = response.data.persons
            console.log($scope.persons);
            $scope.totalRegistros = response.data.persons
            getpageindex()
            if (client_id) {
                return $scope.select({
                    id: client_id
                })
            }
            //
            if (!$scope.person[0] && !paginate) {
                if (response.data.persons.total) {
                    $scope.select(response.data.persons.data[0]);
                }
            }
            $scope.editing = false;
            $scope.searching = false;
        }

        function error(response) {
            alert('Erro ao buscar clientes ');
            $scope.searching = false;
        }
    }

    function _getJsonFromUrl() {
        var query = location.search.substr(1);
        var result = {};
        query.split("&").forEach(function(part) {
            var item = part.split("=");
            result[item[0]] = decodeURIComponent(item[1]);
        });
        return result;
    }
    $scope.toDate = function(date) {
        return new Date(date);
    }
    $scope.edit = function($client) {
        $scope.editing = $client.id;
        // abre e fecha dados
        if ($scope.showData == false) {
            $scope.showData = true;
        } else {
            $scope.showData = false;
        };
    };
    
    $scope.paginateUrl = function(url) {
        _getClients(null, url);
    };

    // Essa aqui executa ao iniciar o MODAL 
    $scope.select = function($client) {
        AlunoService.show($client.id).then(function(response) {
            $scope.client = {};
            $scope.client = response.data;
            // $scope.getCrm($client.id);
            $scope.receiver = $client.id;
            $scope.getMessages($client.id);
            $timeout(function() {
                $scope.$apply();
            }, 600);
        }, function(response) {
            alert('Erro ao buscar informações do cliente');
        });
    };
    
    $scope.urlpage = "/admin/clientes/clientes";
    
    $scope.redirect = function() {
        setTimeout(function() {
            window.location = $scope.urlpage;
        }, 1200);
    }

    

    $scope.excluirCliente = function($client) {
        // $scope.client = {};
        if ($client.id !== undefined) {

           swal({
              title: "Tem certeza?",
              text: 'Deseja realmente excluir esse cliente',
              icon: "warning",
              buttons: true,
              buttons: {
                  cancel: {
                    text: "Cancelar",
                    value: null,
                    visible: true,
                    className: "",
                    closeModal: true,
                  },
                  confirm: {
                    text: "Sim, tenho certeza!",
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: true
                  }},
              dangerMode: true,

            }).then((confirmar) => {
              if (confirmar) {
                // aqui chama o ajax
                $http({
                    url: '../../api/excluirAluno/' + $client.id,
                    method: "GET"
                }).then(function(response) {
                    $scope.redirect();
                    swal("Excluído!", "Cliente excluído com sucesso!", "success");
                });
               
              } else {
                // swal("Your imaginary file is safe!");
              }
            });

        } else {
            swal("Deletar!", "Selecione um aluno para excluir!", "error");
        }
       
    };





    $scope.selectTeste = function($client) {
        alert('' + $client.id);
        AlunoService.show($client.id).then(function(response) {
            $scope.client = {};
            $scope.client = response.data;
            $timeout(function() {
                $scope.$apply();
            }, 600);
        }, function(response) {
            alert('Erro ao buscar informações do cliente');
        });
    };

    $scope.updateAvatar = function($files, $file) {
        if (!$file) {
            return false;
        }
        Upload.upload({
            url: '/api/upload',
            data: {
                file: $file,
                client: $scope.client.id
            },
            method: 'POST'
        }).then(function(resp) {
            toastr.success('Avatar atualizado com sucesso!')
        }, function(resp) {
            console.error(resp)
        }, function(evt) {
            console.log('progress: ' + parseInt(100.0 * evt.loaded / evt.total) + '% file :' + evt.config.data.file.name);
        }).catch(function(err) {
            console.log(err)
        });
    };

    $scope.refreshClientPassword = function($id) {
        
        

        AlunoService.getRefreshClientPassword($id).then(function() {
            //toastr.success('Uma nova senha foi gerada e enviada ao email de cadastro');
            swal("Senha!", "Uma nova senha foi gerada e enviada ao email de cadastro", "success");
        }, function() {
            swal("Senha!", "Ocorreu um erro ao gerar nova senha", "error");
//            toastr.warning('Ocorreu um erro ao gerar nova senha');
        });
    };

    $scope.addObjective = function() {
        swal({
            title: "Adicionar objetivo",
            type: "input",
            showCancelButton: true,
            closeOnConfirm: false,
            animation: "slide-from-top",
            inputPlaceholder: "Objetivo"
        }, function(inputValue) {
            if (inputValue === false) return false;
            if (inputValue === "") {
                swal.showInputError("Informe um objetivo");
                return false
            }
            AlunoService.addObjective(inputValue).then(function(response) {
                swal("Ok!", "Objetivo adicionado", "success");
                init();
            }).catch(function(erro) {})
        });
    }

/*
    sendSocial  
 */
    $scope.sendSocial = function() {
        $scope.mynumber = '55' + $scope.client.dados.celular;
        $scope.email_cli =  $scope.client.email;
        
        // enviar Whats Sozinho
        switch ($scope.contact.crmacao) {
            case 6:
                //Whatsapp - 6
                window.location.href = 'https://api.whatsapp.com/send?phone=' + $scope.mynumber + '&text=' + $scope.contact.observacao
                // window.open('https://api.whatsapp.com/send?phone=' + $scope.mynumber + '&text=' + $scope.contact.observacao, '_blank');
                break;
            case 4:
                //Facebook - 4
                //@todo falta fazer enviar o Facebook para pessoa
                break;
            case 8:
                //E-mail - 8
                 window.location.href = "mailto:"+ $scope.email_cli;

                break;
            case 11:
                //Chat App - 11
                //envia msg e push para o Cliente
                $scope.storeMessage($scope.contact.observacao);
                break;
            default:
                //
        }
        // limpar formulario
        $scope.contact = {};
    }

// Grava o CRM
    $scope.addContact = function() {
        $scope.contact.iduser = $scope.client.id;
        var modal = $('#contato_modal');
        
        AlunoService.addContact($scope.contact)
                .then(function (response) {
                    // modal.modal('hide');
                    $scope.sendSocial();
                    alert(response.data.message);
                    // $scope.getCrm($scope.client.id);
                })
                .catch(function (err) {
                });
    }

    $scope.search = function() {
        
     
         var texto = $scope.query;
        console.log('tamano texto search ',texto);
        
        if(texto.length>2) {
            $scope.searching = true;
            AlunoService.search($scope.query).then(searchSuccess).catch(errorOnSearch);

            function searchSuccess(response) {
                $scope.persons = response.data.persons
                $scope.totalRegistros = response.data.persons
                $scope.searching = false;
            }

            function errorOnSearch(response) {
                console.log(response)
                $scope.searching = false;
            }
        }
        if(texto.length==0) { 
            // resetar a lista de clientes puxando todos
            _getClients(null, null);
         }
    }

    $scope.getCrm = function($user) {
        AlunoService.crm($user).then(function(response) {
            $scope.crm = response.data;
            console.log(response.data)
            $timeout(function() {
                $scope.$apply();
            });
        }, function(response) {});
    }

    $scope.prospect_modal = function(){
        $('#prospect_modal').modal('toggle');
        $scope.loading = false;
    }

    $scope.register = function() {
        $scope.loading = true;
        $scope.errors = {};
  
        AlunoService.register($scope.prospect).then(function(response) {

            $scope.responsavel = response.data.client;
            console.log('$scope.responsavel',$scope.responsavel);
            $scope.loading = false;
            if (response.data.cod == '-1') {
                // swal("Atenção!", response.data.text, "error");
                
                // sou dependete de alguem
                
                // inicio personalização da mensagem conforme o sexo
                var span = document.createElement("span");
                if($scope.prospect.genero == "M" && $scope.responsavel.genero  == "M" ) {
                     span.innerHTML='O <b>'+$scope.prospect.name+' </b> está com o mesmo e-mail do cliente <b>'+$scope.responsavel.name+'</b>. <br> Deseja tornar o <b>' + $scope.prospect.name+' </b> um depentente do cliente <b> '+$scope.responsavel.name+' </b>?';

                } 
                else if($scope.prospect.genero == "F" && $scope.responsavel.genero  == "F" ) {
                     span.innerHTML='A <b>'+$scope.prospect.name+' </b> está com o mesmo e-mail da cliente <b>'+$scope.responsavel.name+'</b>. <br> Deseja tornar a <b>' + $scope.prospect.name+' </b> um depentente da cliente <b> '+$scope.responsavel.name+' </b>?';

                }
                 else if($scope.prospect.genero == "M" && $scope.responsavel.genero  == "F" ) {
                     span.innerHTML='O <b>'+$scope.prospect.name+' </b> está com o mesmo e-mail da cliente <b>'+$scope.responsavel.name+'</b>. <br> Deseja tornar o <b>' + $scope.prospect.name+' </b> um depentente da cliente <b> '+$scope.responsavel.name+' </b>?';

                }
                  else if($scope.prospect.genero == "F" && $scope.responsavel.genero  == "M" ) {
                     span.innerHTML='A <b>'+$scope.prospect.name+' </b> está com o mesmo e-mail do cliente <b>'+$scope.responsavel.name+'</b>. <br> Deseja tornar a <b>' + $scope.prospect.name+' </b> um depentente do cliente <b> '+$scope.responsavel.name+' </b>?';

                } else {
                     span.innerHTML='O <b>'+$scope.prospect.name+' </b> está com o mesmo e-mail do cliente <b>'+$scope.responsavel.name+'</b>. <br> Deseja tornar o <b>' + $scope.prospect.name+' </b> um depentente do cliente <b> '+$scope.responsavel.name+' </b>?';     
                 
                }
                 // fim personalização da mensagem conforme o sexo
                swal({
                     title: "ATENÇÃO!",
                  content: span,
                  icon: "warning",
                  buttons: true,
                  buttons: {
                      cancel: {
                        text: "Cancelar",
                        value: null,
                        visible: true,
                        className: "",
                        closeModal: true,
                      },
                      confirm: {
                        text: "Sim Desejo!",
                        value: true,
                        visible: true,
                        className: "",
                        closeModal: true
                      }},
                  dangerMode: true,

                }).then((confirmar) => {
                  if (confirmar) {
                    // aqui chama o ajax
                  
                     $http({
                            url: '../../api/vincularDepenteAluno/' + $scope.responsavel.id,
                            data: $scope.prospect,
                            method: "POST"
                        }).then(function(response) {
                            $scope.redirect();
                            swal("Vinculado!", "Cliente vinculado com sucesso!", "success");
                        });
                  } else {
                    // swal("Your imaginary file is safe!");
                  }
                });



                // fim do if dependete
                // 
                // 
                

            } else {
                swal("Ok!", 'Cadastro realizado com sucesso', "success");
                $scope.editing = false;
                $('#prospect_modal').modal('hide');
                window.location.reload()
            }
        }).catch(function(response) {
            $scope.loading = false;
            $scope.errors = response.data;
            swal({
                title: 'Erro',
                text: '' + formatErrorMsg(response.data),
                html: true
            })
        })
    }

    function formatErrorMsg(data) {
        var html = '';
        angular.forEach(data, function(item, index) {
            html += '<div>' + item + '</div>'
        })
        return html;
    }
    $scope.atualizar = function() {
        $scope.errors = [];
        //toastr.success('Atualizando dados ....')
        AlunoService.atualizar($scope.client.dados.controle_id).then(function(response) {
           // toastr.success('Cliente atualizado com sucesso!!');
             swal("Cliente!", "Cliente atualizado com sucesso!!", "success");
        }, function(response) {
            $scope.errors = response.data;
            //toastr.warning(formatErrorMsg(response.data));
            swal({
                title: 'Erro',
                text: '' + formatErrorMsg(response.data),
                html: true
            })
            //toastr.warning('Erro ao atualizar dados do cliente');
            $scope.errors = response.data;
        })
        $scope.editing = false;
    };
    $scope.update = function() {
        $scope.errors = []
        toastr.success('Atualizando dados ....');

        console.log('$scope.client', $scope.client);
       
        $scope.meus_dados = $scope.client;
        console.log('parente _id ',$scope.meus_dados.parent_id);


        AlunoService.update($scope.client.id, $scope.client).then(function(response) {
            _refreshPersonsScope($scope.client.id, response.data)

            $scope.responsavel = response.data.client;


             if ($scope.meus_dados.parent_id == 0 && response.data.cod != '-1') {
                swal("Sucesso!", "Cliente atualizado com sucesso!", "success");
                $scope.redirect();
            }

            
            console.log('$scope.responsavel', response.data);

            if (response.data.cod == '-1') {
                // swal("Aviso Adriano!", response.data.text, "error");
                // 
                //  // inicio personalização da mensagem conforme o sexo
                var span = document.createElement("span");
               
                if($scope.meus_dados.genero == "M" && $scope.responsavel.genero  == "M" ) {
                      span.innerHTML='O <b>'+$scope.meus_dados.name+' </b> está com o mesmo e-mail do cliente <b>'+$scope.responsavel.name+'</b>. <br> Deseja tornar o <b>' + $scope.meus_dados.name+' </b> um depentente do cliente <b> '+$scope.responsavel.name+' </b>?';

                } 
                else if($scope.meus_dados.genero == "F" && $scope.responsavel.genero  == "F" ) {
                     span.innerHTML='A <b>'+$scope.meus_dados.name+' </b> está com o mesmo e-mail da cliente <b>'+$scope.responsavel.name+'</b>. <br> Deseja tornar a <b>' + $scope.meus_dados.name+' </b> um depentente da cliente <b> '+$scope.responsavel.name+' </b>?';

                }
                 else if($scope.meus_dados.genero == "M" && $scope.responsavel.genero  == "F" ) {
                     span.innerHTML='O <b>'+$scope.meus_dados.name+' </b> está com o mesmo e-mail da cliente <b>'+$scope.responsavel.name+'</b>. <br> Deseja tornar o <b>' + $scope.meus_dados.name+' </b> um depentente da cliente <b> '+$scope.responsavel.name+' </b>?';

                }
                  else if($scope.meus_dados.genero == "F" && $scope.responsavel.genero  == "M" ) {
                     span.innerHTML='A <b>'+$scope.meus_dados.name+' </b> está com o mesmo e-mail do cliente <b>'+$scope.responsavel.name+'</b>. <br> Deseja tornar a <b>' + $scope.meus_dados.name+' </b> um depentente do cliente <b> '+$scope.responsavel.name+' </b>?';

                } else {
                     span.innerHTML='O <b>'+$scope.meus_dados.name+' </b> está com o mesmo e-mail do cliente <b>'+$scope.responsavel.name+'</b>. <br> Deseja tornar o <b>' + $scope.meus_dados.name+' </b> um depentente do cliente <b> '+$scope.responsavel.name+' </b>?';     
                 
                }
                 // fim personalização da mensagem conforme o sexo
                swal({
                     title: "ATENÇÃO!",
                  content: span,
                  icon: "warning",
                  buttons: true,
                  buttons: {
                      cancel: {
                        text: "Cancelar",
                        value: null,
                        visible: true,
                        className: "",
                        closeModal: true,
                      },
                      confirm: {
                        text: "Sim Desejo!",
                        value: true,
                        visible: true,
                        className: "",
                        closeModal: true
                      }},
                  dangerMode: true,

                }).then((confirmar) => {
                  if (confirmar) {
                    // aqui chama o ajax
                  
                     $http({
                            url: '../../api/vincularUpdateDepenteAluno/' + $scope.responsavel.id,
                            data: $scope.meus_dados,
                            method: "POST"
                        }).then(function(response) {
                            $scope.redirect();
                            swal("Vinculado!", "Cliente vinculado com sucesso!", "success");
                        });
                  } else {
                    // swal("Your imaginary file is safe!");
                  }
                });

                // fim do if dependente
                // 
                // 

            }

            
            
        }, function(response) {
            
            $scope.errors = response.data;
            //toastr.warning(formatErrorMsg(response.data));
            swal({
                title: 'Erro',
                text: '' + formatErrorMsg(response.data),
                html: true
            })
            //toastr.warning('Erro ao atualizar dados do cliente');
            $scope.errors = response.data;
        })
        $scope.editing = false;
    };


    $scope.motivoDesistencia = function(client) {
        swal({
  text: 'Qual motivo da desistência?',
  content: "input",
  button: {
    text: "ENVIAR",
    closeModal: false,
  },
})
.then(motivo => {
  if (!motivo) throw null;
    console.log(motivo);
    
    swal.stopLoading();
    swal.close();

    $scope.update();
                $timeout(function() {
                    AlunoService.convert(client.id,motivo).then(onCorvertionSuccess).catch(onConvertionError)
                }, 1200);

    return 'ok';
  // return fetch(`https://itunes.apple.com/search?term=${name}&entity=movie`);
})
.catch(err => {
  if (err) {
    swal("Oh não!", "Erro ao executar esta operação!", "error");
  } else {
    swal.stopLoading();
    swal.close();
  }
});
    }
    $scope.convert = function(client) {



        swal({
            title: "Atenção!",
            text: getMessage(client),
          icon: "warning",
          buttons: true,
          buttons: {
              cancel: {
                text: "Não",
                value: null,
                visible: true,
                className: "",
                closeModal: true,
              },
              confirm: {
                text: "Sim",
                value: true,
                visible: true,
                className: "",
                closeModal: true
              }},
          dangerMode: true,


        }).then((confirmar) => {
          if (confirmar) {
            // aqui chama o ajax
              
             switch (client.role) {
                case 'cliente':
                    
                $scope.motivoDesistencia(client);
                
                break
             }
             if(client.role != 'cliente') {
                $scope.update();
                $timeout(function() {
                    AlunoService.convert(client.id,'-').then(onCorvertionSuccess).catch(onConvertionError)
                }, 1200);

             }
            
           
          } else {
              
            // swal("Your imaginary file is safe!");
            
          }
        });



        function onCorvertionSuccess(response) {
            swal('Sucesso!', 'prospect convertido para cliente', 'success');
            $scope.client.role = "cliente";
            _refreshPersonsScope(client.id, response.data)
            $timeout(function() {
                $scope.$apply()
            }, 600)
        }

        function onConvertionError(response) {
            console.log(response)
            swal('Erro!', response.data || 'Ocorreu um erro')
        }

        function getMessage(client) {
            switch (client.role) {
                case 'cliente':
                    {
                        return 'Você tem certeza que deseja reverter este cliente para uma oportunidade?'
                        break
                    }
                case 'prospect':
                    {
                        return 'Você tem certeza que deseja converter para cliente?'
                        break;
                    }
                default:
                    {
                        return 'Confirmar esta ação?'
                    }
            }
        }
    }

    function _refreshPersonsScope($id, $data) {
        angular.forEach($scope.persons.data, function(item, i) {
            if (item.id == $id) {
                $scope.persons.data[i] = $data
            }
        });
    }


    $scope.getCep = function(cep) {
        var url = 'https://viacep.com.br/ws/' + cep + '/json/';
        console.log(url);
        $http({
            url: url,
            headers: {
                   'user': undefined,
                   'unidade': undefined,
                   'unity': undefined,
                   '_token': undefined,

                 },
            method: "GET"
        }).then(function(response) {
            
            console.log(response);
            $scope.client.dados.endereco = response.data.logradouro;
            $scope.client.dados.bairro = response.data.bairro;
            $scope.client.dados.cidade = response.data.localidade;
            $scope.client.dados.estado = response.data.uf;



        }, function(response) {
            console.log('Opsss... Algo deu errado!');
        });
    }





    $scope.openForm = function() {
        $scope.editing = $scope.editing ? false : true;
    };
    $scope.closeProfileInfo = function() {
        $scope.showData = false;
    };
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
        $scope.$apply(function() {
            $scope.type = $(e.target).data().type;
        }, 600);
    })
    $('#prospect_modal').on('show.bs.modal', function(e) {
        $('#form-prospect')[0].reset()
    })
}); // fim controler alunoController
app.controller('alunoEditarController', function($http, $scope) {
    $scope.cliente = {};
    $scope.cliente.bairro = $("#bairro").val();
    $scope.cliente.cidade = $("#cidade").val();
}); // fim controler alunoEditarController