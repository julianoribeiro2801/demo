app.controller('spmControllers', function ($http, $scope, $timeout) {
    // GFMS
    // LISTA PROGRAMAS POR ACADEMIA
    $scope.programas = [];
    $http({
        url: 'api/spm/getProgramas',
        method: "POST"
    }).then(function (response) {
        $scope.programas = response.data;
    }, function (response) {
        console.log('Opsss... Algo deu errado ao buscar programas!');
    });

    $scope.listaProgramas = function () {
        $scope.programas = [];
        $http({
            url: 'api/spm/getProgramas',
            method: "POST"
        }).then(function (response) {
            $scope.programas = response.data;
        }, function (response) {
            console.log('Opsss... Algo deu errado ao buscar programas!');
        });
    };

    // LISTA NIVEIS POR ACADEMIA
    $scope.niveis = [];
    $http({
        url: 'api/spm/getNiveis',
        method: "POST"
    }).then(function (response) {
        $scope.niveis = response.data;
    }, function (response) {
        console.log('Opsss... Algo deu errado ao buscar niveis!');
    });

    $scope.listaLocais = function () {
        $scope.niveis = [];
        $http({
            url: 'api/spm/getLocais',
            method: "POST"
        }).then(function (response) {
            $scope.niveis = response.data;
        }, function (response) {
            console.log('Opsss... Algo deu errado ao buscar niveis!');
        });
    };

    // LISTA LOCAIS POR ACADEMIA
    $scope.locais = [];
    $http({
        url: 'api/spm/getLocais',
        method: "POST"
    }).then(function (response) {
        $scope.locais = response.data;
    }, function (response) {
        console.log('Opsss... Algo deu errado ao buscar locais!');
    });

    // LISTA PROFESSORES POR ACADEMIA
    $scope.professores = [];
    $scope.professor = [];
    $http({
        url: 'api/spm/getProfessores',
        method: "POST"
    }).then(function (response) {
        $scope.professores = response.data;
    }, function (response) {
        console.log('Opsss... Algo deu errado ao buscar professores!');
    });

    // LISTA GFM POR ACADEMIA
    $scope.spms = [];
    $http({
        url: 'api/spm/getSpms',
        method: "POST"
    }).then(function (response) {
        $scope.spms = response.data;

        $scope.duracaoTotal = 0;
        $scope.valorTotal = 0;
        $scope.minTotal = 0;
        $scope.maxTotal = 0;
        $scope.ocupacaoTotal = 0;
        $scope.vacanciaTotal = 0;
        $scope.mediaTaxa = 0;
        var tamanho = response.data.length;
        for (var i = 0; i < tamanho; i++) {
            $scope.capacidadeTotal = parseInt($scope.capacidadeTotal) + parseInt(response.data[i].capacidade_max);
            $scope.duracaoTotal = parseInt($scope.duracaoTotal) + parseInt(response.data[i].duracao);
            $scope.valorTotal = parseInt($scope.valorTotal) + parseInt(response.data[i].valor);
            $scope.minTotal = parseInt($scope.minTotal) + parseInt(response.data[i].capacidade_min);
            $scope.maxTotal = parseInt($scope.maxTotal) + parseInt(response.data[i].capacidade_max);
            $scope.ocupacaoTotal = parseFloat($scope.ocupacaoTotal) + parseFloat(response.data[i].ocupacao);
            $scope.vacanciaTotal = parseFloat($scope.vacanciaTotal) + parseFloat(response.data[i].vacancia);
            $scope.mediaTaxa = parseFloat($scope.mediaTaxa) + parseFloat(response.data[i].media);
        }
        $scope.percentual = parseFloat(($scope.vacanciaTotal * 100 ) / $scope.capacidadeTotal);
        $scope.mediaTaxa = Math.round($scope.mediaTaxa / tamanho);
    }, function (response) {
        console.log('Opsss... Algo deu errado ao buscar spms!');
    });
    $scope.capacidadeTotal = 0;
    $scope.listaSpms = function () {
        $http({
            url: 'api/spm/getSpms',
            method: "POST"
        }).then(function (response) {
            $scope.spms = response.data;

            $scope.duracaoTotal = 0;
            $scope.valorTotal = 0;
            $scope.minTotal = 0;
            $scope.maxTotal = 0;
            $scope.ocupacaoTotal = 0;
            $scope.vacanciaTotal = 0;
            $scope.mediaTaxa = 0;
            $scope.capacidadeTotal = 0;
            var tamanho = response.data.length;
            for (var i = 0; i < tamanho; i++) {
                $scope.capacidadeTotal = parseInt($scope.capacidadeTotal) + parseInt(response.data[i].capacidade_max);
                $scope.duracaoTotal = parseInt($scope.duracaoTotal) + parseInt(response.data[i].duracao);
                $scope.valorTotal = parseInt($scope.valorTotal) + parseInt(response.data[i].valor);
                $scope.minTotal = parseInt($scope.minTotal) + parseInt(response.data[i].capacidade_min);
                $scope.maxTotal = parseInt($scope.maxTotal) + parseInt(response.data[i].capacidade_max);
                $scope.ocupacaoTotal = parseFloat($scope.ocupacaoTotal) + parseFloat(response.data[i].ocupacao);
                $scope.vacanciaTotal = parseFloat($scope.vacanciaTotal) + parseFloat(response.data[i].vacancia);
                $scope.mediaTaxa = parseFloat($scope.mediaTaxa) + parseFloat(response.data[i].media);
            }
            $scope.percentual = parseFloat(($scope.vacanciaTotal * 100 ) / $scope.capacidadeTotal);
            $scope.mediaTaxa = Math.round($scope.mediaTaxa / tamanho);
        }, function (response) {
            console.log('Opsss... Algo deu errado ao buscar spms!');
        });
    };

    $scope.taxaOcupacao = [];
    $http({
        url: 'api/spm/getTaxaOcupacao',
        method: "POST"
    }).then(function (response) {
        $scope.taxaOcupacao = response.data;
    }, function (response) {
        console.log('Opsss... Algo deu errado ao buscar spms!');
    });

    $scope.getTaxaOcupacao = function () {
        $http({
            url: 'api/spm/getTaxaOcupacao',
            method: "POST"
        }).then(function (response) {
            $scope.taxaOcupacao = response.data;
        }, function (response) {
            console.log('Opsss... Algo deu errado ao buscar spms!');
        });
    };

    $scope.addSpm = function () {
        $scope.statusBotoesSpm(true);
        $http({
            url: 'api/spm/addSpm',
            method: "POST"
        }).then(function (response) {
            $scope.spm = response.data;
            $scope.listaSpms();
            $scope.editingData[$scope.spm.id] = true;
            // ATIVA OS BOTOES
            $scope.statusBotoesSpm(false);
        }, function (response) {
            console.log('Opsss... Algo deu errado!');
        });
    };

    $scope.editingData = {};

    $scope.alterSpm = function (spm) {
        $scope.editingData[spm.id] = true;
    };

    $scope.upSpm = function (spm) {
        $scope.statusBotoesSpm(true);
        $http({
            url: 'api/spm/upSpm',
            data: {
                id: spm.id,
                idlocal: spm.idlocal,
                idprograma: spm.idprograma,
                idfuncionario: spm.idfuncionario,
                nivel: spm.nivel,
                privado: spm.privado,
                gfmspm: spm.gfmspm,
                hora_inicio: spm.hora_inicio,
                duracao: spm.duracao,
                domingo: spm.domingo,
                segunda: spm.segunda,
                terca: spm.terca,
                quarta: spm.quarta,
                quinta: spm.quinta,
                sexta: spm.sexta,
                sabado: spm.sabado,
                valor: spm.valor,
                capacidade_max: spm.capacidade_max,
                capacidade_min: spm.capacidade_min,
                equalizador: spm.equalizador,
                revisado: spm.revisado
            },
            method: "POST"
        }).then(function (response) {
            $scope.editingData[spm.id] = false;
            $scope.getTaxaOcupacao();
            $scope.listaProgramas();
            $scope.listaSpms();
            // ATIVA OS BOTOES
            $scope.statusBotoesSpm(false);
        }, function (response) {
            console.log('Opsss... Algo deu errado!');
        });
    };

    $scope.adicionaAula = function (nmprograma) {
        $scope.statusBotoesSpm(true);
        $http({
            url: 'api/spm/addAula',
            data: {
                nmprograma: nmprograma
            },
            method: "POST"
        }).then(function (response) {
            $('#addAula').modal('hide')
            $scope.listaProgramas();
            // ATIVA OS BOTOES
            $scope.statusBotoesSpm(false);
        }, function (response) {
            console.log('Opsss... Algo deu errado!');
        });
    };

    $scope.delSpm = function (id) {
        if (id !== undefined) {
            swal({
                title: "Tem certeza?",
                text: "Tem certeza que deseja deletar esse spm?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Sim, tenho certeza!",
                cancelButtonText: "Cancelar",
                closeOnConfirm: false
            }, function () {
                $http({
                    url: 'api/spm/delSpm/' + id,
                    method: "POST"
                }).then(function (response) {
                    $scope.listaSpms();
                    swal("Deletado!", "Spm deletado com sucesso!", "success");
                });
            });
        } else {
            swal("Opsss!", "Selecione um spm para deletar!", "error");
        }
    };

    // LISTA TODOS OS CLIENTES
    $scope.clients = [];
    $http({
        url: 'api/spm/getClients',
        method: "POST"
    }).then(function (response) {
        $scope.clients = response.data;
        $timeout(function () {
            $('#clienteSelect').addClass('chosen-select');
            $('.chosen-select').chosen({width: "100%"});
        }, 500);
    }, function (response) {
        console.log('Opsss... Algo deu errado na busca dos clientes!');
    });

    $scope.cliente = [];
    $scope.postIdSpm = function (id) {
        $scope.cliente.spmId = id;

        $scope.incricoes = [];
        $http({
            url: 'api/spm/getInscricoes',
            data: {
                id: id
            },
            method: "POST"
        }).then(function (response) {
            $scope.incricoes = response.data;
        }, function (response) {
            console.log('Opsss... Algo deu errado na busca dos clientes!');
        });
    };

    $scope.msgAddCliente = '';
    $scope.adicionaCliente = function (cliente) {
        $scope.statusBotoesSpm(true);
        $http({
            url: 'api/spm/adicionaCliente',
            data: {
                spmId: cliente.spmId,
                clienteId: cliente.clienteId
            },
            method: "POST"
        }).then(function (response) {
            $scope.msgAddCliente = response.data.text;
            $timeout(function () {
                $scope.msgAddCliente = '';
            }, 1500);
            $scope.postIdSpm(cliente.spmId);
            // ATIVA OS BOTOES
            $scope.statusBotoesSpm(false);
            $scope.listaSpms();
        }, function (response) {
            console.log('Opsss... Algo deu errado!');
        });
    };

    $scope.reservaDelSpm = function (spmId, clienteId) {
        $scope.statusBotoesSpm(true);
        $http({
            url: 'api/spm/reservaDelSpm',
            data: {
                spmId: spmId,
                clienteId: clienteId
            },
            method: "POST"
        }).then(function (response) {
            $scope.msgAddCliente = response.data.text;
            $timeout(function () {
                $scope.msgAddCliente = '';
            }, 1500);
            $scope.postIdSpm(spmId);
            // ATIVA OS BOTOES
            $scope.statusBotoesSpm(false);
            $scope.listaSpms();
        }, function (response) {
            console.log('Opsss... Algo deu errado!');
        });
    };

    $scope.statusBotoesSpm = function (sts) {
        $scope.addSpmBtn = sts;
        $scope.delSpmBtn = sts;
        $scope.editSpmBtn = sts;
        $scope.saveSpmBtn = sts;
    };

    $scope.nivel = function (nivel) {
        switch (nivel) {
            case '1':
                return 'INICIANTE';
                break;
            case '2':
                return 'INTERMEDIARIO';
                break;
            case '3':
                return 'AVANÇADO';
                break;
            default:
                return '-';
                break;
        }
    };

    $scope.diaSemana = function (dias) {
        var tamanho = dias.length;
        var diassemana = '';
        for (var i = 0; i < tamanho; i++) {
            switch (dias[i]) {
                case '1':
                    dias[i] = 'D, ';
                    break;
                case '2':
                    dias[i] = '2º, ';
                    break;
                case '3':
                    dias[i] = '3º, ';
                    break;
                case '4':
                    dias[i] = '4º, ';
                    break;
                case '5':
                    dias[i] = '5º, ';
                    break;
                case '6':
                    dias[i] = '6º, ';
                    break;
                case '7':
                    dias[i] = 'S, ';
                    break;
            }
            diassemana += dias[i];
        }
        diassemana = diassemana.substr(0, diassemana.length - 2);
        return diassemana;
    };
});