function chamaTela(tela) {
    window.location.href = "/admin/coresApp/" + tela;
};

function hexToRgb(hex) {
    if (typeof hex !== "undefined") {
        if (hex.length > 0) {
            var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
            r = parseInt(result[1], 16);
            g = parseInt(result[2], 16);
            b = parseInt(result[3], 16);
            console.log('rgba', r, g, b);
            setcor = 'rgba(' + r + ', ' + g + ', ' + b + ',' + ($('#txttransparencia').val() / 100) + ')';
            $(".conteudo-overlay").css("background-color", setcor);
        }
    }
}

function hexToRgbGen(hex, transp, tags) {
    if (typeof hex !== "undefined") {
        if (hex.length > 0) {
            var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
            r = parseInt(result[1], 16);
            g = parseInt(result[2], 16);
            b = parseInt(result[3], 16);
            console.log('rgba', r, g, b);
            setcor = 'rgba(' + r + ', ' + g + ', ' + b + ',' + ($(transp).val() / 100) + ')';
            //$(".conteudo-overlay").css("background-color", setcor);
            $(tags).css("background-color", setcor);
        }
    }
}
function hexToRgbGenFix(hex, transp, tags) {
    if (typeof hex !== "undefined") {
        if (hex.length > 0) {
            var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
            r = parseInt(result[1], 16);
            g = parseInt(result[2], 16);
            b = parseInt(result[3], 16);
            console.log('rgba', r, g, b);
            setcor = 'rgba(' + r + ', ' + g + ', ' + b + ',' + (transp / 100) + ')';
            //$(".conteudo-overlay").css("background-color", setcor);
            $(tags).css("background-color", setcor);
        }
    }
}

function showVal() {
    val = $('#txttransparencia').val();
    $("#slider-value").html(val);
    if ($("#txtcorfundo").length <= 7) {
        hexToRgb($("#txtcorfundo").val());
    }
}

function showValCoach() {
    val = $('#txttranspcoach').val();
    $("#slider-value-coach").html(val);
    hexToRgbGen($("#txtcorfundocoach").val(), '#txttranspcoach', '.fundo-coach');
}

function showValMsg() {
    //val = $('#txttranspmsg').val();
    //("#slider-value-msg").html(val);
    hexToRgbGenFix($("#txtcorsim").val(), 50, '.linha_corsim');
    hexToRgbGenFix($("#txtcornao").val(), 50, '.linha_cornao');
}

function showValSel() {
    val = $('#txttranspsel').val();
    $("#slider-value-sel").html(val);
    hexToRgbGen($("#txtcorimparsel").val(), '#txttranspsel', '.cor_linha_impar');
    hexToRgbGen($("#txtcorparsel").val(), '#txttranspsel', '.cor_linha_par');
    
}

function showValTreino() {
    val = $('#txttransptreino').val();
    $("#slider-value-treino").html(val);
    hexToRgbGen($("#txttreinocorsim").val(), '#txttransptreino', '.tabb');
    hexToRgbGen($("#txttreinocornao").val(), '#txttransptreino', '.tabbb');
    hexToRgbGen($("#txttreinosel").val(), '#txttransptreino', '.tab');
}

function showValMusc() {
    val = $('#txttranspmusc').val();
    $("#slider-value-musc").html(val);
    hexToRgbGen($("#txttreinocorsim").val(), '#txttranspmusc', '.conteudo-overlay');
    hexToRgbGen($("#txttreinocornao").val(), '#txttranspmusc', '.tabbb');
    hexToRgbGen($("#txttreinosel").val(), '#txttranspmusc', '.tab');
}

function convertToRgb(hex) {
    var setcor = '';
    if (typeof hex !== "undefined") {
        var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
        r = parseInt(result[1], 16);
        g = parseInt(result[2], 16);
        b = parseInt(result[3], 16);
        //        console.log('rgba',r,g,b);
        setcor = 'rgba(' + r + ', ' + g + ', ' + b + '' + ($('#txttransparencia').val() / 100) + ')';
        //$(".conteudo-overlay").css("background-color", setcor);
    } else {
        setcor = '';
    }
    return setcor;
}

function testaCor(cor, classe, elverdadeiro, tag, campo) {
    
    
    if (cor !== '') {
        $(campo).val(cor);
        // esse aqui muda a paleta
       // $(classe).css(tag, cor);
        $(classe).css('backgroundColor', cor);
        // esse muda direto na tela
        if (elverdadeiro == '.conteudo-overlay') {
            if (cor.length > 7) {
                $(".conteudo-overlay").css("background-color", cor);
            } else {
                hexToRgb(cor);
            }
        } else {
            $(elverdadeiro).css(tag, cor);
        }
        //$(classe).css('backgroundColor',cor);
        // $(elverdadeiro).css('background-color', cor);
        // $('.menu_tema footer').css('background-color', cor);
        $('#nomecor').val(cor);
        
        
       // gravaTema();
    }
}

function gravaTemassss() {
    


    var qualtela = $('#qualTela').val();

    $.post('/admin/api/temas/addTema', {

        cor_background : $("#txtcorfundo").val(),
        tema_id : 2,
        nometela : qualtela,
        idunidade : 1,
        cor_topo : $("#txtcortopo").val(),
        cor_rodape : $("#txtcorrodape").val(),
        cor_texto_topo : $("#txtcortextotopo").val(),
        cor_texto_rodape : $("#txtcortextorodape").val(),
        cor_nome_aluno : $("#txtnomealuno").val(),
        cor_texto_telamensagem : $("#txttextomensagem").val(),
        cor_mensagem_corsim : $("#txtcorsim").val(),
        cor_mensagem_cornao : $("#txtcornao").val(),
        cor_gfm_corsim : $("#txtcorimpargfm").val(),
        cor_textogfm : $("#txttextogfm").val(),
        cor_fundodiagfm : $("#txtfundodia").val(),
        cor_gfm_cornao : $("#txtcorpargfm").val(),
        cor_icone_gfm : $("#txtcoriconegfm").val(),
        
        cor_spm_corsim : $("#txtcorimparspm").val(),
        cor_textospm : $("#txttextospm").val(),
        cor_fundodiaspm : $("#txtfundodiaspm").val(),
        cor_spm_cornao : $("#txtcorparspm").val(),
        cor_icone_spm : $("#txtcoriconespm").val(),
        
        
        cor_textocross : $("#txttextotreinocross").val(),
        cor_fundodesccross : convertToRgb($("#txtdesccross").val()),
        cor_botao_partiu : $("#txtcorbotaopartiu").val(),
        cor_botao_iniciar : $("#txtcorbotaoiniciar").val(),
        cor_cross_corsim : $("#txtcrosscorsim").val(),
        cor_cross_cornao : $("#txtcrosscornao").val(),
        transparencia : $("#txttransparencia").val(),
        ////////////////menu
        cor_textomenu : $("#txttextomenu").val(),
        cor_fundomenu : $("#txtfundomenu").val(),
        cor_iconemenu : $("#txticonemenu").val(),
        //////////////musculação
        cor_texto_musc : $("#txttextomusc").val(),
        cor_ativo_musc : $("#txtcirculolaranja").val(),
        cor_inativo_musc : $("#txtcirculoroxo").val(),
        cor_partiu_musc : $("#txtpartiumusc").val(),
        cor_iniciar_musc : $("#txtiniciarmusc").val(),
        //////////////treino
        cor_texto_treino : $("#txttextotreino").val(),
        cor_treino_corsim : $("#txttreinocorsim").val(),
        cor_treino_cornao : $("#txttreinocornao").val(),
        cor_treino_corselec : $("#txttreinosel").val(),
        /////////////chat
        cor_texto_chat : $("#txttextochat").val(),
        cor_fundo_chat : $("#txtcorfundochat").val(),
        cor_fundo_enviada : $("#txtenviadachat").val(),
        cor_fundo_recebida : $("#txtrecebidachat").val(),
        /////////////contato
        cor_texto_contato : $("#txttextocontato").val(),
        cor_iniciar_contato : $("#txtcoriniciarcontato").val(),
        ///////////////////////////////clube//////////////////////////////////////////////////////////////        
        cor_texto_clube : $("#txttextoclube").val(),
        cor_cab_clube : $("#txtcorcabclube").val(),
        cor_impar_clube : $("#txtcorimparclube").val(),
        cor_par_clube : $("#txtcorparclube").val(),
        ///////////////////ciclismo
        cor_textociclismo : $("#txttextotreinociclismo").val(),
        cor_fundodescciclismo : $("#txtdescciclismo").val(),
        cor_botao_partiuciclismo : $("#txtcorbotaopartiuciclismo").val(),
        cor_botao_iniciarciclismo : $("#txtcorbotaoiniciarciclismo").val(),
        cor_ciclismo_corsim : $("#txtciclismocorsim").val(),
        cor_ciclismo_cornao : $("#txtciclismocornao").val(),
        ///////////////////corrida
        cor_textocorrida : $("#txttextotreinocorrida").val(),
        cor_fundodesccorrida : $("#txtdesccorrida").val(),
        cor_botao_partiucorrida : $("#txtcorbotaopartiucorrida").val(),
        cor_botao_iniciarcorrida : $("#txtcorbotaoiniciarcorrida").val(),
        cor_corrida_corsim : $("#txtcorridacorsim").val(),
        cor_corrida_cornao : $("#txtcorridacornao").val(),
        //////////////////////reserva
        cor_textoreserva : $("#txttextoreserva").val(),
        cor_fundoreserva : $("#txtfundoreserva").val(),
        cor_botao_reserva : $("#txtbotaoreserva").val(),
        cor_boneco_reserva : $("#txtfundoboneco").val(),
                    

        ///////////exercicio
        cor_textoexer : $("#txttextoexer").val(),
        cor_fundoexer : $("#txtfundoexer").val(),
        cor_imparexer : $("#txtcorimparexer").val(),
        cor_parexer : $("#txtcorparexer").val(),
        //////////selecionar treino
        cor_iconeseleciona : $("#txticonesel").val(),
        cor_fundoiconeseleciona : $("#txtfundoiconesel").val(),
        cor_selecionaimpar : $("#txtcorimparsel").val(),
        cor_selecionapar : $("#txtcorparsel").val(),
        //////////empresa
        cor_textoempresa : $("#txttextoempresa").val(),
        cor_fundoempresa : $("#txtcorfundoempresa").val(),
        cor_cabecempresa : $("#txtcorcabecempresa").val(),
        ////////////planejmanto
        cor_textoplan : $('#txttextoplan').val(),
        cor_fundoplan : $('#txtcorfundoplan').val(),
        cor_diaplan : $('#txtcordiaplan').val(),
        cor_horaplan : $('#txtcorhoraplan').val(),
        cor_atvplan : $('#txtcoratvplan').val(),
        ///conquistas
        cor_textoconquistas : $('#txttextoconquistas').val(),
        cor_fundoconquistas : $('#txtfundoconquistas').val()

    }, function (retorno) {


    });
    
    
}


function testaCorChange(cor, classe, elverdadeiro, tag, campo) {
    

    if (cor !== '') {
        $(campo).val(cor);
        // esse aqui muda a paleta
        $(classe).css(backgroundColor, cor);
        // esse muda direto na tela
        if (elverdadeiro == '.conteudo-overlay') {
            hexToRgb(cor);
            //$(".conteudo-overlay").css("background-color", cor);
        } else {
            $(elverdadeiro).css(tag, cor);
        }
        //$(classe).css('backgroundColor',cor);
        // $(elverdadeiro).css('background-color', cor);
        // $('.menu_tema footer').css('background-color', cor);
        $('#nomecor').val(cor);
    }
    
    gravaTema();
    
}
app.controller('temasController', function($http, $scope, $timeout) {
    $http.defaults.headers.common.user = $('meta[name="user_logued"]').attr('content');
    $http.defaults.headers.common.unity = $('meta[name="unidade"]').attr('content');
    $scope.temas = [];
    $scope.tema = {};
    $scope.tela = {};
    $scope.telaSel = {};
    //$scope.tema_id = 2;
    
    
    $scope.cor_background_home="";
    $scope.cor_topo_home="";
    $scope.cor_rodape_home="";
    $scope.cor_texto_topo_home="";
    $scope.cor_texto_rodape_home="";    
    
    $scope.prop = {
        "type": "select",
        "name": "Service",
        "value": "Service 3",
        "values": ["Service 1", "Service 2", "Service 3", "Service 4"]
    };
    //$scope.corfundo = '';
    showVal();
    getTemas();
    getTema();
    gerarTema();

    function gerarTema() {
        $http({
            url: '/admin/api/temas/gerarTema/' + $http.defaults.headers.common.unity,
            method: "get"
        }).then(function(response) {
            $scope.telasTema = response.data.telasTema;
        }, function(response) {
            console.log('Opsss... Algo deu errado ao buscar programas!');
        });
    }
    
    function getTema() {
        $http({
            url: '/api/getTemaAcademia/' + $http.defaults.headers.common.unity,
            method: "get"
        }).then(function(response) {
            $scope.tema_id = response.data.dados.idtema;
            
            

        }, function(response) {
            console.log('Opsss... Algo deu errado ao buscar programas!');
        });
        
        
        //$scope.tema_id = idtema;
        $http({
            url: '/admin/api/temas/getTelasTema',
            data: {
                id:  $scope.tema_id
            },
            method: "POST"
        }).then(function(response) {
            $scope.telas = response.data.telas;
            // essa linha faz a tela vir selecionada
            $scope.telset = $scope.telas[idx];
            // setar cor ao carregar tela
            $scope.changeTela($scope.telset);
        }, function(response) {
            console.log('Opsss... Algo deu errado!');
        });        
        
    }    

    function getTemas() {
        $http({
            url: '/admin/api/temas/getTemas',
            method: "get"
        }).then(function(response) {
            $scope.temas = response.data.temas;
        }, function(response) {
            console.log('Opsss... Algo deu errado ao buscar programas!');
        });
    }
    // quando seleciona a imagem executa automatico
    $scope.uploadFile = function(files) {
        var fd = new FormData();
        //Take the first selected file
        fd.append("image", files[0]);
        console.log(files);
        var uploadUrl = '/admin/api/temas/addLogomarca';
        $http.post(uploadUrl, fd, {
            withCredentials: true,
            headers: {
                'Content-Type': undefined
            },
            transformRequest: angular.identity
        }).then(function(response) {
            console.log('nome image', response.data.image_name);
            if (response.data.status == 'success') {
                $scope.telaSel.image = response.data.image_name
                $scope.telaSel.logomarca_home = $scope.telaSel.image;
            } else {
                $scope.telaSel.image = '';
            }
        }, function(response) {
            toastr.error('Opsss... Algo deu errado!', 'Error');
        });
    };

    $scope.changeTela = function(telas) {
        
        if (tela_atual != telas.nometela) {
            console.log(' tela - ' + telas.nometela);
            chamaTela(telas.nometela);
        }


        $http({
            url: '/admin/api/temas/getTelaTema',
            data: {
                id: telas.id,
                idtema: $scope.tema_id,
                nometela: telas.nometela
            },
            method: "POST"
        }).then(function(response) {
            $scope.telaSel = response.data.tela[0];
            $scope.telaSel.id = response.data.tela[0].id;
            $("#txttransparencia").val(response.data.tela[0].transparencia);
            $("#slider-value").html(response.data.tela[0].transparencia);
            ///home

            if (telas.nometela=='Home'){
                testaCor(response.data.tela[0].cor_background, '.btn-home', '.conteudo-overlay', 'backGroundColor', '.txtcorfundo');
                testaCor(response.data.tela[0].cor_topo, '.bt-roxo2', '.menu_tema header', 'backgroundColor', '.txtcortopo');
                testaCor(response.data.tela[0].cor_rodape, '.bt-roxo3', '.menu_tema footer', 'backgroundColor', '.txtcorrodape');
                testaCor(response.data.tela[0].cor_texto_topo, '.bt-esqueceu', '.academia-name', 'color', '.txtcortextotopo');
                testaCor(response.data.tela[0].cor_texto_rodape, '.bt-texto-rodape', '.texto-rodape', 'color', '.txtcortextorodape');
            } else{
                testaCor(response.data.home[0].cor_background_home, '.btn-home', '.conteudo-overlay', 'backGroundColor', '.txtcorfundo');
                testaCor(response.data.home[0].cor_topo_home, '.bt-roxo2', '.menu_tema header', 'backgroundColor', '.txtcortopo');
                testaCor(response.data.home[0].cor_rodape_home, '.bt-roxo3', '.menu_tema footer', 'backgroundColor', '.txtcorrodape');
                testaCor(response.data.home[0].cor_texto_topo_home, '.bt-esqueceu', '.academia-name', 'color', '.txtcortextotopo');
                testaCor(response.data.home[0].cor_texto_rodape_home, '.bt-texto-rodape', '.texto-rodape', 'color', '.txtcortextorodape');
                
            }    
            testaCor(response.data.tela[0].cor_nome_aluno, '.bt-texto-nome', '.profile-name', 'color', '.txtnomealuno');
            //testaCor(response.data.tela[0].logomarca_home, '.bt-texto-nome', '.bt-texto-rodape', 'color', '.txtnomealuno');
            //coach
            testaCor(response.data.tela[0].cor_fundo_coach,'.bt-corfundocoach','.fundo-coach','backgroundColor','.txtcorfundocoach');
            testaCor(response.data.tela[0].cor_texto_coach, '.bt-cortextocoach','.instrutor','color','.txttextocoach');
            testaCor(response.data.tela[0].transparencia_coach, '.bt-roxo3', '.menu_tema footer', 'backgroundColor', '.txttranspcoach');
            $("#txttranspcoach").val(response.data.tela[0].transparencia_coach);
            $("#slider-value-coach").html(response.data.tela[0].transparencia_coach);
            showValCoach();
            showVal(response.data.tela[0].transparencia);
            ///tela mensagem    
            testaCor(response.data.tela[0].cor_texto_telamensagem, '.bt-cortextomensagem', '.texto_msg', 'color', '.txttextomensagem');
            
            
            testaCor(response.data.tela[0].cor_mensagem_corsim, '.bt-corsim', '.linha_corsim', 'backgroundColor', '.txtcorsim');
            testaCor(response.data.tela[0].cor_mensagem_cornao, '.bt-cornao', '.linha_cornao', 'backgroundColor', '.txtcornao');
            showValMsg();
            
            ///tela gfm
            testaCor(response.data.tela[0].cor_textogfm, '.bt-textogfm', '.textogfm', 'color', '.txttextogfm');
            testaCor(response.data.tela[0].cor_fundodiagfm, '.bt-fundodia', '.fundo_dia', 'backgroundColor', '.txtfundodia');
            testaCor(response.data.tela[0].cor_gfm_corsim, '.bt-corimpargfm', '.cor_impar_gfm', 'backgroundColor', '.txtcorimpargfm');
            testaCor(response.data.tela[0].cor_gfm_cornao, '.bt-corpargfm', '.cor_par_gfm', 'backgroundColor', '.txtcorpargfm');
            testaCor(response.data.tela[0].cor_icone_gfm, '.bt-coriconegfm', '.branco', 'color', '.txtcoriconegfm');
            ///tela cross
            testaCor(response.data.tela[0].cor_textocross, '.bt-txttextotreinocross', '.three', 'color', '.txttextotreinocross');
            testaCor(response.data.tela[0].cor_fundodesccross, '.bt-fundodesccross', '.fundo-desc-cross', 'backgroundColor', '.txtdesccross');
            testaCor(response.data.tela[0].cor_botao_partiu, '.bt-corbotaopartiu', '.partiu', 'backgroundColor', '.txtcorbotaopartiu');
            testaCor(response.data.tela[0].cor_botao_iniciar, '.bt-corbotaoiniciar', '.iniciar', 'backgroundColor', '.txtcorbotaoiniciar');
            testaCor(response.data.tela[0].cor_cross_corsim, '.bt-cross_corsim', '.linha_cross_sim', 'backgroundColor', '.txtcrosscorsim');
            testaCor(response.data.tela[0].cor_cross_cornao, '.bt-cross_cornao', '.linha_cross_nao', 'backgroundColor', '.txtcrosscornao');
            ///tela spm
            testaCor(response.data.tela[0].cor_textospm, '.bt-textospm', '.textospm', 'color', '.txttextospm');
            testaCor(response.data.tela[0].cor_fundodiaspm, '.bt-fundodiaspm', '.fundo_diaspm', 'backgroundColor', '.txtfundodiaspm');
            testaCor(response.data.tela[0].cor_spm_corsim, '.bt-corimparspm', '.cor_impar_spm', 'backgroundColor', '.txtcorimparspm');
            testaCor(response.data.tela[0].cor_spm_cornao, '.bt-corparspm', '.cor_par_spm', 'backgroundColor', '.txtcorparspm');
            testaCor(response.data.tela[0].cor_icone_spm, '.bt-coriconespm', '.branco', 'color', '.txtcoriconespm');
            
            ///menu
            testaCor(response.data.tela[0].cor_textomenu, '.bt-textomenu', '.textomenu', 'color', '.txttextomenu');
            testaCor(response.data.tela[0].cor_fundomenu, '.bt-fundomenu', '.barra', 'backgroundColor', '.txtfundomenu');
            testaCor(response.data.tela[0].cor_iconemenu, '.bt-iconemenu', '.coment', 'color', '.txticonemenu');
            //musculacao
            testaCor(response.data.tela[0].cor_texto_musc, '.bt-textomenu', '.texto-musculacao', 'color', '.txttextomusc');
            testaCor(response.data.tela[0].cor_ativo_musc, '.bt-circulolaranja', '.circulo-laranja', 'backgroundColor', '.txtcirculolaranja');
            testaCor(response.data.tela[0].cor_inativo_musc, '.bt-circuloroxo', '.circulo-roxo', 'backgroundColor', '.txtcirculoroxo');
            testaCor(response.data.tela[0].cor_partiu_musc, '.bt-partiumusc', '.partiumusc', 'backgroundColor', '.txtpartiumusc');
            testaCor(response.data.tela[0].cor_iniciar_musc, '.bt-iniciarmusc', '.iniciarmusc', 'backgroundColor', '.txtiniciarmusc');
            //////////TREINO MUSC
            testaCor(response.data.tela[0].cor_texto_treino, '.bt-textotreino', '.textotreino', 'color', '.txttextotreino');
            testaCor(response.data.tela[0].cor_treino_corsim, '.bt-treinocorsim', '.tabb', 'backgroundColor', '.txttreinocorsim');
            testaCor(response.data.tela[0].cor_treino_cornao, '.bt-treinocornao', '.tabbb', 'backgroundColor', '.txttreinocornao');
            testaCor(response.data.tela[0].cor_treino_corselec, '.bt-treinosel', '.tab', 'backgroundColor', '.txttreinosel');
            /////////CHAT 
            testaCor(response.data.tela[0].cor_texto_chat, '.bt-cortextochat', '.textotreino', 'color', '.txttextochat');
            testaCor(response.data.tela[0].cor_fundo_chat, '.bt-corfundochat', '.fundo', 'backgroundColor', '.txtcorfundochat');
            testaCor(response.data.tela[0].cor_fundo_enviada, '.bt-enviada', '.um', 'backgroundColor', '.txtenviadachat');
            testaCor(response.data.tela[0].cor_fundo_recebida, '.bt-recebida', '.dois', 'backgroundColor', '.txtrecebidachat');
            //contato
            testaCor(response.data.tela[0].cor_texto_contato, '.bt-cortextocontato', '.texto', 'color', '.txttextocontato');
            testaCor(response.data.tela[0].cor_iniciar_contato, '.bt-coriniciarcontato', '.iniciar', 'backgroundColor', '.txtcoriniciarcontato');
            //empresa
            testaCor(response.data.tela[0].cor_texto_empresa, '.bt-cortextoempresa', '.cont', 'color', '.txttextoempresa');
            testaCor(response.data.tela[0].cor_fundo_empresa, '.bt-corfundoempresa', '.tabb', 'backgroundColor', '.txtcorfundoempresa');
            //////clube
            testaCor(response.data.tela[0].cor_texto_clube, '.bt-textoclube', '.empresas', 'color', '.txttextoclube');
            testaCor(response.data.tela[0].cor_cab_clube, '.bt-corcabclube', '.clube_vantagens', 'backgroundColor', '.txtcorcabclube');
            testaCor(response.data.tela[0].cor_impar_clube, '.bt-corimparclube', '.cor_impar_clube', 'backgroundColor', '.txtcorimparclube');
            testaCor(response.data.tela[0].cor_par_clube, '.bt-corparclube', '.cor_par_clube', 'backgroundColor', '.txtcorparclube');
            ///tela ciclismo
            testaCor(response.data.tela[0].cor_textociclismo, '.bt-txttextotreinociclismo', '.three', 'color', '.txttextotreinociclismo');
            testaCor(response.data.tela[0].cor_fundodescciclismo, '.bt-fundodescciclismo', '.fundo-desc-ciclismo', 'backgroundColor', '.txtdescciclismo');
            testaCor(response.data.tela[0].cor_botao_partiuciclismo, '.bt-corbotaopartiuciclismo', '.partiuciclismo', 'backgroundColor', '.txtcorbotaopartiuciclismo');
            testaCor(response.data.tela[0].cor_botao_iniciarciclismo, '.bt-corbotaoiniciar', '.iniciar', 'backgroundColor', '.txtcorbotaoiniciar');
            testaCor(response.data.tela[0].cor_ciclismo_corsim, '.bt-ciclismo_corsim', '.linha_ciclismo_sim', 'backgroundColor', '.txtciclismocorsim');
            testaCor(response.data.tela[0].cor_ciclismo_cornao, '.bt-ciclismo_cornao', '.linha_ciclismo_nao', 'backgroundColor', '.txtciclismocornao');
            ///tela corrida
            testaCor(response.data.tela[0].cor_textocorrida, '.bt-txttextotreinocorrida', '.geral', 'color', '.txttextotreinocorrida');
            testaCor(response.data.tela[0].cor_fundodesccorrida, '.bt-fundodesccorrida', '.fundo-desc-corrida', 'backgroundColor', '.txtdesccorrida');
            testaCor(response.data.tela[0].cor_botao_partiucorrida, '.bt-corbotaopartiucorrida', '.partiucorrida', 'backgroundColor', '.txtcorbotaopartiucorrida');
            testaCor(response.data.tela[0].cor_botao_iniciarcorrida, '.bt-corbotaoiniciar', '.iniciar', 'backgroundColor', '.txtcorbotaoiniciar');
            testaCor(response.data.tela[0].cor_corrida_corsim, '.bt-corrida_corsim', '.linha_corrida_sim', 'backgroundColor', '.txtcorridacorsim');
            testaCor(response.data.tela[0].cor_corrida_cornao, '.bt-corrida_cornao', '.linha_corrida_nao', 'backgroundColor', '.txtcorridacornao');
            //reserva
            testaCor(response.data.tela[0].cor_textoreserva, '.bt-cortextoreserva', '.texto_reserva', 'color', '.txttextoreserva');
            testaCor(response.data.tela[0].cor_fundoreserva, '.bt-corfundoreserva', '.conteudo-overlay', 'backgroundColor', '.txtfundoreserva');
            testaCor(response.data.tela[0].cor_botao_reserva, '.bt-corbotaoreserva', '.reservar', 'backgroundColor', '.txtbotaoreserva');
            testaCor(response.data.tela[0].cor_boneco_reserva, '.bt-corfundoboneco', '.img', 'backgroundColor', '.txtfundoboneco');
            ///exercicio
            testaCor(response.data.tela[0].cor_textoexer, '.bt-textoexer', '.textoexer', 'color', '.txttextoexer');
            testaCor(response.data.tela[0].cor_fundoexer, '.bt-fundoexer', '.barra', 'backgroundColor', '.txtfundoexer');
            testaCor(response.data.tela[0].cor_imparexer, '.bt-corimparexer', '.cor_impar_exer', 'backgroundColor', '.txtcorimparexer');
            testaCor(response.data.tela[0].cor_parexer, '.bt-corparexer', '.cor_par_exer', 'backgroundColor', '.txtcorparexer');
            ///seleciona treino
            testaCor(response.data.tela[0].cor_iconeseleciona, '.bt-iconesel','.icon-trophy','color','.txticonesel');
            testaCor(response.data.tela[0].cor_fundoiconeseleciona,'.bt-fundoiconesel','.colun','backgroundColor','.txtfundoiconesel');
            testaCor(response.data.tela[0].cor_selecionaimpar, '.bt-corimparsel','.cor_linha_impar','backgroundColor','.txtcorimparsel');
            testaCor(response.data.tela[0].cor_selecionapar, '.bt-corparsel','.cor_linha_par','backgroundColor','.txtcorparsel');
            $("#txttranspsel").val(response.data.tela[0].transparencia);
            $("#slider-value-sel").html(response.data.tela[0].transparencia);
            showValSel();            
            
            //empresa
            testaCor(response.data.tela[0].cor_textoempresa, '.bt-cortextoempresa', '.equip', 'color', '.txttextoempresa');
            testaCor(response.data.tela[0].cor_fundoempresa, '.bt-corfundoempresa', '.conteudo-overlay', 'backgroundColor', '.txtcorfundoempresa');
            testaCor(response.data.tela[0].cor_cabecempresa, '.bt-corcabecempresa', '.top', 'backgroundColor', '.txtcorcabecempresa');
            //planejamento
            testaCor(response.data.tela[0].cor_textoplan, '.bt-cortextoplan', '.linha_psa', 'color', '.txttextoplan');
            testaCor(response.data.tela[0].cor_fundoplan, '.bt-corfundoplan', '.fundo', 'backgroundColor', '.txtcorfundoplan');
            testaCor(response.data.tela[0].cor_diaplan, '.bt-cordiaplan', '.dia_semana', 'backgroundColor', '.txtcordiaplan');
            testaCor(response.data.tela[0].cor_horaplan, '.bt-corhoraplan', '.hora_semana', 'backgroundColor', '.txtcorhoraplan');
            testaCor(response.data.tela[0].cor_atvplan, '.bt-coratvplan', '.des_aula_psa', 'backgroundColor', '.txtcoratvplan');
            //conquistas
            testaCor(response.data.tela[0].cor_textoconquistas, '.bt-cortextoconquistas', '.texto', 'color', '.txttextoconquistas');
            testaCor(response.data.tela[0].cor_fundoconquistas, '.bt-corfundoconquistas', '.corllll', 'backgroundColor', '.txtfundoconquistas');
            
            
        }, function(response) {
            console.log('Opsss... Algo deu errado!');
        });
    };

    $scope.changeTema = function(idtema, idx) {
        // LISTA CIDADE
        
        

        //$scope.tema_id = idtema;
        $http({
            url: '/admin/api/temas/getTelasTema',
            data: {
                id: idtema
            },
            method: "POST"
        }).then(function(response) {
            $scope.telas = response.data.telas;
            // essa linha faz a tela vir selecionada
            $scope.telset = $scope.telas[idx];
            // setar cor ao carregar tela
            $scope.changeTela($scope.telset);
        }, function(response) {
            console.log('Opsss... Algo deu errado!');
        });
    };

    // 
   


    function qualtela() {
        var qualtela = $('#qualTela').val();
        var idx = 0;
        switch (qualtela) {
            case "Home":
                idx = 0;
                break;
            case "Mensagem":
                idx = 1;
                break;
            case "Gfm":
                idx = 2;
                break;
            case "Natação":
                idx = 3;
                break;
            case "Cross":
                idx = 4;
                break;
            case "Musculação":
                idx = 5;
                break;
            case "Menu":
                idx = 6;
                break;
            case "Treino":
                idx = 7;
                break;
            case "Chat":
                idx = 8;
                break;
            case "Contato":
                idx = 9;
                break;
            case "Reserva":
                idx = 10;
                break;
            case "Empresa":
                idx = 11;
                break;
            case "Clube":
                idx = 12;
                break;
            case "Ciclismo":
                idx = 13;
                break;
            case "Corrida":
                idx = 14;
                break;
            case "Selecionar_Treino":
                idx = 15;
                break;
            case "Exercicio":
                idx = 16;
                break;
            case "Conquistas":
                idx = 17;
                break;
            case "Planejamento":
                idx = 18;
                break;
            case "Modal":
                idx = 19;
                break;
            case "Coach":
                idx = 20;
                break;
            default:
                idx = 0;
        }
        // seto na mao para char sempre o tema 1
        var idtema = $scope.tema_id;
        
        
        // validar tema Padrao
        var unidade = $http.defaults.headers.common.unity;

        var url_post = 'https://sistemapcb.net.br/api/getTemaAcademia/' + unidade;
       
        
          // $scope.url_tema = 'css/tema';
          $http.get(url_post).then(function (data) {
             
            var total = data.total;
            console.log('cores getTemaAcademia', total);
            console.log(data);
            if (total > 0) {
             // home 
             $scope.changeTema(idtema, idx);
            } else {
             // tema padrao
             idtema = 1;
             $scope.changeTema(idtema, idx);
            }
          });

    }
    qualtela();
    $scope.gravarTema = function(tela_id, tela, tema_id) {
        $scope.telaSel={};
        
        $scope.tema_id = tema_id;
        var qualtela = $('#qualTela').val();

        
        $scope.telaSel.cor_background = $("#txtcorfundo").val();
        $scope.telaSel.tema_id = tema_id;
        $scope.telaSel.nometela = qualtela;
        
        $scope.telaSel.idunidade = $http.defaults.headers.common.unity;

        
        $scope.telaSel.cor_topo = $("#txtcortopo").val();
        $scope.telaSel.cor_rodape = $("#txtcorrodape").val();
        $scope.telaSel.cor_texto_topo = $("#txtcortextotopo").val();
        $scope.telaSel.cor_texto_rodape = $("#txtcortextorodape").val();
        $scope.telaSel.cor_nome_aluno = $("#txtnomealuno").val();
        $scope.telaSel.cor_texto_telamensagem = $("#txttextomensagem").val();
        $scope.telaSel.cor_mensagem_corsim = $("#txtcorsim").val();
        $scope.telaSel.cor_mensagem_cornao = $("#txtcornao").val();
        $scope.telaSel.cor_gfm_corsim = $("#txtcorimpargfm").val();
        $scope.telaSel.cor_textogfm = $("#txttextogfm").val();
        $scope.telaSel.cor_fundodiagfm = $("#txtfundodia").val();
        $scope.telaSel.cor_gfm_cornao = $("#txtcorpargfm").val();
        $scope.telaSel.cor_icone_gfm = $("#txtcoriconegfm").val();
        
        $scope.telaSel.cor_spm_corsim = $("#txtcorimparspm").val();
        $scope.telaSel.cor_textospm = $("#txttextospm").val();
        $scope.telaSel.cor_fundodiaspm = $("#txtfundodiaspm").val();
        $scope.telaSel.cor_spm_cornao = $("#txtcorparspm").val();
        $scope.telaSel.cor_icone_spm = $("#txtcoriconespm").val();
        
        
        $scope.telaSel.cor_textocross = $("#txttextotreinocross").val();
        $scope.telaSel.cor_fundodesccross = convertToRgb($("#txtdesccross").val());
        $scope.telaSel.cor_botao_partiu = $("#txtcorbotaopartiu").val();
        $scope.telaSel.cor_botao_iniciar = $("#txtcorbotaoiniciar").val();
        $scope.telaSel.cor_cross_corsim = $("#txtcrosscorsim").val();
        $scope.telaSel.cor_cross_cornao = $("#txtcrosscornao").val();
        $scope.telaSel.transparencia = $("#txttransparencia").val();
        ////////////////menu
        $scope.telaSel.cor_textomenu = $("#txttextomenu").val();
        $scope.telaSel.cor_fundomenu = $("#txtfundomenu").val();
        $scope.telaSel.cor_iconemenu = $("#txticonemenu").val();
        //////////////musculação
        $scope.telaSel.cor_texto_musc = $("#txttextomusc").val();
        $scope.telaSel.cor_ativo_musc = $("#txtcirculolaranja").val();
        $scope.telaSel.cor_inativo_musc = $("#txtcirculoroxo").val();
        $scope.telaSel.cor_partiu_musc = $("#txtpartiumusc").val();
        $scope.telaSel.cor_iniciar_musc = $("#txtiniciarmusc").val();
        //////////////treino
        $scope.telaSel.cor_texto_treino = $("#txttextotreino").val();
        $scope.telaSel.cor_treino_corsim = $("#txttreinocorsim").val();
        $scope.telaSel.cor_treino_cornao = $("#txttreinocornao").val();
        $scope.telaSel.cor_treino_corselec = $("#txttreinosel").val();
        /////////////chat
        $scope.telaSel.cor_texto_chat = $("#txttextochat").val();
        $scope.telaSel.cor_fundo_chat = $("#txtcorfundochat").val();
        $scope.telaSel.cor_fundo_enviada = $("#txtenviadachat").val();
        $scope.telaSel.cor_fundo_recebida = $("#txtrecebidachat").val();
        /////////////contato
        $scope.telaSel.cor_texto_contato = $("#txttextocontato").val();
        $scope.telaSel.cor_iniciar_contato = $("#txtcoriniciarcontato").val();
        ///////////////////////////////clube//////////////////////////////////////////////////////////////        
        $scope.telaSel.cor_texto_clube = $("#txttextoclube").val();
        $scope.telaSel.cor_cab_clube = $("#txtcorcabclube").val();
        $scope.telaSel.cor_impar_clube = $("#txtcorimparclube").val();
        $scope.telaSel.cor_par_clube = $("#txtcorparclube").val();
        ///////////////////ciclismo
        $scope.telaSel.cor_textociclismo = $("#txttextotreinociclismo").val();
        $scope.telaSel.cor_fundodescciclismo = $("#txtdescciclismo").val();
        $scope.telaSel.cor_botao_partiuciclismo = $("#txtcorbotaopartiuciclismo").val();
        $scope.telaSel.cor_botao_iniciarciclismo = $("#txtcorbotaoiniciarciclismo").val();
        $scope.telaSel.cor_ciclismo_corsim = $("#txtciclismocorsim").val();
        $scope.telaSel.cor_ciclismo_cornao = $("#txtciclismocornao").val();
        ///////////////////corrida
        $scope.telaSel.cor_textocorrida = $("#txttextotreinocorrida").val();
        $scope.telaSel.cor_fundodesccorrida = $("#txtdesccorrida").val();
        $scope.telaSel.cor_botao_partiucorrida = $("#txtcorbotaopartiucorrida").val();
        $scope.telaSel.cor_botao_iniciarcorrida = $("#txtcorbotaoiniciarcorrida").val();
        $scope.telaSel.cor_corrida_corsim = $("#txtcorridacorsim").val();
        $scope.telaSel.cor_corrida_cornao = $("#txtcorridacornao").val();
        //////////////////////reserva
        $scope.telaSel.cor_textoreserva = $("#txttextoreserva").val();
        $scope.telaSel.cor_fundoreserva = $("#txtfundoreserva").val();
        $scope.telaSel.cor_botao_reserva = $("#txtbotaoreserva").val();
        $scope.telaSel.cor_boneco_reserva = $("#txtfundoboneco").val();
                    

        ///////////exercicio
        $scope.telaSel.cor_textoexer = $("#txttextoexer").val();
        $scope.telaSel.cor_fundoexer = $("#txtfundoexer").val();
        $scope.telaSel.cor_imparexer = $("#txtcorimparexer").val();
        $scope.telaSel.cor_parexer = $("#txtcorparexer").val();
        //////////selecionar treino
        $scope.telaSel.cor_iconeseleciona = $("#txticonesel").val();
        $scope.telaSel.cor_fundoiconeseleciona = $("#txtfundoiconesel").val();
        $scope.telaSel.cor_selecionaimpar = $("#txtcorimparsel").val();
        $scope.telaSel.cor_selecionapar = $("#txtcorparsel").val();
        $scope.telaSel.cor_selecionapar = $("#txtcorparsel").val();
        //////////empresa
        $scope.telaSel.cor_textoempresa = $("#txttextoempresa").val();
        $scope.telaSel.cor_fundoempresa = $("#txtcorfundoempresa").val();
        $scope.telaSel.transparencia = $("#txttranspsel").val();
        ////////////planejmanto
        $scope.telaSel.cor_textoplan = $('#txttextoplan').val();
        $scope.telaSel.cor_fundoplan = $('#txtcorfundoplan').val();
        $scope.telaSel.cor_diaplan = $('#txtcordiaplan').val();
        $scope.telaSel.cor_horaplan = $('#txtcorhoraplan').val();
        $scope.telaSel.cor_atvplan = $('#txtcoratvplan').val();
        ///conquistas
        $scope.telaSel.cor_textoconquistas = $('#txttextoconquistas').val();
        $scope.telaSel.cor_fundoconquistas = $('#txtfundoconquistas').val();
        //coach
        $scope.telaSel.cor_fundo_coach = $('#txtcorfundocoach').val();
        $scope.telaSel.cor_texto_coach = $('#txttextocoach').val();
        $scope.telaSel.transparencia_coach = $('#txttranspcoach').val();
        //////////////////
        $http({
            url: '/admin/api/temas/addTema',
            data: $scope.telaSel,
            method: "POST"
        }).then(function(response) {
            if (response.data.type == 'success') {
                swal("Ok!", response.data.text, "success");
            } else {
                swal("Erro!", response.data.text, "error");
            }
        }, function(response) {
            console.log('Opsss... Algo deu errado!');
        });
        
        $http({
            url: '/api/geraCss/' + $http.defaults.headers.common.unity,
            method: "GET"
        }).then(function(response) {
            /*            if (response.data.type == 'success') {
                            swal("Ok!", response.data.text, "success");
                        } else {
                            swal("Erro!", response.data.text, "error");
                        }*/
        }, function(response) {
            console.log('Opsss... Algo deu errado coma  geação do css!');
        });
        // https://sistemapcb.net.br/api/geraCss/80
    };
    //  fim funcao
    //  
});