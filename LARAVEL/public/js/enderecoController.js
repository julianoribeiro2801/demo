

app.controller('enderecoController', function($http, $scope) {
    $scope.endereco = {};

    $scope.consultaEndereco = function() {
         $http.get('https://viacep.com.br/ws/' + $scope.endereco.cep + '/json/').then(function(response) {
                console.log(response.data);
                // 
                $scope.endereco.endereco = response.data.logradouro;
                $scope.endereco.bairro = response.data.bairro;
                $scope.endereco.cidade = response.data.localidade;
                $scope.endereco.estado = response.data.uf;
            }, function(response) {
                console.log(response);
            });
        };

 });