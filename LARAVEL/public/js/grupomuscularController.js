
app.factory('GrupomuscularService', function ($http, $q, $timeout, $window) {
    var config = {
        api: '/api/grupomuscular'
    };
    $http.defaults.headers.common.user = $('meta[name="user_id"]').attr('content') || 1;
    $http.defaults.headers.common.unidade = 1;
    $http.defaults.headers.common._token = $('meta[name="csrf_token"]').attr('content');

    var services = {
        register: register,
        show: show,
        destroy: destroy,
        update: update,
        all: all,
        modalidades: modalidades,
        objetivos: objetivos,
        niveis: niveis
    };


    function all() {
        return $http.get(config.api);
    }

    function register($data) {
        return $http.post(config.api, $data);
    }

    function show($id) {
        return $http.get(config.api + '/grupomuscular/' + $id);
    }
    function modalidades($url) {
        return $http.get($url);
    }
    function objetivos($url) {
        return $http.get($url);
    }

    function niveis($url) {
        return $http.get($url);
    }

    function destroy($id) {
        return $http.delete(config.api + '/' + $id);
    }

    function update($id, $data) {
        return $http.put(config.api + '/' + $id, $data);
    }

    return services;
});

app.controller('grupomuscularController', function ($http, $scope, GrupomuscularService, $timeout) {
    $scope.loading = false;
    $scope.unidade = 1;
    $scope.user = 1;
    $scope.grupomuscular = {};
    $scope.programatreinamento = {};
    $scope.programastreinamento = [];

    //$scope.exibe = "display: none;";

    $scope.exibe = "";
    //$scope.expande = "display: none;";
    $scope.recolhe = "";
    $scope.grupomuscular.dados = {};
    $scope.gruposmusculares = [];
    $scope.editing = false;
    $scope.showData = false;
    $scope.errors = {};
    $scope.teste =
            $scope.modalidade = {};
    $scope.nmmodalidade = {};
    $scope.nmobjetivo = {};
    $scope.modalidades = [];

    $scope.edita = [];

    $scope.objetivo_ = {};
    $scope.objetivo = {};
    $scope.objetivos = [];

    $scope.objetivo1 = {};
    $scope.objetivo2 = {};
    $scope.objetivo3 = {};
    $scope.objetivo4 = {};
    $scope.objetivo5 = {};
    $scope.objetivo6 = {};
    $scope.objetivos1 = [];
    $scope.objetivos2 = [];
    $scope.objetivos3 = [];
    $scope.objetivos4 = [];
    $scope.objetivos5 = [];
    $scope.objetivos6 = [];


    $scope.nivel_ = {};
    $scope.nivel1 = {};
    $scope.nivel2 = {};
    $scope.nivel3 = {};
    $scope.nivel4 = {};
    $scope.nivel5 = {};
    $scope.nivel6 = {};
    $scope.niveis1 = [];
    $scope.niveis2 = [];
    $scope.niveis3 = [];
    $scope.niveis4 = [];
    $scope.niveis5 = [];
    $scope.niveis6 = [];

    $scope.objetivo = {};
    $scope.objetivos = [];
    $scope.processos = [];

    $scope.musculacao = [];
    $scope.ginastica = [];

    $scope.habilidades = [];
    
    
    var valueCpHab = '';
    var textoCpHab = '';    
    /*for (var i = 1; i <= 2; i++) {
        if (i == 1) {
            valueCpHab = 0;
            textoCpHab = "Hab1";
        }
        if (i == 2) {
            valueCpHab = 0;
            textoCpHab = "Hab2";
        }
        $scope.habValue = {
            valor: valueCpHab,
            texto: textoCpHab
        };
        $scope.habilidades.push($scope.habValue);
    }*/
    
    
    getProcessos();
    
    function getProcessos(){
        $http({
            url: '../prescricao/getProcessos',
            method: "GET"
        }).then(function (response) {
                $scope.processos = response.data;

            
        }, function (response) {
            if (typeof response.data.nmobjetivo !== "undefined") {
                toastr.warning("O campo nome é obrigatório");
            }            
            console.log('Opsss... Algo deu errado na busca dos objetivos!');
        });  
    };
    
    
    $scope.statusProcesso = function(processo) {
        swal({
            title: "Tem certeza?",
            text: "Tem certeza que deseja atualizar o status desse processo?",
            icon: "warning",
            buttons: true,
            buttons: {
                cancel: {
                    text: "Cancelar",
                    value: null,
                    visible: true,
                    className: "",
                    closeModal: true,
                },
                confirm: {
                    text: "Sim, tenho certeza!",
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: true
                }
            },
            dangerMode: true,
        }).then((isConfirm) => {
            if (isConfirm) {
                $http({
                    url: '../prescricao/upProcesso',
                    data: {
                        processo: processo
                    },
                    method: "POST"
                }).then(function(response) {
                    swal("Sucesso!", response.data.text, "success");
                }, function(response) {
                    console.log('Opsss... Algo deu errado!');
                });
            } else {
                getProcessos();
            }
        });
        // fim swal
        // 
    }        
    
    $scope.addHabilidade = function (indice) {

        
        $scope.habValue = {
            id: 0,
            nmhabilidade: "",
            st: -1
        };

        $scope.habilidades.unshift($scope.habValue);
        //$scope.matriculas.reverse();


         // ativa o auto completar
        setTimeout(function(){  
                $(".chosen-select").chosen({width: "100%"});
             }, 200);          
    };    
    
    $scope.habilidadeDel = function (nivel, id) {
        
        alert('' + id);

        
        swal({
     title: "Tem certeza?",
            text: "Tem certeza que deseja deletar essa habilidade?",
  icon: "warning",
  buttons: true,
  buttons: {
      cancel: {
        text: "Cancelar",
        value: null,
        visible: true,
        className: "",
        closeModal: true,
      },
      confirm: {
        text: "Sim, tenho certeza!",
        value: true,
        visible: true,
        className: "",
        closeModal: true
      }},
  dangerMode: true,

}).then((willDelete) => {
  if (willDelete) {
    // aqui chama o ajax
        $http({
            url: '../prescricao/delHabilidade/' + id,
            method: "GET"
        }).then(function (response) {

                $scope.getHabilidade(nivel);

        }, function (response) {
            if (typeof response.data.nmobjetivo !== "undefined") {
                toastr.warning("O campo nome é obrigatório");
            }            
            console.log('Opsss... Algo deu errado na busca dos objetivos!');
        });        
        
  } else {
    // swal("Your imaginary file is safe!");
  }
});
// fim swal        
        
    }    
    
    
    $scope.habilidadeSave = function (nivel,indice,nmhabilidade, id) {
        
        $scope.habilidades[indice].st=1;
        
        $http({
            url: '../prescricao/addHabilidade',
            data: {
                id:id,
                nivel:nivel,
                nmhabilidade: nmhabilidade
            },
            method: "POST"
        }).then(function (response) {
            if (response.data.type=='success'){
                toastr.success(response.data.text);
            } else {

                toastr.warning(response.data.text);
            }  
        }, function (response) {
            if (typeof response.data.type !== "success") {
                swal("Salvar!", "Habilidade gravada com sucesso", "success");
                
            }  else{
                swal("Salvar!", "O campo nome é obrigatório", "error");
            }          
            console.log('Opsss... Algo deu errado na busca dos objetivos!');
        });  
    }
    
    $scope.getHabilidade = function (nivel) {
        
        

        
        $http({
            url: '../prescricao/getHabilidade/' + nivel,
            method: "GET"
        }).then(function (response) {
                $scope.habilidades = response.data;

            
        }, function (response) {
            if (typeof response.data.nmobjetivo !== "undefined") {
                toastr.warning("O campo nome é obrigatório");
            }            
            console.log('Opsss... Algo deu errado na busca dos objetivos!');
        });  
    }
   

    $scope.natacao = []; //[{"id":10,"nmobjetivo":"Fortalecimento","niveis":[null]},{"id":11,"nmobjetivo":"Emagrecimento","niveis":[null]},{"id":12,"nmobjetivo":"Condicionamento","niveis":[{"id":67,"idunidade":"1","idmodalidade":"3","idobjetivo":"12","nmnivel":"asdasdasda","dscriterio":"saddasdasdss","created_at":"2017-09-20 13:57:44","updated_at":"2017-09-20 13:57:44"}]}];
    $scope.cross = [];//[{"id":10,"nmobjetivo":"Fortalecimento","niveis":[null]},{"id":11,"nmobjetivo":"Emagrecimento","niveis":[null]},{"id":12,"nmobjetivo":"Condicionamento","niveis":[{"id":67,"idunidade":"1","idmodalidade":"3","idobjetivo":"12","nmnivel":"asdasdasda","dscriterio":"saddasdasdss","created_at":"2017-09-20 13:57:44","updated_at":"2017-09-20 13:57:44"}]}];
    $scope.corrida = [];//[{"id":10,"nmobjetivo":"Fortalecimento","niveis":[null]},{"id":11,"nmobjetivo":"Emagrecimento","niveis":[null]},{"id":12,"nmobjetivo":"Condicionamento","niveis":[{"id":67,"idunidade":"1","idmodalidade":"3","idobjetivo":"12","nmnivel":"asdasdasda","dscriterio":"saddasdasdss","created_at":"2017-09-20 13:57:44","updated_at":"2017-09-20 13:57:44"}]}];
    $scope.ciclismo = []; //[{"id":10,"nmobjetivo":"Fortalecimento","niveis":[null]},{"id":11,"nmobjetivo":"Emagrecimento","niveis":[null]},{"id":12,"nmobjetivo":"Condicionamento","niveis":[{"id":67,"idunidade":"1","idmodalidade":"3","idobjetivo":"12","nmnivel":"asdasdasda","dscriterio":"saddasdasdss","created_at":"2017-09-20 13:57:44","updated_at":"2017-09-20 13:57:44"}]}];
    $scope.datas = [];

    $scope.toDate = function (date) {
        return new Date(date);
    };

    $scope.expande = function (modalidade) {
        /*if ($scope.expande == "display: none;"){
         $scope.expande = "";           
         } else {
         $scope.expande = "display: none;"; 
         }*/

        getNiveis(modalidade);
    };
    $scope.recolhe = function () {
        /*if ($scope.recolhe == "display: none;"){
         $scope.recolhe = "";           
         } else {
         $scope.recolhe = "display: none;"; 
         }*/
        $scope.exibe = "";
    };

    

    $scope.setModalidade = function (modalidade, nmmodalidade, objetivo, nmobjetivo) {
        $scope.objetivo_={}
        $scope.nivel_.idmodalidade=modalidade;
        $scope.nivel_.idobjetivo=objetivo;
        $scope.objetivo_.idmodalidade=modalidade;
        $scope.nmobjetivo=nmobjetivo;
        $scope.modalidade = modalidade;
        $scope.nmmodalidade = nmmodalidade;
        
        //$scope.habilidades=null;
        
        getObjetivos(modalidade);
    };
    
    
    $scope.setModalidadeEdit = function (id,modalidade, nmmodalidade, objetivo, nmobjetivo) {
        $scope.nivel_.idmodalidade=modalidade;
        $scope.nivel_.idobjetivo=objetivo;
        $scope.objetivo_.idmodalidade=modalidade;
        $scope.objetivo_.id=id;
        $scope.objetivo_.nmobjetivo=nmobjetivo;
        $scope.nmobjetivo=nmobjetivo;
        $scope.modalidade = modalidade;
        $scope.nmmodalidade = nmmodalidade;
        getObjetivos(modalidade);
    };
    
    
    $scope.setNivelEdit = function (nivel, modalidade, nmmodalidade, nmobjetivo) {
        
        
        
        $scope.nivel = nivel;
        $scope.nivel_.idmodalidade=modalidade;
        $scope.nivel_=$scope.nivel;
        //$scope.nivel_.id=nivel;
        $scope.modalidade = modalidade;
        $scope.nmmodalidade = nmmodalidade;
        $scope.nmobjetivo = nmobjetivo;
        
        $scope.getHabilidade(nivel.id);
        
    };

    getGruposmusculares();



    getModalidades();
    getObjetivosAll();

    $scope.edit = function ($grupomuscular) {
        $scope.editing = $grupomuscular.id;
        $('#grupos_exercicios').modal('show');

        if ($scope.showData === false) {
            $scope.showData = true;
        } else {
            $scope.showData = false;
        }
    };

    $scope.select = function ($grupomuscular) {
        $scope.grupomuscular = {};
        GrupomuscularService.show($grupomuscular.id).then(function (response) {
            $scope.grupomuscular = response.data;
            $('#grupos_exercicios_edit').modal('show');
            $timeout(function () {
                $scope.$apply();
            });
        }, function (response) {
            alert('Erro ao buscar informações do exercicio!');
        });
    };

    $scope.destroy = function ($grupomuscular) {
        $scope.grupomuscular = {};

        swal({
     title: "Tem certeza?",
            text: "Tem certeza que deseja deletar esse grupo?",
  icon: "warning",
  buttons: true,
  buttons: {
      cancel: {
        text: "Cancelar",
        value: null,
        visible: true,
        className: "",
        closeModal: true,
      },
      confirm: {
        text: "Sim, tenho certeza!",
        value: true,
        visible: true,
        className: "",
        closeModal: true
      }},
  dangerMode: true,

}).then((willDelete) => {
  if (willDelete) {
    // aqui chama o ajax
    GrupomuscularService.destroy($grupomuscular.id).then(function (response) {
                swal("Deletado!", "Grupo deletado com sucesso!", "success");
                getGruposmusculares();
                $timeout(function () {
                    $scope.$apply();
                });
            }, function (response) {
                swal("Erro!", "Grupo não pode ser deletado, pois esta em uso!", "error");
            });
  } else {
    // swal("Your imaginary file is safe!");
  }
});
// fim swal
// 
// 
    
    };


    $scope.addNivel = function () {
        
        $scope.nivel_.habilidades = $scope.habilidades;
        
        $http({
            url: '../prescricao/addNivel',
            data: $scope.nivel_,
            method: "POST"
        }).then(function (response) {
            if (response.data.type=='success'){
                toastr.success(response.data.text);
            } else {
                $('#modal_nivel').modal('toggle');
                toastr.warning(response.data.text);
            }  
            getObjetivosAll();
            $scope.nivel_={};
            $('#modal_nivel').modal('toggle');
        }, function (response) {
            if (typeof response.data.nmnivel !== "undefined") {
                toastr.warning("O campo nome é obrigatório");
            }
            if (typeof response.data.dscriterio !== "undefined") {
                toastr.warning("O campo critério é obrigatório");
            }        
        });

    };
    
    $scope.upNivel = function () {
        $http({
            url: '../prescricao/upNivel',
            data: $scope.nivel_,
            method: "POST"
        }).then(function (response) {
            if (response.data.type=='success'){
                 $('#modal_nivel_edit').modal('toggle');
                toastr.success(response.data.text);
            } else {
                $('#modal_nivel_edit').modal('toggle');
                toastr.warning(response.data.text);
            }  
            getObjetivosAll();
            $scope.nivel_={};
            
        }, function (response) {
            if (typeof response.data.nmnivel !== "undefined") {
                toastr.warning("O campo nome é obrigatório");
            }
            if (typeof response.data.dscriterio !== "undefined") {
                toastr.warning("O campo critério é obrigatório");
            }        
        });

    };    

    $scope.deleteNivel = function (nivel_id) {
        if (nivel_id !== undefined) {
            swal({
   title: "Tem certeza?",
                text: "Tem certeza que deseja deletar esse nível?",
  icon: "warning",
  buttons: true,
  buttons: {
      cancel: {
        text: "Cancelar",
        value: null,
        visible: true,
        className: "",
        closeModal: true,
      },
      confirm: {
        text: "Sim, tenho certeza!",
        value: true,
        visible: true,
        className: "",
        closeModal: true
      }},
  dangerMode: true,

}).then((willDelete) => {
  if (willDelete) {
    // aqui chama o ajax
   $http({
                    url: '../prescricao/deleteNivel/' + nivel_id,
                    method: "POST"
                }).then(function (response) {

                    swal("Deletado!", "Nível deletado com sucesso!", "success");
                    getObjetivosAll();
                });
  } else {
    // swal("Your imaginary file is safe!");
  }
});
// fim swal
// 
        } else {
            swal("Opsss!", "Selecione um local para deletar!", "error");
        }
    };


    $scope.deleteObjetivo = function (objetivo_id) {
        if (objetivo_id !== undefined) {
           swal({
  title: "Tem certeza?",
                text: "Tem certeza que deseja deletar esse objetivo?",
  icon: "warning",
  buttons: true,
  buttons: {
      cancel: {
        text: "Cancelar",
        value: null,
        visible: true,
        className: "",
        closeModal: true,
      },
      confirm: {
        text: "Sim, tenho certeza!",
        value: true,
        visible: true,
        className: "",
        closeModal: true
      }},
  dangerMode: true,

}).then((willDelete) => {
  if (willDelete) {
    // aqui chama o ajax
   $http({
                    url: '../prescricao/deleteObjetivo/' + objetivo_id,
                    method: "POST"
                }).then(function (response) {

                    swal("Deletado!", "Objetivo deletado com sucesso!", "success");
                    getObjetivosAll();
                });
  } else {
    // swal("Your imaginary file is safe!");
  }
});
// fim swal
      
        } else {
            swal("Opsss!", "Selecione um objetivo para deletar!", "error");
        }
    };

    $scope.totalRegistros = function () {
        return parseInt($scope.clients.length) + parseInt($scope.prospects.length);
    };

    $scope.register = function () {
        $scope.loading = true;
        $scope.errors = {};
        GrupomuscularService.register($scope.grupomuscular).then(function (response) {
            swal("Cadastrado!", "Grupo cadastrado com sucesso!", "success");
            getGruposmusculares();
            $('#grupos_exercicios_create').modal('hide');
        }, function (response) {
            $scope.loading = false;
            $scope.errors = response.data;
            alert('Erro ao cadastrar novo exercicio!');
        });
    };
    $scope.update = function () {
        GrupomuscularService.update($scope.grupomuscular.id, $scope.grupomuscular).then(function (response) {
            swal("Atualizado!", "Grupo atualizado com sucesso!", "success");
            $('#grupos_exercicios_edi').modal('hide');
            getGruposmusculares();
        }, function (response) {
            alert('Ocorreu um erro ao atualizar os dados do grupo!');
        });
    };

    $scope.openForm = function () {
        $scope.editing = $scope.editing ? false : true;
    };

    $scope.closeProfileInfo = function () {
        $scope.showData = false;
    };

    function getGruposmusculares() {
        GrupomuscularService.all().then(function (response) {
            $scope.gruposmusculares = response.data.gruposmusculares;
            $timeout(function () {
                $scope.$apply();
            });
        }, function (response) {
            alert('Erro ao buscar grupos musculares!');
        });
    }
    ;

    function getModalidades() {
        GrupomuscularService.modalidades('../prescricao/getModalidades').then(function (response) {
            $scope.modalidades = response.data;


            $timeout(function () {
                $scope.$apply();
            });
        }, function (response) {
            alert('Erro ao buscar grupos musculares!');
        });
    }
    ;

    function getObjetivos(modalidade_id) {
        GrupomuscularService.objetivos('../prescricao/getObjetivos/' + modalidade_id).then(function (response) {
            $scope.objetivos = response.data;


            $timeout(function () {
                $scope.$apply();
            });
        }, function (response) {
            alert('Erro ao buscar grupos musculares!');
        });
    }
    ;

    function getNiveis($modalidade) {
        GrupomuscularService.niveis('../prescricao/getNiveis/' + $modalidade).then(function (response) {
            if ($modalidade == 1) {
                $scope.niveis1 = response.data;
            }
            if ($modalidade == 2) {
                $scope.niveis2 = response.data;
            }
            if ($modalidade == 3) {
                $scope.niveis3 = response.data;
            }
            if ($modalidade == 4) {
                $scope.niveis4 = response.data;
            }
            if ($modalidade == 5) {
                $scope.niveis5 = response.data;
            }
            if ($modalidade == 6) {
                $scope.niveis6 = response.data;
            }
        }, function (response) {
            alert('Erro ao buscar grupos musculares!');
        });
    }
    ;
    
    
    
    
    $scope.addObjetivo = function () {
        $http({
            url: '../prescricao/addObjetivo',
            data: $scope.objetivo_,
            method: "POST"
        }).then(function (response) {
            if (response.data.type=='success'){
                toastr.success(response.data.text);
            } else {
                $('#modal_objetivo').modal('toggle');
                toastr.warning(response.data.text);
            }  
            getObjetivosAll();
            $scope.objetivo_={};
            $('#modal_objetivo').modal('toggle');
        }, function (response) {
            if (typeof response.data.nmobjetivo !== "undefined") {
                toastr.warning("O campo nome é obrigatório");
            }            
            console.log('Opsss... Algo deu errado na busca dos objetivos!');
        });



    };
    
    
    $scope.editObjetivo = function () {
        $http({
            url: '../prescricao/upObjetivo',
            data: $scope.objetivo_,
            method: "POST"
        }).then(function (response) {
            if (response.data.type=='success'){
                toastr.success(response.data.text);
            } else {
                $('#modal_objetivo_edit').modal('toggle');
                toastr.warning(response.data.text);
            }  
            getObjetivosAll();
            $scope.objetivo_={};
            $('#modal_objetivo_edit').modal('toggle');
        }, function (response) {
            if (typeof response.data.nmobjetivo !== "undefined") {
                toastr.warning("O campo nome é obrigatório");
            }            
            console.log('Opsss... Algo deu errado na busca dos objetivos!');
        });



    };    

    function getObjetivosAll() {
        GrupomuscularService.objetivos('../prescricao/getObjetivos/' + 1).then(function (response) {
            $scope.musculacao = response.data;
        }, function (response) {
            alert('Erro ao buscar Objetivos/Níveis de Habilidade!');
        });
        GrupomuscularService.objetivos('../prescricao/getObjetivos/' + 2).then(function (response) {
            $scope.ginastica = response.data;
        }, function (response) {
            alert('Erro ao buscar Objetivos/Níveis de Habilidade!');
        });
        GrupomuscularService.objetivos('../prescricao/getObjetivos/' + 3).then(function (response) {
            $scope.natacao = response.data;
        }, function (response) {
            alert('Erro ao buscar Objetivos/Níveis de Habilidade!');
        });
        GrupomuscularService.objetivos('../prescricao/getObjetivos/' + 4).then(function (response) {
            $scope.cross = response.data;
        }, function (response) {
            alert('Erro ao buscar Objetivos/Níveis de Habilidade!');
        });
        GrupomuscularService.objetivos('../prescricao/getObjetivos/' + 5).then(function (response) {
            $scope.corrida = response.data;
        }, function (response) {
            alert('Erro ao buscar Objetivos/Níveis de Habilidade!');
        });
        GrupomuscularService.objetivos('../prescricao/getObjetivos/' + 6).then(function (response) {
            $scope.ciclismo = response.data;
        }, function (response) {
            alert('Erro ao buscar Objetivos/Níveis de Habilidade!');
        });

        GrupomuscularService.objetivos('../prescricao/getObjetivos/' + 7).then(function (response) {
            $scope.nutricao = response.data;
        }, function (response) {
            alert('Erro ao buscar Objetivos/Níveis de Habilidade!');
        });



    }

});