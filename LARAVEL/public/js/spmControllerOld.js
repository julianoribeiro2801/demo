app.controller('spmController', function ($http, $scope,spmService, $timeout) {
    // GFMS

    // LISTA PROGRAMAS POR ACADEMIA
    $scope.checado = false;
    $scope.aula_atual = '';
    $scope.data_aula = '';
    $scope.aulasant = [];
    $scope.aulaSelect = {};
    $scope.privado = {};
    $scope.aula = {};
    $scope.horario = {};

    $scope.qt_alunos = [];
    $scope.qt_vagas = [];
    $scope.capacidade = [];
    $scope.dt_aula = [];
    

    $scope.programas = [];
    $scope.listareservas = [];
    $scope.listareservasdados = [];
    $scope.matriculas = [];

    $http({
        url: 'api/gfm/getProgramas',
        method: "POST"
    }).then(function (response) {
        $scope.programas = response.data;
    }, function (response) {
        console.log('Opsss... Algo deu errado ao buscar programas!');
    });
    $scope.listaProgramas = function () {
        $scope.programas = [];
        $http({
            url: 'api/gfm/getProgramas',
            method: "POST"
        }).then(function (response) {
            $scope.programas = response.data;
        }, function (response) {
            console.log('Opsss... Algo deu errado ao buscar programas!');
        });
    };

    // LISTA LOCAIS POR ACADEMIA
    $scope.locais = [];
    $http({
        url: 'api/gfm/getLocais',
        method: "POST"
    }).then(function (response) {
        $scope.locais = response.data;
    }, function (response) {
        console.log('Opsss... Algo deu errado ao buscar locais!');
    });

    getClients();


    // LISTA TODOS OS CLIENTES

    function getClientsss() {
        $scope.clients = [];
        $http({
            url: '../admin/api/prescricao/getClients',
            method: "POST"
        }).then(function (response) {
            $scope.clients = response.data;
        });
    }
    ;
    /*$scope.getClients = function($user) {
        spmService.getClients().then(function(response) {
            $scope.clients = response.data;
            console.log(response.data)
             // ativa o auto completar
            setTimeout(function(){  
                $(".chosen-select").chosen({width: "100%"});
             }, 500);
        }, function(response) {});
    }*/
    // LISTA PROFESSORES POR ACADEMIA
    $scope.professores = [];
    $scope.professor = [];
    $http({
        url: 'api/gfm/getProfessores',
        method: "POST"
    }).then(function (response) {
        $scope.professores = response.data;
    }, function (response) {
        console.log('Opsss... Algo deu errado ao buscar professores!');
    });

    // LISTA GFM POR ACADEMIA


     $scope.graficoAudiencia = function () {


        var grafico1 = [];
        var grafico2 = [];
       
        $.get('api/gfm/getGfmsGrafico1', function (retorno1) {
            grafico1 = retorno1.reservasArray;
            grafico2 = retorno1.reservasArray1;
            grafico1.unshift('data1');
            grafico2.unshift('data2');
    

            c3.generate({
                bindto: '#graficoAudiencia',
                data: {
                    columns: [
                        grafico1,
                        grafico2
                    ], names: {
                        data1: 'Taxa de Ocupação',
                        data2: 'Capacidade'
                    },
                    types: {
                        data1: 'area-spline',
                        data2: 'area-spline'
                    },
                    colors: {
                        data1: '#23c6c8',
                        data2: '#ccc'
                    },
                    groups: [['data1', 'data2']]
                },
                axis: {
                    x: {
                        type: 'category',
                        categories: ["-6 Sem", "-5 Sem", "-4 Sem", "-3 Sem", "-2 Sem", "-1 Sem", "Semana Atual"]
                    }
                }, point: {r: 4}
            });
        });


     };

     // é bom chamar com esse delay senao as vezes nao renderiza o grafico
   setTimeout(function() {
       $scope.graficoAudiencia();
    }, 2000);


 

    $scope.niveis = [];
    /*
    $http({
        url: 'api/spm/getNiveis',
        method: "POST"
    }).then(function (response) {
        $scope.niveis = response.data;
    }, function (response) {
        console.log('Opsss... Algo deu errado ao buscar niveis!');
    });*/
    
    $scope.changeObjetivo = function (modalidade,id) {
        
        $http({
            url: '../admin/api/prescricao/getNiveis/' + modalidade + '/' + id,
            method: "POST"
        }).then(function (response) {
            $scope.niveis = response.data;
            $timeout(function () {
                // $scope.ativaRamificacao();
            }, 2000);
        }, function (response) {
            console.log('Opsss... Algo deu errado na busca dos niveis!');
        });
    };
    

    $scope.objetivosginastica =  [];
    $http({
        url: '../admin/prescricao/getObjetivos/2',
        method: "GET"
    }).then(function (response) {
        $scope.objetivosginastica = response.data;
    }, function (response) {
        console.log('Opsss... Algo deu errado na busca dos objetivos!');
    });
    
    $scope.objetivosnatacao =  [];
    $http({
        url: '../admin/prescricao/getObjetivos/3',
        method: "GET"
    }).then(function (response) {
        $scope.objetivosnatacao = response.data;
    }, function (response) {
        console.log('Opsss... Algo deu errado na busca dos objetivos!');
    });

    $scope.gfms = [];
    $http({
        url: 'api/gfm/getGfms/Spm',
        method: "POST"
    }).then(function (response) {
        $scope.gfms = response.data;

        $scope.duracaoTotal = 0;
        $scope.valorTotal = 0;
        $scope.capacidadeTotal = 0;
        $scope.atingidoTotal = 0;
        $scope.mediaTotal = 0;
        var tamanho = response.data.length;
        for (var i = 0; i < tamanho; i++) {
            $scope.duracaoTotal = parseInt($scope.duracaoTotal) + parseInt(response.data[i].duracao);
            $scope.valorTotal = parseInt($scope.valorTotal) + parseInt(response.data[i].valor);
            $scope.capacidadeTotal = parseInt($scope.capacidadeTotal) + parseInt(response.data[i].capacidade);
            $scope.atingidoTotal = parseFloat($scope.atingidoTotal) + parseFloat(response.data[i].atingido);
            $scope.mediaTotal = parseFloat($scope.mediaTotal) + parseFloat(response.data[i].media);
        }
        $scope.mediaTotal = Math.round($scope.mediaTotal / tamanho);
        $scope.atingidoTotal = Math.round($scope.atingidoTotal / tamanho);
    }, function (response) {
        console.log('Opsss... Algo deu errado ao buscar gfms!');
    });

    $scope.getListareserva = function ($id, $dtaula, $privado) {


        if ($privado == 'Não') {
            $http({
                url: '../api/getListareserva/' + $id,
                method: "GET"
            }).then(function (response) {
                $scope.listareservas = response.data.reservas;
                $scope.listareservasdados = response.data.reservasdados;
                $scope.qt_alunos = response.data.reservasdados[0].capacidade - response.data.reservasdados[0].n_reservas;
                $scope.qt_vagas = response.data.reservasdados[0].capacidade - $scope.qt_alunos;
//                $scope.qt_vagas = response.data.reservasdados[0].n_reservas;
                $scope.capacidade = response.data.reservasdados[0].capacidade;                
                $scope.dt_aula=response.data.reservasdados[0].dtaula;
                
            });
        } else {
            $http({
                url: '../api/getListamatricula/' + $id + '/' + $dtaula,
                method: "GET"
            }).then(function (response) {
                $scope.matriculas = response.data.matriculas;
            });
        }
    };

    $scope.getListamatricula = function ($id, $dataaula) {

        $http({
            url: '../api/getListamatricula/' + $id ,
            method: "GET"
        }).then(function (response) {
            $scope.matriculas = response.data.matriculas;
            $scope.vagas = response.data.gfm.capacidade;
            $scope.sobra = response.data.gfm.capacidade - response.data.matriculados;
            
        });
    };

    $scope.getAula = function (id) {
        $http({
            url: '../api/getAula/' + id + '/0/Ter',
            method: "GET"
        }).then(function (response) {
            $scope.qt_vagas = response.data.n_reservas;
            $scope.dt_aula = response.data.dtaula;
            $scope.capacidade = response.data.capacidade;
            $scope.matriculados = response.data.matriculados;
            $scope.vacancia = $scope.capacidade - $scope.matriculados;
            

        }, function (response) {
            console.log('Opsss... Algo deu errado!');
        });

    };

    $scope.checa = function (indice, aluno, aula) {


        if ($scope.matriculas[indice].presente === 0) {
            $scope.matriculas[indice].presente = 1;
            $scope.presencaAdd(aula, aluno);
        } else {
            $scope.matriculas[indice].presente = 0;
            $scope.presencaDel(aula, aluno);

        }



    };
    
    
    // LISTA TODOS OS CLIENTES
    // com auto completar dddd
    $scope.getClients = function($user) {
        spmService.getClients().then(function(response) {
            $scope.clients = response.data;
            console.log(response.data)
             // ativa o auto completar
            setTimeout(function(){  
                $(".chosen-select").chosen({width: "100%"});
             }, 500);
        }, function(response) {});
    }
    
    $scope.getAulasPassadas = function (id, professor, aula, horario, privado, dataaula) {
        
        $scope.professor = professor;
        $scope.aula = aula;
        $scope.horario = horario;
        $scope.aulaSelect = id;
        $scope.privado = privado;



        getClients();
           
        $scope.getListamatricula(id, '2018-01-01');
        $scope.getListaespera(id);
        
        if (privado == 'Não') {
           $("#addCliente").modal('toggle');
            $scope.getListareserva(id,'',privado);
           // $scope.getClients();
        } else {
            $("#addClientePrivate").modal('toggle');
            //$scope.getListamatricula(id, $scope.aulasant[$scope.aulasant.length - 1].dtaula);
            // $scope.getListamatricula(id, $scope.aulasant[$scope.aulasant.length - 1].dtaula);
        }        


    };

    $scope.addAnonimos = function (id, numero) {

        $http({
            url: 'api/gfm/addAnonimos/' + id + '/' + numero,
            method: "POST"
        }).then(function (response) {
            $scope.listareservas = response.data.gfm;
        });

    };

    $scope.reservaAdd = function (aula, aluno) {
        
        //var data = $("#dataR").val();
        /*var teste = new Date();
        var dia = teste.getDay();
        var semana = new Array(6);
        semana[0]='Domingo';
        semana[1]='Segunda-Feira';
        semana[2]='Terça-Feira';
        semana[3]='Quarta-Feira';
        semana[4]='Quinta-Feira';
        semana[5]='Sexta-Feira';
        semana[6]='Sábado';
        //alert(semana[dia]);        */

        $http({
            url: '../api/reservaAdd/' + aula + '/' + aluno + '/0',
            method: "GET"
        }).then(function (response) {
            if (response.data.cod !== '0') {
                swal("Opsss!", response.data.text + "!", "error");
            } else {
                swal("Ok!", response.data.text, "success");
            }
            $scope.getListareserva(aula,'','Não');
            //$scope.getAula(aula);
            // $scope.listareservas = response.data.gfm;
        });


    };

    $scope.presencaAdd = function (aula, aluno) {

        $http({
            url: '../api/reservaAdd/' + aula + '/' + aluno,
            method: "GET"
        }).then(function (response) {
            if (response.data.cod !== '0') {
                swal("Opsss!", response.data.text + "!", "error");
            } //else{
            //swal("Ok!",response.data.text, "success");  
            //}            
            //$scope.getListamatriucla(aula);
            $scope.getAula(aula);
            // $scope.listareservas = response.data.gfm;
        });


    };

    $scope.matriculaAdd = function (aula, aluno) {

        $http({
            url: '../api/matriculaAdd/' + aula + '/' + aluno,
            method: "GET"
        }).then(function (response) {
            if (response.data.cod !== '0') {
                swal("Opsss!", response.data.text + "!", "error");
            } else {
                swal("Ok!", response.data.text, "success");
            }
            $scope.getListareserva(aula);
            $scope.getAula(aula);
            // $scope.listareservas = response.data.gfm;
        });


    };

    $scope.listaGfms = function () {
        $http({
            url: 'api/gfm/getGfms/Spm',
            method: "POST"
        }).then(function (response) {
            $scope.gfms = response.data;

            $scope.duracaoTotal = 0;
            $scope.valorTotal = 0;
            $scope.capacidadeTotal = 0;
            $scope.atingidoTotal = 0;
            $scope.mediaTotal = 0;
            var tamanho = response.data.length;
            for (var i = 0; i < tamanho; i++) {
                $scope.duracaoTotal = parseInt($scope.duracaoTotal) + parseInt(response.data[i].duracao);
                $scope.valorTotal = parseInt($scope.valorTotal) + parseInt(response.data[i].valor);
                $scope.capacidadeTotal = parseInt($scope.capacidadeTotal) + parseInt(response.data[i].capacidade);
                $scope.atingidoTotal = parseFloat($scope.atingidoTotal) + parseFloat(response.data[i].atingido);
                $scope.mediaTotal = Math.round(parseFloat($scope.mediaTotal) + parseFloat(response.data[i].media));
            }
            $scope.mediaTotal = Math.round($scope.mediaTotal / tamanho);
            $scope.atingidoTotal = Math.round($scope.atingidoTotal / tamanho);
        }, function (response) {
            console.log('Opsss... Algo deu errado ao buscar gfms!');
        });
    };



    $scope.taxaOcupacao = [];
    $scope.taxaOcupacaoProfs = []

    $scope.getTaxaOcupacao = function () {
         $http({
            url: '/admin/api/gfm/getTaxaOcupacao',
            method: "POST"
        }).then(function (response) {
            $scope.taxaOcupacao = response.data;
            
            $scope.percOcupacao=(parseFloat($scope.taxaOcupacao.totalReservas) * parseFloat(100)) / parseFloat($scope.taxaOcupacao.totalCapacidade);
        }, function (response) {
            console.log('Opsss... Algo deu errado ao buscar gfms!');
        });
    };

    $scope.getTaxaOcupacaoProfs = function(){

        $http({
            url: '/admin/api/gfm/getTaxaOcupacaoProfs',
            method: "POST"
        }).then(function (response) {
            
            $scope.ocupacaoProfs=response.data;
            
            for (var i = 0; i < $scope.ocupacaoProfs.length; i++) {
                $scope.ocupacaoProfs[i].id=  response.data[i].id ;
                $scope.ocupacaoProfs[i].perc =  parseFloat($scope.ocupacaoProfs[i].perc) ;
            }
            
        }, function (response) {
            console.log('Opsss... Algo deu errado ao buscar gfms!');
        });

    }
    
    // chamar getTaxaOcupacao and  getTaxaOcupacaoProfs
    $scope.getTaxaOcupacao();
    $scope.getTaxaOcupacaoProfs();


   

    $scope.addGfm = function () {
        $scope.statusBotoesGfm(true);
        $scope.tipo='Spm';
        $http({
            url: 'api/gfm/addGfm',
            data: {
                    tipo: $scope.tipo
            },
            method: "POST"
        }).then(function (response) {
            $scope.gfm = response.data;
            $scope.listaGfms();
            $scope.editingData[$scope.gfm.id] = true;
            // ATIVA OS BOTOES
            $scope.statusBotoesGfm(false);
        }, function (response) {
            console.log('Opsss... Algo deu errado!');
        });
    };
    $scope.addReserva = function (idaula, id) {
        if (id !== undefined) {
            $scope.statusBotoesGfm(true);
            $http({
                url: '../api/reservaAdd/' + idaula + '/' + id,
                method: "GET"
            }).then(function (response) {
                if (response.data.cod == '-1') {
                    swal("Opsss!", response.data.text + "!", "error");
                }
                $scope.getListareserva(idaula);
                getClients();

            }, function (response) {
                console.log('Opsss... Algo deu errado!');
            });
        } else {
            swal("!", "Selecione um gfm para deletar!", "error");
        }
    };



    $scope.reservaDel = function (idaula, id) {
        swal({
            title: "Tem certeza?",
            text: "Deseja realmente cancelar a reserva?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Sim, tenho certeza!",
            cancelButtonText: "Cancelar",
            closeOnConfirm: true
        }, function () {
            $scope.statusBotoesGfm(true);
            $http({
                url: '../api/reservaDel/' + idaula + '/' + id,
                method: "GET"
            }).then(function (response) {
                
                $scope.getListareserva(idaula,'','Não');
                //$scope.getAula(idaula);
            }, function (response) {
                console.log('Opsss... Algo deu errado!');
            });
        });
    };

    $scope.presencaDel = function (idaula, id) {

        $scope.statusBotoesGfm(true);
        $http({
            url: '../api/reservaDel/' + idaula + '/' + id,
            method: "GET"
        }).then(function (response) {
            //$scope.getListareserva(idaula);
            $scope.getListamatricula(idaula, $scope.data_aula);
            $scope.getAula(idaula);
        }, function (response) {
            console.log('Opsss... Algo deu errado!');
        });
    };

    $scope.matriculaDel = function (idaula, id) {
        swal({
            title: "Tem certeza?",
            text: "Deseja realmente cancelar a matrícula?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Sim, tenho certeza!",
            cancelButtonText: "Cancelar",
            closeOnConfirm: true
        }, function () {
            $scope.statusBotoesGfm(true);
            $http({
                url: '../api/matriculaDel/' + idaula + '/' + id,
                method: "GET"
            }).then(function (response) {
                $scope.getListamatricula(idaula, $scope.data_aula);
                $scope.getAula(idaula);
            }, function (response) {
                console.log('Opsss... Algo deu errado!');
            });
        });
    };

    $scope.editingData = {};

    $scope.alterGfm = function (gfm) {
        $scope.editingData[gfm.id] = true;
    };

    $scope.upGfm = function (gfm,tipo) {
        
        
        //alert('' + gfm.niveisobj.length);
        $scope.nivs=[];
        for (var i = 0; i < gfm.niveisobj.length; i++) {
            if (gfm.niveisobj[i].marcado==true){
                $scope.nivs.push( gfm.niveisobj[i]);
            }    
        }
        
        

        if (gfm.capacidademin <= gfm.capacidade) {


            $scope.statusBotoesGfm(true);
            $http({
                url: 'api/gfm/upGfm',
                data: {
                    id: gfm.id,
                    idlocal: gfm.idlocal,
                    idprograma: gfm.idprograma,
                    nmprograma: gfm.nmprograma,
                    idfuncionario: gfm.idfuncionario,
                    objetivo: gfm.objetivo,
                    nivel: gfm.nivel,
                    privado: gfm.privado,
                    gfmspm: tipo,
                    hora_inicio: gfm.hora_inicio,
                    duracao: gfm.duracao,
                    diasemana: 0,
                    domingo: gfm.domingo,
                    segunda: gfm.segunda,
                    terca: gfm.terca,
                    quarta: gfm.quarta,
                    quinta: gfm.quinta,
                    sexta: gfm.sexta,
                    sabado: gfm.sabado,
                    valor: gfm.valor,
                    capacidademin: gfm.capacidademin,
                    capacidade: gfm.capacidade,
                    fator: gfm.fator,
                    niveis: $scope.nivs
                },
                method: "POST"
            }).then(function (response) {
                $scope.editingData[gfm.id] = false;
                $scope.getTaxaOcupacao();
                $scope.listaGfms();
                // ATIVA OS BOTOES
                $scope.statusBotoesGfm(false);
            }, function (response) {
                console.log('Opsss... Algo deu errado!');
            });
        } else {
            swal("Opsss!", "Capacidade máxima deve ser maior que a capacidade mínima" + "!", "error");

        }
    };

    $scope.delGfm = function (id) {
        if (id !== undefined) {
            swal({
                title: "Tem certeza?",
                text: "Tem certeza que deseja deletar esse gfm?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Sim, tenho certeza!",
                cancelButtonText: "Cancelar",
                closeOnConfirm: false
            }, function () {
                $http({
                    url: 'api/gfm/delGfm/' + id,
                    method: "POST"
                }).then(function (response) {
                    $scope.listaGfms();
                    swal("Deletado!", "Gfm deletado com sucesso!", "success");
                });
            });
        } else {
            swal("Opsss!", "Selecione um gfm para deletar!", "error");
        }
    };
    $scope.adicionaAula = function (nmprograma) {
        $scope.statusBotoesGfm(true);
        $http({
            url: 'api/spm/addAula',
            data: {
                nmprograma: nmprograma
            },
            method: "POST"
        }).then(function (response) {
            $('#addAula').modal('hide');
            $scope.listaProgramas();
            // ATIVA OS BOTOES
            $scope.statusBotoesSpm(false);
        }, function (response) {
            console.log('Opsss... Algo deu errado!');
        });
    };



    $scope.addAlunoAula = function (indice) {

        $scope.inserted = {
            id: 0,
            name: 'ANÔNIMO'

        };

        $scope.listareservas.push($scope.inserted);
    };


    $scope.addAlunoMatricula = function (indice) {

        $scope.inserted = {
            idaluno: 0,
            name: 'ANÔNIMO'

        };

        $scope.matriculas.push($scope.inserted);
    };
    $scope.statusBotoesGfm = function (sts) {
        $scope.addGfmBtn = sts;
        $scope.delGfmBtn = sts;
        $scope.editGfmBtn = sts;
        $scope.saveGfmBtn = sts;
    };

    /* $scope.diaSemana = function (dia) {
     switch (dia) {
     case '1':
     return 'Domingo';
     break;
     case '2':
     return 'Segunda';
     break;
     case '3':
     return 'Terça';
     break;
     case '4':
     return 'Quarta';
     break;
     case '5':
     return 'Quinta';
     break;
     case '6':
     return 'Sexta';
     break;
     case '7':
     return 'Sábado';
     break;
     }
     };*/

    $scope.diaSemana = function (dias) {
        var tamanho = dias.length;
        var diassemana = '';
        for (var i = 0; i < tamanho; i++) {
            switch (dias[i]) {
                case '1':
                    dias[i] = 'D, ';
                    break;
                case '2':
                    dias[i] = '2ª, ';
                    break;
                case '3':
                    dias[i] = '3ª, ';
                    break;
                case '4':
                    dias[i] = '4ª, ';
                    break;
                case '5':
                    dias[i] = '5ª, ';
                    break;
                case '6':
                    dias[i] = '6ª, ';
                    break;
                case '7':
                    dias[i] = 'S, ';
                    break;
            }
            diassemana += dias[i];
        }
        diassemana = diassemana.substr(0, diassemana.length - 2);
        return diassemana;
    };



});