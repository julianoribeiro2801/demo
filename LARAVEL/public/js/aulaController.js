app.controller('aulaController', function($http, $scope, $timeout) {
       
    $scope.aulas=[];
    $scope.aula={};
    getProgramas();
    
    
    function getProgramas(){
        $http({
            url: '../api/gfm/getProgramas',
            method: "POST"
        }).then(function(response) {
        	$scope.aulas = response.data;
        }, function(response) {
            console.log('Opsss... Algo deu errado ao buscar programas!');
        });
    };    
        
    $scope.adicionaAula = function (id,nmprograma,indice) {
       $scope.aulas[indice].st=0;
      if (id==0){      
        $http({
            url: '../api/spm/addAula',
            data: {
                nmprograma: nmprograma
            },
            method: "POST"
        }).then(function (response) {
            toastr.success('Aula gravada com sucesso!!');
            getProgramas();
            $scope.aula=null;
            $('#addAula').modal('hide');
        }, function (response) {
            console.log('Opsss... Algo deu errado!');
        }); 
      }   else {
        $http({
            url: '../api/spm/upAula',
            data: {
                id: id,
                nmprograma: nmprograma
            },
            method: "POST"
        }).then(function (response) {
            toastr.success('Aula atualizada com sucesso!!');
            getProgramas();
            $scope.aula=null;
            $('#upAula').modal('hide');
        }, function (response) {
            console.log('Opsss... Algo deu errado!');
        });           
      }
    };   
    $scope.selectAula = function (id,indice) {
        $scope.aulas[indice].st =0;
        $http({
            url: '../api/gfm/getAula/' + id,
            method: "GET"
        }).then(function (response) {
            $scope.aula = response.data.aula[0];
        }, function (response) {
            console.log('Opsss... Algo deu errado!');
        });
    };      
    
    $scope.addItemAula = function () {

        $scope.inserted = {
            id: '0',
            nmprograma:'',
            st: 0
        };

        $scope.aulas.push($scope.inserted);
    };         
           
    $scope.delAula = function(id) {

	if(id !== undefined) {
        swal({
    title: "Tem certeza?",
            text: "Tem certeza que deseja deletar essa aula?",
  icon: "warning",
  buttons: true,
  buttons: {
      cancel: {
        text: "Cancelar",
        value: null,
        visible: true,
        className: "",
        closeModal: true,
      },
      confirm: {
        text: "Sim, tenho certeza!",
        value: true,
        visible: true,
        className: "",
        closeModal: true
      }},
  dangerMode: true,

}).then((willDelete) => {
  if (willDelete) {
    // aqui chama o ajax
    
    $http({
                url: '../api/gfm/delAula/' + id,
                method: "POST"
            }).then(function(response) {
                                getProgramas();
                swal("Deletado!", "Aula deletada com sucesso!", "success");
            });
   
  } else {
    // swal("Your imaginary file is safe!");
  }
});
// fim swal
// 
		
        
	} else {
		swal("Opsss!", "Selecione uma aula para deletar!", "error");
	}
    };    
    
}); // fim controler alunoController
