var url_post = "http://personalclubbrasil.com.br/laravel/public/api/";

    app.factory('ClienteService', function ($http, $q, $timeout, $window) {

        var config = {
            api : '/api/cliente'
        };
        


        var services = {
            register : register,
            show: show,
            convert: convert,
            delete: destroy,
            update: update,
            all : all
        };
        
        function all() {
            return $http.get(config.api)
        }

        function register($data){

        }

        function show($id) {

        }

        function convert($id) {

        }

        function destroy($id) {

        }

        function update($id, $data){

        }


        return services;
    })
 
app.controller('clienteController', function($http, $scope, ClienteService) {
   $scope.cliente = {};
   $scope.clients = {};

   $scope.clients = getClientes();

    $scope.consultaEndereco = function(){
       $http.get('https://viacep.com.br/ws/'+$scope.cep+'/json/').then(function(response) {
          console.log(response.data);
            // 
            $scope.cliente.endereco = response.data.logradouro;
            $scope.cliente.bairro = response.data.bairro;
            $scope.cliente.cidade = response.data.localidade;
            $scope.cliente.estado = response.data.uf;
            
          }, function(response) {
            console.log(response);
        });
    };

      function getClientes () {
          ClienteService.all().then(function (response) {
         
              console.log(response);
              return response.data;
          }, function(response){
              alert({$args: response});
              alert({$args: 'Erro ao buscar lista de clientes'});
          });
      }
 });
   
