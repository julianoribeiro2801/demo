app.controller('lojaAppController', function($http, $scope, $timeout) {
       
    $http.defaults.headers.common.user = $('meta[name="user_logued"]').attr('content');
    $http.defaults.headers.common.unidade = $('meta[name="unidade"]').attr('content');
    $http.defaults.headers.common._token = $('meta[name="csrf_token"]').attr('content');       
       
    $scope.aulas=[];
    $scope.statusPersonaliza=0;

    
    
    getModulos();
    
   function getModulos(){
       
   
    $http({
        url: '/admin/api/loja/getModulos',
        data: {
            unidade: $http.defaults.headers.common.unidade
        },
        method: "POST"
    }).then(function (response) {
        
            for (var i = 0; i < response.data.length; i++) {
                if (response.data[i].id_modulo==1){
                    $scope.statusPersonaliza=response.data[i].status;
                }
            }
        

    }, function (response) {
        console.log('Opsss... Algo deu errado!');
    });    
    
   }
    
    
   
    $scope.addModulo = function (modulo_id,st_id) {

        $http({
            url: '/admin/api/loja/addModulo',
            data: {
                unidade: $http.defaults.headers.common.unidade,
                modulo: modulo_id,
                status: st_id
            },
            method: "POST"
        }).then(function (response) {
            getModulos();
              swal("Atualizado!", response.data.text, response.data.type);
              
        }, function (response) {
            console.log('Opsss... Algo deu errado!');
        });
    };      
    
    
}); // fim controler alunoController
