app.controller('programatreinamentoController', function($http, $scope, $timeout, $filter) {

    $scope.data = [];
    $scope.editar = false;
    $scope.agendaprogramatreinamento = {};
    $scope.agendasprogramatreinamento = [];
    $scope.programatreinamento = {};
    $scope.programastreinamento = [];
    $scope.programa = {};
    $scope.programas = [];


     // trazer abas fechadas
    function mostrarNiveis_prog(name, idx) {

        var str = name;
        var res = str.replace('.', '');      
        $(name).toggle();
        $(".bt-" + res).toggleClass("fa-minus fa-plus");
        var novoag =  '.novo_agen'+ idx;
        $(novoag).toggle();
        
    };


    $scope.getProgramas = function() {
        $http({
            url: '../prescricao/getProgramastreinamento',
            method: "GET"
        }).then(function(response) {
            $scope.data = response.data.programas;
            // console.log($scope.data);
        }, function(response) {
            console.log('Opsss... Algo deu errado ao buscar programas!');
        });
    }

    $scope.inicioPrograma = function() {
        $scope.getProgramas();

        $http({
            url: '../prescricao/getAgendasProgramatreinamento/' + 1,
            method: "GET"
        }).then(function(response) {
            //$scope.agendasprogramatreinamento = response.data.agendas;
            // $scope.data = response.data.programas;
        }, function(response) {
            console.log('Opsss... Algo deu errado ao buscar programas!');
        });



    // aqui é q faz todos os subniveis aparecer segundo o que foi clicado
        setTimeout(function(){ 
             $( "li .ddprog" ).click(function() {
             
               var idx = $( this ).attr( "data-index" );
               name = '.nivel_prog'+idx;

               // aqui é q faz todos os subniveis aparecer segundo o que foi clicado
               mostrarNiveis_prog(name, idx);

            });

        }, 2000);

    };

   

    $scope.inicioPrograma();

    $scope.getAgendaProgramastreinamento = function(programa_id) {
        $http({
            url: '../prescricao/getAgendasProgramatreinamento/' + programa_id,
            method: "GET"
        }).then(function(response) {
            $scope.agendasprogramatreinamento = response.data.agendas;
        }, function(response) {
            console.log('Opsss... Algo deu errado ao buscar programas!');
        });
    };

    $scope.getProgramastreinamento = function() {
        $http({
            url: '../prescricao/getProgramastreinamento',
            method: "GET"
        }).then(function(response) {
            //$scope.programastreinamento = response.data.programas;
            $scope.data = response.data.programas;
        }, function(response) {
            console.log('Opsss... Algo deu errado ao buscar programas!');
        });
    };

    $scope.getProgramatreinamento = function(programa_id, programa_nome, indice) {
        $scope.data[indice].st = 0;
        $scope.editar = true;
        /*$scope.programa.id=programa_id;*/
        /*$scope.programa.nmprograma=programa_nome;*/
    };

    $scope.salvar = function() {
        $scope.editar = false;
        $http({
            url: '../prescricao/getProgramastreinamento',
            method: "POST"
        }).then(function(response) {
            $scope.programastreinamento = response.data.programas;
        }, function(response) {
            console.log('Opsss... Algo deu errado ao buscar programas!');
        })
    };

    $scope.addItemAgenda = function(indice) {
        $scope.inserted = {
            id: 'XXX',
            st: -1,
            nrdias: '',
            nmagenda: ''
        };
        $scope.data[indice].agendas.push($scope.inserted);
    };

    $scope.addItemPrograma = function() {
        $scope.editar = true;
        $scope.inserted = {
            id: '0',
            nmprograma: '',
            st: 0
        };
        // unshift insere no inicio do array
        $scope.data.unshift($scope.inserted);
    };

    $scope.removeItemAgenda = function(indice, indice1) {
        $scope.data[indice].agendas.splice(indice1, 1);
    };

    $scope.urlpage = "/admin/prescricao/configuracoes";

    $scope.redirect = function() {
        setTimeout(function() {
            window.location = $scope.urlpage;
        }, 1300);
    }

    $scope.addPrograma = function($programa) {
        $scope.programa = {};
        $scope.programa = $programa;
        $http({
            url: '../prescricao/addProgramatreinamento',
            data: $scope.programa,
            method: "POST"
        }).then(function(response) {
            toastr.success('Programa gravado com sucesso!!');
           
            $scope.inicioPrograma();
            
        }, function(response) {
            if (typeof response.data.nmprograma !== "undefined") {
                toastr.warning("O campo nome é obrigatório");
            }
            //            toastr.warning("Ocorreu um erro ao atualizar os dados");
            console.log('Opsss... Algo deu errado!');
        });
    };

    $scope.upPrograma = function($programa) {
        $scope.editar = false;
        $scope.programa = {};
        $scope.programa = $programa;
        if ($programa.id !== '0') {
            $http({
                url: '../prescricao/upProgramatreinamento',
                data: $scope.programa,
                method: "POST"
            }).then(function(response) {
                toastr.success('Programa atualizado com sucesso!!');
                $scope.inicioPrograma();
                // $scope.getProgramastreinamento();
            }, function(response) {
                if (typeof response.data.nmprograma !== "undefined") {
                    toastr.warning("O campo nome é obrigatório");
                }
                //            toastr.warning("Ocorreu um erro ao atualizar os dados");
                console.log('Opsss... Algo deu errado!');
            });
        } else {
            $http({
                url: '../prescricao/addProgramatreinamento',
                data: $scope.programa,
                method: "POST"
            }).then(function(response) {
                toastr.success('Programa gravado com sucesso!!');
                $scope.inicioPrograma();
                //$scope.getProgramastreinamento();
            }, function(response) {
                if (typeof response.data.nmprograma !== "undefined") {
                    toastr.warning("O campo nome é obrigatório");
                }
                //    toastr.warning("Ocorreu um erro ao atualizar os dados");
                console.log('Opsss... Algo deu errado!');
            });
        }
    };

    $scope.desaTivar = function(indice, indice1) {
        // $scope.data[indice].agendas[indice].st = 2;
        $scope.data[indice].agendas[indice1].st = 2;

        console.log('indice', indice);
        console.log('indice1', indice1);

    }

    $scope.gravarAgenda = function(programa_id, nr_dias, nm_agenda, indice, indice1) {
        $http({
            url: '../prescricao/addAgenda',
            data: {
                nrdias: nr_dias,
                idprogramatreinamento: programa_id,
                nmagenda: nm_agenda
            },
            method: "POST"
        }).then(function(response) {
           
            // $scope.inicioPrograma();
            $scope.desaTivar(indice, indice1);

            
            swal("Gravado!", "Agendamento gravado com sucesso!", "success");
        }, function(response) {
            console.log('Opsss... Algo deu errado ao gravar agenda!');
        });
    };

  

    $scope.upAgenda = function(agenda_id, nr_dias, nm_agenda, indice, indice1) {

        $http({
            url: '../prescricao/upAgenda',
            data: {
                nrdias: nr_dias,
                agenda_id: agenda_id,
                nmagenda: nm_agenda
            },
            method: "POST"
        }).then(function(response) {

            // $scope.getProgramas();                     
            swal("Atualizado!", "Agenda atualizada com sucesso!", "success");
            $scope.desaTivar(indice, indice1);

        }, function(response) {
            console.log('Opsss... Algo deu errado ao buscar programas!');
        });
    };




    $scope.excluirAgenda = function(agenda_id, indice, indice1) {
        if (agenda_id !== undefined) {

              swal({
           title: "Tem certeza?",
                text: "Tem certeza que deseja deletar essa agenda?",
          icon: "warning",
          buttons: true,
          buttons: {
              cancel: {
                text: "Cancelar",
                value: null,
                visible: true,
                className: "",
                closeModal: true,
              },
              confirm: {
                text: "Sim, tenho certeza!",
                value: true,
                visible: true,
                className: "",
                closeModal: true
              }},
          dangerMode: true,

        }).then((willDelete) => {
          if (willDelete) {
            // aqui chama o ajax
             if (agenda_id !== 'XXX') {
                    $http({
                        url: '../prescricao/deleteAgenda/' + agenda_id,
                        method: "POST"
                    }).then(function(response) {
                        if (response.data.type == "success") {
                            swal("Deletado!", response.data.text, "success");
                            $scope.data[indice].agendas.splice(indice1, 1);
                        } else {
                            swal("Erro!", response.data.text, "error");
                        }
                    });
                    // $scope.inicioPrograma();
                    //$scope.getProgramas();
                } else {
                    swal("Deletado!", "Agenda deletada com sucesso!!", "success");
                    $scope.data[indice].agendas.splice(indice1, 1);
                    // $scope.inicioPrograma();
                    //$scope.getProgramas();
                }
           
          } else {
            // swal("Your imaginary file is safe!");
          }
        });




        } else {
            swal("Opsss!", "Selecione uma agenda para deletar!", "error");
        }
    };
    $scope.deleteProgramatreinamento = function(programa_id, indice) {
        if (programa_id !== undefined) {


               swal({
          title: "Tem certeza?",
                text: "Tem certeza que deseja deletar esse programa de treinamento?",
          icon: "warning",
          buttons: true,
          buttons: {
              cancel: {
                text: "Cancelar",
                value: null,
                visible: true,
                className: "",
                closeModal: true,
              },
              confirm: {
                text: "Sim, tenho certeza!",
                value: true,
                visible: true,
                className: "",
                closeModal: true
              }},
          dangerMode: true,

        }).then((willDelete) => {
              if (willDelete) {
                // aqui chama o ajax
               //$scope.data.splice(indice,indice);  
                $http({
                    url: '../prescricao/deleteProgramatreinamento/' + programa_id,
                    method: "POST"
                }).then(function(response) {
                    if (response.data.type == "success") {
                        swal("Deletado!", response.data.text, "success");
                        $scope.data.splice(indice, 1);
                    } else {
                        swal("Erro!", response.data.text, "error");
                    }
                }, function (response) {
                 swal("Erro!", 'Este programa possui programas em uso.', "error");
              
                });
              } else {
                // cancelar
              }
            });
        // fim do swal



            $scope.inicioPrograma();
        } else {
            swal("Opsss!", "Selecione um local para deletar!", "error");
        }
    };
});