// ja era


app.factory('AlunoService', function ($http, $q, $timeout, $window) {

            var config = {

                api: '/api/cliente'

            };



            $http.defaults.headers.common.user = $('meta[name="user_logued"]').attr('content');

            $http.defaults.headers.common.unidade = $('meta[name="unidade"]').attr('content');

            $http.defaults.headers.common._token = $('meta[name="csrf_token"]').attr('content');



            var services = {

                register: register,

                show: show,

                convert: convert,

                update: update,

                deleteCli: deleteCli,

                all: all,

                search: search,

                paginate: paginate,

                crm: crm,

                getRefreshClientPassword: getRefreshClientPassword,

                getCore: getCore,

                addObjective: addObjective,

                addContact: addContact

            };



            function all() {

                return $http.get(config.api)

            }



            function paginate(url) {

                console.log(url);

                return $http.get(url)

            }



            function register($data) {

                return $http.post(config.api, $data);

            }



            function show($id) {

                return $http.get(config.api + '/client/' + $id);

            }



            function search($query) {

                return $http.get(config.api + '/search/' + $query);

            }



            function convert($id) {

                return $http.patch(config.api + '/' + $id);

            }

            

            function deleteCli($id) {

                return $http.patch(config.api + '/' + $id);

            }



            function update($id, $data) {

                return $http.put(config.api + '/' + $id, $data);

            }



            function getCore() {

                return $http.get('/api/core');

            }



            function getRefreshClientPassword($id) {

                return $http.put(config.api + '/refresh/password/' + $id);

            }



            function crm($cliente) {

                return $http.get(config.api + '/crm/' + $cliente);

            }



            function addObjective($objective) {

                return $http.post('/api/objectives', {nmobjetivo: $objective});

            }



            function addContact(data) {

                return $http.post('/api/contact', data);

            }



            return services;

        })



app.controller('alunoController1', function ($http, $scope, AlunoService, $timeout, $rootScope, Upload) {



    $scope.crm = [];

    $scope.editing = false;

    $scope.showData = false;

    $scope.person = {};

    $scope.persons = [];



    $scope.query;

    $scope.searching = false

    $scope.errors = {};

    $scope.contact = {};

    $scope.page = 1;

    var keys;

    var key;





    $scope.toDate = function (date) {

        return new Date(date);

    };



    $scope.edit = function ($client) {



        $scope.editing = $client.id;

        // abre e fecha dados

        if ($scope.showData == false) {

            $scope.showData = true;

        } else {

            $scope.showData = false;

        }

        ;

    };

    $scope.select = function ($client) {

        AlunoService.show($client.id).then(function (response) {

            $scope.client = {};

            $scope.client = response.data;

            //$scope.getCrm($client.id);



            $timeout(function () {

                $scope.$apply();

            }, 600);

        }, function (response) {

            alert('Erro ao buscar informações do cliente');

        });

    };



    $scope.excluirCliente = function ($client) {



   // $scope.client = {};

        if ($client.id !== undefined) {

            swal({

                title: "Tem certeza?",

                text: 'Deseja realmente excluir esse cliente',

                type: "warning",

                showCancelButton: true,

                confirmButtonColor: "#DD6B55",

                confirmButtonText: "Sim, tenho certeza!",

                cancelButtonText: "Cancelar",

                closeOnConfirm: false

            }, function () {

                $http({

                    url: '../../api/excluirAluno/' + $client.id,

                    method: "GET"

                }).then(function (response) {



                    swal("Excluido!", "Cliente excluido com sucesso!", "success");



                });

            });

        } else {

            swal("Deletar!", "Selecione um aluno para excluir!", "error");

        }    



    };

    $scope.selectTeste = function ($client) {

        alert('' + $client.id);

        AlunoService.show($client.id).then(function (response) {

            $scope.client = {};

            $scope.client = response.data;



            $timeout(function () {

                $scope.$apply();

            }, 600);

        }, function (response) {

            alert('Erro ao buscar informações do cliente');

        });

    };

    $scope.updateAvatar = function ($files, $file) {

        if (!$file) {

            return false;

        }

        Upload.upload({

            url: '/api/upload',

            data: {file: $file, client: $scope.client.id},

            method: 'POST'

        })

                .then(function (resp) {

                    toastr.success('Avatar atualizado com sucesso!')

                }, function (resp) {

                    console.error(resp)

                }, function (evt) {

                    console.log('progress: ' + parseInt(100.0 * evt.loaded / evt.total) + '% file :' + evt.config.data.file.name);

                })

                .catch(function (err) {

                    console.log(err)

                });

    };



    $scope.refreshClientPassword = function ($id) {

        AlunoService.getRefreshClientPassword($id)

                .then(function () {

                    toastr.success('Uma nova senha foi gerada e enviada ao email de cadastro');

                }, function () {

                    toastr.warning('Ocorreu um erro ao gerar nova senha');

                });

    };



    $scope.addObjective = function () {

        swal({

            title: "Adicionar objetivo",

            type: "input",

            showCancelButton: true,

            closeOnConfirm: false,

            animation: "slidefromtop",

            inputPlaceholder: "Objetivo"

        },

        function (inputValue) {

            if (inputValue === false)

                return false;



            if (inputValue === "") {

                swal.showInputError("Informe um objetivo");

                return false

            }

            AlunoService.addObjective(inputValue)

                    .then(function (response) {

                        swal("Ok!", "Objetivo adicionado", "success");

                        init();

                    })

                    .catch(function (erro) {



                    })

        });

    }



    $scope.addContact = function () {

        $scope.contact.iduser = $scope.client.id;

        var modal = $('#contato_modal');



        AlunoService.addContact($scope.contact)

                .then(function (response) {

                    modal.modal('hide')

                    alert(response.data.message);

                    $scope.contact = {};

                    $scope.getCrm($scope.client.id);

                })

                .catch(function (err) {



                });

    }



    $scope.search = function () {

        $scope.searching = true;

        AlunoService.search($scope.query).then(searchSuccess).catch(errorOnSearch)



        function searchSuccess(response) {

            $scope.persons = response.data.persons

            $scope.totalRegistros = response.data.persons

            $scope.searching = false;



        }



        function errorOnSearch(response) {

            console.log(response)

            $scope.searching = false;

        }

    }



    $scope.getCrm = function ($user) {

        AlunoService.crm($user).then(function (response) {

            $scope.crm = response.data;

            console.log(response.data)

            $timeout(function () {

                $scope.$apply();

            });

        }, function (response) {



        });

    }



    $scope.register = function () {

        $scope.loading = true;

        $scope.errors = {};



        AlunoService.register($scope.prospect).then(function (response) {

            $scope.loading = false;

            alert('Cadastro realizado com sucesso');

            $scope.editing = false;

            $('#prospect_modal').modal('hide');

            window.location.reload()

        })

                .catch(function (response) {

                    $scope.loading = false;

                    $scope.errors = response.data;



                    swal({

                        title: 'Erro',

                        text: '' + formatErrorMsg(response.data),

                        html: true

                    })

                })

    }



    function formatErrorMsg(data) {

        var html = '';

        angular.forEach(data, function (item, index) {

            html += '<div>' + item + '</div>'

        })

        return html;

    }



    $scope.update = function () {





        $scope.errors = []

        toastr.success('Atualizando dados ....')

        AlunoService.update($scope.client.id, $scope.client)

                .then(function (response) {

                    _refreshPersonsScope($scope.client.id, response.data)

                    $scope.client = response.data



                    toastr.success('Cliente atualizado com sucesso!!');

                }, function (response) {



                    $scope.errors = response.data;

                    //toastr.warning(formatErrorMsg(response.data));

                    swal({

                        title: 'Erro',

                        text: '' + formatErrorMsg(response.data),

                        html: true

                    })

                    //toastr.warning('Erro ao atualizar dados do cliente');

                    $scope.errors = response.data;

                })

        $scope.editing = false;

    };



    $scope.convert = function (client) {



        swal({

            title: "Atenção!",

            text: getMessage(client),

            type: "warning",

            showCancelButton: true,

            closeOnConfirm: false,

            confirmButtonText: 'SIM',

            cancelButtonText: 'NÃO',

            showLoaderOnConfirm: true

        },

        function () {



            $scope.update();

            $timeout(function () {

                AlunoService.convert(client.id)

                        .then(onCorvertionSuccess)

                        .catch(onConvertionError)

            },

                    3000);





        });



        function onCorvertionSuccess(response) {

            swal('Sucesso!', 'prospect convertido para cliente', 'success');

            $scope.client.role = "cliente";

            _refreshPersonsScope(client.id, response.data)

            $timeout(function () {

                $scope.$apply()

            }, 600)

        }



        function onConvertionError(response) {



            console.log(response)

            swal('Erro!', response.data || 'Ocorreu um erro')

        }



        function getMessage(client) {

            switch (client.role) {

                case 'cliente':

                {

                    return 'Você tem certeza que deseja reverter este cliente para uma oportunidade?'

                    break

                }

                case 'prospect':

                {

                    return 'Você tem certeza que deseja converter para cliente?'

                    break;

                }

                default:

                {

                    return 'Confirmar esta ação?'

                }

            }

        }

    }



    function _refreshPersonsScope($id, $data) {

        angular.forEach($scope.persons.data, function (item, i) {

            if (item.id == $id) {

                $scope.persons.data[i] = $data

            }

        });

    }



    $scope.consultaEndereco = function () {

        $http.get('https://viacep.com.br/ws/' + $scope.client.dados.cep + '/json/').then(function (response) {

            console.log(response.data);

            //

            $scope.client.dados.endereco = response.data.logradouro;

            $scope.client.dados.bairro = response.data.bairro;

            $scope.client.dados.cidade = response.data.localidade;

            $scope.client.dados.estado = response.data.uf;

        }, function (response) {



        });

    };



    $scope.openForm = function () {

        $scope.editing = $scope.editing ? false : true;

    };

    $scope.closeProfileInfo = function () {

        $scope.showData = false;

    };









    $('a[datatoggle="tab"]').on('shown.bs.tab', function (e) {

        $scope.$apply(function () {

            $scope.type = $(e.target).data().type;

        }, 600);

    })





    $('#prospect_modal').on('show.bs.modal', function (e) {

        $('#formprospect')[0].reset()

    })





}); // fim controler alunoController



app.controller('alunoEditarController', function ($http, $scope) {

    $scope.cliente = {};

    $scope.cliente.bairro = $("#bairro").val();

    $scope.cliente.cidade = $("#cidade").val();

}); // fim controler alunoEditarController

