//var url_post = "http://personalclubbrasil.com.br/laravel/public/api/";

app.factory('ProjetoService', function ($http, $q, $timeout, $window) {

    var config = {
        // api : 'http://personalclubbrasil.com.br/laravel/public/api/cliente'
        api : '/api/projeto'
    }
    $http.defaults.headers.common.user = $('meta[name="user_id"]').attr('content') || 1;
    $http.defaults.headers.common.unidade = 1;
    $http.defaults.headers.common._token = $('meta[name="csrf_token"]').attr('content');

    var services = {
        register : register,
        show: show,
        convert: convert,
        delete: destroy,
        update: update,
        all : all
    }
    function all() {
        return $http.get(config.api)
    }

    function register($data){
        return $http.post(config.api, $data);
    }

    function show($id) {
        return $http.get(config.api + '/projeto/'+$id);
    }

    function convert($id) {
        return $http.patch(config.api + '/' + $id);
    }

    function destroy($id) {

    }

    function update($id, $data){
        
        return $http.put(config.api + '/' + $id, $data);
    }

    function crm($cliente = null) {
        return  $http.get(config.api + '/crm/'+$cliente);
     }
    return services;
})

app.controller('projetoController', function($http, $scope, ProjetoService, $timeout) {
    $scope.loading = false;
    $scope.unidade = 1;
    $scope.user = 1;
    $scope.client = {};
    $scope.client.dados = {};
    $scope.clients = [];
    $scope.crm = [];
    $scope.editing = false;
    $scope.showData = false;
    $scope.errors = {};


    $scope.projetos = {};

    $scope.projeto = {};
    $scope.projeto.dados = {};

    $scope.toDate = function (date) {
        return new Date(date);
    }

    getProjetos();
    
    $scope.edit = function ($client) {

        $scope.editing = $client.id;
        // abre e fecha dados
        if($scope.showData ==false) {
             $scope.showData = true;
        } else {
             $scope.showData = false;
               // $scope.closeProfileInfo();

        }

    }
    
    $scope.select = function ($client) {
        $scope.client = {};
       ProjetoService.show($client.id).then(function(response){
           $scope.client = response.data;

           $scope.getCrm($client.id);
           $timeout(function(){
               $scope.$apply();
           })
       }, function(response){
           alert({$args: 'Erro ao buscar informações do projeto'})
       })
    }



    $scope.getCrm = function($user = null){
        ProjetoService.crm($user).then( function(response){
           $scope.crm = response.data;
           console.log(response.data)
           $timeout(function () {
               $scope.$apply();
           });
        }, function (response) {

        });
    }

    $scope.totalRegistros = function () {
        return parseInt($scope.clients.length) + parseInt($scope.prospects.length);
    }

    $scope.register = function () {
        $scope.loading = true;
        $scope.errors = {};

        ProjetoService.register($scope.projeto).then(function (response) {
            //getProjetos();
            $timeout(function(){
                $scope.$apply();
            })

            $scope.loading = false;
            $('#prospect_modal').modal('hide');
        }, function (response) {
            $scope.loading = false;
            $scope.errors = response.data;
            alert({$args: 'Erro ao cadastrar novo projeto'});
        })
    }

    $scope.update = function () {
        ProjetoService.update($scope.client.id, $scope.client).then(function (response) {
           alert({$args: 'Cliente atualizado com sucesso!!'});
           getProjetos();
            $scope.select($scope.projeto)
        }, function (response) {
            alert({$args: 'Ocorreu um erro ao atualizar os dados do cliente'});
        })
    }



    $scope.openForm = function () {
        $scope.editing = $scope.editing ? false : true;
    }
    $scope.closeProfileInfo = function () {
        $scope.showData = false;
    }

    function getProjetos() {

        
        ProjetoService.all().then(function (response) {
          
           
            $scope.projetos = response.data.projetos;
            //alert($scope.projeto.dsmeta) ;

            if (!$scope.projetos[0]){
                if(response.data.projetos[0]){
                    $scope.select(response.data.projetos[0]);
                }
            }
            $timeout(function(){
                $scope.$apply();
            });
        }, function (response) {
            alert({$args: 'Erro ao buscar projeto '});
        });
    }
}); // fim controler alunoController

app.controller('alunoEditarController', function($http, $scope) {
    $scope.cliente = {};
    $scope.cliente.bairro = $("#bairro").val();
    $scope.cliente.cidade = $("#cidade").val();
}); // fim controler alunoEditarController

