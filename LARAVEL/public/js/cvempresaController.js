app.controller('cvempresaController', function($http, $scope, $timeout) {
    $scope.loading = false;
    $scope.cvempresa = {};
    $scope.cvempresas = [];
    $scope.segmentos = [];
    $scope.errors = {};

/*      $scope.checkboxModel = {
       value1 : false
     };*/
     $scope.value1 = false;

     
     
    $scope.toDate = function (date) {
        return new Date(date);
    };

    getCvempresas();
    
	$http({
		url: './api/cvempresa/getSegmentos',
		method: "GET"
	}).then(function(response) {
		$scope.segmentos = response.data.segmentos;
	}, function(response) {
            console.log('Opsss... Algo deu errado!');
	});
    

        function getCvempresas() {
		$http({
			url: './api/cvempresa/getEmpresas',
			method: "GET"
		}).then(function(response) {
			$scope.cvempresas = response.data.empresas;
		}, function(response) {
			console.log('Opsss... Algo deu errado!');
		});
	};

	$scope.editCvempresa= function(id) {
		$http({
			url: './api/cvempresa/getEmpresa/' + id,
			method: "GET"
		}).then(function(response) {
			$scope.cvempresa = response.data.cvempresa;
                        if (response.data.cvempresa.indet=='true'){
                            $scope.value1=true;
                        }    

		}, function(response) {
			console.log('Opsss... Algo deu errado!');
		});
	};

       $scope.vct = function() {
           
         alert('sdfd' + $scope.checkbox1.value1);  
       };

	$scope.deleteCvempresa = function(cvempresa_id) {
		if(cvempresa_id !== undefined) {

			swal({
    title: "Tem certeza?",
				text: "Tem certeza que deseja deletar essa empresa?",
  icon: "warning",
  buttons: true,
  buttons: {
      cancel: {
        text: "Cancelar",
        value: null,
        visible: true,
        className: "",
        closeModal: true,
      },
      confirm: {
        text: "Sim, tenho certeza!",
        value: true,
        visible: true,
        className: "",
        closeModal: true
      }},
  dangerMode: true,

}).then((willDelete) => {
  if (willDelete) {
    // aqui chama o ajax
    $http({
					url: './api/cvempresa/deleteCvempresa/' + cvempresa_id,
					method: "POST"
				}).then(function(response) {
					getCvempresas();
					swal("Deletado!", "Empresa deletada com sucesso!", "success");
				});
   
  } else {
    // swal("Your imaginary file is safe!");
  }
});
// fim swal
		
		} else {
			swal("Opsss!", "Selecione um local para deletar!", "error");
		}
	};
    $scope.totalRegistros = function () {
        return parseInt($scope.clients.length) + parseInt($scope.prospects.length);
    };


    $scope.openForm = function () {
        $scope.editing = $scope.editing ? false : true;
    };
    $scope.closeProfileInfo = function () {
        $scope.showData = false;
    };

}); // fim controler alunoController
