app.controller('painelController', function ($http, $scope, $timeout) {
    $scope.loading = false;
    $scope.cvempresa = {};
    $scope.totalprospects = [];
    $scope.tpperc = [];
    $scope.total = [];
    $scope.totalcli = [];
    $scope.totalclientes = [];
    $scope.cliperc = [];
    $scope.totaldesativados = [];
    $scope.desatperc = [];
    $scope.totallogins = [];
    $scope.listas = [];
    $scope.revisaotreinos = [];
    $scope.agendas = [];
    $scope.agenda = {};
   
    $scope.errors = {};
    $scope.diasemana_select = '';
    $scope.dia_semana = '';
    $scope.edita = false;
    $scope.prof_anterior = '';

    $scope.turmasel = 0;
    $scope.dados1 = [];




    getIndicadorProjeto();
    getRotatividade();
    getTotalProspects();
    getTotalLogins();
    getRevisaoTreino();
    getAgendaPrograma();
    getListas();
    //addSub();
 


    $scope.diasemana = function () {
        var hoje = new Date();
        var dia_semana = hoje.getDay();
        var semana = new Array(6);
        semana[0] = 'Dom';
        semana[1] = 'Seg';
        semana[2] = 'Ter';
        semana[3] = 'Qua';
        semana[4] = 'Qui';
        semana[5] = 'Sex';
        semana[6] = 'Sab';
        return semana[dia_semana];
    }


    $scope.c3JS = function () {



        $http({
            url: '/admin/api/painel/getGraficosC3',
            method: "get"
        }).then(function (response) {
            $scope.dados1 = response.data.dados1Array;
            $scope.dados2 = response.data.dados2Array;
            $scope.dados3 = response.data.dados3Array;
            $scope.dados5 = response.data.dados5Array;//['Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov',
            //'Dez', 'Jan', 'Fev', 'Mar', 'Abr'];


            c3.generate({
                bindto: '#resultados_academia',
                data: {
                    columns: [
                        $scope.dados1,
                        $scope.dados2,
                        $scope.dados3
                                //['data1', datax[0], $scope.dados1[0], -50, -40, -60, -50,-30, -20, -50, -40, -60, -50, - 40],

                    ], names: {
                        data1: 'Desativados',
                        data2: 'Ativados',
                        data3: 'Resultado'
                    },
                    
                
                    type: 'bar',
                    
                    
                    types: {
                        data3: 'spline',
                    }, colors: {
                        data1: '#ed5565',
                        data2: '#1ab394',
                        data3: '#fed862'
                    },
                    groups: [
                        ['data1', 'data2']
                    ]
                },
                grid: {
                    y: {
                        lines: [{value: 0}]
                    }
                },
                axis: {
                    x: {
                        type: 'category',
                        categories: $scope.dados5
                    }
                },
                point: {r: 5}
            });

        }, function (response) {
            console.log('Opsss... Algo deu errado ao buscar professores!');
        });


        $http({
            url: '/admin/api/ListarClientes',
            method: "get"
        }).then(function (response) {
            
         }, function (response) {
            console.log('Opsss... Algo deu errado ao buscar professores!');
        });
        
        
       
        $http({
            url: '/admin/api/painel/getGraficosC32',
            method: "get"
        }).then(function (response) {
            $scope.dados1 = response.data.dados1Array;
            $scope.dadosx={};
            var teste = [];
            for (var i = 0; i < $scope.dados1.length; i++) {            
                teste[i]=$scope.dados1[i];
            }

            c3.generate({
                bindto: '#professores',
                data: {
                    columns: teste
                },
                grid: {
                    y: {
                        lines: [{value: 0}]
                    }
                },
                axis: {
                    x: {
                        type: 'category',
                        categories: $scope.dados5
                    }
                },
                point: {r: 3}
            });

        }, function (response) {
            console.log('Opsss... Algo deu errado ao buscar professores!');
        });

    }

    $scope.c3JS();

    // LISTA PROFESSORES POR ACADEMIA
    $scope.professores = [];
    $scope.professor = [];
   
    $scope.profs_indicador = [];
    $scope.profs_rotatividade = [];

    $scope.getProfessores = function () {
        $http({
            url: '/admin/api/painel/getProfessores',
            method: "POST"
        }).then(function (response) {
            $scope.professores = response.data;
        }, function (response) {
            console.log('Opsss... Algo deu errado ao buscar professores!');
        });
    };
    
    function getIndicadorProjeto() {
        $http({
            url: '/admin/api/painel/getIndicadorProjeto',
            method: "get"
        }).then(function (response) {
            $scope.profs_indicador = response.data;
        }, function (response) {
            console.log('Opsss... Algo deu errado ao buscar professores!');
        });
    };    
    
    function getRotatividade() {
        $http({
            url: '/admin/api/painel/getRotatividade',
            method: "get"
        }).then(function (response) {
            $scope.profs_rotatividade = response.data;
        }, function (response) {
            console.log('Opsss... Algo deu errado ao buscar professores!');
        });
    };     

    $scope.getProfessores();
    

   $scope.projetosAtrasados = function (aluno, prof_ant, professor) {
       

        $http({
            url: '/admin/api/painel/projetosAtrasados',
            method: "GET"
        }).then(function (response) {

          $scope.projetos_etapas = response.data;
          console.log( ' $scope.projetos_etapas', $scope.projetos_etapas);

        }, function (response) {
            console.log('Opsss... Algo deu errado!');
        });

    };

     $scope.projetosAtrasados();


    $scope.taxaOcupacao = [];
    $scope.taxaOcupacaoProfs = [];
    $scope.desativados = [];

    $scope.getTaxaOcupacao = function () {
         $http({
            url: '/admin/api/gfm/getTaxaOcupacao',
            method: "POST"
        }).then(function (response) {
            $scope.taxaOcupacao = response.data;
            
            $scope.percOcupacao=(parseFloat($scope.taxaOcupacao.totalReservas) * parseFloat(100)) / parseFloat($scope.taxaOcupacao.totalCapacidade);
        }, function (response) {
            console.log('Opsss... Algo deu errado ao buscar gfms!');
        });
    };


    
         $http({
             url: '/admin/api/painel/getDesativados',
            method: "get"
        }).then(function (response) {
            $scope.desativados = response.data;
        }, function (response) {
            console.log('Opsss... Algo deu errado ao buscar gfms!');
        });
    

    $scope.getTaxaOcupacaoProfs = function(){

        $http({
            url: '/admin/api/gfm/getTaxaOcupacaoProfs',
            method: "POST"
        }).then(function (response) {
            
            $scope.ocupacaoProfs=response.data;
            
            for (var i = 0; i < $scope.ocupacaoProfs.length; i++) {
                $scope.ocupacaoProfs[i].id=  response.data[i].id ;
                $scope.ocupacaoProfs[i].perc =  parseFloat($scope.ocupacaoProfs[i].perc) ;
            }
            
        }, function (response) {
            console.log('Opsss... Algo deu errado ao buscar gfms!');
        });

    }
    
    // chamar getTaxaOcupacao and  getTaxaOcupacaoProfs
    $scope.getTaxaOcupacao();
    $scope.getTaxaOcupacaoProfs();



    $scope.upTurma = function (aluno, prof_ant, professor) {
       

        $http({
            url: '/admin/api/painel/upTurma/' + aluno + '/' + professor,
            method: "GET"
        }).then(function (response) {

            $scope.getProfessores();
            $scope.getTurmasProf(prof_ant);
            getAgendaPrograma();
        }, function (response) {
            console.log('Opsss... Algo deu errado!');
        });

    };

    $scope.getTurmasProf = function (id) {

    
        if ($scope.turmasel == id) {
            $scope.turmasel = -1;
        } else {
            $scope.turmasel = id;
        }


        $scope.prof_anterior = id;

        $http({
            url: '/admin/api/painel/getCarteiraProfessor/' + id,
//            url: '/admin/api/painel/getTurmasProf/' + id,
            method: "GET"
        }).then(function (response) {
            $('.trocar_carteira').fadeIn(120);
            $scope.turmas = response.data.turma;


        }, function (response) {
            console.log('Opsss... Algo deu errado!');
        });

    };



    /*   $scope.getNumeroAlunosProf = function(id){
     
     $scope.prof_anterior = id;
     
     $http({
     url: '/admin/api/painel/getNumeroAlunosProf/' + id,
     method: "GET"
     }).then(function (response) {
     
     $scope.turmas = response.data.turma;
     
     }, function (response) {
     console.log('Opsss... Algo deu errado!');
     });        
     
     };     */

  

    // $scope.getAula = function (id) {

    //     $http({
    //         url: '../api/getAula/' + id + '/0/' + $scope.dia_semana,
    //         method: "GET"
    //     }).then(function (response) {
    //         $scope.qt_vagas = response.data.n_reservas;
    //         $scope.dt_aula = response.data.dtaula;
    //         $scope.capacidade = response.data.capacidade;
    //         $scope.matriculados = response.data.matriculados;
    //         $scope.vacancia = $scope.capacidade - $scope.matriculados;


    //     }, function (response) {
    //         console.log('Opsss... Algo deu errado!');
    //     });

    // };

    function getTurmasMusculacao() {

        $http({
            url: '/admin/api/painel/getTurmas',
            method: "GET"
        }).then(function (response) {
            $scope.totalprospects = response.data.totais.totalprospects;
            $scope.tpperc = response.data.totais.perc;
            $scope.total = response.data.totais.total;
            $scope.totalclientes = response.data.totalclientes.total;
            $scope.totalcli = response.data.totalclientes.totalcli;
            $scope.cliperc = response.data.totalclientes.perc;
            $scope.totaldesativados = response.data.totaldesativados.total;
            $scope.totaldesat = response.data.totaldesativados.total;
            $scope.desatperc = response.data.totaldesativados.perc;
        }, function (response) {
            console.log('Opsss... Algo deu errado!');
        });
    }
    ;


    function getTotalProspects() {

        $http({
            url: '/admin/api/painel/getTotalProspects',
            method: "GET"
        }).then(function (response) {
            $scope.totalprospects = response.data.totais.totalprospects;
            $scope.tpperc = response.data.totais.perc;
            $scope.total = response.data.totais.total;
            $scope.totalclientes = response.data.totalclientes.total;
            $scope.totalcli = response.data.totalclientes.totalcli;
            $scope.cliperc = response.data.totalclientes.perc;
            $scope.totaldesativados = response.data.totaldesativados.total;
            $scope.totaldesat = response.data.totaldesativados.total;
            $scope.desatperc = response.data.totaldesativados.perc;
        }, function (response) {
            console.log('Opsss... Algo deu errado!');
        });
    }
    ;

    function getTotalLogins() {
        $http({
            url: '/admin/api/painel/getTotalLogins',
            method: "GET"
        }).then(function (response) {
            $scope.totallogins = response.data.totalogins[0].total;
        }, function (response) {
            console.log('Opsss... Algo deu errado!');
        });
    }
    ;
    function getListas() {////teste egoi/ criar egoiController
        $http({
            url: '/admin/api/egoi/getListas',
            method: "GET"
        }).then(function (response) {
            $scope.listas = response.data.listas;
        }, function (response) {
            console.log('Opsss... Algo deu errado!');
        });
    }
    ;

    /* function addSub() {////teste egoi/ criar egoiController
     $http({
     url: '/admin/api/egoi/addSubscriber',
     data: {
     email: 'vinicius.pondian_spotify@hotmail.com',
     celular: '45999126082',
     nome: 'JOSE',
     sobrenome: 'RIBEIRO'
     },            
     method: "POST"
     }).then(function (response) {
     $scope.listas = response.data.listas;
     }, function (response) {
     console.log('Opsss... Algo deu errado!');
     });
     };    */
    function getRevisaoTreino() {
        $http({
            url: '/admin/api/painel/getRevisaoTreino',
            method: "GET"
        }).then(function (response) {
            $scope.revisaotreinos = response.data.revisao;
        }, function (response) {
            console.log('Opsss... Algo deu errado!');
        });
    }
   
    function getAgendaPrograma() {
        $http({
            url: '/admin/api/painel/getAgendaPrograma',
            method: "GET"
        }).then(function (response) {
            $scope.agendas = response.data.agendas;
        }, function (response) {
            console.log('Opsss... Algo deu errado!');
        });
    }
  

}); // fim controler alunoController


