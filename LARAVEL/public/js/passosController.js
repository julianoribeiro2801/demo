app.controller('passosController', function($http, $scope, $timeout) {

// alert('passosController');

  // LISTA EMPRESA
    $scope.empresa = {};


    $http({
        url: '/admin/api/unidade/getEmpresa',
        method: "POST"
    }).then(function (response) {
    	console.log(response);
        $scope.empresa = response.data[0];
        $scope.carregaCidade(response.data[0].idestado);
    }, function (response) {
        console.log('Opsss... Algo deu errado!');
    });


// LISTA ESTADOS
    $scope.estados = {};
    $http({
        url: '/admin/api/unidade/getEstados',
        method: "POST"
    }).then(function (response) {
        $scope.estados = response.data;
       /* $timeout(function () {
            $('#estado').addClass('chosen-select');
            $('.chosen-select').chosen({width: "100%"});
        }, 500);*/
    }, function (response) {
        console.log('Opsss... Algo deu errado!');
    });


      $scope.carregaCidade = function (estado) {
        // LISTA CIDADE
        $scope.cidades = {};
        $http({
            url: '/admin/api/unidade/getCidades',
            data: {
                estado: estado
            },
            method: "POST"
        }).then(function (response) {
            $scope.cidades = response.data;
            $timeout(function () {
                $('.cidade').addClass('chosen-select');
                $('.chosen-select').chosen({width: "100%"});
            }, 500);
        }, function (response) {
            console.log('Opsss... Algo deu errado!');
        });
    };


  $scope.changeEstado = function (estado) {
        // LISTA CIDADE
        $scope.cidades = {};
        $http({
            url: '/admin/api/unidade/getCidades',
            data: {
                estado: estado
            },
            method: "POST"
        }).then(function (response) {
            $scope.cidades = response.data;
          /*  $timeout(function () {
                $("#cidade").val('').trigger("chosen:updated");
                $('#cidade').addClass('chosen-select');
                $('.chosen-select').chosen({width: "100%"});
            }, 500);*/
        }, function (response) {
            console.log('Opsss... Algo deu errado!');
        });
    };



    $scope.upEmpresa = function ($empresa) {


        $scope.empresa = {};
        $scope.empresa = $empresa;

        console.log('Opsss... Algo deu errado!', $scope.empresa);

         if (!$scope.empresa.fantasia) {
            swal ( "Oops" ,  "O campo fantasia é obrigatório" ,  "error" )
                return false;
        }
        if (!$scope.empresa.idestado) {
            // swal ( "Oops" ,  "O campo estado é obrigatório" ,  "error" );
            //  Como não vai ser obrigado a preencher so coloquei um valor padrao
            $scope.empresa.idestado = 1;
             $scope.empresa.idcidade = 1;
            // toastr.warning("O campo estado é obrigatório");
            // return false;
        }
        // if (!$scope.empresa.idcidade) {
           

        //       swal ( "Oops" ,  "O campo cidade é obrigatório" ,  "error" )
        //     // toastr.warning("O campo cidade é obrigatório");
        //     return false;
        // }


        if ($('input#acceptTerms').is(':checked')) {

	        $http({
	            url: '/admin/api/unidade/upEmpresa',
	            data: $scope.empresa,
	            method: "POST"
	        }).then(function (response) {
	            // toastr.success('Empresa atualizada com sucesso!');

	            swal('Empresa atualizada com sucesso!', {
				  buttons: false,
				  timer: 1500,
				  icon: "success",
				});

				setTimeout(function(){  window.location.href="/admin/coresApp/Home"; }, 1400);
	          
	            
	        }, function (response) {
	           
	//            toastr.warning("Ocorreu um erro ao atualizar os dados");
	            console.log('Opsss... Algo deu errado!');
	        });
	    } else {
	    	swal ( "Oops" ,  "Aceite os Termos e Condições." ,  "error" )

	    }
    };



});

app.controller('passosParabensController', function($http, $scope, $timeout) {
 	

 	 $http({
	            url: '/admin/api/unidade/upPasso',
	            data: {id:''},
	            method: "POST"
	        }).then(function (response) {
	            // toastr.success('Empresa atualizada com sucesso!');

	   //          swal('Empresa atualizada com sucesso!', {
				//   buttons: false,
				//   timer: 1500,
				//   icon: "success",
				// });

				// setTimeout(function(){  window.location.href="/admin/coresApp/Home"; }, 1400);
	               
	        }, function (response) {
	            
	            console.log('Opsss... Algo deu errado!');
	        });
	   


});