//var url_post = "http://personalclubbrasil.com.br/laravel/public/api/";

app.factory('LocalService', function ($http, $q, $timeout, $window) {

    var config = {
        // api : 'http://personalclubbrasil.com.br/laravel/public/api/local'
        api : '/api/local'
    };
    $http.defaults.headers.common.user = $('meta[name="user_id"]').attr('content') || 1;
    $http.defaults.headers.common.unidade = 1;
    $http.defaults.headers.common._token = $('meta[name="csrf_token"]').attr('content');

    var services = {
        register : register,
        show: show,
        destroy: destroy,
        update: update,
        all : all

    };
    function all() {
        return $http.get(config.api);
    }

    function register($data){
        return $http.post(config.api, $data);
    }

    function show($id) {
        return $http.get(config.api + '/local/'+$id);
    }

    function destroy($id) {
       return $http.delete(config.api+ '/' + $id);
    }
    
    function update($id, $data){
        
        return $http.put(config.api + '/' + $id, $data);
    }



    return services;
});

app.controller('localController', function($http, $scope, LocalService, $timeout) {
    $scope.loading = false;
    $scope.unidade = 1;
    $scope.user = 1;
    $scope.local = {};
    $scope.local.dados = {};
    $scope.locais = [];
    $scope.editing = false;
    $scope.showData = false;
    $scope.errors = {};


    getLocals();
    
    $scope.edit = function ($local) {

        $scope.editing = $local.id;
        // abre e fecha dados
        if($scope.showData===false) {
             $scope.showData = true;
        } else {
             $scope.showData = false;
               // $scope.closeProfileInfo();

        }

    };
    
    $scope.select = function ($local) {
        $scope.local = {};
       LocalService.show($local.id).then(function(response){
           $scope.local = response.data;

           
           $('#locais_edit').modal('show');
           
           $timeout(function(){
               $scope.$apply();
           });
       }, function(response){
           alert({$args: 'Erro ao buscar informações do local'});
       });
    };

    $scope.destroy = function ($local) {
       $scope.local = {};
       LocalService.destroy($local.id).then(function(response){
          
           
           alert({$args: 'Local removido com sucesso'});
           
            getLocals();
           
           $timeout(function(){
               $scope.$apply();
           });
       }, function(response){
           alert({$args: 'Erro ao remover local'});
       });
    };    




    $scope.totalRegistros = function () {
        return parseInt($scope.clients.length) + parseInt($scope.prospects.length);
    };

    $scope.register = function () {
        $scope.loading = true;
        $scope.errors = {};

        LocalService.register($scope.local).then(function (response) {
            //getLocals();
            $timeout(function(){
                $scope.$apply();
            });

            $scope.loading = false;
            $('#local_modal').modal('hide');
        }, function (response) {
            $scope.loading = false;
            $scope.errors = response.data;
            alert({$args: 'Erro ao cadastrar novo local'});
        });
    };

    $scope.update = function () {
        LocalService.update($scope.client.id, $scope.local).then(function (response) {
           alert({$args: 'Cliente atualizado com sucesso!!'});
           getLocals();
            $scope.select($scope.local);
        }, function (response) {
            alert({$args: 'Ocorreu um erro ao atualizar os dados do local'});
        });
    };


    function getLocals() {


        LocalService.all().then(function (response) {
            $scope.locais = response.data.locais;

            $timeout(function(){
                $scope.$apply();
            });
        }, function (response) {
            alert({$args: 'Erro ao buscar locais '});
        });
    }
}); // fim controler alunoController

