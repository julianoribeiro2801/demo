var jsonFica = '';

function carregaCombo(id) {
    $('#agenda_id').val(id);
    $.ajax({
        type: 'POST',
        url: "../admin/api/prescricao/getClients",
        //data: { empresa: $("#empresa").val() },
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        success: function(obj) {
            if (obj != null) {
                var data = obj.data;
                var selectbox = $('#clienteSelect');
                selectbox.find('option').remove();
                for (var i = 0; i < obj.length; i++) {
                    $('<option>').val(obj[i].id).text(obj[i].name).appendTo(selectbox);
                }
                $('#clienteSelect').addClass('chosen-select');
                $('.chosen-select').chosen({
                    width: "100%"
                });
            }
        }
    });
}

function agendaAluno(id, agenda, datagenda) {
    $.post('../admin/api/agendamento/addAgendamento', {
        aluno: id,
        idagenda: agenda,
        dtagenda: datagenda
    }, function(retorno) {
        if (retorno.retorno.cod == 0) {
            swal("Ok!", retorno.retorno.text, "success");
        } else {
            swal("Erro!", retorno.retorno.text, "error");
        }
        //$('#modalAgenda').modal('hide');
        pegaDia(datagenda);
    });
}

function gravaHorario(horaagenda, duracao) {
    var diasemana = $("input[name=diaSelect]").val();
    switch (diasemana) {
        case "Domingo":
            diasemana = 1;
            break;
        case "Segunda":
            diasemana = 2;
            break;
        case "Terça":
            diasemana = 3;
            break;
        case "Quarta":
            diasemana = 4;
            break;
        case "Quinta":
            diasemana = 5;
            break;
        case "Sexta":
            diasemana = 6;
            break;
        case "Sábado":
            diasemana = 7;
            break;
        default:
            diasemana = 1;
    }
    $.post('../admin/api/agendamento/addHorario', {
        dia: diasemana,
        horario: horaagenda,
        duracao: duracao
    }, function(retorno) {
        if (retorno.retorno.cod == 0) {
            //             $('#horario').val=='25:00';
            swal("Ok!", retorno.retorno.text, "success");
        } else {
            swal("Erro!", retorno.retorno.text, "error");
        }
    });
}

function cancela(id) {
    swal({
        title: "Tem certeza?",
        text: "Tem certeza que deseja deletar essa agenda?",
        icon: "warning",
        buttons: true,
        buttons: {
            cancel: {
                text: "Cancelar",
                value: null,
                visible: true,
                className: "",
                closeModal: true,
            },
            confirm: {
                text: "Sim Desejo!",
                value: true,
                visible: true,
                className: "",
                closeModal: true
            }
        },
        dangerMode: true,
    }).then((willDelete) => {
        if (willDelete) {
            // aqui chama o ajax
            canc(id);
        } else {
            // swal("Your imaginary file is safe!");
        }
    });
    // fim swal
}

function canc(id) {
    $.post('../admin/api/agendamento/cancelaAgendamento', {
        id: id
    }, function(retorno) {
        if (retorno.retorno.cod == 0) {
            swal("Ok!", retorno.retorno.text, "success");
        } else {
            swal("Erro!", retorno.retorno.text, "error");
        }
        pegaDia($('#dtagenda').val());
    });
}

function pegaDia(dia) {
    //$( "#dia_comum div" ).parent().css( "background-color", "#f6f5f6" );
    $("td").removeClass("dia_click");
    $('.col-' + dia).addClass('dia_click');
    $('#dtagenda').val(dia);
    $.ajax({
        method: "get",
        dataType: "json",
        url: '../admin/api/agendamento/getAgendamentos/' + dia,
        success: function(data) {
            var divPai = $('#external-events');
            $('#external-events').empty();
            for (var i = 0; i < data.agendamentos.length; i++) {
                if (data.agendamentos[i].name == "") {
                    divPai.append("<div class=\"external-event navy-bg rotulos_h_agendamento\" id=" + data.agendamentos[i].id + "  name=" + data.agendamentos[i].id + " data-toggle=\"modal\" data-target=\"#modalAgendar\"  value=" + data.agendamentos[i].id + " onclick=carregaCombo(" + data.agendamentos[i].id + ")>" + data.agendamentos[i].horario + "</div>");
                } else {
                    divPai.append("<div  class=\"external-event navy-bg rotulos_h_agendado\" id=" + data.agendamentos[i].id + "  name=" + data.agendamentos[i].id + " value=" + data.agendamentos[i].id + " onclick=cancela(" + data.agendamentos[i].idagendamento + ")>" + data.agendamentos[i].horario + ' - ' + data.agendamentos[i].name + "<b><b> <i class=\"fa fa-times-circle\" aria-hidden=\"true\"></div>");
                }
            }
        }
    });
}
app.controller('agendamentoController', function($http, $scope, $timeout) {
    $scope.loading = false;
    $scope.agendamentos = [];
    $scope.clients = [];
    $scope.idagendaSel = {};
    $scope.testevar = 'teste';
    // $scope.teste = jsonFica;
    $scope.testeScope1 = [];
    // LISTA TODOS OS CLIENTES
    /* $scope.getClients = function () {
         $http({
             url: '../admin/api/prescricao/getClients',
             method: "POST"
         }).then(function (response) {
             $scope.clients = response.data;
             $timeout(function () {
                 $('#clienteSelect').addClass('chosen-select');
                 $('.chosen-select').chosen({width: "100%"});
             }, 500);
                 
         }, function (response) {
             $http({
                 url: '../admin/api/prescricao/getClients',
                 method: "POST"
             }).then(function (response) {
                 $scope.clients = response.data;
                 $timeout(function () {
                     $('#clienteSelect').addClass('chosen-select');
                     // $('.chosen-select').chosen({width: "100%"});
                 }, 500);
             }, function (response) {
                 console.log('Opsss... Algo deu errado na busca dos clientes!');
             });
         });
     }*/
    $scope.delHorario = function(id) {
        swal({
            title: "Tem certeza?",
            text: "Tem certeza que deseja deletar esse horário?",
            icon: "warning",
            buttons: true,
            buttons: {
                cancel: {
                    text: "Cancelar",
                    value: null,
                    visible: true,
                    className: "",
                    closeModal: true,
                },
                confirm: {
                    text: "Sim Desejo!",
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: true
                }
            },
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                // aqui chama o ajax
                $http({
                    url: '../admin/api/agendamento/delHorario/' + id,
                    method: "GET"
                }).then(function(response) {
                    if (response.data.retorno.cod == 0) {
                        swal("Ok!", response.data.retorno.text, "success");
                    } else {
                        swal("Erro!", response.data.retorno.text, "error");
                    }
                    $scope.getHorarios();
                });
            } else {
                // swal("Your imaginary file is safe!");
            }
        });
        // fim swal
        // 
        // 
    };
    
    $scope.addHorario = function(horaagenda, duracao) {
        var diasemana = $("input[name=diaSelect]").val();
        switch (diasemana) {
            case "Domingo":
                diasemana = 1;
                break;
            case "Segunda":
                diasemana = 2;
                break;
            case "Terça":
                diasemana = 3;
                break;
            case "Quarta":
                diasemana = 4;
                break;
            case "Quinta":
                diasemana = 5;
                break;
            case "Sexta":
                diasemana = 6;
                break;
            case "Sábado":
                diasemana = 7;
                break;
            default:
                diasemana = 1;
        }
        $http({
            url: '../admin/api/agendamento/addHorario',
            data: {
                dia: diasemana,
                horario: horaagenda,
                duracao: duracao
            },
            method: "POST"
        }).then(function(response) {
            if (response.data.retorno.cod == 0) {
                swal("Ok!", response.data.retorno.text, "success");
            } else {
                swal("Erro!", response.data.retorno.text, "error");
            }
            $scope.getHorarios(diasemana);
        }, function(response) {
            console.log('Opsss... Algo deu errado!');
        });
    };
    $scope.getAgendamentos = function() {
        $http({
            url: '../admin/api/agendamento/getAgendamentos/2017-12-17',
            method: "GET"
        }).then(function(response) {
            $scope.agendamentos = response.data.agendamentos;
        }, function(response) {
            console.log('Opsss... Algo deu errado!');
        });
    }
    /*  get horarios inicio
     */
    $scope.horarios = {};
    $scope.getHorarios = function(id) {
        $http({
            url: '../admin/api/agendamento/getHorarios',
            method: "get"
        }).then(function(response) {
            $scope.horarios = response.data;
            console.log($scope.horarios);
        }, function(response) {
            console.log('Opsss... Algo deu errado!');
        });
    };
    //fim get horarios
    //$scope.getClients();
    $scope.getAgendamentos();
    $scope.agendar = function(id) {
        alert('' + id);
        $scope.idagendaSel = id;
    };
    $scope.agendarAluno = function(id, agenda, datagenda) {
        $http({
            url: '../admin/api/agendamento/addAgendamento',
            data: {
                aluno: id,
                idagenda: agenda,
                dtagenda: datagenda
            },
            method: "POST"
        }).then(function(response) {
            $scope.cidades = response.data;
        }, function(response) {
            console.log('Opsss... Algo deu errado!');
        });
    };
}); // fim controler alunoController