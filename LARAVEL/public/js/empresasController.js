app.controller('empresasController', function($http, $scope, $timeout, Upload) {
    
    $http.defaults.headers.common.user = $('meta[name="user_logued"]').attr('content');
    $http.defaults.headers.common.unity = $('meta[name="unidade"]').attr('content');
    
    
    
    $scope.inactive = true;
    $scope.changeStatus = function() {
        $scope.inactive = !$scope.inactive;
    };
    $scope.origem = {};
    $scope.tipoSel = '';
    $scope.funcionario_idestado = 6;
    $scope.origemVetor = [];
    $scope.sexo = [];
    $scope.desabilita = false;
    for (var i = 1; i <= 5; i++) {
        if (i == 1) {
            var valueCp = "admin";
            var textoCp = "Administrador de Rede";
        }
        if (i == 2) {
            var valueCp = "adminfilial";
            var textoCp = "Administrador Filial";
        }
        if (i == 3) {
            var valueCp = "coordenador";
            var textoCp = "Coordenador";
        }
        if (i == 4) {
            var valueCp = "professor";
            var textoCp = "Professor";
        }
        if (i == 5) {
            var valueCp = "recepcionista";
            var textoCp = "Recepcionista";
        }
        $scope.origem = {
            valor: valueCp,
            texto: textoCp
        };
        $scope.origemVetor.push($scope.origem);
    }
    $scope.antecede = [];
    $scope.antecipa = [];
    $scope.simnao = [];
    $scope.intervalos = [];
    for (var i = 0; i <= 6; i++) {
        if (i == 0) {
            var valueCp = 0;
            var textoCp = "Não permitir";
        }
        if (i == 1) {
            var valueCp = 1;
            var textoCp = "1 dia";
        }
        if (i == 2) {
            var valueCp = 2;
            var textoCp = "2 dias";
        }
        if (i == 3) {
            var valueCp = 3;
            var textoCp = "3 dias";
        }
        if (i == 4) {
            var valueCp = 4;
            var textoCp = "4 dias";
        }
        if (i == 5) {
            var valueCp = 5;
            var textoCp = "5 dias";
        }
        if (i == 6) {
            var valueCp = 6;
            var textoCp = "6 dias";
        }
        $scope.origem = {
            valor: valueCp,
            texto: textoCp
        };
        $scope.antecede.push($scope.origem);
    }
    for (var i = 0; i <= 6; i++) {
        if (i == 0) {
            var valueCp = 0;
            var textoCp = "Não permitir";
        }
        if (i == 1) {
            var valueCp = 1;
            var textoCp = "1 aula";
        }
        if (i == 2) {
            var valueCp = 2;
            var textoCp = "2 aula";
        }
        if (i == 3) {
            var valueCp = 3;
            var textoCp = "3 aulas";
        }
        if (i == 4) {
            var valueCp = 4;
            var textoCp = "4 aulas";
        }
        if (i == 5) {
            var valueCp = 5;
            var textoCp = "5 aulas";
        }
        if (i == 6) {
            var valueCp = 6;
            var textoCp = "6 aulas";
        }
        if (i == 6) {
            var valueCp = 7;
            var textoCp = "7 aulas";
        }
        $scope.origem = {
            valor: valueCp,
            texto: textoCp
        };
        $scope.antecipa.push($scope.origem);
    }
    var valueCpSexo = '';
    var textoCpSexo = '';
    for (var i = 1; i <= 2; i++) {
        if (i == 1) {
            valueCpSexo = "M";
            textoCpSexo = "Masculino";
        }
        if (i == 2) {
            valueCpSexo = "F";
            textoCpSexo = "Feminino";
        }
        $scope.sexoValue = {
            valor: valueCpSexo,
            texto: textoCpSexo
        };
        $scope.sexo.push($scope.sexoValue);
    }
    var valueCpSn = '';
    var textoCpSn = '';
    for (var i = 1; i <= 2; i++) {
        if (i == 1) {
            valueCpSn = "S";
            textoCpSn = "SIM";
        }
        if (i == 2) {
            valueCpSn = "N";
            textoCpSn = "NÃO";
        }
        $scope.sexoValue = {
            valor: valueCpSn,
            texto: textoCpSn
        };
        $scope.simnao.push($scope.sexoValue);
    }
    for (var i = 0; i <= 4; i++) {
        if (i == 0) {
            var valueCp = 0
            var textoCp = "Sem intervalo";
        }
        if (i == 1) {
            var valueCp = 15;
            var textoCp = "15 minutos";
        }
        if (i == 2) {
            var valueCp = 30;
            var textoCp = "30 minutos";
        }
        if (i == 3) {
            var valueCp = 45;
            var textoCp = "45 minutos";
        }
        if (i == 4) {
            var valueCp = 60;
            var textoCp = "60 minutos";
        }
        $scope.origem = {
            valor: valueCp,
            texto: textoCp
        };
        $scope.intervalos.push($scope.origem);
    }
    $scope.refreshPassword = function(id) {
        console.log(id)
        $http.put('/api/cliente/refresh/password/' + id).then(function() {
            toastr.success('Uma nova senha foi gerada e enviada ao email de cadastro');
        }, function() {
            toastr.warning('Ocorreu um erro ao gerar nova senha');
        })
    }
    $scope.carregaCidade = function(estado) {
        // LISTA CIDADE
        $scope.cidades = {};
        $http({
            url: '../api/unidade/getCidades',
            data: {
                estado: estado
            },
            method: "POST"
        }).then(function(response) {
            $scope.cidades = response.data;
            $timeout(function() {
                $('.cidade').addClass('chosen-select');
                $('.chosen-select').chosen({
                    width: "100%"
                });
            }, 500);
        }, function(response) {
            console.log('Opsss... Algo deu errado!');
        });
    };
    // LISTA EMPRESA
    $scope.getDadosEmpresa = function() {
        $scope.empresa = {};
        $http({
            url: '../api/unidade/getEmpresa',
            method: "POST"
        }).then(function(response) {
            $scope.empresa = response.data[0];
            $scope.carregaCidade(response.data[0].idestado);
        }, function(response) {
            console.log('Opsss... Algo deu errado!');
        });
        $scope.configura = {};
        $http({
            url: '../api/unidade/getConfiguraGfm',
            method: "POST"
        }).then(function(response) {
            $scope.configura = response.data[0];
            slider.noUiSlider.set([$scope.configura.indice_inicial, $scope.configura.final]);
        }, function(response) {
            console.log('Opsss... Algo deu errado!');
        });
        // LISTA ESTADOS
        $scope.estados = {};
        $http({
            url: '../api/unidade/getEstados',
            method: "POST"
        }).then(function(response) {
            $scope.estados = response.data;
            /* $timeout(function () {
                 $('#estado').addClass('chosen-select');
                 $('.chosen-select').chosen({width: "100%"});
             }, 500);*/
        }, function(response) {
            console.log('Opsss... Algo deu errado!');
        });
        $scope.estadosFiliais = {};
        $http({
            url: '../api/unidade/getEstados',
            method: "POST"
        }).then(function(response) {
            $scope.estadosFiliais = response.data;
            /* $timeout(function () {
                 $('#estadoFilial').addClass('chosen-select');
                 $('.chosen-select').chosen({width: "100%"});
             }, 500);*/
        }, function(response) {
            console.log('Opsss... Algo deu errado!');
        });
        $scope.estadosFuncionarios = {};
        $http({
            url: '../api/unidade/getEstados',
            method: "POST"
        }).then(function(response) {
            $scope.estadosFuncionarios = response.data;
            $timeout(function() {
                $('.estadoFuncionario').addClass('chosen-select');
                $('.chosen-select').chosen({
                    width: "100%"
                });
            }, 500);
        }, function(response) {
            console.log('Opsss... Algo deu errado!');
        });
    }


    $scope.getDadosEmpresa();

    $scope.getCep = function(cep) {
        var url = 'https://viacep.com.br/ws/' + cep + '/json/';
        console.log(url);
        $http({
            url: url,
            method: "GET"
        }).then(function(response) {
            console.log(response);
            $scope.empresa.endereco = response.data.logradouro;
            $scope.empresa.bairro = response.data.bairro;
            $scope.empresa.numero = '' ;
            $scope.empresa.idestado = '' ;
            $scope.empresa.idcidade = '' ;



        }, function(response) {
            console.log('Opsss... Algo deu errado!');
        });
    }

    $scope.upConfiguraGfm = function() {
        
        $scope.configura.user_update=$http.defaults.headers.common.user;
        
        $http({
            url: '../api/unidade/upConfiguraGfm',
            data: $scope.configura,
            method: "POST"
        }).then(function(response) {
            toastr.success('Configuração atualizada com sucesso!!');
            $scope.inactive = true;
            $scope.getFiliais();
        }, function(response) {
            if (typeof response.data.fantasia !== "undefined") {
                toastr.warning("O campo fantasia é obrigatório");
            }
            console.log('Opsss... Algo deu errado!');
        });
    };

    $scope.changeEstado = function(estado) {
        // LISTA CIDADE
        $scope.cidades = {};
        $http({
            url: '../api/unidade/getCidades',
            data: {
                estado: estado
            },
            method: "POST"
        }).then(function(response) {
            $scope.cidades = response.data;
            /*  $timeout(function () {
                  $("#cidade").val('').trigger("chosen:updated");
                  $('#cidade').addClass('chosen-select');
                  $('.chosen-select').chosen({width: "100%"});
              }, 500);*/
        }, function(response) {
            console.log('Opsss... Algo deu errado!');
        });
    };

    var slider = document.getElementById('range_slider');
    $scope.qualValor = function() {
        $scope.v_range = slider.noUiSlider.get();
        // console.log('v_range',$scope.v_range);
        $scope.configura.range = $scope.v_range;
        $http({
            url: '../api/unidade/upConfiguraGfm',
            data: $scope.configura,
            method: "POST"
        }).then(function(response) {
            toastr.success('Configuração atualizada com sucesso!!');
            $scope.inactive = true;
            $scope.getFiliais();
        }, function(response) {
            if (typeof response.data.fantasia !== "undefined") {
                toastr.warning("O campo fantasia é obrigatório");
            }
            console.log('Opsss... Algo deu errado!');
        });
    }
    $scope.startNouislider = function() {
        var classes = ['c-18deg-color', 'c-22deg-color', 'c-24deg-color'];
        noUiSlider.create(slider, {
            start: [49, 70],
            connect: [true, true, true],
            step: 1,
            tooltips: true,
            format: wNumb({
                decimals: 0,
                suffix: '%'
            }),
            range: {
                'min': [0],
                'max': [100]
            }
        });
        var connect = slider.querySelectorAll('.noUi-connect');
        for (var i = 0; i < connect.length; i++) {
            connect[i].classList.add(classes[i]);
        }
        $scope.v_range = slider.noUiSlider.get();
    };
    $scope.startNouislider();
    $scope.changeEstadoFilial = function(estado) {
        // LISTA CIDADE
        $scope.cidadesFiliais = {};
        $http({
            url: '../api/unidade/getCidades',
            data: {
                estado: estado
            },
            method: "POST"
        }).then(function(response) {
            $scope.cidadesFiliais = response.data;
            /* $timeout(function () {
                 $("#cidadeFilial").val('').trigger("chosen:updated");
                 $('#cidadeFilial').addClass('chosen-select');
                 $('.chosen-select').chosen({width: "100%"});
             }, 500);*/
        }, function(response) {
            console.log('Opsss... Algo deu errado!');
        });
    };
    slider.noUiSlider.on('update', function(values, handle) {
        $scope.v_range = slider.noUiSlider.get();
        $scope.configura.range = $scope.v_range;
        $http({
            url: '../api/unidade/upConfiguraGfm',
            data: $scope.configura,
            method: "POST"
        }).then(function(response) {
            //toastr.success('Configuração atualizada com sucesso!!');
        }, function(response) {
            //console.log('Opsss... Algo deu errado!');
        });
    });

    $scope.changeEstadoFuncionario = function(estado) {
        // LISTA CIDADE
        $scope.cidadesFuncionarios = {};
        $http({
            url: '../api/unidade/getCidades',
            data: {
                estado: estado
            },
            method: "POST"
        }).then(function(response) {
            $scope.cidadesFuncionarios = response.data;
            $timeout(function() {
                $(".cidadeFuncionario").val('').trigger("chosen:updated");
                $('.cidadeFuncionario').addClass('chosen-select');
                $('.chosen-select').chosen({
                    width: "100%"
                });
            }, 500);
        }, function(response) {
            console.log('Opsss... Algo deu errado!');
        });
    };
    $scope.carregaCidadeFilial = function(estado) {
        // LISTA CIDADE
        $scope.cidadesFiliais = {};
        $http({
            url: '../api/unidade/getCidades',
            data: {
                estado: estado
            },
            method: "POST"
        }).then(function(response) {
            $scope.cidadesFiliais = response.data;
            /*  $timeout(function () {
                  $('#cidadeFilial').addClass('chosen-select');
                  $('.chosen-select').chosen({width: "100%"});
              }, 500);*/
        }, function(response) {
            console.log('Opsss... Algo deu errado!');
        });
    };
    $scope.carregaCidadeFuncionario = function(estado) {
        // LISTA CIDADE
        $scope.cidadesFuncionarios = {};
        $http({
            url: '../api/unidade/getCidades',
            data: {
                estado: estado
            },
            method: "POST"
        }).then(function(response) {
            $scope.cidadesFuncionarios = response.data;
            $timeout(function() {
                $('.cidadeFuncionario').addClass('chosen-select');
                $('.chosen-select').chosen({
                    width: "100%"
                });
            }, 500);
        }, function(response) {
            console.log('Opsss... Algo deu errado!');
        });
    };
    // LISTA FILIAIS
    $scope.filiais = {};
    $http({
        url: '../api/unidade/getFiliais',
        method: "POST"
    }).then(function(response) {
        $scope.filiais = response.data;
    }, function(response) {
        console.log('Opsss... Algo deu errado!');
    });
    $scope.getFiliais = function() {
        $http({
            url: '../api/unidade/getFiliais',
            method: "POST"
        }).then(function(response) {
            $scope.filiais = response.data;
        }, function(response) {
            console.log('Opsss... Algo deu errado!');
        });
    };
    $scope.editFilial = function(id) {
        $http({
            url: '../api/unidade/getFilial',
            data: {
                id: id
            },
            method: "POST"
        }).then(function(response) {
            $scope.carregaCidadeFilial(response.data[0].idestado);
            $scope.filial = response.data[0];
            /*$timeout(function () {
                $("#estadoFilial").val('').trigger("chosen:updated");
                $('#estadoFilial').addClass('chosen-select');
                $('#cidadeFilial').addClass('chosen-select');
                $('.chosen-select').chosen({width: "100%"});
            }, 500);*/
        }, function(response) {
            console.log('Opsss... Algo deu errado!');
        });
    };
    $scope.upEmpresa = function($empresa) {
        $scope.empresa = {};
        $scope.empresa = $empresa;
        $http({
            url: '../api/unidade/upEmpresa',
            data: $scope.empresa,
            method: "POST"
        }).then(function(response) {
            toastr.success('Empresa atualizada com sucesso!!');
            $scope.inactive = true;
            $scope.getFiliais();
        }, function(response) {
            if (typeof response.data.fantasia !== "undefined") {
                toastr.warning("O campo fantasia é obrigatório");
            }
            if (typeof response.data.razao_social !== "undefined") {
                toastr.warning("O campo razão social é obrigatório");
            }
            if (typeof response.data.idestado !== "undefined") {
                toastr.warning("O campo estado é obrigatório");
            }
            if (typeof response.data.idcidade !== "undefined") {
                toastr.warning("O campo cidade é obrigatório");
            }
            //            toastr.warning("Ocorreu um erro ao atualizar os dados");
            console.log('Opsss... Algo deu errado!');
        });
    };
    $scope.updateFilial = function($filial) {
        $scope.filial = {};
        $scope.filial = $filial;
        $http({
            url: '../api/unidade/upFilial',
            data: $scope.filial,
            method: "POST"
        }).then(function(response) {
            toastr.success('Filial atualizada com sucesso!!');
            $('#modalEditalFilial').modal('toggle');
            $scope.getFiliais();
        }, function(response) {
            if (typeof response.data.fantasia !== "undefined") {
                toastr.warning("O campo fantasia é obrigatório");
            }
            if (typeof response.data.razao_social !== "undefined") {
                toastr.warning("O campo razão social é obrigatório");
            }
            if (typeof response.data.idestado !== "undefined") {
                toastr.warning("O campo estado é obrigatório");
            }
            if (typeof response.data.idcidade !== "undefined") {
                toastr.warning("O campo cidade é obrigatório");
            }
            //            toastr.warning("Ocorreu um erro ao atualizar os dados");
            console.log('Opsss... Algo deu errado!');
        });
    };
    $scope.addFilial = function($filial) {
        $scope.filial = {};
        $scope.filial = $filial;
        $http({
            url: '../api/unidade/addFilial',
            data: $scope.filial,
            method: "POST"
        }).then(function(response) {
            toastr.success('Filial gravada com sucesso!!');
            $scope.getFiliais();
        }, function(response) {
            if (typeof response.data.fantasia !== "undefined") {
                toastr.warning("O campo fantasia é obrigatório");
            }
            if (typeof response.data.razao_social !== "undefined") {
                toastr.warning("O campo razão social é obrigatório");
            }
            if (typeof response.data.idestado !== "undefined") {
                toastr.warning("O campo estado é obrigatório");
            }
            if (typeof response.data.idcidade !== "undefined") {
                toastr.warning("O campo cidade é obrigatório");
            }
            //            toastr.warning("Ocorreu um erro ao atualizar os dados");
            console.log('Opsss... Algo deu errado!');
        });
    };
    $scope.novo = function() {
        $scope.local = {};
        $scope.funcionario = {};
        //$('#role').empty();
    };
    $scope.addLocal = function(local) {
        $scope.local = {};
        $scope.local = local;
        $http({
            url: '../api/unidade/addLocal',
            data: $scope.local,
            method: "POST"
        }).then(function(response) {
            toastr.success('Ambiente gravado com sucesso!!');
            $('#modalNovoLocal').modal('toggle');
            $scope.getLocais();
        }, function(response) {
            if (typeof response.data.nmlocal !== "undefined") {
                toastr.warning("O campo nome é obrigatório");
            }
            if (typeof response.data.metros !== "undefined") {
                toastr.warning("O campo metros é obrigatório");
            }
            //            toastr.warning("Ocorreu um erro ao atualizar os dados");
            console.log('Opsss... Algo deu errado!');
        });
    };
    $scope.upLocal = function($local) {
        $http({
            url: '../api/unidade/upLocal',
            data: $local,
            method: "POST"
        }).then(function(response) {
            toastr.success('Ambiente atualizado com sucesso!!');
            $('#modalEditalLocal').modal('toggle');
            $scope.getLocais();
            // $scope.getFiliais();
        }, function(response) {
            if (typeof response.data.fantasia !== "undefined") {
                toastr.warning("O campo fantasia é obrigatório");
            }
            if (typeof response.data.razao_social !== "undefined") {
                toastr.warning("O campo razão social é obrigatório");
            }
            //            toastr.warning("Ocorreu um erro ao atualizar os dados");
            console.log('Opsss... Algo deu errado!');
        });
    };
    //     $scope.upLocal = function ($local) {
    //         $http({
    //             url: '../api/unidade/addLocal',
    //             data: $local,
    //             method: "POST"
    //         }).then(function (response) {
    //             toastr.success('Ambiente atualizado com sucesso!!');
    //             $('#modalNovoLocal').modal('toggle');
    //             $scope.getFiliais();
    //         }, function (response) {
    //             if (typeof response.data.nmlocal !== "undefined") {
    //                 toastr.warning("O campo nome é obrigatório");
    //             }
    //             if (typeof response.data.metros !== "undefined") {
    //                 toastr.warning("O campo metros é obrigatório");
    //             }
    // //            toastr.warning("Ocorreu um erro ao atualizar os dados");
    //             console.log('Opsss... Algo deu errado!');
    //         });
    //     };
    // DELETA FILIAL
    $scope.deleteFilial = function(filial_id) {
        if (filial_id !== undefined) {
            swal({
                title: "Tem certeza?",
                text: "Tem certeza que deseja deletar essa filial?",
                icon: "warning",
                buttons: true,
                buttons: {
                    cancel: {
                        text: "Cancelar",
                        value: null,
                        visible: true,
                        className: "",
                        closeModal: true,
                    },
                    confirm: {
                        text: "Sim, tenho certeza!",
                        value: true,
                        visible: true,
                        className: "",
                        closeModal: true
                    }
                },
                dangerMode: true,
            }).then((isConfirm) => {
                if (isConfirm) {
                    // aqui chama o ajax
                    $http({
                        url: '../api/unidade/delFilial/' + filial_id,
                        method: "POST"
                    }).then(function(response) {
                        $scope.getFiliais();
                        swal("Deletado!", "Filial deletada com sucesso!", "success");
                    });
                }
            });
            // fim swal
            // 
        } else {
            swal("Opsss!", "Selecione um filial para deletar!", "error");
        }
    };
    // LISTA FILIAIS
    $scope.locais = {};
    $http({
        url: '../api/unidade/getLocais',
        method: "POST"
    }).then(function(response) {
        $scope.locais = response.data;
    }, function(response) {
        console.log('Opsss... Algo deu errado!');
    });
    $scope.getLocais = function() {
        $http({
            url: '../api/unidade/getLocais',
            method: "POST"
        }).then(function(response) {
            $scope.locais = response.data;
        }, function(response) {
            console.log('Opsss... Algo deu errado!');
        });
    };
    $scope.editLocal = function(id) {
        $http({
            url: '../api/unidade/getLocal',
            data: {
                id: id
            },
            method: "POST"
        }).then(function(response) {
            $scope.local = response.data[0];
        }, function(response) {
            console.log('Opsss... Algo deu errado!');
        });
    };
    // DELETA LOCAL
    $scope.deleteLocal = function(local_id) {
        if (local_id !== undefined) {
            swal({
                title: "Tem certeza?",
                text: "Tem certeza que deseja deletar esse local?",
                icon: "warning",
                buttons: true,
                buttons: {
                    cancel: {
                        text: "Cancelar",
                        value: null,
                        visible: true,
                        className: "",
                        closeModal: true,
                    },
                    confirm: {
                        text: "Sim, tenho certeza!",
                        value: true,
                        visible: true,
                        className: "",
                        closeModal: true
                    }
                },
                dangerMode: true,
            }).then((isConfirm) => {
                if (isConfirm) {
                    // aqui chama o ajax
                    $http({
                        url: '../api/unidade/delLocal/' + local_id,
                        method: "POST"
                    }).then(function(response) {
                        $scope.getLocais();
                        swal("Deletado!", "Local deletado com sucesso!", "success");
                    });
                }
            });
            // fim swal
            // 
        } else {
            swal("Opsss!", "Selecione um local para deletar!", "error");
        }
    };
    // LISTA NUTRICIONISTAS
    $scope.nutricionistas = {};
    $http({
        url: '../api/unidade/getNutricionistas',
        method: "POST"
    }).then(function(response) {
        $scope.nutricionistas = response.data;
    }, function(response) {
        console.log('Opsss... Algo deu errado!');
    });
    $scope.getNutricionistas = function() {
        $http({
            url: '../api/unidade/getNutricionistas',
            method: "POST"
        }).then(function(response) {
            $scope.nutricionistas = response.data;
        }, function(response) {
            console.log('Opsss... Algo deu errado!');
        });
    };
    $scope.editNutricionista = function(id) {
        $http({
            url: '../api/unidade/getNutricionista',
            data: {
                id: id
            },
            method: "POST"
        }).then(function(response) {
            $scope.carregaCidadeFuncionario(response.data.idestado);
            $scope.funcionario = response.data;
            $timeout(function() {
                $('.estadoFuncionario').addClass('chosen-select');
                $('.cidadeFuncionario').addClass('chosen-select');
                $('.chosen-select').chosen({
                    width: "100%"
                });
            }, 500);
        }, function(response) {
            console.log('Opsss... Algo deu errado!');
        });
    };
    $scope.addNutricionista = function(nutri) {
        $scope.funcionario = {};
        $scope.funcionario = nutri;
        $http({
            url: '../api/unidade/addNutricionista',
            data: $scope.funcionario,
            method: "POST"
        }).then(function(response) {
            if (response.data.type == 'success') {
                toastr.success(response.data.text);
            } else {
                toastr.warning(response.data.text);
            }
            $('#modalEditalNutricionista').modal('toggle');
            $scope.getNutricionistas();
        }, function(response) {
            if (typeof response.data.name !== "undefined") {
                toastr.warning("O campo nome é obrigatório");
            }
            if (typeof response.data.email !== "undefined") {
                toastr.warning("O campo email é obrigatório");
            }
            if (typeof response.data.telefone !== "undefined") {
                toastr.warning("O campo telefone é obrigatório");
            }
            if (typeof response.data.dt_nascimento !== "undefined") {
                toastr.warning("O campo data de nascimento é obrigatório");
            }
            if (typeof response.data.cpf !== "undefined") {
                toastr.warning("O campo cpf é obrigatório");
            }
            if (typeof response.data.endereco !== "undefined") {
                toastr.warning("O campo endereco é obrigatório");
            }
            if (typeof response.data.numero !== "undefined") {
                toastr.warning("O campo número é obrigatório");
            }
            if (typeof response.data.cep !== "undefined") {
                toastr.warning("O campo cep é obrigatório");
            }
            if (typeof response.data.bairro !== "undefined") {
                toastr.warning("O campo bairro é obrigatório");
            }
            if (typeof response.data.idestado !== "undefined") {
                toastr.warning("O campo estado é obrigatório");
            }
            if (typeof response.data.idcidade !== "undefined") {
                toastr.warning("O campo cidade é obrigatório");
            }
            console.log('Opsss... Algo deu errado!');
        });
    };
    $scope.upNutricionista = function(nutri) {
        $scope.funcionario = {};
        $scope.funcionario = nutri;
        $http({
            url: '../api/unidade/upNutricionista',
            data: $scope.funcionario,
            method: "POST"
        }).then(function(response) {
            if (response.data.type == 'success') {
                toastr.success(response.data.text);
            } else {
                toastr.warning(response.data.text);
            }
            $('#modalEditalNutricionista').modal('toggle');
            $scope.getNutricionistas();
        }, function(response) {
            if (typeof response.data.name !== "undefined") {
                toastr.warning("O campo nome é obrigatório");
            }
            if (typeof response.data.email !== "undefined") {
                toastr.warning("O campo email é obrigatório");
            }
            if (typeof response.data.telefone !== "undefined") {
                toastr.warning("O campo telefone é obrigatório");
            }
            if (typeof response.data.dt_nascimento !== "undefined") {
                toastr.warning("O campo data de nascimento é obrigatório");
            }
            if (typeof response.data.cpf !== "undefined") {
                toastr.warning("O campo cpf é obrigatório");
            }
            if (typeof response.data.endereco !== "undefined") {
                toastr.warning("O campo endereco é obrigatório");
            }
            if (typeof response.data.numero !== "undefined") {
                toastr.warning("O campo número é obrigatório");
            }
            if (typeof response.data.cep !== "undefined") {
                toastr.warning("O campo cep é obrigatório");
            }
            if (typeof response.data.bairro !== "undefined") {
                toastr.warning("O campo bairro é obrigatório");
            }
            if (typeof response.data.idestado !== "undefined") {
                toastr.warning("O campo estado é obrigatório");
            }
            if (typeof response.data.idcidade !== "undefined") {
                toastr.warning("O campo cidade é obrigatório");
            }
            console.log('Opsss... Algo deu errado!');
        });
    };



    $scope.addFuncionario = function(nutri) {
        $scope.funcionario = {};
        $scope.funcionario = nutri;
        // $scope.desabilita = true;

        console.log($scope.funcionario);
        console.log($scope.funcionario.name);

        if (!$scope.funcionario.name) {
           alert("O campo nome é obrigatório");
            return false;
        }
        if ($scope.funcionario.email == undefined) {
            alert("O campo email é obrigatório");
            return false;
        }
        /*if ($scope.funcionario.telefone == undefined) {
            alert("O campo telefone é obrigatório");
            return false;
        }*/
        /*if ($scope.funcionario.dt_nascimento == undefined) {
            alert("O campo data de nascimento é obrigatório");
            return false;
        }*/
        /*if ($scope.funcionario.cpf == undefined) {
            alert("O campo cpf é obrigatório");
            return false;
        }*/
        /*if ($scope.funcionario.role == undefined) {
            alert("O campo tipo é obrigatório");
            return false;
        }
        /*if ($scope.funcionario.endereco == undefined) {
            alert("O campo endereco é obrigatório");
            return false;
        }
        if ($scope.funcionario.numero == undefined) {
            alert("O campo número é obrigatório");
            return false;
        }
        if ($scope.funcionario.cep == undefined) {
            alert("O campo cep é obrigatório");
            return false;
        }
        if ($scope.funcionario.bairro == undefined) {
            alert("O campo bairro é obrigatório");
            return false;
        }
        if ($scope.funcionario.idestado == undefined) {
            alert("O campo estado é obrigatório");
            return false;
        }
        if ($scope.funcionario.idcidade == undefined) {
            alert("O campo cidade é obrigatório");
            return false;
        }
        if ($scope.funcionario.password == undefined) {
            alert("Informe a sernha com no mínimo 6 caracteres");
            return false;
        }*/




        // send request

        $http({
            url: '../api/unidade/addFuncionario',
            data: $scope.funcionario,
            method: "POST"
        }).then(function(response) {
            $scope.funcionario = null;
            if (response.data.type == 'success') {
                $('#modalNovoFuncionario').modal('toggle');
                swal("Salvar!", response.data.text, "success");
                //toastr.success(response.data.text);
            } else {
                $('#modalNovoFuncionario').modal('toggle');
                if (response.data.cod == '-1') {
                    //toastr.warning(response.data.cod + ' - ' + response.data.text);
                    swal({
                        title: "Tem certeza?",
                        text: "Funcionário excluído. Deseja reativá-lo?",
                        icon: "warning",
                        buttons: true,
                        buttons: {
                            cancel: {
                                text: "Cancelar",
                                value: null,
                                visible: true,
                                className: "",
                                closeModal: true,
                            },
                            confirm: {
                                text: "Sim, tenho certeza!",
                                value: true,
                                visible: true,
                                className: "",
                                closeModal: true
                            }
                        },
                        dangerMode: true,
                    }).then((isConfirm) => {
                        if (isConfirm) {
                            // aqui chama o ajax
                            $http({
                                url: '../api/unidade/delFuncionario/' + response.data.id + '/A',
                                method: "POST"
                            }).then(function(response) {
                                $scope.getFuncionarios();
                                swal("Reativado!", "Funcionário reativado com sucesso!", "success");
                            });
                        }
                    });
                    // fim swal
                } else {
                    toastr.warning(response.data.text);
                }
            }
            $scope.getFuncionarios();
        }, function(response) {
           
            console.log('Opsss... Algo deu errado!');
        });

    



    };
    $scope.updateAvatar = function($files, $file, $funcionario) {
        if (!$file) {
            return false;
        }
        Upload.upload({
            url: '/api/upload',
            data: {
                file: $file,
                client: $funcionario
            },
            method: 'POST'
        }).then(function(resp) {
            toastr.success('Avatar atualizado com sucesso!')
        }, function(resp) {
            console.error(resp)
        }, function(evt) {
            console.log('progress: ' + parseInt(100.0 * evt.loaded / evt.total) + '% file :' + evt.config.data.file.name);
        }).catch(function(err) {
            console.log(err)
        });
    };
    $scope.upFuncionario = function(nutri) {
        $scope.funcionario = {};
        $scope.funcionario = nutri;
        $http({
            url: '../api/unidade/upFuncionario',
            data: $scope.funcionario,
            method: "POST"
        }).then(function(response) {
            if (response.data.type == 'success') {
                $('#modalEditalFuncionario').modal('toggle');
                //toastr.success(response.data.text);
                swal("Parabéns!", response.data.text, "success");
            } else {
                $('#modalEditalFuncionario').modal('toggle');
                // toastr.warning(response.data.text);
                swal("Erro!", response.data.text, "error");
            }
            $scope.getFuncionarios();
        }, function(response) {
            if (typeof response.data.name !== "undefined") {
                swal("O campo nome é obrigatório", "error");
            }
            if (typeof response.data.genero !== "undefined") {
                swal("O campo sexo é obrigatório", "error");
            }
            if (typeof response.data.email !== "undefined") {
                swal("O campo email é obrigatório", "error");
            }
            if (typeof response.data.telefone !== "undefined") {
                swal("O campo telefone é obrigatório", "error");
            }
            if (typeof response.data.dt_nascimento !== "undefined") {
                swal("O campo data de nascimento é obrigatório", "error");
            }
            if (typeof response.data.cpf !== "undefined") {
                swal("O campo cpf é obrigatório", "error");
            }
            if (typeof response.data.role !== "undefined") {
                swal("O campo tipo é obrigatório", "error");
            }
            if (typeof response.data.endereco !== "undefined") {
                swal("O campo endereco é obrigatório", "error");
            }
            if (typeof response.data.numero !== "undefined") {
                swal("O campo número é obrigatório", "error");
            }
            if (typeof response.data.cep !== "undefined") {
                swal("O campo cep é obrigatório", "error");
            }
            if (typeof response.data.bairro !== "undefined") {
                swal("O campo bairro é obrigatório", "error");
            }
            if (typeof response.data.idestado !== "undefined") {
                swal("O campo estado é obrigatório", "error");
            }
            if (typeof response.data.idcidade !== "undefined") {
                swal("O campo cidade é obrigatório", "error");
            }
            console.log('Opsss... Algo deu errado!');
        });
    };
    // DELETA NUTRICIONISTA
    $scope.deleteNutricionista = function(nutricionista_id) {
        if (nutricionista_id !== undefined) {
            swal({
                title: "Tem certeza?",
                text: "Tem certeza que deseja deletar esse nutricionista?",
                icon: "warning",
                buttons: true,
                buttons: {
                    cancel: {
                        text: "Cancelar",
                        value: null,
                        visible: true,
                        className: "",
                        closeModal: true,
                    },
                    confirm: {
                        text: "Sim, tenho certeza!",
                        value: true,
                        visible: true,
                        className: "",
                        closeModal: true
                    }
                },
                dangerMode: true,
            }).then((isConfirm) => {
                if (isConfirm) {
                    // aqui chama o ajax
                    $http({
                        url: '../api/unidade/delNutricionista/' + nutricionista_id,
                        method: "POST"
                    }).then(function(response) {
                        $scope.getNutricionistas();
                        swal("Deletado!", "Nutricionista deletado com sucesso!", "success");
                    });
                }
            });
            // fim swal
            // 
        } else {
            swal("Opsss!", "Selecione um nutricionista para deletar!", "error");
        }
    };
    // LISTA FUNCIONÁRIOS
    $scope.funcionarios = {};
    $http({
        url: '../api/unidade/getFuncionarios',
        method: "POST"
    }).then(function(response) {
        $scope.funcionarios = response.data;
    }, function(response) {
        console.log('Opsss... Algo deu errado!');
    });
    $scope.getFuncionarios = function() {
        $http({
            url: '../api/unidade/getFuncionarios',
            method: "POST"
        }).then(function(response) {
            $scope.funcionarios = response.data;
        }, function(response) {
            console.log('Opsss... Algo deu errado!');
        });
    };
    $scope.editFuncionario = function(id) {
        $scope.funcionario = {};
        $http({
            url: '../api/unidade/getFuncionario',
            data: {
                id: id
            },
            method: "POST"
        }).then(function(response) {
            $scope.carregaCidadeFuncionario(response.data.idestado);
            $scope.tipoSel = response.data.role;
            $scope.funcionario = response.data;
            $scope.funcionario.genero = response.data.genero;
            console.log($scope.funcionario);
            $timeout(function() {
                $('.estadoFuncionario').addClass('chosen-select');
                $('.cidadeFuncionario').addClass('chosen-select');
                $('.chosen-select').chosen({
                    width: "100%"
                });
            }, 500);
        }, function(response) {
            console.log('Opsss... Algo deu errado!');
        });
    };



     $scope.getCepFuncionario = function(cep) {
        var url = 'https://viacep.com.br/ws/' + cep + '/json/';
        console.log(url);
        $http({
            url: url,
            method: "GET"
        }).then(function(response) {
            console.log(response);
            $scope.funcionario.endereco = response.data.logradouro;
            $scope.funcionario.bairro = response.data.bairro;
            $scope.funcionario.numero = '' ;
            $scope.funcionario.idestado = '' ;
            $scope.funcionario.cidadeFuncionario = '' ;
            


        }, function(response) {
            console.log('Opsss... Algo deu errado!');
        });
    }


    // DELETA FUNCIONARIO
    $scope.deleteFuncionario = function(funcionario_id) {
        if (funcionario_id !== undefined) {
            swal({
                title: "Tem certeza?",
                text: "Tem certeza que deseja deletar esse funcionário?",
                icon: "warning",
                buttons: true,
                buttons: {
                    cancel: {
                        text: "Cancelar",
                        value: null,
                        visible: true,
                        className: "",
                        closeModal: true,
                    },
                    confirm: {
                        text: "Sim, tenho certeza!",
                        value: true,
                        visible: true,
                        className: "",
                        closeModal: true
                    }
                },
                dangerMode: true,
            }).then((isConfirm) => {
                if (isConfirm) {
                    // aqui chama o ajax
                    $http({
                        url: '../api/unidade/delFuncionario/' + funcionario_id + '/I',
                        method: "POST"
                    }).then(function(response) {
                        $scope.getFuncionarios();
                        swal("Deletado!", "Funcionário deletado com sucesso!", "success");
                    });
                }
            });
            // fim swal
            // 
        } else {
            swal("Opsss!", "Selecione um funcionário para deletar!", "error");
        }
    };
    // LISTA BLACKLISTMODALIDADES
    $scope.blacklists = {};
    $http({
        url: '../api/unidade/getModalidadesBlackList',
        method: "POST"
    }).then(function(response) {
        $scope.tamanhobl = response.data.length;
        $scope.blacklists = response.data;
        // LISTA MODALIDADES
        $scope.modalidades = [];
        $http({
            url: '../api/unidade/getModalidades',
            method: "POST"
        }).then(function(response) {
            var tamanho = response.data.length;
            var statusModalidade = 'S';
            for (var i = 0; i < tamanho; i++) {
                for (var j = 0; j < $scope.tamanhobl; j++) {
                    if (response.data[i].id === parseInt($scope.blacklists[j].atividade_id)) {
                        statusModalidade = 'N';
                        break;
                    } else {
                        statusModalidade = 'S';
                        continue;
                    }
                }
                $scope.modalidade = {
                    id: response.data[i].id,
                    nmatividade: response.data[i].nmatividade,
                    stmenuapp: statusModalidade
                };
                $scope.modalidades.push($scope.modalidade);
            }
        }, function(response) {
            console.log('Opsss... Algo deu errado!');
        });
    }, function(response) {
        console.log('Opsss... Algo deu errado!');
    });
    $scope.getBlacklists = function() {
        $scope.blacklists = {};
        $http({
            url: '../api/unidade/getModalidadesBlackList',
            method: "POST"
        }).then(function(response) {
            $scope.tamanhobl = response.data.length;
            $scope.blacklists = response.data;
            // LISTA MODALIDADES
            $scope.modalidades = [];
            $http({
                url: '../api/unidade/getModalidades',
                method: "POST"
            }).then(function(response) {
                var tamanho = response.data.length;
                var statusModalidade = 'S';
                for (var i = 0; i < tamanho; i++) {
                    for (var j = 0; j < $scope.tamanhobl; j++) {
                        if (response.data[i].id === parseInt($scope.blacklists[j].atividade_id)) {
                            statusModalidade = 'N';
                            break;
                        } else {
                            statusModalidade = 'S';
                            continue;
                        }
                    }
                    $scope.modalidade = {
                        id: response.data[i].id,
                        nmatividade: response.data[i].nmatividade,
                        stmenuapp: statusModalidade
                    };
                    $scope.modalidades.push($scope.modalidade);
                }
            }, function(response) {
                console.log('Opsss... Algo deu errado!');
            });
        }, function(response) {
            console.log('Opsss... Algo deu errado!');
        });
    };
    $scope.statusModal = function(modalidade) {
        swal({
            title: "Tem certeza?",
            text: "Tem certeza que deseja atualizar o status dessa modalidade?",
            icon: "warning",
            buttons: true,
            buttons: {
                cancel: {
                    text: "Cancelar",
                    value: null,
                    visible: true,
                    className: "",
                    closeModal: true,
                },
                confirm: {
                    text: "Sim, tenho certeza!",
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: true
                }
            },
            dangerMode: true,
        }).then((isConfirm) => {
            if (isConfirm) {
                $http({
                    url: '../api/unidade/upModalidadesBlackList',
                    data: {
                        id: modalidade
                    },
                    method: "POST"
                }).then(function(response) {
                    swal("Sucesso!", response.data.text, "success");
                }, function(response) {
                    console.log('Opsss... Algo deu errado!');
                });
            } else {
                $scope.getBlacklists();
            }
        });
        // fim swal
        // 
    }
});