


///////////////////////////////////////////////////////////////
function getCalendarioPadrao(tipo,codigo,mes,ano) {


    var classTipo = '';
    var modalidade =0;
    switch (tipo) {
        case 'natacao':
            modalidade =3;

            classTipo = $('.calendarioNatacao');

            break;

        case 'cross':
            modalidade =4;
            classTipo = $('.calendarioCross');

            break;

        case 'corrida':
            modalidade =5;

            classTipo = $('.calendarioCorrida');

            break;

        case 'ciclismo':
            modalidade =6;

            classTipo = $('.calendarioCiclismo');

            break;


    }

    $(classTipo).html('Aguarde, carregando calendario...');

    $(".btnprescicaoSalvar").hide();

    $.post('/admin/api/prescricao/getCaledarioPadrao', {
        mes: mes, ano: ano, tipo: tipo, treino_id: codigo

    }, function (retorno) {

        $(classTipo).html('');

        $(classTipo).append(retorno);

        $(".anteriorMes").prop("disabled", false);

        $(".proximoMes").prop("disabled", false);

        $(".btnprescicaoSalvar").show();

    });
    
    $.ajax({
        type: 'POST',
        url: '../api/prescricao/getFichaPadraoCross/' + codigo,
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        success: function (obj) {
            
            //console.log(obj.treinopadrao);
            if (tipo == 'cross') {
                $("#nome_treino_cross").val(obj.treinopadrao[0].nome_treino);
                $("#objetivo_treino_cross").val(obj.treinopadrao[0].idobjetivo);
                $("#nivel_treino_cross").val(obj.treinopadrao[0].idnivel);
                $("#sexo_treino_cross").val(obj.treinopadrao[0].treino_sexo);

                if (obj.treinopadrao[0].treino_sexo !== '') {
                    var selectbox = $('#sexo_treino_cross');
                    selectbox.find('option').remove();

                    var descSexo = '';
                    var descSexo1 = '';
                    var sSexo1 = '';
                    if (obj.treinopadrao[0].treino_sexo === 'M') {
                        descSexo = 'MASCULINO';
                        descSexo1 = 'FEMININO';
                        sSexo1 = 'F';
                    }
                    if (obj.treinopadrao[0].treino_sexo === 'F') {
                        descSexo = 'FEMININO';
                        descSexo1 = 'MASCULINO';
                        sSexo1 = 'M';
                    }
                    $('<option>').val(obj.treinopadrao[0].treino_sexo).text(descSexo).appendTo(selectbox);
                    $('<option>').val(sSexo1).text(descSexo1).appendTo(selectbox);
                }
            }
            if (tipo == 'corrida') {

                $("#nome_treino_corrida").val(obj.treinopadrao[0].nome_treino);
                $("#objetivo_treino_corrida").val(obj.treinopadrao[0].idobjetivo);
                $("#nivel_treino_corrida").val(obj.treinopadrao[0].idnivel);
                $("#sexo_treino_corrida").val(obj.treinopadrao[0].treino_sexo);
                
                if (obj.treinopadrao[0].treino_sexo !== '') {
                    var selectbox = $('#sexo_treino_corrida');
                    selectbox.find('option').remove();

                    var descSexo = '';
                    var descSexo1 = '';
                    var sSexo1 = '';
                    if (obj.treinopadrao[0].treino_sexo === 'M') {
                        descSexo = 'MASCULINO';
                        descSexo1 = 'FEMININO';
                        sSexo1 = 'F';
                    }
                    if (obj.treinopadrao[0].treino_sexo === 'F') {
                        descSexo = 'FEMININO';
                        descSexo1 = 'MASCULINO';
                        sSexo1 = 'M';
                    }
                    $('<option>').val(obj.treinopadrao[0].treino_sexo).text(descSexo).appendTo(selectbox);
                    $('<option>').val(sSexo1).text(descSexo1).appendTo(selectbox);
                }
            }    
            if (tipo == 'natacao') {

                $("#nome_treino_natacao").val(obj.treinopadrao[0].nome_treino);
                $("#objetivo_treino_natacao").val(obj.treinopadrao[0].idobjetivo);
                $("#nivel_treino_natacao").val(obj.treinopadrao[0].idnivel);
                $("#sexo_treino_natacao").val(obj.treinopadrao[0].treino_sexo);

                if (obj.treinopadrao[0].treino_sexo !== '') {
                    var selectbox = $('#sexo_treino_natacao');
                    selectbox.find('option').remove();

                    var descSexo = '';
                    var descSexo1 = '';
                    var sSexo1 = '';
                    if (obj.treinopadrao[0].treino_sexo === 'M') {
                        descSexo = 'MASCULINO';
                        descSexo1 = 'FEMININO';
                        sSexo1 = 'F';
                    }
                    if (obj.treinopadrao[0].treino_sexo === 'F') {
                        descSexo = 'FEMININO';
                        descSexo1 = 'MASCULINO';
                        sSexo1 = 'M';
                    }
                    $('<option>').val(obj.treinopadrao[0].treino_sexo).text(descSexo).appendTo(selectbox);
                    $('<option>').val(sSexo1).text(descSexo1).appendTo(selectbox);
                }
            }                    
            if (tipo == 'ciclismo') {

                $("#nome_treino_ciclismo").val(obj.treinopadrao[0].nome_treino);
                $("#objetivo_treino_ciclismo").val(obj.treinopadrao[0].idobjetivo);
                $("#nivel_treino_ciclismo").val(obj.treinopadrao[0].idnivel);
                $("#sexo_treino_ciclismo").val(obj.treinopadrao[0].treino_sexo);

                if (obj.treinopadrao[0].treino_sexo !== '') {
                    var selectbox = $('#sexo_treino_ciclismo');
                    selectbox.find('option').remove();

                    var descSexo = '';
                    var descSexo1 = '';
                    var sSexo1 = '';
                    if (obj.treinopadrao[0].treino_sexo === 'M') {
                        descSexo = 'MASCULINO';
                        descSexo1 = 'FEMININO';
                        sSexo1 = 'F';
                    }
                    if (obj.treinopadrao[0].treino_sexo === 'F') {
                        descSexo = 'FEMININO';
                        descSexo1 = 'MASCULINO';
                        sSexo1 = 'M';
                    }
                    $('<option>').val(obj.treinopadrao[0].treino_sexo).text(descSexo).appendTo(selectbox);
                    $('<option>').val(sSexo1).text(descSexo1).appendTo(selectbox);
                }
            }             
            changeObjetivo(modalidade,obj.treinopadrao[0].idobjetivo,obj.treinopadrao[0].idnivel,obj.treinopadrao[0].nmnivel);
        }
        
    });    

}



function changeObjetivo(tipo, id,nivel,nmnivel) {
    

    var selectbox='';
    var nome='';
    $.ajax({
        type: 'POST',
        url: '../api/prescricao/getNiveis/' + tipo + '/' + id,
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        success: function (obj) {
            var data = obj.data;
            
                if (tipo== '3'){
                    selectbox = $('#nivel_treino_natacao');
                }
                if (tipo== '4'){
                    selectbox = $('#nivel_treino_cross');

                }
                if (tipo== '5'){

                    selectbox = $('#nivel_treino_corrida');

                }
                if (tipo== '6'){

                    selectbox = $('#nivel_treino_ciclismo');
                }
                    
            
//            /selectbox = $('#nivel_treino_cross');
            selectbox.find('option').remove();
              $('<option>').val(nivel).text(nmnivel).appendTo(selectbox);
            for (var i = 0; i < obj.length; i++) {

                $('<option>').val(obj[i].id).text(obj[i].nmnivel).appendTo(selectbox);
            }
            
        }
    });
}

app.controller('treinopadraoController', function ($http, $scope, $timeout) {
    $scope.linhasAmemo = [];
    $scope.linhasBmemo = [];
    $scope.linhasCmemo = [];
    $scope.linhasDmemo = [];
    $scope.linhasEmemo = [];
    $scope.linhasFmemo = [];
    $scope.linhasGmemo = [];

    $scope.treino_objetivo_cross = '';
    $scope.treino_nivel_cross = '';

    $scope.video_preview = '';

    $scope.valido = true;
    $scope.treinoSel = {};
    $scope.treino_iniciado = 'N';
    $scope.exibe_msg = true;
    $scope.ordemAbaA = [];
    $scope.ord = [];

    $scope.exercicio = [];
    $scope.series = [];
    $scope.repeticoes = [];
    $scope.carga = [];
    $scope.interval = [];
    $scope.treino_observacao='';
    $scope.treino_id_padrao = [];
    $scope.treino_id_padraocorrida = [];
    $scope.treino_id_padraociclismo = [];
    $scope.treino_id_padraonatacao = [];

    $scope.treino_padrao_cross = [];

    $scope.grupos = [];


    $scope.exibePrescricao = false;
    $scope.treino_id = '';
    $scope.treino_qtddias = '';
    $scope.treino_revisao = '';

    $scope.treino_nivel = null;
    $scope.objetivo_id = null;
    $scope.treino_padrao = false;
    $scope.treino_atual = false;
    $scope.treino_nome_padrao = 'ssss';
    $scope.objetivos = [];
    $scope.objetivosCross = [];
    $scope.objetivosNatacao = [];
    $scope.objetivosCorrida = [];
    $scope.objetivosCiclismo = [];

    $scope.addTreinoBtn = false;
    $scope.salvarTreinoBtn = false;
    $scope.delTreinoBtn = false;
    $scope.addLinhaBtn = false;

    $scope.addTreinoBtnCross = false;
    $scope.salvarTreinoBtnCross = false;
    $scope.delTreinoBtnCross = false;
    $scope.addLinhaBtnCross = false;

    $scope.addTreinoBtnCorrida = false;
    $scope.salvarTreinoBtnCorrida = false;
    $scope.delTreinoBtnCorrida = false;
    $scope.addLinhaBtnCorrida = false;

    $scope.addTreinoBtnCiclismo = false;
    $scope.salvarTreinoBtnCiclismo = false;
    $scope.delTreinoBtnCiclismo = false;
    $scope.addLinhaBtnCiclismo = false;

    $scope.addTreinoBtnNatacao = false;
    $scope.salvarTreinoBtnNatacao = false;
    $scope.delTreinoBtnNatacao = false;
    $scope.addLinhaBtnNatacao = false;

    $scope.semTreino = false;
    $scope.semTreinoPadrao = false;
    $scope.treino_sexo = '';

    $scope.agendas = [];
    $scope.agenda = {};
    $scope.valida = {};
    $scope.linha = [];

    $scope.treinospadrao = [];
    $scope.treinopadrao = {};

    $scope.treinospadraom = [];
    $scope.treinopadraom = {};

    $scope.treinospadraof = [];
    $scope.treinopadraof = {};

    $scope.treinospadraocrossm = [];
    $scope.treinopadraocrossm = {};
    $scope.treinospadraocrossf = [];
    $scope.treinopadraocrossf = {};

    $scope.treinospadraocorridam = [];
    $scope.treinopadraocorridam = {};
    $scope.treinospadraocorridaf = [];
    $scope.treinopadraocorridaf = {};

    $scope.treinospadraociclismom = [];
    $scope.treinopadraociclismom = {};
    $scope.treinospadraociclismof = [];
    $scope.treinopadraociclismof = {};

    $scope.treinospadraonatacaom = [];
    $scope.treinopadraonatacaom = {};
    $scope.treinospadraonatacaof = [];
    $scope.treinopadraonatacaof = {};



    $scope.fichaspadrao = [];
    $scope.numdias = {};

    $scope.agendado = {};
    $scope.niveis = [];
    $scope.nivel = {};
    $scope.treinox = {};

    $scope.programastreinamento = [];
    $scope.programatreinamento = {};
    $scope.tabsStatus = {
        tab1: true,
        tab2: false,
        tab3: false,
        tab4: false,
        tab5: false,
        tab6: false,
        tab7: false
    };
    
    
    

    $scope.programasaluno = [];
    $scope.programaaluno = {};


    $scope.numdias = 1;


    $scope.selecionaTudo = function ($event) {
        $event.target.select();
    };


    $scope.rodaPreview = function (video) {
        
       $scope.video_preview = video;
       
       $('#modal_preview').modal('toggle');

    }

    $scope.setaCodigo = function (id, tipo) {
        var data = new Date();
        var mes = data.getMonth() + 1;
        var ano = data.getFullYear();
    
        $("#cod").val(id);
        $scope.treino_id_padrao = id;        
        if (tipo=='cross'){
            $scope.treino_id_padrao = id;
        }    
        if (tipo=='corrida'){
            $scope.treino_id_padraocorrida = id;
        }    
        if (tipo=='ciclismo'){
            $scope.treino_id_padraociclismo = id;
        }    
        if (tipo=='natacao'){
            $scope.treino_id_padraonatacao = id;
        }    
        getCalendarioPadrao(tipo,id,mes,ano);



    };


    $scope.medida_duracaosVetor = [];
    for (var i = 1; i <= 5; i++) {
        if (i == 1) {
            $scope.medida_duracao = {
                valor: 'rep',
                texto: 'Rep'
            };
        }
        if (i == 2) {
            $scope.medida_duracao = {
                valor: 'min',
                texto: 'Min'
            };
        }
        if (i == 3) {
            $scope.medida_duracao = {
                valor: 'seg',
                texto: 'Seg'
            };
        }
        if (i == 4) {
            $scope.medida_duracao = {
                valor: 'kmh',
                texto: 'Km/h'
            };
        }
        if (i == 5) {
            $scope.medida_duracao = {
                valor: 'metro',
                texto: 'Metros'
            };
        }

        $scope.medida_duracaosVetor.push($scope.medida_duracao);
    }



    $scope.medida_intensidadeVetor = [];
    for (var i = 1; i <= 4; i++) {
        if (i == 1) {
            $scope.medida_intensidade = {
                valor: 'kg',
                texto: 'Kg'
            };
        }
        if (i == 2) {
            $scope.medida_intensidade = {
                valor: 'kmh',
                texto: 'Km/h'
            };
        }
        if (i == 3) {
            $scope.medida_intensidade = {
                valor: 'grau',
                texto: 'Graus'
            };
        }
        if (i == 4) {
            $scope.medida_intensidade = {
                valor: 'bpm',
                texto: 'Bpm'
            };
        }


        $scope.medida_intensidadeVetor.push($scope.medida_intensidade);
    }

    $scope.super_serieVetor = [];
    for (var i = 1; i <= 3; i++) {
        if (i == 1) {
            $scope.super_serie = {
                valor: 1,
                texto: 'Exercício Simples'
            };
        }
        if (i == 2) {
            $scope.super_serie = {
                valor: 2,
                texto: 'Bi-Série'
            };
        }
        if (i == 3) {
            $scope.super_serie = {
                valor: 3,
                texto: 'Tri-Série'
            };
        }

        $scope.super_serieVetor.push($scope.super_serie);
    }
    
    
    $scope.sexoVetor = [];
    for (var i = 1; i <= 2; i++) {
        if (i == 1) {
            $scope.sexo = {
                valor:  'M',
                texto: 'Masculino'
            };
        }
        if (i == 2) {
            $scope.sexo = {
                valor: 'F',
                texto: 'Feminino'
            };
        }
        $scope.sexoVetor.push($scope.sexo);
    }    

    // ADICIONA NOVA NUTRICAO
    $scope.addNutricaoPadrao = function () {
        // DESATIVA OS BOTOES
        $scope.statusBotoesNutri(true);
        $scope.msgNutricao = 'Aguarde... criando prescrição!';
        $http({
            url: '/admin/api/prescricao/addNutricaoPadrao',
            data: {
                aluno_id: $scope.client_id
            },
            method: "POST"
        }).then(function (response) {
            $scope.newNutricao = response.data;

            $scope.id = response.data.id;
            $scope.nutricao_descricao = '';
            $scope.nutricao_datainicio = '';
            $scope.nutricao_datatermino = '';
            $scope.nutricao_nivel = '';
            $scope.nutricao_padrao = false;

            $scope.nutricoes.push($scope.newNutricao);
            $scope.nutricao_id = $scope.nutricoes[$scope.nutricoes.length - 1];
            $scope.msgNutricao = '';
            // ATIVA OS BOTOES
            $scope.statusBotoesNutri(false);
        }, function (response) {
            console.log('Opsss... Algo deu errado!');
        });
    };

    // MUDA NUTRIÇÃO
    $scope.changeNutricaoPadrao = function (nutricao_id) {
        if (nutricao_id !== undefined) {
            // DESATIVA OS BOTOES
            $scope.statusBotoesNutri(true);
            $scope.msgNutricao = 'Aguarde... carregando prescrição!';
            $http({
                url: '../../api/prescricao/getNutricaoPadrao',
                data: {
                    nutricao_id: nutricao_id
                },
                method: "POST"
            }).then(function (response) {
                $scope.id = response.data.id;
                $scope.nutricao_descricao = response.data.nutricao_descricao;
                $scope.nutricao_datainicio = response.data.nutricao_datainicio;
                $scope.nutricao_datatermino = response.data.nutricao_datatermino;
                $scope.nutricao_nivel = response.data.nutricao_nivel;
                $scope.nutricao_padrao = (response.data.nutricao_padrao === 'S') ? true : false;

                $scope.msgNutricao = '';
                // ATIVA OS BOTÕES
                $scope.statusBotoesNutri(false);
            }, function (response) {
                $scope.msgNutricao = '';
                console.log('Opsss... Algo deu errado!');
            });
        }
    };

    // DELETA NUTRIÇÃO
    $scope.deleteNutricaoPadrao = function (nutricao_id) {
        if (nutricao_id !== undefined) {
            swal({
title: "Tem certeza?",
                text: "Tem certeza que deseja deletar essa prescrição?",
  icon: "warning",
  buttons: true,
  buttons: {
      cancel: {
        text: "Cancelar",
        value: null,
        visible: true,
        className: "",
        closeModal: true,
      },
      confirm: {
        text: "Sim, tenho certeza!",
        value: true,
        visible: true,
        className: "",
        closeModal: true
      }},
  dangerMode: true,

}).then((willDelete) => {
  if (willDelete) {
    // aqui chama o ajax
    $http({
                    url: '../../api/prescricao/delNutricaoPadrao/' + nutricao_id,
                    method: "POST"
                }).then(function (response) {
                    $scope.listaNutricoesAluno();
                    swal("Deletado!", "Prescrição deletada com sucesso!", "success");
                });
  } else {
    // swal("Your imaginary file is safe!");
  }
});
// fim swal
// 
           
        } else {
            swal("Opsss!", "Selecione uma prescrição para deletar!", "error");
        }
    };

    // ATUALIZA TREINO
    $scope.salvarNutricaoPadrao = function () {
        if ($scope.id !== undefined) {
            // DESATIVA OS BOTOES
            $scope.statusBotoesNutri(true);
            $http({
                url: '../admin/api/prescricao/upNutricaoPadrao',
                data: {
                    nutricao_id: $scope.id,
                    nutricao_descricao: $scope.nutricao_descricao,
                    nutricao_datainicio: $scope.nutricao_datainicio,
                    nutricao_datatermino: $scope.nutricao_datatermino,
                    nutricao_nivel: $scope.nutricao_nivel,
                    nutricao_padrao: $scope.nutricao_padrao,
                },
                method: "POST"
            }).then(function (response) {
                swal('Sucesso!', 'Prescrição salva com sucesso!', 'success');
                // ATIVA OS BOTOES
                $scope.statusBotoesNutri(false);
                $scope.changeNutricao($scope.id);
            }, function (response) {
                console.log('Opsss... Algo deu errado!');
            });
        } else {
            swal("Opsss!", "Selecione ou crie uma prescrição antes de salvar!", "error");
        }
    };

    $scope.statusBotoesNutri = function (sts) {
        $scope.addNutricaoBtn = sts;
        $scope.salvarNutricaoBtn = sts;
        $scope.delNutricaoBtn = sts;
    };
    // FECHA NUTRIÇÃO




    $scope.gerarCode = function (letra, seq) {
        var data = new Date();
        var dia = data.getDate();
        var mes = data.getMonth() + 1;
        var ano = data.getFullYear();
        var hora = data.getHours();
        var minuto = data.getMinutes();
        var segundo = data.getSeconds();
        var milisec = data.getMilliseconds();

        var hash_code = [ano, mes, dia, hora, minuto, segundo, milisec, letra, seq].join('');
        return hash_code;
    };

    $scope.AtualizaPosicoesA = function (scopo) {
        $scope.linhasAmemo = $scope.linhasA;
        var rows = $('#abaAaa tbody >tr.ordena-treino');
        var columns = "";
        $scope.linhasA = [];
        $scope.superA = [];



        for (var i = 0; i < rows.length; i++) {
            columns = $(rows[i]).find('td');


            for (var k = 0; k < $scope.linhasAmemo.length; k++) {

                console.log($(columns[9]).text() + '-' + $scope.linhasAmemo[k].hash_code);
                if ($(columns[9]).text() == $scope.linhasAmemo[k].hash_code) {
                    $scope.superA = [];

                    for (var j = 0; j < $scope.linhasAmemo[k].superserie.length; j++) {
                        $scope.super = {
                            id: j + 1,
                            ficha: $scope.linhasAmemo[k].superserie[j].ficha,
                            letra: 'A',
                            exercicio: $scope.linhasAmemo[k].superserie[j].exercicio,
                            intervalo: $scope.linhasAmemo[k].superserie[j].intervalo,
                            series: $scope.linhasAmemo[k].superserie[j].series,
                            repeticoes: $scope.linhasAmemo[k].superserie[j].repeticoes,
                            medida_duracao: $scope.linhasAmemo[k].superserie[j].medida_duracao,
                            carga: $scope.linhasAmemo[k].superserie[j].null,
                            medida_intensidade: $scope.linhasAmemo[k].superserie[j].medida_intensidade,
                            ordem: 0,
                            observacao: $scope.linhasAmemo[k].superserie[j].observacao,
                            super_serie: $scope.linhasAmemo[k].superserie[j].superserie,
                            seq: 0
                        };
                        console.log(' ordem ' + $scope.super.repeticoes);
                        $scope.superA.push($scope.super);
                    }
                }
            }


            $scope.insertK = {
                exercicio: parseInt($(columns[1]).text()),
                ordem: i + 1,
                id: i + 1,
                ficha: $(columns[0]).text(),
                letra: 'A',
                series: $(columns[2]).text(),
                repeticoes: $(columns[3]).text(),
                carga: $(columns[5]).text(),
                intervalo: $(columns[7]).text(),
                observacao: $(columns[10]).text(),
                medida_duracao: $(columns[4]).text(),
                medida_intensidade: $(columns[6]).text(),
                super_serie: parseInt($(columns[8]).text()),
                hash_code: $(columns[9]).text(),
                superserie: $scope.superA

            };

            $scope.linhasA.push($scope.insertK);


        }
        $scope.linhasA.push(scopo);

        $timeout(function () {
            for (var i = 0; i <= $scope.linhasA.length; i++) {
                $('#execicioSelectA' + i).addClass('chosen-select');
                $('.chosen-select').chosen({width: "100%"});

            }
        }, 500);

    };

    $scope.AtualizaPosicoesB = function (scopo) {
        $scope.linhasBmemo = $scope.linhasB;
        var rows = $('#abaBbb tbody >tr.ordena-treino');
        var columns = "";
        $scope.linhasB = [];
        $scope.superB = [];



        for (var i = 0; i < rows.length; i++) {
            columns = $(rows[i]).find('td');


            for (var k = 0; k < $scope.linhasBmemo.length; k++) {
                if ($(columns[9]).text() == $scope.linhasBmemo[k].hash_code) {
                    $scope.superB = [];

                    for (var j = 0; j < $scope.linhasBmemo[k].superserie.length; j++) {
                        $scope.super = {
                            id: j + 1,
                            ficha: $scope.linhasBmemo[k].superserie[j].ficha,
                            letra: 'B',
                            exercicio: $scope.linhasBmemo[k].superserie[j].exercicio,
                            intervalo: $scope.linhasBmemo[k].superserie[j].intervalo,
                            series: $scope.linhasBmemo[k].superserie[j].series,
                            repeticoes: $scope.linhasBmemo[k].superserie[j].repeticoes,
                            medida_duracao: $scope.linhasBmemo[k].superserie[j].medida_duracao,
                            carga: $scope.linhasBmemo[k].superserie[j].null,
                            medida_intensidade: $scope.linhasBmemo[k].superserie[j].medida_intensidade,
                            ordem: 0,
                            observacao: $scope.linhasBmemo[k].superserie[j].observacao,
                            super_serie: $scope.linhasBmemo[k].superserie[j].superserie,
                            seq: 0
                        };
                        $scope.superB.push($scope.super);
                    }
                }
            }


            $scope.insertK = {
                exercicio: parseInt($(columns[1]).text()),
                ordem: i + 1,
                id: i + 1,
                ficha: $(columns[0]).text(),
                letra: 'B',
                series: $(columns[2]).text(),
                repeticoes: $(columns[3]).text(),
                carga: $(columns[5]).text(),
                intervalo: $(columns[7]).text(),
                observacao: $(columns[10]).text(),
                medida_duracao: $(columns[4]).text(),
                medida_intensidade: $(columns[6]).text(),
                super_serie: parseInt($(columns[8]).text()),
                hash_code: $(columns[9]).text(),
                superserie: $scope.superB

            };

            $scope.linhasB.push($scope.insertK);


        }
        $scope.linhasB.push(scopo);

        $timeout(function () {
            for (var i = 0; i <= $scope.linhasA.length; i++) {
                $('#execicioSelectA' + i).addClass('chosen-select');
                $('.chosen-select').chosen({width: "100%"});
            }
        }, 500);

    };
    $scope.AtualizaPosicoesC = function (scopo) {
        $scope.linhasCmemo = $scope.linhasC;
        var rows = $('#abaCcc tbody >tr.ordena-treino');
        var columns = "";
        $scope.linhasC = [];
        $scope.superC = [];



        for (var i = 0; i < rows.length; i++) {
            columns = $(rows[i]).find('td');


            for (var k = 0; k < $scope.linhasCmemo.length; k++) {
                if ($(columns[9]).text() == $scope.linhasCmemo[k].hash_code) {
                    $scope.superC = [];

                    for (var j = 0; j < $scope.linhasCmemo[k].superserie.length; j++) {
                        $scope.super = {
                            id: j + 1,
                            ficha: $scope.linhasCmemo[k].superserie[j].ficha,
                            letra: 'C',
                            exercicio: $scope.linhasCmemo[k].superserie[j].exercicio,
                            intervalo: $scope.linhasCmemo[k].superserie[j].intervalo,
                            series: $scope.linhasCmemo[k].superserie[j].series,
                            repeticoes: $scope.linhasCmemo[k].superserie[j].repeticoes,
                            medida_duracao: $scope.linhasCmemo[k].superserie[j].medida_duracao,
                            carga: $scope.linhasCmemo[k].superserie[j].null,
                            medida_intensidade: $scope.linhasCmemo[k].superserie[j].medida_intensidade,
                            ordem: 0,
                            observacao: $scope.linhasCmemo[k].superserie[j].observacao,
                            super_serie: $scope.linhasCmemo[k].superserie[j].superserie,
                            seq: 0
                        };
                        $scope.superC.push($scope.super);
                    }
                }
            }


            $scope.insertK = {
                exercicio: parseInt($(columns[1]).text()),
                ordem: i + 1,
                id: i + 1,
                ficha: $(columns[0]).text(),
                letra: 'C',
                series: $(columns[2]).text(),
                repeticoes: $(columns[3]).text(),
                carga: $(columns[5]).text(),
                intervalo: $(columns[7]).text(),
                observacao: $(columns[10]).text(),
                medida_duracao: $(columns[4]).text(),
                medida_intensidade: $(columns[6]).text(),
                super_serie: parseInt($(columns[8]).text()),
                hash_code: $(columns[9]).text(),
                superserie: $scope.superC

            };

            $scope.linhasC.push($scope.insertK);


        }
        $scope.linhasC.push(scopo);

        $timeout(function () {
            for (var i = 0; i <= $scope.linhasC.length; i++) {
                $('#execicioSelectC' + i).addClass('chosen-select');
                $('.chosen-select').chosen({width: "100%"});
            }
        }, 500);

    };
    $scope.AtualizaPosicoesD = function (scopo) {
        $scope.linhasDmemo = $scope.linhasD;
        var rows = $('#abaDdd tbody >tr.ordena-treino');
        var columns = "";
        $scope.linhasD = [];
        $scope.superD = [];



        for (var i = 0; i < rows.length; i++) {
            columns = $(rows[i]).find('td');


            for (var k = 0; k < $scope.linhasDmemo.length; k++) {
                if ($(columns[9]).text() == $scope.linhasDmemo[k].hash_code) {
                    $scope.superD = [];

                    for (var j = 0; j < $scope.linhasDmemo[k].superserie.length; j++) {
                        $scope.super = {
                            id: j + 1,
                            ficha: $scope.linhasDmemo[k].superserie[j].ficha,
                            letra: 'D',
                            exercicio: $scope.linhasDmemo[k].superserie[j].exercicio,
                            intervalo: $scope.linhasDmemo[k].superserie[j].intervalo,
                            series: $scope.linhasDmemo[k].superserie[j].series,
                            repeticoes: $scope.linhasDmemo[k].superserie[j].repeticoes,
                            medida_duracao: $scope.linhasDmemo[k].superserie[j].medida_duracao,
                            carga: $scope.linhasDmemo[k].superserie[j].null,
                            medida_intensidade: $scope.linhasDmemo[k].superserie[j].medida_intensidade,
                            ordem: 0,
                            observacao: $scope.linhasDmemo[k].superserie[j].observacao,
                            super_serie: $scope.linhasDmemo[k].superserie[j].superserie,
                            seq: 0
                        };
                        $scope.superD.push($scope.super);
                    }
                }
            }


            $scope.insertK = {
                exercicio: parseInt($(columns[1]).text()),
                ordem: i + 1,
                id: i + 1,
                ficha: $(columns[0]).text(),
                letra: 'D',
                series: $(columns[2]).text(),
                repeticoes: $(columns[3]).text(),
                carga: $(columns[5]).text(),
                intervalo: $(columns[7]).text(),
                observacao: $(columns[10]).text(),
                medida_duracao: $(columns[4]).text(),
                medida_intensidade: $(columns[6]).text(),
                super_serie: parseInt($(columns[8]).text()),
                hash_code: $(columns[9]).text(),
                superserie: $scope.superD

            };

            $scope.linhasD.push($scope.insertK);


        }
        $scope.linhasD.push(scopo);

        $timeout(function () {
            for (var i = 0; i <= $scope.linhasD.length; i++) {
                $('#execicioSelectD' + i).addClass('chosen-select');
                $('.chosen-select').chosen({width: "100%"});
            }
        }, 500);

    };
    $scope.AtualizaPosicoesE = function (scopo) {
        $scope.linhasEmemo = $scope.linhasE;
        var rows = $('#abaEee tbody >tr.ordena-treino');
        var columns = "";
        $scope.linhasE = [];
        $scope.superE = [];



        for (var i = 0; i < rows.length; i++) {
            columns = $(rows[i]).find('td');


            for (var k = 0; k < $scope.linhasEmemo.length; k++) {
                if ($(columns[9]).text() == $scope.linhasEmemo[k].hash_code) {
                    $scope.superE = [];

                    for (var j = 0; j < $scope.linhasEmemo[k].superserie.length; j++) {
                        $scope.super = {
                            id: j + 1,
                            ficha: $scope.linhasEmemo[k].superserie[j].ficha,
                            letra: 'E',
                            exercicio: $scope.linhasEmemo[k].superserie[j].exercicio,
                            intervalo: $scope.linhasEmemo[k].superserie[j].intervalo,
                            series: $scope.linhasEmemo[k].superserie[j].series,
                            repeticoes: $scope.linhasEmemo[k].superserie[j].repeticoes,
                            medida_duracao: $scope.linhasEmemo[k].superserie[j].medida_duracao,
                            carga: $scope.linhasEmemo[k].superserie[j].null,
                            medida_intensidade: $scope.linhasEmemo[k].superserie[j].medida_intensidade,
                            ordem: 0,
                            observacao: $scope.linhasEmemo[k].superserie[j].observacao,
                            super_serie: $scope.linhasEmemo[k].superserie[j].superserie,
                            seq: 0
                        };
                        $scope.superE.push($scope.super);
                    }
                }
            }


            $scope.insertK = {
                exercicio: parseInt($(columns[1]).text()),
                ordem: i + 1,
                id: i + 1,
                ficha: $(columns[0]).text(),
                letra: 'E',
                series: $(columns[2]).text(),
                repeticoes: $(columns[3]).text(),
                carga: $(columns[5]).text(),
                intervalo: $(columns[7]).text(),
                observacao: $(columns[10]).text(),
                medida_duracao: $(columns[4]).text(),
                medida_intensidade: $(columns[6]).text(),
                super_serie: parseInt($(columns[8]).text()),
                hash_code: $(columns[9]).text(),
                superserie: $scope.superE

            };

            $scope.linhasE.push($scope.insertK);


        }
        $scope.linhasE.push(scopo);

        $timeout(function () {
            for (var i = 0; i <= $scope.linhasE.length; i++) {
                $('#execicioSelectE' + i).addClass('chosen-select');
                $('.chosen-select').chosen({width: "100%"});
            }
        }, 500);

    };
    $scope.AtualizaPosicoesF = function (scopo) {
        $scope.linhasFmemo = $scope.linhasF;
        var rows = $('#abaFff tbody >tr.ordena-treino');
        var columns = "";
        $scope.linhasF = [];
        $scope.superF = [];



        for (var i = 0; i < rows.length; i++) {
            columns = $(rows[i]).find('td');


            for (var k = 0; k < $scope.linhasFmemo.length; k++) {
                if ($(columns[9]).text() == $scope.linhasFmemo[k].hash_code) {
                    $scope.superF = [];

                    for (var j = 0; j < $scope.linhasFmemo[k].superserie.length; j++) {
                        $scope.super = {
                            id: j + 1,
                            ficha: $scope.linhasFmemo[k].superserie[j].ficha,
                            letra: 'F',
                            exercicio: $scope.linhasFmemo[k].superserie[j].exercicio,
                            intervalo: $scope.linhasFmemo[k].superserie[j].intervalo,
                            series: $scope.linhasEmemo[k].superserie[j].series,
                            repeticoes: $scope.linhasFmemo[k].superserie[j].repeticoes,
                            medida_duracao: $scope.linhasFmemo[k].superserie[j].medida_duracao,
                            carga: $scope.linhasFmemo[k].superserie[j].null,
                            medida_intensidade: $scope.linhasFmemo[k].superserie[j].medida_intensidade,
                            ordem: 0,
                            observacao: $scope.linhasFmemo[k].superserie[j].observacao,
                            super_serie: $scope.linhasFmemo[k].superserie[j].superserie,
                            seq: 0
                        };
                        $scope.superF.push($scope.super);
                    }
                }
            }


            $scope.insertK = {
                exercicio: parseInt($(columns[1]).text()),
                ordem: i + 1,
                id: i + 1,
                ficha: $(columns[0]).text(),
                letra: 'F',
                series: $(columns[2]).text(),
                repeticoes: $(columns[3]).text(),
                carga: $(columns[5]).text(),
                intervalo: $(columns[7]).text(),
                observacao: $(columns[10]).text(),
                medida_duracao: $(columns[4]).text(),
                medida_intensidade: $(columns[6]).text(),
                super_serie: parseInt($(columns[8]).text()),
                hash_code: $(columns[9]).text(),
                superserie: $scope.superF

            };

            $scope.linhasF.push($scope.insertK);


        }
        $scope.linhasF.push(scopo);

        $timeout(function () {
            for (var i = 0; i <= $scope.linhasF.length; i++) {
                $('#execicioSelectF' + i).addClass('chosen-select');
                $('.chosen-select').chosen({width: "100%"});
            }
        }, 500);

    };
    $scope.AtualizaPosicoesG = function (scopo) {
        $scope.linhasGmemo = $scope.linhasG;
        var rows = $('#abaGgg tbody >tr.ordena-treino');
        var columns = "";
        $scope.linhasG = [];
        $scope.superG = [];



        for (var i = 0; i < rows.length; i++) {
            columns = $(rows[i]).find('td');

            for (var k = 0; k < $scope.linhasGmemo.length; k++) {
                if ($(columns[9]).text() == $scope.linhasGmemo[k].hash_code) {
                    $scope.superG = [];

                    for (var j = 0; j < $scope.linhasGmemo[k].superserie.length; j++) {
                        $scope.super = {
                            id: j + 1,
                            ficha: $scope.linhasGmemo[k].superserie[j].ficha,
                            letra: 'G',
                            exercicio: $scope.linhasGmemo[k].superserie[j].exercicio,
                            intervalo: $scope.linhasGmemo[k].superserie[j].intervalo,
                            series: $scope.linhasGmemo[k].superserie[j].series,
                            repeticoes: $scope.linhasGmemo[k].superserie[j].repeticoes,
                            medida_duracao: $scope.linhasGmemo[k].superserie[j].medida_duracao,
                            carga: $scope.linhasGmemo[k].superserie[j].null,
                            medida_intensidade: $scope.linhasGmemo[k].superserie[j].medida_intensidade,
                            ordem: 0,
                            observacao: $scope.linhasGmemo[k].superserie[j].observacao,
                            super_serie: $scope.linhasGmemo[k].superserie[j].superserie,
                            seq: 0
                        };
                        $scope.superG.push($scope.super);
                    }
                }
            }

            $scope.insertK = {
                exercicio: parseInt($(columns[1]).text()),
                ordem: i + 1,
                id: i + 1,
                ficha: $(columns[0]).text(),
                letra: 'F',
                series: $(columns[2]).text(),
                repeticoes: $(columns[3]).text(),
                carga: $(columns[5]).text(),
                intervalo: $(columns[7]).text(),
                observacao: $(columns[10]).text(),
                medida_duracao: $(columns[4]).text(),
                medida_intensidade: $(columns[6]).text(),
                super_serie: parseInt($(columns[8]).text()),
                hash_code: $(columns[9]).text(),
                superserie: $scope.superG

            };

            $scope.linhasG.push($scope.insertK);


        }
        $scope.linhasG.push(scopo);

        $timeout(function () {
            for (var i = 0; i <= $scope.linhasF.length; i++) {
                $('#execicioSelectF' + i).addClass('chosen-select');
                $('.chosen-select').chosen({width: "100%"});
            }
        }, 500);

    };



    $scope.AtualizaPosicoesASave = function () {

        $scope.linhasAmemo = $scope.linhasA;
        var rows = $('#abaAaa tbody >tr.ordena-treino');
        var columns = "";
        $scope.linhasA = [];
        $scope.superA = [];

        for (var i = 0; i < rows.length; i++) {
            columns = $(rows[i]).find('td');
            for (var k = 0; k < $scope.linhasAmemo.length; k++) {

                if ($(columns[9]).text() == $scope.linhasAmemo[k].hash_code) {
                    $scope.superA = [];
                    for (var j = 0; j < $scope.linhasAmemo[k].superserie.length; j++) {
                        $scope.super = {
                            id: j + 1,
                            ficha: $scope.linhasAmemo[k].superserie[j].ficha,
                            letra: 'A',
                            exercicio: $scope.linhasAmemo[k].superserie[j].exercicio,
                            intervalo: $scope.linhasAmemo[k].superserie[j].intervalo,
                            series: $scope.linhasAmemo[k].superserie[j].series,
                            repeticoes: $scope.linhasAmemo[k].superserie[j].repeticoes,
                            medida_duracao: $scope.linhasAmemo[k].superserie[j].medida_duracao,
                            carga: $scope.linhasAmemo[k].superserie[j].null,
                            medida_intensidade: $scope.linhasAmemo[k].superserie[j].medida_intensidade,
                            ordem: 0,
                            observacao: $scope.linhasAmemo[k].superserie[j].observacao,
                            super_serie: $scope.linhasAmemo[k].superserie[j].superserie,
                            seq: 0
                        };
                        $scope.superA.push($scope.super);
                    }
                }
            }

            $scope.insertK = {
                exercicio: parseInt($(columns[1]).text()),
                ordem: i + 1,
                id: i + 1,
                ficha: $(columns[0]).text(),
                letra: 'A',
                series: $(columns[2]).text(),
                repeticoes: $(columns[3]).text(),
                medida_duracao: $(columns[4]).text(),
                carga: $(columns[5]).text(),
                medida_intensidade: $(columns[6]).text(),
                intervalo: $(columns[7]).text(),
                super_serie: parseInt($(columns[8]).text()),
                hash_code: $(columns[9]).text(),
                superserie: $scope.superA,
                observacao: $(columns[10]).text()
            };
            $scope.linhasA.push($scope.insertK);


        }



    };
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    $scope.AtualizaPosicoesBSave = function () {

        $scope.linhasBmemo = $scope.linhasB;
        var rows = $('#abaBbb tbody >tr.ordena-treino');
        var columns = "";
        $scope.linhasB = [];
        $scope.superB = [];

        for (var i = 0; i < rows.length; i++) {
            columns = $(rows[i]).find('td');
            for (var k = 0; k < $scope.linhasBmemo.length; k++) {
                if ($(columns[9]).text() == $scope.linhasBmemo[k].hash_code) {
                    $scope.superB = [];
                    for (var j = 0; j < $scope.linhasBmemo[k].superserie.length; j++) {
                        $scope.super = {
                            id: j + 1,
                            ficha: $scope.linhasBmemo[k].superserie[j].ficha,
                            letra: 'B',
                            exercicio: $scope.linhasBmemo[k].superserie[j].exercicio,
                            intervalo: $scope.linhasBmemo[k].superserie[j].intervalo,
                            series: $scope.linhasBmemo[k].superserie[j].series,
                            repeticoes: $scope.linhasBmemo[k].superserie[j].repeticoes,
                            medida_duracao: $scope.linhasBmemo[k].superserie[j].medida_duracao,
                            carga: $scope.linhasBmemo[k].superserie[j].null,
                            medida_intensidade: $scope.linhasBmemo[k].superserie[j].medida_intensidade,
                            ordem: 0,
                            observacao: $scope.linhasBmemo[k].superserie[j].observacao,
                            super_serie: $scope.linhasBmemo[k].superserie[j].superserie,
                            seq: 0
                        };
                        $scope.superB.push($scope.super);
                    }
                }
            }

            $scope.insertK = {
                exercicio: parseInt($(columns[1]).text()),
                ordem: i + 1,
                id: i + 1,
                ficha: $(columns[0]).text(),
                letra: 'B',
                series: $(columns[2]).text(),
                repeticoes: $(columns[3]).text(),
                medida_duracao: $(columns[4]).text(),
                carga: $(columns[5]).text(),
                medida_intensidade: $(columns[6]).text(),
                intervalo: $(columns[7]).text(),
                super_serie: parseInt($(columns[8]).text()),
                hash_code: $(columns[9]).text(),
                superserie: $scope.superB,
                observacao: $(columns[10]).text()
            };
            $scope.linhasB.push($scope.insertK);


        }



    };
    $scope.AtualizaPosicoesCSave = function () {

        $scope.linhasCmemo = $scope.linhasC;
        var rows = $('#abaCcc tbody >tr.ordena-treino');
        var columns = "";
        $scope.linhasC = [];
        $scope.superC = [];

        for (var i = 0; i < rows.length; i++) {
            columns = $(rows[i]).find('td');
            for (var k = 0; k < $scope.linhasCmemo.length; k++) {
                if ($(columns[9]).text() == $scope.linhasCmemo[k].hash_code) {
                    $scope.superC = [];
                    for (var j = 0; j < $scope.linhasCmemo[k].superserie.length; j++) {
                        $scope.super = {
                            id: j + 1,
                            ficha: $scope.linhasCmemo[k].superserie[j].ficha,
                            letra: 'C',
                            exercicio: $scope.linhasCmemo[k].superserie[j].exercicio,
                            intervalo: $scope.linhasCmemo[k].superserie[j].intervalo,
                            series: $scope.linhasCmemo[k].superserie[j].series,
                            repeticoes: $scope.linhasCmemo[k].superserie[j].repeticoes,
                            medida_duracao: $scope.linhasCmemo[k].superserie[j].medida_duracao,
                            carga: $scope.linhasCmemo[k].superserie[j].null,
                            medida_intensidade: $scope.linhasCmemo[k].superserie[j].medida_intensidade,
                            ordem: 0,
                            observacao: $scope.linhasCmemo[k].superserie[j].observacao,
                            super_serie: $scope.linhasCmemo[k].superserie[j].superserie,
                            seq: 0
                        };
                        $scope.superC.push($scope.super);
                    }
                }
            }

            $scope.insertK = {
                exercicio: parseInt($(columns[1]).text()),
                ordem: i + 1,
                id: i + 1,
                ficha: $(columns[0]).text(),
                letra: 'C',
                series: $(columns[2]).text(),
                repeticoes: $(columns[3]).text(),
                medida_duracao: $(columns[4]).text(),
                carga: $(columns[5]).text(),
                medida_intensidade: $(columns[6]).text(),
                intervalo: $(columns[7]).text(),
                super_serie: parseInt($(columns[8]).text()),
                hash_code: $(columns[9]).text(),
                superserie: $scope.superC,
                observacao: $(columns[10]).text()
            };
            $scope.linhasC.push($scope.insertK);


        }



    };
    $scope.AtualizaPosicoesDSave = function () {
        $scope.linhasDmemo = $scope.linhasD;
        var rows = $('#abaDdd tbody >tr.ordena-treino');
        var columns = "";
        $scope.linhasD = [];
        $scope.superD = [];

        for (var i = 0; i < rows.length; i++) {
            columns = $(rows[i]).find('td');
            for (var k = 0; k < $scope.linhasDmemo.length; k++) {
                if ($(columns[9]).text() == $scope.linhasDmemo[k].hash_code) {
                    $scope.superD = [];
                    for (var j = 0; j < $scope.linhasDmemo[k].superserie.length; j++) {
                        $scope.super = {
                            id: j + 1,
                            ficha: $scope.linhasDmemo[k].superserie[j].ficha,
                            letra: 'D',
                            exercicio: $scope.linhasDmemo[k].superserie[j].exercicio,
                            intervalo: $scope.linhasDmemo[k].superserie[j].intervalo,
                            series: $scope.linhasDmemo[k].superserie[j].series,
                            repeticoes: $scope.linhasDmemo[k].superserie[j].repeticoes,
                            medida_duracao: $scope.linhasDmemo[k].superserie[j].medida_duracao,
                            carga: $scope.linhasDmemo[k].superserie[j].null,
                            medida_intensidade: $scope.linhasDmemo[k].superserie[j].medida_intensidade,
                            ordem: 0,
                            observacao: $scope.linhasDmemo[k].superserie[j].observacao,
                            super_serie: $scope.linhasDmemo[k].superserie[j].superserie,
                            seq: 0
                        };
                        $scope.superD.push($scope.super);
                    }
                }
            }

            $scope.insertK = {
                exercicio: parseInt($(columns[1]).text()),
                ordem: i + 1,
                id: i + 1,
                ficha: $(columns[0]).text(),
                letra: 'D',
                series: $(columns[2]).text(),
                repeticoes: $(columns[3]).text(),
                medida_duracao: $(columns[4]).text(),
                carga: $(columns[5]).text(),
                medida_intensidade: $(columns[6]).text(),
                intervalo: $(columns[7]).text(),
                super_serie: parseInt($(columns[8]).text()),
                hash_code: $(columns[9]).text(),
                superserie: $scope.superD,
                observacao: $(columns[10]).text()
            };
            $scope.linhasD.push($scope.insertK);


        }


    };
    $scope.AtualizaPosicoesESave = function () {
        $scope.linhasEmemo = $scope.linhasE;
        var rows = $('#abaEee tbody >tr.ordena-treino');
        var columns = "";
        $scope.linhasE = [];
        $scope.superE = [];

        for (var i = 0; i < rows.length; i++) {
            columns = $(rows[i]).find('td');
            for (var k = 0; k < $scope.linhasEmemo.length; k++) {
                if ($(columns[9]).text() == $scope.linhasEmemo[k].hash_code) {
                    $scope.superE = [];
                    for (var j = 0; j < $scope.linhasEmemo[k].superserie.length; j++) {
                        $scope.super = {
                            id: j + 1,
                            ficha: $scope.linhasEmemo[k].superserie[j].ficha,
                            letra: 'E',
                            exercicio: $scope.linhasEmemo[k].superserie[j].exercicio,
                            intervalo: $scope.linhasEmemo[k].superserie[j].intervalo,
                            series: $scope.linhasEmemo[k].superserie[j].series,
                            repeticoes: $scope.linhasEmemo[k].superserie[j].repeticoes,
                            medida_duracao: $scope.linhasEmemo[k].superserie[j].medida_duracao,
                            carga: $scope.linhasEmemo[k].superserie[j].null,
                            medida_intensidade: $scope.linhasEmemo[k].superserie[j].medida_intensidade,
                            ordem: 0,
                            observacao: $scope.linhasEmemo[k].superserie[j].observacao,
                            super_serie: $scope.linhasEmemo[k].superserie[j].superserie,
                            seq: 0
                        };
                        $scope.superE.push($scope.super);
                    }
                }
            }

            $scope.insertK = {
                exercicio: parseInt($(columns[1]).text()),
                ordem: i + 1,
                id: i + 1,
                ficha: $(columns[0]).text(),
                letra: 'E',
                series: $(columns[2]).text(),
                repeticoes: $(columns[3]).text(),
                medida_duracao: $(columns[4]).text(),
                carga: $(columns[5]).text(),
                medida_intensidade: $(columns[6]).text(),
                intervalo: $(columns[7]).text(),
                super_serie: parseInt($(columns[8]).text()),
                hash_code: $(columns[9]).text(),
                superserie: $scope.superE,
                observacao: $(columns[10]).text()
            };
            $scope.linhasE.push($scope.insertK);


        }


    };

    $scope.AtualizaPosicoesFSave = function () {
        $scope.linhasFmemo = $scope.linhasF;
        var rows = $('#abaFff tbody >tr.ordena-treino');
        var columns = "";
        $scope.linhasF = [];
        $scope.superF = [];

        for (var i = 0; i < rows.length; i++) {
            columns = $(rows[i]).find('td');
            for (var k = 0; k < $scope.linhasFmemo.length; k++) {
                if ($(columns[9]).text() == $scope.linhasFmemo[k].hash_code) {
                    $scope.superF = [];
                    for (var j = 0; j < $scope.linhasFmemo[k].superserie.length; j++) {
                        $scope.super = {
                            id: j + 1,
                            ficha: $scope.linhasFmemo[k].superserie[j].ficha,
                            letra: 'F',
                            exercicio: $scope.linhasFmemo[k].superserie[j].exercicio,
                            intervalo: $scope.linhasFmemo[k].superserie[j].intervalo,
                            series: $scope.linhasFmemo[k].superserie[j].series,
                            repeticoes: $scope.linhasFmemo[k].superserie[j].repeticoes,
                            medida_duracao: $scope.linhasFmemo[k].superserie[j].medida_duracao,
                            carga: $scope.linhasFmemo[k].superserie[j].null,
                            medida_intensidade: $scope.linhasFmemo[k].superserie[j].medida_intensidade,
                            ordem: 0,
                            observacao: $scope.linhasFmemo[k].superserie[j].observacao,
                            super_serie: $scope.linhasFmemo[k].superserie[j].superserie,
                            seq: 0
                        };
                        $scope.superF.push($scope.super);
                    }
                }
            }

            $scope.insertK = {
                exercicio: parseInt($(columns[1]).text()),
                ordem: i + 1,
                id: i + 1,
                ficha: $(columns[0]).text(),
                letra: 'F',
                series: $(columns[2]).text(),
                repeticoes: $(columns[3]).text(),
                medida_duracao: $(columns[4]).text(),
                carga: $(columns[5]).text(),
                medida_intensidade: $(columns[6]).text(),
                intervalo: $(columns[7]).text(),
                super_serie: parseInt($(columns[8]).text()),
                hash_code: $(columns[9]).text(),
                superserie: $scope.superF,
                observacao: $(columns[10]).text()
            };
            $scope.linhasF.push($scope.insertK);


        }


    };


    $scope.AtualizaPosicoesGSave = function () {
        $scope.linhasGmemo = $scope.linhasG;
        var rows = $('#abaGgg tbody >tr.ordena-treino');
        var columns = "";
        $scope.linhasG = [];
        $scope.superG = [];

        for (var i = 0; i < rows.length; i++) {
            columns = $(rows[i]).find('td');
            for (var k = 0; k < $scope.linhasGmemo.length; k++) {
                if ($(columns[9]).text() == $scope.linhasGmemo[k].hash_code) {
                    $scope.superG = [];
                    for (var j = 0; j < $scope.linhasGmemo[k].superserie.length; j++) {
                        $scope.super = {
                            id: j + 1,
                            ficha: $scope.linhasGmemo[k].superserie[j].ficha,
                            letra: 'G',
                            exercicio: $scope.linhasGmemo[k].superserie[j].exercicio,
                            intervalo: $scope.linhasGmemo[k].superserie[j].intervalo,
                            series: $scope.linhasGmemo[k].superserie[j].series,
                            repeticoes: $scope.linhasGmemo[k].superserie[j].repeticoes,
                            medida_duracao: $scope.linhasGmemo[k].superserie[j].medida_duracao,
                            carga: $scope.linhasGmemo[k].superserie[j].null,
                            medida_intensidade: $scope.linhasGmemo[k].superserie[j].medida_intensidade,
                            ordem: 0,
                            observacao: $scope.linhasGmemo[k].superserie[j].observacao,
                            super_serie: $scope.linhasGmemo[k].superserie[j].superserie,
                            seq: 0
                        };
                        $scope.superG.push($scope.super);
                    }
                }
            }

            $scope.insertK = {
                exercicio: parseInt($(columns[1]).text()),
                ordem: i + 1,
                id: i + 1,
                ficha: $(columns[0]).text(),
                letra: 'G',
                series: $(columns[2]).text(),
                repeticoes: $(columns[3]).text(),
                medida_duracao: $(columns[4]).text(),
                carga: $(columns[5]).text(),
                medida_intensidade: $(columns[6]).text(),
                intervalo: $(columns[7]).text(),
                super_serie: parseInt($(columns[8]).text()),
                hash_code: $(columns[9]).text(),
                superserie: $scope.superG,
                observacao: $(columns[10]).text()
            };
            $scope.linhasG.push($scope.insertK);


        }


    };

    $http({
        url: '../../api/grupomuscular',
        method: "GET"
    }).then(function (response) {
        $scope.grupos = response.data;
    }, function (response) {
        console.log('Opsss... Algo deu errado na busca dos objetivos!');
    });

    $http({
        url: '../api/prescricao/getObjetivos/3',
        method: "POST"
    }).then(function (response) {
        $scope.objetivosNatacao = response.data;
    }, function (response) {
        console.log('Opsss... Algo deu errado na busca dos objetivos!');
    });    

    $http({
        url: '../api/prescricao/getObjetivos/4',
        method: "POST"
    }).then(function (response) {
        $scope.objetivosCross = response.data;
    }, function (response) {
        console.log('Opsss... Algo deu errado na busca dos objetivos!');
    });

    $http({
        url: '../api/prescricao/getObjetivos/5',
        method: "POST"
    }).then(function (response) {
        $scope.objetivosCorrida = response.data;
    }, function (response) {
        console.log('Opsss... Algo deu errado na busca dos objetivos!');
    });
    
    $http({
        url: '../api/prescricao/getObjetivos/6',
        method: "POST"
    }).then(function (response) {
        $scope.objetivosCiclismo = response.data;
    }, function (response) {
        console.log('Opsss... Algo deu errado na busca dos objetivos!');
    });
    

    $http({
        url: '../api/prescricao/getObjetivos/1',
        method: "POST"
    }).then(function (response) {
        $scope.objetivos = response.data;
    }, function (response) {
        console.log('Opsss... Algo deu errado na busca dos objetivos!');
    });



    getPadrao();
    getPadraoCross();
    getPadraoCorrida();
    getPadraoCiclismo();
    getPadraoNatacao();
    
    //treinos padrao
    $scope.getPadraoMF = function () {
        $http({
            url: '../api/prescricao/getPadrao/F',
            method: "GET"
        }).then(function (response) {
            $scope.treinospadraof = response.data.treinospadrao;
        }, function (response) {
            console.log('Opsss... Algo deu errado!');
        });

        $http({
            url: '../api/prescricao/getPadrao/M',
            method: "GET"
        }).then(function (response) {
            $scope.treinospadraom = response.data.treinospadrao;
        }, function (response) {
            console.log('Opsss... Algo deu errado!');
        });
    };
    function getPadrao() {
        $http({
            url: '../api/prescricao/getPadrao/F',
            method: "GET"
        }).then(function (response) {
            $scope.treinospadraof = response.data.treinospadrao;
        }, function (response) {
            console.log('Opsss... Algo deu errado!');
        });

        $http({
            url: '../api/prescricao/getPadrao/M',
            method: "GET"
        }).then(function (response) {
            $scope.treinospadraom = response.data.treinospadrao;
        }, function (response) {
            console.log('Opsss... Algo deu errado!');
        });

    }
    ;

    function getPadraoCross() {
        $http({
            url: '../api/prescricao/getPadraoModalidade/F/cross',
            method: "GET"
        }).then(function (response) {
            $scope.treinospadraocrossf = response.data.treinospadrao;
        }, function (response) {
            console.log('Opsss... Algo deu errado!');
        });

        $http({
            url: '../api/prescricao/getPadraoModalidade/M/cross',
            method: "GET"
        }).then(function (response) {
            $scope.treinospadraocrossm = response.data.treinospadrao;
        }, function (response) {
            console.log('Opsss... Algo deu errado!');
        });

    }
    ;
    function getPadraoCorrida() {
        $http({
            url: '../api/prescricao/getPadraoModalidade/F/corrida',
            method: "GET"
        }).then(function (response) {
            $scope.treinospadraocorridaf = response.data.treinospadrao;
        }, function (response) {
            console.log('Opsss... Algo deu errado!');
        });

        $http({
            url: '../api/prescricao/getPadraoModalidade/M/corrida',
            method: "GET"
        }).then(function (response) {
            $scope.treinospadraocorridam = response.data.treinospadrao;
        }, function (response) {
            console.log('Opsss... Algo deu errado!');
        });

    }
    ;    
    function getPadraoCiclismo() {
        $http({
            url: '../api/prescricao/getPadraoModalidade/F/ciclismo',
            method: "GET"
        }).then(function (response) {
            $scope.treinospadraociclismof = response.data.treinospadrao;
        }, function (response) {
            console.log('Opsss... Algo deu errado!');
        });

        $http({
            url: '../api/prescricao/getPadraoModalidade/M/ciclismo',
            method: "GET"
        }).then(function (response) {
            $scope.treinospadraociclismom = response.data.treinospadrao;
        }, function (response) {
            console.log('Opsss... Algo deu errado!');
        });

    };    
    function getPadraoNatacao() {
        $http({
            url: '../api/prescricao/getPadraoModalidade/F/natacao',
            method: "GET"
        }).then(function (response) {
            $scope.treinospadraonatacaof = response.data.treinospadrao;
        }, function (response) {
            console.log('Opsss... Algo deu errado!');
        });

        $http({
            url: '../api/prescricao/getPadraoModalidade/M/natacao',
            method: "GET"
        }).then(function (response) {
            $scope.treinospadraonatacaom = response.data.treinospadrao;
        }, function (response) {
            console.log('Opsss... Algo deu errado!');
        });

    };    

    $scope.changeObjetivoMusculacao = function (modalidade, id) {
        $http({
            url: '../api/prescricao/getNiveis/' + modalidade + '/' + id,
            method: "POST"
        }).then(function (response) {
            $scope.niveis = response.data;
            $timeout(function () {
                // $scope.ativaRamificacao();
            }, 2000);
        }, function (response) {
            console.log('Opsss... Algo deu errado na busca dos niveis!');
        });

    };
    $scope.getFichaPadraoCross = function (id) {

        $http({
            url: '../api/prescricao/getFichaPadraoCross',
            data: {
                treino_id: id
            },
            method: "POST"
        }).then(function (response) {

            $scope.sexo_treino_cross = response.data.treinopadrao[0].treinosexo;
            $scope.treino_nome_cross = response.data.treinopadrao[0].nome_treino;
            $scope.treino_objetivo_cross = response.data.treinopadrao[0].idobjetivo;
            $scope.treino_nivel_cross = response.data.treinopadrao[0].idnivel;
            
            $("#objetivo_treino_cross").val(response.data.treinopadrao[0].idobjetivo);
            $("#nivel_treino_cross").val(response.data.treinopadrao[0].idnivel);
            $("#sexo_treino_cross").val(response.data.treinopadrao[0].treinosexo);

            $scope.setTreinoEdit(id);


        });
        /* $http({
         url: '/admin/api/prescricao/getCaledarioPadrao',
         data: {
         mes: 2, ano: 2017, tipo: 'cross', treino_id: id
         
         },
         method: "POST"
         }).then(function (response) {        
         
         
         });*/

    };

    $scope.getFichaPadrao = function (id) {
        $scope.treinoSel = id;


        $http({
            url: '../api/prescricao/getFichaPadrao',
            data: {
                treino_id: id
            },
            method: "POST"
        }).then(function (response) {
            $scope.treino_id = id;
            $scope.fichaspadrao = response.data.treinofichas;
            $scope.treinopadrao = response.data.treinopadrao;
            $scope.numdias = response.data.treinopadrao.treino_qtddias;
            $scope.treino_qtddias = response.data.treinopadrao[0].treino_qtddias;
            $scope.objetivo_id = response.data.treinopadrao[0].treino_objetivo;
            $scope.treino_observacao = response.data.treinopadrao[0].treino_observacao;
            $scope.nivel_id = response.data.treinopadrao[0].treino_nivel;
            $scope.treino_nivel = response.data.treinopadrao[0].treino_nivel;
            $scope.treino_nome_padrao = response.data.treinopadrao[0].treino_nome_padrao;
            $scope.treino_sexo = response.data.treinopadrao[0].treino_sexo;


            $scope.semTreino = true;
            $scope.changeTreinoPadrao($scope.treino_id);

        });
    };



    $scope.changeExercicio = function (exercicio) {
        for (var i = 0; i < $scope.exercicios.length; i++) {
            if ($scope.exercicios[i].id == exercicio) {
                return i;
            }
        }
    };


    // ALTERA QUANTIDADE DE DIAS DE TREINO
    $scope.addTab = function (treino_qtddias) {
        $scope.tabsStatus = {};
        for (var i = 1; i <= 7; i++) {
            if (i <= treino_qtddias) {
                $scope.tabsStatus['tab' + i] = true;
            } else {
                $scope.tabsStatus['tab' + i] = false;
            }
        }
    };


    // ADICIONA NOVO TREINO

    $scope.addTreinoCrossPadrao = function () {
        $("#n_abas").show(120);
        $("#n_abas2").show(120);
        // DESATIVA OS BOTOES
        $scope.statusBotoesCross(true);
        $scope.msgTreino = 'Aguarde... criando treino!';
        $http({
            url: '../api/prescricao/addTreinoModalidadePadrao',
            data: {
                //treino_id: $scope.treino_id, 
                idobjetivo: null,
                idnivel: null,
                nome_treino: null,
                tipo_prescricao :'cross'

            },
            method: "POST"
        }).then(function (response) {

            $scope.treino_id_padrao = response.data.treinos;
            $scope.semTreinoPadrao = true;
            
            $scope.setaCodigo($scope.treino_id_padrao,'cross');
        }, function (response) {
            console.log('Opsss... Algo deu errado!');
        });

    };

    $scope.addTreinoCorridaPadrao = function () {
        $("#n_abas").show(120);
        $("#n_abas2").show(120);
        // DESATIVA OS BOTOES
        $scope.statusBotoesCorrida(true);
        $scope.msgTreino = 'Aguarde... criando treino!';
        $http({
            url: '../api/prescricao/addTreinoModalidadePadrao',
            data: {
                //treino_id: $scope.treino_id, 
                idobjetivo: null,
                idnivel: null,
                nome_treino: null,
                tipo_prescricao: 'corrida'
                

            },
            method: "POST"
        }).then(function (response) {

            $scope.treino_id_padraocorrida = response.data.treinos;
            $scope.semTreinoPadrao = true;
            
            $scope.setaCodigo($scope.treino_id_padraocorrida,'corrida');
        }, function (response) {
            console.log('Opsss... Algo deu errado!');
        });

    };
    $scope.addTreinoCiclismoPadrao = function () {
        $("#n_abas").show(120);
        $("#n_abas2").show(120);
        // DESATIVA OS BOTOES
        $scope.statusBotoesCiclismo(true);
        $scope.msgTreino = 'Aguarde... criando treino!';
        $http({
            url: '../api/prescricao/addTreinoModalidadePadrao',
            data: {
                //treino_id: $scope.treino_id, 
                idobjetivo: null,
                idnivel: null,
                nome_treino: null,
                tipo_prescricao: 'ciclismo'

            },
            method: "POST"
        }).then(function (response) {

            $scope.treino_id_padraociclismo = response.data.treinos;
            $scope.semTreinoPadrao = true;
            
            $scope.setaCodigo($scope.treino_id_padraociclismo,'ciclismo');
        }, function (response) {
            console.log('Opsss... Algo deu errado!');
        });

    };
    $scope.addTreinoNatacaoPadrao = function () {
        $("#n_abas").show(120);
        $("#n_abas2").show(120);
        // DESATIVA OS BOTOES
        $scope.statusBotoesNatacao(true);
        $scope.msgTreino = 'Aguarde... criando treino!';
        $http({
            url: '../api/prescricao/addTreinoModalidadePadrao',
            data: {
                //treino_id: $scope.treino_id, 
                idobjetivo: null,
                idnivel: null,
                nome_treino: null,
                tipo_prescricao: 'natacao'

            },
            method: "POST"
        }).then(function (response) {

            $scope.treino_id_padraonatacao = response.data.treinos;
            $scope.semTreinoPadrao = true;
            
            $scope.setaCodigo($scope.treino_id_padraonatacao,'natacao');
        }, function (response) {
            console.log('Opsss... Algo deu errado!');
        });

    };

    $scope.getTreinosCross = function () {
        $scope.treinos = {};
        $http({
            url: '../../api/prescricao/getTreinosCross',
            data: {
                aluno_id: $scope.client_id
            },
            method: "POST"
        }).then(function (response) {
            $scope.treinos = response.data;
            if (response.data.length !== 0) {
                $scope.changeTreino(response.data[response.data.length - 1].treino_id);
                $scope.treino_id_padrao = $scope.treinos[$scope.treinos.length - 1];
            } else {

//                $scope.delTreinoBtn = true;
                //              $scope.semTreino = true;
            }
        }, function (response) {
            console.log('Opsss... Algo deu errado!');
        });

    };


    $scope.setTreinoEdit = function (id) {
        $scope.treino_id_padrao = id;

    };

    $scope.addTreino = function () {
        $("#n_abas").show(120);
        $("#n_abas2").show(120);
        // DESATIVA OS BOTOES
        $scope.statusBotoes(true);
        $scope.msgTreino = 'Aguarde... criando treino!';
        $http({
            url: '../api/prescricao/addTreinoPadrao',
            data: {
                //treino_id: $scope.treino_id, 
                treino_sexo: null,
                treino_qtddias: null,
                treino_nivel: null,
                objetivo_id: null,
                treino_nome_padrao: null,
                treino_tempoestimado: null,
                treino_observacao: null
            },
            method: "POST"
        }).then(function (response) {
            $scope.addTab(1);
            $scope.newTreino = response.data.treinos;
            $scope.treino_id = response.data.treinos;
            $scope.treino_qtddias = '';
            $scope.treino_iniciado = 'N';
            $scope.treino_nivel = null;
            $scope.treino_revisao = '';
            $scope.treino_padrao = false;
            $scope.treino_atual = false;
            $scope.treino_nome_padrao = '';


            $scope.linhasA = []; // LIMPA VETOR A
            $scope.linhasB = []; // LIMPA VETOR B
            $scope.linhasC = []; // LIMPA VETOR C
            $scope.linhasD = []; // LIMPA VETOR D
            $scope.linhasE = []; // LIMPA VETOR E
            $scope.linhasF = []; // LIMPA VETOR F
            $scope.linhasG = []; // LIMPA VETOR G
            $scope.msgTreino = '';
            // ATIVA OS BOTOES
            $scope.statusBotoes(false);
            $scope.delTreinoBtn = false;
            $scope.semTreino = true;
        }, function (response) {
            console.log('Opsss... Algo deu errado!');
        });
    };

    $scope.statusBotoes = function (sts) {

        $scope.addTreinoBtn = sts;
        $scope.salvarTreinoBtn = sts;
        $scope.delTreinoBtn = sts;
        $scope.addLinhaBtn = sts;
    };
    $scope.statusBotoesCross = function (sts) {

        $scope.addTreinoBtnCross = sts;
        $scope.salvarTreinoBtnCross = sts;
        $scope.delTreinoBtnCross = sts;
        $scope.addLinhaBtnCross = sts;
    };
    $scope.statusBotoesCorrida = function (sts) {

        $scope.addTreinoBtnCorrida = sts;
        $scope.salvarTreinoBtnCorrida = sts;
        $scope.delTreinoBtnCorrida = sts;
        $scope.addLinhaBtnCorrida = sts;
    };
    $scope.statusBotoesCiclismo = function (sts) {

        $scope.addTreinoBtnCiclismo = sts;
        $scope.salvarTreinoBtnCiclismo = sts;
        $scope.delTreinoBtnCiclismo = sts;
        $scope.addLinhaBtnCiclismo = sts;
        
    };

    $scope.statusBotoesNatacao = function (sts) {

        $scope.addTreinoBtnNatacao = sts;
        $scope.salvarTreinoBtnNatacao = sts;
        $scope.delTreinoBtnNatacao = sts;
        $scope.addLinhaBtnNatacao = sts;
        
    };

    // ATUALIZA TREINO

    $scope.validarSerie = function ($linhas) {

        return ret;
    };

    $scope.salvarModalidade = function (tipo) {
        
        
        btnprescricaoSalvarCross(tipo);
        
        if (tipo == 'cross') {
            getPadraoCross();
        }
        if (tipo == 'corrida') {
            getPadraoCorrida()();
        }
        if (tipo == 'natacao') {
            getPadraoNatacao()();
        }
        if (tipo == 'ciclismo') {
            getPadraoCiclismo()();
        }
        
        $timeout(function () {
            $scope.statusBotoesCross(false);
            $scope.statusBotoesCiclismo(false);
            $scope.statusBotoesNatacao(false);
            $scope.statusBotoesCorrida(false);
            $scope.treino_id_padrao=0;
            $scope.treino_id_padrao = 0;
            $scope.treino_id_padraocorrida =0;
            $scope.treino_id_padraociclismo = 0;
            $scope.treino_id_padraonatacao = 0;
        }, 1000);        
        
        
    };


    $scope.salvarTreino = function () {

        $scope.AtualizaPosicoesASave();
        $scope.AtualizaPosicoesBSave();
        $scope.AtualizaPosicoesCSave();
        $scope.AtualizaPosicoesDSave();
        $scope.AtualizaPosicoesESave();
        $scope.AtualizaPosicoesFSave();
        $scope.AtualizaPosicoesGSave();

        if ($scope.treino_id !== undefined) {
            if ($scope.objetivo_id !== null) {
                if ($scope.treino_nivel !== null) {
                    for (var i = 1; i <= $scope.treino_qtddias; i++) {
                        switch (i) {
                            case 1:

                                if ($scope.linhasA.length === 0) {
                                    swal("Opsss!", "Informe pelo menos um exercício na aba 'A' antes de salvar!", "error");
                                    return false;
                                }
                                break;
                            case 2:
                                if ($scope.linhasB.length === 0) {
                                    swal("Opsss!", "Informe pelo menos um exercício na aba 'B' antes de salvar!", "error");
                                    return false;
                                }
                                break;
                            case 3:
                                if ($scope.linhasC.length === 0) {
                                    swal("Opsss!", "Informe pelo menos um exercício na aba 'C' antes de salvar!", "error");
                                    return false;
                                }
                                break;
                            case 4:
                                if ($scope.linhasD.length === 0) {
                                    swal("Opsss!", "Informe pelo meno um exercício na aba 'D' antes de salvar!", "error");
                                    return false;
                                }
                                break;
                            case 5:
                                if ($scope.linhasE.length === 0) {
                                    swal("Opsss!", "Informe pelo menos um exercício na aba 'E' antes de salvar!", "error");
                                    return false;
                                }
                                break;
                            case 6:
                                if ($scope.linhasF.length === 0) {
                                    swal("Opsss!", "Informe pelo menos um exercício na aba 'F' antes de salvar!", "error");
                                    return false;
                                }
                                break;
                            case 7:
                                if ($scope.linhasG.length === 0) {
                                    swal("Opsss!", "Informe pelo menos um exercício na aba 'G' antes de salvar!", "error");
                                    return false;
                                }
                                break;
                        }
                    }
                    // DESATIVA OS BOTOES
                    $scope.statusBotoes(true);
                    $http({
                        url: '../api/prescricao/upTreinoPadrao',
                        data: {
                            treino_id: $scope.treino_id,
                            treino_qtddias: $scope.treino_qtddias,
                            treino_nivel: $scope.treino_nivel,
                            objetivo_id: $scope.objetivo_id,
                            treino_sexo: $scope.treino_sexo,
                            treino_observacao: $scope.treino_observacao,
                            treino_nome_padrao: $scope.treino_nome_padrao,
                            treino_tempoestimado: ($scope.tempoestimadoA + $scope.tempoestimadoB + $scope.tempoestimadoC + $scope.tempoestimadoD + $scope.tempoestimadoE + $scope.tempoestimadoF + $scope.tempoestimadoG)

                        },
                        method: "POST"
                    }).then(function (response) {
                        // SALVA EXERCICIOS NO TREINO A
                        if ($scope.linhasA.length > 0) {
                            

                            
                            $http({
                                url: '../api/prescricao/addExercicioTreinoPadrao/' + $scope.treino_id,
                                data: $scope.linhasA,
                                method: "POST"
                            }).then(function (response) {
                                console.log(response);
                            }, function (response) {
                                console.log('Opsss... Algo deu errado!');
                            });

                            $http({
                                url: '../api/prescricao/saveTempoEstimadoPadrao',
                                data: {
                                    treino_id: $scope.treino_id,
                                    letra: 'A',
                                    tempoestimado: $scope.tempoestimadoA,
                                    tempomusculacao: $scope.tempomusculacaoA
                                },
                                method: "POST"
                            }).then(function (response) {
                                console.log(response);
                            }, function (response) {
                                console.log('Opsss... Algo deu errado ao salvar tempo estimado!');
                            });

                        }
                        // FECHA SALVA EXERCICIOS NO TREINO A
                        // SALVA EXERCICIOS NO TREINO B
                        if ($scope.linhasB.length > 0) {
                            $http({
                                url: '../api/prescricao/addExercicioTreinoPadrao/' + $scope.treino_id,
                                data: $scope.linhasB,
                                method: "POST"
                            }).then(function (response) {
                                console.log(response);
                            }, function (response) {
                                console.log('Opsss... Algo deu errado!');
                            });

                            $http({
                                url: '../api/prescricao/saveTempoEstimadoPadrao',
                                data: {
                                    treino_id: $scope.treino_id,
                                    letra: 'B',
                                    tempoestimado: $scope.tempoestimadoB,
                                    tempomusculacao: $scope.tempomusculacaoB
                                },
                                method: "POST"
                            }).then(function (response) {
                                console.log(response);
                            }, function (response) {
                                console.log('Opsss... Algo deu errado ao salvar tempo estimado!');
                            });
                        }
                        // FECHA SALVA EXERCICIOS NO TREINO B
                        // SALVA EXERCICIOS NO TREINO C
                        if ($scope.linhasC.length > 0) {
                            $http({
                                url: '../api/prescricao/addExercicioTreinoPadrao/' + $scope.treino_id,
                                data: $scope.linhasC,
                                method: "POST"
                            }).then(function (response) {
                                console.log(response);
                            }, function (response) {
                                console.log('Opsss... Algo deu errado!');
                            });

                            $http({
                                url: '../api/prescricao/saveTempoEstimadoPadrao',
                                data: {
                                    treino_id: $scope.treino_id,
                                    letra: 'C',
                                    tempoestimado: $scope.tempoestimadoC,
                                    tempomusculacao: $scope.tempomusculacaoC
                                },
                                method: "POST"
                            }).then(function (response) {
                                console.log(response);
                            }, function (response) {
                                console.log('Opsss... Algo deu errado ao salvar tempo estimado!');
                            });
                            $scope.AtualizaPosicoesC();
                        }
                        // FECHA SALVA EXERCICIOS NO TREINO C
                        // SALVA EXERCICIOS NO TREINO D
                        if ($scope.linhasD.length > 0) {
                            $http({
                                url: '../api/prescricao/addExercicioTreinoPadrao/' + $scope.treino_id,
                                data: $scope.linhasD,
                                method: "POST"
                            }).then(function (response) {
                                console.log(response);
                            }, function (response) {
                                console.log('Opsss... Algo deu errado!');
                            });

                            $http({
                                url: '../api/prescricao/saveTempoEstimadoPadrao',
                                data: {
                                    treino_id: $scope.treino_id,
                                    letra: 'D',
                                    tempoestimado: $scope.tempoestimadoD,
                                    tempomusculacao: $scope.tempomusculacaoD
                                },
                                method: "POST"
                            }).then(function (response) {
                                console.log(response);
                            }, function (response) {
                                console.log('Opsss... Algo deu errado ao salvar tempo estimado!');
                            });
                        }
                        // FECHA SALVA EXERCICIOS NO TREINO D
                        // SALVA EXERCICIOS NO TREINO E
                        if ($scope.linhasE.length > 0) {
                            $http({
                                url: '../api/prescricao/addExercicioTreinoPadrao/' + $scope.treino_id,
                                data: $scope.linhasE,
                                method: "POST"
                            }).then(function (response) {
                                console.log(response);
                            }, function (response) {
                                console.log('Opsss... Algo deu errado!');
                            });

                            $http({
                                url: '../api/prescricao/saveTempoEstimadoPadrao',
                                data: {
                                    treino_id: $scope.treino_id,
                                    letra: 'E',
                                    tempoestimado: $scope.tempoestimadoE,
                                    tempomusculacao: $scope.tempomusculacaoE
                                },
                                method: "POST"
                            }).then(function (response) {
                                console.log(response);
                            }, function (response) {
                                console.log('Opsss... Algo deu errado ao salvar tempo estimado!');
                            });
                        }
                        // FECHA SALVA EXERCICIOS NO TREINO E
                        // SALVA EXERCICIOS NO TREINO F
                        if ($scope.linhasF.length > 0) {
                            $http({
                                url: '../api/prescricao/addExercicioTreinoPadrao/' + $scope.treino_id,
                                data: $scope.linhasF,
                                method: "POST"
                            }).then(function (response) {
                                console.log(response);
                            }, function (response) {
                                console.log('Opsss... Algo deu errado!');
                            });

                            $http({
                                url: '../api/prescricao/saveTempoEstimadoPadrao',
                                data: {
                                    treino_id: $scope.treino_id,
                                    letra: 'F',
                                    tempoestimado: $scope.tempoestimadoF,
                                    tempomusculacao: $scope.tempomusculacaoF
                                },
                                method: "POST"
                            }).then(function (response) {
                                console.log(response);
                            }, function (response) {
                                console.log('Opsss... Algo deu errado ao salvar tempo estimado!');
                            });
                        }
                        // FECHA SALVA EXERCICIOS NO TREINO F
                        // SALVA EXERCICIOS NO TREINO G
                        if ($scope.linhasG.length > 0) {
                            $http({
                                url: '../api/prescricao/addExercicioTreinoPadrao/' + $scope.treino_id,
                                data: $scope.linhasG,
                                method: "POST"
                            }).then(function (response) {
                                console.log(response);
                            }, function (response) {
                                console.log('Opsss... Algo deu errado!');
                            });

                            $http({
                                url: '../api/prescricao/saveTempoEstimadoPadrao',
                                data: {
                                    treino_id: $scope.treino_id,
                                    letra: 'G',
                                    tempoestimado: $scope.tempoestimadoG,
                                    tempomusculacao: $scope.tempomusculacaoG
                                },
                                method: "POST"
                            }).then(function (response) {
                                console.log(response);
                            }, function (response) {
                                console.log('Opsss... Algo deu errado ao salvar tempo estimado!');
                            });
                        }
                        // FECHA SALVA EXERCICIOS NO TREINO G
                        swal('Sucesso!', 'Treino salvo com sucesso!', 'success');
                        // ATIVA OS BOTOES
                        $timeout(function () {
                            /*$('#execicioSelectA' + $scope.insertA.id).addClass('chosen-select');
                             $('.chosen-select').chosen({width: "100%"});*/
                        }, 500);
                        $scope.statusBotoes(false);
                        getPadrao();
                        $scope.changeTreinoPadrao($scope.treino_id);
                        //getPadrao();
                    }, function (response) {
                        console.log('Opsss... Algo deu errado!');
                    });
                } else {
                    swal("Opsss!", "Informe o nível de habilidade antes de salvar!", "error");
                }
            } else {
                swal("Opsss!", "Informe o objetivo do treino!", "error");
            }
        } else {
            swal("Opsss!", "Selecione ou crie um treino antes de salvar!", "error");
        }

        $scope.getFichaPadrao($scope.treino_id);
        
        ////chamar aqui reload da arvore de 
        $scope.getPadraoMF();
        
    };///////////////////////

    $scope.delFichaSuperSerie = function (ficha_id, ficha_super, super_serie) {

        // console.log( $scope.treino_id );

        $http({
            url: '../api/prescricao/delExercicioSuperSeriePadrao/' + ficha_id + '/' + $scope.treino_id + '/' + ficha_super + '/' + super_serie,
            method: "GET"
        }).then(function (response) {
        });
    };

    $scope.changeGenericoTesteA = function (e, linha) {

        var ex = e - 1;
        if (ex > linha.superserie.length) {
            var jaTem = linha.superserie.length;
            var adiciona = ex - jaTem;
            for (var i = 0; i < adiciona; i++) {
                var series = '1';
                var repeticoes = null;
                var intervalo = null;

                $scope.insertExer = {
                    id: linha.superserie.length + 1,
                    letra: linha.letra,
                    exercicio: 0,
                    intervalo: intervalo,
                    series: series,
                    repeticoes: repeticoes,
                    medida_duracao: 'rep',
                    carga: 0,
                    medida_intensidade: 'kg',
                    ordem: $scope.linhasA.length + 1,
                    observacao: 0,
                    super_serie: parseInt(1),
                    seq: $scope.linhasA.length + 1

                };
                linha.superserie.push($scope.insertExer);
            }

            $timeout(function () {
                for (var i = 1; i <= linha.superserie.length; i++) {
                    $('#execicioSelect' + linha.letra + '-super' + i).addClass('chosen-select');
                    $('.chosen-select').chosen({width: "100%"});
                }
            }, 500);
        } else {
            for (var x = ex; x <= linha.superserie.length; x++) {
                $scope.delFichaSuperSerie(linha.superserie[linha.superserie.length - 1].ficha, linha.ficha, e);
                linha.superserie.splice(linha.superserie.length - 1, 1);
            }
        }
    };



    // LINHAS DA COLUNA A
    $scope.linhasA = [];
    $scope.addLinhaA = function () {

        var data = new Date();
        var dia = data.getDate();
        var mes = data.getMonth() + 1;
        var ano = data.getFullYear();
        var hora = data.getHours();
        var minuto = data.getMinutes();
        var segundo = data.getSeconds();
        var milisec = data.getMilliseconds();

        var hash_code = [ano, mes, dia, hora, minuto, segundo, milisec, 'A', $scope.linhasA.length + 1].join('');

        $scope.valido = false;
        if ($scope.treino_nome_padrao.length > 0) {
            $scope.valido = true;
        } else {
            $scope.valido = false;
            swal("Opsss!", "Informe o nome do treino!", "error");
        }


        if ($scope.valido === true) {
            $scope.valido = false;
            if ($scope.treino_sexo !== '') {
                $scope.valido = true;
            } else {
                $scope.valido = false;
                swal("Opsss!", "Selecione o sexo!", "error");
            }
        }

        if ($scope.valido === true) {
            $scope.valido = false;
            if ($scope.objetivo_id > 0) {
                $scope.valido = true;
            } else {
                $scope.valido = false;
                swal("Opsss!", "Selecione o objetivo do treino!", "error");
            }
        }
        if ($scope.valido === true) {
            $scope.valido = false;
            if ($scope.treino_nivel > 0) {
                $scope.valido = true;
            } else {
                $scope.valido = false;
                swal("Opsss!", "Selecione o nível de habilidade!", "error");
            }
        }

        if ($scope.valido === true) {

            var series = '1';
            var repeticoes = null;
            var intervalo = null;
            if ($scope.linhasA.length > 0) {
                series = $scope.linhasA[0].series;
                repeticoes = $scope.linhasA[0].repeticoes;
                intervalo = $scope.linhasA[0].intervalo;

            }
            $scope.insertA = {
                id: $scope.linhasA.length + 1,
                letra: 'A',
                exercicio: null,
                intervalo: intervalo,
                series: series,
                repeticoes: repeticoes,
                medida: 'rep',
                carga: null,
                observacao: null,
                ordem: $scope.linhasA.length + 1,
                medida_duracao: 'rep',
                medida_intensidade: 'kg',
                super_serie: parseInt(1),
                superserie: [],
                hash_code: hash_code
            };
            if ($scope.linhasA.length > 0) {
                if ($scope.linhasA[0].series == null) {
                    swal("Opsss!", "Informe o número de séries do exercicio!", "error");
                } else {
                    $scope.AtualizaPosicoesA($scope.insertA);
                }
            } else {
                $scope.AtualizaPosicoesA($scope.insertA);
            }

        }
    };

    $scope.removeLinhaA = function (index, ficha_id) {
        $scope.linhasA.splice(index, 1);
        $scope.delFicha(ficha_id);
    };


    $scope.removeLinhaSuper = function (indice, superseries, linha, ficha_mae) {

        $scope.k = [];
        $scope.k = linha;
        $scope.linha.super_serie = $scope.k.length;
        $scope.delFichaSuperSerie(superseries.ficha, ficha_mae, $scope.k.length);
        linha.splice(indice, 1);
    };

    // FECHA LINHAS DA COLUNA A                        
    // LINHAS DA COLUNA B
    $scope.linhasB = [];
    $scope.addLinhaB = function () {

        var data = new Date();
        var dia = data.getDate();
        var mes = data.getMonth() + 1;
        var ano = data.getFullYear();
        var hora = data.getHours();
        var minuto = data.getMinutes();
        var segundo = data.getSeconds();
        var milisec = data.getMilliseconds();

        var hash_code = [ano, mes, dia, hora, minuto, segundo, milisec, 'B', $scope.linhasB.length + 1].join('');

        var series = '1';
        var repeticoes = null;
        var intervalo = null;
        if ($scope.linhasB.length > 0) {
            series = $scope.linhasB[0].series;
            repeticoes = $scope.linhasB[0].repeticoes;
            intervalo = $scope.linhasB[0].intervalo;
        }
        $scope.insertB = {
            id: $scope.linhasB.length + 1,
            letra: 'B',
            exercicio: null,
            intervalo: intervalo,
            series: series,
            repeticoes: repeticoes,
            medida: 'rep',
            carga: null,
            observacao: null,
            ordem: $scope.linhasB.length + 1,
            medida_duracao: 'rep',
            medida_intensidade: 'kg',
            super_serie: parseInt(1),
            superserie: [],
            hash_code: hash_code
        };

        if ($scope.linhasB.length > 0) {
            if ($scope.linhasB[0].series == null) {
                swal("Opsss!", "Informe o número de séries do exercicio!", "error");
            } else {
                $scope.AtualizaPosicoesB($scope.insertB);
            }
        } else {
            $scope.AtualizaPosicoesB($scope.insertB);
        }

        $timeout(function () {
            $('#execicioSelectB' + $scope.insertB.id - 1).addClass('chosen-select');
            $('.chosen-select').chosen({width: "100%"});
        }, 500);

    };

    $scope.removeLinhaB = function (index, ficha_id) {
        $scope.linhasB.splice(index, 1);
        $scope.delFicha(ficha_id);
    };
    // FECHA LINHAS DA COLUNA B

    // LINHAS DA COLUNA C
    $scope.linhasC = [];
    $scope.addLinhaC = function () {

        var data = new Date();
        var dia = data.getDate();
        var mes = data.getMonth() + 1;
        var ano = data.getFullYear();
        var hora = data.getHours();
        var minuto = data.getMinutes();
        var segundo = data.getSeconds();
        var milisec = data.getMilliseconds();

        var hash_code = [ano, mes, dia, hora, minuto, segundo, milisec, 'C', $scope.linhasC.length + 1].join('');

        var series = '1';
        var repeticoes = null;
        var intervalo = null;
        if ($scope.linhasC.length > 0) {
            series = $scope.linhasC[0].series;
            repeticoes = $scope.linhasC[0].repeticoes;
            intervalo = $scope.linhasC[0].intervalo;
        }
        $scope.insertC = {
            id: $scope.linhasC.length + 1,
            letra: 'C',
            exercicio: null,
            intervalo: intervalo,
            series: series,
            repeticoes: repeticoes,
            medida: 'rep',
            carga: null,
            observacao: null,
            ordem: $scope.linhasC.length + 1,
            medida_duracao: 'rep',
            medida_intensidade: 'kg',
            super_serie: parseInt(1),
            superserie: [],
            hash_code: hash_code
        };
        if ($scope.linhasC.length > 0) {
            if ($scope.linhasC[0].series == null) {
                swal("Opsss!", "Informe o número de séries do exercicio!", "error");
            } else {
                $scope.AtualizaPosicoesC($scope.insertC);
            }
        } else {
            $scope.AtualizaPosicoesC($scope.insertC);
        }

        $timeout(function () {
            $('#execicioSelectC' + $scope.insertC.id - 1).addClass('chosen-select');
            $('.chosen-select').chosen({width: "100%"});
        }, 500);

    };

    $scope.removeLinhaC = function (index, ficha_id) {
        $scope.linhasC.splice(index, 1);
        $scope.delFicha(ficha_id);
    };
    // FECHA LINHAS DA COLUNA C

    // LINHAS DA COLUNA D
    $scope.linhasD = [];
    $scope.addLinhaD = function () {

        var data = new Date();
        var dia = data.getDate();
        var mes = data.getMonth() + 1;
        var ano = data.getFullYear();
        var hora = data.getHours();
        var minuto = data.getMinutes();
        var segundo = data.getSeconds();
        var milisec = data.getMilliseconds();

        var hash_code = [ano, mes, dia, hora, minuto, segundo, milisec, 'D', $scope.linhasD.length + 1].join('');

        var series = '1';
        var repeticoes = null;
        var intervalo = null;
        if ($scope.linhasD.length > 0) {
            series = $scope.linhasD[0].series;
            repeticoes = $scope.linhasD[0].repeticoes;
            intervalo = $scope.linhasD[0].intervalo;
        }
        $scope.insertD = {
            id: $scope.linhasD.length + 1,
            letra: 'D',
            exercicio: null,
            intervalo: intervalo,
            series: series,
            repeticoes: repeticoes,
            medida: 'rep',
            carga: null,
            observacao: null,
            ordem: $scope.linhasD.length + 1,
            medida_duracao: 'rep',
            medida_intensidade: 'kg',
            super_serie: parseInt(1),
            superserie: [],
            hash_code: hash_code
        };
        if ($scope.linhasD.length > 0) {
            if ($scope.linhasD[0].series == null) {
                swal("Opsss!", "Informe o número de séries do exercicio!", "error");
            } else {
                $scope.AtualizaPosicoesD($scope.insertD);
            }
        } else {
            $scope.AtualizaPosicoesD($scope.insertD);
        }

        $timeout(function () {
            $('#execicioSelectD' + $scope.insertD.id - 1).addClass('chosen-select');
            $('.chosen-select').chosen({width: "100%"});
        }, 500);

    };

    $scope.removeLinhaD = function (index, ficha_id) {
        $scope.linhasD.splice(index, 1);
        $scope.delFicha(ficha_id);
    };
    // FECHA LINHAS DA COLUNA D

    // LINHAS DA COLUNA E
    $scope.linhasE = [];
    $scope.addLinhaE = function () {
        var data = new Date();
        var dia = data.getDate();
        var mes = data.getMonth() + 1;
        var ano = data.getFullYear();
        var hora = data.getHours();
        var minuto = data.getMinutes();
        var segundo = data.getSeconds();
        var milisec = data.getMilliseconds();

        var hash_code = [ano, mes, dia, hora, minuto, segundo, milisec, 'E', $scope.linhasE.length + 1].join('');

        var series = '1';
        var repeticoes = null;
        var intervalo = null;
        if ($scope.linhasE.length > 0) {
            series = $scope.linhasE[0].series;
            repeticoes = $scope.linhasE[0].repeticoes;
            intervalo = $scope.linhasE[0].intervalo;
        }
        $scope.insertE = {
            id: $scope.linhasE.length + 1,
            letra: 'E',
            exercicio: null,
            intervalo: intervalo,
            series: series,
            repeticoes: repeticoes,
            medida: 'rep',
            carga: null,
            observacao: null,
            ordem: $scope.linhasE.length + 1,
            medida_duracao: 'rep',
            medida_intensidade: 'kg',
            super_serie: parseInt(1),
            superserie: [],
            hash_code: hash_code
        };
        if ($scope.linhasE.length > 0) {
            if ($scope.linhasE[0].series == null) {
                swal("Opsss!", "Informe o número de séries do exercicio!", "error");
            } else {
                $scope.AtualizaPosicoesE($scope.insertE);
            }
        } else {
            $scope.AtualizaPosicoesE($scope.insertE);
        }

        $timeout(function () {
            $('#execicioSelectE' + $scope.insertE.id - 1).addClass('chosen-select');
            $('.chosen-select').chosen({width: "100%"});
        }, 500);

    };

    $scope.removeLinhaE = function (index, ficha_id) {
        $scope.linhasE.splice(index, 1);
        $scope.delFicha(ficha_id);
    };
    // FECHA LINHAS DA COLUNA E
    // LINHAS DA COLUNA F
    $scope.linhasF = [];
    $scope.addLinhaF = function () {
        var data = new Date();
        var dia = data.getDate();
        var mes = data.getMonth() + 1;
        var ano = data.getFullYear();
        var hora = data.getHours();
        var minuto = data.getMinutes();
        var segundo = data.getSeconds();
        var milisec = data.getMilliseconds();

        var hash_code = [ano, mes, dia, hora, minuto, segundo, milisec, 'F', $scope.linhasF.length + 1].join('');

        var series = '1';
        var repeticoes = null;
        var intervalo = null;
        if ($scope.linhasF.length > 0) {
            series = $scope.linhasF[0].series;
            repeticoes = $scope.linhasF[0].repeticoes;
            intervalo = $scope.linhasF[0].intervalo;
        }
        $scope.insertF = {
            id: $scope.linhasE.length + 1,
            letra: 'F',
            exercicio: null,
            intervalo: intervalo,
            series: series,
            repeticoes: repeticoes,
            medida: 'rep',
            carga: null,
            observacao: null,
            ordem: $scope.linhasF.length + 1,
            medida_duracao: 'rep',
            medida_intensidade: 'kg',
            super_serie: parseInt(1),
            superserie: [],
            hash_code: hash_code
        };
        if ($scope.linhasF.length > 0) {
            if ($scope.linhasF[0].series == null) {
                swal("Opsss!", "Informe o número de séries do exercicio!", "error");
            } else {
                $scope.AtualizaPosicoesF($scope.insertF);
            }
        } else {
            $scope.AtualizaPosicoesF($scope.insertF);
        }

        $timeout(function () {
            $('#execicioSelectF' + $scope.insertF.id - 1).addClass('chosen-select');
            $('.chosen-select').chosen({width: "100%"});
        }, 500);

    };

    $scope.removeLinhaF = function (index, ficha_id) {
        $scope.linhasF.splice(index, 1);
        $scope.delFicha(ficha_id);
    };
    // FECHA LINHAS DA COLUNA F

    // LINHAS DA COLUNA G
    $scope.linhasG = [];
    $scope.addLinhaG = function () {
        var data = new Date();
        var dia = data.getDate();
        var mes = data.getMonth() + 1;
        var ano = data.getFullYear();
        var hora = data.getHours();
        var minuto = data.getMinutes();
        var segundo = data.getSeconds();
        var milisec = data.getMilliseconds();

        var hash_code = [ano, mes, dia, hora, minuto, segundo, milisec, 'G', $scope.linhasG.length + 1].join('');

        var series = '1';
        var repeticoes = null;
        var intervalo = null;
        if ($scope.linhasG.length > 0) {
            series = $scope.linhasG[0].series;
            repeticoes = $scope.linhasG[0].repeticoes;
            intervalo = $scope.linhasG[0].intervalo;
        }
        $scope.insertG = {
            id: $scope.linhasG.length + 1,
            letra: 'F',
            exercicio: null,
            intervalo: intervalo,
            series: series,
            repeticoes: repeticoes,
            medida: 'rep',
            carga: null,
            observacao: null,
            ordem: $scope.linhasG.length + 1,
            medida_duracao: 'rep',
            medida_intensidade: 'kg',
            super_serie: parseInt(1),
            superserie: [],
            hash_code: hash_code
        };
        if ($scope.linhasG.length > 0) {
            if ($scope.linhasG[0].series == null) {
                swal("Opsss!", "Informe o número de séries do exercicio!", "error");
            } else {
                $scope.AtualizaPosicoesG($scope.insertG);
            }
        } else {
            $scope.AtualizaPosicoesG($scope.insertG);
        }

        $timeout(function () {
            $('#execicioSelectG' + $scope.insertG.id - 1).addClass('chosen-select');
            $('.chosen-select').chosen({width: "100%"});
        }, 500);

    };

    $scope.removeLinhaG = function (index, ficha_id) {
        $scope.linhasG.splice(index, 1);
        $scope.delFicha(ficha_id);
    };
    // FECHA LINHAS DA COLUNA G
    // LISTA TODOS OS EXERCICIOS


    $scope.exercicios = {};
    
    $http({
        url: '../api/prescricao/getExercicios',
        method: "POST"
    }).then(function (response) {
        $scope.exercicios = response.data.exercicios;
        $scope.grupos = response.data.gruposmusculares;
    }, function (response) {
        console.log('Opsss... Algo deu errado!');
    });



    // INTERVALOS
    $scope.intervalosVetor = [];
    for (var i = 5; i < 304; ) {
        $scope.intervalo = {
            valor: i,
            texto: i + ' seg'
        };
        $scope.intervalosVetor.push($scope.intervalo);
        i = i + 5;
    }

    // SERIES
    $scope.seriesVetor = [];
    for (var i = 1; i <= 10; i++) {
        $scope.serie = {
            valor: i,
            texto: i + ' séries'
        };
        $scope.seriesVetor.push($scope.serie);
    }

    $scope.medidasVetor = [];
    for (var i = 1; i <= 3; i++) {
        if (i == 1) {
            $scope.medida = {
                valor: 'rep',
                texto: 'rep'
            };
        }
        if (i == 2) {
            $scope.medida = {
                valor: 'min',
                texto: 'min'
            };
        }
        if (i == 3) {
            $scope.medida = {
                valor: 'seg',
                texto: 'seg'
            };
        }

        $scope.medidasVetor.push($scope.medida);
    }
    $scope.delFicha = function (ficha_id) {
        $http({
            //    ../api/prescricao/addExercicioTreinoPadrao
            url: '../api/prescricao/delExercicioTreinoPadrao/' + ficha_id,
            method: "POST"
        }).then(function (response) {
        });
    };

    $scope.changeTreinoPadrao = function (treino_id) {

        $("#n_abas").show(120);
        $("#n_abas2").show(120);

        if (treino_id !== undefined) {
            // DESATIVA OS BOTOES
            $scope.statusBotoes(true);
            $scope.msgTreino = 'Aguarde... carregando treino!';
            $http({
                url: '../api/prescricao/getFichaPadrao',
                data: {
                    treino_id: treino_id
                },
                method: "POST"
            }).then(function (response) {
                $scope.addTab(response.data.treino_qtddias);
                $scope.treino_id = treino_id;
                $scope.fichaspadrao = response.data.treinofichas;
                $scope.treinopadrao = response.data.treinopadrao;
                $scope.numdias = response.data.treinopadrao[0].treino_qtddias;
                $scope.treino_qtddias = response.data.treinopadrao[0].treino_qtddias;
                $scope.treino_observacao = response.data.treinopadrao[0].treino_observacao;
                $scope.objetivo_id = response.data.treinopadrao[0].treino_objetivo;
                $scope.changeObjetivoMusculacao('1',$scope.objetivo_id);
                $scope.treino_nivel = response.data.treinopadrao[0].treino_nivel;
                $scope.treino_nome_padrao = response.data.treinopadrao[0].treino_nome_padrao;
                $scope.treino_sexo = response.data.treinopadrao[0].treino_sexo;
                if (response.data.treinopadrao[0].treino_qtddias == null) {
                    $scope.addTab(1);
                } else {
                    $scope.addTab(response.data.treinopadrao[0].treino_qtddias);
                }
                $scope.treino_qtddias = response.data.treinopadrao[0].treino_qtddias;
                $scope.semTreino = true;

                var tamanho = $scope.fichaspadrao.length;
                $scope.linhasA = []; // LIMPA VETOR A
                $scope.linhasB = []; // LIMPA VETOR B
                $scope.linhasC = []; // LIMPA VETOR C
                $scope.linhasD = []; // LIMPA VETOR D
                $scope.linhasE = []; // LIMPA VETOR E
                $scope.linhasF = []; // LIMPA VETOR F
                $scope.linhasG = []; // LIMPA VETOR G

                if (tamanho > 0) {
                    for (var i = 0; i < tamanho; i++) {
                        switch ($scope.fichaspadrao[i].letra) {

                            // CARREGA EXERCICIO NO TREINO A
                            case 'A':
                                $scope.insertA = {
                                    id: $scope.linhasA.length + 1,
                                    ficha: $scope.fichaspadrao[i].ficha,
                                    letra: $scope.fichaspadrao[i].letra,
                                    exercicio: $scope.fichaspadrao[i].exercicio,
                                    intervalo: $scope.fichaspadrao[i].intervalo,
                                    series: $scope.fichaspadrao[i].series,
                                    ordem: $scope.fichaspadrao[i].ordem,
                                    repeticoes: $scope.fichaspadrao[i].repeticoes,
                                    medida: $scope.fichaspadrao[i].medida,
                                    carga: $scope.fichaspadrao[i].carga,
                                    observacao: $scope.fichaspadrao[i].observacao,
                                    medida_duracao: $scope.fichaspadrao[i].medida_duracao,
                                    medida_intensidade: $scope.fichaspadrao[i].medida_intensidade,
                                    super_serie: parseInt($scope.fichaspadrao[i].super_serie),
                                    superserie: $scope.fichaspadrao[i].superserie,
                                    video: $scope.fichaspadrao[i].video,
                                    hash_code: $scope.gerarCode($scope.fichaspadrao[i].letra, i)

                                };
                                $scope.linhasA.push($scope.insertA);
                                $timeout(function () {
                                    for (var i = 0; i <= tamanho; i++) {
                                        $('#execicioSelectA' + i).addClass('chosen-select');
                                        $('.chosen-select').chosen({width: "100%"});

                                    }

                                }, 500);

                                break;
                                // CARREGA EXERCICIO NO TREINO B
                            case 'B':
                                $scope.insertB = {
                                    id: $scope.linhasB.length + 1,
                                    ficha: $scope.fichaspadrao[i].ficha,
                                    letra: $scope.fichaspadrao[i].letra,
                                    exercicio: $scope.fichaspadrao[i].exercicio,
                                    intervalo: $scope.fichaspadrao[i].intervalo,
                                    series: $scope.fichaspadrao[i].series,
                                    ordem: $scope.fichaspadrao[i].ordem,
                                    repeticoes: $scope.fichaspadrao[i].repeticoes,
                                    medida: $scope.fichaspadrao[i].medida,
                                    carga: $scope.fichaspadrao[i].carga,
                                    observacao: $scope.fichaspadrao[i].observacao,
                                    medida_duracao: $scope.fichaspadrao[i].medida_duracao,
                                    medida_intensidade: $scope.fichaspadrao[i].medida_intensidade,
                                    super_serie: parseInt($scope.fichaspadrao[i].super_serie),
                                    superserie: $scope.fichaspadrao[i].superserie,
                                    video: $scope.fichaspadrao[i].video,
                                    hash_code: $scope.gerarCode($scope.fichaspadrao[i].letra, i)
                                };
                                $scope.linhasB.push($scope.insertB);
                                $timeout(function () {
                                    for (var i = 0; i <= tamanho; i++) {
                                        $('#execicioSelectB' + i).addClass('chosen-select');
                                        $('.chosen-select').chosen({width: "100%"});
                                    }
                                }, 500);
                                break;
                                // CARREGA EXERCICIO NO TREINO C
                            case 'C':
                                $scope.insertC = {
                                    id: $scope.linhasC.length + 1,
                                    ficha: $scope.fichaspadrao[i].ficha,
                                    letra: $scope.fichaspadrao[i].letra,
                                    exercicio: $scope.fichaspadrao[i].exercicio,
                                    intervalo: $scope.fichaspadrao[i].intervalo,
                                    series: $scope.fichaspadrao[i].series,
                                    ordem: $scope.fichaspadrao[i].ordem,
                                    repeticoes: $scope.fichaspadrao[i].repeticoes,
                                    medida: $scope.fichaspadrao[i].medida,
                                    carga: $scope.fichaspadrao[i].carga,
                                    observacao: $scope.fichaspadrao[i].observacao,
                                    medida_duracao: $scope.fichaspadrao[i].medida_duracao,
                                    medida_intensidade: $scope.fichaspadrao[i].medida_intensidade,
                                    super_serie: parseInt($scope.fichaspadrao[i].super_serie),
                                    superserie: $scope.fichaspadrao[i].superserie,
                                    video: $scope.fichaspadrao[i].video,
                                    hash_code: $scope.gerarCode($scope.fichaspadrao[i].letra, i)
                                };
                                $scope.linhasC.push($scope.insertC);
                                $timeout(function () {
                                    for (var i = 0; i <= tamanho; i++) {
                                        $('#execicioSelectC' + i).addClass('chosen-select');
                                        $('.chosen-select').chosen({width: "100%"});
                                    }
                                }, 500);
                                break;
                                // CARREGA EXERCICIO NO TREINO D
                            case 'D':
                                $scope.insertD = {
                                    id: $scope.linhasD.length + 1,
                                    ficha: $scope.fichaspadrao[i].ficha,
                                    letra: $scope.fichaspadrao[i].letra,
                                    exercicio: $scope.fichaspadrao[i].exercicio,
                                    intervalo: $scope.fichaspadrao[i].intervalo,
                                    series: $scope.fichaspadrao[i].series,
                                    ordem: $scope.fichaspadrao[i].ordem,
                                    repeticoes: $scope.fichaspadrao[i].repeticoes,
                                    medida: $scope.fichaspadrao[i].medida,
                                    carga: $scope.fichaspadrao[i].carga,
                                    observacao: $scope.fichaspadrao[i].observacao,
                                    medida_duracao: $scope.fichaspadrao[i].medida_duracao,
                                    medida_intensidade: $scope.fichaspadrao[i].medida_intensidade,
                                    super_serie: parseInt($scope.fichaspadrao[i].super_serie),
                                    superserie: $scope.fichaspadrao[i].superserie,
                                    video: $scope.fichaspadrao[i].video,
                                    hash_code: $scope.gerarCode($scope.fichaspadrao[i].letra, i)
                                };
                                $scope.linhasD.push($scope.insertD);
                                $timeout(function () {
                                    for (var i = 0; i <= tamanho; i++) {
                                        $('#execicioSelectD' + i).addClass('chosen-select');
                                        $('.chosen-select').chosen({width: "100%"});
                                    }
                                }, 500);
                                break;
                                // CARREGA EXERCICIO NO TREINO E
                            case 'E':
                                $scope.insertE = {
                                    id: $scope.linhasE.length + 1,
                                    ficha: $scope.fichaspadrao[i].ficha,
                                    letra: $scope.fichaspadrao[i].letra,
                                    exercicio: $scope.fichaspadrao[i].exercicio,
                                    intervalo: $scope.fichaspadrao[i].intervalo,
                                    series: $scope.fichaspadrao[i].series,
                                    ordem: $scope.fichaspadrao[i].ordem,
                                    repeticoes: $scope.fichaspadrao[i].repeticoes,
                                    medida: $scope.fichaspadrao[i].medida,
                                    carga: $scope.fichaspadrao[i].carga,
                                    observacao: $scope.fichaspadrao[i].observacao,
                                    medida_duracao: $scope.fichaspadrao[i].medida_duracao,
                                    medida_intensidade: $scope.fichaspadrao[i].medida_intensidade,
                                    super_serie: parseInt($scope.fichaspadrao[i].super_serie),
                                    superserie: $scope.fichaspadrao[i].superserie,
                                    video: $scope.fichaspadrao[i].video,
                                    hash_code: $scope.gerarCode($scope.fichaspadrao[i].letra, i)
                                };
                                $scope.linhasE.push($scope.insertE);
                                $timeout(function () {
                                    for (var i = 0; i <= tamanho; i++) {
                                        $('#execicioSelectE' + i).addClass('chosen-select');
                                        $('.chosen-select').chosen({width: "100%"});
                                    }
                                }, 500);
                                break;
                                // CARREGA EXERCICIO NO TREINO F
                            case 'F':
                                $scope.insertF = {
                                    id: $scope.linhasF.length + 1,
                                    ficha: $scope.fichaspadrao[i].ficha,
                                    letra: $scope.fichaspadrao[i].letra,
                                    exercicio: $scope.fichaspadrao[i].exercicio,
                                    intervalo: $scope.fichaspadrao[i].intervalo,
                                    series: $scope.fichaspadrao[i].series,
                                    ordem: $scope.fichaspadrao[i].ordem,
                                    repeticoes: $scope.fichaspadrao[i].repeticoes,
                                    medida: $scope.fichaspadrao[i].medida,
                                    carga: $scope.fichaspadrao[i].carga,
                                    observacao: $scope.fichaspadrao[i].observacao,
                                    medida_duracao: $scope.fichaspadrao[i].medida_duracao,
                                    medida_intensidade: $scope.fichaspadrao[i].medida_intensidade,
                                    super_serie: parseInt($scope.fichaspadrao[i].super_serie),
                                    superserie: $scope.fichaspadrao[i].superserie,
                                    video: $scope.fichaspadrao[i].video,
                                    hash_code: $scope.gerarCode($scope.fichaspadrao[i].letra, i)
                                };
                                $scope.linhasF.push($scope.insertF);
                                $timeout(function () {
                                    for (var i = 0; i <= tamanho; i++) {
                                        $('#execicioSelectF' + i).addClass('chosen-select');
                                        $('.chosen-select').chosen({width: "100%"});
                                    }
                                }, 500);
                                break;
                                // CARREGA EXERCICIO NO TREINO G
                            case 'G':
                                $scope.insertG = {
                                    id: $scope.linhasG.length + 1,
                                    ficha: $scope.fichaspadrao[i].ficha,
                                    letra: $scope.fichaspadrao[i].letra,
                                    exercicio: $scope.fichaspadrao[i].exercicio,
                                    intervalo: $scope.fichaspadrao[i].intervalo,
                                    series: $scope.fichaspadrao[i].series,
                                    ordem: $scope.fichaspadrao[i].ordem,
                                    repeticoes: $scope.fichaspadrao[i].repeticoes,
                                    medida: $scope.fichaspadrao[i].medida,
                                    carga: $scope.fichaspadrao[i].carga,
                                    observacao: $scope.fichaspadrao[i].observacao,
                                    medida_duracao: $scope.fichaspadrao[i].medida_duracao,
                                    medida_intensidade: $scope.fichaspadrao[i].medida_intensidade,
                                    super_serie: parseInt($scope.fichaspadrao[i].super_serie),
                                    superserie: $scope.fichaspadrao[i].superserie,
                                    video: $scope.fichaspadrao[i].video,
                                    hash_code: $scope.gerarCode($scope.fichaspadrao[i].letra, i)
                                };
                                $scope.linhasG.push($scope.insertG);
                                $timeout(function () {
                                    for (var i = 0; i <= tamanho; i++) {
                                        $('#execicioSelectG' + i).addClass('chosen-select');
                                        $('.chosen-select').chosen({width: "100%"});
                                    }
                                }, 500);
                                break;
                        }

                    }
                }
                $scope.msgTreino = '';
                // ATIVA OS BOTÕES
                $scope.statusBotoes(false);
                //}, function(response) {
                //	console.log('Opsss... Algo deu errado!');
                //});
                // FECHA BUSCA EXERCICIOS DO TREINO
            }, function (response) {
                $scope.linhasA = []; // LIMPA VETOR A
                $scope.linhasB = []; // LIMPA VETOR B
                $scope.linhasC = []; // LIMPA VETOR C
                $scope.linhasD = []; // LIMPA VETOR D
                $scope.linhasE = []; // LIMPA VETOR E
                $scope.linhasF = []; // LIMPA VETOR F
                $scope.linhasG = []; // LIMPA VETOR G
                $scope.msgTreino = '';
                console.log('Opsss... Algo deu errado!');
            });
        }
    };

    // DELETA TREINO
    $scope.deleteTreino = function (treino_id) {

        if (treino_id !== undefined) {

        swal({
 title: "Tem certeza?",
                text: "Tem certeza que deseja deletar esse treino?",
  icon: "warning",
  buttons: true,
  buttons: {
      cancel: {
        text: "Cancelar",
        value: null,
        visible: true,
        className: "",
        closeModal: true,
      },
      confirm: {
        text: "Sim, tenho certeza!",
        value: true,
        visible: true,
        className: "",
        closeModal: true
      }},
  dangerMode: true,

}).then((willDelete) => {
  if (willDelete) {
    // aqui chama o ajax
     $http({
                    url: '../../api/prescricao/delTreino/' + treino_id,
                    method: "POST"
                }).then(function (response) {
                    $scope.listaTreinosAluno();
                    swal("Deletado!", "Treino deletado com sucesso!", "success");
                });
  } else {
    // swal("Your imaginary file is safe!");
  }
});
// fim swal
// 
        } else {
            swal("Opsss!", "Selecione um treino para deletar!", "error");
        }
    };


    $scope.delTreinoPadrao = function (id) {
        if (id !== undefined) {
            swal({
  title: "Tem certeza?",
                text: "Tem certeza que deseja deletar esse treino padrão?",
  icon: "warning",
  buttons: true,
  buttons: {
      cancel: {
        text: "Cancelar",
        value: null,
        visible: true,
        className: "",
        closeModal: true,
      },
      confirm: {
        text: "Sim, tenho certeza!",
        value: true,
        visible: true,
        className: "",
        closeModal: true
      }},
  dangerMode: true,

}).then((willDelete) => {
  if (willDelete) {
    // aqui chama o ajax
    $http({
                    url: '../api/prescricao/delTreinoPadrao/' + id,
                    method: "POST"
                }).then(function (response) {
                    getPadrao();

                    swal("Deletado!", "Treino padrão deletado com sucesso!", "success");
                });
  } else {
    // swal("Your imaginary file is safe!");
  }
});
// fim swal
// 
         
        } else {
            swal("Opsss!", "Selecione um treino para deletar!", "error");
        }
    };


    // CALCULA TEMPO ESTIMADO
    $scope.calcTempoEstimadoA = function (linhaA) {
        if (linhaA.series != null && linhaA.repeticoes != null) {
            if (linhaA.intervalo == null) {
                linhaA.intervalo = 0;
            }
            $scope.numExerciciosA += 1;
            $scope.alongamentoA = parseInt($scope.alongamentoA) + parseInt(linhaA.series);
            $scope.num_repeticoesA = parseInt($scope.num_repeticoesA) + (parseInt(linhaA.series) * parseInt(linhaA.repeticoes));
            $scope.num_repeticoesA13 = parseInt($scope.num_repeticoesA) * 1.3;
            $scope.seg_inter_exerciciosA = parseInt($scope.numExerciciosA) * 30;
            $scope.seg_inter_seriesA = parseInt($scope.alongamentoA) * parseInt(linhaA.intervalo);
            $scope.seg_aquecimentoA = 600;
            $scope.seg_alongamentoA = 240;
            $scope.total_segA = parseInt($scope.num_repeticoesA13) + parseInt($scope.seg_inter_exerciciosA) + parseInt($scope.seg_inter_seriesA) + parseInt($scope.seg_aquecimentoA) + parseInt($scope.seg_alongamentoA);
            $scope.total_minA = parseInt($scope.total_segA) / 60;
            $scope.tempoestimadoA = parseFloat($scope.total_minA.toFixed(1));
            $scope.tempomusculacaoA = parseFloat($scope.total_minA.toFixed(1)) - 14; // 14 min de aquecimento e alongamento
            $scope.tempomusculacaoA = parseFloat($scope.tempomusculacaoA.toFixed(1));

            // MONTA HORA
            var valuesA = String($scope.tempoestimadoA).split(".");
            $scope.horaEstimadoA = parseInt(valuesA[0]);
            if (angular.isUndefined(valuesA[1])) {
                $scope.minEstimadoA = '00';
            } else {
                $scope.minEstimadoA = 60 * (parseInt(valuesA[1] + 0) / 100);
                if (parseInt($scope.minEstimadoA) < 10) {
                    $scope.minEstimadoA = '0' + $scope.minEstimadoA;
                }
            }

            var valuesAb = String($scope.tempomusculacaoA).split(".");
            $scope.horaMusculacaoA = parseInt(valuesAb[0]);
            if (angular.isUndefined(valuesAb[1])) {
                $scope.minMusculacaoA = '00';
            } else {
                $scope.minMusculacaoA = 60 * (parseInt(valuesA[1] + 0) / 100);
                if (parseInt($scope.minMusculacaoA) < 10) {
                    $scope.minMusculacaoA = '0' + $scope.minMusculacaoA;
                }
            }

            $scope.tempoestimadoA = $scope.horaEstimadoA + ':' + $scope.minEstimadoA;
            $scope.tempomusculacaoA = $scope.horaMusculacaoA + ':' + $scope.minMusculacaoA;
        }
    };

    $scope.calcTempoA = function () {
        $scope.numExerciciosA = 3; // 3 somados com aquecimento e alongamento + 1
        $scope.tempoestimadoA = 0;
        $scope.tempomusculacaoA = 0;
        $scope.alongamentoA = 0;
        $scope.num_repeticoesA = 0;
        $scope.num_repeticoesA13 = 0;
        $scope.seg_inter_exerciciosA = 0;
        $scope.seg_inter_seriesA = 0;
        $scope.seg_aquecimentoA = 0;
        $scope.seg_alongamentoA = 0;
        $scope.total_segA = 0;
        $scope.total_minA = 0;
        var tamanho = $scope.linhasA.length;
        for (var i = 0; i < tamanho; i++) {
            $scope.calcTempoEstimadoA($scope.linhasA[i]);
        }
    };

    $scope.calcTempoEstimadoB = function (linhaB) {
        if (linhaB.series != null && linhaB.repeticoes != null) {
            if (linhaB.intervalo == null) {
                linhaB.intervalo = 0;
            }
            $scope.numExerciciosB += 1;
            $scope.alongamentoB = parseInt($scope.alongamentoB) + parseInt(linhaB.series);
            $scope.num_repeticoesB = parseInt($scope.num_repeticoesB) + (parseInt(linhaB.series) * parseInt(linhaB.repeticoes));
            $scope.num_repeticoesB13 = parseInt($scope.num_repeticoesB) * 1.3;
            $scope.seg_inter_exerciciosB = parseInt($scope.numExerciciosB) * 30;
            $scope.seg_inter_seriesB = parseInt($scope.alongamentoB) * parseInt(linhaB.intervalo);
            $scope.seg_aquecimentoB = 600;
            $scope.seg_alongamentoB = 240;
            $scope.total_segB = parseInt($scope.num_repeticoesB13) + parseInt($scope.seg_inter_exerciciosB) + parseInt($scope.seg_inter_seriesB) + parseInt($scope.seg_aquecimentoB) + parseInt($scope.seg_alongamentoB);
            $scope.total_minB = parseInt($scope.total_segB) / 60;
            $scope.tempoestimadoB = parseFloat($scope.total_minB.toFixed(1));
            $scope.tempomusculacaoB = parseFloat($scope.total_minB.toFixed(1)) - 14; // 14 min de aquecimento e alongamento
            $scope.tempomusculacaoB = parseFloat($scope.tempomusculacaoB.toFixed(1));

            // MONTA HORA
            var valuesB = String($scope.tempoestimadoB).split(".");
            $scope.horaEstimadoB = parseInt(valuesB[0]);
            if (angular.isUndefined(valuesB[1])) {
                $scope.minEstimadoB = '00';
            } else {
                $scope.minEstimadoB = 60 * (parseInt(valuesB[1] + 0) / 100);
                if (parseInt($scope.minEstimadoB) < 10) {
                    $scope.minEstimadoB = '0' + $scope.minEstimadoB;
                }
            }

            var valuesBb = String($scope.tempomusculacaoB).split(".");
            $scope.horaMusculacaoB = parseInt(valuesBb[0]);
            if (angular.isUndefined(valuesBb[1])) {
                $scope.minMusculacaoB = '00';
            } else {
                $scope.minMusculacaoB = 60 * (parseInt(valuesB[1] + 0) / 100);
                if (parseInt($scope.minMusculacaoB) < 10) {
                    $scope.minMusculacaoB = '0' + $scope.minMusculacaoB;
                }
            }

            $scope.tempoestimadoB = $scope.horaEstimadoB + ':' + $scope.minEstimadoB;
            $scope.tempomusculacaoB = $scope.horaMusculacaoB + ':' + $scope.minMusculacaoB;
        }
    };

    $scope.calcTempoB = function () {
        $scope.numExerciciosB = 3; // 3 somados com aquecimento e alongamento + 1
        $scope.tempoestimadoB = 0;
        $scope.tempomusculacaoB = 0;
        $scope.alongamentoB = 0;
        $scope.num_repeticoesB = 0;
        $scope.num_repeticoesB13 = 0;
        $scope.seg_inter_exerciciosB = 0;
        $scope.seg_inter_seriesB = 0;
        $scope.seg_aquecimentoB = 0;
        $scope.seg_alongamentoB = 0;
        $scope.total_segB = 0;
        $scope.total_minB = 0;
        var tamanho = $scope.linhasB.length;
        for (var i = 0; i < tamanho; i++) {
            $scope.calcTempoEstimadoB($scope.linhasB[i]);
        }
    };

    $scope.calcTempoEstimadoC = function (linhaC) {
        if (linhaC.series != null && linhaC.repeticoes != null) {
            if (linhaC.intervalo == null) {
                linhaC.intervalo = 0;
            }
            $scope.numExerciciosC += 1;
            $scope.alongamentoC = parseInt($scope.alongamentoC) + parseInt(linhaC.series);
            $scope.num_repeticoesC = parseInt($scope.num_repeticoesC) + (parseInt(linhaC.series) * parseInt(linhaC.repeticoes));
            $scope.num_repeticoesC13 = parseInt($scope.num_repeticoesC) * 1.3;
            $scope.seg_inter_exerciciosC = parseInt($scope.numExerciciosC) * 30;
            $scope.seg_inter_seriesC = parseInt($scope.alongamentoC) * parseInt(linhaC.intervalo);
            $scope.seg_aquecimentoC = 600;
            $scope.seg_alongamentoC = 240;
            $scope.total_segC = parseInt($scope.num_repeticoesC13) + parseInt($scope.seg_inter_exerciciosC) + parseInt($scope.seg_inter_seriesC) + parseInt($scope.seg_aquecimentoC) + parseInt($scope.seg_alongamentoC);
            $scope.total_minC = parseInt($scope.total_segC) / 60;
            $scope.tempoestimadoC = parseFloat($scope.total_minC.toFixed(1));
            $scope.tempomusculacaoC = parseFloat($scope.total_minC.toFixed(1)) - 14; // 14 min de aquecimento e alongamento
            $scope.tempomusculacaoC = parseFloat($scope.tempomusculacaoC.toFixed(1));

            // MONTA HORA
            var valuesC = String($scope.tempoestimadoC).split(".");
            $scope.horaEstimadoC = parseInt(valuesC[0]);
            if (angular.isUndefined(valuesC[1])) {
                $scope.minEstimadoC = '00';
            } else {
                $scope.minEstimadoC = 60 * (parseInt(valuesC[1] + 0) / 100);
                if (parseInt($scope.minEstimadoC) < 10) {
                    $scope.minEstimadoC = '0' + $scope.minEstimadoC;
                }
            }

            var valuesCb = String($scope.tempomusculacaoC).split(".");
            $scope.horaMusculacaoC = parseInt(valuesCb[0]);
            if (angular.isUndefined(valuesCb[1])) {
                $scope.minMusculacaoC = '00';
            } else {
                $scope.minMusculacaoC = 60 * (parseInt(valuesC[1] + 0) / 100);
                if (parseInt($scope.minMusculacaoC) < 10) {
                    $scope.minMusculacaoC = '0' + $scope.minMusculacaoC;
                }
            }

            $scope.tempoestimadoC = $scope.horaEstimadoC + ':' + $scope.minEstimadoC;
            $scope.tempomusculacaoC = $scope.horaMusculacaoC + ':' + $scope.minMusculacaoC;
        }
    };

    $scope.calcTempoC = function () {
        $scope.numExerciciosC = 3; // 3 somados com aquecimento e alongamento + 1
        $scope.tempoestimadoC = 0;
        $scope.tempomusculacaoC = 0;
        $scope.alongamentoC = 0;
        $scope.num_repeticoesC = 0;
        $scope.num_repeticoesC13 = 0;
        $scope.seg_inter_exerciciosC = 0;
        $scope.seg_inter_seriesC = 0;
        $scope.seg_aquecimentoC = 0;
        $scope.seg_alongamentoC = 0;
        $scope.total_segC = 0;
        $scope.total_minC = 0;
        var tamanho = $scope.linhasC.length;
        for (var i = 0; i < tamanho; i++) {
            $scope.calcTempoEstimadoC($scope.linhasC[i]);
        }
    };

    $scope.calcTempoEstimadoD = function (linhaD) {
        if (linhaD.series != null && linhaD.repeticoes != null) {
            if (linhaD.intervalo == null) {
                linhaD.intervalo = 0;
            }
            $scope.numExerciciosD += 1;
            $scope.alongamentoD = parseInt($scope.alongamentoD) + parseInt(linhaD.series);
            $scope.num_repeticoesD = parseInt($scope.num_repeticoesD) + (parseInt(linhaD.series) * parseInt(linhaD.repeticoes));
            $scope.num_repeticoesD13 = parseInt($scope.num_repeticoesD) * 1.3;
            $scope.seg_inter_exerciciosD = parseInt($scope.numExerciciosD) * 30;
            $scope.seg_inter_seriesD = parseInt($scope.alongamentoD) * parseInt(linhaD.intervalo);
            $scope.seg_aquecimentoD = 600;
            $scope.seg_alongamentoD = 240;
            $scope.total_segD = parseInt($scope.num_repeticoesD13) + parseInt($scope.seg_inter_exerciciosD) + parseInt($scope.seg_inter_seriesD) + parseInt($scope.seg_aquecimentoD) + parseInt($scope.seg_alongamentoD);
            $scope.total_minD = parseInt($scope.total_segD) / 60;
            $scope.tempoestimadoD = parseFloat($scope.total_minD.toFixed(1));
            $scope.tempomusculacaoD = parseFloat($scope.total_minD.toFixed(1)) - 14; // 14 min de aquecimento e alongamento
            $scope.tempomusculacaoD = parseFloat($scope.tempomusculacaoD.toFixed(1));

            // MONTA HORA
            var valuesD = String($scope.tempoestimadoD).split(".");
            $scope.horaEstimadoD = parseInt(valuesD[0]);
            if (angular.isUndefined(valuesD[1])) {
                $scope.minEstimadoD = '00';
            } else {
                $scope.minEstimadoD = 60 * (parseInt(valuesD[1] + 0) / 100);
                if (parseInt($scope.minEstimadoD) < 10) {
                    $scope.minEstimadoD = '0' + $scope.minEstimadoD;
                }
            }

            var valuesDb = String($scope.tempomusculacaoD).split(".");
            $scope.horaMusculacaoD = parseInt(valuesDb[0]);
            if (angular.isUndefined(valuesDb[1])) {
                $scope.minMusculacaoD = '00';
            } else {
                $scope.minMusculacaoD = 60 * (parseInt(valuesD[1] + 0) / 100);
                if (parseInt($scope.minMusculacaoD) < 10) {
                    $scope.minMusculacaoD = '0' + $scope.minMusculacaoD;
                }
            }

            $scope.tempoestimadoD = $scope.horaEstimadoD + ':' + $scope.minEstimadoD;
            $scope.tempomusculacaoD = $scope.horaMusculacaoD + ':' + $scope.minMusculacaoD;
        }
    };

    $scope.calcTempoD = function () {
        $scope.numExerciciosD = 3; // 3 somados com aquecimento e alongamento + 1
        $scope.tempoestimadoD = 0;
        $scope.tempomusculacaoD = 0;
        $scope.alongamentoD = 0;
        $scope.num_repeticoesD = 0;
        $scope.num_repeticoesD13 = 0;
        $scope.seg_inter_exerciciosD = 0;
        $scope.seg_inter_seriesD = 0;
        $scope.seg_aquecimentoD = 0;
        $scope.seg_alongamentoD = 0;
        $scope.total_segD = 0;
        $scope.total_minD = 0;
        var tamanho = $scope.linhasD.length;
        for (var i = 0; i < tamanho; i++) {
            $scope.calcTempoEstimadoD($scope.linhasD[i]);
        }
    };

    $scope.calcTempoEstimadoE = function (linhaE) {
        if (linhaE.series != null && linhaE.repeticoes != null) {
            if (linhaE.intervalo == null) {
                linhaE.intervalo = 0;
            }
            $scope.numExerciciosE += 1;
            $scope.alongamentoE = parseInt($scope.alongamentoE) + parseInt(linhaE.series);
            $scope.num_repeticoesE = parseInt($scope.num_repeticoesE) + (parseInt(linhaE.series) * parseInt(linhaE.repeticoes));
            $scope.num_repeticoesE13 = parseInt($scope.num_repeticoesE) * 1.3;
            $scope.seg_inter_exerciciosE = parseInt($scope.numExerciciosE) * 30;
            $scope.seg_inter_seriesE = parseInt($scope.alongamentoE) * parseInt(linhaE.intervalo);
            $scope.seg_aquecimentoE = 600;
            $scope.seg_alongamentoE = 240;
            $scope.total_segE = parseInt($scope.num_repeticoesE13) + parseInt($scope.seg_inter_exerciciosE) + parseInt($scope.seg_inter_seriesE) + parseInt($scope.seg_aquecimentoE) + parseInt($scope.seg_alongamentoE);
            $scope.total_minE = parseInt($scope.total_segE) / 60;
            $scope.tempoestimadoE = parseFloat($scope.total_minE.toFixed(1));
            $scope.tempomusculacaoE = parseFloat($scope.total_minE.toFixed(1)) - 14; // 14 min de aquecimento e alongamento
            $scope.tempomusculacaoE = parseFloat($scope.tempomusculacaoE.toFixed(1));

            // MONTA HORA
            var valuesE = String($scope.tempoestimadoE).split(".");
            $scope.horaEstimadoE = parseInt(valuesE[0]);
            if (angular.isUndefined(valuesE[1])) {
                $scope.minEstimadoE = '00';
            } else {
                $scope.minEstimadoE = 60 * (parseInt(valuesE[1] + 0) / 100);
                if (parseInt($scope.minEstimadoE) < 10) {
                    $scope.minEstimadoE = '0' + $scope.minEstimadoE;
                }
            }

            var valuesEb = String($scope.tempomusculacaoE).split(".");
            $scope.horaMusculacaoE = parseInt(valuesEb[0]);
            if (angular.isUndefined(valuesEb[1])) {
                $scope.minMusculacaoE = '00';
            } else {
                $scope.minMusculacaoE = 60 * (parseInt(valuesE[1] + 0) / 100);
                if (parseInt($scope.minMusculacaoE) < 10) {
                    $scope.minMusculacaoE = '0' + $scope.minMusculacaoE;
                }
            }

            $scope.tempoestimadoE = $scope.horaEstimadoE + ':' + $scope.minEstimadoE;
            $scope.tempomusculacaoE = $scope.horaMusculacaoE + ':' + $scope.minMusculacaoE;
        }
    };

    $scope.calcTempoE = function () {
        $scope.numExerciciosE = 3; // 3 somados com aquecimento e alongamento + 1
        $scope.tempoestimadoE = 0;
        $scope.tempomusculacaoE = 0;
        $scope.alongamentoE = 0;
        $scope.num_repeticoesE = 0;
        $scope.num_repeticoesE13 = 0;
        $scope.seg_inter_exerciciosE = 0;
        $scope.seg_inter_seriesE = 0;
        $scope.seg_aquecimentoE = 0;
        $scope.seg_alongamentoE = 0;
        $scope.total_segE = 0;
        $scope.total_minE = 0;
        var tamanho = $scope.linhasE.length;
        for (var i = 0; i < tamanho; i++) {
            $scope.calcTempoEstimadoE($scope.linhasE[i]);
        }
    };

    $scope.calcTempoEstimadoF = function (linhaF) {
        if (linhaF.series != null && linhaF.repeticoes != null) {
            if (linhaF.intervalo == null) {
                linhaF.intervalo = 0;
            }
            $scope.numExerciciosF += 1;
            $scope.alongamentoF = parseInt($scope.alongamentoF) + parseInt(linhaF.series);
            $scope.num_repeticoesF = parseInt($scope.num_repeticoesF) + (parseInt(linhaF.series) * parseInt(linhaF.repeticoes));
            $scope.num_repeticoesF13 = parseInt($scope.num_repeticoesF) * 1.3;
            $scope.seg_inter_exerciciosF = parseInt($scope.numExerciciosF) * 30;
            $scope.seg_inter_seriesF = parseInt($scope.alongamentoF) * parseInt(linhaF.intervalo);
            $scope.seg_aquecimentoF = 600;
            $scope.seg_alongamentoF = 240;
            $scope.total_segF = parseInt($scope.num_repeticoesF13) + parseInt($scope.seg_inter_exerciciosF) + parseInt($scope.seg_inter_seriesF) + parseInt($scope.seg_aquecimentoF) + parseInt($scope.seg_alongamentoF);
            $scope.total_minF = parseInt($scope.total_segF) / 60;
            $scope.tempoestimadoF = parseFloat($scope.total_minF.toFixed(1));
            $scope.tempomusculacaoF = parseFloat($scope.total_minF.toFixed(1)) - 14; // 14 min de aquecimento e alongamento
            $scope.tempomusculacaoF = parseFloat($scope.tempomusculacaoF.toFixed(1));

            // MONTA HORA
            var valuesF = String($scope.tempoestimadoF).split(".");
            $scope.horaEstimadoF = parseInt(valuesF[0]);
            if (angular.isUndefined(valuesF[1])) {
                $scope.minEstimadoF = '00';
            } else {
                $scope.minEstimadoF = 60 * (parseInt(valuesF[1] + 0) / 100);
                if (parseInt($scope.minEstimadoF) < 10) {
                    $scope.minEstimadoF = '0' + $scope.minEstimadoF;
                }
            }

            var valuesFb = String($scope.tempomusculacaoF).split(".");
            $scope.horaMusculacaoF = parseInt(valuesFb[0]);
            if (angular.isUndefined(valuesFb[1])) {
                $scope.minMusculacaoF = '00';
            } else {
                $scope.minMusculacaoF = 60 * (parseInt(valuesF[1] + 0) / 100);
                if (parseInt($scope.minMusculacaoF) < 10) {
                    $scope.minMusculacaoF = '0' + $scope.minMusculacaoF;
                }
            }

            $scope.tempoestimadoF = $scope.horaEstimadoF + ':' + $scope.minEstimadoF;
            $scope.tempomusculacaoF = $scope.horaMusculacaoF + ':' + $scope.minMusculacaoF;
        }
    };

    $scope.calcTempoF = function () {
        $scope.numExerciciosF = 3; // 3 somados com aquecimento e alongamento + 1
        $scope.tempoestimadoF = 0;
        $scope.tempomusculacaoF = 0;
        $scope.alongamentoF = 0;
        $scope.num_repeticoesF = 0;
        $scope.num_repeticoesF13 = 0;
        $scope.seg_inter_exerciciosF = 0;
        $scope.seg_inter_seriesF = 0;
        $scope.seg_aquecimentoF = 0;
        $scope.seg_alongamentoF = 0;
        $scope.total_segF = 0;
        $scope.total_minF = 0;
        var tamanho = $scope.linhasF.length;
        for (var i = 0; i < tamanho; i++) {
            $scope.calcTempoEstimadoF($scope.linhasF[i]);
        }
    };

    $scope.calcTempoEstimadoG = function (linhaG) {
        if (linhaG.series != null && linhaG.repeticoes != null) {
            if (linhaG.intervalo == null) {
                linhaG.intervalo = 0;
            }
            $scope.numExerciciosG += 1;
            $scope.alongamentoG = parseInt($scope.alongamentoG) + parseInt(linhaG.series);
            $scope.num_repeticoesG = parseInt($scope.num_repeticoesG) + (parseInt(linhaG.series) * parseInt(linhaG.repeticoes));
            $scope.num_repeticoesG13 = parseInt($scope.num_repeticoesG) * 1.3;
            $scope.seg_inter_exerciciosG = parseInt($scope.numExerciciosG) * 30;
            $scope.seg_inter_seriesG = parseInt($scope.alongamentoG) * parseInt(linhaG.intervalo);
            $scope.seg_aquecimentoG = 600;
            $scope.seg_alongamentoG = 240;
            $scope.total_segG = parseInt($scope.num_repeticoesG13) + parseInt($scope.seg_inter_exerciciosG) + parseInt($scope.seg_inter_seriesG) + parseInt($scope.seg_aquecimentoG) + parseInt($scope.seg_alongamentoG);
            $scope.total_minG = parseInt($scope.total_segG) / 60;
            $scope.tempoestimadoG = parseFloat($scope.total_minG.toFixed(1));
            $scope.tempomusculacaoG = parseFloat($scope.total_minG.toFixed(1)) - 14; // 14 min de aquecimento e alongamento
            $scope.tempomusculacaoG = parseFloat($scope.tempomusculacaoG.toFixed(1));

            // MONTA HORA
            var valuesG = String($scope.tempoestimadoG).split(".");
            $scope.horaEstimadoG = parseInt(valuesG[0]);
            if (angular.isUndefined(valuesG[1])) {
                $scope.minEstimadoG = '00';
            } else {
                $scope.minEstimadoG = 60 * (parseInt(valuesG[1] + 0) / 100);
                if (parseInt($scope.minEstimadoG) < 10) {
                    $scope.minEstimadoG = '0' + $scope.minEstimadoG;
                }
            }

            var valuesGb = String($scope.tempomusculacaoG).split(".");
            $scope.horaMusculacaoG = parseInt(valuesGb[0]);
            if (angular.isUndefined(valuesGb[1])) {
                $scope.minMusculacaoG = '00';
            } else {
                $scope.minMusculacaoG = 60 * (parseInt(valuesG[1] + 0) / 100);
                if (parseInt($scope.minMusculacaoG) < 10) {
                    $scope.minMusculacaoG = '0' + $scope.minMusculacaoG;
                }
            }

            $scope.tempoestimadoG = $scope.horaEstimadoG + ':' + $scope.minEstimadoG;
            $scope.tempomusculacaoG = $scope.horaMusculacaoG + ':' + $scope.minMusculacaoG;
        }
    };

    $scope.calcTempoG = function () {
        $scope.numExerciciosG = 3; // 3 somados com aquecimento e alongamento + 1
        $scope.tempoestimadoG = 0;
        $scope.tempomusculacaoG = 0;
        $scope.alongamentoG = 0;
        $scope.num_repeticoesG = 0;
        $scope.num_repeticoesG13 = 0;
        $scope.seg_inter_exerciciosG = 0;
        $scope.seg_inter_seriesG = 0;
        $scope.seg_aquecimentoG = 0;
        $scope.seg_alongamentoG = 0;
        $scope.total_segG = 0;
        $scope.total_minG = 0;
        var tamanho = $scope.linhasG.length;
        for (var i = 0; i < tamanho; i++) {
            $scope.calcTempoEstimadoG($scope.linhasG[i]);
        }
    };

    $timeout(function () {
        $scope.calcTempoA();
        $scope.calcTempoB();
        $scope.calcTempoC();
        $scope.calcTempoD();
        $scope.calcTempoE();
        $scope.calcTempoF();
        $scope.calcTempoG();
    }, 5000);


    $scope.$watch('inputVal', function (val) {
        if (val) {
            $scope.exibePrescricao = true;
            $scope.client_id = val;


            // BUSCA DASDOS DO CLIENTE
            $scope.client = {};
            $http({
                url: '../../api/prescricao/getClient',
                data: {
                    aluno_id: $scope.client_id
                },
                method: "POST"
            }).then(function (response) {
                $scope.client = response.data[0];
                console.log($scope.client);
            }, function (response) {
                console.log('Opsss... Algo deu errado na busca do cliente!');
            });

            ////programas do aluno
            $http({
                url: '../../prescricao/getProgramasAluno/' + $scope.client_id,
                method: "GET"
            }).then(function (response) {
                $scope.programasaluno = response.data.programasaluno;
            }, function (response) {
                console.log('Opsss... Algo deu errado ao buscar programas!');
            });






            // LISTA TREINOS DO ALUNO
            $scope.treinos = {};
            $http({
                url: '../../api/prescricao/getTreinos',
                data: {
                    aluno_id: $scope.client_id
                },
                method: "POST"
            }).then(function (response) {
                $scope.treinos = response.data;
                if (response.data.length !== 0) {
                    $scope.changeTreino(response.data[response.data.length - 1].treino_id);
                    $scope.treino_id = $scope.treinos[$scope.treinos.length - 1];
                } else {
                    // $scope.addTreino();
                    $scope.delTreinoBtn = true;
                    $scope.semTreino = true;
                }
            }, function (response) {
                console.log('Opsss... Algo deu errado!');
            });



            // MUDA TREINO
            $scope.changeTreino = function (treino_id) {
                $("#n_abas").show(120);
                $("#n_abas2").show(120);
                if (treino_id !== undefined) {
                    // DESATIVA OS BOTOES

                    $http({
                        url: '../../api/prescricao/getTreinoIniciado',
                        data: {
                            treino_id: treino_id,
                            aluno_id: $scope.client_id
                        },
                        method: "POST"
                    }).then(function (response) {
                        $scope.treino_iniciado = response.data.status;
                    });



                    $scope.statusBotoes(true);
                    $scope.msgTreino = 'Aguarde... carregando treino!';
                    $http({
                        url: '../../api/prescricao/getTreino',
                        data: {
                            treino_id: treino_id
                        },
                        method: "POST"
                    }).then(function (response) {
                        if (response.data.treino_qtddias == null) {
                            $scope.addTab(1);
                        } else {
                            $scope.addTab(response.data.treino_qtddias);
                        }
                        $scope.treino_qtddias = response.data.treino_qtddias;
                        $scope.treino_nivel = response.data.treino_nivel;
                        $scope.objetivo_id = response.data.objetivo_id;
                        $scope.treino_revisao = response.data.treino_revisao;
                        $scope.treino_padrao = (response.data.treino_padrao === 'S') ? true : false;
                        $scope.treino_atual = (response.data.treino_atual === '1') ? true : false;
                        $scope.treino_nome_padrao = response.data.treino_nome_padrao;
                        // BUSCA EXERCICIOS DO TREINO
                        $http({
                            url: '../../api/prescricao/getFicha',
                            data: {
                                treino_id: treino_id
                            },
                            method: "POST"
                        }).then(function (response) {
                            var tamanho = response.data.length;
                            $scope.linhasA = []; // LIMPA VETOR A
                            $scope.linhasB = []; // LIMPA VETOR B
                            $scope.linhasC = []; // LIMPA VETOR C
                            $scope.linhasD = []; // LIMPA VETOR D
                            $scope.linhasE = []; // LIMPA VETOR E
                            $scope.linhasF = []; // LIMPA VETOR F
                            $scope.linhasG = []; // LIMPA VETOR G
                            if (tamanho > 0) {
                                for (var i = 0; i < tamanho; i++) {
                                    switch (response.data[i].letra) {
                                        // CARREGA EXERCICIO NO TREINO A
                                        case 'A':
                                            $scope.insertA = {
                                                id: $scope.linhasA.length + 1,
                                                ficha: response.data[i].ficha,
                                                letra: response.data[i].letra,
                                                exercicio: response.data[i].exercicio,
                                                intervalo: response.data[i].intervalo,
                                                series: response.data[i].series,
                                                repeticoes: response.data[i].repeticoes,
                                                carga: response.data[i].carga,
                                                observacao: response.data[i].observacao
                                            };

                                            $scope.linhasA.push($scope.insertA);

                                            $timeout(function () {
                                                for (var i = 0; i < tamanho; i++) {
                                                    $('#execicioSelectA' + i).addClass('chosen-select');
                                                    $('.chosen-select').chosen({width: "100%"});
                                                }
                                            }, 500);
                                            break;
                                            // CARREGA EXERCICIO NO TREINO B
                                        case 'B':
                                            $scope.insertB = {
                                                id: $scope.linhasB.length + 1,
                                                ficha: response.data[i].ficha,
                                                letra: response.data[i].letra,
                                                exercicio: response.data[i].exercicio,
                                                intervalo: response.data[i].intervalo,
                                                series: response.data[i].series,
                                                repeticoes: response.data[i].repeticoes,
                                                carga: response.data[i].carga,
                                                observacao: response.data[i].observacao
                                            };
                                            $scope.linhasB.push($scope.insertB);
                                            $timeout(function () {
                                                for (var i = 0; i < tamanho; i++) {
                                                    $('#execicioSelectB' + i).addClass('chosen-select');
                                                    $('.chosen-select').chosen({width: "100%"});
                                                }
                                            }, 500);
                                            break;
                                            // CARREGA EXERCICIO NO TREINO C
                                        case 'C':
                                            $scope.insertC = {
                                                id: $scope.linhasC.length + 1,
                                                ficha: response.data[i].ficha,
                                                letra: response.data[i].letra,
                                                exercicio: response.data[i].exercicio,
                                                intervalo: response.data[i].intervalo,
                                                series: response.data[i].series,
                                                repeticoes: response.data[i].repeticoes,
                                                carga: response.data[i].carga,
                                                observacao: response.data[i].observacao
                                            };
                                            $scope.linhasC.push($scope.insertC);
                                            $timeout(function () {
                                                for (var i = 0; i < tamanho; i++) {
                                                    $('#execicioSelectC' + i).addClass('chosen-select');
                                                    $('.chosen-select').chosen({width: "100%"});
                                                }
                                            }, 500);
                                            break;
                                            // CARREGA EXERCICIO NO TREINO D
                                        case 'D':
                                            $scope.insertD = {
                                                id: $scope.linhasD.length + 1,
                                                ficha: response.data[i].ficha,
                                                letra: response.data[i].letra,
                                                exercicio: response.data[i].exercicio,
                                                intervalo: response.data[i].intervalo,
                                                series: response.data[i].series,
                                                repeticoes: response.data[i].repeticoes,
                                                carga: response.data[i].carga,
                                                observacao: response.data[i].observacao
                                            };
                                            $scope.linhasD.push($scope.insertD);
                                            $timeout(function () {
                                                for (var i = 0; i < tamanho; i++) {
                                                    $('#execicioSelectD' + i).addClass('chosen-select');
                                                    $('.chosen-select').chosen({width: "100%"});
                                                }
                                            }, 500);
                                            break;
                                            // CARREGA EXERCICIO NO TREINO E
                                        case 'E':
                                            $scope.insertE = {
                                                id: $scope.linhasE.length + 1,
                                                ficha: response.data[i].ficha,
                                                letra: response.data[i].letra,
                                                exercicio: response.data[i].exercicio,
                                                intervalo: response.data[i].intervalo,
                                                series: response.data[i].series,
                                                repeticoes: response.data[i].repeticoes,
                                                carga: response.data[i].carga,
                                                observacao: response.data[i].observacao
                                            };
                                            $scope.linhasE.push($scope.insertE);
                                            $timeout(function () {
                                                for (var i = 0; i < tamanho; i++) {
                                                    $('#execicioSelectE' + i).addClass('chosen-select');
                                                    $('.chosen-select').chosen({width: "100%"});
                                                }
                                            }, 500);
                                            break;
                                            // CARREGA EXERCICIO NO TREINO F
                                        case 'F':
                                            $scope.insertF = {
                                                id: $scope.linhasF.length + 1,
                                                ficha: response.data[i].ficha,
                                                letra: response.data[i].letra,
                                                exercicio: response.data[i].exercicio,
                                                intervalo: response.data[i].intervalo,
                                                series: response.data[i].series,
                                                repeticoes: response.data[i].repeticoes,
                                                carga: response.data[i].carga,
                                                observacao: response.data[i].observacao
                                            };
                                            $scope.linhasF.push($scope.insertF);
                                            $timeout(function () {
                                                for (var i = 0; i < tamanho; i++) {
                                                    $('#execicioSelectF' + i).addClass('chosen-select');
                                                    $('.chosen-select').chosen({width: "100%"});
                                                }
                                            }, 500);
                                            break;
                                            // CARREGA EXERCICIO NO TREINO G
                                        case 'G':
                                            $scope.insertG = {
                                                id: $scope.linhasG.length + 1,
                                                ficha: response.data[i].ficha,
                                                letra: response.data[i].letra,
                                                exercicio: response.data[i].exercicio,
                                                intervalo: response.data[i].intervalo,
                                                series: response.data[i].series,
                                                repeticoes: response.data[i].repeticoes,
                                                carga: response.data[i].carga,
                                                observacao: response.data[i].observacao
                                            };
                                            $scope.linhasG.push($scope.insertG);
                                            $timeout(function () {
                                                for (var i = 0; i < tamanho; i++) {
                                                    $('#execicioSelectG' + i).addClass('chosen-select');
                                                    $('.chosen-select').chosen({width: "100%"});
                                                }
                                            }, 500);
                                            break;
                                    }

                                }
                            }
                            $scope.msgTreino = '';
                            // ATIVA OS BOTÕES
                            $scope.statusBotoes(false);
                        }, function (response) {
                            console.log('Opsss... Algo deu errado!');
                        });
                        // FECHA BUSCA EXERCICIOS DO TREINO
                    }, function (response) {
                        $scope.linhasA = []; // LIMPA VETOR A
                        $scope.linhasB = []; // LIMPA VETOR B
                        $scope.linhasC = []; // LIMPA VETOR C
                        $scope.linhasD = []; // LIMPA VETOR D
                        $scope.linhasE = []; // LIMPA VETOR E
                        $scope.linhasF = []; // LIMPA VETOR F
                        $scope.linhasG = []; // LIMPA VETOR G
                        $scope.msgTreino = '';
                        console.log('Opsss... Algo deu errado!');
                    });
                }
            };

            // MUDA TREINO
            // LISTA TREINOS DO ALUNO
            $scope.listaTreinosAluno = function () {
                $scope.treinos = {};
                $http({
                    url: '../../api/prescricao/getTreinos',
                    data: {
                        aluno_id: $scope.client_id
                    },
                    method: "POST"
                }).then(function (response) {
                    $scope.treinos = response.data;
                    if (response.data.length !== 0) {
                        $scope.changeTreino(response.data[response.data.length - 1].treino_id);
                        $scope.treino_id = $scope.treinos[$scope.treinos.length - 1];
                    } else {
                        // $scope.addTreino();
                    }
                }, function (response) {
                    console.log('Opsss... Algo deu errado!');
                });
            };





            // NUTRIÇÃO
            // LISTA NUTRIÇÕES DO ALUNO
            $scope.nutricoes = {};
            $http({
                url: '../../api/prescricao/getNutricoes',
                data: {
                    aluno_id: $scope.client_id
                },
                method: "POST"
            }).then(function (response) {
                $scope.nutricoes = response.data;
                if (response.data.length !== 0) {
                    $scope.changeNutricao(response.data[response.data.length - 1].id);
                    $scope.nutricao_id = $scope.nutricoes[$scope.nutricoes.length - 1];
                } else {
                    // $scope.addNutricao();
                }
            }, function (response) {
                console.log('Opsss... Algo deu errado!');
            });

            // LISTA TREINOS DO ALUNO
            $scope.listaNutricoesAluno = function () {
                $http({
                    url: '../../api/prescricao/getNutricoes',
                    data: {
                        aluno_id: $scope.client_id
                    },
                    method: "POST"
                }).then(function (response) {
                    $scope.nutricoes = response.data;
                    if (response.data.length !== 0) {
                        $scope.changeNutricao(response.data[response.data.length - 1].id);
                        $scope.nutricao_id = $scope.nutricoes[$scope.nutricoes.length - 1];
                    } else {
                        // $scope.addNutricao();
                    }
                }, function (response) {
                    console.log('Opsss... Algo deu errado!');
                });
            };

            // ADICIONA NOVA NUTRICAO
            $scope.addNutricao = function () {
                // DESATIVA OS BOTOES
                $scope.statusBotoesNutri(true);
                $scope.msgNutricao = 'Aguarde... criando prescrição!';
                $http({
                    url: '../../api/prescricao/addNutricao',
                    data: {
                        aluno_id: $scope.client_id
                    },
                    method: "POST"
                }).then(function (response) {
                    $scope.newNutricao = response.data;

                    $scope.id = response.data.id;
                    $scope.nutricao_descricao = '';
                    $scope.nutricao_datainicio = '';
                    $scope.nutricao_datatermino = '';
                    $scope.nutricao_nivel = '';
                    $scope.nutricao_padrao = false;

                    $scope.nutricoes.push($scope.newNutricao);
                    $scope.nutricao_id = $scope.nutricoes[$scope.nutricoes.length - 1];
                    $scope.msgNutricao = '';
                    // ATIVA OS BOTOES
                    $scope.statusBotoesNutri(false);
                }, function (response) {
                    console.log('Opsss... Algo deu errado!');
                });
            };

            // MUDA NUTRIÇÃO
            $scope.changeNutricao = function (nutricao_id) {
                if (nutricao_id !== undefined) {
                    // DESATIVA OS BOTOES
                    $scope.statusBotoesNutri(true);
                    $scope.msgNutricao = 'Aguarde... carregando prescrição!';
                    $http({
                        url: '../../api/prescricao/getNutricao',
                        data: {
                            nutricao_id: nutricao_id
                        },
                        method: "POST"
                    }).then(function (response) {
                        $scope.id = response.data.id;
                        $scope.nutricao_descricao = response.data.nutricao_descricao;
                        $scope.nutricao_datainicio = response.data.nutricao_datainicio;
                        $scope.nutricao_datatermino = response.data.nutricao_datatermino;
                        $scope.nutricao_nivel = response.data.nutricao_nivel;
                        $scope.nutricao_padrao = (response.data.nutricao_padrao === 'S') ? true : false;

                        $scope.msgNutricao = '';
                        // ATIVA OS BOTÕES
                        $scope.statusBotoesNutri(false);
                    }, function (response) {
                        $scope.msgNutricao = '';
                        console.log('Opsss... Algo deu errado!');
                    });
                }
            };

            // DELETA NUTRIÇÃO
            $scope.deleteNutricao = function (nutricao_id) {
                if (nutricao_id !== undefined) {
                   
                   swal({
   title: "Tem certeza?",
                        text: "Tem certeza que deseja deletar essa prescrição?",
  icon: "warning",
  buttons: true,
  buttons: {
      cancel: {
        text: "Cancelar",
        value: null,
        visible: true,
        className: "",
        closeModal: true,
      },
      confirm: {
        text: "Sim, tenho certeza!",
        value: true,
        visible: true,
        className: "",
        closeModal: true
      }},
  dangerMode: true,

}).then((willDelete) => {
  if (willDelete) {
    // aqui chama o ajax
    $http({
                            url: '../../api/prescricao/delNutricao/' + nutricao_id,
                            method: "POST"
                        }).then(function (response) {
                            $scope.listaNutricoesAluno();
                            swal("Deletado!", "Prescrição deletada com sucesso!", "success");
                        });
  } else {
    // swal("Your imaginary file is safe!");
  }
});
// fim swal

                 
                } else {
                    swal("Opsss!", "Selecione uma prescrição para deletar!", "error");
                }
            };

            // ATUALIZA TREINO
            $scope.salvarNutricao = function () {
                if ($scope.id !== undefined) {
                    // DESATIVA OS BOTOES
                    $scope.statusBotoesNutri(true);
                    $http({
                        url: '../../api/prescricao/upNutricao',
                        data: {
                            nutricao_id: $scope.id,
                            nutricao_descricao: $scope.nutricao_descricao,
                            nutricao_datainicio: $scope.nutricao_datainicio,
                            nutricao_datatermino: $scope.nutricao_datatermino,
                            nutricao_nivel: $scope.nutricao_nivel,
                            nutricao_padrao: $scope.nutricao_padrao,
                        },
                        method: "POST"
                    }).then(function (response) {
                        swal('Sucesso!', 'Prescrição salva com sucesso!', 'success');
                        // ATIVA OS BOTOES
                        $scope.statusBotoesNutri(false);
                        $scope.changeNutricao($scope.id);
                    }, function (response) {
                        console.log('Opsss... Algo deu errado!');
                    });
                } else {
                    swal("Opsss!", "Selecione ou crie uma prescrição antes de salvar!", "error");
                }
            };

            $scope.statusBotoesNutri = function (sts) {
                $scope.addNutricaoBtn = sts;
                $scope.salvarNutricaoBtn = sts;
                $scope.delNutricaoBtn = sts;
            };
            // FECHA NUTRIÇÃO

            // PROJETOS / ETAPAS
            // LISTA PROJETOS DO ALUNO
            $scope.projetos = [];
            $http({
                url: '../../api/prescricao/getProjetos',
                data: {
                    aluno_id: $scope.client_id
                },
                method: "POST"
            }).then(function (response) {
                $scope.projetos = response.data;
            }, function (response) {
                console.log('Opsss... Algo deu errado!');
            });

            $scope.listaProjetosAluno = function () {
                $http({
                    url: '../../api/prescricao/getProjetos',
                    data: {
                        aluno_id: $scope.client_id
                    },
                    method: "POST"
                }).then(function (response) {
                    $scope.projetos = response.data;
                }, function (response) {
                    console.log('Opsss... Algo deu errado!');
                });
            };

            $scope.addProjeto = function () {
                if ($scope.projetos.length) {



                    swal({
  title: "Tem certeza?",
                        text: "Deseja realmente iniciar um novo projeto? o projeto anterior será finalizado!",
  icon: "warning",
  buttons: true,
  buttons: {
      cancel: {
        text: "Cancelar",
        value: null,
        visible: true,
        className: "",
        closeModal: true,
      },
      confirm: {
        text: "Sim, tenho certeza!",
        value: true,
        visible: true,
        className: "",
        closeModal: true
      }},
  dangerMode: true,

}).then((willDelete) => {
  if (willDelete) {
    // aqui chama o ajax
    $scope.statusBotoesProjeto(true);
                        $http({
                            url: '../../api/prescricao/addProjeto',
                            data: {
                                aluno_id: $scope.client_id
                            },
                            method: "POST"
                        }).then(function (response) {
                            $scope.projeto = response.data;
                            $scope.listaProjetosAluno();
                            $scope.listaEtapas();
                            $scope.editingData[$scope.projeto.id] = true;
                            $timeout(function () {
                                $scope.ativaCalendarioProjeto();
                            }, 1000);
                            // ATIVA OS BOTOES
                            $scope.statusBotoesProjeto(false);
                        }, function (response) {
                            console.log('Opsss... Algo deu errado!');
                        });
  } else {
    // swal("Your imaginary file is safe!");
  }
});
// fim swal
                 
                } else {
                    $scope.statusBotoesProjeto(true);
                    $http({
                        url: '../../api/prescricao/addProjeto',
                        data: {
                            aluno_id: $scope.client_id
                        },
                        method: "POST"
                    }).then(function (response) {
                        $scope.projeto = response.data;
                        $scope.listaProjetosAluno();
                        $scope.listaEtapas();
                        $scope.editingData[$scope.projeto.id] = true;
                        // ATIVA OS BOTOES
                        $scope.statusBotoesProjeto(false);
                    }, function (response) {
                        console.log('Opsss... Algo deu errado!');
                    });
                }
            };

            $scope.editingData = {};

            $scope.alterProjeto = function (projeto) {
                $scope.editingData[projeto.id] = true;

                $scope.ativaCalendarioProjeto();
            };

            $scope.updateProjeto = function (projeto) {
                $scope.statusBotoesProjeto(true);
                $http({
                    url: '../../api/prescricao/upProjeto',
                    data: {
                        id: projeto.id,
                        projeto_nome: projeto.projeto_nome,
                        projeto_data: projeto.projeto_data
                    },
                    method: "POST"
                }).then(function (response) {
                    $scope.editingData[projeto.id] = false;
                    $scope.listaEtapas();
                    // ATIVA OS BOTOES
                    $scope.statusBotoesProjeto(false);
                }, function (response) {
                    console.log('Opsss... Algo deu errado!');
                });
            };

            $scope.delProjeto = function (projeto_id) {
                if (projeto_id !== undefined) {
                 swal({
  title: "Tem certeza?",
                        text: "Tem certeza que deseja deletar esse projeto?",
  icon: "warning",
  buttons: true,
  buttons: {
      cancel: {
        text: "Cancelar",
        value: null,
        visible: true,
        className: "",
        closeModal: true,
      },
      confirm: {
        text: "Sim, tenho certeza!",
        value: true,
        visible: true,
        className: "",
        closeModal: true
      }},
  dangerMode: true,

}).then((willDelete) => {
  if (willDelete) {
    // aqui chama o ajax
    $http({
                            url: '../../api/prescricao/delProjeto/' + projeto_id,
                            method: "POST"
                        }).then(function (response) {
                            $scope.listaProjetosAluno();
                            swal("Deletado!", "Projeto deletado com sucesso!", "success");
                        });
  } else {
    // swal("Your imaginary file is safe!");
  }
});
// fim swal

                } else {
                    swal("Opsss!", "Selecione um projeto para deletar!", "error");
                }
            };

            $scope.etapas = [];
            $http({
                url: '../../api/prescricao/getEtapas',
                method: "POST"
            }).then(function (response) {
                $scope.etapas = response.data;
            }, function (response) {
                console.log('Opsss... Algo deu errado!');
            });

            $scope.listaEtapas = function () {
                $http({
                    url: '../../api/prescricao/getEtapas',
                    method: "POST"
                }).then(function (response) {
                    $scope.etapas = response.data;
                }, function (response) {
                    console.log('Opsss... Algo deu errado!');
                });
            };

            $scope.addEtapa = function () {
                if ($scope.etapas.length > 0) {

                    swal({
title: "Tem certeza?",
                        text: "Deseja realmente iniciar uma nova etapa? a etapa anterior será finalizada!",
  icon: "warning",
  buttons: true,
  buttons: {
      cancel: {
        text: "Cancelar",
        value: null,
        visible: true,
        className: "",
        closeModal: true,
      },
      confirm: {
        text: "Sim, tenho certeza!",
        value: true,
        visible: true,
        className: "",
        closeModal: true
      }},
  dangerMode: true,

}).then((willDelete) => {
  if (willDelete) {
    // aqui chama o ajax
   $scope.statusBotoesProjeto(true);
                        $http({
                            url: '../../api/prescricao/addEtapa',
                            method: "POST"
                        }).then(function (response) {
                            $scope.etapa = response.data;
                            $scope.listaEtapas();
                            $scope.editingDataEtapa[$scope.etapa.id] = true;
                            $timeout(function () {
                                $scope.ativaCalendarioEtapa(response.data.projeto_data);
                            }, 1000);
                            // ATIVA OS BOTOES
                            $scope.statusBotoesProjeto(false);
                        }, function (response) {
                            console.log('Opsss... Algo deu errado!');
                        });
  } else {
    // swal("Your imaginary file is safe!");
  }
});
// fim swal
// 
                  
                } else {
                    $scope.statusBotoesProjeto(true);
                    $http({
                        url: '../../api/prescricao/addEtapa',
                        method: "POST"
                    }).then(function (response) {
                        $scope.etapa = response.data;
                        $scope.listaEtapas();
                        $scope.editingDataEtapa[$scope.etapa.id] = true;
                        // ATIVA OS BOTOES
                        $scope.statusBotoesProjeto(false);
                    }, function (response) {
                        console.log('Opsss... Algo deu errado!');
                    });
                }
            };

            $scope.editingDataEtapa = {};

            $scope.alterEtapa = function (etapa, projeto_data) {
                $scope.editingDataEtapa[etapa.id] = true;

                $scope.ativaCalendarioEtapa(projeto_data);
            };

            $scope.updateEtapa = function (etapa) {
                $scope.statusBotoesProjeto(true);
                $http({
                    url: '../../api/prescricao/upEtapa',
                    data: {
                        id: etapa.id,
                        etapa_nome: etapa.etapa_nome,
                        etapa_data: etapa.etapa_data,
                        etapa_categoria: etapa.etapa_categoria
                    },
                    method: "POST"
                }).then(function (response) {
                    if (response.data['type'] === 'error') {
                        swal("Erro!", response.data['text'], "error");
                    } else {
                        $scope.editingDataEtapa[etapa.id] = false;
                    }
                    // ATIVA OS BOTOES
                    $scope.statusBotoesProjeto(false);
                }, function (response) {
                    console.log('Opsss... Algo deu errado!');
                });
            };

            $scope.delEtapa = function (etapa_id) {
                if (etapa_id !== undefined) {

                    swal({
   title: "Tem certeza?",
                        text: "Tem certeza que deseja deletar essa etapa?",
  icon: "warning",
  buttons: true,
  buttons: {
      cancel: {
        text: "Cancelar",
        value: null,
        visible: true,
        className: "",
        closeModal: true,
      },
      confirm: {
        text: "Sim, tenho certeza!",
        value: true,
        visible: true,
        className: "",
        closeModal: true
      }},
  dangerMode: true,

}).then((willDelete) => {
  if (willDelete) {
    // aqui chama o ajax
    $http({
                            url: '../../api/prescricao/delEtapa/' + etapa_id,
                            method: "POST"
                        }).then(function (response) {
                            $scope.listaEtapas();
                            swal("Deletado!", "Etapa deletada com sucesso!", "success");
                        });
  } else {
    // swal("Your imaginary file is safe!");
  }
});
// fim swal
// 
                } else {
                    swal("Opsss!", "Selecione uma etapa para deletar!", "error");
                }
            };

            $scope.statusBotoesProjeto = function (sts) {
                $scope.addProjetoBtn = sts;
            };

            $scope.ativaCalendarioEtapa = function (projeto_data) {
                $('#data_3 .input-group.date').datepicker({
                    todayBtn: "linked",
                    keyboardNavigation: false,
                    forceParse: false,
                    calendarWeeks: true,
                    startDate: 'today',
                    endDate: projeto_data,
                    autoclose: true
                });
            };

            $scope.ativaCalendarioProjeto = function () {
                $('#data_4 .input-group.date').datepicker({
                    todayBtn: "linked",
                    keyboardNavigation: false,
                    forceParse: false,
                    calendarWeeks: true,
                    startDate: 'today',
                    autoclose: true
                });
            };
            // FECHA PROJETOS / ETAPAS

            // PSA
            // LISTA PROGRAMAS
            $scope.programas = {};
            $http({
                url: '../../api/prescricao/getProgramas',
                method: "POST"
            }).then(function (response) {
                $scope.programas = response.data;
            }, function (response) {
                console.log('Opsss... Algo deu errado!');
            });

            // LISTA PSAS DO ALUNO
            $scope.psas = [];
            $http({
                url: '../../api/prescricao/getPsas',
                data: {
                    aluno_id: $scope.client_id
                },
                method: "POST"
            }).then(function (response) {
                $scope.psas = response.data;
                $timeout(function () {
                    $('.clockpicker').clockpicker();
                }, 500);
            }, function (response) {
                console.log('Opsss... Algo deu errado!');
            });

            $scope.listaPsasAluno = function () {
                $http({
                    url: '../../api/prescricao/getPsas',
                    data: {
                        aluno_id: $scope.client_id
                    },
                    method: "POST"
                }).then(function (response) {
                    $scope.psas = response.data;
                    $timeout(function () {
                        $('.clockpicker').clockpicker();
                    }, 500);
                }, function (response) {
                    console.log('Opsss... Algo deu errado!');
                });
            };

            $scope.addPsa = function () {
                $scope.statusBotoesPsa(true);
                $http({
                    url: '../../api/prescricao/addPsa/' + $scope.client_id,
                    method: "POST"
                }).then(function (response) {
                    $scope.newPsa = response.data;
                    $scope.listaPsasAluno();
                    $scope.statusBotoesPsa(false);
                }, function (response) {
                    console.log('Opsss... Algo deu errado!');
                });
            };

            $scope.salvarPsa = function (psa) {
                $scope.statusBotoesPsa(true);
                $http({
                    url: '../../api/prescricao/salvaPsa',
                    data: psa,
                    method: "POST"
                }).then(function (response) {
                    swal('Sucesso!', 'PSA salva com sucesso!', 'success');
                    $scope.listaPsasAluno();
                    $scope.statusBotoesPsa(false);
                }, function (response) {
                    console.log('Opsss... Algo deu errado!');
                });
            };

            $scope.delPsa = function (id) {
                $scope.statusBotoesPsa(true);
                $http({
                    url: '../../api/prescricao/delPsa/' + id,
                    method: "POST"
                }).then(function (response) {
                    $scope.listaPsasAluno();
                    $scope.statusBotoesPsa(false);
                });
            };

            $scope.statusBotoesPsa = function (sts) {
                $scope.addPsaBtn = sts;
                $scope.salvarPsaBtn = sts;
                $scope.delPsaBtn = sts;
            };
            // FECHA PSA


            // INICIO TREINO PADRAO
            //$scope.objetivos = {};
            $http({
                url: '../../api/prescricao/getObjetivos/1',
                method: "POST"
            }).then(function (response) {
                $scope.objetivos = response.data;
            }, function (response) {
                console.log('Opsss... Algo deu errado na busca dos objetivos!');
            });
            $http({
                url: '../../api/prescricao/getObjetivos/4',
                method: "POST"
            }).then(function (response) {
                $scope.objetivosCross = response.data;
            }, function (response) {
                console.log('Opsss... Algo deu errado na busca dos objetivos!');
            });

            $http({
                url: '../../api/prescricao/getNiveis/1/1',
                method: "POST"
            }).then(function (response) {
                $scope.niveis = response.data;
                $timeout(function () {
                    // $scope.ativaRamificacao();
                }, 2000);
            }, function (response) {
                console.log('Opsss... Algo deu errado na busca dos niveis!');
            });


            $scope.fullTreinosM = [];
            $http({
                url: '../../api/prescricao/getFullTreinosM',
                method: "POST"
            }).then(function (response) {
                $scope.fullTreinosM = response.data;
            }, function (response) {
                console.log('Opsss... Algo deu errado!');
            });

            $scope.listafullTreinosM = function () {
                $http({
                    url: '../../api/prescricao/getFullTreinosM',
                    method: "POST"
                }).then(function (response) {
                    $scope.fullTreinosM = response.data;
                }, function (response) {
                    console.log('Opsss... Algo deu errado!');
                });
            };

            $scope.fullTreinosF = [];
            $http({
                url: '../../api/prescricao/getFullTreinosF',
                method: "POST"
            }).then(function (response) {
                $scope.fullTreinosF = response.data;
            }, function (response) {
                console.log('Opsss... Algo deu errado!');
            });

            $scope.listafullTreinosF = function () {
                $http({
                    url: '../../api/prescricao/getFullTreinosF',
                    method: "POST"
                }).then(function (response) {
                    $scope.fullTreinosF = response.data;
                }, function (response) {
                    console.log('Opsss... Algo deu errado!');
                });
            };

            $scope.isNivel = function (nivel) {
                return function (treino) {
                    return treino.treino_nivel == nivel;
                }
            };

            $scope.isObjetivo = function (objetivo) {
                return function (treino) {
                    return treino.treino_objetivo == objetivo;
                }
            };


            $scope.postIdTreino = function (id) {
                $http({
                    url: '../../api/prescricao/getTreinoPadrao',
                    data: {
                        id: id
                    },
                    method: "POST"
                }).then(function (response) {
                    $scope.treinopadrao = response.data;
                }, function (response) {
                    console.log('Opsss... Algo deu errado na busca do treino padrão!');
                });
            };



            $scope.changeNomeTreinoPadrao = function (treinopadrao) {
                $http({
                    url: '../../api/prescricao/changeNomeTreinoPadrao',
                    data: {
                        treino_id: treinopadrao.treino_id,
                        treino_nome_padrao: treinopadrao.treino_nome_padrao
                    },
                    method: "POST"
                }).then(function (response) {
                    $scope.listafullTreinosM();
                    $scope.listafullTreinosF();
                    swal("Sucesso!", "Nome alterado com sucesso!", "success");
                    $("#modalChangeNomeTreinoPadrao").modal('hide');
                }, function (response) {
                    console.log('Opsss... Algo deu errado na busca do treino padrão!');
                });
            };
            // FIM TREINO PADRAO

            $scope.ativaRamificacao = function () {
                $('#jstree1').jstree({
                    'core': {
                        'check_callback': true
                    },
                    'plugins': ['types', 'dnd'],
                    'types': {
                        'default': {
                            'icon': 'fa fa-folder'
                        },
                        'html': {
                            'icon': 'fa fa-file-code-o'
                        },
                        'svg': {
                            'icon': 'fa fa-file-picture-o'
                        },
                        'css': {
                            'icon': 'fa fa-file-code-o'
                        },
                        'img': {
                            'icon': 'fa fa-file-image-o'
                        },
                        'js': {
                            'icon': 'fa fa-file-text-o'
                        }
                    }
                });
                $('#jstree2').jstree({
                    'core': {
                        'check_callback': true
                    },
                    'plugins': ['types', 'dnd'],
                    'types': {
                        'default': {
                            'icon': 'fa fa-folder'
                        },
                        'html': {
                            'icon': 'fa fa-file-code-o'
                        },
                        'svg': {
                            'icon': 'fa fa-file-picture-o'
                        },
                        'css': {
                            'icon': 'fa fa-file-code-o'
                        },
                        'img': {
                            'icon': 'fa fa-file-image-o'
                        },
                        'js': {
                            'icon': 'fa fa-file-text-o'
                        }
                    }
                });
            };
        }
    });



});

app.filter('myDateFormat', function myDateFormat($filter) {
    return function (text) {
        var tempdate = new Date(text.replace(/-/g, "/"));
        return $filter('date')(tempdate, "dd/MM/yyyy");
    }
});