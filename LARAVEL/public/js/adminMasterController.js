
app.controller('adminMasterController', function ($http, $scope, $timeout) {

    $http.defaults.headers.common.user = $('meta[name="user_logued"]').attr('content');
    $http.defaults.headers.common.unity = $('meta[name="unidade"]').attr('content');

    $scope.loading = false;

    $scope.empresa = [];
    $scope.empresas = [];
    $scope.matriz=0;

    // LISTA ESTADOS
    $scope.estados = {};
    $http({
        url: '/admin/api/unidade/getEstados',
        method: "POST"
    }).then(function (response) {
        $scope.estados = response.data;
        $timeout(function () {
            $('#estado').addClass('chosen-select');
            $('.chosen-select').chosen({width: "100%"});
        }, 500);
    }, function (response) {
        console.log('Opsss... Algo deu errado!');
    });


    $http({
        url: '/adminmaster/getEmpresas',
        method: "POST"
    }).then(function (response) {
        $scope.empresas = response.data;
        console.log('getEmpresas', $scope.empresas);

    }, function (response) {
        console.log('Opsss... Algo deu errado!');
    });

    $scope.novaMatriz = function (estado) {
        
         $scope.empresa = {};
         $scope.empresa.parent_id = 0;
         
    };
    
    $scope.novaFilial = function ($id) {
        
         $scope.empresa.parent_id = $id;
         $scope.matriz = $id;
    };    
    $scope.mudaStatus = function ($id,$tipo) {
        var msg = "";
        var confirma = "";
        if ($tipo == '0') {
            msg = "Deseja realmente desativar essa Empresa?";
            confirma = "Empresa desativada com sucesso";
        } else {
            if ($tipo == '1') {
                msg = "Deseja realmente ativar essa Empresa?";
                confirma = "Empresa ativada com sucesso";
            }
        }

        swal({
     title: "Tem certeza?",
            text: msg,
  icon: "warning",
  buttons: true,
  buttons: {
      cancel: {
        text: "Cancelar",
        value: null,
        visible: true,
        className: "",
        closeModal: true,
      },
      confirm: {
        text: "Sim Desejo!",
        value: true,
        visible: true,
        className: "",
        closeModal: true
      }},
  dangerMode: true,

}).then((willDelete) => {
  if (willDelete) {
    // aqui chama o ajax
        muda($id,$tipo);
            swal("Atualizado!", confirma, "success");
  } else {
    // swal("Your imaginary file is safe!");
  }
});

       

    };    
    
    $scope.mudaLicenca = function ($id,$tipo) {
        var msg = "";
        var confirma = "";
        if ($tipo == 'full') {
            msg = "Deseja realmente mudar a licença dessa Empresa para Full?";
            confirma = "Licença alterada com sucesso";
        } else {
            if ($tipo == 'trial') {
                msg = "Deseja realmente mudar a licença dessa Empresa para Trial?";
                confirma = "Licença alterada com sucesso";
            }
        }

        swal({
     title: "Tem certeza?",
            text: msg,
  icon: "warning",
  buttons: true,
  buttons: {
      cancel: {
        text: "Cancelar",
        value: null,
        visible: true,
        className: "",
        closeModal: true,
      },
      confirm: {
        text: "Sim Desejo!",
        value: true,
        visible: true,
        className: "",
        closeModal: true
      }},
  dangerMode: true,

}).then((willDelete) => {
  if (willDelete) {
    // aqui chama o ajax
      mudaLic($id,$tipo);
            swal("Atualizado!", confirma, "success");
   
  } else {
    // swal("Your imaginary file is safe!");
  }
});
// fim swal
    

    };        
    
    function muda(id, tipo) {

        $http({
            url: '/adminmaster/mudaStatus',
            data: {
                id: id,
                situacao: tipo
            },
            method: "POST"
        }).then(function (response) {
           
            $scope.getEmpresas();

        });

    }    
    
    function mudaLic(id, tipo) {

        $http({
            url: '/adminmaster/mudaLicenca',
            data: {
                id: id,
                licenca: tipo
            },
            method: "POST"
        }).then(function (response) {
           
            $scope.getEmpresas();

        });

    }  
    $scope.changeEstado = function (estado) {
        
        // LISTA CIDADE
        
        $scope.cidades = {};
        $http({
            url: '/admin/api/unidade/getCidades',
            data: {
                estado: estado
            },
            method: "POST"
        }).then(function (response) {
            $scope.cidades = response.data;
            $timeout(function () {
                $("#cidade").val('').trigger("chosen:updated");
                $('#cidade').addClass('chosen-select');
                $('.chosen-select').chosen({width: "100%"});
            }, 500);
        }, function (response) {
            console.log('Opsss... Algo deu errado!');
        });
    };

    
    $scope.getEmpresas = function () {
        
        
        $http({
            url: '/adminmaster/getEmpresas',
            method: "POST"
        }).then(function (response) {
            $scope.empresas = response.data;
            
        }, function (response) {
            console.log('Opsss... Algo deu errado!');
        });        
        
    };        

    $scope.getMatriz = function ($id) {
        
        
        $http({
            url: '/adminmaster/getMatriz/' + $id,
            method: "GET"
        }).then(function (response) {
           
           $scope.empresa = response.data[0];
           
           $scope.changeEstado(response.data[0].idestado);

        }, function (response) {
            console.log('Opsss... Algo deu errado!');
        });
        
    };
    
    $scope.salvar_dados = function () {
      
        
        $http({
            url: '/adminmaster/addAdminMatriz',
            data: $scope.empresa,
            method: "POST"
        }).then(function (response) {

            
        }, function (response) {
            console.log('Opsss... Algo deu errado!');
        });
    };
    

    $scope.salvar = function () {
        

        $http({
            url: '/adminmaster/addAdminMatriz',
            data: $scope.empresa,
            method: "POST"
        }).then(function (response) {
                if (response.data.type=='success'){
                    toastr.success(response.data.text);
                }else{
                    toastr.success(response.data.text);
                }        





        }, function (response) {
            console.log('Opsss... Algo deu errado!');
        });
    };
    $scope.atualizar = function () {
        $http({
            url: '/adminmaster/upAdminMatriz',
            data: $scope.empresa,
            method: "POST"
        }).then(function (response) {
                if (response.data.type=='success'){
                    toastr.success(response.data.text);
                }else{
                    toastr.success(response.data.text);
                }        


        }, function (response) {
            console.log('Opsss... Algo deu errado!');
        });
    };


}); // fim controler alunoController
