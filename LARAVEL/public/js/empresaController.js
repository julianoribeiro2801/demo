//var url_post = "http://personalclubbrasil.com.br/laravel/public/api/";

app.factory('LocalService', function ($http, $q, $timeout, $window) {

    var config = {
        // api : 'http://personalclubbrasil.com.br/laravel/public/api/local'
        api : '/admin/local'
    };
    $http.defaults.headers.common.user = $('meta[name="user_id"]').attr('content') || 1;
    $http.defaults.headers.common.unidade = 1;
    $http.defaults.headers.common._token = $('meta[name="csrf_token"]').attr('content');

    var services = {
        register : register,
        show: show,
        destroy: destroy,
        update: update,
        all : all
    };
    function all() {
        return $http.get(config.api);
    }
    function register($data){
        return $http.post(config.api, $data);
    }
    function show($id) {
        return $http.get(config.api + '/local/'+$id);
    }
    function destroy($id) {
       return $http.delete(config.api+ '/' + $id);
    }
    function update($id, $data){
        
        return $http.put(config.api + '/' + $id, $data);
    }
    return services;
});

app.factory('ModalidadeService', function ($http, $q, $timeout, $window) {

    var config = {
        // api : 'http://personalclubbrasil.com.br/laravel/public/api/local'
        api : '/admin/modalidade'
    };
    $http.defaults.headers.common.user = $('meta[name="user_id"]').attr('content') || 1;
    $http.defaults.headers.common.unidade = 1;
    $http.defaults.headers.common._token = $('meta[name="csrf_token"]').attr('content');

    var services = {
        register : register,
        show: show,
        destroy: destroy,
        update: update,
        mudastatus: mudastatus,
        all : all
    };
    function all() {
        return $http.get(config.api);
    }
    function register($data){
        return $http.post(config.api, $data);
    }
    function show($id) {
        return $http.get(config.api + '/local/'+$id);
    }
    function destroy($id) {
       return $http.delete(config.api+ '/' + $id);
    }
    function update($id, $data){
        
        return $http.put(config.api + '/' + $id, $data);
    }
    function mudastatus($id, $data){
        
        return $http.put(config.api + '/mudastatus' + $id, $data);
    }
    return services;
});

app.factory('FuncionarioService', function ($http, $q, $timeout, $window) {

    var config = {
        // api : 'http://personalclubbrasil.com.br/laravel/public/api/local'
        api : '/admin/funcionario'
    };
    $http.defaults.headers.common.user = $('meta[name="user_id"]').attr('content') || 1;
    $http.defaults.headers.common.unidade = 1;
    $http.defaults.headers.common._token = $('meta[name="csrf_token"]').attr('content');

    var services = {
        register : register,
        show: show,
        destroy: destroy,
        update: update,
        all : all
    };
    function all() {
        return $http.get(config.api);
    }
    function register($data){
        return $http.post(config.api, $data);
    }
    function show($id) {
        return $http.get(config.api + '/funcionario/'+$id);
    }
    function destroy($id) {
       return $http.delete(config.api+ '/' + $id);
    }
    function update($id, $data){
        
        return $http.put(config.api + '/' + $id, $data);
    }
    return services;
});


app.factory('UnidadeService', function ($http, $q, $timeout, $window) {

    var config = {
        // api : 'http://personalclubbrasil.com.br/laravel/public/api/local'
        api : '/admin/empresa'
    };
    $http.defaults.headers.common.user = $('meta[name="user_id"]').attr('content') || 1;
    $http.defaults.headers.common.unidade = 1;
    $http.defaults.headers.common._token = $('meta[name="csrf_token"]').attr('content');

    var services = {
        register : register,
        show: show,
        destroy: destroy,
        update: update,
        all : all,
        carregaCidade:carregaCidade,
        matriz:matriz
    };
    function carregaCidade($id) {
        return $http.get(config.api + '/cidade/'+$id);
    }
    function all() {
        return $http.get(config.api + '/filiais/');
    }
    function matriz() {
        return $http.get(config.api + '/matriz/');
    }
    function register($data){
        return $http.post(config.api, $data);
    }
    function show($id) {
        return $http.get(config.api + '/unidade/'+$id);
    }
    function destroy($id) {
       return $http.delete(config.api+ '/' + $id);
    }
    function update($id, $data){
        
        return $http.put(config.api + '/' + $id, $data);
    }
    
    return services;
});

app.controller('empresaController', function($http, $scope, LocalService,ModalidadeService,FuncionarioService,UnidadeService, $timeout) {
    $scope.loading = false;
    $scope.unidade = {};
   
    
    $scope.idunidade = [];
    $scope.user_id = 1;
    $scope.local = {};
    $scope.local.dados = {};
    $scope.locais = [];

    $scope.modalidade = {};
    $scope.modalidade.dados = {};
    $scope.modalidades = [];
    
    $scope.funcionario = {};
    $scope.funcionario.dados = {};
    $scope.funcionarios = [];
    
    $scope.pessoa = {};
    $scope.pessoa.dados = {};
    $scope.pessoas = [];
    
    $scope.unidade = {};
    $scope.unidade.dados = {};
    $scope.unidades = [];
    
    $scope.empresa = {};
    $scope.empresa.dados = {};
    $scope.empresas = [];
        
    $scope.empresadado = {};
    //$scope.dados.dados = {};
    $scope.empresadados = [];
        
    
    $scope.editing = false;
    $scope.showData = false;
    $scope.errors = {};

    $scope.cidades = [];
    $scope.estados = [];
    
    
    /*$http({
        url: '../../admin/empresa/getUnidade',
        method: "GET"
    }).then(function(response) {
        
        $scope.idunidade = response.data.idunidade;
       
    }, function(response) {
        console.log('Opsss... Algo deu errado na busca dos clientes!');
    }); */       


    
    getMatriz();
    getLocals();
    getModalidades();
    getFuncionarios();
    
    getUnidades();

    
    $scope.edit = function ($local) {

        $scope.editing = $local.id;
        // abre e fecha dados
        if($scope.showData===false) {
             $scope.showData = true;
        } else {
             $scope.showData = false;
               // $scope.closeProfileInfo();

        }

    };
 
    function confirma($tp){
         var texto= "";
         
         alert({$args: $tp});
          if ($tp==='S'){
            texto="Deseja realmente desativar a modalidade?";  
          }else{
            texto="Deseja realmente ativar a modalidade?";    
          }    
        
          swal({
                title: texto,
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sim, confirma!'
          }).then(function () {
                swal(
                    'Deleted!',
                    'Your file has been deleted.',
                    'success'
                );
          })        
        
    };
    
    $scope.teste1 = function ($modalidade) {
        

        confirma($scope.modalidade.stmenuapp);


         $scope.modalidade=$modalidade;
         if ($scope.modalidade.stmenuapp==='N'){
            $scope.modalidade.stmenuapp='S' ;
         } else{
           $scope.modalidade.stmenuapp='N' ;  
         };

         ModalidadeService.update($modalidade.id,$scope.modalidade).then(function(response){
         $scope.modalidade = response.data;
           
         $timeout(function(){
             $scope.$apply();
         });
       }, function(response){
           alert({$args: 'Erro ao buscar informações do local'});
       });
    };    
    
    $scope.select = function ($local) {
        $scope.local = {};
       LocalService.show($local.id).then(function(response){
           $scope.local = response.data;

           
           $('#locais_edit').modal('show');
           
           $timeout(function(){
               $scope.$apply();
           });
       }, function(response){
           alert({$args: 'Erro ao buscar informações do local'});
       });
    };
    

    $scope.destroy = function ($local) {
       $scope.local = {};
       LocalService.destroy($local.id).then(function(response){
           alert({$args: 'Local removido com sucesso'});
           
            getLocals();
           
           $timeout(function(){
               $scope.$apply();
           });
       }, function(response){
           alert({$args: 'Erro ao remover local'});
       });
    };    

    $scope.totalRegistros = function () {
        return parseInt($scope.clients.length) + parseInt($scope.prospects.length);
    };

    $scope.register = function () {
        $scope.loading = true;
        $scope.errors = {};
        $scope.local.idunidade = $scope.unidade;
        LocalService.register($scope.local).then(function (response) {
            getLocals();
            $timeout(function(){
                $scope.$apply();
            });

            $scope.loading = false;
            $('#local_modal').modal('hide');
        }, function (response) {
            $scope.loading = false;
            $scope.errors = response.data;
            alert({$args: 'Erro ao cadastrar novo local'});
        });
    };

    $scope.update = function () {
        LocalService.update($scope.client.id, $scope.local).then(function (response) {
           alert({$args: 'Cliente atualizado com sucesso!!'});
           getLocals();
            $scope.select($scope.local);
        }, function (response) {
            alert({$args: 'Ocorreu um erro ao atualizar os dados do local'});
        });
    };


    function getLocals() {


        LocalService.all().then(function (response) {
            $scope.locais = response.data.locais;
            $timeout(function(){
                $scope.$apply();
            });
        }, function (response) {
            alert({$args: 'Erro ao buscar locais '});
        });
    }
    function getModalidades() {


        ModalidadeService.all().then(function (response) {
            $scope.modalidades = response.data.modalidades;

            $timeout(function(){
                $scope.$apply();
            });
        }, function (response) {
            alert({$args: 'Erro ao buscar locais '});
        });
    }
    
    
////////////////////////funcionarios    
    $scope.selectFuncionario = function ($funcionario) {
        $scope.funcionario = {};
       FuncionarioService.show($funcionario.id).then(function(response){
           $scope.local = response.data;

           
           $('#funcionarios_edit').modal('show');
           
           $timeout(function(){
               $scope.$apply();
           });
       }, function(response){
           alert({$args: 'Erro ao buscar informações do funcionario'});
       });
    };
    

    $scope.destroyFuncionario = function ($funcionario) {
       $scope.local = {};
       FuncionarioService.destroy($funcionario.id).then(function(response){
           alert({$args: 'Funcionário removido com sucesso'});
           
            getLocals();
           
           $timeout(function(){
               $scope.$apply();
           });
       }, function(response){
           alert({$args: 'Erro ao remover funcionario'});
       });
    };    


    $scope.registerFuncionario = function () {
        $scope.loading = true;
        $scope.errors = {};
        $scope.pessoa.idunidade = $scope.idunidade;
        alert({$args: $scope.pessoa.email});
        FuncionarioService.register($scope.pessoa).then(function (response) {
            getFuncionarios();
            $timeout(function(){
                $scope.$apply();
            });

            $scope.loading = false;
            $('#funcionario_modal').modal('hide');
        }, function (response) {
            $scope.loading = false;
            $scope.errors = response.data;
            alert({$args: 'Erro ao cadastrar novo funcionario'});
        });
    };

    $scope.teste = function ($estado) {
        //alert($estado);
       UnidadeService.carregaCidade($estado).then(function (response) {
             $scope.cidades = response.data.cidades;

            $timeout(function(){
                $scope.$apply();
            });
        }, function (response) {
            alert({$args: 'Erro ao buscar funcionarios'});
        });
       
    };

    $scope.updateFuncionario = function () {
        LocalService.update($scope.funcionario.id, $scope.funcionario).then(function (response) {
           alert({$args: 'Funcionario atualizado com sucesso!!'});
           getFuncionarios();
            $scope.select($scope.funcionario);
        }, function (response) {
            alert({$args: 'Ocorreu um erro ao atualizar os dados do funcionario'});
        });
    };

    
    function getFuncionarios() {


        FuncionarioService.all().then(function (response) {
            $scope.funcionarios = response.data.funcionarios;

            $timeout(function(){
                $scope.$apply();
            });
        }, function (response) {
            alert({$args: 'Erro ao buscar funcionarios'});
        });
    }
    
///////////////////////////////////////////unidades / filiais
    $scope.selectUnidade = function ($unidade) {
        $scope.funcionario = {};
       UnidadeService.show($unidade.id).then(function(response){
           $scope.local = response.data;

           
           $('#funcionarios_edit').modal('show');
           
           $timeout(function(){
               $scope.$apply();
           });
       }, function(response){
           alert({$args: 'Erro ao buscar informações do funcionario'});
       });
    };
    

    $scope.destroyUnidade = function ($unidade) {
       $scope.local = {};
       UnidadeService.destroy($unidade.id).then(function(response){
           alert({$args: 'Unidade removida com sucesso'});
           
            getLocals();
           
           $timeout(function(){
               $scope.$apply();
           });
       }, function(response){
           alert({$args: 'Erro ao remover unidade'});
       });
    };    


    $scope.registerUnidade = function () {
        $scope.loading = true;
        $scope.errors = {};
        $scope.unidade.user_id = $scope.user_id;
        $scope.unidade.parent_id = $scope.empresa.idunidade;
        alert(''+$scope.unidade.parent_id);
        //$scope.unidade.idunidade = $scope.idunidade;
        
        //$scope.unidade.idunidade = $scope.unidade;
        UnidadeService.register($scope.unidade).then(function (response) {
            $timeout(function(){
                $scope.$apply();
            });
            $scope.loading = false;
            
            $('#empresa_modal').modal('hide');
            getUnidades();
            
        }, function (response) {
            $scope.loading = false;
            $scope.errors = response.data;
            alert({$args: 'Erro ao cadastrar nova unidade'});
        });
    };

    $scope.updateUnidade = function ($empresa) {
          $scope.empresa=$empresa;
           UnidadeService.update($empresa.id, $scope.empresa).then(function (response) {
           alert({$args: 'Matriz atualizada com sucesso!!'});
           //getUnidades();
            
        }, function (response) {
            alert({$args: 'Ocorreu um erro ao atualizar os dados da matriz'});
        });
    };

    
    function getUnidades() {


        UnidadeService.all().then(function (response) {
            $scope.unidades = response.data.unidades;
            $scope.estados = response.data.estados;

            $timeout(function(){
                $scope.$apply();
            });
        }, function (response) {
            alert({$args: 'Erro ao buscar unidades'});
        });
    }
    function getMatriz() {
        $scope.empresa = {};
        $scope.dado = {};
        UnidadeService.matriz().then(function (response) {
            $scope.empresas = response.data.empresa;
            $scope.empresa=$scope.empresas[0];  

            $scope.empresadados = response.data.dados;
            $scope.empresadado=$scope.empresadados[0];  
            $scope.unidade.parent_id=$scope.empresadados[0].idunidade;
            $timeout(function(){
                $scope.$apply();
            });
        }, function (response) {
            alert({$args: 'Erro ao buscar unidades'});
        });
    }


    
    
}); // fim controler alunoController

