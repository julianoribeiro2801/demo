app.controller('gfmController_home', function ($http, $scope, $timeout) {
    // GFMS

    // LISTA PROGRAMAS POR ACADEMIA
    $scope.checado = false;
    $scope.aula_atual = '';
    $scope.data_aula = '';
    $scope.aulasant = [];
    $scope.aulaSelect = {};
    $scope.privado = {};
    $scope.aula = {};
    $scope.horario = {};

    $scope.qt_alunos = [];
    $scope.qt_vagas = [];
    $scope.capacidade = [];
    $scope.dt_aula = [];

    $scope.programas = [];
    $scope.listareservas = [];
    $scope.matriculas = [];





    $scope.getListareserva = function ($id, $dtaula, $privado) {

        if ($privado == 'Não') {
            $http({
                url: '../api/getListareserva/' + $id,
                method: "GET"
            }).then(function (response) {
                $scope.listareservas = response.data.reservas;
            });
        } else {
            $http({
                url: '../api/getListamatricula/' + $id + '/' + $dtaula,
                method: "GET"
            }).then(function (response) {
                $scope.matriculas = response.data.matriculas;
            });
        }
    };

    $scope.getListamatricula = function ($id, $dataaula) {

        $http({
            url: '../api/getListamatricula/' + $id + '/' + $dataaula,
            method: "GET"
        }).then(function (response) {
            $scope.matriculas = response.data.matriculas;
        });
    };

    $scope.getAula = function (id) {
        $http({
            url: '../api/getAula/' + id + '/0',
            method: "GET"
        }).then(function (response) {
            $scope.qt_vagas = response.data.n_reservas;
            $scope.dt_aula = response.data.dtaula;
            $scope.capacidade = response.data.capacidade;
            $scope.matriculados = response.data.matriculados;

        }, function (response) {
            console.log('Opsss... Algo deu errado!');
        });

    };

    $scope.checa = function (indice, aluno, aula) {


        if ($scope.matriculas[indice].presente === 0) {
            $scope.matriculas[indice].presente = 1;
            $scope.presencaAdd(aula, aluno);
        } else {
            $scope.matriculas[indice].presente = 0;
            $scope.presencaDel(aula, aluno);

        }



    };


    $scope.addGfm = function () {
        $scope.statusBotoesGfm(true);
        $http({
            url: 'api/gfm/addGfm',
            method: "POST"
        }).then(function (response) {
            $scope.gfm = response.data;
            $scope.listaGfms();
            $scope.editingData[$scope.gfm.id] = true;
            // ATIVA OS BOTOES
            $scope.statusBotoesGfm(false);
        }, function (response) {
            console.log('Opsss... Algo deu errado!');
        });
    };
    $scope.addReserva = function (idaula, id) {
        if (id !== undefined) {
            $scope.statusBotoesGfm(true);
            $http({
                url: '../api/reservaAdd/' + idaula + '/' + id,
                method: "GET"
            }).then(function (response) {
                if (response.data.cod == '-1') {
                    swal("Opsss!", response.data.text + "!", "error");
                }
                $scope.getListareserva(idaula);
                getClients();

            }, function (response) {
                console.log('Opsss... Algo deu errado!');
            });
        } else {
            swal("!", "Selecione um gfm para deletar!", "error");
        }
    };



    $scope.reservaDel = function (idaula, id) {

        swal({
  title: "Tem certeza?",
            text: "Deseja realmente cancelar a reserva?",
  icon: "warning",
  buttons: true,
  buttons: {
      cancel: {
        text: "Cancelar",
        value: null,
        visible: true,
        className: "",
        closeModal: true,
      },
      confirm: {
        text: "Sim, tenho certeza!",
        value: true,
        visible: true,
        className: "",
        closeModal: true
      }},
  dangerMode: true,

}).then((willDelete) => {
  if (willDelete) {
    // aqui chama o ajax
    $scope.statusBotoesGfm(true);
            $http({
                url: '../api/reservaDel/' + idaula + '/' + id,
                method: "GET"
            }).then(function (response) {
                $scope.getListareserva(idaula);
                $scope.getAula(idaula);
            }, function (response) {
                console.log('Opsss... Algo deu errado!');
            });
  } else {
    // swal("Your imaginary file is safe!");
  }
});
// fim swal
// 
    };

    $scope.presencaDel = function (idaula, id) {

        $scope.statusBotoesGfm(true);
        $http({
            url: '../api/reservaDel/' + idaula + '/' + id,
            method: "GET"
        }).then(function (response) {
            //$scope.getListareserva(idaula);
            $scope.getListamatricula(idaula, $scope.data_aula);
            $scope.getAula(idaula);
        }, function (response) {
            console.log('Opsss... Algo deu errado!');
        });
    };

    $scope.matriculaDel = function (idaula, id) {

        swal({
 title: "Tem certeza?",
            text: "Deseja realmente cancelar a matrícula?",
  icon: "warning",
  buttons: true,
  buttons: {
      cancel: {
        text: "Cancelar",
        value: null,
        visible: true,
        className: "",
        closeModal: true,
      },
      confirm: {
        text: "Sim, tenho certeza!",
        value: true,
        visible: true,
        className: "",
        closeModal: true
      }},
  dangerMode: true,

}).then((willDelete) => {
  if (willDelete) {
    // aqui chama o ajax
   $scope.statusBotoesGfm(true);
            $http({
                url: '../api/matriculaDel/' + idaula + '/' + id,
                method: "GET"
            }).then(function (response) {
                $scope.getListamatricula(idaula, $scope.data_aula);
                $scope.getAula(idaula);
            }, function (response) {
                console.log('Opsss... Algo deu errado!');
            });
  } else {
    // swal("Your imaginary file is safe!");
  }
});
// fim swal
// 
       
    };

    $scope.editingData = {};

    $scope.alterGfm = function (gfm) {
        $scope.editingData[gfm.id] = true;
    };

    $scope.upGfm = function (gfm) {

        if (gfm.capacidademin <= gfm.capacidade) {


            $scope.statusBotoesGfm(true);
            $http({
                url: 'api/gfm/upGfm',
                data: {
                    id: gfm.id,
                    idlocal: gfm.idlocal,
                    idprograma: gfm.idprograma,
                    nmprograma: gfm.nmprograma,
                    idfuncionario: gfm.idfuncionario,
                    nivel: gfm.nivel,
                    privado: gfm.privado,
                    gfmspm: gfm.gfmspm,
                    hora_inicio: gfm.hora_inicio,
                    duracao: gfm.duracao,
                    diasemana: 0,
                    domingo: gfm.domingo,
                    segunda: gfm.segunda,
                    terca: gfm.terca,
                    quarta: gfm.quarta,
                    quinta: gfm.quinta,
                    sexta: gfm.sexta,
                    sabado: gfm.sabado,
                    valor: gfm.valor,
                    capacidademin: gfm.capacidademin,
                    capacidade: gfm.capacidade,
                    fator: gfm.fator
                },
                method: "POST"
            }).then(function (response) {
                $scope.editingData[gfm.id] = false;
                $scope.getTaxaOcupacao();
                $scope.listaGfms();
                // ATIVA OS BOTOES
                $scope.statusBotoesGfm(false);
            }, function (response) {
                console.log('Opsss... Algo deu errado!');
            });
        } else {
            swal("Opsss!", "Capacidade máxima deve ser maior que a capacidade mínima" + "!", "error");

        }
    };

    $scope.delGfm = function (id) {
        if (id !== undefined) {

            swal({
   title: "Tem certeza?",
                text: "Tem certeza que deseja deletar esse gfm?",
  icon: "warning",
  buttons: true,
  buttons: {
      cancel: {
        text: "Cancelar",
        value: null,
        visible: true,
        className: "",
        closeModal: true,
      },
      confirm: {
        text: "Sim, tenho certeza!",
        value: true,
        visible: true,
        className: "",
        closeModal: true
      }},
  dangerMode: true,

}).then((willDelete) => {
  if (willDelete) {
    // aqui chama o ajax
    $http({
                    url: 'api/gfm/delGfm/' + id,
                    method: "POST"
                }).then(function (response) {
                    $scope.listaGfms();
                    swal("Deletado!", "Gfm deletado com sucesso!", "success");
                });
  } else {
    // swal("Your imaginary file is safe!");
  }
});
// fim swal
           
        } else {
            swal("Opsss!", "Selecione um gfm para deletar!", "error");
        }
    };
    $scope.adicionaAula = function (nmprograma) {
        $scope.statusBotoesGfm(true);
        $http({
            url: 'api/spm/addAula',
            data: {
                nmprograma: nmprograma
            },
            method: "POST"
        }).then(function (response) {
            $('#addAula').modal('hide');
            $scope.listaProgramas();
            // ATIVA OS BOTOES
            $scope.statusBotoesSpm(false);
        }, function (response) {
            console.log('Opsss... Algo deu errado!');
        });
    };



    $scope.addAlunoAula = function (indice) {

        $scope.inserted = {
            id: 0,
            name: 'ANÔNIMO'

        };

        $scope.listareservas.push($scope.inserted);
    };


    $scope.addAlunoMatricula = function (indice) {

        $scope.inserted = {
            idaluno: 0,
            name: 'ANÔNIMO'

        };

        $scope.matriculas.push($scope.inserted);
    };
    $scope.statusBotoesGfm = function (sts) {
        $scope.addGfmBtn = sts;
        $scope.delGfmBtn = sts;
        $scope.editGfmBtn = sts;
        $scope.saveGfmBtn = sts;
    };

    /* $scope.diaSemana = function (dia) {
     switch (dia) {
     case '1':
     return 'Domingo';
     break;
     case '2':
     return 'Segunda';
     break;
     case '3':
     return 'Terça';
     break;
     case '4':
     return 'Quarta';
     break;
     case '5':
     return 'Quinta';
     break;
     case '6':
     return 'Sexta';
     break;
     case '7':
     return 'Sábado';
     break;
     }
     };*/

    $scope.diaSemana = function (dias) {
        var tamanho = dias.length;
        var diassemana = '';
        for (var i = 0; i < tamanho; i++) {
            switch (dias[i]) {
                case '1':
                    dias[i] = 'D, ';
                    break;
                case '2':
                    dias[i] = '2ª, ';
                    break;
                case '3':
                    dias[i] = '3ª, ';
                    break;
                case '4':
                    dias[i] = '4ª, ';
                    break;
                case '5':
                    dias[i] = '5ª, ';
                    break;
                case '6':
                    dias[i] = '6ª, ';
                    break;
                case '7':
                    dias[i] = 'S, ';
                    break;
            }
            diassemana += dias[i];
        }
        diassemana = diassemana.substr(0, diassemana.length - 2);
        return diassemana;
    };



});