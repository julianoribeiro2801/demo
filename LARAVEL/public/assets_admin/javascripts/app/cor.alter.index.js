$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$("input[name=cor_tipo]:radio").change(function () {
    var tipo = $(this).val();
    if(tipo === '1') {
    	$(".tipoCor").fadeIn("slow");
        $(".tipoImg").fadeOut("slow");

        $("#cor_imagem").attr('disabled','disabled');
        $("#cor_hex").removeAttr('disabled');
    } else {
        $(".tipoImg").fadeIn("slow");
        $(".tipoCor").fadeOut("slow");

        $("#cor_hex").attr('disabled','disabled');
        $("#cor_imagem").removeAttr('disabled');
    }
});