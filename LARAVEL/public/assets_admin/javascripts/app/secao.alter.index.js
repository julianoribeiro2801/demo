$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function getCategoria() {
    var secao = document.getElementById("produto_cat_parent").value;

    var secoes = $('#produto_cat_parent');
    var categoria = $('#produto_category');

    categoria.attr('disabled', 'true');
    secoes.attr('disabled', 'true');

    categoria.html('<option value=""> Carregando categorias... </option>');

    $.post('categorias', {
        secao: secao
    }, function(retorno) {
        categoria.html(retorno).removeAttr('disabled');
        secoes.removeAttr('disabled');
    });
}

function getCategoriaUp() {
    var secao = document.getElementById("produto_cat_parent").value;

    var secoes = $('#produto_cat_parent');
    var categoria = $('#produto_category');

    categoria.attr('disabled', 'true');
    secoes.attr('disabled', 'true');

    categoria.html('<option value=""> Carregando categorias... </option>');

    $.post('../categorias', {
        secao: secao
    }, function(retorno) {
        categoria.html(retorno).removeAttr('disabled');
        secoes.removeAttr('disabled');
    });
}