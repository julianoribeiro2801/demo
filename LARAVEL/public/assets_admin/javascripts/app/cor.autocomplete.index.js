$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

// Autocomplete dos produtos relacionados
$('input[name=\'rel2\']').autocomplete({
    source: function(request, response) {
        $.post('cores', {
            corcod: $('#input-rel2').val()
        }, function(data) {
            response($.map(data, function(item) {
                return {
                    label: item['cor_cod'] + ' - ' + item['cor_titulo'],
                    value: item['cor_id']
                }
            }));
        });
    },
    select: function(item, it) {
        $('.ui-helper-hidden-accessible').hide();
        $('#input-rel2').val('');
        $('#featured-rel2' + it.item.value).remove();
        $('#featured-rel2').append('<div id="featured-rel2' + it.item.value + '"><i class="fa fa-minus-circle"></i> ' + it.item.label + '<input type="hidden" name="rel2[]" value="' + it.item.value + '" /></div>');
        return false;
    }
});
// Retira produtos relacionados
$('#featured-rel2').delegate('.fa-minus-circle', 'click', function() {
    $(this).parent().remove();
});