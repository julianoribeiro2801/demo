$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

// Autocomplete dos produtos relacionados
$('input[name=\'rel\']').autocomplete({
    source: function(request, response) {
        $.post('clientes', {
            usuario_fantasia: $('#input-rel').val()
        }, function(data) {
            response($.map(data, function(item) {
                return {
                    label: item['usuario_razaosocial'],
                    value: item['usuario_id']
                }
            }));
        });
    },
    select: function(item, it) {
        $('.ui-helper-hidden-accessible').hide();
        $('#input-rel').val('');
        $('#featured-rel' + it.item.value).remove();
        $('#featured-rel').append('<div id="featured-rel' + it.item.value + '"><i class="fa fa-minus-circle"></i> ' + it.item.label + '<input type="hidden" name="rel[]" value="' + it.item.value + '" /></div>');
        return false;
    }
});
// Retira produtos relacionados
$('#featured-rel').delegate('.fa-minus-circle', 'click', function() {
    $(this).parent().remove();
});