$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function deletar(id) {
    $('#modal-deleta .modal-body b').html('<img src="../../assets_admin/images/load.gif">');
    $.post('deleta', {
        id: id, acao: 'return'
    }, function(retorno) {
        $('#modal-deleta .modal-body b').html(retorno[0]['category_title']);
    }, "json");
    $('#modal-deleta').data('id', id).modal('show');
}

$('#btnYes').click(function() {
    $('#btnYes').attr("disabled","disabled").text('aguarde...');
    var id = $('#modal-deleta').data('id');
    $.post('deleta', {
        id: id, acao: 'delete'
    }, function(retorno) {
        new PNotify({
            title: retorno['title'],
            text: retorno['text'],
            type: retorno['type']
        });
        if(retorno['type'] === 'success'){
            $('[data-id='+id+']').closest('tr').css("background-color", "#d2322d");
            $('[data-id='+id+']').closest('tr').fadeOut("slow");
        }
        $('#btnYes').removeAttr("disabled").text('Confirmar');
        $('#modal-deleta').modal('hide');
    }, "json");
});