$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function deletar(id) {
    $('#modal-deleta .modal-body article').html('<img src="../../assets_admin/images/load.gif">');
    $.post('deleta', {
        id: id, acao: 'return'
    }, function(retorno) {
        $('#modal-deleta .modal-body article').html(retorno);
    });
    $('#modal-deleta').data('id', id).modal('show');
}

$('#btnYes').click(function() {
    $('#btnYes').attr("disabled","disabled").text('aguarde...');
    var id = $('#modal-deleta').data('id');
    var pedido_vendedor = $('#pedido_vendedor').val();
    var pedido_transporte = $('#pedido_transporte').val();
    var pedido_condicoes = $('#pedido_condicoes').val();
    var pedido_obs = $('#pedido_obs').val();

    $.post('deleta', {
        id: id, acao: 'delete', pedido_vendedor: pedido_vendedor, pedido_transporte: pedido_transporte, pedido_condicoes: pedido_condicoes, pedido_obs: pedido_obs
    }, function(retorno) {
        new PNotify({
            title: retorno['title'],
            text: retorno['text'],
            type: retorno['type']
        });
        if(retorno['type'] === 'success'){
            $('[data-id='+id+']').closest('tr').css("background-color", "#2FA60D");
            $('[data-id='+id+']').closest('tr').fadeOut("slow");
        }
        $('#btnYes').removeAttr("disabled").text('Confirmar');
        $('#modal-deleta').modal('hide');
    }, "json");
});

$('#btnDel').click(function() {
    var id = $('#modal-deleta').data('id');
    $('#modal-deleta').modal('hide');
    excluir(id);
});

function excluir(id) {
    $('#modal-excluir .modal-body b').html('<img src="../../assets_admin/images/load.gif">');
    $.post('deleta', {
        id: id, acao: 'ver'
    }, function(retorno) {
        $('#modal-excluir .modal-body b').html(retorno[0]['pedido_id']);
    }, "json");
    $('#modal-excluir').data('id', id).modal('show');
}

$('#btnYesExcluir').click(function() {
    $('#btnYesExcluir').attr("disabled","disabled").text('aguarde...');
    var id = $('#modal-excluir').data('id');
    $.post('deleta', {
        id: id, acao: 'excluir'
    }, function(retorno) {
        new PNotify({
            title: retorno['title'],
            text: retorno['text'],
            type: retorno['type']
        });
        if(retorno['type'] === 'success'){
            $('[data-id='+id+']').closest('tr').css("background-color", "#d2322d");
            $('[data-id='+id+']').closest('tr').fadeOut("slow");
        }
        $('#btnYesExcluir').removeAttr("disabled").text('Confirmar');
        $('#modal-excluir').modal('hide');
    }, "json");
});