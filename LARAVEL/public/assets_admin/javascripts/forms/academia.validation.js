$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

(function () {
    'use strict';
    $("#PostFormAcademia").validate({
        highlight: function (label) {
            $(label).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (label) {
            $(label).closest('.form-group').removeClass('has-error');
            label.remove();
        },
        errorPlacement: function (error, element) {
            var placement = element.closest('.input-group');
            if (!placement.get(0)) {
            	placement = element;
            }
            if (error.text() !== '') {
                placement.after(error);
            }
        },
        rules: {
            academia_title: {required: true, minlength: 2, maxlength: 100},
            academia_cnpj: {required: true},
            academia_rua: {required: true, minlength: 2, maxlength: 100},
            academia_numero: {required: true, minlength: 2, maxlength: 10},
            academia_bairro: {required: true, minlength: 2, maxlength: 100},
            academia_telefone: {required: true},
            academia_status: {required: true},
            academia_raio: {required: true, number: true, min: 0, max: 100000},
        },
        messages: {
            academia_title: {required: "Informe o título!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 100 caracteres!"},
            academia_cnpj: {required: "Informe o cnpj!"},
            academia_rua: {required: "Informe a rua!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 100 caracteres!"},
            academia_numero: {required: "Informe o número!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 10 caracteres!"},
            academia_bairro: {required: "Informe o bairro!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 100 caracteres!"},
            academia_telefone: {required: "Informe o telefone!"},
            academia_status: {required: "Selecione o status!"},
            academia_raio: {required: "Informe o raio de bloqueio!", number: "Informe apenas números!", min: "Informe um número maior que 0", max: "Informe um número menor que 100000"},
        },
        submitHandler: function (form) {
            $('#SendPostForm').attr("disabled", "disabled");
            $('.load').show();
            $('#PostFormAcademia').ajaxSubmit({
                type: 'post',
                url: 'insere',
                uploadProgress: function (evento, posicao, total, porcentagem) {
                    $('.progress-bar').css('width', porcentagem + '%');
                    $('.progress-bar').html(porcentagem + '%');
                }, success: function (retorno) {
                    new PNotify({
                        title: retorno['title'],
                        text: retorno['text'],
                        type: retorno['type']
                    });
                    if (retorno['type'] === 'success') {
                        setTimeout(function () {
                            window.location = "gerenciar"
                        }, 3000);
                    } else {
                        $('#SendPostForm').removeAttr("disabled");
                        setTimeout(function () {
                            $('.load').hide();
                            $('.progress-bar').css('width', '');
                            $('.progress-bar').html('');
                        }, 2000);
                        return false;
                    }
                }, error: function () {
                    new PNotify({
                        title: 'Erro!',
                        text: 'Opsss... Algo deu errado!',
                        type: 'error'
                    });
                    $('#SendPostForm').removeAttr("disabled");
                    setTimeout(function () {
                        $('.load').hide();
                        $('.progress-bar').css('width', '');
                        $('.progress-bar').html('');
                    }, 2000);
                    return false;
                }
            });
        }
    });
    $("#PostFormAcademiaUp").validate({
        highlight: function (label) {
            $(label).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (label) {
            $(label).closest('.form-group').removeClass('has-error');
            label.remove();
        },
        errorPlacement: function (error, element) {
            var placement = element.closest('.input-group');
            if (!placement.get(0)) {
                placement = element;
            }
            if (error.text() !== '') {
                placement.after(error);
            }
        },
        rules: {
            academia_title: {required: true, minlength: 2, maxlength: 100},
            academia_cnpj: {required: true},
            academia_rua: {required: true, minlength: 2, maxlength: 100},
            academia_numero: {required: true, minlength: 2, maxlength: 10},
            academia_bairro: {required: true, minlength: 2, maxlength: 100},
            academia_telefone: {required: true},
            academia_status: {required: true}
        },
        messages: {
            academia_title: {required: "Informe o título!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 100 caracteres!"},
            academia_cnpj: {required: "Informe o cnpj!"},
            academia_rua: {required: "Informe a rua!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 100 caracteres!"},
            academia_numero: {required: "Informe o número!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 10 caracteres!"},
            academia_bairro: {required: "Informe o bairro!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 100 caracteres!"},
            academia_telefone: {required: "Informe o telefone!"},
            academia_status: {required: "Selecione o status!"}
        },
        submitHandler: function (form) {
            $('#SendPostForm').attr("disabled", "disabled");
            $('.load').show();
            $('#PostFormAcademiaUp').ajaxSubmit({
                type: 'post',
                url: '../atualiza',
                uploadProgress: function (evento, posicao, total, porcentagem) {
                    $('.progress-bar').css('width', porcentagem + '%');
                    $('.progress-bar').html(porcentagem + '%');
                }, success: function (retorno) {
                    new PNotify({
                        title: retorno['title'],
                        text: retorno['text'],
                        type: retorno['type']
                    });
                    if (retorno['type'] === 'success') {
                        setTimeout(function () {
                            window.location = "../gerenciar"
                        }, 3000);
                    } else {
                        $('#SendPostForm').removeAttr("disabled");
                        setTimeout(function () {
                            $('.load').hide();
                            $('.progress-bar').css('width', '');
                            $('.progress-bar').html('');
                        }, 2000);
                        return false;
                    }
                }, error: function () {
                    new PNotify({
                        title: 'Erro!',
                        text: 'Opsss... Algo deu errado!',
                        type: 'error'
                    });
                    $('#SendPostForm').removeAttr("disabled");
                    setTimeout(function () {
                        $('.load').hide();
                        $('.progress-bar').css('width', '');
                        $('.progress-bar').html('');
                    }, 2000);
                    return false;
                }
            });
        }
    });
}).apply(this, [jQuery]);