$.ajaxSetup({
	headers: {
		'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')
	}
});

(function () {
	'use strict';
	$("#PostFormEmpresaUp").validate({
		highlight: function (label) {
			$(label).closest('.form-group').removeClass('has-success').addClass('has-error');
		},
		success: function (label) {
			$(label).closest('.form-group').removeClass('has-error');
			label.remove();
		},
		errorPlacement: function (error, element) {
			var placement = element.closest('.input-group');
			if (!placement.get(0)) {
				placement = element;
			}
			if (error.text() !== '') {
				placement.after(error);
			}
		},
		rules: {
			fantasia: {required: true, minlength: 2, maxlength: 60},
			razao_social: {required: true, minlength: 2, maxlength: 60},
			cnpj: {required: true},
			inscricao_estadual: {number: true, minlength: 2, maxlength: 20},
			endereco: {required: true, minlength: 2, maxlength: 60},
			numero: {required: true, minlength: 2, maxlength: 10},
			cep: {required: true},
			bairro: {required: true, minlength: 2, maxlength: 60},
			idestado: {required: true},
			idcidade: {required: true},
			telefone: {required: true},
			celular: {required: true},
			email: {required: true, email: true},
			site: {required: true, minlength: 2, maxlength: 100}
		},
		messages: {
			fantasia: {required: "Informe o nome fantasia!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 60 caracteres!"},
			razao_social: {required: "Informe a razão social!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 60 caracteres!"},
			cnpj: {required: "Informe o cnpj!"},
			inscricao_estadual: {number: "Ïnforme apenas números!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 20 caracteres!"},
			endereco: {required: "Informe o endereço!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 60 caracteres!"},
			numero: {required: "Informe o número!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 10 caracteres!"},
			cep: {required: "Informe o cep!"},
			bairro: {required: "Informe o bairro!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 60 caracteres!"},
			idestado: {required: "Selecione um estado!"},
			idcidade: {required: "Selecione uma cidade!"},
			telefone: {required: "Informe o telefone!"},
			celular: {required: "Informe o celular!"},
			email: {required: "Informe o e-mail!", email: "Informe um e-mail válido!"},
			site: {required: "Informe o site!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 100 caracteres!"}
		},
		submitHandler: function (form) {
			$('#SendPostForm').attr("disabled", "disabled");
			$('.loadEmpresa').show();
			$('#PostFormEmpresaUp').ajaxSubmit({
				type: 'post',
				url: '../api/unidade/upEmpresa',
				uploadProgress: function (evento, posicao, total, porcentagem) {
					$('.progress-bar').css('width', porcentagem + '%');
					$('.progress-bar').html(porcentagem + '%');
				}, success: function (retorno) {
					console.log(retorno);
					new PNotify({
						title: retorno['title'],
						text: retorno['text'],
						type: retorno['type']
					});
					$('#SendPostForm').removeAttr("disabled");
					setTimeout(function () {
						$('.loadEmpresa').hide();
						$('.progress-bar').css('width', '');
						$('.progress-bar').html('');
					}, 2000);
					return false;
				}, error: function () {
					new PNotify({
						title: 'Erro!',
						text: 'Opsss... Algo deu errado!',
						type: 'error'
					});
					$('#SendPostForm').removeAttr("disabled");
					setTimeout(function () {
						$('.loadEmpresa').hide();
						$('.progress-bar').css('width', '');
						$('.progress-bar').html('');
					}, 2000);
					return false;
				}
			});
		}
	});
	$("#PostFormFilial").validate({
		highlight: function (label) {
			$(label).closest('.form-group').removeClass('has-success').addClass('has-error');
		},
		success: function (label) {
			$(label).closest('.form-group').removeClass('has-error');
			label.remove();
		},
		errorPlacement: function (error, element) {
			var placement = element.closest('.input-group');
			if (!placement.get(0)) {
				placement = element;
			}
			if (error.text() !== '') {
				placement.after(error);
			}
		},
		rules: {
			fantasia: {required: true, minlength: 2, maxlength: 60},
			razao_social: {required: true, minlength: 2, maxlength: 60},
			cnpj: {required: true},
			inscricao_estadual: {number: true, minlength: 2, maxlength: 20},
			endereco: {required: true, minlength: 2, maxlength: 60},
			numero: {required: true, minlength: 2, maxlength: 10},
			cep: {required: true},
			bairro: {required: true, minlength: 2, maxlength: 60},
			idestado: {required: true},
			idcidade: {required: true},
			telefone: {required: true},
			celular: {required: true},
			email: {required: true, email: true}
		},
		messages: {
			fantasia: {required: "Informe o nome fantasia!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 60 caracteres!"},
			razao_social: {required: "Informe a razão social!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 60 caracteres!"},
			cnpj: {required: "Informe o cnpj!"},
			inscricao_estadual: {number: "Ïnforme apenas números!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 20 caracteres!"},
			endereco: {required: "Informe o endereço!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 60 caracteres!"},
			numero: {required: "Informe o número!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 10 caracteres!"},
			cep: {required: "Informe o cep!"},
			bairro: {required: "Informe o bairro!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 60 caracteres!"},
			idestado: {required: "Selecione um estado!"},
			idcidade: {required: "Selecione uma cidade!"},
			telefone: {required: "Informe o telefone!"},
			celular: {required: "Informe o celular!"},
			email: {required: "Informe o e-mail!", email: "Informe um e-mail válido!"}
		},
		submitHandler: function (form) {
			$('#SendPostForm').attr("disabled", "disabled");
			$('.load').show();
			$('#PostFormFilial').ajaxSubmit({
				type: 'post',
				url: '../api/unidade/addFilial',
				uploadProgress: function (evento, posicao, total, porcentagem) {
					$('.progress-bar').css('width', porcentagem + '%');
					$('.progress-bar').html(porcentagem + '%');
				}, success: function (retorno) {
					console.log(retorno);
					new PNotify({
						title: retorno['title'],
						text: retorno['text'],
						type: retorno['type']
					});
					if (retorno['type'] === 'success') {
						setTimeout(function () {
							location.reload();
						}, 3000);
					} else {
						$('#SendPostForm').removeAttr("disabled");
						setTimeout(function () {
							$('.load').hide();
							$('.progress-bar').css('width', '');
							$('.progress-bar').html('');
						}, 2000);
						return false;
					}
				}, error: function () {
					new PNotify({
						title: 'Erro!',
						text: 'Opsss... Algo deu errado!',
						type: 'error'
					});
					$('#SendPostForm').removeAttr("disabled");
					setTimeout(function () {
						$('.load').hide();
						$('.progress-bar').css('width', '');
						$('.progress-bar').html('');
					}, 2000);
					return false;
				}
			});
		}
	});
	$("#PostFormFilialUp").validate({
		highlight: function (label) {
			$(label).closest('.form-group').removeClass('has-success').addClass('has-error');
		},
		success: function (label) {
			$(label).closest('.form-group').removeClass('has-error');
			label.remove();
		},
		errorPlacement: function (error, element) {
			var placement = element.closest('.input-group');
			if (!placement.get(0)) {
				placement = element;
			}
			if (error.text() !== '') {
				placement.after(error);
			}
		},
		rules: {
			fantasia: {required: true, minlength: 2, maxlength: 60},
			razao_social: {required: true, minlength: 2, maxlength: 60},
			cnpj: {required: true},
			inscricao_estadual: {required: true, number: true, minlength: 2, maxlength: 20},
			endereco: {required: true, minlength: 2, maxlength: 60},
			numero: {required: true, minlength: 2, maxlength: 10},
			cep: {required: true},
			bairro: {required: true, minlength: 2, maxlength: 60},
			idestado: {required: true},
			idcidade: {required: true},
			telefone: {required: true},
			celular: {required: true},
			email: {required: true, email: true},
			site: {required: true, minlength: 2, maxlength: 100}
		},
		messages: {
			fantasia: {required: "Informe o nome fantasia!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 60 caracteres!"},
			razao_social: {required: "Informe a razão social!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 60 caracteres!"},
			cnpj: {required: "Informe o cnpj!"},
			inscricao_estadual: {required: "Informe a inscrição estadual!", number: "Ïnforme apenas números!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 20 caracteres!"},
			endereco: {required: "Informe o endereço!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 60 caracteres!"},
			numero: {required: "Informe o número!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 10 caracteres!"},
			cep: {required: "Informe o cep!"},
			bairro: {required: "Informe o bairro!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 60 caracteres!"},
			idestado: {required: "Selecione um estado!"},
			idcidade: {required: "Selecione uma cidade!"},
			telefone: {required: "Informe o telefone!"},
			celular: {required: "Informe o celular!"},
			email: {required: "Informe o e-mail!", email: "Informe um e-mail válido!"},
			site: {required: "Informe o site!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 100 caracteres!"}
		},
		submitHandler: function (form) {
			$('#SendPostForm').attr("disabled", "disabled");
			$('.load').show();
			$('#PostFormFilialUp').ajaxSubmit({
				type: 'post',
				url: '../api/unidade/upFilial',
				uploadProgress: function (evento, posicao, total, porcentagem) {
					$('.progress-bar').css('width', porcentagem + '%');
					$('.progress-bar').html(porcentagem + '%');
				}, success: function (retorno) {
					console.log(retorno);
					new PNotify({
						title: retorno['title'],
						text: retorno['text'],
						type: retorno['type']
					});
					if (retorno['type'] === 'success') {
						setTimeout(function () {
							location.reload();
						}, 3000);
					} else {
						$('#SendPostForm').removeAttr("disabled");
						setTimeout(function () {
							$('.load').hide();
							$('.progress-bar').css('width', '');
							$('.progress-bar').html('');
						}, 2000);
						return false;
					}
				}, error: function () {
					new PNotify({
						title: 'Erro!',
						text: 'Opsss... Algo deu errado!',
						type: 'error'
					});
					$('#SendPostForm').removeAttr("disabled");
					setTimeout(function () {
						$('.load').hide();
						$('.progress-bar').css('width', '');
						$('.progress-bar').html('');
					}, 2000);
					return false;
				}
			});
		}
	});
	$("#PostFormLocal").validate({
		highlight: function (label) {
			$(label).closest('.form-group').removeClass('has-success').addClass('has-error');
		},
		success: function (label) {
			$(label).closest('.form-group').removeClass('has-error');
			label.remove();
		},
		errorPlacement: function (error, element) {
			var placement = element.closest('.input-group');
			if (!placement.get(0)) {
				placement = element;
			}
			if (error.text() !== '') {
				placement.after(error);
			}
		},
		rules: {
			nmlocal: {required: true, minlength: 2, maxlength: 60},
			metros: {required: true, minlength: 2, maxlength: 60, number: true}
		},
		messages: {
			nmlocal: {required: "Informe o nome!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 60 caracteres!"},
			metros: {required: "Informe o m²!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 10 caracteres!", number: "Informe apenas números!"}
		},
		submitHandler: function (form) {
			$('#SendPostForm').attr("disabled", "disabled");
			$('.load').show();
			$('#PostFormLocal').ajaxSubmit({
				type: 'post',
				url: '../api/unidade/addLocal',
				uploadProgress: function (evento, posicao, total, porcentagem) {
					$('.progress-bar').css('width', porcentagem + '%');
					$('.progress-bar').html(porcentagem + '%');
				}, success: function (retorno) {
					console.log(retorno);
					new PNotify({
						title: retorno['title'],
						text: retorno['text'],
						type: retorno['type']
					});
					if (retorno['type'] === 'success') {
						setTimeout(function () {
							location.reload();
						}, 3000);
					} else {
						$('#SendPostForm').removeAttr("disabled");
						setTimeout(function () {
							$('.load').hide();
							$('.progress-bar').css('width', '');
							$('.progress-bar').html('');
						}, 2000);
						return false;
					}
				}, error: function () {
					new PNotify({
						title: 'Erro!',
						text: 'Opsss... Algo deu errado!',
						type: 'error'
					});
					$('#SendPostForm').removeAttr("disabled");
					setTimeout(function () {
						$('.load').hide();
						$('.progress-bar').css('width', '');
						$('.progress-bar').html('');
					}, 2000);
					return false;
				}
			});
		}
	});
	$("#PostFormLocalUp").validate({
		highlight: function (label) {
			$(label).closest('.form-group').removeClass('has-success').addClass('has-error');
		},
		success: function (label) {
			$(label).closest('.form-group').removeClass('has-error');
			label.remove();
		},
		errorPlacement: function (error, element) {
			var placement = element.closest('.input-group');
			if (!placement.get(0)) {
				placement = element;
			}
			if (error.text() !== '') {
				placement.after(error);
			}
		},
		rules: {
			nmlocal: {required: true, minlength: 2, maxlength: 60},
			metros: {required: true, minlength: 2, maxlength: 60, number: true}
		},
		messages: {
			nmlocal: {required: "Informe o nome!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 60 caracteres!"},
			metros: {required: "Informe o m²!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 10 caracteres!", number: "Informe apenas números!"}
		},
		submitHandler: function (form) {
			$('#SendPostForm').attr("disabled", "disabled");
			$('.load').show();
			$('#PostFormLocalUp').ajaxSubmit({
				type: 'post',
				url: '../api/unidade/upLocal',
				uploadProgress: function (evento, posicao, total, porcentagem) {
					$('.progress-bar').css('width', porcentagem + '%');
					$('.progress-bar').html(porcentagem + '%');
				}, success: function (retorno) {
					console.log(retorno);
					new PNotify({
						title: retorno['title'],
						text: retorno['text'],
						type: retorno['type']
					});
					if (retorno['type'] === 'success') {
						setTimeout(function () {
							location.reload();
						}, 3000);
					} else {
						$('#SendPostForm').removeAttr("disabled");
						setTimeout(function () {
							$('.load').hide();
							$('.progress-bar').css('width', '');
							$('.progress-bar').html('');
						}, 2000);
						return false;
					}
				}, error: function () {
					new PNotify({
						title: 'Erro!',
						text: 'Opsss... Algo deu errado!',
						type: 'error'
					});
					$('#SendPostForm').removeAttr("disabled");
					setTimeout(function () {
						$('.load').hide();
						$('.progress-bar').css('width', '');
						$('.progress-bar').html('');
					}, 2000);
					return false;
				}
			});
		}
	});
	$("#PostFormFuncionario").validate({
		highlight: function (label) {
			$(label).closest('.form-group').removeClass('has-success').addClass('has-error');
		},
		success: function (label) {
			$(label).closest('.form-group').removeClass('has-error');
			label.remove();
		},
		errorPlacement: function (error, element) {
			var placement = element.closest('.input-group');
			if (!placement.get(0)) {
				placement = element;
			}
			if (error.text() !== '') {
				placement.after(error);
			}
		},
		rules: {
			name: {required: true, minlength: 2, maxlength: 60},
			email: {required: true, email: true},
			//telefone: {required: true},
			//dt_nascimento: {required: true},
			cpf3: {required: true},
			origem: {required: true},
			endereco: {required: true, minlength: 2, maxlength: 60},
			numero: {required: true, minlength: 2, maxlength: 10},
			cep: {required: true},
			bairro: {required: true, minlength: 2, maxlength: 60},
			idestado: {required: true},
			idcidade: {required: true}
		},
		messages: {
			name: {required: "Informe o nome!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 60 caracteres!"},
			email: {required: "Informe o e-mail!", email: "Informe um e-mail válido!"},
			//telefone: {required: "Informe o telefone!"},
			//dt_nascimento: {required: "Informe a data de nascimento!"},
			cpf: {required: "Informe o cpf!"},
			origem: {required: "Informe a origem!"},
			endereco: {required: "Informe o endereço!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 60 caracteres!"},
			numero: {required: "Informe o número!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 10 caracteres!"},
			cep: {required: "Informe o cep!"},
			bairro: {required: "Informe o bairro!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 60 caracteres!"},
			idestado: {required: "Selecione um estado!"},
			idcidade: {required: "Selecione uma cidade!"}
		},
		submitHandler: function (form) {
			$('#SendPostForm').attr("disabled", "disabled");
			$('.load').show();
			$('#PostFormFuncionario').ajaxSubmit({
				type: 'post',
				url: '../api/unidade/addFuncionario',
				uploadProgress: function (evento, posicao, total, porcentagem) {
					$('.progress-bar').css('width', porcentagem + '%');
					$('.progress-bar').html(porcentagem + '%');
				}, success: function (retorno) {
					console.log(retorno);
					new PNotify({
						title: retorno['title'],
						text: retorno['text'],
						type: retorno['type']
					});
					if (retorno['type'] === 'success') {
						setTimeout(function () {
							location.reload();
						}, 3000);
					} else {
						$('#SendPostForm').removeAttr("disabled");
						setTimeout(function () {
							$('.load').hide();
							$('.progress-bar').css('width', '');
							$('.progress-bar').html('');
						}, 2000);
						return false;
					}
				}, error: function () {
					new PNotify({
						title: 'Erro!',
						text: 'Opsss... Algo deu errado!',
						type: 'error'
					});
					$('#SendPostForm').removeAttr("disabled");
					setTimeout(function () {
						$('.load').hide();
						$('.progress-bar').css('width', '');
						$('.progress-bar').html('');
					}, 2000);
					return false;
				}
			});
		}
	});
	$("#PostFormFuncionarioUp").validate({
		highlight: function (label) {
			$(label).closest('.form-group').removeClass('has-success').addClass('has-error');
		},
		success: function (label) {
			$(label).closest('.form-group').removeClass('has-error');
			label.remove();
		},
		errorPlacement: function (error, element) {
			var placement = element.closest('.input-group');
			if (!placement.get(0)) {
				placement = element;
			}
			if (error.text() !== '') {
				placement.after(error);
			}
		},
		rules: {
			name: {required: true, minlength: 2, maxlength: 60},
			email: {required: true, email: true},
			//telefone: {required: true},
			//dt_nascimento: {required: true},
			cpf4: {required: true},
			origem: {required: true},
			endereco: {required: true, minlength: 2, maxlength: 60},
			numero: {required: true, minlength: 2, maxlength: 10},
			cep: {required: true},
			bairro: {required: true, minlength: 2, maxlength: 60},
			idestado: {required: true},
			idcidade: {required: true}
		},
		messages: {
			name: {required: "Informe o nome!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 60 caracteres!"},
			email: {required: "Informe o e-mail!", email: "Informe um e-mail válido!"},
			telefone: {required: "Informe o telefone!"},
			//dt_nascimento: {required: "Informe a data de nascimento!"},
			cpf: {required: "Informe o cpf!"},
			origem: {required: "Informe a origem!"},
			endereco: {required: "Informe o endereço!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 60 caracteres!"},
			numero: {required: "Informe o número!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 10 caracteres!"},
			cep: {required: "Informe o cep!"},
			bairro: {required: "Informe o bairro!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 60 caracteres!"},
			idestado: {required: "Selecione um estado!"},
			idcidade: {required: "Selecione uma cidade!"}
		},
		submitHandler: function (form) {
			$('#SendPostForm').attr("disabled", "disabled");
			$('.load').show();
			$('#PostFormFuncionarioUp').ajaxSubmit({
				type: 'post',
				url: '../api/unidade/upFuncionario',
				uploadProgress: function (evento, posicao, total, porcentagem) {
					$('.progress-bar').css('width', porcentagem + '%');
					$('.progress-bar').html(porcentagem + '%');
				}, success: function (retorno) {
					console.log(retorno);
					new PNotify({
						title: retorno['title'],
						text: retorno['text'],
						type: retorno['type']
					});
					if (retorno['type'] === 'success') {
						setTimeout(function () {
							location.reload();
						}, 3000);
					} else {
						$('#SendPostForm').removeAttr("disabled");
						setTimeout(function () {
							$('.load').hide();
							$('.progress-bar').css('width', '');
							$('.progress-bar').html('');
						}, 2000);
						return false;
					}
				}, error: function () {
					new PNotify({
						title: 'Erro!',
						text: 'Opsss... Algo deu errado!',
						type: 'error'
					});
					$('#SendPostForm').removeAttr("disabled");
					setTimeout(function () {
						$('.load').hide();
						$('.progress-bar').css('width', '');
						$('.progress-bar').html('');
					}, 2000);
					return false;
				}
			});
		}
	});
	$("#PostFormNutricionista").validate({
		highlight: function (label) {
			$(label).closest('.form-group').removeClass('has-success').addClass('has-error');
		},
		success: function (label) {
			$(label).closest('.form-group').removeClass('has-error');
			label.remove();
		},
		errorPlacement: function (error, element) {
			var placement = element.closest('.input-group');
			if (!placement.get(0)) {
				placement = element;
			}
			if (error.text() !== '') {
				placement.after(error);
			}
		},
		rules: {
			name: {required: true, minlength: 2, maxlength: 60},
			email: {required: true, email: true},
			//telefone: {required: true},
			//dt_nascimento: {required: true},
			cpf: {required: true},
			origem: {required: true},
			endereco: {required: true, minlength: 2, maxlength: 60},
			numero: {required: true, minlength: 2, maxlength: 10},
			cep: {required: true},
			bairro: {required: true, minlength: 2, maxlength: 60},
			idestado: {required: true},
			idcidade: {required: true}
		},
		messages: {
			name: {required: "Informe o nome!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 60 caracteres!"},
			email: {required: "Informe o e-mail!", email: "Informe um e-mail válido!"},
			//telefone: {required: "Informe o telefone!"},
			//dt_nascimento: {required: "Informe a data de nascimento!"},
			cpf: {required: "Informe o cpf!"},
			origem: {required: "Informe a origem!"},
			endereco: {required: "Informe o endereço!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 60 caracteres!"},
			numero: {required: "Informe o número!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 10 caracteres!"},
			cep: {required: "Informe o cep!"},
			bairro: {required: "Informe o bairro!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 60 caracteres!"},
			idestado: {required: "Selecione um estado!"},
			idcidade: {required: "Selecione uma cidade!"}
		},
		submitHandler: function (form) {
			$('#SendPostForm').attr("disabled", "disabled");
			$('.load').show();
			$('#PostFormNutricionista').ajaxSubmit({
				type: 'post',
				url: '../api/unidade/addNutricionista',
				uploadProgress: function (evento, posicao, total, porcentagem) {
					$('.progress-bar').css('width', porcentagem + '%');
					$('.progress-bar').html(porcentagem + '%');
				}, success: function (retorno) {
					console.log(retorno);
					new PNotify({
						title: retorno['title'],
						text: retorno['text'],
						type: retorno['type']
					});
					if (retorno['type'] === 'success') {
						setTimeout(function () {
							location.reload();
						}, 3000);
					} else {
						$('#SendPostForm').removeAttr("disabled");
						setTimeout(function () {
							$('.load').hide();
							$('.progress-bar').css('width', '');
							$('.progress-bar').html('');
						}, 2000);
						return false;
					}
				}, error: function () {
					new PNotify({
						title: 'Erro!',
						text: 'Opsss... Algo deu errado!',
						type: 'error'
					});
					$('#SendPostForm').removeAttr("disabled");
					setTimeout(function () {
						$('.load').hide();
						$('.progress-bar').css('width', '');
						$('.progress-bar').html('');
					}, 2000);
					return false;
				}
			});
		}
	});
	$("#PostFormNutricionistaUp").validate({
		highlight: function (label) {
			$(label).closest('.form-group').removeClass('has-success').addClass('has-error');
		},
		success: function (label) {
			$(label).closest('.form-group').removeClass('has-error');
			label.remove();
		},
		errorPlacement: function (error, element) {
			var placement = element.closest('.input-group');
			if (!placement.get(0)) {
				placement = element;
			}
			if (error.text() !== '') {
				placement.after(error);
			}
		},
		rules: {
			name: {required: true, minlength: 2, maxlength: 60},
			email: {required: true, email: true},
			//telefone: {required: true},
			//dt_nascimento: {required: true},
			cpf: {required: true},
			origem: {required: true},
			endereco: {required: true, minlength: 2, maxlength: 60},
			numero: {required: true, minlength: 2, maxlength: 10},
			cep: {required: true},
			bairro: {required: true, minlength: 2, maxlength: 60},
			idestado: {required: true},
			idcidade: {required: true}
		},
		messages: {
			name: {required: "Informe o nome!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 60 caracteres!"},
			email: {required: "Informe o e-mail!", email: "Informe um e-mail válido!"},
			//telefone: {required: "Informe o telefone!"},
			//dt_nascimento: {required: "Informe a data de nascimento!"},
			cpf: {required: "Informe o cpf!"},
			origem: {required: "Informe a origem!"},
			endereco: {required: "Informe o endereço!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 60 caracteres!"},
			numero: {required: "Informe o número!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 10 caracteres!"},
			cep: {required: "Informe o cep!"},
			bairro: {required: "Informe o bairro!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 60 caracteres!"},
			idestado: {required: "Selecione um estado!"},
			idcidade: {required: "Selecione uma cidade!"}
		},
		submitHandler: function (form) {
			$('#SendPostForm').attr("disabled", "disabled");
			$('.load').show();
			$('#PostFormNutricionistaUp').ajaxSubmit({
				type: 'post',
				url: '../api/unidade/upNutricionista',
				uploadProgress: function (evento, posicao, total, porcentagem) {
					$('.progress-bar').css('width', porcentagem + '%');
					$('.progress-bar').html(porcentagem + '%');
				}, success: function (retorno) {
					console.log(retorno);
					new PNotify({
						title: retorno['title'],
						text: retorno['text'],
						type: retorno['type']
					});
					if (retorno['type'] === 'success') {
						setTimeout(function () {
							location.reload();
						}, 3000);
					} else {
						$('#SendPostForm').removeAttr("disabled");
						setTimeout(function () {
							$('.load').hide();
							$('.progress-bar').css('width', '');
							$('.progress-bar').html('');
						}, 2000);
						return false;
					}
				}, error: function () {
					new PNotify({
						title: 'Erro!',
						text: 'Opsss... Algo deu errado!',
						type: 'error'
					});
					$('#SendPostForm').removeAttr("disabled");
					setTimeout(function () {
						$('.load').hide();
						$('.progress-bar').css('width', '');
						$('.progress-bar').html('');
					}, 2000);
					return false;
				}
			});
		}
	});
}).apply(this, [jQuery]);