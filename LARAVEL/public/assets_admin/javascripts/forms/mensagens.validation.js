$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

(function () {
    'use strict';
    $("#PostFormMensagem").validate({
        highlight: function (label) {
            $(label).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (label) {
            $(label).closest('.form-group').removeClass('has-error');
            label.remove();
        },
        errorPlacement: function (error, element) {
            var placement = element.closest('.input-group');
            if (!placement.get(0)) {
            	placement = element;
            }
            if (error.text() !== '') {
                placement.after(error);
            }
        },
        rules: {
            msg_title: {required: true, minlength: 2, maxlength: 100},
            msg_content: {required: true, minlength: 2, maxlength: 500},
            msg_inicio: {required: true},
            msg_fim: {required: true}
        },
        messages: {
            msg_title: {required: "Informe o título!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 100 caracteres!"},
            msg_content: {required: "Informe o conteúdo!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 500 caracteres!"},
            msg_inicio: {required: "Informe a data início!"},
            msg_fim: {required: "Informe a data fim!"}
        },
        submitHandler: function (form) {
            $('#SendPostForm').attr("disabled", "disabled");
            $('.load').show();
            $('#PostFormMensagem').ajaxSubmit({
                type: 'post',
                url: 'insere',
                uploadProgress: function (evento, posicao, total, porcentagem) {
                    $('.progress-bar').css('width', porcentagem + '%');
                    $('.progress-bar').html(porcentagem + '%');
                }, success: function (retorno) {
                    new PNotify({
                        title: retorno['title'],
                        text: retorno['text'],
                        type: retorno['type']
                    });
                    if (retorno['type'] === 'success') {
                        setTimeout(function () {
                            window.location = "gerenciar"
                        }, 3000);
                    } else {
                        $('#SendPostForm').removeAttr("disabled");
                        setTimeout(function () {
                            $('.load').hide();
                            $('.progress-bar').css('width', '');
                            $('.progress-bar').html('');
                        }, 2000);
                        return false;
                    }
                }, error: function () {
                    new PNotify({
                        title: 'Erro!',
                        text: 'Opsss... Algo deu errado!',
                        type: 'error'
                    });
                    $('#SendPostForm').removeAttr("disabled");
                    setTimeout(function () {
                        $('.load').hide();
                        $('.progress-bar').css('width', '');
                        $('.progress-bar').html('');
                    }, 2000);
                    return false;
                }
            });
        }
    });
    $("#PostFormMensagemUp").validate({
        highlight: function (label) {
            $(label).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (label) {
            $(label).closest('.form-group').removeClass('has-error');
            label.remove();
        },
        errorPlacement: function (error, element) {
            var placement = element.closest('.input-group');
            if (!placement.get(0)) {
                placement = element;
            }
            if (error.text() !== '') {
                placement.after(error);
            }
        },
        rules: {
            msg_title: {required: true, minlength: 2, maxlength: 100},
            msg_content: {required: true, minlength: 2, maxlength: 500},
            msg_inicio: {required: true},
            msg_fim: {required: true}
        },
        messages: {
            msg_title: {required: "Informe o título!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 100 caracteres!"},
            msg_content: {required: "Informe o conteúdo!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 500 caracteres!"},
            msg_inicio: {required: "Informe a data início!"},
            msg_fim: {required: "Informe a data fim!"}
        },
        submitHandler: function (form) {
            $('#SendPostForm').attr("disabled", "disabled");
            $('.load').show();
            $('#PostFormMensagemUp').ajaxSubmit({
                type: 'post',
                url: '../atualiza',
                uploadProgress: function (evento, posicao, total, porcentagem) {
                    $('.progress-bar').css('width', porcentagem + '%');
                    $('.progress-bar').html(porcentagem + '%');
                }, success: function (retorno) {
                    new PNotify({
                        title: retorno['title'],
                        text: retorno['text'],
                        type: retorno['type']
                    });
                    if (retorno['type'] === 'success') {
                        setTimeout(function () {
                            window.location = "../gerenciar"
                        }, 3000);
                    } else {
                        $('#SendPostForm').removeAttr("disabled");
                        setTimeout(function () {
                            $('.load').hide();
                            $('.progress-bar').css('width', '');
                            $('.progress-bar').html('');
                        }, 2000);
                        return false;
                    }
                }, error: function () {
                    new PNotify({
                        title: 'Erro!',
                        text: 'Opsss... Algo deu errado!',
                        type: 'error'
                    });
                    $('#SendPostForm').removeAttr("disabled");
                    setTimeout(function () {
                        $('.load').hide();
                        $('.progress-bar').css('width', '');
                        $('.progress-bar').html('');
                    }, 2000);
                    return false;
                }
            });
        }
    });
}).apply(this, [jQuery]);