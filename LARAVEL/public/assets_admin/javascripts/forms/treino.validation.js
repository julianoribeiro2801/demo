$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

(function () {
    'use strict';
    $("#PostFormTreino").validate({
        highlight: function (label) {
            $(label).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (label) {
            $(label).closest('.form-group').removeClass('has-error');
            label.remove();
        },
        errorPlacement: function (error, element) {
            var placement = element.closest('.input-group');
            if (!placement.get(0)) {
            	placement = element;
            }
            if (error.text() !== '') {
                placement.after(error);
            }
        },
        rules: {
            treino_title: {required: true, minlength: 2, maxlength: 100},
            treino_descricao: {required: true, minlength: 2, maxlength: 200}
        },
        messages: {
            treino_title: {required: "Informe o título!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 100 caracteres!"},
            treino_descricao: {required: "Informe a descrição!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 200 caracteres!"}
        },
        submitHandler: function (form) {
            $('#SendPostForm').attr("disabled", "disabled");
            $('.load').show();
	        var selected = $("#ms_example0 option:selected");
	        var message = "";
	        selected.each(function () {
	            message += $(this).val() + ",";
	        });
            $('#PostFormTreino').ajaxSubmit({
                type: 'post',
                url: 'insere',
                data: {
                	treino_dia: message
                },
                uploadProgress: function (evento, posicao, total, porcentagem) {
                    $('.progress-bar').css('width', porcentagem + '%');
                    $('.progress-bar').html(porcentagem + '%');
                }, success: function (retorno) {
                    new PNotify({
                        title: retorno['title'],
                        text: retorno['text'],
                        type: retorno['type']
                    });
                    if (retorno['type'] === 'success') {
                        setTimeout(function () {
                            window.location = "gerenciar"
                        }, 3000);
                    } else {
                        $('#SendPostForm').removeAttr("disabled");
                        setTimeout(function () {
                            $('.load').hide();
                            $('.progress-bar').css('width', '');
                            $('.progress-bar').html('');
                        }, 2000);
                        return false;
                    }
                }, error: function () {
                    new PNotify({
                        title: 'Erro!',
                        text: 'Opsss... Algo deu errado!',
                        type: 'error'
                    });
                    $('#SendPostForm').removeAttr("disabled");
                    setTimeout(function () {
                        $('.load').hide();
                        $('.progress-bar').css('width', '');
                        $('.progress-bar').html('');
                    }, 2000);
                    return false;
                }
            });
        }
    });
    $("#PostFormTreinoUp").validate({
        highlight: function (label) {
            $(label).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (label) {
            $(label).closest('.form-group').removeClass('has-error');
            label.remove();
        },
        errorPlacement: function (error, element) {
            var placement = element.closest('.input-group');
            if (!placement.get(0)) {
                placement = element;
            }
            if (error.text() !== '') {
                placement.after(error);
            }
        },
        rules: {
            treino_title: {required: true, minlength: 2, maxlength: 100},
            treino_descricao: {required: true, minlength: 2, maxlength: 200}
        },
        messages: {
            treino_title: {required: "Informe o título!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 100 caracteres!"},
            treino_descricao: {required: "Informe a descrição!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 200 caracteres!"}
        },
        submitHandler: function (form) {
            $('#SendPostForm').attr("disabled", "disabled");
            $('.load').show();
            var selected = $("#ms_example0 option:selected");
	        var message = "";
	        selected.each(function () {
	            message += $(this).val() + ",";
	        });
            $('#PostFormTreinoUp').ajaxSubmit({
                type: 'post',
                url: '../atualiza',
                data: {
                	treino_dia: message
                },
                uploadProgress: function (evento, posicao, total, porcentagem) {
                    $('.progress-bar').css('width', porcentagem + '%');
                    $('.progress-bar').html(porcentagem + '%');
                }, success: function (retorno) {
                    new PNotify({
                        title: retorno['title'],
                        text: retorno['text'],
                        type: retorno['type']
                    });
                    if (retorno['type'] === 'success') {
                        setTimeout(function () {
                            window.location = "../gerenciar"
                        }, 3000);
                    } else {
                        $('#SendPostForm').removeAttr("disabled");
                        setTimeout(function () {
                            $('.load').hide();
                            $('.progress-bar').css('width', '');
                            $('.progress-bar').html('');
                        }, 2000);
                        return false;
                    }
                }, error: function () {
                    new PNotify({
                        title: 'Erro!',
                        text: 'Opsss... Algo deu errado!',
                        type: 'error'
                    });
                    $('#SendPostForm').removeAttr("disabled");
                    setTimeout(function () {
                        $('.load').hide();
                        $('.progress-bar').css('width', '');
                        $('.progress-bar').html('');
                    }, 2000);
                    return false;
                }
            });
        }
    });
}).apply(this, [jQuery]);