$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

(function () {
    'use strict';
    $("#PostFormCor").validate({
        highlight: function (label) {
            $(label).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (label) {
            $(label).closest('.form-group').removeClass('has-error');
            label.remove();
        },
        errorPlacement: function (error, element) {
            var placement = element.closest('.input-group');
            if (!placement.get(0)) {
                placement = element;
            }
            if (error.text() !== '') {
                placement.after(error);
            }
        },
        rules: {
            cor_tipo: {required: true},
            cor_imagem: {required: true},
            cor_hex: {required: true, minlength: 4, maxlength: 7},
            cor_cod: {required: true, minlength: 2, maxlength: 20},
            cor_titulo: {required: true, minlength: 2, maxlength: 150},
            cor_category: {required: true},
            cor_catcor: {required: true},
            cor_ordem: {number: true, required: true, maxlength: 3}
        },
        messages: {
            cor_tipo: {required: "Selecione o tipo!"},
            cor_imagem: {required: "Selecione um imagem!"},
            cor_hex: {required: "Informe a cor!", minlength: "Mínimo 4 caracteres!", maxlength: "Máximo 7 caracteres!"},
            cor_cod: {required: "Informe o código!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 20 caracteres!"},
            cor_titulo: {required: 'Informe o título!', minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 150 caracteres!"},
            cor_category: {required: 'Selecione uma categoria!'},
            cor_catcor: {required: 'Selecione uma categoria cor!'},
            cor_ordem: {number: 'Informe apenas números', required: 'Inform a ordem da cor!', maxlength: "Máximo 3 caracteres!"}
        },
        submitHandler: function (form) {
            $('#SendPostForm').attr("disabled", "disabled");
            $('.load').show();
            $('#PostFormCor').ajaxSubmit({
                type: 'post',
                url: 'insere',
                uploadProgress: function (evento, posicao, total, porcentagem) {
                    $('.progress-bar').css('width', porcentagem + '%');
                    $('.progress-bar').html(porcentagem + '%');
                }, success: function (retorno) {
                    new PNotify({
                        title: retorno['title'],
                        text: retorno['text'],
                        type: retorno['type']
                    });
                    if (retorno['type'] === 'success') {
                        setTimeout(function () {
                            window.location = "gerenciar"
                        }, 3000);
                    } else {
                        $('#SendPostForm').removeAttr("disabled");
                        setTimeout(function () {
                            $('.load').hide();
                            $('.progress-bar').css('width', '');
                            $('.progress-bar').html('');
                        }, 2000);
                        return false;
                    }
                }, error: function () {
                    new PNotify({
                        title: 'Erro!',
                        text: 'Opsss... Algo deu errado!',
                        type: 'error'
                    });
                    $('#SendPostForm').removeAttr("disabled");
                    setTimeout(function () {
                        $('.load').hide();
                        $('.progress-bar').css('width', '');
                        $('.progress-bar').html('');
                    }, 2000);
                    return false;
                }
            });
        }
    });
    $("#PostFormCorUp").validate({
        highlight: function (label) {
            $(label).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (label) {
            $(label).closest('.form-group').removeClass('has-error');
            label.remove();
        },
        errorPlacement: function (error, element) {
            var placement = element.closest('.input-group');
            if (!placement.get(0)) {
                placement = element;
            }
            if (error.text() !== '') {
                placement.after(error);
            }
        },
        rules: {
            cor_tipo: {required: true},
            cor_hex: {required: true, minlength: 4, maxlength: 7},
            cor_cod: {required: true, minlength: 2, maxlength: 20},
            cor_titulo: {required: true, minlength: 2, maxlength: 150},
            cor_ordem: {number: true, required: true, maxlength: 3}
        },
        messages: {
            cor_tipo: {required: "Selecione o tipo!"},
            cor_hex: {required: "Informe a cor!", minlength: "Mínimo 4 caracteres!", maxlength: "Máximo 7 caracteres!"},
            cor_cod: {required: "Informe o código!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 20 caracteres!"},
            cor_titulo: {required: 'Informe o título!', minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 150 caracteres!"},
            cor_ordem: {number: 'Informe apenas números', required: 'Inform a ordem da cor!', maxlength: "Máximo 3 caracteres!"}
        },
        submitHandler: function (form) {
            $('#SendPostForm').attr("disabled", "disabled");
            $('.load').show();
            $('#PostFormCorUp').ajaxSubmit({
                type: 'post',
                url: '../atualiza',
                uploadProgress: function (evento, posicao, total, porcentagem) {
                    $('.progress-bar').css('width', porcentagem + '%');
                    $('.progress-bar').html(porcentagem + '%');
                }, success: function (retorno) {
                    new PNotify({
                        title: retorno['title'],
                        text: retorno['text'],
                        type: retorno['type']
                    });
                    if (retorno['type'] === 'success') {
                        setTimeout(function () {
                            window.location = "../gerenciar"
                        }, 3000);
                    } else {
                        $('#SendPostForm').removeAttr("disabled");
                        setTimeout(function () {
                            $('.load').hide();
                            $('.progress-bar').css('width', '');
                            $('.progress-bar').html('');
                        }, 2000);
                        return false;
                    }
                }, error: function () {
                    new PNotify({
                        title: 'Erro!',
                        text: 'Opsss... Algo deu errado!',
                        type: 'error'
                    });
                    $('#SendPostForm').removeAttr("disabled");
                    setTimeout(function () {
                        $('.load').hide();
                        $('.progress-bar').css('width', '');
                        $('.progress-bar').html('');
                    }, 2000);
                    return false;
                }
            });
        }
    });
}).apply(this, [jQuery]);