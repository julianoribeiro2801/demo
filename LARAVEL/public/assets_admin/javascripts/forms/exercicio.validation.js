$.ajaxSetup({
	headers: {
		'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')
	}
});

(function () {
	'use strict';
	$("#PostFormExercicio").validate({
		highlight: function (label) {
			$(label).closest('.form-group').removeClass('has-success').addClass('has-error');
		},
		success: function (label) {
			$(label).closest('.form-group').removeClass('has-error');
			label.remove();
		},
		errorPlacement: function (error, element) {
			var placement = element.closest('.input-group');
			if (!placement.get(0)) {
				placement = element;
			}
			if (error.text() !== '') {
				placement.after(error);
			}
		},
		rules: {
			idgrupomuscular: {required: true},
			nmexercicio: {required: true, minlength: 2, maxlength: 100}
		},
		messages: {
			idgrupomuscular: {required: "Selecione o grupo muscular!"},
			nmexercicio: {required: "Informe a descrição!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 100 caracteres!"}
		},
		submitHandler: function (form) {
			$('#SendPostForm').attr("disabled", "disabled");
			$('.load').show();
			$('#PostFormExercicio').ajaxSubmit({
				type: 'post',
				url: 'insere',
				uploadProgress: function (evento, posicao, total, porcentagem) {
					$('.progress-bar').css('width', porcentagem + '%');
					$('.progress-bar').html(porcentagem + '%');
				}, success: function (retorno) {
					new PNotify({
						title: retorno['title'],
						text: retorno['text'],
						type: retorno['type']
					});
					if (retorno['type'] === 'success') {
                                            
						setTimeout(function () {
							location.reload();
						}, 3000);
					} else {
						$('#SendPostForm').removeAttr("disabled");
						setTimeout(function () {
							$('.load').hide();
							$('.progress-bar').css('width', '');
							$('.progress-bar').html('');
						}, 2000);
						return false;
					}
				}, error: function () {
					new PNotify({
						title: 'Erro!',
						text: 'Opsss... Algo deu errado!',
						type: 'error'
					});
					$('#SendPostForm').removeAttr("disabled");
					setTimeout(function () {
						$('.load').hide();
						$('.progress-bar').css('width', '');
						$('.progress-bar').html('');
					}, 2000);
					return false;
				}
			});
		}
	});
	$("#PostFormExercicioUp").validate({
		highlight: function (label) {
			$(label).closest('.form-group').removeClass('has-success').addClass('has-error');
		},
		success: function (label) {
			$(label).closest('.form-group').removeClass('has-error');
			label.remove();
		},
		errorPlacement: function (error, element) {
			var placement = element.closest('.input-group');
			if (!placement.get(0)) {
				placement = element;
			}
			if (error.text() !== '') {
				placement.after(error);
			}
		},
		rules: {
            idgrupomuscular: {required: true},
            nmexercicio: {required: true, minlength: 2, maxlength: 100}
        },
        messages: {
            idgrupomuscular: {required: "Selecione o grupo muscular!"},
            nmexercicio: {required: "Informe a descrição!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 100 caracteres!"}
        },
		submitHandler: function (form) {
			$('#SendPostForm').attr("disabled", "disabled");
			$('.load').show();
			$('#PostFormExercicioUp').ajaxSubmit({
				type: 'post',
				url: 'atualiza',
				uploadProgress: function (evento, posicao, total, porcentagem) {
					$('.progress-bar').css('width', porcentagem + '%');
					$('.progress-bar').html(porcentagem + '%');
				}, success: function (retorno) {
					console.log(retorno);
					new PNotify({
						title: retorno['title'],
						text: retorno['text'],
						type: retorno['type']
					});
					if (retorno['type'] === 'success') {
						location.reload();
					} else {
						$('#SendPostForm').removeAttr("disabled");
						setTimeout(function () {
							$('.load').hide();
							$('.progress-bar').css('width', '');
							$('.progress-bar').html('');
						}, 2000);
						return false;
					}
				}, error: function (retorno) {
					console.log(retorno);
					new PNotify({
						title: 'Erro!',
						text: 'Opsss... Algo deu errado!',
						type: 'error'
					});
					$('#SendPostForm').removeAttr("disabled");
					setTimeout(function () {
						$('.load').hide();
						$('.progress-bar').css('width', '');
						$('.progress-bar').html('');
					}, 2000);
					return false;
				}
			});
		}
	});
}).apply(this, [jQuery]);