$.ajaxSetup({
	headers: {
		'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')
	}
});

(function () {
	'use strict';
        
        
	$("#PostFormObjetivo").validate({
		highlight: function (label) {
			   $(label).closest('.form-group').removeClass('has-success').addClass('has-error');
		},
		success: function (label) {
			$(label).closest('.form-group').removeClass('has-error');
			label.remove();
		},
		errorPlacement: function (error, element) {
			var placement = element.closest('.input-group');
			if (!placement.get(0)) {
				placement = element;
			}
			if (error.text() !== '') {
				placement.after(error);
			}
		},
		rules: {
                    idmodalidade: {required: true, number: true, min: 1, max: 1000000},
                    nmobjetivo: {required: true, minlength: 2, maxlength: 100}
                    
		},
		messages: {
                    idmodalidade: {required: "Selecione a modalidade!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 100 caracteres!"},
                    nmobjetivo: {required: "Informe a descrição!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 100 caracteres!"}
                    
		},
		submitHandler: function (form) {
			$('#SendPostForm').attr("disabled", "disabled");
			$('.load').show();
			$('#PostFormObjetivo').ajaxSubmit({
				type: 'post',
				url: '../prescricao/addObjetivo',
				uploadProgress: function (evento, posicao, total, porcentagem) {
					$('.progress-bar').css('width', porcentagem + '%');
					$('.progress-bar').html(porcentagem + '%');
				}, success: function (retorno) {
					new PNotify({
						title: retorno['title'],
						text: retorno['text'],
						type: retorno['type']
					});
					if (retorno['type'] === 'success') {
						setTimeout(function () {
							location.reload();
						}, 3000);
					} else {
						$('#SendPostForm').removeAttr("disabled");
						setTimeout(function () {
							$('.load').hide();
							$('.progress-bar').css('width', '');
							$('.progress-bar').html('');
						}, 2000);
						return false;
					}
				}, error: function () {
					new PNotify({
						title: 'Erro!',
						text: 'Opsss... Algo deu errado!',
						type: 'error'
					});
					$('#SendPostForm').removeAttr("disabled");
					setTimeout(function () {
						$('.load').hide();
						$('.progress-bar').css('width', '');
						$('.progress-bar').html('');
					}, 2000);
					return false;
				}
			});
		}
	});
	$("#PostFormObjetivoUp").validate({
		highlight: function (label) {
			$(label).closest('.form-group').removeClass('has-success').addClass('has-error');
		},
		success: function (label) {
			$(label).closest('.form-group').removeClass('has-error');
			label.remove();
		},
		errorPlacement: function (error, element) {
			var placement = element.closest('.input-group');
			if (!placement.get(0)) {
				placement = element;
			}
			if (error.text() !== '') {
				placement.after(error);
			}
		},
		rules: {
                    idmodalidade: {required: true, number: true, min: 1, max: 1000000},
                    nmobjetivo: {required: true, minlength: 2, maxlength: 100}
                    
                    
		},
		messages: {
                    idmodalidade: {required: "Informe a descrição!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 100 caracteres!"},
                    nmobjetivo: {required: "Informe a descrição!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 100 caracteres!"}
                    
		},
		submitHandler: function (form) {
			$('#SendPostForm').attr("disabled", "disabled");
			$('.load').show();
			$('#PostFormObjetivoUp').ajaxSubmit({
				type: 'post',
				url: '../prescricao/upObjetivo',
				uploadProgress: function (evento, posicao, total, porcentagem) {
					$('.progress-bar').css('width', porcentagem + '%');
					$('.progress-bar').html(porcentagem + '%');
				}, success: function (retorno) {
					console.log(retorno);
					new PNotify({
						title: retorno['title'],
						text: retorno['text'],
						type: retorno['type']
					});
					if (retorno['type'] === 'success') {
						location.reload();
					} else {
						$('#SendPostForm').removeAttr("disabled");
						setTimeout(function () {
							$('.load').hide();
							$('.progress-bar').css('width', '');
							$('.progress-bar').html('');
						}, 2000);
						return false;
					}
				}, error: function (retorno) {
					console.log(retorno);
					new PNotify({
						title: 'Erro!',
						text: 'Opsss... Algo deu errado!',
						type: 'error'
					});
					$('#SendPostForm').removeAttr("disabled");
					setTimeout(function () {
						$('.load').hide();
						$('.progress-bar').css('width', '');
						$('.progress-bar').html('');
					}, 2000);
					return false;
				}
			});
		}
	});
}).apply(this, [jQuery]);