$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

(function () {
    'use strict';
    $("#PostFormCatcor").validate({
        highlight: function (label) {
            $(label).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (label) {
            $(label).closest('.form-group').removeClass('has-error');
            label.remove();
        },
        errorPlacement: function (error, element) {
            var placement = element.closest('.input-group');
            if (!placement.get(0)) {
                placement = element;
            }
            if (error.text() !== '') {
                placement.after(error);
            }
        },
        rules: {
            catcores_title: {required: true, minlength: 2, maxlength: 45}
        },
        messages: {
            catcores_title: {required: "Informe o título!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 45 caracteres!"}
        },
        submitHandler: function (form) {
            $('#SendPostForm').attr("disabled", "disabled");
            $('.load').show();
            $('#PostFormCatcor').ajaxSubmit({
                type: 'post',
                url: 'insere',
                uploadProgress: function (evento, posicao, total, porcentagem) {
                    $('.progress-bar').css('width', porcentagem + '%');
                    $('.progress-bar').html(porcentagem + '%');
                }, success: function (retorno) {
                    new PNotify({
                        title: retorno['title'],
                        text: retorno['text'],
                        type: retorno['type']
                    });
                    if (retorno['type'] === 'success') {
                        setTimeout(function () {
                            window.location = "gerenciar"
                        }, 3000);
                    } else {
                        $('#SendPostForm').removeAttr("disabled");
                        setTimeout(function () {
                            $('.load').hide();
                            $('.progress-bar').css('width', '');
                            $('.progress-bar').html('');
                        }, 2000);
                        return false;
                    }
                }, error: function () {
                    new PNotify({
                        title: 'Erro!',
                        text: 'Opsss... Algo deu errado!',
                        type: 'error'
                    });
                    $('#SendPostForm').removeAttr("disabled");
                    setTimeout(function () {
                        $('.load').hide();
                        $('.progress-bar').css('width', '');
                        $('.progress-bar').html('');
                    }, 2000);
                    return false;
                }
            });
        }
    });
    $("#PostFormCatcorUp").validate({
        highlight: function (label) {
            $(label).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (label) {
            $(label).closest('.form-group').removeClass('has-error');
            label.remove();
        },
        errorPlacement: function (error, element) {
            var placement = element.closest('.input-group');
            if (!placement.get(0)) {
                placement = element;
            }
            if (error.text() !== '') {
                placement.after(error);
            }
        },
        rules: {
            catcores_title: {required: true, minlength: 2, maxlength: 45}
        },
        messages: {
            catcores_title: {required: "Informe o título!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 45 caracteres!"}
        },
        submitHandler: function (form) {
            $('#SendPostForm').attr("disabled", "disabled");
            $('.load').show();
            $('#PostFormCatcorUp').ajaxSubmit({
                type: 'post',
                url: '../atualiza',
                uploadProgress: function (evento, posicao, total, porcentagem) {
                    $('.progress-bar').css('width', porcentagem + '%');
                    $('.progress-bar').html(porcentagem + '%');
                }, success: function (retorno) {
                    new PNotify({
                        title: retorno['title'],
                        text: retorno['text'],
                        type: retorno['type']
                    });
                    if (retorno['type'] === 'success') {
                        setTimeout(function () {
                            window.location = "../gerenciar"
                        }, 3000);
                    } else {
                        $('#SendPostForm').removeAttr("disabled");
                        setTimeout(function () {
                            $('.load').hide();
                            $('.progress-bar').css('width', '');
                            $('.progress-bar').html('');
                        }, 2000);
                        return false;
                    }
                }, error: function () {
                    new PNotify({
                        title: 'Erro!',
                        text: 'Opsss... Algo deu errado!',
                        type: 'error'
                    });
                    $('#SendPostForm').removeAttr("disabled");
                    setTimeout(function () {
                        $('.load').hide();
                        $('.progress-bar').css('width', '');
                        $('.progress-bar').html('');
                    }, 2000);
                    return false;
                }
            });
        }
    });
}).apply(this, [jQuery]);