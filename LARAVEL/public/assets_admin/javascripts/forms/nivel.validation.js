$.ajaxSetup({
	headers: {
		'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')
	}
});

(function () {
	'use strict';
        
        
	$("#PostFormNivel").validate({
		highlight: function (label) {
			   $(label).closest('.form-group').removeClass('has-success').addClass('has-error');
		},
		success: function (label) {
			$(label).closest('.form-group').removeClass('has-error');
			label.remove();
		},
		errorPlacement: function (error, element) {
			var placement = element.closest('.input-group');
			if (!placement.get(0)) {
				placement = element;
			}
			if (error.text() !== '') {
				placement.after(error);
			}
		},
		rules: {
                    idmodalidade: {required: true, number: true, min: 1, max: 1000000},
                    idobjetivo: {required: true, number: true, min: 1, max: 1000000},
                    nmnivel: {required: true, minlength: 2, maxlength: 100},
                    dscriterio: {required: true, minlength: 0, maxlength: 500}
                    
		},
		messages: {
                    idmodalidade: {required: "Selecione a modalidade!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 100 caracteres!"},
                    idobjetivo: {required: "Selecione o objetivo!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 100 caracteres!"},
                    nmnivel: {required: "Informe a descrição!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 100 caracteres!"},
                    dscriterio: {required: "Máximo 500 caracteres!", minlength: "Mínimo 0 caracteres!", maxlength: "Máximo 500 caracteres!"}
                    
		},
		submitHandler: function (form) {
			$('#SendPostForm').attr("disabled", "disabled");
			$('.load').show();
			$('#PostFormNivel').ajaxSubmit({
				type: 'post',
				url: '../prescricao/addNivel',
				uploadProgress: function (evento, posicao, total, porcentagem) {
					$('.progress-bar').css('width', porcentagem + '%');
					$('.progress-bar').html(porcentagem + '%');
				}, success: function (retorno) {
					new PNotify({
						title: retorno['title'],
						text: retorno['text'],
						type: retorno['type']
					});
					if (retorno['type'] === 'success') {
						setTimeout(function () {
							//location.reload();
						}, 3000);
					} else {
						$('#SendPostForm').removeAttr("disabled");
						setTimeout(function () {
							$('.load').hide();
							$('.progress-bar').css('width', '');
							$('.progress-bar').html('');
						}, 2000);
						return false;
					}
				}, error: function () {
					new PNotify({
						title: 'Erro!',
						text: 'Opsss... Algo deu errado!',
						type: 'error'
					});
					$('#SendPostForm').removeAttr("disabled");
					setTimeout(function () {
						$('.load').hide();
						$('.progress-bar').css('width', '');
						$('.progress-bar').html('');
					}, 2000);
					return false;
				}
			});
		}
	});
	$("#PostFormNivelUp").validate({
		highlight: function (label) {
			$(label).closest('.form-group').removeClass('has-success').addClass('has-error');
		},
		success: function (label) {
			$(label).closest('.form-group').removeClass('has-error');
			label.remove();
		},
		errorPlacement: function (error, element) {
			var placement = element.closest('.input-group');
			if (!placement.get(0)) {
				placement = element;
			}
			if (error.text() !== '') {
				placement.after(error);
			}
		},
		rules: {
                    idmodalidade: {required: true, number: true, min: 1, max: 1000000},
                    nmnivel: {required: true, minlength: 2, maxlength: 100},
                    dscriterio: {required: true, minlength: 0, maxlength: 500}
                    
		},
		messages: {
                    nmnivel: {required: "Informe a descrição!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 100 caracteres!"},
                    dscriterio: {required: "Máximo 500 caracteres!", minlength: "Mínimo 0 caracteres!", maxlength: "Máximo 500 caracteres!"}
                    
		},
		submitHandler: function (form) {
			$('#SendPostForm').attr("disabled", "disabled");
			$('.load').show();
			$('#PostFormNivelUp').ajaxSubmit({
				type: 'post',
				url: '../prescricao/upNivel',
				uploadProgress: function (evento, posicao, total, porcentagem) {
					$('.progress-bar').css('width', porcentagem + '%');
					$('.progress-bar').html(porcentagem + '%');
				}, success: function (retorno) {
					console.log(retorno);
					new PNotify({
						title: retorno['title'],
						text: retorno['text'],
						type: retorno['type']
					});
					if (retorno['type'] === 'success') {
						location.reload();
					} else {
						$('#SendPostForm').removeAttr("disabled");
						setTimeout(function () {
							$('.load').hide();
							$('.progress-bar').css('width', '');
							$('.progress-bar').html('');
						}, 2000);
						return false;
					}
				}, error: function (retorno) {
					console.log(retorno);
					new PNotify({
						title: 'Erro!',
						text: 'Opsss... Algo deu errado!',
						type: 'error'
					});
					$('#SendPostForm').removeAttr("disabled");
					setTimeout(function () {
						$('.load').hide();
						$('.progress-bar').css('width', '');
						$('.progress-bar').html('');
					}, 2000);
					return false;
				}
			});
		}
	});
}).apply(this, [jQuery]);