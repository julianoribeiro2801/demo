$.ajaxSetup({
	headers: {
		'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')
	}
});

(function () {
	'use strict';
	$("#PostFormCvempresa").validate({
		highlight: function (label) {
			$(label).closest('.form-group').removeClass('has-success').addClass('has-error');
		},
		success: function (label) {
			$(label).closest('.form-group').removeClass('has-error');
			label.remove();
		},
		errorPlacement: function (error, element) {
			var placement = element.closest('.input-group');
			if (!placement.get(0)) {
				placement = element;
			}
			if (error.text() !== '') {
				placement.after(error);
			}
		},
		rules: {
			nmempresa: {required: true, minlength: 2, maxlength: 60},
			cnpj: {required: true},
			idsegmento: {required: true},
			percdesconto: {number: true, minlength: 1, maxlength: 2}
		},
		messages: {
			nmempresa: {required: "Informe o nome da empresa!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 60 caracteres!"},
			cnpj: {required: "Informe o cnpj!"},
			idsegmento: {required: "Selecione o segmento!"},
			percdesconto: {number: "Informe apenas números!", minlength: "Mínimo 1 caracteres!", maxlength: "Máximo 2 caracteres!"}
		},
		submitHandler: function (form) {
			$('#SendPostForm').attr("disabled", "disabled");
			$('.load').show();
			$('#PostFormCvempresa').ajaxSubmit({
				type: 'post',
				url: './api/cvempresa/addEmpresa',
				uploadProgress: function (evento, posicao, total, porcentagem) {
					$('.progress-bar').css('width', porcentagem + '%');
					$('.progress-bar').html(porcentagem + '%');
				}, success: function (retorno) {
					console.log(retorno);
					new PNotify({
						title: retorno['title'],
						text: retorno['text'],
						type: retorno['type']
					});
					if (retorno['type'] === 'success') {
						setTimeout(function () {
							location.reload();
						}, 3000);
					} else {
						$('#SendPostForm').removeAttr("disabled");
						setTimeout(function () {
							$('.load').hide();
							$('.progress-bar').css('width', '');
							$('.progress-bar').html('');
						}, 2000);
						return false;
					}
				}, error: function () {
					new PNotify({
						title: 'Erro!',
						text: 'Opsss... Algo deu errado!',
						type: 'error'
					});
					$('#SendPostForm').removeAttr("disabled");
					setTimeout(function () {
						$('.load').hide();
						$('.progress-bar').css('width', '');
						$('.progress-bar').html('');
					}, 2000);
					return false;
				}
			});
		}
	});
	$("#PostFormCvempresaUp").validate({
		highlight: function (label) {
			$(label).closest('.form-group').removeClass('has-success').addClass('has-error');
		},
		success: function (label) {
			$(label).closest('.form-group').removeClass('has-error');
			label.remove();
		},
		errorPlacement: function (error, element) {
			var placement = element.closest('.input-group');
			if (!placement.get(0)) {
				placement = element;
			}
			if (error.text() !== '') {
				placement.after(error);
			}
		},
		rules: {
			nmempresa: {required: true, minlength: 2, maxlength: 60},
			cnpj1: {required: true},
			idsegmento: {required: true},                        
			percdesconto: {number: true, minlength: 1, maxlength: 2}
		},
		messages: {
			nmempresa: {required: "Informe o nome da empresa!", minlength: "Mínimo 2 caracteres!", maxlength: "Máximo 60 caracteres!"},
			cnpj1: {required: "Informe o cnpj!"},
			idsegmento: {required: "Selecione o segmento!"},
			percdesconto: {number: "Informe apenas números!", minlength: "Mínimo 1 caracteres!", maxlength: "Máximo 2 caracteres!"}
		},
		submitHandler: function (form) {
			$('#SendPostForm').attr("disabled", "disabled");
			$('.load').show();
			$('#PostFormCvempresaUp').ajaxSubmit({
				type: 'post',
				url: './api/cvempresa/upEmpresa',
				uploadProgress: function (evento, posicao, total, porcentagem) {
					$('.progress-bar').css('width', porcentagem + '%');
					$('.progress-bar').html(porcentagem + '%');
				}, success: function (retorno) {
					console.log(retorno);
					new PNotify({
						title: retorno['title'],
						text: retorno['text'],
						type: retorno['type']
					});
					if (retorno['type'] === 'success') {
						setTimeout(function () {
							location.reload();
						}, 3000);
					} else {
						$('#SendPostForm').removeAttr("disabled");
						setTimeout(function () {
							$('.load').hide();
							$('.progress-bar').css('width', '');
							$('.progress-bar').html('');
						}, 2000);
						return false;
					}
				}, error: function () {
					new PNotify({
						title: 'Erro!',
						text: 'Opsss... Algo deu errado!',
						type: 'error'
					});
					$('#SendPostForm').removeAttr("disabled");
					setTimeout(function () {
						$('.load').hide();
						$('.progress-bar').css('width', '');
						$('.progress-bar').html('');
					}, 2000);
					return false;
				}
			});
		}
	});
}).apply(this, [jQuery]);