$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

(function ($) {
    'use strict';
    var datatableInit = function () {
        var $table = $('#PostFormUsuario');
        $table.dataTable({
            "bPaginate": true,
            "aaSorting": [[0, "desc"]],
            "oLanguage": {
                "sProcessing": "Processando...",
                "sLengthMenu": "Mostrar _MENU_ registros",
                "sZeroRecords": "Não foram encontrados resultados",
                "sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando de 0 até 0 de 0 registros",
                "sInfoFiltered": "(filtrado de _MAX_ registros no total)",
                "sInfoPostFix": "",
                "sSearch": "Pesquisa:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Primeiro",
                    "sPrevious": "Anterior",
                    "sNext": "Seguinte",
                    "sLast": "Último"
                }
            },
            bProcessing: true,
            processing: true,
            serverSide: true,
            "ajax": {
                "url": "listagem",
                "type": "POST"
            }
        });
        $(".dataTables_filter label").first().focus();
    };

    $(function () {
        datatableInit();
    });

}).apply(this, [jQuery]);