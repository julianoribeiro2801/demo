<?php
 $return_arr = array();

 if (isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
    }
   // Access-Control headers are received during OPTIONS requests
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
 
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         
 
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
 
        exit(0);
    }
 
 
    $postdata = file_get_contents("php://input");
    
    
  try {  
   if (isset($postdata)) {
      $request = json_decode($postdata);

      $idaluno = $request->idaluno;
      $unidade = $request->idunidade;
       
      $totConquista=0;
      $posicao=0;
      
      
               
      include "conecta.php";        

      $pdo = new PDO("mysql:host={$dbhost};dbname={$dbname}", $dbuser, $dbpass);
      $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);  
     // $pdo->setAttribute(PDO::ATTR_AUTOCOMMIT,false);  
     // $pdo->beginTransaction();
      
      
      
      
      $sql="select sum(vlrdesconto) as total from cv_alunocompra where ";
      $sql .= " idunidade = " . $unidade . " and idaluno = " . $idaluno;
      $sql .= " and extract( year from dtcompra) = year (current_date())";
      $sql .= " and extract( month from dtcompra) = month (current_date())";
      
      $con = new PDO("mysql:host={$dbhost};dbname={$dbname}", $dbuser, $dbpass);
      $con->query('SET NAMES utf8');
      $consulta = $con->query($sql);

      while($c = $consulta->fetch(PDO::FETCH_OBJ)) {  
           $row_array['IDALUNO'] =$idaluno;
           $row_array['TOTAL'] = number_format($c->total,2);
           array_push($return_arr,$row_array);
      }
      

    printf( json_encode($return_arr));      


     // printf('Registro efetuado com sucesso');
 }

} catch (PDOException $exception) {
    //$pdo->rollBack();
    printf('Não foi possível realizar a operação: %s' , $exception);
    
 }
 
?>