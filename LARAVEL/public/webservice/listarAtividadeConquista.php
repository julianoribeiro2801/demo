<?php
 $return_arr = array();

 if (isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
    }
   // Access-Control headers are received during OPTIONS requests
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
 
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         
 
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
 
        exit(0);
    }
 
 
    $postdata = file_get_contents("php://input");
    
    
  try {  
   if (isset($postdata)) {
      $request = json_decode($postdata);

      $aluno = $request->idaluno;
      $unidade = $request->idunidade;   
               
      include "conecta.php";        

      $pdo = new PDO("mysql:host={$dbhost};dbname={$dbname}", $dbuser, $dbpass);
      $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);  
     // $pdo->setAttribute(PDO::ATTR_AUTOCOMMIT,false);  
     // $pdo->beginTransaction();
      
//      $sql="select idatividade, nmatividade from atividade left join  where ";
//      $sql .= " stamenuapp = 'S' order by idatividade";
      
      
      
      
      $sql="select a.id as idatividade, a.nmatividade, c.id as idconquista, c.nmconquista,";
      $sql .= " coalesce(cusu.stparticipa, 'N') as stparticipa , coalesce(rk.nrposicao,0) as nrposicao ";
      $sql .= " from atividade as a, conquista as c left join";
      $sql .= " conquistausuario as cusu";
      $sql .= " on cusu.idconquista = c.id" ;
      $sql .= " and cusu.idunidade = " . $unidade;
      $sql .= " and cusu.idaluno = " . $aluno;
      $sql .= " left join rankingconquista as rk";
      $sql .= " on rk.idaluno = cusu.idaluno and";
      $sql .= " rk.idunidade = cusu.idunidade and";
      $sql .= " rk.idconquista = cusu.idconquista";
      $sql .= " where"; 
      $sql .= " c.idatividade = a.id";
      $sql .= " and a.stmenuapp = 'S' order by a.id";
     
      $con = new PDO("mysql:host={$dbhost};dbname={$dbname}", $dbuser, $dbpass);
      $con->query('SET NAMES utf8');
      $consulta = $con->query($sql);

      while($c = $consulta->fetch(PDO::FETCH_OBJ)) {  
           $row_array['IDALUNO'] = $aluno;
           $row_array['IDUNIDADE'] = $unidade;
           $row_array["IDCONQUISTA"]= $c->idconquista;
           $row_array["IDATIVIDADE"]= $c->idatividade;
           $row_array["NMATIVIDADE"]= $c->nmatividade;
           $row_array["NMCONQUISTA"]= $c->nmconquista;
           $row_array["STPARTICIPA"]= $c->stparticipa;
           $row_array["NRPOSICAO"]= $c->nrposicao;
           array_push($return_arr,$row_array);
      }
      

    printf( json_encode($return_arr));      


     // printf('Registro efetuado com sucesso');
 }

} catch (PDOException $exception) {
    //$pdo->rollBack();
    printf('Não foi possível realizar a operação: %s' , $exception);
    
 }
 
?>