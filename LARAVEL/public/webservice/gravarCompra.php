<?php
  $return_arr = array();
 if (isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
    }
   // Access-Control headers are received during OPTIONS requests
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
 
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         
 
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
 
        exit(0);
    }
 
 
  $postdata = file_get_contents("php://input");
    
    
  try {  
   if (isset($postdata)) {
      $request = json_decode($postdata);

      $aluno = $request->idaluno;
      $unidade = $request->idunidade;
//      $conquista = $request->idconquista;
      $empresa = $request->idempresa;
      $compra = $request->vlrcompra;
      $compra =str_replace('R$ ','',$compra);
      $compra =str_replace('.','',$compra); //retira o ponto
      $compra =str_replace(',','.',$compra); // no lugar da virgula colaco o ponto


      $perdesconto=0;
      $desconto=0;

      
               
      include "conecta.php";        

      $pdo = new PDO("mysql:host={$dbhost};dbname={$dbname}", $dbuser, $dbpass);
      $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);  
     // $pdo->setAttribute(PDO::ATTR_AUTOCOMMIT,false);  
     // $pdo->beginTransaction();
      
      
      $sql="select percdesconto from cv_empresa where ";
      $sql .= " idunidade = " . $unidade ;
      
      $con = new PDO("mysql:host={$dbhost};dbname={$dbname}", $dbuser, $dbpass);
      $con->query('SET NAMES utf8');
      $consulta = $con->query($sql);

      while($c = $consulta->fetch(PDO::FETCH_OBJ)) {  
         $percdesconto=$c->percdesconto; 
      }      
      $desconto = ($compra * $percdesconto) / 100;

//////////////////////////////////////////////////////////////////////////////
      

      $stmt = $pdo->prepare("insert into cv_alunocompra (idaluno, idunidade, idempresa, dtcompra,vlrcompra,percdesconto, vlrdesconto) "
              . "values (:aluno, :unidade,:empresa ,CURRENT_DATE,:compra,:percdesconto, :desconto) ");
      $stmt->execute(array(
        ':aluno' => $aluno,
        ':unidade' => $unidade,
        ':empresa' => $empresa,
        ':compra' => $compra,
        ':percdesconto' => $percdesconto,
        ':desconto' => $desconto,
      )); 
       
     $row_array['MENSAGEM'] = 'Registro efetuado com sucesso';
     array_push($return_arr,$row_array);

     printf( json_encode($return_arr));             

 }

} catch (PDOException $exception) {
    //$pdo->rollBack();
    printf('Não foi possível realizar a operação: %s' , $exception);
    
 }
 
?>