<?php
  $return_arr = array();
 if (isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
    }
   // Access-Control headers are received during OPTIONS requests
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
 
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         
 
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
 
        exit(0);
    }
 
 
  $postdata = file_get_contents("php://input");
    
    
  try {  
   if (isset($postdata)) {
      $request = json_decode($postdata);

      $aluno = $request->idaluno;
      $unidade = $request->idunidade;
      $conquista = $request->idconquista;
       
      $totConquista=0;
      $posicao=0;
      
      include "conecta.php";        

      $pdo = new PDO("mysql:host={$dbhost};dbname={$dbname}", $dbuser, $dbpass);
      $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);  

      $posNova=1;
      $sql="select rk.idaluno from rankingconquista rk, conquistausuario cqu where";
      $sql .= " rk.idunidade = " . $unidade;
      $sql .= " and rk.idconquista = " . $conquista;
      $sql .= " and rk.idunidade = cqu.idunidade";
      $sql .= " and rk.idaluno = cqu.idaluno";
      $sql .= " and rk.idconquista = cqu.idconquista";
      $sql .= " and cqu.stparticipa = 'S'";
      $sql .= " order by rk.qtconquista desc";

      $con = new PDO("mysql:host={$dbhost};dbname={$dbname}", $dbuser, $dbpass);
      $con->query('SET NAMES utf8');
      $consulta = $con->query($sql);

      while($c = $consulta->fetch(PDO::FETCH_OBJ)) {      
         $aluno=$c->idaluno;
         $stmt = $pdo->prepare("UPDATE rankingconquista set nrposicao = :posicao where idaluno = :aluno and idunidade = :unidade and idconquista = :conquista ");
         $stmt->execute(array(
          ':aluno' => $aluno,
          ':unidade' => $unidade,
          ':conquista' => $conquista,
          ':posicao' => $posNova
      
         )); 
         $posNova = $posNova + 1;   
      }
      $row_array["resposta"]= "ATUALIZAÇÃO EFETUADA COM SUCESSO";
      array_push($return_arr,$row_array);
      
      

      printf( json_encode($return_arr));      

      //$pdo->commit();
     // printf('Registro efetuado com sucesso');
 }

} catch (PDOException $exception) {
    //$pdo->rollBack();
    printf('Não foi possível realizar a operação: %s' , $exception);
    
 }
 
?>