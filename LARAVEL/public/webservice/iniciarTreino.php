<?php
  $return_arr = array();
 if (isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
    }
   // Access-Control headers are received during OPTIONS requests
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
 
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         
 
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
 
        exit(0);
    }
 
 
  $postdata = file_get_contents("php://input");
    
    
  try {  
   if (isset($postdata)) {
       
       
      date_default_timezone_set('America/Sao_Paulo');       
       
      $request = json_decode($postdata);

      $aluno = $request->idaluno;
      $unidade = $request->idunidade;
      $prescricao = $request->idtreino;
      $fichatreino = $request->fichatreino;
      $nrdiatreino=0;
      if($fichatreino=="A"){
         $nrdiatreino=1;
      }
      if($fichatreino=="B"){
         $nrdiatreino=2;
      }
      if($fichatreino=="C"){
         $nrdiatreino=3;
      }
      if($fichatreino=="D"){
         $nrdiatreino=4;
      }
      if($fichatreino=="E"){
         $nrdiatreino=5;
      }
      if($fichatreino=="F"){
        $nrdiatreino=6;
      }
      if($fichatreino=="G"){
        $nrdiatreino=7;
      }


     
      $i=0;
      
      include "conecta.php";        

      $pdo = new PDO("mysql:host={$dbhost};dbname={$dbname}", $dbuser, $dbpass);
      $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);  
      $pdo1 = new PDO("mysql:host={$dbhost};dbname={$dbname}", $dbuser, $dbpass);
      $pdo1->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);  
      $pdo2 = new PDO("mysql:host={$dbhost};dbname={$dbname}", $dbuser, $dbpass);
      $pdo2->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);  
     // $pdo->setAttribute(PDO::ATTR_AUTOCOMMIT,false);  
     // $pdo->beginTransaction();
      ////
      
     $pdo->beginTransaction();  
     
     $exercicio=0;
     $qtserie=0;
     $temEvolucao=0; 
     $tpEdicao="";
     $idInserido=0;
     $cargaAnt=0;
         
     $inicio=date('Y-m-d H:i:s');
     $sql = "INSERT INTO historicotreino (IDALUNO, IDUNIDADE, IDTREINO,NRDIATREINO,
             TPATIVIDADE, DTINICIO, 
             NRDIASUGESTAO,STTREINO, 
             DTREGISTRO)";
     $sql .= " values (:aluno,:unidade,:prescricao,:nrdiatreino,'M',:inicio,:nrdiasugestao,:sttreino,CURRENT_TIMESTAMP)";

     $stmt = $pdo->prepare($sql);           
                   
     $stmt->execute(array(
        ':aluno' => $aluno,
        ':unidade' => $unidade,
        ':prescricao' => $prescricao,
        ':nrdiatreino' => $nrdiatreino,
        ':nrdiasugestao' => $nrdiatreino,
        ':inicio' => $inicio,
        ':sttreino' => "A",
     )); 
      $dia_hoje = date('Y-m-d') ; //////////////////////ate aqui grava historico treino
      
     $sql="select l.exercicio_id, l.ficha_letra, l.ficha_id, l.ficha_series, l.ficha_repeticoes from treino_ficha l, "
             . " treino_musculacao p where p.unidade_id = " . $unidade
             . " and p.aluno_id = " . $aluno
             . " and l.treino_id = " . $prescricao 
             . " and l.ficha_letra = '" . $fichatreino ."'"
             . " and p.treino_id = l.treino_id";


     $con = new PDO("mysql:host={$dbhost};dbname={$dbname}", $dbuser, $dbpass);
     $con->query('SET NAMES utf8');
     $consulta = $con->query($sql);
     $sql="";
     $nrlancExer=0;
     $idInserido=0;
     $qtSerie=0;
     while($c = $consulta->fetch(PDO::FETCH_OBJ)) {
          $nrlancExer=$c->ficha_id;
          $qtSerie=0;
          $sql = "insert evolucaotreino (idaluno,idunidade,idprescricao,nrdiatreino,nrlancamento,qtrepeticao,qtserie, qtcarga,dtregistro,idexercicio)";
          $sql .= " values (" .$aluno . "," . $unidade . "," . $prescricao . "," . $nrdiatreino . "," . $c->ficha_id . ",'" . $c->ficha_repeticoes . "','" . $c->ficha_series . "','" . 0 . "','" . $dia_hoje . "'," . $c->exercicio_id . ")";
          $stmt = $pdo->prepare($sql);           
          $stmt->execute(array(
          ));  
          
          $idInserido= $pdo->lastInsertId("id");
          
          $cargaAnt=0;
          $sql2="select qtcarga, nrserie,qtrepeticao from serietreino where idaluno = " . $aluno;
          $sql2 .=" and idunidade = " . $unidade;
          $sql2 .=" and idprescricao = " . $prescricao;
          $sql2 .=" and nrdiatreino = "  . $nrdiatreino ;
          $sql2 .=" and nrlancamento = " . $c->ficha_id;
          $sql2 .=" and nrlancamentodia = (";
          $sql2 .=" select max(nrlancamentodia) from serietreino ";
          $sql2 .=" where idaluno = " . $aluno;
          $sql2 .=" and idunidade = " . $unidade;
          $sql2 .=" and idprescricao = " . $prescricao;
          $sql2 .=" and nrdiatreino = "  . $nrdiatreino ;
          $sql2 .=" and nrlancamento = " . $c->ficha_id ;
          $sql2 .=" and nrlancamentodia < " . $idInserido .  " )";
          
          $con2 = new PDO("mysql:host={$dbhost};dbname={$dbname}", $dbuser, $dbpass);
          $con2->query('SET NAMES utf8');
          $consulta2 = $con2->query($sql2);       
          $contSeries=0;

          while($c2 = $consulta2->fetch(PDO::FETCH_OBJ)) {   
               $qtSerie=1;
               $cargaAnt=$c2->qtcarga;
               $sql1 = "insert serietreino (idaluno,idunidade,idprescricao,nrdiatreino,nrlancamento,nrlancamentodia,nrserie,qtrepeticao,qtcarga,dtregistro)";
               $sql1 .= " values (" . $aluno. "," . $unidade . "," . $prescricao . "," . $nrdiatreino . "," . $c->ficha_id . "," . $idInserido .  ",'" . $c2->nrserie . "','" . $c2->qtrepeticao . "','" . $cargaAnt . "','" . $dia_hoje . "')";
               $stmt1 = $pdo1->prepare($sql1);           
               $stmt1->execute(array(
               ));  
               $contSeries++;
               //$y++;
              
              //echo $cargaAnt ." | ";
          }
          
          if ($contSeries<$c->ficha_series){
//          if ($qtSerie<1){
             $y = $contSeries + 1;
          
             while( $y <= $c->ficha_series ){
                $sql1 = "insert serietreino (idaluno,idunidade,idprescricao,nrdiatreino,nrlancamento,nrlancamentodia,nrserie,qtrepeticao,qtcarga,dtregistro)";
                $sql1 .= " values (" . $aluno. "," . $unidade . "," . $prescricao . "," . $nrdiatreino . "," . $c->ficha_id . "," . $idInserido .  ",'" . $y . "','" . $c->ficha_repeticoes . "','" . 0 . "','" . $dia_hoje . "')";
                $stmt1 = $pdo1->prepare($sql1);           
                $stmt1->execute(array(
                ));  
               $y++;

             }           
              
          }
     }      
         
     $pdo->commit();
  /*   $sql ="SELECT l.nrserie,l.nrdiatreino, l.nrlancamento,l.nrlancamentodia,l.qtrepeticao, l.qtcarga ";
     $sql .= "FROM serietreino as l ";
     $sql .= " where l.idunidade= " . $unidade;
     $sql .= " AND l.idaluno = " . $aluno . " AND l.idprescricao = " . $prescricao;
     $sql .= " AND l.nrdiatreino = " . $nrdiatreino ;
     $sql .= " AND l.nrlancamentodia  = ";
     $sql .= " (select max(nrlancamentodia) from serietreino s";
     $sql .= " where s.idunidade= " . $unidade;
     $sql .= " AND s.idaluno = " . $aluno . " AND s.idprescricao = " . $prescricao;
     $sql .= " AND s.nrdiatreino = " . $nrdiatreino ;
     $sql .= " AND s.nrlancamentodia < " . $idInserido . ")";
       
             
     $con = new PDO("mysql:host={$dbhost};dbname={$dbname}", $dbuser, $dbpass);
     $con->query('SET NAMES utf8');
     $consulta = $con->query($sql);
     $temSerie=0;
     while($c = $consulta->fetch(PDO::FETCH_OBJ)) {
          $sql ="update serietreino set";
          $sql .= " qtrepeticao = " . $c->qtrepeticao;
          $sql .= " , qtcarga = " . $c->qtcarga;
          $sql .= " where idaluno = " . $aluno;
          $sql .= " and idunidade = " . $unidade;
          $sql .= " and idprescricao = " . $prescricao;
          $sql .= " and nrdiatreino = " . $c->nrdiatreino;
          $sql .= " and nrdiatreino = " . $c->nrdiatreino;
          $sql .= " and nrserie = " . $c->nrserie ;
          $sql .= " and nrlancamentodia = " . $idInserido ;
                  
          $stmt = $pdo->prepare($sql);           
          $stmt->execute(array(
          ));  
     }*/

     
     $row_array['MENSAGEM'] = 'Registro efetuado com sucesso';
     array_push($return_arr,$row_array);
      

     printf( json_encode($return_arr));      
      
 }

} catch (PDOException $exception) {
    $pdo->rollBack();
    printf('Não foi possível realizar a operação: %s' , $exception);
    
 }
 
?>