<?php
  $return_arr = array();
 if (isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
    }
   // Access-Control headers are received during OPTIONS requests
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
 
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         
 
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
 
        exit(0);
    }
 
 
  $postdata = file_get_contents("php://input");
    
    
  try {  
   if (isset($postdata)) {
      $request = json_decode($postdata);

      $idaluno = $request->idaluno;
      $idunidade = $request->idunidade;
      $fichatreino = $request->fichatreino;
      $idexercicio = $request->idexercicio;
      $nrlancamento = $request->nrlancamento;
      
      
      $diatreino=0;
      if($fichatreino=="A"){
         $diatreino=1;
      }
      if($fichatreino=="B"){
         $diatreino=2;
      }
      if($fichatreino=="C"){
         $diatreino=3;
      }
      if($fichatreino=="D"){
         $diatreino=4;
      }
      if($fichatreino=="E"){
         $diatreino=5;
      }
      if($fichatreino=="F"){
        $diatreino=6;
      }
      
      include "conecta.php";        

      $pdo = new PDO("mysql:host={$dbhost};dbname={$dbname}", $dbuser, $dbpass);
      $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);  
      $pdo1 = new PDO("mysql:host={$dbhost};dbname={$dbname}", $dbuser, $dbpass);
      $pdo1->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);  
      
 
//      $sql="select l.idaluno,t.unidade_id,l.treino_id,l.ficha_letra,l.exercicio_id,l.nrlancamento,"

             $dia_hoje = date('Y-m-d') ; //////////////////////ate aqui grava historico treino
       
        $ultLct=0;
        $sql  = "select max(nrlancamentodia) as max from serietreino where idaluno = " . $idaluno;
        $sql .= " and nrdiatreino = " . $diatreino ;
        $sql .= " and nrlancamento = " . $nrlancamento ;
        $sql .= " and idunidade = " . $idunidade ;
        $sql .= " and idprescricao = (select treino_id from treino_musculacao where aluno_id = " . $idaluno;
        $sql .= " and idunidade = " . $idunidade . " and treino_atual = 1 )";        

        $con = new PDO("mysql:host={$dbhost};dbname={$dbname}", $dbuser, $dbpass);
        $con->query('SET NAMES utf8');
        $consulta = $con->query($sql);
        while($c = $consulta->fetch(PDO::FETCH_OBJ)) {
            $ultLct=$c->max;
        }
        
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////        
        if ($ultLct==0){  //SE EXERCICIO INSERIDO DCOM TREINO EM ANDAMENTO GERA SERIES
            $sql="select l.treino_id,l.exercicio_id, l.ficha_letra, l.ficha_id, l.ficha_series, l.ficha_repeticoes from treino_ficha l, "
                 . " treino_musculacao p where p.unidade_id = " . $idunidade
                 . " and p.aluno_id = " . $idaluno
                 . " and l.treino_id = (select treino_id from treino_musculacao where aluno_id = " . $idaluno
                 . " and unidade_id = " . $idunidade . " and treino_atual = 1 )"
                 . " and l.ficha_letra = '" . $fichatreino ."'"
                 . " and l.ficha_id = " . $nrlancamento
                 . " and p.treino_id = l.treino_id";
                 $con = new PDO("mysql:host={$dbhost};dbname={$dbname}", $dbuser, $dbpass);
                 $con->query('SET NAMES utf8');
                 $consulta = $con->query($sql);
                 $sql="";
                 $nrlancExer=0;
                 $idInserido=0;
                 $qtSerie=0;
                 $cargaAnt=0;
                 
                 while($c = $consulta->fetch(PDO::FETCH_OBJ)) {            
                      $nrlancExer=$c->ficha_id;
                      $qtSerie=0;
                      $sql = "insert evolucaotreino (idaluno,idunidade,idprescricao,nrdiatreino,nrlancamento,qtrepeticao,qtserie, qtcarga,dtregistro,idexercicio)";
                      $sql .= " values (" .$idaluno . "," . $idunidade . "," . $c->treino_id . "," . $diatreino . "," . $c->ficha_id . ",'" . $c->ficha_repeticoes . "','" . $c->ficha_series . "','" . 0 . "','" . $dia_hoje . "'," . $c->exercicio_id . ")";
                      $stmt = $pdo->prepare($sql);           
                      $stmt->execute(array(
                      ));  
                      $idInserido= $pdo->lastInsertId("id");
                      $ultLct=$idInserido;
                      $y = 1;
                      while( $y <= $c->ficha_series ){
                        $sql1 = "insert serietreino (idaluno,idunidade,idprescricao,nrdiatreino,nrlancamento,nrlancamentodia,nrserie,qtrepeticao,qtcarga,dtregistro)";
                        $sql1 .= " values (" . $idaluno. "," . $idunidade . "," . $c->treino_id . "," . $diatreino . "," . $c->ficha_id . "," . $idInserido .  ",'" . $y . "','" . $c->ficha_repeticoes . "','" . $cargaAnt . "','" . $dia_hoje . "')";
                        $stmt1 = $pdo1->prepare($sql1);           
                        $stmt1->execute(array(
                        ));  
                        $y++;
                      }                        
                     
                }
            
            
            
        }///////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $sql ="select s.id,s.qtcarga, s.qtrepeticao, s.idaluno, s.idunidade, s.idprescricao, s.nrdiatreino, s.nrserie, s.nrlancamentodia, "
                . "le.ficha_series as qts, "
                . "e.id as idexercicio,"
                . "s.nrlancamento,";
        $sql .= " e.nmexercicio ";
        $sql .= "from serietreino as s, treino_ficha as le ,exercicio as e where";
        $sql .= " le.treino_id = s.idprescricao ";
        $sql .= " and s.idaluno = " . $idaluno;
        $sql .= " and s.idunidade = " . $idunidade ;
        $sql .= " and le.treino_id = s.idprescricao ";
        $sql .= " and le.ficha_letra = '" . $fichatreino . "'";
        $sql .= " and le.ficha_id = s.nrlancamento ";
        $sql .= " and s.nrlancamento = " . $nrlancamento;
        $sql .= " and s.nrlancamentodia = " . $ultLct;
        $sql .= " and e.id = " . $idexercicio;
        $sql .= " and s.idprescricao = (select treino_id from treino_musculacao where aluno_id = " . $idaluno;
        $sql .= " and unidade_id = " . $idunidade . " and treino_atual = '1') order by s.nrdiatreino, s.nrserie";
        
        $con5 = new PDO("mysql:host={$dbhost};dbname={$dbname}", $dbuser, $dbpass);
        $con5->query('SET NAMES utf8');
        $consulta5 = $con5->query($sql);
        
        $aluno=0;
        $unidade=0;
        $prescricao=0;
        $nrdiatreino=0;
        $serie=0;
        $exercicio=0;
        $nmexercicio=0;
        $x=0;
        $qts=0;
        
        while($c5 = $consulta5->fetch(PDO::FETCH_OBJ)) {
              $qts=$c5->qts;  
              $aluno=$c5->idaluno;
              $unidade=$c5->idunidade;  
              $prescricao=$c5->idprescricao;
              $nrdiatreino=$c5->nrdiatreino;
              $serie=$c5->nrserie;
              $exercicio=$c5->idexercicio;
              $nmexercicio=$c5->nmexercicio;
              
///////////////////////////////////////////////////////////////////////              
              //$row_array['ID'] = $c5->id;
              $row_array['IDALUNO'] = $c5->idaluno;
              $row_array['NRLANCAMENTODIA'] = $c5->nrlancamentodia;
              $row_array['IDUNIDADE'] = $c5->idunidade;
              $row_array["IDPRESCRICAO"]= $c5->idprescricao ;
              $row_array["NRDIATREINO"]=  $c5->nrdiatreino ;
              $row_array["NRSERIE"]=  $c5->nrserie ;
              $row_array["IDEXERCICIO"]=  $c5->idexercicio ;
              $row_array["NRLANCAMENTO"]=  $c5->nrlancamento ;
              $row_array["NMEXERCICIO"]=  $c5->nmexercicio ;
              $row_array["QTREPETICAO"]=  $c5->qtrepeticao ;
              $row_array["QTCARGA"]=  $c5->qtcarga ;
              $x++;
              
              array_push($return_arr,$row_array);
        }
        
        
        $y=$qts-$x;
        for($i =1; $i <= $y; $i++){
              $row_array['IDALUNO'] = $aluno;
              $row_array['IDUNIDADE'] = $unidade;
              $row_array["IDPRESCRICAO"]=  $prescricao ;
              $row_array["NRDIATREINO"]=  $nrdiatreino ;
              $row_array["NRSERIE"]=  $x + $i;
              $row_array["IDEXERCICIO"]=  $exercicio ;
              //$row_array["NRLANCAMENTO"]=  $row['nrlancamento'] ;
              $row_array["NMEXERCICIO"]=  $nmexercicio ;
              $row_array["QTREPETICAO"]=  0 ;
              $row_array["QTCARGA"]= 0 ;
              array_push($return_arr,$row_array);

        }        
     //printf( json_encode($sql));      
     printf( json_encode($return_arr));      
      
 }

} catch (PDOException $exception) {
    //$pdo->rollBack();
    printf('Não foi possível realizar a operação: %s' , $exception);
    
 }
 

 
?>