<?php
  $return_arr = array();
 if (isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
    }
   // Access-Control headers are received during OPTIONS requests
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
 
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         
 
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
 
        exit(0);
    }
 
 
  $postdata = file_get_contents("php://input");
    
    
  try {  
    
   if (isset($postdata)) {

      $request = json_decode($postdata);
     //  print_r($request);
     // die();
      $idaluno = $request->idaluno;
      $nrserie = $request->nrserie;

      $idunidade = $request->idunidade;
      $idprescricao = $request->idprescricao;
      $fichatreino = $request->fichatreino;
      

      $series = $request->series;  
      //$agrupado=$request->agrupado;  
      
      $agrupado=isset($request->agrupado) ? $request->agrupado : '1';
      
      echo $agrupado;
      $dia_hoje = date('Y-m-d') ;      
      $diatreino=0;
      if($fichatreino=="A"){
         $diatreino=1;
      }
      if($fichatreino=="B"){
         $diatreino=2;
      }
      if($fichatreino=="C"){
         $diatreino=3;
      }
      if($fichatreino=="D"){
         $diatreino=4;
      }
      if($fichatreino=="E"){
         $diatreino=5;
      }
      if($fichatreino=="F"){
        $diatreino=6;
      }
      if($fichatreino=="G"){
        $diatreino=7;
      }
      
      include "conecta.php";        

      $pdo = new PDO("mysql:host={$dbhost};dbname={$dbname}", $dbuser, $dbpass);
      $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);  
      $pdo1 = new PDO("mysql:host={$dbhost};dbname={$dbname}", $dbuser, $dbpass);
      $pdo1->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);        
      
  
 // foreach($array as $array_item){      
        // $object = $array[$i];
        // $sql ="SELECT max(nrlancamentodia) as maximo from serietreino";
        // $sql .= " WHERE idaluno = " . $idaluno;
        // $sql .= " AND idunidade= " . $idunidade;
        // $sql .= " AND nrdiatreino = " . $diatreino ;
        // $sql .= " AND nrlancamento = " . $object->nrlancamento;
        // $ultimoDia=1;        

        // $con = new PDO("mysql:host={$dbhost};dbname={$dbname}", $dbuser, $dbpass);
        // $con->query('SET NAMES utf8');
        // $consulta = $con->query($sql);
        // while($c = $consulta->fetch(PDO::FETCH_OBJ)) {
        //     $ultimoDia=$c->maximo;        
        // }  

            $sql1  = "update serietreino set ";
            $sql1 .= "QTREPETICAO = '" . $series[0]->repeticao;
            $sql1 .= "', QTCARGA = '" . $series[0]->carga;
            $sql1 .= "', DTREGISTRO = '" . $dia_hoje ;
            $sql1 .= "' where IDALUNO = :aluno" ;
            $sql1 .= " AND IDUNIDADE = " . $idunidade;
            $sql1 .= " AND IDPRESCRICAO = " . $idprescricao;
            $sql1 .= " AND NRDIATREINO = " . $diatreino;
            $sql1 .= " AND NRLANCAMENTO = " . $series[0]->nrlancamento;
            $sql1 .= " AND NRLANCAMENTODIA = " . $series[0]->nrlancamentodia;
            if ($agrupado==1){//1=nao --0 = sim
                $sql1 .= " AND NRSERIE = " . $nrserie ;
            }    

            // echo $sql1;
            $stmt = $pdo->prepare($sql1);           
            $stmt->execute(array(':aluno' => $idaluno,));  


      
  //    $i++; 
  // }
     $row_array['MENSAGEM'] = 'Registro efetuado com sucesso';
     array_push($return_arr,$row_array);
     printf( json_encode($return_arr));      
      
 }

} catch (PDOException $exception) {
     $row_array['MENSAGEM'] = 'Não foi possível realizar a operação:' . $exception;
     array_push($return_arr,$row_array);
     printf( json_encode($return_arr));      
     //printf('Não foi possível realizar a operação: %s' , $exception);
    
 }
 

 
?>