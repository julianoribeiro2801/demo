<?php

$return_arr = array();
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}


// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}


$postdata = file_get_contents("php://input");


try {
    if (isset($postdata)) {

    date_default_timezone_set('America/Sao_Paulo');   

        $request = json_decode($postdata);

        $idaluno = $request->idaluno;
        $idunidade = $request->idunidade;

        include "conecta.php";


        $arr = [];
        $dataHoje=date('Y-m-d') ; 
        $dataFim = date('Y-m-d H:i:s') ;  
//////////////////////////////////////////////////////////////        
        //fechar treinos com data anterior        
        $sql1 = "select date_format(dtinicio, '%Y-%m-%d') as dtinicio, idaluno, idtreino,id  from historicotreino where idaluno = " . $idaluno;
        $sql1 .= " and idunidade = " . $idunidade ;
        $sql1 .= " and sttreino = 'A'" ;

        $pdo = new PDO("mysql:host={$dbhost};dbname={$dbname}", $dbuser, $dbpass);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        
        $con = new PDO("mysql:host={$dbhost};dbname={$dbname}", $dbuser, $dbpass);
        $con->query('SET NAMES utf8');
        $consulta = $con->query($sql1);
        while ($c = $consulta->fetch(PDO::FETCH_OBJ)) {
            if ($c->dtinicio < $dataHoje){
                $sql = "update historicotreino set sttreino = 'F', dtfim = :datafim  where id = :id" ;
                $stmt = $pdo->prepare($sql);                  
                $stmt->execute(array(
                    ':id' => $c->id,
                    ':datafim' => $dataFim
                ));                
            }
            
        }    ////peg
      
//////////////////////////////////////////////////////////////////        
        $stat="F";    
        $sql1 = "select treino_qtddias as diastreino, treino_id from treino_musculacao where aluno_id = " . $idaluno;
        $sql1 .= " and unidade_id = " . $idunidade . " and treino_id = (select treino_id";
        $sql1 .= " from treino_musculacao where aluno_id = " . $idaluno . " and unidade_id = " . $idunidade . " and treino_atual = '1')";
        $con = new PDO("mysql:host={$dbhost};dbname={$dbname}", $dbuser, $dbpass);
        $con->query('SET NAMES utf8');
        $consulta = $con->query($sql1);
        while ($c = $consulta->fetch(PDO::FETCH_OBJ)) {
            $idtreino = $c->treino_id;
            $diastreino = $c->diastreino;
        }    ////pega treino vigente  
        $pdo = new PDO("mysql:host={$dbhost};dbname={$dbname}", $dbuser, $dbpass);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $pdo1 = new PDO("mysql:host={$dbhost};dbname={$dbname}", $dbuser, $dbpass);
        $pdo1->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $sql = "select idaluno, idunidade, nrdiatreino, idtreino,dtinicio, pausado, sttreino from historicotreino where ";
        $sql.=" idaluno = " . $idaluno;
        $sql.=" and idunidade = " . $idunidade;
        $sql.=" and idtreino = " . $idtreino;
        $sql.=" and id = (select max(id) from historicotreino where idaluno = " . $idaluno;
        $sql.=" and idunidade = " . $idunidade . " and idtreino = " . $idtreino . " )";
        $con = new PDO("mysql:host={$dbhost};dbname={$dbname}", $dbuser, $dbpass);
        $con->query('SET NAMES utf8');
        $consulta = $con->query($sql);
        $treinoAberto = "";
        $fichaAberta = "";
        $ultimoDia = 0;
        $pausado = "N";
        while ($c = $consulta->fetch(PDO::FETCH_OBJ)) {
            $pausado = $c->pausado;
            $stat = $c->sttreino;
            $ultimoDia = $c->nrdiatreino;

            IF ($c->nrdiatreino == 1) {
                $fichaAberta = "A";
            }
            IF ($c->nrdiatreino == 2) {
                $fichaAberta = "B";
            }
            IF ($c->nrdiatreino == 3) {
                $fichaAberr = "C";
            }
            IF ($c->nrdiatreino == 4) {
                $ficha = "D";
            }
            IF ($c->nrdiatreino == 5) {
                $ficha = "E";
            }
            IF ($c->nrdiatreino == 6) {
                $ficha = "F";
            }
            IF ($c->nrdiatreino == 7) {
                $ficha = "G";
            }
        }
        if ($stat=='F'){
            $ultimoDia=$ultimoDia+1;
            if ($ultimoDia>$diastreino){
                $ultimoDia=1;
            }
            
        }

            

        $sql1 = "select treino_qtddias as diastreino, treino_id from treino_musculacao where aluno_id = " . $idaluno;
        $sql1 .= " and unidade_id = " . $idunidade . " and treino_id = (select treino_id";
        $sql1 .= " from treino_musculacao where aluno_id = " . $idaluno . " and unidade_id = " . $idunidade . " and treino_atual = '1')";
        $con = new PDO("mysql:host={$dbhost};dbname={$dbname}", $dbuser, $dbpass);
        $con->query('SET NAMES utf8');
        $consulta = $con->query($sql1);
        while ($c = $consulta->fetch(PDO::FETCH_OBJ)) {
            $idtreino = $c->treino_id;
            $diastreino = $c->diastreino;
        }    ////pega treino vigente  

        $ficha_letra = "";
        for ($i = 1; $i <= $diastreino; $i++) {
            if ($i == 1) {
                $ficha_letra = "A";
            }
            if ($i == 2) {
                $ficha_letra = "B";
            }
            if ($i == 3) {
                $ficha_letra = "C";
            }
            if ($i == 4) {
                $ficha_letra = "D";
            }
            if ($i == 5) {
                $ficha_letra = "E";
            }
            if ($i == 6) {
                $ficha_letra = "F";
            }
            if ($i == 7) {
                $ficha_letra = "G";
            }
            if ($i == 8) {
                $ficha_letra = "H";
            }

            $row_array['NRDIATREINO'] = $ficha_letra;
            $row_array['NMGRUPO'] = 'grupos'; //BuscaGrupo($c->treino_id,$numdia,$idaluno);
            $row_array['DURACAO'] = '20'; //BuscaDuracao($row['idprescricao'],$row['nrdiatreino'],$id,$unidade);
            $row_array['NOME'] = $ficha_letra;
            if ($ultimoDia == $i) {  ///....depois converter pra numeroc
                $row_array['REALIZAR'] = 'S';
            } else {
                $row_array['REALIZAR'] = 'N';
            }
            if ($stat !== 'A') {
                $row_array['STABERTO'] = 'N';
            } else {
                $row_array['STABERTO'] = 'S';
            }
            $row_array['IDPRESCRICAO'] = $idtreino;
            $row_array['DSABERTO'] = $ultimoDia;
            $row_array['PAUSADO'] = $pausado;
            $row_array['DTINICIO'] = '12:00:00'; //BuscaHoraInicio($row['idprescricao'],$row['nrdiatreino'],$id,$unidade);
            array_push($return_arr, $row_array);
        }

       /* $ultimodia = 0;
        $sql = "select coalesce(nrdiatreino,0) as dia, sttreino, pausado  from historicotreino where idunidade= " . $idunidade;
        $sql.= " and idaluno = " . $idaluno . " and idtreino = (select treino_id";
        $sql.= " from treino_musculacao where aluno_id = " . $idaluno . " and unidade_id = " . $idunidade . " and treino_atual = '1') and sttreino = 'F'";
        $sql.= " and id = (select max(id) from historicotreino";
        $sql.= " where idunidade= " . $idunidade;
        $sql.= " and idaluno = " . $idaluno . " and idtreino = (select treino_id ";
        $sql.= "from treino_musculacao where aluno_id = " . $idaluno . " and unidade_id = " . $idunidade . " and sttreino = 'F' and treino_atual = '1'))";
        $sql.= " and sttreino = 'F' ";


        $con = new PDO("mysql:host={$dbhost};dbname={$dbname}", $dbuser, $dbpass);
        $con->query('SET NAMES utf8');
        $consulta = $con->query($sql);


        $stat = "X";
        $pausado = "";
        while ($c = $consulta->fetch(PDO::FETCH_OBJ)) {
            $pausado = $c->pausado;
            $stat = $c->sttreino;
            $ultimodia = $c->dia;
        }


        $ultimodia = $ultimodia + 1;
        $idtreino = 0;
        $diastreino = 1;
        $sql1 = "select treino_qtddias as diastreino, treino_id from treino_musculacao where aluno_id = " . $idaluno;
        $sql1 .= " and unidade_id = " . $idunidade . " and treino_id = (select treino_id";
        $sql1 .= " from treino_musculacao where aluno_id = " . $idaluno . " and unidade_id = " . $idunidade . " and treino_atual = '1')";
        $con = new PDO("mysql:host={$dbhost};dbname={$dbname}", $dbuser, $dbpass);
        $con->query('SET NAMES utf8');
        $consulta = $con->query($sql1);
        while ($c = $consulta->fetch(PDO::FETCH_OBJ)) {
            $idtreino = $c->treino_id;

            $diastreino = $c->diastreino;
        }
//////////////////////////////////////////        
        if ($ultimodia > $diastreino) {

            $ultimodia = 1;
        }


        $ficha_letra = "";
        for ($i = 1; $i <= $diastreino; $i++) {
            if ($i == 1) {
                $ficha_letra = "A";
            }
            if ($i == 2) {
                $ficha_letra = "B";
            }
            if ($i == 3) {
                $ficha_letra = "C";
            }
            if ($i == 4) {
                $ficha_letra = "D";
            }
            if ($i == 5) {
                $ficha_letra = "E";
            }
            if ($i == 6) {
                $ficha_letra = "F";
            }
            if ($i == 7) {
                $ficha_letra = "G";
            }
            if ($i == 8) {
                $ficha_letra = "H";
            }

            $row_array['NRDIATREINO'] = $ficha_letra;
            $row_array['NMGRUPO'] = 'grupos'; //BuscaGrupo($c->treino_id,$numdia,$idaluno);
            $row_array['DURACAO'] = '20'; //BuscaDuracao($row['idprescricao'],$row['nrdiatreino'],$id,$unidade);
            $row_array['NOME'] = $ficha_letra;
            if ($ultimodia == $i) {  ///....depois converter pra numeroc
                $row_array['REALIZAR'] = 'S';
            } else {
                $row_array['REALIZAR'] = 'N';
            }
            if ($stat == 'X') {
                $row_array['STABERTO'] = 'N';
            } else {
                $row_array['STABERTO'] = 'S';
            }
            $row_array['IDPRESCRICAO'] = $idtreino;
            $row_array['DSABERTO'] = $ultimodia;
            $row_array['PAUSADO'] = $pausado;
            $row_array['DTINICIO'] = '12:00:00'; //BuscaHoraInicio($row['idprescricao'],$row['nrdiatreino'],$id,$unidade);
            array_push($return_arr, $row_array);
        }



        /* $numdia=0;
          $sql= "select distinct(l.ficha_letra),l.treino_id from treino_ficha l where l.treino_id =" . $idtreino . " and l.ficha_letra between 'A' and '" . $letraLimite . "' order by l.ficha_letra";
          $con = new PDO("mysql:host={$dbhost};dbname={$dbname}", $dbuser, $dbpass);
          $con->query('SET NAMES utf8');
          $cont=0;
          $consulta = $con->query($sql);
          while($c = $consulta->fetch(PDO::FETCH_OBJ)) {
          if ($c->ficha_letra=='A'){
          $numdia=1;
          }
          if ($c->ficha_letra=='B'){
          $numdia=2;
          }
          if ($c->ficha_letra=='C'){
          $numdia=3;
          }
          if ($c->ficha_letra=='D'){
          $numdia=4;
          }
          if ($c->ficha_letra=='E'){
          $numdia=5;
          }
          if ($c->ficha_letra=='F'){
          $numdia=6;
          }
          if ($c->ficha_letra=='G'){
          $numdia=7;
          }
          if ($c->ficha_letra=='H'){
          $numdia=8;
          }
          $row_array['NRDIATREINO']=$c->ficha_letra;
          $row_array['NMGRUPO']=  'grupos';//BuscaGrupo($c->treino_id,$numdia,$idaluno);
          $row_array['DURACAO']=  '20';//BuscaDuracao($row['idprescricao'],$row['nrdiatreino'],$id,$unidade);
          $row_array['NOME']=$c->ficha_letra;
          // echo "cont " . $cont;
          if ($ultimodia==$numdia){  ///....depois converter pra numeroc
          $row_array['REALIZAR']='S';
          } else {
          $row_array['REALIZAR']='N';
          }
          /*if ($c->ficha_letra==$arr[$ultimodia - 1]){  ///....depois converter pra numeroc
          $row_array['REALIZAR']='S';
          } else {
          $row_array['REALIZAR']='N';
          }
          if ($stat!=''){
          $row_array['STABERTO']='N';
          } else {
          $row_array['STABERTO']='S';
          }
          $row_array['IDPRESCRICAO']=$c->treino_id;
          $row_array['DSABERTO']=$ultimodia;
          $row_array['DTINICIO']= '12:00:00'; //BuscaHoraInicio($row['idprescricao'],$row['nrdiatreino'],$id,$unidade);
          array_push($return_arr,$row_array);
          $cont++;
          } */

        printf(json_encode($return_arr));
    }
} catch (PDOException $exception) {
    //$pdo->rollBack();
    printf('Não foi possível realizar a operação: %s', $exception);
}
?>