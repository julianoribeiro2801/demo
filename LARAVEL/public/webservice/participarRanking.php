<?php
  $return_arr = array();
 if (isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
    }
   // Access-Control headers are received during OPTIONS requests
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
 
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         
 
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
 
        exit(0);
    }
 
 
  $postdata = file_get_contents("php://input");
    
    
  try {  
   if (isset($postdata)) {
      $request = json_decode($postdata);

      $aluno = $request->idaluno;
      $unidade = $request->idunidade;
//      $conquista = $request->idconquista;
      $atividade = $request->idatividade;
      $participa = $request->participa;
       
      $totConquista=0;
      $posicao=0;
      
      
               
      include "conecta.php";        

      $pdo = new PDO("mysql:host={$dbhost};dbname={$dbname}", $dbuser, $dbpass);
      $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);  
     // $pdo->setAttribute(PDO::ATTR_AUTOCOMMIT,false);  
     // $pdo->beginTransaction();
      

//////////////////////////////////////////////////////////////////////////////
      
      $part="";
      
      $sql="select idatividade, id as idconquista from conquista where ";
      $sql .= " idatividade = " . $atividade;
      $con = new PDO("mysql:host={$dbhost};dbname={$dbname}", $dbuser, $dbpass);
      $con->query('SET NAMES utf8');
      $consulta = $con->query($sql);

      while($c = $consulta->fetch(PDO::FETCH_OBJ)) {
          $conquista = $c->idconquista;
      }
      
      
      
      $sql="select idaluno from conquistausuario where ";
      $sql .= " idaluno = " . $aluno;
      $sql .= " and idunidade = " . $unidade;
      $sql .= " and idconquista = " . $conquista;
      $con = new PDO("mysql:host={$dbhost};dbname={$dbname}", $dbuser, $dbpass);
      $con->query('SET NAMES utf8');
      $consulta = $con->query($sql);

      while($c = $consulta->fetch(PDO::FETCH_OBJ)) {
          $part = "S";
      } /// total do aluno
//////////////////////////////////////////////////////////////////////////////////////////      
   if ($part != ""){      
      $stmt = $pdo->prepare("UPDATE conquistausuario set stparticipa = :participa where idaluno = :aluno and idunidade = :unidade and idconquista = :conquista ");
      $stmt->execute(array(
        ':aluno' => $aluno,
        ':unidade' => $unidade,
        ':conquista' => $conquista,
        ':participa' => $participa,
      )); 
   } else {
      $stmt = $pdo->prepare("INSERT INTO conquistausuario(IDUNIDADE, IDALUNO, IDCONQUISTA, STCONQUISTA, DTCONQUISTA, QTCONQUISTA, STPARTICIPA, DTREGISTRO) "
              . "            VALUES (:unidade, :aluno, :conquista,'S',CURRENT_DATE, 0,:participa,CURRENT_DATE)");
      $stmt->execute(array(
        ':aluno' => $aluno,
        ':unidade' => $unidade,
        ':conquista' => $conquista,
        ':participa' => $participa,
      )); 

       
   }
     $row_array['MENSAGEM'] = 'Registro efetuado com sucesso';
     array_push($return_arr,$row_array);
      

     printf( json_encode($return_arr));             

 }

} catch (PDOException $exception) {
    //$pdo->rollBack();
    printf('Não foi possível realizar a operação: %s' , $exception);
    
 }
 
?>