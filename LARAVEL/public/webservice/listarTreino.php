<?php
  $return_arr = array();
 if (isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
    }
   // Access-Control headers are received during OPTIONS requests
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
 
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         
 
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
 
        exit(0);
    }
 
 
  $postdata = file_get_contents("php://input");
    
    
  try {  
   if (isset($postdata)) {
      $request = json_decode($postdata);

      $idaluno = $request->idaluno;
      $idunidade = $request->idunidade;
      $diatreino = $request->diatreino;

      include "conecta.php";        

      $pdo = new PDO("mysql:host={$dbhost};dbname={$dbname}", $dbuser, $dbpass);
      $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);  
      $pdo1 = new PDO("mysql:host={$dbhost};dbname={$dbname}", $dbuser, $dbpass);
      $pdo1->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);  
      
 
//      $sql="select l.idaluno,t.unidade_id,l.treino_id,l.ficha_letra,l.exercicio_id,l.nrlancamento,"

       
       
    $sql = " select l.ficha_id, l.ord,e.video,e.qtduracao,t.aluno_id,t.unidade_id,t.treino_id,l.ficha_letra, coalesce(l.exercicio_id,0) AS exercicio_id ,"
               . " coalesce(e.nmexercicio,'NÃO INFORMADO') as nmexercicio ,l.ficha_series,l.ficha_repeticoes,e.dsurlminiatura"
               . " from treino_musculacao as t, treino_ficha as l left join "
               . " exercicio as e on e.id = l.exercicio_id where t.unidade_id= " . $idunidade . " and t.aluno_id = " . $idaluno . " and "
               . " t.treino_atual = 1 and l.ficha_letra = '" . $diatreino . "'"
               . " and l.treino_id = t.treino_id order by ficha_letra,ord";
    
    $con = new PDO("mysql:host={$dbhost};dbname={$dbname}", $dbuser, $dbpass);
    $con->query('SET NAMES utf8');
    
    $consulta = $con->query($sql);
    while($c = $consulta->fetch(PDO::FETCH_OBJ)) {
            $row_array['IDALUNO'] =$c->aluno_id;
            $row_array['IDUNIDADE'] = $c->unidade_id;
            $row_array["IDPRESCRICAO"]=  $c->treino_id;
            $row_array["NRDIATREINO"]=  $c->ficha_letra ;
            $row_array["IDEXERCICIO"]= $c->exercicio_id;
            $row_array["NRLANCAMENTO"]=  $c->ficha_id ;
            $row_array["NMEXERCICIO"]=  $c->nmexercicio;
            $row_array["QTSERIE"]=  $c->ficha_series;
            $row_array["QTREPETICAO"]=  $c->ficha_repeticoes;
            //$row_array["QTDURACAO"]= $c->qtduracao * $c->ficha_repeticoes * $c->ficha_series  ;
            $row_array["QTINTERVALO"]=  0 ;//nao tem na tabela
            $row_array["ORDEM"]=  $c->ord;            
            $row_array["DSURLMINIATURA"]=  $c->dsurlminiatura;            
            $row_array["VIDEO"]=  $c->video;            
            array_push($return_arr,$row_array);
      }

     printf( json_encode($return_arr));      
//      printf( json_encode($sql));      
      
 }

} catch (PDOException $exception) {
    //$pdo->rollBack();
    printf('Não foi possível realizar a operação: %s' , $exception);
    
 }
 

 
?>