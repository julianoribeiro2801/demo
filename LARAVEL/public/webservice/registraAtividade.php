<?php
 date_default_timezone_set('America/Sao_Paulo');
 $data_hora_atual = date('Y-m-d H:i:s');

 if (isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
    }
   // Access-Control headers are received during OPTIONS requests
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
 
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         
 
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
 
        exit(0);
    }

 
 
    $postdata = file_get_contents("php://input");
    
    
  try {  
   if (isset($postdata)) {
      $request = json_decode($postdata);

      $aluno = $request->idaluno;
      $unidade = $request->idunidade;
      $atividade = $request->idatividade;
      $qtatividade = str_replace(':','.',$request->qtatividade);
      $stmovatividade = $request->stmovatividade;
       
      include "conecta.php";        

      $pdo = new PDO("mysql:host={$dbhost};dbname={$dbname}", $dbuser, $dbpass);
      $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);  
     
      $pdo->setAttribute(PDO::ATTR_AUTOCOMMIT,false);  
     
      $pdo->beginTransaction();
     
      $con = new PDO("mysql:host={$dbhost};dbname={$dbname}", $dbuser, $dbpass);
      $con->query('SET NAMES utf8');
 ////////////////////////////////
      $stmt = $pdo->prepare("INSERT INTO movatividade (IDALUNO,IDUNIDADE,IDATIVIDADE, QTATIVIDADE,DTREGISTRO,STMOVATIVIDADE) VALUES (:aluno,:unidade,:atividade,:qtatividade,
         :data_hora_atual,:stmovatividade)");
      $stmt->execute(array(
        ':aluno' => $aluno,
        ':unidade' => $unidade,
        ':atividade' => $atividade,
        ':qtatividade' => $qtatividade,
        ':stmovatividade' => $stmovatividade,
        ':data_hora_atual'=>  $data_hora_atual
      
      )); 
     
      $pdo->commit();
      printf('Registro efetuado com sucesso');
 }

} catch (PDOException $exception) {
    
    $pdo->rollBack();
    
    printf('Não foi possível realizar a operação: %s' , $exception);
    
 }
 
 

?>