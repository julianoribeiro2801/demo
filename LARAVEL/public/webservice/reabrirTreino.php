<?php
 $return_arr = array();

 if (isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
    }
    date_default_timezone_set('America/Sao_Paulo');    
    
   // Access-Control headers are received during OPTIONS requests
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
 
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         
 
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
 
        exit(0);
    }
 
 
    $postdata = file_get_contents("php://input");
    
    
  try {  
   if (isset($postdata)) {
      date_default_timezone_set('America/Sao_Paulo');       
      $request = json_decode($postdata);

      $idaluno = $request->idaluno;
      $unidade = $request->idunidade;
      $prescricao = $request->idtreino;
      $fichatreino = $request->fichatreino;
      $nrdiatreino=0;
      if($fichatreino=="A"){
         $nrdiatreino=1;
      }
      if($fichatreino=="B"){
         $nrdiatreino=2;
      }
      if($fichatreino=="C"){
         $nrdiatreino=3;
      }
      if($fichatreino=="D"){
         $nrdiatreino=4;
      }
      if($fichatreino=="E"){
         $nrdiatreino=5;
      }
      if($fichatreino=="F"){
        $nrdiatreino=6;
      }
      if($fichatreino=="G"){
        $nrdiatreino=7;
      }
      if($fichatreino=="H"){
        $nrdiatreino=8;
      }
      if($fichatreino=="I"){
        $nrdiatreino=9;
      }
      if($fichatreino=="J"){
        $nrdiatreino=10;
      }
       
        
      $dtHj= date('Y-m-d');

      include "conecta.php";        

      $pdo = new PDO("mysql:host={$dbhost};dbname={$dbname}", $dbuser, $dbpass);
      $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);  
     // $pdo->setAttribute(PDO::ATTR_AUTOCOMMIT,false);  
     // $pdo->beginTransaction();
      
      
            $ultimo=0;
            $sql = "select max(id) as maximo from historicotreino" .
            " where idunidade = " .  $unidade .
            " and idaluno = " .  $idaluno .
            " and idtreino = " .  $prescricao .
            " and nrdiatreino = " .  $nrdiatreino;
           $con = new PDO("mysql:host={$dbhost};dbname={$dbname}", $dbuser, $dbpass);
           $con->query('SET NAMES utf8');
           $consulta = $con->query($sql);
           $duracao=0;
           while($c = $consulta->fetch(PDO::FETCH_OBJ)) {
               $ultimo=$c->maximo;
            }      
      
      
            $reabre=0;
             $sql = "select max(id) as maximo from historicotreino" .
             " where idunidade = " .  $unidade .
             " and idaluno = " .  $idaluno .
             " and idtreino = " .  $prescricao .
             " and sttreino = 'F'" .              
             " and nrdiatreino = " .  $nrdiatreino;
           $con = new PDO("mysql:host={$dbhost};dbname={$dbname}", $dbuser, $dbpass);
           $con->query('SET NAMES utf8');
           $consulta = $con->query($sql);
           $duracao=0;
           while($c = $consulta->fetch(PDO::FETCH_OBJ)) {
               $reabre=$c->maximo;
           }      
           if ($ultimo ==  $reabre){
                $sql = "update historicotreino set sttreino = 'A'" . 
                     " where id = " . $reabre;
                $stmt = $pdo->prepare($sql);              
                $stmt->execute(array(
                    ':aluno' => $idaluno,
                    ':unidade' => $unidade,
                    ':prescricao' => $prescricao,
                    ':nrdiatreino' => $nrdiatreino,
                    //':dtfim' => $dtHj, 
                )); 
                $row_array['MENSAGEM'] = 'Registro efetuado com sucesso';
          } else {
              $row_array['MENSAGEM'] = 'Treino já reaberto';
          }      

       array_push($return_arr,$row_array);
      

     printf( json_encode($return_arr));      
 }

} catch (PDOException $exception) {
    //$pdo->rollBack();
    printf('Não foi possível realizar a operação: %s' , $exception);
    
 }
 
?>