 <?php
 $return_arr = array();

 if (isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
    }
   // Access-Control headers are received during OPTIONS requests
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
 
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         
 
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            header("Access-Control-Allow-Headers:{$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
 
        exit(0);
    }
 
 
    $postdata = file_get_contents("php://input");
    
    
  try {  
   if (isset($postdata)) {
      $request = json_decode($postdata);
      $idaluno = $request->idaluno;
      $idunidade = $request->idunidade;

       
   
      
               
      include "conecta.php";        

      $pdo = new PDO("mysql:host={$dbhost};dbname={$dbname}", $dbuser, $dbpass);
      $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);  
     // $pdo->setAttribute(PDO::ATTR_AUTOCOMMIT,false);  
     // $pdo->beginTransaction();
      
      $sql="select idaluno, idunidade, nrdiatreino, idtreino,dtinicio from historicotreino where ";
      $sql.=" idaluno = " . $idaluno;
      $sql.=" and idunidade = " . $idunidade;
      $sql.=" and sttreino = 'A'";
      
      $con = new PDO("mysql:host={$dbhost};dbname={$dbname}", $dbuser, $dbpass);
      $con->query('SET NAMES utf8');
      $consulta = $con->query($sql);

      while($c = $consulta->fetch(PDO::FETCH_OBJ)) {  
            $row_array['IDTREINO'] = $c->idtreino;
           $row_array['IDALUNO'] = $c->idaluno;
           $row_array['IDUNIDADE'] = $c->idunidade;
           $row_array['DTINICIO']= $c->dtinicio;
           $row_array['NRDIATREINO']= $c->nrdiatreino;
           IF ($c->nrdiatreino==1){
               $ficha="A";
           }
           IF ($c->nrdiatreino==2){
               $ficha="B";
           }
           IF ($c->nrdiatreino==3){
               $ficha="C";
           }
           IF ($c->nrdiatreino==4){
               $ficha="D";
           }
           IF ($c->nrdiatreino==5){
               $ficha="E";
           }
           IF ($c->nrdiatreino==6){
               $ficha="F";
           }
           IF ($c->nrdiatreino==7){
               $ficha="G";
           }
           IF ($c->nrdiatreino==8){
               $ficha="H";
           }
           IF ($c->nrdiatreino==9){
               $ficha="I";
           }
           IF ($c->nrdiatreino==10){
               $ficha="J";
           }
           $row_array['FICHATREINO']= $ficha;
           array_push($return_arr,$row_array);
      }
    printf( json_encode($return_arr));      


     // printf('Registro efetuado com sucesso');
 }

} catch (PDOException $exception) {
    //$pdo->rollBack();
    printf('Não foi possível realizar a operação: %s' , $exception);
    
 }
 
?>