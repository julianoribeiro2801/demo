<?php
/*APLICATIVO GERADOR CRUD ExtJs/Php/Mysql.
 Desenvolvido por AMARILDO DIAS
 dias@dias.adm.br
 http://www.dias.adm.br 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, version 3.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 <http://www.gnu.org/licenses/>.
*/
include "../Config.php";
//conecta
$link = @mysql_connect($local_server,$usuario_server,$senha_server)or die ('{"success":"false"}');
//Seleciona DB
mysql_select_db($dbname, $link)or die ('{"success":"false"}');
//Varariaveis do filtro
$campo1 = $_GET['Field1'];
$cond1 = $_GET['Cond1'];
$val1 = $_GET['Valor1'];
$tablename = $_GET['Tablename'];
$chave = $_GET['Chave'];
$colunas = $_GET['Campos'];

//Limitando quantidade de registros retornados
$start = $_GET['start'];;
$limit = $_GET['limit'];;
$lim = "$start, $limit";

//Monta Consulta SQL
mysql_query("SET NAMES 'utf8';");
$sql = new SqlQuery();
if (isset($colunas)){
	if ($php_ver == "php5_x"){	
		$colun = json_decode($colunas);
	}
	else {		
		$c = json_decode(stripslashes(json_encode($colunas)));
		$colun = json_decode($c);
	} 
	foreach ($colun as $value) {
   		$sql -> Add("col", $value);
	}
}
$sql -> Add("table",$tablename);	
	if ($val1<>""){
		$sql -> Add("where","$campo1 $cond1 '$val1'");
	};	
$sql -> Add("orderby",$campo1);

$query = $sql->toSql();

//Gera PDF
if ($link){
//NUMERO RANDOMICO PARA O RELATORIO
$rel = "relatorio_$tablename".rand(0, 99999).".pdf";
//T�TULO DO RELAT�RIO                                     
$titulo      =  "Relatorio PDF";                 
//LOGO QUE SER� COLOCADO NO RELAT�RIO                     
$imagem      =  "./logo.png";                      
//ENDERE�O DA BIBLIOTECA FPDF                             
$end_fpdf    =  "./Fpdf";             
//NUMERO DE RESULTADOS POR P�GINA                         
$por_pagina  =  25;                                       
//ENDERE�O ONDE SER� GERADO O PDF                         
$end_final   = "./RelGer/$rel"; //relatorio.pdf"; 
//TIPO DO PDF GERADO  F-> SALVA NO ENDERE�O ESPECIFICADO NA VAR END_FINAL     
$tipo_pdf    =  "F";                                      

//EXECUTA QUERY
$sqlpdf    =   mysql_query($query, $link);
$rowpdf    =   mysql_num_rows($sqlpdf);           

//VERIFICA SE RETORNOU ALGUMA LINHA
if(!$rowpdf) { echo "Nao retornou nenhum registro"; die; }

//CALCULA QUANTAS P�GINAS SERAO NECESSARIAS
$paginas   =  ceil($rowpdf/$por_pagina);        

//PREPARA PARA GERAR O PDF
define("FPDF_FONTPATH", "$end_fpdf/font/");
require_once("$end_fpdf/fpdf.php");        
$pdf   =   new FPDF('L');

//INICIALIZA AS VARI�VEIS
$linha_atual =  0;
$inicio      =  0;

//P�GINAS

for($x=1; $x<=$paginas; $x++) {
   //VERIFICA QUANT LINHAS
   $inicio      =  $linha_atual;
   $fim         =  $linha_atual + $por_pagina;
   if($fim > $rowpdf) $fim = $rowpdf;
   
   //$pdf->SetMargins(1.0, 1.0, 1.0); 
      
   $pdf->Open();                    
   $pdf->AddPage();                 
   $pdf->SetFont("Arial", "B", 7); 
   
   //MONTA O CABE�ALHO              
   $pdf->Image($imagem, 10, 5);
   $pdf->Ln(1);
   $pdf->Cell(260, 5, "P�gina $x de $paginas", 0, 1, 'R');
   $pdf->Ln(8);
   $pdf->Cell(0,5, $tablename, 0,1,'C');          
   
   //QUEBRA DE LINHA
   $pdf->Ln(2);
 
   //CABECALHO DAS COLUNAS
   $c = 0;
   $j = 0;
   $col = sizeof($colun)-1;
   foreach ($colun as $value) {
   	  $pdf->Cell(45, 5, $value, 1, $c, 'L');
   	  $j++;
   	  if($j==$col) $c=1;
   }
    
   //EXIBE OS REGISTROS
   $pdf->SetFont("Arial", "", 7);
   $quant = sizeof($colun)-1;
   for ($i=$inicio; $i<$fim; $i++) {
   	$f = 0;
   	$p = 0;
    foreach ($colun as $value) {
   	  $pdf->Cell(45, 5, mysql_result($sqlpdf, $i, $value), 1, $p, 'L');      
	  $f++;
	  if($f==$quant) $p=1;
    }
   $linha_atual++;
   
   }//FECHA FOR(REGISTROS - i)
   
}//FECHA FOR(PAGINAS - x)

//SAIDA DO PDF
$pdf->Output("$end_final", "$tipo_pdf");

//echo '{"sql" : '.json_encode($sql->toSql()).'}';
echo json_encode($rel);
}//FECHA IF $link
?>


<?php
class SqlQuery {
		var $Fields = array ("col", "table", "where", "value", "limit");
		var $Sql = array();
		function Add($method,$param){
			if(in_array(strtolower($method),$this->Fields)) 
			{
			$this->Sql[$method][] = $param;
			}
		}
		function addWhereMulti($column, $values, $possitive = TRUE) {	
			foreach ($values as $value)
				$where[] = "$column = '$value'";	
				$sqlWhere = implode($where, " OR ");
			if (!$possitive)
				$sqlWhere = "NOT (".$sqlWhere.") OR $column IS NULL";;	
			$this->addWhere($sqlWhere);	
		}
		function toSql() {	
			if (!($this->Sql["table"])) {
				return FALSE;
			}	
			$query = FALSE;	
					$query = "SELECT `".implode($this->Sql["col"], "`, `")."` FROM ".implode($this->Sql["table"], " ");
				if (isset ($this->Sql["where"]))
					$query .= " WHERE (".implode($this->Sql["where"], ") AND (").")";
				if (isset ($this->Sql["orderby"]))
					$query .= " ORDER BY ".implode($this->Sql["orderby"], ", ");
				if (isset ($this->Sql["limit"]))
					$query .= " LIMIT ".implode($this->Sql["limit"], ", ");
			return $query;
	  }
  }
?>