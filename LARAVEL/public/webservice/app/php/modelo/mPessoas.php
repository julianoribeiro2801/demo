<?php
include_once "conexao.php";

class mPessoas extends conexao
{
    private $IDPESSOA;
    private $NOME;
    private $NOMEMAE;
    private $NOMEPAI;
    private $RG;
    private $CPF;
    private $DATANASC;
    private $IDESTADOCIVIL;
    private $SEXO;
    private $IDCIDADE;
    private $ENDERECO;
    private $NUMERO;
    private $BAIRRO;
    private $CEP;
    private $CODSUS;
    // Setters
    public function setId($valor)
    {
        $this->IDPESSOA = $valor;
    }

    public function setTitulo($valor)
    {
        $this->NOME = $valor;
    }
    public function setNomeMae($valor)
    {
        $this->NOMEMAE = $valor;
    }
    public function setNomePai($valor)
    {
        $this->NOMEPAI = $valor;
    }
    public function setRG($valor)
    {
        $this->RG = $valor;
    }
    public function setCPF($valor)
    {
        $this->CPF = $valor;
    }
    public function setDataNascimento($valor)
    {
         $this->DATANASC = $this->ConvertData("BRA", "EUA", $valor);
    }
    public function setEstadocivil($valor)
    {
        $this->IDESTADOCIVIL = $valor;
    }
    public function setSexo($valor)
    {
        $this->SEXO = $valor;
    }
    public function setIdcidade($valor)
    {
        $this->IDCIDADE = $valor;
    }
    public function setEndereco($valor)
    {
        $this->ENDERECO = $valor;
    }
    public function setNumero($valor)
    {
        $this->NUMERO = $valor;
    }
    public function setBairro($valor)
    {
        $this->BAIRRO = $valor;
    }
    public function setCep($valor)
    {
        $this->CEP = $valor;
    }
    public function setCodsus($valor)
    {
        $this->CODSUS = $valor;
    }
    
    // Getters
    public function getId()
    {
        return $this->IDPESSOA;
    }

    public function getTitulo()
    {
        return $this->NOME;
    }
    public function getNomeMae()
    {
        return $this->NOMEMAE;
    }
    public function getNomePai()
    {
        return $this->NOMEPAI;
    }
    public function getRG()
    {
        return $this->RG;
    }
    public function getCPF()
    {
        return $this->CPF;
    }
    public function getDataNascimento($converter = true)
    {
        if ($converter == true) {
            return $this->DATANASC;
        } else {
            return $this->ConvertData("EUA", "BRA", $this->DATANASC);
        }
    }    
    public function getEstadocivil()
    {
        return $this->IDESTADOCIVIL;
    }
    public function getSexo()
    {
        return $this->SEXO;
    }
    public function getIdcidade()
    {
        return $this->IDCIDADE;
    }
    public function getEndereco()
    {
        return $this->ENDERECO;
    }
    public function getNumero()
    {
        return $this->NUMERO;
    }
    public function getBairro()
    {
        return $this->BAIRRO;
    }
    public function getCep()
    {
        return $this->CEP;
    }
    public function getCodsus()
    {
        return $this->CODSUS;
    }
    
}

?>