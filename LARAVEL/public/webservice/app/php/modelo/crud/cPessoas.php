<?php
include_once "../modelo/mPessoas.php";

class cPessoas extends mPessoas
{
    protected $sqlInsert = "INSERT INTO PESSOA (IDPESSOA,NOME,NOMEMAE,NOMEPAI, RG, CPF,DATANASC,IDESTADOCIVIL,SEXO, IDCIDADE, ENDERECO, NUMERO, BAIRRO,CEP,CODSUS) VALUES (GEN_ID(SEQ_PESSOA,1),'%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')";
    protected $sqlUpdate = "UPDATE PESSOA SET NOME = '%s', NOMEMAE = '%s', NOMEPAI = '%s', RG= '%s', CPF = '%s', DATANASC= '%s', IDESTADOCIVIL = '%s', SEXO = '%s', IDCIDADE = '%s', ENDERECO = '%s', NUMERO = '%s', BAIRRO = '%s',CEP = '%s',CODSUS = '%s' WHERE IDPESSOA = '%s'";
    protected $sqlDelete = "DELETE FROM PESSOA WHERE (IDPESSOA = '%s')";
    protected $sqlSelect = "SELECT %s C.IDPESSOA,C.NOME,C.NOMEMAE, COALESCE(NOMEPAI,'') as NOMEPAI, C.RG, C.CPF, C.DATANASC, C.IDESTADOCIVIL, C.SEXO,C.IDCIDADE, C.ENDERECO, C.NUMERO, C.BAIRRO,C.CEP,C.CODSUS FROM PESSOA AS C %s %s";
    protected $sqlSelectJson = "SELECT * FROM Alunos1 WHERE alunoID < 1000";
//    protected $sqlSelectJson = "SELECT C.IDPESSOA,C.NOME,C.NOMEMAE, COALESCE(NOMEPAI,'') as NOMEPAI, C.RG, C.CPF, C.DATANASC, C.IDESTADOCIVIL, C.SEXO,C.IDCIDADE, C.ENDERECO, C.NUMERO, C.BAIRRO,C.CEP,C.CODSUS FROM PESSOA AS C WHERE IDPESSOA < 1000";
    protected $sqlSelectSimple = "SELECT C.IDPESSOA,C.NOME,C.NOMEMAE, COALESCE(NOMEPAI,'') as NOMEPAI, C.RG, C.CPF, C.DATANASC, C.IDESTADOCIVIL, C.SEXO,C.IDCIDADE, C.ENDERECO, C.NUMERO, C.BAIRRO,C.CEP,C.CODSUS FROM PESSOA AS C, MUNICIPIO AS M %s %s %s %s";
////////////////////////
    protected $sqlSelectId = "SELECT C.IDPESSOA,C.NOME,C.NOMEMAE, COALESCE(NOMEPAI,'') as NOMEPAI, C.RG, C.CPF, C.DATANASC, C.IDESTADOCIVIL, C.SEXO,C.IDCIDADE, C.ENDERECO, C.NUMERO, C.BAIRRO,C.CEP,C.CODSUS FROM PESSOA AS C, MUNICIPIO AS M %s %s";
////////////////////////
    protected $sqlSelectLike = "SELECT C.IDPESSOA,C.NOME,C.NOMEMAE, COALESCE(NOMEPAI,'') as NOMEPAI, C.RG, C.CPF, C.DATANASC, C.IDESTADOCIVIL, C.SEXO,C.IDCIDADE, C.ENDERECO, C.NUMERO, C.BAIRRO,C.CEP,C.CODSUS FROM PESSOA AS C, MUNICIPIO AS M %s AND M.IBGE = C.IDCIDADE %s";
    protected $sqlSelectTot = "SELECT C.IDPESSOA,C.NOME,C.NOMEMAE, COALESCE(NOMEPAI,'') as NOMEPAI, C.RG, C.CPF, C.DATANASC, C.IDESTADOCIVIL, C.SEXO,C.IDCIDADE, C.ENDERECO, C.NUMERO, C.BAIRRO,C.CEP,C.CODSUS FROM PESSOA AS C , MUNICIPIO AS M %s AND M.IBGE = C.IDCIDADE %s";
    protected $sqlSelectMax = "SELECT IDPESSOA,NOME,NOMEMAE, COALESCE(NOMEPAI,'') as NOMEPAI, RG, CPF, DATANASC, IDESTADOCIVIL, SEXO,IDCIDADE, ENDERECO, NUMERO, BAIRRO,CEP,CODSUS FROM PESSOA WHERE IDPESSOA = (SELECT MAX(IDPESSOA) FROM PESSOA WHERE IDCIDADE = %s )";

    public function insertPessoa()
    {
        $sql = sprintf($this->sqlInsert, $this->getTitulo(),$this->getNomeMae(),$this->getNomePai(),$this->getRG(),$this->getCPF(),$this->getDataNascimento(),$this->getEstadocivil(),$this->getSexo(),$this->getIdcidade(),$this->getEndereco(),$this->getNumero(),$this->getBairro(),$this->getCep(),$this->getCodsus());
        return $this->RunQuery($sql);
    }
    public function update()
    {
        $sql = sprintf($this->sqlUpdate, $this->getTitulo(),$this->getNomeMae(),$this->getNomePai(),$this->getRG(),$this->getCPF(),$this->getDataNascimento(),$this->getEstadocivil(),$this->getSexo(),$this->getIdcidade(),$this->getEndereco(),$this->getNumero(),$this->getBairro(),$this->getCep(),$this->getCodsus(), $this->getId());
        return $this->RunQuery($sql);
    }

    public function delete()
    {
        $sql = sprintf($this->sqlDelete, $this->getId());
        return $this->RunQuery($sql);
    }
    public function select($more, $where = '' , $order = '')
    {
        $where = (empty($where) ? "" : " WHERE " . $where);
        $order = (empty($order) ? "" : " ORDER BY " . $order);

        $sql = sprintf($this->sqlSelect,$more,$where, $order);
        return $this->RunSelect($sql);
    }
    public function selectJson()
    {

        $sql = sprintf($this->sqlSelectJson,"","", "");
        return $this->RunSelect($sql);
    }
    public function selectMaximo($where)
//    public function selectMaximo($more, $where = '' , $order = '')
    {
        $where = (empty($where) ? "" : " " . $where);
        $order = (empty($order) ? "" : " ORDER BY " . $order);

        $sql = sprintf($this->sqlSelectMax,$where);
//        $sql = sprintf($this->sqlSelectMax,$more,$where, $order);
        return $this->RunSelect($sql);
    }

    public function selectId($where, $order)
    {
        $where = (empty($where) ? "" : " WHERE " . $where);
        $order = (empty($order) ? "" : " ORDER BY " . $order);

        $sql = sprintf($this->sqlSelectId,$where,$order);
        return $this->RunSelect($sql);
    }
    public function selectSimple($where,$cond1,$cond2, $order)
    {
        $where = (empty($where) ? "" : " WHERE " . $where);
        $order = (empty($order) ? "" : " ORDER BY " . $order);

        $sql = sprintf($this->sqlSelectSimple,$where,$cond1,$cond2, $order);
        return $this->RunSelect($sql);
    }
    public function selectLike($where = '' , $order = '')
    {
        $where = (empty($where) ? "" : " WHERE " . $where);
        $order = (empty($order) ? "" : " ORDER BY " . $order);

        $sql = sprintf($this->sqlSelectLike,$where, $order);
        return $this->RunSelect($sql);
    }
    public function selectTot( $where = '' , $order = '')
    {
        $where = (empty($where) ? "" : " WHERE " . $where);
        $order = (empty($order) ? "" : " ORDER BY " . $order);

        $sql = sprintf($this->sqlSelectTot,$where, $order);
        return $this->RunSelect($sql);
    }

    public function ultimo_registro($where)
    {
        return $this->selectMaximo($where);
    }
}

?>