<?php

class conexao
{
    /*private $host = "localhost";
    private $user = "SYSDBA";
    private $pass = "masterkey";
    private $db = "127.0.0.1:C:\\SISTEMASTURBO\\BANCODEDADOS\\ASTURBO.GDB";  */

    private $host = "mysql09.academiaaquafit.hospedagemdesites.ws";
    private $user = "academiaaquafit7";
    private $pass = "Aquafit123";
    private $db = "academiaaquafit7";      
//    private $host = "localhost";
//    private $user = "SYSDBA";
//    private $pass = "masterkey";
//    private $db = "127.0.0.1:C:\\SISTEMASTURBO\\BANCODEDADOS\\ASTURBO.GDB";  

//    private $host = "localhost";
//    private $user = "root";
//    private $pass = "";
//    private $db = "avon";  

    public function Connect()
    {
//        return new PDO("firebird:dbname=$this->db", $this->user, $this->pass);
        return new PDO("mysql:host=$this->host; dbname=$this->db", $this->user, $this->pass);
        
    }

    public function RunQuery($sql)
    {
        $res = $this->Connect()->prepare($sql);
        $ret = $res->execute();

        if ($res->errorCode() > 0) {
            print_r($res->errorInfo());
        }
        
        return $ret;
    }

    public function ConvertDataHora($formatoDE, $formatoPARA, $valor)
    {
        if (!empty($valor)) {
            if ($formatoDE == "EUA") {
                if ($formatoPARA == "BRA") {
                    $data_hora = explode(" ", $valor);

                    $listData = explode("/", $data_hora[0]);
                    $listHora = explode(":", $data_hora[1]);

                    $ret = $listData[2] . "/" . $listData[1] . "/" . $listData[0] . " " . $listHora[0] . ":" . $listHora[1] . ":" . $listHora[2];
                    return $ret;
                }
            }
            if ($formatoDE == "BRA") {
                if ($formatoPARA == "EUA") {
                    $data_hora = explode(" ", $valor);

                    $listData = explode("/", $data_hora[0]);
                    $listHora = explode(":", $data_hora[1]);

                    $ret = $listData[2] . "-" . $listData[1] . "-" . $listData[0] . " " . $listHora[0] . ":" . $listHora[1] . ":" . $listHora[2];
                    return $ret;
                }
            }
        }
        return null;
    }

    public function ConvertData($formatoDE, $formatoPARA, $valor)
    {
        if (!empty($valor)) {
            if ($formatoDE == "EUA") {
                if ($formatoPARA == "BRA") {
                    $list = explode("-", $valor);
                    $ret = $list[2] . "/" . $list[1] . "/" . $list[0];
                    return $ret;
                }
            }
            if ($formatoDE == "BRA") {
                if ($formatoPARA == "EUA") {
                    $list = explode("/", $valor);
                    $ret = $list[2] . "-" . $list[1] . "-" . $list[0];
                    return $ret;
                }
            }
        }
        return null;
    }
    public function FormataMoeda($valor)
    {
        $valor = str_replace("R$ ", "", $valor);
        $valor = str_replace(".", "", $valor);
        $valor = str_replace(",", ".", $valor);
        return $valor;
    }
    public function RunSelect($sql)
    {
        $res = $this->Connect()->prepare($sql);
        $res->execute();
        return $res->fetchAll(PDO::FETCH_ASSOC ) ;
//        return $res->fetchAll(PDO::FETCH_ASSOC);
    }
}

?>