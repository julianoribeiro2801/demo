<?php
header("Content-Type: text/html;  charset=utf-8",true);
include_once "../modelo/crud/cPessoas.php";

$acao = $_GET['acao'];


if ($acao == "readTudo") {

    $dados = new cPessoas();

    $where = " ";
    $order = " ";

    $retorno = $dados->selectJson();
    $dados="";
    foreach ($retorno as $chave => $valor) {
        $retornoDados[$chave] = $valor;
        $dados[$chave]["alunoID"]= $valor['alunoID']; 
        $dados[$chave]["alunoNome"]=  $valor['alunoNome'] ;
        $dados[$chave]["alunoEndereco"]= $valor['alunoEndereco'];
        $dados[$chave]["alunoBairro"]= $valor['alunoBairro'];
        $dados[$chave]["alunoCidade"]= $valor['alunoCidade'];
        $dados[$chave]["alunoCEP"]= $valor['alunoCEP'];
        $dados[$chave]["alunoEstado"]= $valor['alunoEstado'];
        $dados[$chave]["alunoTelefone"]= $valor['alunoTelefone'];
        $dados[$chave]["alunoCelular"]= $valor['alunoCelular'];
        $dados[$chave]["alunoSexo"]= $valor['alunoSexo'];
        $dados[$chave]["alunoCPF"]= $valor['alunoCPF'];
        $dados[$chave]["alunoIdentidade"]= $valor['alunoIdentidade'];
        $dados[$chave]["alunoUFIdentidade"]= $valor['alunoUFIdentidade'];
        $dados[$chave]["alunoEmail"]= $valor['alunoEmail'];
        $dados[$chave]["alunoDataNascimento"]= $valor['alunoDataNascimento'];
        $dados[$chave]["alunoPai"]= $valor['alunoPai'];
        $dados[$chave]["alunoMae"]= $valor['alunoMae'];
        $dados[$chave]["alunoObjetivo"]= $valor['alunoObjetivo'];
        $dados[$chave]["alunoProfissao"]= $valor['alunoProfissao'];
        $dados[$chave]["alunoEstadoCivil"]= $valor['alunoEstadoCivil'];
        $dados[$chave]["alunoEmpresa"]= $valor['alunoEmpresa'];
        $dados[$chave]["alunoTelefoneEmpresa"]= $valor['alunoTelefoneEmpresa'];
        $dados[$chave]["alunoResponsavel"]= $valor['alunoResponsavel'];
        $dados[$chave]["alunoTelefoneResponsavel"]= $valor['alunoTelefoneResponsavel'];
        $dados[$chave]["alunoDataExame"]= $valor['alunoDataExame'];
        $dados[$chave]["alunoDataAvaliacao"]= $valor['alunoDataAvaliacao'];
        $dados[$chave]["alunoTemCartao"]= $valor['alunoTemCartao'];
        $dados[$chave]["alunoCartao"]= $valor['alunoCartao'];
        $dados[$chave]["alunoExcluido"]= $valor['alunoExcluido'];
        $dados[$chave]["alunoSenha"]= $valor['alunoSenha'];
        $dados[$chave]["alunoObs"]= $valor['alunoObs'];
        $dados[$chave]["alunoDtCadastro"]= $valor['alunoDtCadastro'];
        $dados[$chave]["alunoSoubeAcademia"]= $valor['alunoSoubeAcademia'];
        $dados[$chave]["alunoHorario"]= $valor['alunoHorario'];
        $dados[$chave]["funcID"]= $valor['funcID']; 
        $dados[$chave]["alunoCatracaReentrada"]= $valor['alunoCatracaReentrada'];
        $dados[$chave]["alunoCatracaMinutos"]= $valor['alunoCatracaMinutos'];
        $dados[$chave]["alunoCatracaSegundos"]= $valor['alunoCatracaSegundos'];
        $dados[$chave]["alunoCatracaSemana"]= $valor['alunoCatracaSemana'];
        $dados[$chave]["alunoCatracaVezesSemana"]= $valor['alunoCatracaVezesSemana'];
        $dados[$chave]["alunoCatracaDia"]= $valor['alunoCatracaDia']; 
        $dados[$chave]["alunoCatracaVezesDia"]= $valor['alunoCatracaVezesDia'];
        $dados[$chave]["alunoDigitosCelular"]= $valor['alunoDigitosCelular'];       
        echo $dados[$chave]["alunoNome"];
    }       
    //$total_registros = sizeof($dados->selectTot($where, $order));
    $total_registros=0;
    echo json_encode(array("Pessoas" => $dados));
//    echo json_encode(array("retorno" => $retorno));

    //exit();
}

if ($acao == "read") {
    $dados = new cPessoas();

    $more = " FIRST " . $_POST['limit'] . " SKIP " . $_POST['start'];
    $where = "";
    $order = " C.NOME ASC ";

    $retorno = $dados->select($more, $where, $order);
    $total_registros = sizeof($dados->selectTot($where, $order));

    echo json_encode(array("success" => true, "retorno" => $retorno, "total" => $total_registros));
    exit();
}
if ($acao == "readLike") {
    $dados = new cPessoas();

    $more = " FIRST " . $_POST['limit'] . " SKIP " . $_POST['start'];
    $where = "";
    $order = " C.NOME ASC ";

    $retorno = $dados->select($more, $where, $order);
    $total_registros = sizeof($dados->selectTot($where, $order));

    echo json_encode(array("success" => true, "retorno" => $retorno, "total" => $total_registros));
    exit();
}
if ($acao == "readSimple") {
    $dados = new cPessoas();

    //$more = " FIRST " . $_POST['limit'] . " SKIP " . $_POST['start'];
    $where = " IDPESSOA < 100";
    $order = " C.NOME ASC ";

    $retorno = $dados->selectSimple($where, $order);
    $total_registros = sizeof($dados->selectTot($where, $order));

    echo json_encode(array("success" => true, "retorno" => $retorno, "total" => $total_registros));
    exit();
}

if ($acao == "create") {
    $dados = new cPessoas();
    $dadosPOST = json_decode($_POST['retorno']);

    $dados->setNome(strtoupper($dadosPOST->NOME));

    $dados->insertPessoa();

    $retorno = $dados->ultimo_registro($dadosPOST->IDCIDADE);

    echo json_encode(array("success" => true, "retorno" => $retorno));
    exit();
}

if ($acao == "update") {
    $dados = new cPessoas();
    $dadosPOST = json_decode($_POST['retorno']);


    
    $dados->setTitulo(strtoupper($dadosPOST->NOME));
    $dados->setNomeMae(strtoupper($dadosPOST->NOMEMAE));
    $dados->setNomePai(strtoupper($dadosPOST->NOMEPAI));
    $dados->setRG(strtoupper($dadosPOST->RG));
    $dados->setCPF(strtoupper($dadosPOST->CPF));
    $dados->setDataNascimento($dadosPOST->DATANASC); 
    $dados->setEstadocivil($dadosPOST->IDESTADOCIVIL); 
    $dados->setSexo($dadosPOST->SEXO); 
    $dados->setIdcidade($dadosPOST->IDCIDADE); 
    $dados->setEndereco($dadosPOST->ENDERECO); 
    $dados->setNumero($dadosPOST->NUMERO); 
    $dados->setBairro($dadosPOST->BAIRRO); 
    $dados->setCep($dadosPOST->CEP); 
    $dados->setCodSus($dadosPOST->CODSUS); 
    
    
    $dados->setId($dadosPOST->IDPESSOA);

    $dados->update();

    echo json_encode(array("success" => true, "retorno" => $dadosPOST));
    exit();
}

if ($acao == "destroy") {
    $dados = new cPessoas();
    $dadosPOST = json_decode($_POST['retorno']);

    if (is_array($dadosPOST)) {

        foreach ($dadosPOST as $valorPOST) {
            $dados->setId($valorPOST->IDPESSOA);
            $dados->delete();
        }

    } else {
        $dados->setId($dadosPOST->IDPESSOA);
        $dados->delete();
    }

    echo json_encode(array("success" => true));
    exit();
}

?>
