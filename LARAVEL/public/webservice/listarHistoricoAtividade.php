<?php
 $return_arr = array();

 if (isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
    }
   // Access-Control headers are received during OPTIONS requests
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
 
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         
 
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            header("Access-Control-Allow-Headers:{$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
 
        exit(0);
    }
 
 
    $postdata = file_get_contents("php://input");
    
    
  try {  
   if (isset($postdata)) {
      $request = json_decode($postdata);


      $atividade = $request->idatividade;
      $aluno = $request->idaluno;
      $unidade= $request->idunidade;
       
   
      
               
      include "conecta.php";     
      include "funcoes.php"; 

      $pdo = new PDO("mysql:host={$dbhost};dbname={$dbname}", $dbuser, $dbpass);
      $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);  
     // $pdo->setAttribute(PDO::ATTR_AUTOCOMMIT,false);  
     // $pdo->beginTransaction();
      
      $sql="select idatividade, idaluno, idunidade, dtregistro, qtatividade from movatividade where ";
      $sql .= " idatividade = " . $atividade;
      $sql .= " and idaluno = " . $aluno;
      $sql .= " and idunidade = " . $unidade;
      $sql .= " and stmovatividade = 'F' ";
      $sql .= " order by dtregistro desc";
      

      $con = new PDO("mysql:host={$dbhost};dbname={$dbname}", $dbuser, $dbpass);
      $con->query('SET NAMES utf8');
      $consulta = $con->query($sql);

      while($c = $consulta->fetch(PDO::FETCH_OBJ)) {  
           $row_array['IDATIVIDADE'] = $c->idatividade;
           $row_array['IDALUNO'] = $c->idaluno;
           $row_array['IDUNIDADE']= $c->idunidade;
           $row_array['DTREGISTRO']= date("d/m/Y H:i", strtotime($c->dtregistro));           
           
           //$row_array['QTATIVIDADE']= str_replace(".",":",$c->qtatividade);
           
           if ($c->idatividade==1){
             $numer=$c->qtatividade;    
             $row_array["QTATIVIDADE"]= str_replace(".",",",   number_format($numer,1));
           }else{
             if ($c->idatividade==3){
                $row_array["QTATIVIDADE"]=  number_format($c->qtatividade,0 , ',', '.');
             }else {
                if ($c->idatividade==4){
                   $row_array["QTATIVIDADE"]=  number_format($c->qtatividade,2, ':', ',');
                } else {    
                  if ($c->idatividade==5){
                      $row_array["QTATIVIDADE"]=  number_format($c->qtatividade,2, ',', '.');
                   } else {    
                      if ($c->idatividade==2){
                          $row_array["QTATIVIDADE"]=  number_format($c->qtatividade,0, ',', '');
                      } else {    
                          $row_array["QTATIVIDADE"]= number_format($c->qtatividade,0,'',',');         
                      }      
                   }   
                }   
             }   
           }
           
                   
           $row_array['DIASEMANA']= diasemanaAbr(date("Y-m-d", strtotime($c->dtregistro)));
           array_push($return_arr,$row_array);
      }
     

    printf( json_encode($return_arr));      


     // printf('Registro efetuado com sucesso');
 }

} catch (PDOException $exception) {
    //$pdo->rollBack();
    printf('Não foi possível realizar a operação: %s' , $exception);
    
 }
 
?>