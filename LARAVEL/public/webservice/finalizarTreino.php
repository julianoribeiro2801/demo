<?php
 $return_arr = array();

 if (isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
    }
    date_default_timezone_set('America/Sao_Paulo');    
   // Access-Control headers are received during OPTIONS requests
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
 
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         
 
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
 
        exit(0);
    }
 
 
    $postdata = file_get_contents("php://input");

    
  try {  
   if (isset($postdata)) {
       
      date_default_timezone_set('America/Sao_Paulo');
      
      $request = json_decode($postdata);

      $idaluno = $request->idaluno;
      $unidade = $request->idunidade;
      $prescricao = $request->idtreino;
      $fichatreino = $request->fichatreino;
      $tempo_parado = $request->tempo_parado;

      $nrdiatreino=0;
      if($fichatreino=="A"){
         $nrdiatreino=1;
      }
      if($fichatreino=="B"){
         $nrdiatreino=2;
      }
      if($fichatreino=="C"){
         $nrdiatreino=3;
      }
      if($fichatreino=="D"){
         $nrdiatreino=4;
      }
      if($fichatreino=="E"){
         $nrdiatreino=5;
      }
      if($fichatreino=="F"){
        $nrdiatreino=6;
      }      
      if($fichatreino=="G"){
        $nrdiatreino=7;
      }      
      if($fichatreino=="H"){
        $nrdiatreino=8;
      }      
      if($fichatreino=="I"){
        $nrdiatreino=9;
      }      
      if($fichatreino=="J"){
        $nrdiatreino=10;
      }      
      
       
               
      include "conecta.php";        

      $pdo = new PDO("mysql:host={$dbhost};dbname={$dbname}", $dbuser, $dbpass);
      $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);  
     // $pdo->setAttribute(PDO::ATTR_AUTOCOMMIT,false);  
     // $pdo->beginTransaction();
      
      
      $varTon=0;
      
      
      $sql="SELECT sum( s.qtrepeticao * s.qtcarga ) as ton,h.dtinicio as di, h.dtfim as df FROM serietreino s,historicotreino h WHERE ";
      $sql .= " s.idaluno = " . $idaluno;
      $sql .= " and s.idunidade = " . $unidade;
      $sql .= " and s.idprescricao = " . $prescricao;
      $sql .= " and h.sttreino = 'A'" ;              
      $sql .= " and s.nrdiatreino = " . $nrdiatreino;  
      
      $sql .= " and s.dtregistro = (select max(dtregistro) from";  
      $sql .= " serietreino x where x.idaluno = " . $idaluno;  
      $sql .= " and x.idunidade = " . $unidade;
      $sql .= " and x.idprescricao = " . $prescricao . " and nrdiatreino = " . $nrdiatreino . " )";
      
      $sql .= " and s.idaluno = h.idaluno";
      $sql .= " and s.idunidade = h.idunidade";
      $sql .= " and s.idprescricao = h.idtreino";
      $sql .= " and s.nrdiatreino = h.nrdiatreino";  

   
      $con = new PDO("mysql:host={$dbhost};dbname={$dbname}", $dbuser, $dbpass);
      $con->query('SET NAMES utf8');
      $consulta = $con->query($sql);
      
      while($c = $consulta->fetch(PDO::FETCH_OBJ)) {
                        

           $varTon = $c->ton / 1000;
           
      }  
     $dataIni=date('Y-m-d H:i:s') ; 
     
     $sql="select h.dtinicio as di, h.dtfim as df FROM historicotreino as h";
     $sql .= " where h.idunidade = " . $unidade;
     $sql .= " and h.idtreino = " . $prescricao;
     $sql .= " and h.sttreino = 'A'" ;              
     $sql .= " and h.nrdiatreino = " . $nrdiatreino;  
     $sql .= " and h.idaluno = " . $idaluno;  
        
     $con = new PDO("mysql:host={$dbhost};dbname={$dbname}", $dbuser, $dbpass);
     $con->query('SET NAMES utf8');
     $consulta = $con->query($sql);
     $duracao=0;
     while($c = $consulta->fetch(PDO::FETCH_OBJ)) {
        
         $dataIni = $c->di;

     }
    
//////////////////////////////busca duracao do treino        
           /*$sql="select l.ficha_series,l.ficha_repeticoes,e.qtduracao"
           . " from treino_ficha as l, treino_musculacao t,"
           . " exercicio as e  where t.unidade_id = " . $unidade . " and t.aluno_id = " 
           . $idaluno. " and l.treino_id = " . $prescricao . " and l.ficha_letra = '" . $fichatreino 
           . "' and e.id = l.exercicio_id and t.treino_id = l.treino_id order by l.ficha_letra";*/

           $sql="select tempoestimado from tempo_estimado where letra = '" . $fichatreino . "'"  ;
           $sql .= " and treino_id = " . $prescricao ;
           $sql .= " and client_id = " . $idaluno;
           
     
           $con = new PDO("mysql:host={$dbhost};dbname={$dbname}", $dbuser, $dbpass);
           $con->query('SET NAMES utf8');
           $consulta = $con->query($sql);
           $duracao="";
           while($c = $consulta->fetch(PDO::FETCH_OBJ)) {
               //$duracao=$duracao + ($c->qtduracao * $c->ficha_repeticoes * $c->ficha_series)  ;
               //$dura=explode(":", $c->tempoestimado);;
               if ($c->tempoestimado=='0'){
                   $duracao= "00" . ":00:00";
               } else {
                    $duracao= "00" . ":" . $c->tempoestimado;
               }    

           }
           
           $converter = $duracao;
//           $converter = date('H:i:s',mktime(0,0,$duracao,15,03,2013));//Converter os segundos em no formato mm:ss; 
////////////////////////////////////////////////////      
        $dataFim = date('Y-m-d H:i:s') ;       
        $horaFim = date('H:i:s') ;       
        
        $dateStart = new DateTime($dataIni);
        $dateNow   = new DateTime($dataFim);
        
        $dateDiff = $dateStart->diff($dateNow);
        //$result = ((($dateDiff->d * 24) * 60) * 60) + (($dateDiff->h * 60) * 60) + ($dateDiff->i * 60) + $dateDiff->s;
        $result = sprintf("%02d",(($dateDiff->d * 24) + $dateDiff->h))  . ":" . sprintf("%02d",($dateDiff->i)) . ":" . sprintf("%02d",$dateDiff->s);
        $tpGasto = $result;

        //$date_time  = new DateTime($dataIni);
        //$diff = $date_time->diff( new DateTime($dataFim));      
        
//        $gasto=$diff->format('%H:%i:%s'); 
////////CALCULAR TEWMPO PERDIDO
        
        $tpDuracao = $converter;////DURACAo
        $tpDuracao;
        
        $hora1 = explode(":",$tpDuracao);
        $hora2 = explode(":",$tpGasto);
     
        //echo $tpDuracao . "|";
        //echo $tpGasto . "|";
        
        
        $acumulador1 = ($hora1[0] * 3600) + ($hora1[1] * 60) + $hora1[2];
        $acumulador2 = ($hora2[0] * 3600) + ($hora2[1] * 60) + $hora2[2];
        
        $resultado = 0;
        if($acumulador1>$acumulador2 ){
           $resultado = $acumulador1 - $acumulador2;    
        }
        if($acumulador2>$acumulador1 ){
           $resultado = $acumulador2 - $acumulador1;    
        }
            
        
        
        
        $hora_ponto = floor($resultado / 3600);
        $resultado = $resultado - ($hora_ponto * 3600);
        $min_ponto = floor($resultado / 60);
        $resultado = $resultado - ($min_ponto * 60);
        $secs_ponto = $resultado;
        //Grava na variável resultado final
        $tempo = $hora_ponto.":".$min_ponto.":".$secs_ponto;
        
      
/////////////////////////////////////////////////////////////////////////      
     $sql = "update historicotreino set sttreino = 'F' , qtton = :ton,duracao = :duracao," . 
             "tempogasto= :gasto, diferencatempo = :diferenca, dtfim = :datafim,tempo_parado = :tempo_parado"  . 
             " where idunidade = :unidade" .
             " and idaluno = :aluno" .
             " and idtreino = :prescricao" . 
             " and sttreino = 'A'" .              
             " and nrdiatreino = :nrdiatreino";  

      $stmt = $pdo->prepare($sql);              
      $stmt->execute(array(
        ':aluno' => $idaluno,
        ':unidade' => $unidade,
        ':prescricao' => $prescricao,
        ':nrdiatreino' => $nrdiatreino,
        ':ton' => $varTon,
        ':datafim' => $dataFim,
        ':duracao' => $converter,  
        ':gasto' => $tpGasto,  
        ':diferenca' => $tempo,  
        ':tempo_parado' => $tempo_parado,  
      ));
 
       $row_array['MENSAGEM'] = 'Registro efetuado com sucesso';
       $row_array['TON'] = $varTon;
       $tpg=explode(":", $tpGasto);
       if($tpg[0]> 0){        
          $row_array['TEMPOGASTO'] = sprintf("%02d", $tpg[0]) . "h" . sprintf("%02d", $tpg[1])  . "m" . sprintf("%02d", $tpg[2]) . 's'  ;
       }else {
          $row_array['TEMPOGASTO'] = sprintf("%02d", $tpg[1])  . "m" . sprintf("%02d", $tpg[2]) . 's'  ;
                
        }     
       
       //$row_array['TEMPOGASTO'] = $gasto;
       
       $row_array['HORAINICIO'] =  $dataIni;
       $row_array['HORATERMINO'] =  $horaFim;
       $row_array['DURACAO'] = $converter;

       $total = $converter;

       if ($converter > $tpGasto){
          $row_array['DIFERENCATEMPO'] = 0 ; 
          $row_array['MENSAGEMFINAL'] = "PARABÉNS, TREINO REALIZADO COM SUCESSO." ; 
          
       }  else {
          $tphoras=explode(":",$tempo); 
          if($tpg[0]> 0){ 
             $row_array['DIFERENCATEMPO'] = sprintf("%02d", $tphoras[0]) . "h" . sprintf("%02d", $tphoras[1])  . "m" . sprintf("%02d", $tphoras[2]) . "s" ;   
          }else{
             $row_array['DIFERENCATEMPO'] = sprintf("%02d", $tphoras[1])  . "m" . sprintf("%02d", $tphoras[2]) . "s" ;   
              
          }   
          $row_array['MENSAGEMFINAL'] = "" ; 
          $row_array['TEMPOPARADO'] = $tempo_parado ; 
          
       }
       
       
       array_push($return_arr,$row_array);
      

     printf( json_encode($return_arr));      
 }

} catch (PDOException $exception) {
    //$pdo->rollBack();
    printf('Não foi possível realizar a operação: %s' , $exception);
    
 }
 
?>