<?php
  $return_arr = array();
 if (isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
    }
   // Access-Control headers are received during OPTIONS requests
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
 
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         
 
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
 
        exit(0);
    }
 
 
  $postdata = file_get_contents("php://input");
    
    
  try {  
   if (isset($postdata)) {
      $request = json_decode($postdata);

      $aluno = $request->idaluno;
      $unidade = $request->idunidade;
      $prescricao = $request->idprescricao;
      $nrdiatreino = $request->nrdiatreino;
      $nrlancamento = $request->nrlancamento;
      $tp = $request->tp;
      $nrfase = 1;
      $nrciclo = 1;
      
      $series = $request->series;
      
      $count =sizeof($series);
      $i=0;
      
      include "conecta.php";        

      $pdo = new PDO("mysql:host={$dbhost};dbname={$dbname}", $dbuser, $dbpass);
      $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);  
     // $pdo->setAttribute(PDO::ATTR_AUTOCOMMIT,false);  
     // $pdo->beginTransaction();
            
      ////
     $exercicio=0;
     $qtserie=0;
     $temEvolucao=0; 
     $tpEdicao="";
      /////////
      
      foreach($series as $array_item){               
           $object = $series[$i];
           
          
           $serie=$object->serie;
           $carga=$object->carga;      
           $repeticao=$object->repeticao; 


         /*$sql="select l.idexercicio, l.qtserie from listaexerciciotreino l, prescricao p where l.idunidade= " . $unidade . " and l.idaluno = " . $aluno . " and l.idprescricao = " . $prescricao . " and l.nrdiatreino = " . $nrdiatreino . " and l.nrlancamento = " . $nrlancamento . " and p.idprescricao = l.idprescricao and p.idaluno = l.idaluno";

          $qtserie=0;        
          $exercicio=0;        
          $con = new PDO("mysql:host={$dbhost};dbname={$dbname}", $dbuser, $dbpass);
          $con->query('SET NAMES utf8');
          $consulta = $con->query($sql);
          while($c = $consulta->fetch(PDO::FETCH_OBJ)) {
                $exercicio=$c->idexercicio;
                $qtserie=$c->qtserie;
          }           
          $sql ="SELECT e.idexercicio,l.nrlancamento,";
          $sql .= " e.nmexercicio, l.qtrepeticao ";
          $sql .= "FROM evolucaotreino as l, exercicio as e WHERE";
          $sql .= " e.idexercicio = " . $exercicio ;
          $sql .= " AND e.idexercicio = l.idexercicio";
          $sql .= " AND l.idunidade= " . $unidade;
          $sql .= " AND l.idaluno = " . $aluno . " AND l.idprescricao = " . $prescricao;
          $sql .= " AND l.nrdiatreino = " . $nrdiatreino ;
          $sql .= " AND l.nrlancamento = " . $nrlancamento;

          $temEvolucao=0;   
          
          $con1 = new PDO("mysql:host={$dbhost};dbname={$dbname}", $dbuser, $dbpass);
          $con1->query('SET NAMES utf8');
          $consulta1 = $con1->query($sql);
          while($c1 = $consulta1->fetch(PDO::FETCH_OBJ)) {
              $temEvolucao=1;        
          
          }
          if ($temEvolucao==1){
             $tpEdicao="U" ;
          } else {
            $tpEdicao="I" ;
          }  
          ///////////////////////////////////////////////
         $dia_hoje = date('Y-m-d') ;
         /*if  ($tpEdicao=='I'){
         
           $sql = "insert evolucaotreino (idaluno,idunidade,idprescricao,nrfase,nrciclo,nrdiatreino,nrlancamento,qtrepeticao,qtserie, qtcarga,dtregistro,idexercicio)";
           $sql .= " values (:aluno,:unidade,:prescricao,:nrfase,:nrciclo,:nrdiatreino,:nrlancamento,:repeticao,:serie,:carga,CURRENT_DATE,:exercicio)";

           $stmt = $pdo->prepare($sql);           
                   
           $stmt->execute(array(
             ':aluno' => $aluno,
             ':unidade' => $unidade,
             ':prescricao' => $prescricao,
             ':nrfase' => $nrfase,
             ':nrciclo' => $nrciclo,
             ':nrdiatreino' => $nrdiatreino,
             ':nrlancamento' => $nrlancamento,
             ':repeticao' => $repeticao,
             ':serie' => $serie,
             ':carga' => $carga,
             ':exercicio' => $exercicio,
           )); 
       
        }  */
        /*$sql ="select s.qtcarga, s.idaluno, s.idunidade, s.idprescricao, s.nrdiatreino, s.nrserie, s.nrlancamento ";
        $sql .= "from serietreino as s where";
        $sql .= " s.idunidade= " . $unidade;
        $sql .= " and s.idaluno = " . $aluno; 
        $sql .= " and s.idprescricao = " . $prescricao;
        $sql .= " and s.nrfase = " . $nrfase;
        $sql .= " and s.nrciclo = " . $nrciclo;
        $sql .= " and s.nrdiatreino = " . $nrdiatreino;
        $sql .= " and s.nrlancamento = " . $nrlancamento;
        $sql .= " and s.nrserie = " . $serie;

        echo  "serie : " . $serie;
        $temEvolucao=0;   
          
        $con1 = new PDO("mysql:host={$dbhost};dbname={$dbname}", $dbuser, $dbpass);
        $con1->query('SET NAMES utf8');
        $consulta1 = $con1->query($sql);
        while($c1 = $consulta1->fetch(PDO::FETCH_OBJ)) {
          $temEvolucao=1;        
        }*/
        
        /*if ($temEvolucao==1){
           $tpEdicao="U" ;
        } else {
          $tpEdicao="I" ;
        }*/
///////////////////////////////////
    if ($tp=='MOB'){    
        /*if ($tpEdicao=="I"){
            $sql = "insert into serietreino (idaluno,idunidade,idprescricao,nrfase,nrciclo,nrdiatreino,nrlancamento,nrserie,qtrepeticao,qtcarga,dtregistro)";
            $sql .= " values (:aluno,:unidade,:prescricao,:nrfase,:nrciclo,:nrdiatreino,:nrlancamento,:serie,:repeticao,:carga,CURRENT_DATE)";

            $stmt = $pdo->prepare($sql);           
            $stmt->execute(array(
              ':aluno' => $aluno,
              ':unidade' => $unidade,
              ':prescricao' => $prescricao,
              ':nrfase' => $nrfase,
              ':nrciclo' => $nrciclo,
              ':nrdiatreino' => $nrdiatreino,
              ':nrlancamento' => $nrlancamento,
              ':repeticao' => $repeticao,
              ':serie' => $serie,
              ':carga' => $carga,
            ));         
            
            
        } else {*/
         $sql ="SELECT max(nrlancamentodia) as maximo from serietreino";
         $sql .= " WHERE idaluno = " . $aluno;
         $sql .= " AND idunidade= " . $unidade;
         $sql .= " AND nrdiatreino = " . $nrdiatreino ;
         $sql .= " AND nrlancamento = " . $nrlancamento;
         echo $sql;
         $ultimoDia=1;        
         $con = new PDO("mysql:host={$dbhost};dbname={$dbname}", $dbuser, $dbpass);
         $con->query('SET NAMES utf8');
         $consulta = $con->query($sql);
         while($c = $consulta->fetch(PDO::FETCH_OBJ)) {
            $ultimoDia=$c->maximo;        
         }               

           $sql  = "update serietreino set ";
           $sql .= "qtrepeticao = " . $repeticao ;
           $sql .= ", qtcarga = " . $carga ;
           $sql .= ", dtregistro = CURRENT_DATE";
           $sql .= " where idaluno = " . $aluno;
           $sql .= " AND idunidade = " . $unidade ;
           $sql .= " AND idprescricao = " . $prescricao;
           $sql .= " AND nrfase = " . $nrfase ;
           $sql .= " AND nrciclo = " . $nrciclo ;
           $sql .= " AND nrdiatreino = " . $nrdiatreino;
           $sql .= " AND nrlancamento = " . $nrlancamento;
           $sql .= " AND nrlancamentodia = " . $ultimoDia;
           $sql .= " AND nrserie = " . $serie ;
           $stmt = $pdo->prepare($sql);           
           $stmt->execute(array(
           ));         
           

        $row_array['IDERRO'] = '02';
        $row_array['NMERRO'] = 'REGISTRO INSERIDO COM SUCESSO';
        array_push($return_arr,$row_array);
        echo json_encode($return_arr);
    }      
   $i++;     
  }      
//////////////////////////////////        


      


 }

} catch (PDOException $exception) {
    //$pdo->rollBack();
    printf('Não foi possível realizar a operação: %s' , $exception);
    
 }
 
?>