<!DOCTYPE html>
<html lang="pt-br">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>PersonalClubBrasil</title>

    {!! Html::style("tema_assets/css/bootstrap.min.css") !!}
    {!! Html::style("tema_assets/font-awesome/css/font-awesome.css") !!}

    {!! Html::style("tema_assets/css/animate.css") !!}
    {!! Html::style("tema_assets/css/style.css") !!}
    {!! Html::style("css/app.css") !!}


</head>

<body style="    background-color: #2f4050;">

<style>
    .form-horizontal {
        color: #ddd;
        text-align: left;
    }
    .form-horizontal .control-label {
        text-align: left;
    }
    .tit_painel {
        color: #f5f5f5;
        font-size: 36px;
    }
    .enviaMail {
        color: #fe9404;
    text-decoration: underline;
    font-weight: normal;
}

.enviaMail:hover{
	color: #e8ad5d;
	 text-decoration: underline;
}
</style>
<div class="middle-box text-center loginscreen animated fadeInDown">
    <div>
      
      

	
		<div style="color: #fff">
			{{ HTML::image('tema_assets/img/logo_topo.png') }}
             <h2 style="font-size: 32px; ">Seu acesso expirou!</h2>
            
		
                    <h2 style="margin-top:40px">Entre em contato: </h2>
                    <h3> <a class="enviaMail" href="mailto:contato@personalclubbrasil.com.br"> contato@personalclubbrasil.com.br</a></h3>
		</div>
			

      
    </div>
</div>

<!-- Mainly scripts -->
{!! HTML::script("tema_assets/js/jquery-2.1.1.js") !!}
{!! HTML::script("tema_assets/js/bootstrap.min.js") !!}

</body>

</html>



