@extends('layouts.app')
@section('content')


@push('scripts')

{!! Html::script("tema_assets/js/plugins/steps/jquery.steps.min.js") !!}
{!! Html::script("tema_assets/js/plugins/validate/jquery.validate.min.js") !!}

{!! Html::script("js/passosController.js") !!}

@endpush
<!--  wizard -->
{!! Html::style("tema_assets/css/plugins/steps/jquery.steps.css") !!}


<div class="wrapper wrapper-content" ng-controller="passosController">
    <div class="ibox-content">

        <h2>
            Personalizar meu Aplicativo!
        </h2>

        <p>
            Complete seus dados para iniciar o teste com o aplicativo.
        </p>


        <div class="wizard">
            
      
            <div class="steps clearfix">

                <ul role="tablist"><li role="tab" class="current" aria-disabled="false" aria-selected="false"><a id="form-t-0" href="#form-h-0" aria-controls="form-p-0"><span class="number">1.</span> 
                    Meus Dados
                </a></li><li role="tab" class="disabled last"  aria-disabled="false" aria-selected="true"><a id="form-t-1" href="#form-h-1" aria-controls="form-p-1"><span class="current-info audible">current step: </span><span class="number">2.</span> 
                    Personaliar App
                </a></li><li role="tab" class="disabled last" aria-disabled="true"><a id="form-t-2" href="#form-h-2" aria-controls="form-p-2"><span class="number">3.</span> 
                    Parabéns!
                </a></li>
                </ul>
            </div>

          </div>

            
              <fieldset style="padding:10px 20px">
                <h2>
                    Confirme seus da dados
                </h2>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>
                                Nome do Resposável
                            </label>
                            <input class="form-control required" id="userName" 
                            name="userName"  value="{{$user->name}}"  
                            type="text">
                            </input>
                        </div>
                        <div class="form-group">
                            <label>
                                E-mail do Resposável
                            </label>
                            <input class="form-control required" id="email" name="email"
                            value="{{$user->email}}" ng-model="empresa.email" type="text">
                            </input>
                        </div>
                     
                        <div class="form-group">
                            <label>
                                Nome Fantasia (da Academia)
                            </label>
                            <input class="form-control required" id="fantasia" name="fantasia" 
                             value="{{$user->fantasia}}" ng-model="empresa.fantasia"  type="text">
                            </input>
                        </div>

                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>
                                        CNPJ:
                                    </label>
                                    <input class="form-control required"
                                    value="{{$user->cnpj}}" ng-model="empresa.cnpj" id="userName" name="userName" type="text">
                                    </input>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>
                                       Celular (WhatsApp do Resposável ) 
                                    </label>
                                    <input class="form-control required" id="userName" name="userName" 
                                     value="{{$user->celular}}" ng-model="empresa.celular"  type="text">
                                    </input>

                                </div>
                            </div>
                        </div>
                      
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Estado:</label>
                                    <select class="form-control" ng-model="empresa.idestado" name="idestado" ng-change="changeEstado(empresa.idestado)" ng-disabled="inactive">
                                        <option value="">Selecione</option>
                                        <option ng-repeat="estado in estados" ng-value="estado.id">@{{estado.nome}}/@{{estado.uf}}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Cidade:</label>
                                    <select class="form-control" ng-model="empresa.idcidade" name="idcidade" ng-disabled="inactive" >
                                        <option value="">Selecione</option>
                                        <option ng-repeat="cidade in cidades" ng-value="cidade.id">@{{cidade.nome}}</option>
                                    </select>
                                </div>
                            </div>
                        </div>



                        <div class="form-group">
                            <input class="required" id="acceptTerms" name="acceptTerms" type="checkbox">
                                <label for="acceptTerms">
                                    Eu aceito os Termos e Condições.
                                </label>
                            </input>
                        </div>
                    </div>
                    
                </div>
            </fieldset>


{{--  inicio do confirme seus dados --}}
        <div class="row">
            <div class="col-md-6">

                
            </div>
            

             <div class="col-md-3">
              {{--   <button class="btn btn-default btn-rounded btn-block" ng-click="gravarTema(tela.id, tela.nometela,tema_id)">
                    
                    Anterior  <i class="fa fa-chevron-right"> </i>
                </button> --}}
            </div>
            <div class="col-md-3">
                <a ng-click="upEmpresa(empresa)"   class="btn btn-lg btn-primary btn-block" >
                    Próximo  <i class="fa fa-chevron-right"> </i>
                </a>



{{-- 
                 <button ng-click="cadastrar_lead()" ng-disabled="clicado==1" class="btn btn-lg btn-primary btn-block" >
                            <strong>INICIAR TESTE COMPLETO <i class="fa fa-arrow-right" aria-hidden="true"></i></strong></button> --}}


            </div> 

        </div>

        <hr>
{{--  fim do confirme seus dados --}}

      
    </div>




</div>


<style>
.btn-primary {
    color: #fff;
    background-color: #35b729;
    border-color: #35b729;
}
.btn-primary:active {
    color: #fff;
    background-color: #34c726 !important;
    border-color: #35b729 !important;
}
.btn-primary:hover {
    color: #fff;
    background-color: #34c726 !important;
    border-color: #35b729 !important;
}
   
.btn-primary:focus, .btn-primary.focus {
    color: #fff;
    background-color: #35b729 !important;
    border-color: #35b729 !important;
}
</style>

@endsection
