<html>
<script id="tinyhippos-injected">if (window.top.ripple) {
        window.top.ripple("bootstrap").inject(window, document);
    }</script>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>{{ config('app.name') }} | Nova senha</title>

    {!! Html::style("tema_assets/css/bootstrap.min.css") !!}
    {!! Html::style("tema_assets/font-awesome/css/font-awesome.css") !!}

    {!! Html::style("tema_assets/css/animate.css") !!}
    {!! Html::style("tema_assets/css/style.css") !!}
    {!! Html::style("css/app.css") !!}

</head>

<body class="gray-bg" cz-shortcut-listen="true">

<div class="passwordBox animated fadeInDown">
    <div class="row">

        <div class="col-md-12">
            <div class="ibox-content">


                <div class="row">

                    <div class="col-lg-12">

                        <form class="m-t" role="form" method="POST" action="{{ url('/password/reset') }}">
                            {{ csrf_field() }}

                            <input type="hidden" name="token" value="{{ $token }}">

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-12 control-label">E-Mail </label>

                                <div class="col-md-12">
                                    <input id="email" type="email" class="form-control" name="email"
                                           value="{{ $email or old('email') }}" required autofocus>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="col-md-12 control-label">Nova senha</label>

                                <div class="col-md-12">
                                    <input id="password" type="password" class="form-control" name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                <label for="password-confirm" class="col-md-12 control-label">Confirmar nova senha</label>
                                <div class="col-md-12">
                                    <input id="password-confirm" type="password" class="form-control"
                                           name="password_confirmation" required>

                                    @if ($errors->has('password_confirmation'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">

                                <div class="col-md-6">
                                    <br><br>
                                    <button type="submit" class="btn btn-primary block full-width m-b">
                                        Salvar nova senha
                                    </button>
                                </div>
                                <div class="col-md-6">
                                    <br><br>
                                    <a href="{{ route('login') }}" class="btn btn-default block full-width m-b">
                                       Fazer login
                                    </a>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <p class="m-t">
        <small style="color: #ccc">Copyright Personal Club Brasil © 2016-<?php echo date("Y")?></small>
    </p>
</div>


</body>
</html>