<html>
<script id="tinyhippos-injected">if (window.top.ripple) {
        window.top.ripple("bootstrap").inject(window, document);
    }</script>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>{{ config('app.name') }} | Nova senha</title>

    {!! Html::style("tema_assets/css/bootstrap.min.css") !!}
    {!! Html::style("tema_assets/font-awesome/css/font-awesome.css") !!}

    {!! Html::style("tema_assets/css/animate.css") !!}
    {!! Html::style("tema_assets/css/style.css") !!}
    {!! Html::style("css/app.css") !!}

</head>

<body class="gray-bg" cz-shortcut-listen="true">
    <div class="passwordBox animated fadeInDown">
        <div class="row">

            <div class="col-md-12">
                <div class="ibox-content">

                    <h2 class="font-bold">Recuperar senha</h2>

                    <p>
                        Informe seu email, o link para recuperar sua senha sera enviado em seu email.
                    </p>
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="row">

                        <div class="col-lg-12">
                            <form class="m-t" role="form" method="POST" action="{{ url('/password/email') }}">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <input type="email" class="form-control" id="email" type="email" name="email" value="{{ old('email') }}" required>
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <button type="submit" class="btn btn-primary block full-width m-b">Enviar link para recuperar senha</button>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-6">
                Copyright Personal Club Brasil
            </div>
            <div class="col-md-6 text-right">
                <small>© 2016-2017</small>
            </div>
        </div>
    </div>
</body>
</html>
