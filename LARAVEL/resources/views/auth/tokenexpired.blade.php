<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PersonalClubBrasil</title>
    {!! Html::style("tema_assets/css/bootstrap.min.css") !!}
    {!! Html::style("tema_assets/font-awesome/css/font-awesome.css") !!}

    {!! Html::style("tema_assets/css/animate.css") !!}
    {!! Html::style("tema_assets/css/style.css") !!}
    {!! Html::style("css/app.css") !!}
    {!! Html::style("dist/app.css") !!}

</head>

<body class="auth-page">
<div class="middle-box  animated fadeInDown">
    <div class="auth-form">
        <div class="text-center">
            <img src="{{url('/')}}/tema_assets/img/logo_topo.png">
        </div>


        <div class="loader_home" style="margin-bottom: 20px">
            <div class="sk-spinner sk-spinner-wave">
                <div class="sk-rect1"></div>
                <div class="sk-rect2"></div>
                <div class="sk-rect3"></div>
                <div class="sk-rect4"></div>
                <div class="sk-rect5"></div>
            </div>
        </div>
		
		<h3 style="text-align: center;"> Seu token de segurança expirou por favor solicite novo envio.
		</h3>

		<p>
			<a href="https://sistemapcb.com.br/password/reset" class="btn btn-primary block full-width m-b"
			style="background-color: #1cb394; border:0"> Solicitar novo token</a>
			 
		</p>
        

        <p class="m-t">
            <small style="color: #ccc">Copyright Personal Club Brasil © 2016-<?php echo date("Y")?></small>
        </p>
    </div>
</div>

<!-- Mainly scripts -->
{!! HTML::script("tema_assets/js/jquery-2.1.1.js") !!}
{!! HTML::script("tema_assets/js/bootstrap.min.js") !!}

</body>

</html>
