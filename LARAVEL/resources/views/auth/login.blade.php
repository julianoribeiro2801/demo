<!DOCTYPE html>
<html lang="pt-br">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>PersonalClubBrasil</title>

    {!! Html::style("tema_assets/css/bootstrap.min.css") !!}
    {!! Html::style("tema_assets/font-awesome/css/font-awesome.css") !!}

    {!! Html::style("tema_assets/css/animate.css") !!}
    {!! Html::style("tema_assets/css/style.css") !!}
    {!! Html::style("css/app.css") !!}


</head>

<body style="    background-color: #2f4050;">

<style>
    .form-horizontal {
        color: #ddd;
        text-align: left;
    }
    .form-horizontal .control-label {
        text-align: left;
    }
    .tit_painel {
        color: #f5f5f5;
        font-size: 36px;
    }
</style>

<div class="middle-box text-center loginscreen animated fadeInDown">
    <div>
        <div>
            <h2 class="tit_painel">Painel de Controle</h2>
            {{ HTML::image('tema_assets/img/logo_topo.png') }}
            
        </div>
        @if ($errors->has('msg'))
            <div class="alert alert-danger">
                <strong>{{ $errors->first('msg') }}</strong>
            </div>

        @endif

        {{-- inicio --}}
        <div  class="loader_home" style="margin-bottom: 20px">
            <div class="sk-spinner sk-spinner-wave">
                <div class="sk-rect1"></div>
                <div class="sk-rect2"></div>
                <div class="sk-rect3"></div>
                <div class="sk-rect4"></div>
                <div class="sk-rect5"></div>
            </div>
        </div>

        {{-- fim --}}

        <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
          
           {!! csrf_field() !!}

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email" class="col-md-12 control-label">E-mail</label>

                <div class="col-md-12">
                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                    @if ($errors->has('email'))
                        <span class="help-block">
                            {{-- <strong>{{ $errors->first('email') }}</strong> --}}
                            <strong> Seu E-mail não corresponde aos nossos registros. </strong>

                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password" class="col-md-12 control-label">Senha</label>

                <div class="col-md-12">
                    <input id="password" type="password" class="form-control" name="password" required>

                    @if ($errors->has('password'))
                        <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-12 ">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : ''}}> Lembrar
                        </label>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-block btn-primary">
                        Logar
                    </button>
                    <br>
                    <span class="pull-right">
                        <a class="btn btn-link" style="margin-right: -10px" href="{{ url('/password/reset') }}">
                        Esqueceu a senha ?
                    </a>

                    </span>
                     
                   
                </div>
            </div>
        </form>

        <p class="m-t">
            <small style="color: #ccc">Copyright Personal Club Brasil © 2016-<?php echo date("Y")?></small>
        </p>
    </div>
</div>

<!-- Mainly scripts -->
{!! HTML::script("tema_assets/js/jquery-2.1.1.js") !!}
{!! HTML::script("tema_assets/js/bootstrap.min.js") !!}

</body>

</html>
