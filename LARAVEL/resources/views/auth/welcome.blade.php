<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PersonalClubBrasil</title>
    {!! Html::style("tema_assets/css/bootstrap.min.css") !!}
    {!! Html::style("tema_assets/font-awesome/css/font-awesome.css") !!}

    {!! Html::style("tema_assets/css/animate.css") !!}
    {!! Html::style("tema_assets/css/style.css") !!}
    {!! Html::style("css/app.css") !!}
    {!! Html::style("dist/app.css") !!}

</head>

<body class="auth-page">
<div class="middle-box  animated fadeInDown">
    <div class="auth-form">
        <div class="text-center">

            <img src="{{url('/')}}/tema_assets/img/logo_topo.png">
        </div>


        <div class="loader_home" style="margin-bottom: 20px">
            <div class="sk-spinner sk-spinner-wave">
                <div class="sk-rect1"></div>
                <div class="sk-rect2"></div>
                <div class="sk-rect3"></div>
                <div class="sk-rect4"></div>
                <div class="sk-rect5"></div>
            </div>
        </div>
        <div class="body-message text-center">
            <h1>Parabéns!</h1>
            <p class="user-name"> <b>{{ $user->name }}</b></p>
            <p>Agora é só baixar o aplicativo</p>
            <div class="row">
                <div class="col-md-6">
                    <a class="link-store" href="https://play.google.com/store/apps/details?id=br.aquafit.cliques" target="_blank">
                        <img src="{{ asset('icones/google-play-badge.png') }}" alt="">
                    </a>
                </div>
                <div class="col-md-6">
                    <a class="link-store" href="https://itunes.apple.com/us/app/personalcb/id1290866858?ls=1&mt=8" target="_blank">
                        <img src="{{ asset('icones/app-store-badge.png') }}" alt="">
                    </a>
                </div>
            </div>
        </div>

        <p class="m-t">
            <small style="color: #ccc">Copyright Personal Club Brasil © 2016-<?php echo date("Y")?></small>
        </p>
    </div>
</div>

<!-- Mainly scripts -->
{!! HTML::script("tema_assets/js/jquery-2.1.1.js") !!}
{!! HTML::script("tema_assets/js/bootstrap.min.js") !!}

</body>

</html>
