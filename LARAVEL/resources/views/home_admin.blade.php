@extends('layouts.app')
@section('content')
@push('scripts')

{!! Html::script("js/painelController.js") !!}
{!! Html::script("js/gfmService.js") !!}
{!! Html::script("js/gfmPainelController.js") !!}

{{-- Aluno alunoController --}}
{!! Html::script("js/alunoController.js") !!}


<!-- d3 and c3 charts -->
{!! Html::script("tema_assets/js/plugins/d3/d3.min.js") !!}
{!! Html::script("tema_assets/js/plugins/c3/c3.min.js") !!}

@endpush
<!-- c3 Charts -->
{!! Html::style("tema_assets/css/plugins/c3/c3.min.css") !!}




<div class="wrapper wrapper-content" ng-controller="painelController">



    {{-- inicio --}}



    <div class="row">

        <div class="col-lg-4">
            <div class="painelwig">
                <div class="row painelBorda" >
                    <div class="col-xs-6 text-center painelwig1">
                        <i class="fa fa-heart fa-5x"></i>
                    </div>
                    <div class="col-xs-6 text-center">

                        <h2  style="margin-top:32px" class="font-bold">@{{tpperc}}%  </h2>
                        <?php
                        $meses = array(
                            '01' => 'Janeiro',
                            '02' => 'Fevereiro',
                            '03' => 'Março',
                            '04' => 'Abril',
                            '05' => 'Maio',
                            '06' => 'Junho',
                            '07' => 'Julho',
                            '08' => 'Agosto',
                            '09' => 'Setembro',
                            '10' => 'Outubro',
                            '11' => 'Novembro',
                            '12' => 'Dezembro'
                        );
                        ?>
                        <span> Ativados em {{  $meses[date('m')]}} </span>
                        {{-- < div class = "stat-percent font-bold" > @{{totalprospects}} / @{{total}}
                        <i class="fa fa-level-up"></i> 
                    </div>--}}
                </div>
            </div>
        </div>

    </div>

    <div class="col-lg-4">
        <div class="painelwig">
            <div class="row painelBorda">
                <div class="col-xs-12 painelwig2 text-center">
                    <i class="fa fa-users fa-2x"></i>
                </div>
                <div class="col-xs-12 text-center">
                    <h2 style="margin-top: 10px;" class="font-bold"> @{{totalcli}} </h2>
                    <span> Ativos </span>
                    <div style="margin-top: 10px;"></div>
                    {{-- < div class = "stat-percent font-bold" > @{{totalclientes}} / @{{totalcli}}  
                    <i class="fa fa-level-up"></i> 
                </div>--}}
            </div>
        </div>
    </div>
</div>

<div class="col-lg-4">
    <div class="painelwig">
        <div class="row painelBorda">

            <div class="col-xs-6 text-center">

                <h2 style="margin-top:32px"  class="font-bold">-@{{desatperc}}%</h2>
                <span> Desativados em {{  $meses[date('m')]}} </span>
                {{-- < div class = "stat-percent font-bold" > - @{{totaldesativados}} 
                <i class="fa fa-level-down"></i> 
            </div>--}}
        </div>
        <div class="col-xs-6 red-bg text-center painelwig3">
            <img src="dist/heart-boken.png" style="width: 65px; margin-top: 6px;" alt="">
            {{-- < i class = "fa fa-heart fa-5x" > < /i> --}}
        </div>
    </div>
</div>
</div>


</div>


<div class="row" style="margin-top:15px">
    <div class="col-xs-12 col-sm-6">
        <div class="ibox-content">
            <div class="text-center">
                <h5>Ativados e desativados nos últimos 12 meses </h5>

            </div>
            <div class="">
                <div id="resultados_academia"></div>

            </div>
        </div>
    </div>

    <div class="col-xs-12 col-sm-6">
        <div style="margin-top:25px" class="visible-xs"></div>
        <div class="ibox-content">
            <div class="text-center">
                <h5>Ativados e desativados nos últimos 12 meses por Professor </h5>
            </div>
            <div class="">
                <div id="professores"></div>
            </div>
        </div>
    </div>

</div>



<div class="row" style="margin-top:25px">
    <div class="col-lg-12">
       
        @include('home.desativados')
    </div>
  
</div>


<div class="row" style="margin-top:25px">
    <div class="col-lg-6">
        @include('home.projetos')
    </div>
    <div class="col-lg-6">
         @include('home.agendamentos')

    </div>
</div>



{{--  --}}

<div class="row" >
    <div class="col-lg-4">
        @include('home.carteira_clientes')

    </div>

    <div class="col-lg-8">
        <div class="row">
            <div class="col-lg-6" >

               
                @include('home.gfm_ocupacao')

            </div>
            <div class="col-lg-6">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>TREINO - Revisão de Treino </h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content" >


                        <table class="table table-hover no-margins" style="width: 100%">
                            <thead style="display: block; width: 97%;">
                                <tr>
                                    <th style="width: 54%">Cliente</th>
                                    <th class="col-xs-2">Data</th>
                                    <th class="col-xs-4">Professor</th>
                                    <th class="col-xs-2">Dias</th>
                                </tr>
                            </thead>
                            <tbody style=" height: 230px;
                                   overflow-y: auto; display: block; 
                                   width: 100%;">
                                <tr ng-repeat="revisaotreino in revisaotreinos">
                                    <td style="width: 54%"><a href="@{{revisaotreino.url}}">@{{revisaotreino.name}}</a> </td>
                                    <td style="width: 16%">@{{revisaotreino.treino_revisao}}</td>
                                    <td style="width: 32%">@{{revisaotreino.professor}}</td>
                                    <td style="width: 16%" ng-show="revisaotreino.dtf >= 0" class="text-success">@{{revisaotreino.dtf}}</td>
                                    <td style="width: 16%" ng-show="revisaotreino.dtf < 0" class="text-danger">@{{revisaotreino.dtf}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


            <div ng-controller="gfmPainelController">

                @include('admin.gfms.modal')
                @include('admin.gfms.modal_privado')

                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>GMF do dia</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        <div class="tabs-container">
                            <ul class="nav nav-tabs">
                                <li ng-class="{'active': diahoje == 'Dom'}" ng-show="true" ng-click="setDiasemana('Dom')">
                                    <a data-toggle="tab" href="#tab-1">
                                        Dom
                                    </a>
                                </li>
                                <li ng-class="{'active': diahoje == 'Seg'}" ng-show="true">
                                    <a data-toggle="tab" href="#tab-2"  ng-click="setDiasemana('Seg')">
                                        Seg
                                    </a>
                                </li>
                                <li ng-class="{'active': diahoje == 'Ter'}" ng-show="true">
                                    <a data-toggle="tab" href="#tab-3"  ng-click="setDiasemana('Ter')">
                                        Ter
                                    </a>
                                </li>
                                <li ng-class="{'active': diahoje == 'Qua'}" ng-show="true">
                                    <a data-toggle="tab" href="#tab-4"  ng-click="setDiasemana('Qua')">
                                        Qua
                                    </a>
                                </li>
                                <li ng-class="{'active': diahoje == 'Qui'}" ng-show="true">
                                    <a data-toggle="tab" href="#tab-5"  ng-click="setDiasemana('Qui')">
                                        Qui
                                    </a>
                                </li>
                                <li ng-class="{'active': diahoje == 'Sex'}" ng-show="true">
                                    <a data-toggle="tab" href="#tab-6"  ng-click="setDiasemana('Sex')">
                                        Sex
                                    </a>
                                </li>
                                <li ng-class="{'active': diahoje == 'Sab'}" ng-show="true">
                                    <a data-toggle="tab" href="#tab-7"  ng-click="setDiasemana('Sab')">
                                        Sáb
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content" 
                                 style="overflow-y: scroll; background: #fff; height: 258px; ">
                                <!-- INICIO DA ABA 1 -->
                                <div class="tab-pane active" id="tab-1" ng-show="true" >
                                    <div class="ibox-content">
                                        <table class="table table-hover no-margins">
                                            <thead>
                                                <tr>
                                                    <th>Horário</th>
                                                    <th>Privado</th>
                                                    <th>Aula</th>
                                                    <th>Professor</th>
                                                    <th>Ocupação</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat="auladia in aulasdia| filter:{diasemana:'1'}">
                                                    <td>@{{auladia.hora_inicio}} </td>
                                                    <td>@{{auladia.privado}}</td>
                                                    <td>@{{auladia.nmprograma}}</a> </td>
                                                    <td>@{{auladia.nmprofessor}}</td>
                                                    <td>@{{auladia.ocupacao}}/@{{auladia.capacidade}}</td>
                                                    <td >
                                                        <i class="fa fa-plus" aria-hidden="true"  
                                                           ng-click="getAulasPassadas(auladia.id, auladia.nmprofessor, auladia.nmprograma, auladia.hora_inicio, auladia.privado)" ></i>                                                      
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab-2" ng-show="true" >
                                    <div class="ibox-content">
                                        <table class="table table-hover no-margins">
                                            <thead>
                                                <tr>
                                                    <th>Horário</th>
                                                    <th>Privado</th>
                                                    <th>Aula</th>
                                                    <th>Professor</th>
                                                    <th>Ocupação</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat="auladia in aulasdia| filter:{diasemana:'2'}">
                                                    <td>@{{auladia.hora_inicio}}</td>
                                                    <td> @{{auladia.privado}}</td>
                                                    <td>@{{auladia.nmprograma}}</a> </td>
                                                    <td>@{{auladia.nmprofessor}}</td>
                                                    <td>@{{auladia.ocupacao}}/@{{auladia.capacidade}}</td>
                                                    <td >
                                                        <i class="fa fa-plus" aria-hidden="true"  
                                                           ng-click="getAulasPassadas(auladia.id, auladia.nmprofessor, auladia.nmprograma, auladia.hora_inicio, auladia.privado)" ></i>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab-3" ng-show="true" >
                                    <div class="ibox-content">
                                        <table class="table table-hover no-margins">
                                            <thead>
                                                <tr>
                                                    <th>Horário</th>
                                                    <th>Privado</th>
                                                    <th>Aula</th>
                                                    <th>Professor</th>
                                                    <th>Ocupação</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat="auladia in aulasdia| filter:{diasemana:'3'}">
                                                    <td>@{{auladia.hora_inicio}}</td>
                                                    <td> @{{auladia.privado}}</td>
                                                    <td>@{{auladia.nmprograma}}</a> </td>
                                                    <td>@{{auladia.nmprofessor}}</td>
                                                    <td>@{{auladia.ocupacao}}/@{{auladia.capacidade}}</td>
                                                    <td >
                                                        <i class="fa fa-plus" ar<i class="fa fa-plus" aria-hidden="true"  
                                                                                   ng-click="getAulasPassadas(auladia.id, auladia.nmprofessor, auladia.nmprograma, auladia.hora_inicio, auladia.privado)" ></i>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab-4" ng-show="true" >
                                    <div class="ibox-content">
                                        <table class="table table-hover no-margins">
                                            <thead>
                                                <tr>
                                                    <th>Horário</th>
                                                    <th>Privado</th>
                                                    <th>Aula</th>
                                                    <th>Professor</th>
                                                    <th>Ocupação</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat="auladia in aulasdia| filter:{diasemana:'4'}">
                                                    <td>@{{auladia.hora_inicio}}</td>
                                                    <td> @{{auladia.privado}}</td>
                                                    <td>@{{auladia.nmprograma}}</a> </td>
                                                    <td>@{{auladia.nmprofessor}}</td>
                                                    <td>@{{auladia.ocupacao}}/@{{auladia.capacidade}}</td>
                                                    <td >
                                                        <i class="fa fa-plus" aria-hidden="true"  
                                                           ng-click="getAulasPassadas(auladia.id, auladia.nmprofessor, auladia.nmprograma, auladia.hora_inicio, auladia.privado)" ></i>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab-5" ng-show="true" >
                                    <div class="ibox-content">
                                        <table class="table table-hover no-margins">
                                            <thead>
                                                <tr>
                                                    <th>Horário</th>
                                                    <th>Privado</th>
                                                    <th>Aula</th>
                                                    <th>Professor</th>
                                                    <th>Ocupação</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat="auladia in aulasdia| filter:{diasemana:'5'}">
                                                    <td>@{{auladia.hora_inicio}} </td>
                                                    <td> @{{auladia.privado}}</td>
                                                    <td>@{{auladia.nmprograma}}</a> </td>
                                                    <td>@{{auladia.nmprofessor}}</td>
                                                    <td>@{{auladia.ocupacao}}/@{{auladia.capacidade}}</td>
                                                    <td >
                                                        <i class="fa fa-plus" aria-hidden="true"  
                                                           ng-click="getAulasPassadas(auladia.id, auladia.nmprofessor, auladia.nmprograma, auladia.hora_inicio, auladia.privado)" ></i>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab-6" ng-show="true" ng-click="pegaDiaSemana()">
                                    <div class="ibox-content">
                                        <table class="table table-hover no-margins">
                                            <thead>
                                                <tr>
                                                    <th>Horário</th>
                                                    <th>Privado</th>
                                                    <th>Aula</th>
                                                    <th>Professor</th>
                                                    <th>Ocupação</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat="auladia in aulasdia| filter:{diasemana:'6'}">
                                                    <td>@{{auladia.hora_inicio}}</td>
                                                    <td> @{{auladia.privado}}</td>
                                                    <td>@{{auladia.nmprograma}}</a> </td>
                                                    <td>@{{auladia.nmprofessor}}</td>
                                                    <td>@{{auladia.ocupacao}}/@{{auladia.capacidade}}</td>
                                                    <td >
                                                        <i class="fa fa-plus" aria-hidden="true"  
                                                           ng-click="getAulasPassadas(auladia.id, auladia.nmprofessor, auladia.nmprograma, auladia.hora_inicio, auladia.privado)" ></i>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab-7" ng-show="true" >
                                    <div class="ibox-content">
                                        <table class="table table-hover no-margins">
                                            <thead>
                                                <tr>
                                                    <th>Horário</th>
                                                    <th>Privado</th>
                                                    <th>Aula</th>
                                                    <th>Professor</th>
                                                    <th>Ocupação</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat="auladia in aulasdia| filter:{diasemana:'7'}">
                                                    <td>@{{auladia.hora_inicio}}</td>
                                                    <td> @{{auladia.privado}}</td>
                                                    <td>@{{auladia.nmprograma}}</a> </td>
                                                    <td>@{{auladia.nmprofessor}}</td>
                                                    <td>@{{auladia.ocupacao}}/@{{auladia.capacidade}}</td>
                                                    <td >
                                                        <i class="fa fa-plus" aria-hidden="true"  
                                                           ng-click="getAulasPassadas(auladia.id, auladia.nmprofessor, auladia.nmprograma, auladia.hora_inicio, auladia.privado)" ></i>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- fim da contreller --}}
            </div>




        </div>
    </div>
</div>
</div>




<style>

    .c3-line {
        stroke-width: 2px;
    }

    {{-- .c3-circle {
          fill: red !important;
          width: 10px;
      } --}}

    </style>
    @endsection