<div aria-hidden="true" class="modal inmodal fade" id="grupos_exercicios_create" role="dialog" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
				<h4 class="modal-title">Novo Grupo</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label>Descrição</label>
					<input class="form-control" placeholder="Descrição" ng-model="grupomuscular.nmgrupo">
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" ng-click="register()" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-check"></i> Salvar</button>
			</div>
		</div>
	</div>
</div>