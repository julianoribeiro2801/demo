<div aria-hidden="true" class="modal inmodal fade" id="modal_objetivo" role="dialog" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                <h4 class="modal-title">Novo objetivo</h4>
            </div>
            <!--<form name="PostFormObjetivo" id="PostFormObjetivo" method="post" enctype="multipart/form-data">                    -->
                <div class="modal-body">
                    <div class="form-group">
                        <label>Modalidade</label>
                        <input type="hidden" ng-model="objetivo_.idmodalidade" name="idmodalidade">
                        <input class="form-control"  disabled="true" placeholder="{{nmmodalidade}}" name="nmmodalidade">
                    </div>

                    <div class="form-group">
                        <label>Descrição</label>
                        <input class="form-control" ng-model="objetivo_.nmobjetivo" placeholder="Descrição" name="nmobjetivo">
                    </div>
                </div>

                <div class="modal-footer">
                    <div class="form-group">
                        <button type="button" ng-click="addObjetivo()" value="SendPostForm" class="btn btn-primary pull-right"><i class="fa fa-check"></i> Salvar</button>
                        <!--<button type="submit" name="SendPostForm" id="SendPostForm" value="SendPostForm" class="btn btn-primary pull-right"><i class="fa fa-check"></i> Salvar</button>-->
                    </div>
                </div>
            <!--</form>    -->
        </div>
    </div>
</div>  
<div aria-hidden="true" class="modal inmodal fade" id="modal_objetivo_edit" role="dialog" tabindex="-1" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                <h4 class="modal-title">Editar objetivo</h4>
            </div>
            <form name="PostFormObjetivoUp" id="PostFormNivelUp" method="post" enctype="multipart/form-data">                    
                <div class="modal-body">
                    <div class="form-group">
                        <label>Modalidade</label>
                        <input type="hidden" ng-model="objetivo_.idmodalidade">
                        <input type="hidden"   ng-model="objetivo_.id" name="objetivo_id">
                        <input class="form-control"  disabled="true" placeholder="{{nmmodalidade}}" name="nmmodalidade">
                    </div>
                    <div class="form-group">
                        <label>Descrição</label>
                        <input class="form-control"  ng-model="objetivo_.nmobjetivo" placeholder="Descrição" name="nmobjetivo">
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="form-group">
                        <!--<button type="submit" name="SendPostForm" id="SendPostForm" value="SendPostForm" class="btn btn-primary pull-right"><i class="fa fa-check"></i> Salvar</button>-->
                        <button type="button" ng-click="editObjetivo()" value="SendPostForm" class="btn btn-primary pull-right"><i class="fa fa-check"></i> Salvar</button>                        
                    </div>
                </div>
            </form>    
        </div>
    </div>
</div>