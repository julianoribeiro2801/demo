@push('scripts')
{!! Html::script("js/programatreinamentoController.js") !!}
@endpush
<div class="ibox" ng-controller="programatreinamentoController">
    <div class="ibox-title">
        <div class="ibox-tools">
            <a class="collapse-link"><h5>Programas de Treinamento Personalizado</h5><i class="fa fa-chevron-down"></i></a>
        </div>
    </div>

    <div class="ibox-content" style="margin-bottom:60px;">
        <div class="pull-right" style="margin-left:2px">
            <button class="btn btn-primary" ng-click="addItemPrograma()"><i class="fa fa-plus-square"></i> Novo programa</button>
            <!--<button class="btn btn-primary" data-toggle="modal" data-target="#modal_programa"><i class="fa fa-plus-square"></i> Novo programa</button>-->
        </div>
        <div class="input-group">
            <input type="text" ng-model="searchPrograma" name="busca" placeholder="Pesquisar programas" class="input form-control">
            <span class="input-group-btn"><button type="button" class="btn btn btn-primary"><i class="fa fa-search"></i></button></span>
        </div>		
        <div style="clear:both;"></div>


        {{-- teste  --}}


        <div class="clients-list">
            <div class="table-responsive">
                <table class="table table-striped table-hover table-responsive">
                    <tbody>

                    <div class="dd" id="nestable_prog2">
                        <ol class="dd-list">

                            {{-- NIVEL PRINCIPAL --}}
                            <li class="dd-item "  ng-repeat="item1 in data| filter:searchPrograma">                  
                                <div class="dd-handle"   style="overflow: hidden;"> 
                                    <div class="ddprog" data-index="@{{$index}}" >
                                        <span class="pull-left">
                                            <i class="fa fa-plus bt-nivel_prog@{{$index}}"> </i> <span ng-show="item1.st > 0">@{{item1.nmprograma}}</span>
                                        </span>
                                        <input ng-show="item1.st == 0" type="text" name="nmprograma" class="editprogname" ng-model="item1.nmprograma">
                                    </div>

                                    <span class="pull-right">
                                        <button class="btn btn-primary btn-sm oculta novo_agen@{{$index}}" ng-click="addItemAgenda($index)"><i class="fa fa-plus-square"></i> Novo Agendamento</button>                              
                                        <button ng-show="item1.st > 0" class="btn btn-primary btn-sm" ng-click="getProgramatreinamento(item1.id, item1.nmprograma, $index)"><i class="fa fa-edit"></i></button>
                                        <button ng-show="item1.st == 0" class="btn btn-primary btn-sm" ng-click="upPrograma(item1)"><i class="fa fa-save"></i></button>
                                        <!--<button class="btn btn-primary btn-sm" data-toggle="modal"  ng-click="getProgramatreinamento(item1.id,item1.nmprograma)" data-target="#modal_programa_edit"><i class="fa fa-edit"></i></button>-->
                                        <button class="btn btn-danger btn-sm" ng-click="deleteProgramatreinamento(item1.id, $indice)"><i class="fa fa-trash"></i></button>
                                    </span>
                                </div>


                                {{-- SUBNIVEIS  --}}
                                <ol class="dd-list oculta nivel_prog@{{$index}}"  >
                                    <li class="dd-item"  id="nodrag" data-id="@{{child1.id}}" ng-repeat="child1 in item1.agendas| orderBy:'id'">    
                                        <div border="0" class="dd-handle">@{{ $index + 1}}º Agendamento personalizado
                                            <div class="row">
                                                <div class="col-sm-2" >
                                                    <div class="form-group">
                                                        <input type="number" ng-disabled="child1.st > 0" class="form-control small" name="nrdias" ng-model="child1.nrdias" ng-value="child1.nrdias" placeholder="Dias após o @{{ $index + 1}}º atendimento  "
                                                               style="    width: 110%; text-align: center;">
                                                    </div>
                                                </div>
                                                <div class="col-sm-10">
                                                    <span class="pull-left">

                                                        <input type="text" ng-disabled="child1.st > 0" class="form-control small" name="nmagenda" ng-model="child1.nmagenda" ng-value="child1.nmagenda" placeholder="Descrição" style="width: 154%;     font-weight: normal;">                          

                                                    </span>

                                                    <span class="pull-right">
                                                        <div class="form-group">
                                                            <button id="btnEdita" ng-show="child1.st > 0" class="btn btn-primary btn-sm" ng-click="child1.st = 0"><i class="fa fa-pencil"></i></button>                
                                                            <button id="btnSalva" ng-show="child1.st == -1" class="btn btn-primary btn-sm" ng-click="gravarAgenda(item1.id, child1.nrdias, child1.nmagenda, data.indexOf(item1), $index)"><i class="fa fa-save"></i></button>

                                                            <button id="btnUp" ng-show="child1.st == 0" class="btn btn-primary btn-sm" ng-click="upAgenda(child1.id, child1.nrdias, child1.nmagenda, data.indexOf(item1), $index)"><i class="fa fa-save"></i></button>                
                                                            <button id="btnDel" class="btn btn-danger btn-sm" ng-click="excluirAgenda(child1.id, data.indexOf(item1), $index)"><i class="fa fa-trash"></i></button>
                                                        </div>
                                                    </span>

                                                </div>
                                            </div>                                                    

                                        </div>
                                    </li> 
                                </ol>
                            </li>
                        </ol>
                    </div>


                    </tbody>
                </table>
            </div>
        </div>    


        {{-- fim teste --}}



    </div>
    @include('prescricao.configuracoes.modal_programa')        
</div>

<style type="text/css">

    .ddprog {
        width: 50%;
        position: absolute;
        height: 85%;
        //{{-- background: #000 --}}
    }

    li .oculta { 
        display: none
    }

    .exercicios_cadastro td {
        text-align: left;
    }

    .dd-handle:hover { 
        font-weight: normal;
    }

</style>


