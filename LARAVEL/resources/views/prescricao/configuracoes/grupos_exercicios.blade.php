@push('scripts') {!! Html::script("js/grupomuscularController.js") !!} @endpush
<style>
    .dd-item>button {
        font-size: 22px;
        margin-top: 3px;
    }

    .dd3-item>button {
        margin-left: 0px;
    }

    .nivel1 {
        display: none
    }

    .nivel2 {
        display: none
    }

    .nivel3 {
        display: none
    }

    .nivel4 {
        display: none
    }

    .nivel5 {
        display: none
    }

    .nivel6 {
        display: none
    }

    .nivel7 {
        display: none
    }


    .nivel1:hover {
        background-color: #FFF !important;
    }

    .nivel2:hover {
        background-color: #FFF !important;
    }

    .nivel3:hover {
        background-color: #FFF !important;
    }

    .nivel4:hover {
        background-color: #FFF !important;
    }

    .nivel5:hover {
        background-color: #FFF !important;
    }

    .nivel6:hover {
        background-color: #FFF !important;
    }

    .nivel7:hover {
        background-color: #FFF !important;
    }
</style>
<div class="ibox" ng-controller="grupomuscularController">
    <div class="ibox-title">
        <div class="ibox-tools">
            <a class="collapse-link">
                <h5>
                    Objetivos/Níveis de Habilidade
                </h5>
                <i class="fa fa-chevron-down">
                </i>
            </a>
        </div>
    </div>
    <div class="ibox-content">

        <div class="clients-list">
            <div class="table-responsive">
                <table class="table  table-hover table-responsive">
                    <tbody>
                        <tr>
                            <td onclick="mostrarNiveis('.nivel1')">
                                <i class="fa fa-plus bt-nivel1">
                                </i>
                                Musculação
                            </td>
                            <td>
                            </td>
                            <td style="width:5%;">
                            </td>
                            <td style="width:5%;">
                                <button class="btn btn-primary btn-sm" ng-click="setModalidade(1, 'MUSCULAÇÃO')" data-target="#modal_objetivo" data-toggle="modal">
                                    <i class="fa fa-plus-square">
                                    </i>
                                    Novo Objetivo
                                </button>
                            </td>
                        </tr>
                        <tr class="nivel1">
                            <td colspan="4">
                                <!-- MUSCULACAO -->
                                <div class="dd" id="nestable1">
                                    <ol class="dd-list">
                                        <li class="dd-item" data-id="@{{item.id}}" ng-repeat="item in musculacao">

                                            <div class="dd3-content">
                                                @{{item.nmobjetivo}}
                                                <button class="btn btn-danger btn-xs pull-right" style="margin-right:2px" ng-click="deleteObjetivo(item.id)">
                                                    <i class="fa fa-trash-o"></i>

                                                </button>

                                                <button class="btn btn-primary btn-xs pull-right" style="margin-right:2px" data-target="#modal_objetivo_edit" data-toggle="modal"
                                                    ng-click="setModalidadeEdit(item.id, 1, 'MUSCULAÇÃO', item.id, item.nmobjetivo)">
                                                    <i class="fa fa-pencil-square"></i>


                                                </button>
                                                <button class="btn btn-primary btn-xs pull-right" style="margin-right:2px" data-target="#modal_nivel" data-toggle="modal"
                                                    ng-click="setModalidade(1, 'MUSCULAÇÃO', item.id, item.nmobjetivo)">
                                                    <i class="fa fa-plus-square">
                                                    </i>
                                                    Novo Nível
                                                </button>

                                            </div>

                                            <ol class="dd-list">
                                                <li class="dd-item" data-id="@{{child.id}}" ng-repeat="child in item.niveis">
                                                    <div class="dd3-content">
                                                        @{{child.nmnivel}}
                                                        <button class="btn btn-danger btn-xs pull-right" ng-click="deleteNivel(child.id)" style="margin-right:2px">
                                                            <i class="fa fa-trash-o">
                                                            </i>

                                                        </button>
                                                        <button class="btn btn-primary btn-xs pull-right" style="margin-right:2px" data-target="#modal_nivel_edit" data-toggle="modal"
                                                            ng-click="setNivelEdit(child, 1, 'MUSCULAÇÃO', item.nmobjetivo)">
                                                            <i class="fa fa-pencil-square">
                                                            </i>
                                                        </button>

                                                    </div>
                                                </li>
                                            </ol>
                                        </li>

                                    </ol>
                                </div>

                            </td>
                        </tr>
                        <tr>
                            <td onclick="mostrarNiveis('.nivel2')">
                                <i class="fa fa-plus bt-nivel2">
                                </i>
                                Ginástica
                            </td>
                            <td>
                            </td>
                            <td style="width:5%;">
                            </td>
                            <td style="width:5%;">
                                <button class="btn btn-primary btn-sm" data-target="#modal_objetivo" data-toggle="modal" ng-click="setModalidade(2, 'GINÁSTICA')">
                                    <i class="fa fa-plus-square">
                                    </i>
                                    Novo Objetivo
                                </button>
                            </td>
                        </tr>
                        <tr class="nivel2">
                            <td colspan="4">
                                <div class="dd" id="nestable2">
                                    <ol class="dd-list">
                                        <li class="dd-item" data-id="@{{item.id}}" ng-repeat="item in ginastica">

                                            <div class="dd3-content">
                                                @{{item.nmobjetivo}}

                                                <button class="btn btn-danger btn-xs pull-right" style="margin-right:2px" ng-click="deleteObjetivo(item.id)">
                                                    <i class="fa fa-trash-o"></i>

                                                </button>

                                                <button class="btn btn-primary btn-xs pull-right" style="margin-right:2px" data-target="#modal_objetivo_edit" data-toggle="modal"
                                                    ng-click="setModalidadeEdit(item.id, 2, 'GINÁSTICA', item.id, item.nmobjetivo)">
                                                    <i class="fa fa-pencil-square"></i>


                                                </button>
                                                <button class="btn btn-primary btn-xs pull-right" data-target="#modal_nivel" data-toggle="modal" ng-click="setModalidade(2, 'GINÁSTICA', item.id, item.nmobjetivo)"
                                                    style="margin-right:2px">
                                                    <i class="fa fa-plus-square">
                                                    </i>
                                                    Novo Nível
                                                </button>

                                            </div>

                                            <ol class="dd-list">
                                                <li class="dd-item" data-id="@{{child.id}}" ng-repeat="child in item.niveis">
                                                    <div class="dd3-content">
                                                        @{{child.nmnivel}}
                                                        <button class="btn btn-danger btn-xs pull-right" ng-click="deleteNivel(child.id)">
                                                            <i class="fa fa-trash-o">
                                                            </i>

                                                        </button>
                                                        <button class="btn btn-primary btn-xs pull-right" style="margin-right:2px" data-target="#modal_nivel_edit" data-toggle="modal"
                                                            ng-click="setNivelEdit(child, 2, 'GINÁSTICA', item.nmobjetivo)">
                                                            <i class="fa fa-pencil-square">
                                                            </i>
                                                        </button>

                                                    </div>

                                                </li>
                                            </ol>
                                        </li>
                                    </ol>
                                </div>
                            </td>
                        </tr>


                        <tr>
                            <td onclick="mostrarNiveis('.nivel3')">
                                <i class="fa fa-plus bt-nivel3">
                                </i>
                                Natação
                            </td>
                            <td>
                            </td>
                            <td style="width:5%;">
                            </td>
                            <td style="width:5%;">


                                <button class="btn btn-primary btn-sm" data-target="#modal_objetivo" data-toggle="modal" ng-click="setModalidade(3, 'NATAÇÃO')">
                                    <i class="fa fa-plus-square">
                                    </i>
                                    Novo Objetivo
                                </button>
                            </td>
                        </tr>
                        <tr class="nivel3">
                            <td colspan="4">
                                <div class="dd" id="nestable3">
                                    <ol class="dd-list">
                                        <li class="dd-item" data-id="@{{item.id}}" ng-repeat="item in natacao">
                                            <div class="dd3-content">
                                                @{{item.nmobjetivo}}


                                                <button class="btn btn-danger btn-xs pull-right" style="margin-right:2px" ng-click="deleteObjetivo(item.id)">
                                                    <i class="fa fa-trash-o"></i>

                                                </button>

                                                <button class="btn btn-primary btn-xs pull-right" style="margin-right:2px" data-target="#modal_objetivo_edit" data-toggle="modal"
                                                    ng-click="setModalidadeEdit(item.id, 3, 'NATAÇÃO', item.id, item.nmobjetivo)">
                                                    <i class="fa fa-pencil-square"></i>


                                                </button>

                                                <button class="btn btn-primary btn-xs pull-right" data-target="#modal_nivel" data-toggle="modal" ng-click="setModalidade(3, 'NATAÇÃO', item.id, item.nmobjetivo)">
                                                    <i class="fa fa-plus-square">
                                                    </i>
                                                    Novo Nível

                                                </button>
                                            </div>
                                            <ol class="dd-list">
                                                <li class="dd-item" data-id="@{{child.id}}" ng-repeat="child in item.niveis">
                                                    <div class="dd3-content">
                                                        @{{child.nmnivel}}
                                                        <button class="btn btn-danger btn-xs pull-right" ng-click="deleteNivel(child.id)">
                                                            <i class="fa fa-trash-o">
                                                            </i>

                                                        </button>
                                                        <button class="btn btn-primary btn-xs pull-right" style="margin-right:2px" data-target="#modal_nivel_edit" data-toggle="modal"
                                                            ng-click="setNivelEdit(child, 3, 'NATAÇÃO', item.nmobjetivo)">
                                                            <i class="fa fa-pencil-square">
                                                            </i>
                                                        </button>

                                                    </div>
                                                </li>
                                            </ol>
                                        </li>
                                    </ol>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td onclick="mostrarNiveis('.nivel4')">
                                <i class="fa fa-plus bt-nivel4">
                                </i>
                                Cross Training
                            </td>
                            <td>
                            </td>
                            <td style="width:5%;">
                            </td>
                            <td style="width:5%;">
                                <button class="btn btn-primary btn-sm" data-target="#modal_objetivo" data-toggle="modal" ng-click="setModalidade(4, 'CROSS TRAINING')">
                                    <i class="fa fa-plus-square">
                                    </i>
                                    Novo Objetivo
                                </button>
                            </td>
                        </tr>
                        <tr class="nivel4">
                            <td colspan="4">
                                <div class="dd" id="nestable4">
                                    <ol class="dd-list">
                                        <li class="dd-item" data-id="@{{item.id}}" ng-repeat="item in cross">
                                            <div class="dd3-content">
                                                <button class="btn btn-danger btn-xs pull-right" style="margin-right:2px" ng-click="deleteObjetivo(item.id)">
                                                    <i class="fa fa-trash-o"></i>

                                                </button>

                                                <button class="btn btn-primary btn-xs pull-right" style="margin-right:2px" data-target="#modal_objetivo_edit" data-toggle="modal"
                                                    ng-click="setModalidadeEdit(item.id, 4, 'CROSS TRAINING', item.id, item.nmobjetivo)">
                                                    <i class="fa fa-pencil-square"></i>


                                                </button>

                                                @{{item.nmobjetivo}}
                                                <button class="btn btn-primary btn-xs pull-right" style="margin-right:2px" data-target="#modal_nivel" data-toggle="modal"
                                                    ng-click="setModalidade(4, 'CROSS TRAINING', item.id, item.nmobjetivo)">
                                                    <i class="fa fa-plus-square">
                                                    </i>
                                                    Novo Nível
                                                </button>
                                            </div>
                                            <ol class="dd-list">
                                                <li class="dd-item" data-id="@{{child.id}}" ng-repeat="child in item.niveis">
                                                    <div class="dd3-content">
                                                        @{{child.nmnivel}}
                                                        <button class="btn btn-danger btn-xs pull-right" ng-click="deleteNivel(child.id)">
                                                            <i class="fa fa-trash-o">
                                                            </i>

                                                        </button>
                                                        <button class="btn btn-primary btn-xs pull-right" data-target="#modal_nivel_edit" style="margin-right:2px" data-toggle="modal"
                                                            ng-click="setNivelEdit(child, 4, 'CROSS TRAINING', item.nmobjetivo)">
                                                            <i class="fa fa-pencil-square">
                                                            </i>
                                                        </button>

                                                    </div>
                                                </li>
                                            </ol>
                                        </li>
                                    </ol>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td onclick="mostrarNiveis('.nivel5')">
                                <i class="fa fa-plus bt-nivel5">
                                </i>
                                Corrida
                            </td>
                            <td>
                            </td>
                            <td style="width:5%;">
                            </td>
                            <td style="width:5%;">
                                <button class="btn btn-primary btn-sm" data-target="#modal_objetivo" data-toggle="modal" ng-click="setModalidade(5, 'CORRIDA')">
                                    <i class="fa fa-plus-square">
                                    </i>
                                    Novo Objetivo
                                </button>
                            </td>
                        </tr>
                        <tr class="nivel5">
                            <td colspan="4">
                                <div class="dd" id="nestable5">
                                    <ol class="dd-list">
                                        <li class="dd-item" data-id="@{{item.id}}" ng-repeat="item in corrida">
                                            <div class="dd3-content">
                                                <button class="btn btn-danger btn-xs pull-right" style="margin-right:2px" ng-click="deleteObjetivo(item.id)">
                                                    <i class="fa fa-trash-o"></i>

                                                </button>

                                                <button class="btn btn-primary btn-xs pull-right" style="margin-right:2px" data-target="#modal_objetivo_edit" data-toggle="modal"
                                                    ng-click="setModalidadeEdit(item.id, 5, 'CORRIDA', item.id, item.nmobjetivo)">
                                                    <i class="fa fa-pencil-square"></i>


                                                </button>

                                                @{{item.nmobjetivo}}

                                                <button class="btn btn-primary btn-xs pull-right" data-target="#modal_nivel" data-toggle="modal" ng-click="setModalidade(5, 'CORRIDA', item.id, item.nmobjetivo)">
                                                    <i class="fa fa-plus-square">
                                                    </i>
                                                    Novo Nível
                                                </button>
                                            </div>
                                            <ol class="dd-list">
                                                <li class="dd-item" data-id="@{{child.id}}" ng-repeat="child in item.niveis">
                                                    <div class="dd3-content">
                                                        @{{child.nmnivel}}
                                                        <button class="btn btn-danger btn-xs pull-right" ng-click="deleteNivel(child.id)">
                                                            <i class="fa fa-trash-o">
                                                            </i>

                                                        </button>
                                                        <button class="btn btn-primary btn-xs pull-right" style="margin-right:2px" data-target="#modal_nivel_edit" data-toggle="modal"
                                                            ng-click="setNivelEdit(child, 5, 'CORRIDA', item.nmobjetivo)">
                                                            <i class="fa fa-pencil-square">
                                                            </i>
                                                        </button>

                                                    </div>
                                                </li>
                                            </ol>
                                        </li>
                                    </ol>
                                </div>
                            </td>
                        </tr>


                        <tr>
                            <td onclick="mostrarNiveis('.nivel6')">
                                <i class="fa fa-plus bt-nivel6">
                                </i>
                                Ciclismo
                            </td>
                            <td>
                            </td>
                            <td style="width:10%;">
                            </td>
                            <td style="width:10%;">
                                <button class="btn btn-primary btn-sm" ng-click="setModalidade(6, 'CICLISMO')" data-target="#modal_objetivo" data-toggle="modal">
                                    <i class="fa fa-plus-square">
                                    </i>
                                    Novo Objetivo
                                </button>
                            </td>
                        </tr>
                        <tr class="nivel6">
                            <td colspan="4">
                                <div class="dd" id="nestable6">
                                    <ol class="dd-list">
                                        <li class="dd-item" data-id="@{{item.id}}" ng-repeat="item in ciclismo">
                                            <div class="dd3-content">
                                                <button class="btn btn-danger btn-xs pull-right" style="margin-right:2px" ng-click="deleteObjetivo(item.id)">
                                                    <i class="fa fa-trash-o"></i>

                                                </button>

                                                <button class="btn btn-primary btn-xs pull-right" style="margin-right:2px" data-target="#modal_objetivo_edit" data-toggle="modal"
                                                    ng-click="setModalidadeEdit(item.id, 6, 'CICLISMO', item.id, item.nmobjetivo)">
                                                    <i class="fa fa-pencil-square"></i>


                                                </button>
                                                @{{item.nmobjetivo}}
                                                <button class="btn btn-primary btn-xs pull-right" data-target="#modal_nivel" data-toggle="modal" ng-click="setModalidade(6, 'CICLISMO', item.id, item.nmobjetivo)">
                                                    <i class="fa fa-plus-square">
                                                    </i>
                                                    Novo Nível
                                                </button>
                                            </div>
                                            <ol class="dd-list">
                                                <li class="dd-item" data-id="@{{child.id}}" ng-repeat="child in item.niveis">
                                                    <div class="dd3-content">
                                                        @{{child.nmnivel}}
                                                        <button class="btn btn-danger btn-xs pull-right" ng-click="deleteNivel(child.id)">
                                                            <i class="fa fa-trash-o">
                                                            </i>

                                                        </button>
                                                        <button class="btn btn-primary btn-xs pull-right" data-target="#modal_nivel_edit" style="margin-right:2px" data-toggle="modal"
                                                            ng-click="setNivelEdit(child, 6, 'CICLISMO', item.nmobjetivo)">
                                                            <i class="fa fa-pencil-square">
                                                            </i>
                                                        </button>
                                                    </div>
                                                </li>
                                            </ol>
                                        </li>
                                    </ol>
                                </div>
                            </td>
                        </tr>

                        {{-- fim do cliclismo --}}

                        <tr>
                            <td onclick="mostrarNiveis('.nivel7')">
                                <i class="fa fa-plus bt-nivel7">
                                </i>
                                Nutrição
                            </td>
                            <td>
                            </td>
                            <td style="width:10%;">
                            </td>
                            <td style="width:10%;">
                                <button class="btn btn-primary btn-sm" ng-click="setModalidade(7, 'NUTRIÇÃO')" data-target="#modal_objetivo" data-toggle="modal">
                                    <i class="fa fa-plus-square">
                                    </i>
                                    Novo Objetivo
                                </button>
                            </td>
                        </tr>
                        <tr class="nivel7">
                            <td colspan="4">
                                <div class="dd" id="nestable7">
                                    <ol class="dd-list">
                                        <li class="dd-item" data-id="@{{item.id}}" ng-repeat="item in nutricao">
                                            <div class="dd3-content">
                                                <button class="btn btn-danger btn-xs pull-right" style="margin-right:2px" ng-click="deleteObjetivo(item.id)">
                                                    <i class="fa fa-trash-o"></i>

                                                </button>

                                                <button class="btn btn-primary btn-xs pull-right" style="margin-right:2px" data-target="#modal_objetivo_edit" data-toggle="modal"
                                                    ng-click="setModalidadeEdit(item.id, 7, 'NUTRIÇÃO', item.id, item.nmobjetivo)">
                                                    <i class="fa fa-pencil-square"></i>


                                                </button>
                                                @{{item.nmobjetivo}}
                                                <button class="btn btn-primary btn-xs pull-right" data-target="#modal_nivel" data-toggle="modal" ng-click="setModalidade(7, 'NUTRIÇÃO', item.id, item.nmobjetivo)">
                                                    <i class="fa fa-plus-square">
                                                    </i>
                                                    Novo Nível
                                                </button>
                                            </div>
                                            <ol class="dd-list">
                                                <li class="dd-item" data-id="@{{child.id}}" ng-repeat="child in item.niveis">
                                                    <div class="dd3-content">
                                                        @{{child.nmnivel}}
                                                        <button class="btn btn-danger btn-xs pull-right" ng-click="deleteNivel(child.id)">
                                                            <i class="fa fa-trash-o">
                                                            </i>

                                                        </button>
                                                        <button class="btn btn-primary btn-xs pull-right" data-target="#modal_nivel_edit" style="margin-right:2px" data-toggle="modal"
                                                            ng-click="setNivelEdit(child, 7, 'NUTRIÇÃO', item.nmobjetivo)">
                                                            <i class="fa fa-pencil-square">
                                                            </i>
                                                        </button>
                                                    </div>
                                                </li>
                                            </ol>
                                        </li>
                                    </ol>
                                </div>
                            </td>
                        </tr>
                        {{-- fim nutricao --}}

                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @include('prescricao.configuracoes.modal_grupo_exercicios')
    @include('prescricao.configuracoes.modal_grupo_exercicios_edit')
    @include('prescricao.configuracoes.modal_nivel') 
    @include('prescricao.configuracoes.modal_objetivo')

    <div style="margin-top:30px">   </div>
<div class="ibox">
    <div class="ibox-title">
        <div class="ibox-tools">
            <a class="collapse-link">
                <h5>
                    Processos de Atendimento
                </h5>
                <i class="fa fa-chevron-down">
                </i>
            </a>
        </div>
    </div>

    <div class="ibox-content">

        <div class="clients-list">
            <div class="table-responsive">
                <table class="table table-striped table-hover">
                    <tbody>
                        <tr ng-repeat="processo in processos">
                            <td><label>@{{ processo.nmprocesso }}</label></td>
                            <td style="width: 50px">

                                <div class="switch">
                                    <div class="onoffswitch">
                                        <input type="checkbox" ng-checked="processo.statusprocesso=='S'" checked class="onoffswitch-checkbox" id="@{{ processo.id }}" ng-click="statusProcesso(processo.id)">
                                        <label class="onoffswitch-label" for="@{{ processo.id }}">
                                            <span class="onoffswitch-inner" ></span>
                                            <span class="onoffswitch-switch"></span>
                                        </label>
                                    </div>
                                </div>

                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
    
</div>







@push('scripts')
<script>
    // trazer abas fechadas
    function mostrarNiveis(name) {
        var str = name;
        var res = str.replace('.', '');
        $(name).toggle();
        $(".bt-" + res).toggleClass("fa-minus fa-plus");
    };
</script> @endpush @section('css') {!! Html::style("assets_admin/vendor/pnotify/pnotify.custom.css") !!} {!! Html::style("assets_admin/vendor/select2/select2.css")
!!} {!! Html::style("assets_admin/vendor/bootstrap-fileupload/bootstrap-fileupload.min.css") !!} @endsection @section('scripts')
{!! HTML::script("assets_admin/vendor/jquery-form/jquery.form.min.js") !!} {!! HTML::script("assets_admin/vendor/jquery-validation/jquery.validate.js")
!!} {!! HTML::script("assets_admin/javascripts/forms/nivel.validation.js") !!} {!! HTML::script("assets_admin/javascripts/forms/programatreinamento.validation.js")
!!} {!! HTML::script("assets_admin/javascripts/forms/objetivo.validation.js") !!} {!! HTML::script("assets_admin/javascripts/forms/exercicio.validation.js")
!!} {!! Html::script("assets_admin/vendor/pnotify/pnotify.custom.js") !!} {!! Html::script("assets_admin/vendor/jquery-maskedinput/jquery.maskedinput.min.js")
!!} {!! Html::script("assets_admin/vendor/select2/select2.js") !!} {!! Html::script("assets_admin/vendor/bootstrap-fileupload/bootstrap-fileupload.min.js")
!!} @endsection