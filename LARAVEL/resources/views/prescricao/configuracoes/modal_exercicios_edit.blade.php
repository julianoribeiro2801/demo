<div aria-hidden="true" class="modal inmodal fade" id="exercicios_edit" role="dialog" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
				<h4 class="modal-title">Editar Exercício</h4>
			</div>
			<form name="PostFormExercicioUp" id="PostFormExercicioUp" method="post" enctype="multipart/form-data" class="form-horizontal">
				<div class="modal-body">
					<span style="display:none;"><input type="text" name="id" ng-model="exercicio.id"></span>
					<div class="form-group">
						<label>Nome do Exercício</label>
						<input class="form-control" name="nmexercicio" placeholder="Nome do Exercício" ng-model="exercicio.nmexercicio">
					</div>
					<div class="form-group">
						<label>Grupo Muscular</label>
						<div class="input-group m-b" ng-hide="clickede">
							<select name="idgrupomuscular" class="select2_demo_1 form-control" ng-model="exercicio.idgrupomuscular">
								<option ng-repeat="grupo in grupos" ng-value="@{{ grupo.id }}">@{{ grupo.nmgrupo }}</option>
							</select>
							<a ng-init="clickede=false" ng-click="clickede=!clickede" class="btn btn-primary input-group-addon">
								<i class="fa fa-plus" aria-hidden="true"></i> Novo Grupo
							</a>
						</div>
						<div class="input-group m-b" ng-show="clickede">
							<input class="form-control" name="nmgrupo" placeholder="Nome do Grupo">
							<a ng-click="clickede=!clickede" class="btn btn-primary input-group-addon">
								<i class="fa fa-undo" aria-hidden="true"></i> Grupo Existente
							</a>
						</div>
					</div>
					
					<style>
						.showfoto {
							display: none;
						}
					</style>
					<script>
						function mostrarCampoVideo() {
								$('.showfoto').toggle();
								$('.showVideo').toggle();
						}
					</script>

					<div class="form-group">
						 <p>Selecione o tipo do arquivo que será enviado:</p>
                                <div class="radio radio-primary radio-inline">
                            <input type="radio" id="videoRadio1" onclick="mostrarCampoVideo()" value="videoRadio1" name="radioInline" checked="">
                            <label for="videoRadio1"> Video </label>
                        </div>
                        <div class="radio radio-primary radio-inline">
                            <input type="radio" id="videoRadio2" onclick="mostrarCampoVideo()" value="videoRadio2" name="radioInline">
                            <label for="videoRadio2"> Foto</label>
                        </div>
						
					</div>

					<div class="form-group showfoto" >
						<label>Foto <small>(Jpg, Png, Gif até 1MB)</small></label> 
						<div class="fileinput fileinput-new input-group" data-provides="fileinput">
							<div class="form-control" data-trigger="fileinput">
								<i class="glyphicon glyphicon-file fileinput-exists"></i>
								<span class="fileinput-filename"></span>
							</div>
							<span class="input-group-addon btn btn-default btn-file">
								<span class="fileinput-new">Selecionar</span>
								<span class="fileinput-exists">Mudar</span>
								<input type="file" name="dsurlminiatura" id="dsurlminiatura"/>
							</span>
							<a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remover</a>
						</div>
					</div>
					<div class="form-group showVideo" >
						<label>Vídeo <small>(Mp4 até 1MB)</small></label> 
						<div class="fileinput fileinput-new input-group" data-provides="fileinput">
							<div class="form-control" data-trigger="fileinput">
								<i class="glyphicon glyphicon-file fileinput-exists"></i>
								<span class="fileinput-filename"></span>
							</div>
							<span class="input-group-addon btn btn-default btn-file">
								<span class="fileinput-new">Selecionar</span>
								<span class="fileinput-exists">Mudar</span>
								<input type="file" name="video" id="video" />
							</span>
							<a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remover</a>
						</div>
					</div>
					<div class="row load" style="display:none;">
						<div class="col-sm-12">
							<div class="progress progress-striped light active">
								<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="" aria-valuemin="0" aria-valuemax="100">
									<span class="sr-only"></span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
                                    <div class="pull-left" ng-hide='exercicio.video==""'>
					<button type="button" name="SendPostForm" id="SendPostForm" ng-click="exibeVideo()" class="btn btn-primary"><i class="fa fa-download"></i>@{{texto}}</button>
                                    </div>    
                                    <div class="pull-left">
					<button type="button" name="SendPostForm" id="SendPostForm" ng-click="gerarMiniatura(exercicio)" class="btn btn-primary"><i class="fa fa-download"></i>Gerar miniatura</button>
                                    </div>    
                                    <div class="pull-right">
					<button type="submit" name="SendPostForm" id="SendPostForm" value="SendPostForm" class="btn btn-primary"><i class="fa fa-check"></i> Salvar</button>
                                    </div>    
				</div>
                            <div width="80" height="60" ng-show="video=='S'">
                                <video  width="80px" height="60px" ng-src="http://sistemapcb.com.br/public/uploads/exercicios/@{{exercicio.video}}"   controls>

                                </video>

                            </div>                              
                                            
                            
			</form>
		</div>
	</div>
</div>