@push('scripts')
	{!! Html::script("js/exercicioController.js") !!}
@endpush
<div class="ibox" ng-controller="exercicioController">
    <div class="ibox-title">
        <div class="ibox-tools">
            <a class="collapse-link">
                <h5>
                    Exercícios / Grupos Musculares
                </h5>
                <i class="fa fa-chevron-down">
                </i>
            </a>
        </div>
    </div>
    <div class="ibox-content" style="margin-bottom:60px;">
        <div class="pull-right" style="margin-left:2px">
            <button class="btn btn-primary" data-target="#m_exercicios" data-toggle="modal">
                <i class="fa fa-plus-square">
                </i>
                Novo
            </button>
        </div>

        <div class="input-group">
            <input class="input form-control" ng-model="searchExercicio" 
            ng-change="getExerciciosBusca()" placeholder="Pesquisar exercício" type="text">
                <span class="input-group-btn">
                    <button class="btn btn btn-primary" type="button">
                        <i class="fa fa-search">
                        </i>
                    </button>
                </span>
            </input>
        </div>

        <div style="clear:both;">
        </div>

        <div class="clients-list">
            <!-- 	<div class="full-height-scroll"> -->
            <div class="table-responsive exercicios_cadastro">


                <table class="table table-striped table-hover">
                    <tbody>
                        <tr ng-repeat="exercicio in exercicios.data ">
                            <td width="80">
                                <div style="width:100%;height:60px;background-image:url('{{ url('/') }}/uploads/exercicios/@{{ (exercicio.dsurlminiatura != '') ? exercicio.dsurlminiatura : 'exercicio.png' }}'); background-size:cover;">
                                </div>
                            </td>
                            <td>@{{ exercicio.id }}</td>
                            <td>
                               @{{ exercicio.nmexercicio }} / @{{ exercicio.nmgrupo }}
                            </td>
                            <td style="width:5%;">
                                <button class="btn btn-primary btn-sm" data-toggle="modal"  ng-click="select(exercicio)">
                                    <i class="fa fa-edit">
                                    </i>
                                </button>
                            </td>
                            <td style="width:5%;">
                                <button class="btn btn-danger btn-sm" ng-click="deleteExercicio(exercicio.id)">
                                    <i class="fa fa-trash">
                                    </i>
                                </button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>


             {{-- paginacao --}}
                <div class="text-center">
                                   
                    <nav aria-label="Page navigation">
                        <ul class="pagination">
                            <li  ng-class="{'page-item' : true, 'disabled' : !exercicios.prev_page_url}">
                                <a class="page-link" ng-click="paginateUrl(exercicios.prev_page_url)" aria-label="Voltar">
                                    <span aria-hidden="true">&laquo;</span>
                                    <span class="sr-only">Voltar</span>
                                </a>
                            </li>
                            
                            {{-- // com isso temos um limit de 1mil alunos por academia 
                                porque são 50 alunos por pagina--}}
                            <li ng-if="exercicios.last_page > $index"  ng-repeat="n in [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16]"class="page-item">
                                <a ng-click=paginateUrl("/admin/prescricao/getExercicios?page="+($index+1)) class="page-link" href="#@{{$index + 1}}">@{{ $index + 1 }}</a> 
                            </li>
                          


                            <li ng-class="{'page-item' : true, 'disabled' : !exercicios.next_page_url}">
                                <a class="page-link" ng-click="paginateUrl(exercicios.next_page_url)" aria-label="Next">
                                    <span aria-hidden="true">&raquo;</span>
                                    <span class="sr-only">Proximo</span>
                                </a>
                            </li>

                        </ul>
                    </nav>
                </div>
        </div>
       
    </div>
    @include('prescricao.configuracoes.modal_exercicios')
    @include('prescricao.configuracoes.modal_exercicios_edit') 
</div>

<style type="text/css">
    .exercicios_cadastro td {
		text-align: left;
	}
</style>


@section('scripts')


@endsection