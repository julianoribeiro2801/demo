
<div aria-hidden="true" class="modal inmodal fade" id="modal_programa" role="dialog" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
				<h4 class="modal-title">Novo programa</h4>
			</div>
			<form name="PostFormPrograma" id="PostFormPrograma" method="post" enctype="multipart/form-data">                    
                            <div class="modal-body">

				<div class="form-group">
					<label>Descrição</label>
					<input class="form-control"  placeholder="Descrição" ng-model="programa.nmprograma" name="nmprograma" id="nmprograma">
				</div>
                            </div>
                            
                            <div class="modal-footer">
                                <div class="form-group">
                                <button class="btn btn-primary" ng-click="addPrograma(programa)" type="button">
                                        <i class="fa fa-check"></i>Salvar
                                </button>                                   </div>
                            </div>
                        </form>    
		</div>
	</div>
</div>

<div aria-hidden="true" class="modal inmodal fade" id="modal_programa_edit" role="dialog" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
				<h4 class="modal-title">Editar programa</h4>
			</div>
			<form name="PostFormProgramaUp" id="PostFormProgramaUp" method="post" enctype="multipart/form-data">                    
                            <div class="modal-body">
				<div class="form-group">
                                    
                                    	<input type="text" ng-hide="true" ng-model="programa.id" name="id">

					<label>Descrição</label>
					<input class="form-control"  placeholder="Descrição" ng-model="programa.nmprograma" name="nmprograma">
				</div>
                            </div>
                            <div class="modal-footer">
                                <!--<div class="form-group">
                			<button type="submit" name="SendPostForm" id="SendPostForm" value="SendPostForm" class="btn btn-primary pull-right"><i class="fa fa-check"></i> Salvar</button>
                                </div>-->
                                <button class="btn btn-primary" ng-click="upPrograma(programa)" type="button">
                                        <i class="fa fa-check"></i>Salvar
                                </button>                      
                                
                            </div>
                        </form>    
		</div>
	</div>
</div>