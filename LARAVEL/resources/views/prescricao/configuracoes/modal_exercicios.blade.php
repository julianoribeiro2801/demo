<div aria-hidden="true" class="modal inmodal fade" id="m_exercicios" role="dialog" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                <h4 class="modal-title">Novo Exercício</h4>
            </div>
            <form name="PostFormExercicio" id="PostFormExercicio" method="post" enctype="multipart/form-data" class="form-horizontal">
                <div class="modal-body" ng-class="{'is-loading' : loading}">
                    <div class="loading"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></div>
                    <div class="form-group">
                        <label>Nome do Exercício</label>
                        <input class="form-control" name="nmexercicio" placeholder="Nome do Exercício" ng-model="exercicio.nmexercicio">
                    </div>

                {{-- teste  foto--}}
              {{--   <label>Teste File</label>
                <input type="file" id="file" name="file"/>
                <button class="btn btn-primary" ng-click="add()">Add</button> --}}

                  
                {{-- teste --}}



                    <div class="form-group">
                        <label>Grupo Muscular</label>
                        <div class="input-group m-b" ng-hide="clickede">
                            <select name="idgrupomuscular" class="select2_demo_1 form-control" ng-model="exercicio.idgrupomuscular">
                                <option value="1">Selecione o grupo</option>
                                <option ng-repeat="grupo in grupos" value="@{{ grupo.id}}">@{{ grupo.nmgrupo}}</option>	
                            </select>
                            <a ng-init="clickede = false" ng-click="clickede = !clickede" class="btn btn-primary input-group-addon">
                                <i class="fa fa-plus" aria-hidden="true"></i> Novo Grupo
                            </a>
                        </div>
                        <div class="input-group m-b" ng-show="clickede">
                            <input class="form-control" name="nmgrupo" placeholder="Nome do Grupo">
                            <a ng-click="clickede = !clickede" class="btn btn-primary input-group-addon">
                                <i class="fa fa-undo" aria-hidden="true"></i> Grupo Existente
                            </a>
                        </div>
                    </div>

                    <script>
                        function videoradio() {
                            $('.foto').toggle();
                            $('.video').toggle();
                        }
                    </script>

                    <div class="form-group" style="margin-top: -20px;
                         margin-bottom: 10px;">
                        <div class="radio radio-primary radio-inline">
                            <input type="radio" id="videoradio1" value="option1" name="radioInline" checked="" onclick="videoradio()">
                            <label for="inlineRadio1"> Video </label>
                        </div>
                        <div class="radio radio-primary  radio-inline">
                            <input type="radio" id="videoradio1" value="option2" name="radioInline" onclick="videoradio()">
                            <label for="inlineRadio2"> Foto </label>
                        </div>
                    </div>

                    <div class="form-group foto" style="display: none;">
                        <label>Foto <small>(jpg, png, gif)</small></label> 
                        <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                            <div class="form-control" data-trigger="fileinput">
                                <i class="glyphicon glyphicon-file fileinput-exists"></i>
                                <span class="fileinput-filename"></span>
                            </div>
                            <span class="input-group-addon btn btn-default btn-file">
                                <span class="fileinput-new">Selecionar</span>
                                <span class="fileinput-exists">Mudar</span>
                                <input type="file" name="dsurlminiatura"/>
                            </span>
                            <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remover</a>
                        </div>
                    </div>

                    <div class="form-group video">
                        <label>Vídeo <small>(mp4) 2MB</small></label> 
                        <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                            <div class="form-control" data-trigger="fileinput">
                                <i class="glyphicon glyphicon-file fileinput-exists"></i>
                                <span class="fileinput-filename"></span>
                            </div>
                            <span class="input-group-addon btn btn-default btn-file">
                                <span class="fileinput-new">Selecionar</span>
                                <span class="fileinput-exists">Mudar</span>
                                <input type="file" name="video"/>
                            </span>
                            <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remover</a>
                        </div>
                    </div>

                    <div class="row load" style="display:none;">
                        <div class="col-sm-12">
                            <div class="progress progress-striped light active">
                                <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="" aria-valuemin="0" aria-valuemax="100">
                                    <span class="sr-only"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" name="SendPostForm" id="SendPostForm" value="SendPostForm" class="btn btn-primary"><i class="fa fa-check"></i> Salvar</button>
                    <!--<button class="btn btn-primary" ng-click="addExercicio(exercicio)" type="button">
                    <i class="fa fa-check"></i>Salvar</button>-->
                </div>
            </form>
        </div>

            
        
    </div>
</div>