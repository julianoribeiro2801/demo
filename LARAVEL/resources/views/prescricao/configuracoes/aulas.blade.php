@push('scripts')
	{!! Html::script("js/aulaController.js") !!}
@endpush
<div class="ibox" ng-controller="aulaController">
	<div class="ibox-title">
		<div class="ibox-tools">
			<a class="collapse-link"><h5>Aulas</h5><i class="fa fa-chevron-down"></i></a>
		</div>
	</div>
    
	<div class="ibox-content" style="margin-bottom:60px;">
		<div class="pull-right" style="margin-left:2px">
                    <button class="btn btn-primary" ng-click="addItemAula()"><i class="fa fa-plus-square"></i> Novo</button>
		</div>

		<div class="input-group">
			<input type="text" ng-model="searchPrograma" placeholder="Pesquisar aulas" class="input form-control">
			<span class="input-group-btn"><button type="button" class="btn btn btn-primary"><i class="fa fa-search"></i></button></span>
		</div>		

		<div style="clear:both;"></div>
<div class="clients-list">
            <!-- 	<div class="full-height-scroll"> -->
            <div class="table-responsive exercicios_cadastro">


                <table class="table table-striped table-hover">
                    <tbody>
                        <tr ng-repeat="aula in aulas | filter:searchAula">
                            <td ng-show="aula.st>0">
                                @{{ aula.nmprograma }}
                            </td>
                            <td ng-show="aula.st==0">
                               <input ng-show="aula.st==0" type="text" name="nmprograma" ng-model="aula.nmprograma">
                            </td>
                            <td style="width:5%;">
                                <button ng-show="aula.st>0" class="btn btn-primary btn-sm" ng-click="selectAula(aula.id,$index)">
                                    <i class="fa fa-edit">
                                    </i>
                                </button>
                                <button ng-show="aula.st==0" class="btn btn-primary btn-sm" ng-click="adicionaAula(aula.id,aula.nmprograma,$index)">
                                    <i class="fa fa-save">
                                    </i>
                                </button>
                                <!--<button ng-show="aula.st>0" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#upAula" ng-click="selectAula(aula.id)">
                                    <i class="fa fa-edit">
                                    </i>
                                </button>
                                <button ng-show="aula.st==0" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#upAula" ng-click="selectAula(aula.id)">
                                    <i class="fa fa-save">
                                    </i>
                                </button>-->
                            </td>
                            <td style="width:5%;">
                                <button class="btn btn-danger btn-sm" ng-click="delAula(aula.id)">
                                    <i class="fa fa-trash">
                                    </i>
                                </button>
                                
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>  
	</div>
    
        <div aria-hidden="true" class="modal inmodal fade" id="addAula" role="dialog" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                        <h4 class="modal-title">Adicionar Aula</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Nome da Aula:</label>
                            <input type="hidden" class="form-control" ng-model="aula.id">                            
                            <input type="text" class="form-control" ng-model="aula.nmprograma">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" ng-click="adicionaAula(aula.id,aula.nmprograma)" class="btn btn-primary"><i class="fa fa-check"></i> Salvar</button>
                    </div>
                </div>
            </div>
        </div>
        <div aria-hidden="true" class="modal inmodal fade" id="upAula" role="dialog" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                        <h4 class="modal-title">Editar Aula</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Nome da Aula:</label>
                            <input type="hidden" class="form-control" ng-model="aula.id">                            
                            <input type="text" class="form-control" ng-model="aula.nmprograma">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" ng-click="adicionaAula(aula.id,aula.nmprograma)" class="btn btn-primary"><i class="fa fa-check"></i> Salvar</button>
                    </div>
                </div>
            </div>
        </div>      
</div>

<style type="text/css">
	.exercicios_cadastro td {
		text-align: left;
	}
        
        
        
</style>

@push('scripts')
{!! HTML::script("tema_assets/js/plugins/nestable/jquery.nestable.js") !!}
 <!-- Nestable List -->
   <!--<script src="http://sistemapcb.com.br/tema_assets/js/plugins/nestable/jquery.nestable.js"></script>-->
  <script>
         $(document).ready(function(){
              //  $('#nestable77').draggable( "destroy" );

             var updateOutput = function (e) {
                 var list = e.length ? e : $(e.target),
                         output = list.data('output');
                 
                 alert(list);
                 if (window.JSON) {
                     output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
                 } else {
                     output.val('JSON browser support required for this demo.');
                 }
             };
           
             // activate Nestable for list 1
             //updateOutput($('#nestable77').data('output', $('#nestable77-output')));


             $('#nestable77').on('click', function (e) {
                 var target = $(e.target),
                         action = target.data('action');
                 if (action === 'expand-all') {
                     $('.dd').nestable('expandAll');
                 }
                 if (action === 'collapse-all') {
                     $('.dd').nestable('collapseAll');
                 }
             });
         });
    </script>
    @endpush
    @include('prescricao.configuracoes.modal_programa')    