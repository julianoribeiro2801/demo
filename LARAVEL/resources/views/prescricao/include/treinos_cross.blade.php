<div class="row" style="padding-left: 12px;">
			<!-- MASCULINO -->
			<div class="dd" id="nestableTcrossM" ng-hide="client.genero == 'F'">
				<ol class="dd-list">
					<li class="dd-item" data-id="1">
						<div class="dd3-content">
							Masculino
						</div>
						<ol class="dd-list">
							<li class="dd-item" data-id="2" ng-repeat="treinopadraocrossm in treinospadraocrossm">
								<div class="dd3-content">
									@{{ treinopadraocrossm.nmobjetivo}}
								</div>
								<ol class="dd-list">
									<li class="dd-item" data-id="3" ng-repeat="nivelcrossm in treinopadraocrossm.niveis">
										<div class="dd3-content">
											@{{ nivelcrossm.nmnivel}}
										</div>
										<ol class="dd-list">
											<li class="dd-item" data-id="4">
												<div class="dd3-content" ng-repeat="treinoxcrossm in nivelcrossm.treinos">
													<a style=" margin-left: -16px;" ng-click="setaCodigo(treinoxcrossm.id, 'cross')">
														@{{ treinoxcrossm.nome_treino}}
													</a>
													<span class="pull-right">
														<a ng-click="setaCodigo(treinoxcrossm.id,'cross')" style="margin-right: 8px;">
															<i aria-hidden="true" class="fa fa-pencil">
															</i>
														</a>
														<a ng-click="delTreinoPadrao(treinoxcrossm.id)"  >
															<i aria-hidden="true" class="fa fa-trash">
															</i>
														</a>
													</span>
												</div>
											</li>
										</ol>
									</li>
								</ol>
							</li>
						</ol>
					</li>
				</ol>
			</div>
			<!-- INICIALIZAR O nestable  -->
			<script>
			$(document).ready(function () {
			// activate Nestable Maculino
			$('#nestableTcrossM').nestable();
			// activate Nestable Feminino
			$('#nestableTcrossF').nestable();
			$('.dd').nestable('collapseAll');
			});
			</script>
			<!-- FIM  -->
			<!-- FEMININO -->
			<div class="dd" id="nestableTcrossF" ng-hide="client.genero == 'M'">
				<ol class="dd-list">
					<li class="dd-item" data-id="1">
						<div class="dd3-content">
							Feminino
							
						</div>
						<div style="display:none"><input type="text" name="cod" id="cod"></div>
						<ol class="dd-list">
							<li class="dd-item" data-id="2" ng-repeat="treinopadraocrossf in treinospadraocrossf">
								<div class="dd3-content">
									@{{ treinopadraocrossf.nmobjetivo}}
								</div>
								<ol class="dd-list">
									<li class="dd-item" data-id="3" ng-repeat="nivelcrossf in treinopadraocrossf.niveis">
										<div class="dd3-content">
											@{{ nivelcrossf.nmnivel}}
										</div>
										<ol class="dd-list">
											<li class="dd-item" data-id="4">
												<div class="dd3-content" ng-repeat="treinoxcrossf in nivelcrossf.treinos">
													<a style=" margin-left: -16px;" value="@{{treinoxcrossf.id}}" name="sep"  ng-click="setaCodigo(treinoxcrossf.id,'cross')">
														@{{ treinoxcrossf.nome_treino}}
													</a>
													<span class="pull-right">
														
														<a value="@{{treinoxcrossf.id}}" name="sep1" ng-click="setaCodigo(treinoxcrossf.id,'cross')" style="  margin-right: 8px;">
															<i aria-hidden="true" class="fa fa-pencil">
															</i>
														</a>
														<a ng-click="delTreinoPadrao(treinoxcrossf.treino_id)"  >
															<i aria-hidden="true" class="fa fa-trash">
															</i>
														</a>
													</span>
												</div>
											</li>
										</ol>
									</li>
								</ol>
							</li>
						</ol>
					</li>
				</ol>
			</div>
		</div>