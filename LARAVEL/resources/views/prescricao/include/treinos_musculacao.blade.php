<div class="row" style="padding-left: 12px;">
			<!-- MASCULINO -->
			<div class="dd" id="nestableTpadraoM" style="margin-top: -5px;" ng-hide="client.genero == 'F'">
				<ol class="dd-list">
					<li class="dd-item" data-id="1">
						<div class="dd3-content">
							Masculino
							
						</div>
						<ol class="dd-list">
							<li class="dd-item" data-id="2" ng-repeat="treinopadraom in treinospadraom">
								<div class="dd3-content">
									@{{ treinopadraom.nmobjetivo}}
								</div>
								<ol class="dd-list">
									<li class="dd-item" data-id="3" ng-repeat="nivelm in treinopadraom.niveis">
										<div class="dd3-content">
											@{{ nivelm.nmnivel}}
										</div>
										<ol class="dd-list">
											<li class="dd-item" data-id="4">
												<div class="dd3-content" ng-repeat="treinoxm in nivelm.treinos">
													<a style=" margin-left: -16px;" ng-click="getFichaPadrao(treinoxm.treino_id)">
														@{{ treinoxm.treino_nome_padrao}}
													</a>
													<span class="pull-right">
														<!--<a onclick="getCalendarioPadrao(treinoxm.treino_id)" style="    margin-right: 8px;">
															<i aria-hidden="true" class="fa fa-pencil">
															</i>
														</a>-->
														<a ng-click="delTreinoPadrao(treinoxm.treino_id)"  >
															<i aria-hidden="true" class="fa fa-trash">
															</i>
														</a>
													</span>
												</div>
											</li>
										</ol>
									</li>
								</ol>
							</li>
						</ol>
					</li>
				</ol>
			</div>
			<!-- INICIALIZAR O nestable  -->
			<script>
			$(document).ready(function () {
				// activate Nestable Maculino
				$('#nestableTpadraoM').nestable();
				// activate Nestable Feminino
				$('#nestableTpadraoF').nestable();
				$('.dd').nestable('collapseAll');
			});
			</script>
			<!-- FIM  -->
			<!-- FEMININO -->
			<div class="dd" id="nestableTpadraoF" ng-hide="client.genero == 'M'">
				<ol class="dd-list">
					<li class="dd-item" data-id="1">
						<div class="dd3-content">
							Feminino
						</div>
						<ol class="dd-list">
							<li class="dd-item" data-id="2" ng-repeat="treinopadraof in treinospadraof">
								<div class="dd3-content">
									@{{ treinopadraof.nmobjetivo}}
								</div>
								<ol class="dd-list">
									<li class="dd-item" data-id="3" ng-repeat="nivelf in treinopadraof.niveis">
										<div class="dd3-content">
											@{{ nivelf.nmnivel}}
										</div>
										<ol class="dd-list">
											<li class="dd-item" data-id="4">
												<div class="dd3-content" ng-repeat="treinoxf in nivelf.treinos">
													<a style=" margin-left: -16px;" ng-click="getFichaPadrao(treinoxf.treino_id)">
														@{{ treinoxf.treino_nome_padrao}}
													</a>
													<span class="pull-right">
														<!--<a ng-click="getFichaPadrao(treinoxf.treino_id)" style="    margin-right: 8px;">
															<i aria-hidden="true" class="fa fa-pencil">
															</i>
														</a>-->
														<a ng-click="delTreinoPadrao(treinoxf.treino_id)"  >
															<i aria-hidden="true" class="fa fa-trash">
															</i>
														</a>
													</span>
												</div>
											</li>
										</ol>
									</li>
								</ol>
							</li>
						</ol>
					</li>
				</ol>
			</div>
		</div>