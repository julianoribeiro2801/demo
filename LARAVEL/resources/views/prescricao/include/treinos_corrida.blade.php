<div class="row" style="padding-left: 12px;">
			<!-- MASCULINO -->
			<div class="dd" id="nestableTcorridaM" ng-hide="client.genero == 'F'">
				<ol class="dd-list">
					<li class="dd-item" data-id="1">
						<div class="dd3-content">
							Masculino
						</div>
						<ol class="dd-list">
							<li class="dd-item" data-id="2" ng-repeat="treinopadraocorridam in treinospadraocorridam">
								<div class="dd3-content">
									@{{ treinopadraocorridam.nmobjetivo}}
								</div>
								<ol class="dd-list">
									<li class="dd-item" data-id="3" ng-repeat="nivelcorridam in treinopadraocorridam.niveis">
										<div class="dd3-content">
											@{{ nivelcorridam.nmnivel}}
										</div>
										<ol class="dd-list">
											<li class="dd-item" data-id="4">
												<div class="dd3-content" ng-repeat="treinoxcorridam in nivelcorridam.treinos">
													<a style=" margin-left: -16px;" ng-click="setaCodigo(treinoxcorridam.id,'corrida')">
														@{{ treinoxcorridam.nome_treino}}
													</a>
													<span class="pull-right">
														<a ng-click="setaCodigo(treinoxcorridam.id)" style="margin-right: 8px;">
															<i aria-hidden="true" class="fa fa-pencil">
															</i>
														</a>
														<a ng-click="delTreinoPadrao(treinoxcorridam.id,'corrida')"  >
															<i aria-hidden="true" class="fa fa-trash">
															</i>
														</a>
													</span>
												</div>
											</li>
										</ol>
									</li>
								</ol>
							</li>
						</ol>
					</li>
				</ol>
			</div>
			<!-- INICIALIZAR O nestable  -->
			<script>
			$(document).ready(function () {
			// activate Nestable Maculino
			$('#nestableTcorridaM').nestable();
			// activate Nestable Feminino
			$('#nestableTcorridaF').nestable();
			$('.dd').nestable('collapseAll');
			});
			</script>
			<!-- FIM  -->
			<!-- FEMININO -->
			<div class="dd" id="nestableTcorridaF" ng-hide="client.genero == 'M'">
				<ol class="dd-list">
					<li class="dd-item" data-id="1">
						<div class="dd3-content">
							Feminino
							
						</div>
						<div style="display:none"><input type="text" name="cod" id="cod"></div>
						<ol class="dd-list">
							<li class="dd-item" data-id="2" ng-repeat="treinopadraocorridaf in treinospadraocorridaf">
								<div class="dd3-content">
									@{{ treinopadraocorridaf.nmobjetivo}}
								</div>
								<ol class="dd-list">
									<li class="dd-item" data-id="3" ng-repeat="nivelcorridaf in treinopadraocorridaf.niveis">
										<div class="dd3-content">
											@{{ nivelcorridaf.nmnivel}}
										</div>
										<ol class="dd-list">
											<li class="dd-item" data-id="4">
												<div class="dd3-content" ng-repeat="treinoxcorridaf in nivelcorridaf.treinos">
													<a style=" margin-left: -16px;" value="@{{treinoxcorridaf.id}}" name="sep"  ng-click="setaCodigo(treinoxcorridaf.id,'corrida')">
														@{{ treinoxcorridaf.nome_treino}}
													</a>
													<span class="pull-right">
														
														<a value="@{{treinoxcorridaf.id}}" name="sep1" ng-click="setaCodigo(treinoxcorridaf.id,'corrida')" style="  margin-right: 8px;">
															<i aria-hidden="true" class="fa fa-pencil">
															</i>
														</a>
														<a ng-click="delTreinoPadrao(treinoxcorridaf.treino_id)"  >
															<i aria-hidden="true" class="fa fa-trash">
															</i>
														</a>
													</span>
												</div>
											</li>
										</ol>
									</li>
								</ol>
							</li>
						</ol>
					</li>
				</ol>
			</div>
		</div>