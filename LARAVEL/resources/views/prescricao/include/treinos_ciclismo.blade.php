<div class="row" style="padding-left: 12px;">
			<!-- MASCULINO -->
			<div class="dd" id="nestableTciclismoM" ng-hide="client.genero == 'F'">
				<ol class="dd-list">
					<li class="dd-item" data-id="1">
						<div class="dd3-content">
							Masculino
						</div>
						<ol class="dd-list">
							<li class="dd-item" data-id="2" ng-repeat="treinopadraociclismom in treinospadraociclismom">
								<div class="dd3-content">
									@{{ treinopadraociclismom.nmobjetivo}}
								</div>
								<ol class="dd-list">
									<li class="dd-item" data-id="3" ng-repeat="nivelciclismom in treinopadraociclismom.niveis">
										<div class="dd3-content">
											@{{ nivelciclismom.nmnivel}}
										</div>
										<ol class="dd-list">
											<li class="dd-item" data-id="4">
												<div class="dd3-content" ng-repeat="treinoxciclismom in nivelciclismom.treinos">
													<a style=" margin-left: -16px;" ng-click="setaCodigo(treinoxciclismom.id,'ciclismo')">
														@{{ treinoxciclismom.nome_treino}}
													</a>
													<span class="pull-right">
														<a ng-click="setaCodigo(treinoxciclismom.id,'ciclismo')" style="margin-right: 8px;">
															<i aria-hidden="true" class="fa fa-pencil">
															</i>
														</a>
														<a ng-click="delTreinoPadrao(treinoxciclismom.id)"  >
															<i aria-hidden="true" class="fa fa-trash">
															</i>
														</a>
													</span>
												</div>
											</li>
										</ol>
									</li>
								</ol>
							</li>
						</ol>
					</li>
				</ol>
			</div>
			<!-- INICIALIZAR O nestable  -->
			<script>
			$(document).ready(function () {
			// activate Nestable Maculino
			$('#nestableTciclismoM').nestable();
			// activate Nestable Feminino
			$('#nestableTciclismoF').nestable();
			$('.dd').nestable('collapseAll');
			});
			</script>
			<!-- FIM  -->
			<!-- FEMININO -->
			<div class="dd" id="nestableTciclismoF" ng-hide="client.genero == 'M'">
				<ol class="dd-list">
					<li class="dd-item" data-id="1">
						<div class="dd3-content">
							Feminino
							
						</div>
						<div style="display:none"><input type="text" name="cod" id="cod"></div>
						<ol class="dd-list">
							<li class="dd-item" data-id="2" ng-repeat="treinopadraociclismof in treinospadraociclismof">
								<div class="dd3-content">
									@{{ treinopadraociclismof.nmobjetivo}}
								</div>
								<ol class="dd-list">
									<li class="dd-item" data-id="3" ng-repeat="nivelciclismof in treinopadraociclismof.niveis">
										<div class="dd3-content">
											@{{ nivelciclismof.nmnivel}}
										</div>
										<ol class="dd-list">
											<li class="dd-item" data-id="4">
												<div class="dd3-content" ng-repeat="treinoxciclismof in nivelciclismof.treinos">
													<a style=" margin-left: -16px;" value="@{{treinoxciclismof.id}}" name="sep"  ng-click="setaCodigo(treinoxciclismof.id,'ciclismo')">
														@{{ treinoxciclismof.nome_treino}}
													</a>
													<span class="pull-right">
														
														<a value="@{{treinoxciclismof.id}}" name="sep1" ng-click="setaCodigo(treinoxciclismof.id,'ciclismo')" style="  margin-right: 8px;">
															<i aria-hidden="true" class="fa fa-pencil">
															</i>
														</a>
														<a ng-click="delTreinoPadrao(treinoxciclismof.treino_id)"  >
															<i aria-hidden="true" class="fa fa-trash">
															</i>
														</a>
													</span>
												</div>
											</li>
										</ol>
									</li>
								</ol>
							</li>
						</ol>
					</li>
				</ol>
			</div>
		</div>