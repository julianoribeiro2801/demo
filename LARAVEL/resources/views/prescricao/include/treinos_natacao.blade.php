<div class="row" style="padding-left: 12px;">
			<!-- MASCULINO -->
			<div class="dd" id="nestableTnatacaoM" ng-hide="client.genero == 'F'">
				<ol class="dd-list">
					<li class="dd-item" data-id="1">
						<div class="dd3-content">
							Masculino
						</div>
						<ol class="dd-list">
							<li class="dd-item" data-id="2" ng-repeat="treinopadraonatacaom in treinospadraonatacaom">
								<div class="dd3-content">
									@{{ treinopadraonatacaom.nmobjetivo}}
								</div>
								<ol class="dd-list">
									<li class="dd-item" data-id="3" ng-repeat="nivelnatacaom in treinopadraonatacaom.niveis">
										<div class="dd3-content">
											@{{ nivelnatacaom.nmnivel}}
										</div>
										<ol class="dd-list">
											<li class="dd-item" data-id="4">
												<div class="dd3-content" ng-repeat="treinoxnatacaom in nivelnatacaom.treinos">
													<a style=" margin-left: -16px;" ng-click="setaCodigo(treinoxnatacaom.id,'natacao')">
														@{{ treinoxnatacaom.nome_treino}}
													</a>
													<span class="pull-right">
														<a ng-click="setaCodigo(treinoxnatacaom.id,'natacao')" style="margin-right: 8px;">
															<i aria-hidden="true" class="fa fa-pencil">
															</i>
														</a>
														<a ng-click="delTreinoPadrao(treinoxnatacaom.id)"  >
															<i aria-hidden="true" class="fa fa-trash">
															</i>
														</a>
													</span>
												</div>
											</li>
										</ol>
									</li>
								</ol>
							</li>
						</ol>
					</li>
				</ol>
			</div>
			<!-- INICIALIZAR O nestable  -->
			<script>
			$(document).ready(function () {
			// activate Nestable Maculino
			$('#nestableTnatacaoM').nestable();
			// activate Nestable Feminino
			$('#nestableTnatacaoF').nestable();
			$('.dd').nestable('collapseAll');
			});
			</script>
			<!-- FIM  -->
			<!-- FEMININO -->
			<div class="dd" id="nestableTnatacaoF" ng-hide="client.genero == 'M'">
				<ol class="dd-list">
					<li class="dd-item" data-id="1">
						<div class="dd3-content">
							Feminino
							
						</div>
						<div style="display:none"><input type="text" name="cod" id="cod"></div>
						<ol class="dd-list">
							<li class="dd-item" data-id="2" ng-repeat="treinopadraonatacaof in treinospadraonatacaof">
								<div class="dd3-content">
									@{{ treinopadraonatacaof.nmobjetivo}}
								</div>
								<ol class="dd-list">
									<li class="dd-item" data-id="3" ng-repeat="nivelnatacaof in treinopadraonatacaof.niveis">
										<div class="dd3-content">
											@{{ nivelnatacaof.nmnivel}}
										</div>
										<ol class="dd-list">
											<li class="dd-item" data-id="4">
												<div class="dd3-content" ng-repeat="treinoxnatacaof in nivelnatacaof.treinos">
													<a style=" margin-left: -16px;" value="@{{treinoxnatacaof.id}}" name="sep"  ng-click="setaCodigo(treinoxnatacaof.id,'natacao')">
														@{{ treinoxnatacaof.nome_treino}}
													</a>
													<span class="pull-right">
														
														<a value="@{{treinoxnatacaof.id}}" name="sep1" ng-click="setaCodigo(treinoxnatacaof.id,'natacao')" style="  margin-right: 8px;">
															<i aria-hidden="true" class="fa fa-pencil">
															</i>
														</a>
														<a ng-click="delTreinoPadrao(treinoxnatacaof.treino_id)"  >
															<i aria-hidden="true" class="fa fa-trash">
															</i>
														</a>
													</span>
												</div>
											</li>
										</ol>
									</li>
								</ol>
							</li>
						</ol>
					</li>
				</ol>
			</div>
		</div>