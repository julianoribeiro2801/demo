@extends('layouts.app')

@section('content')
	@push('scripts')
		{!! Html::script("js/prescricaoController.js") !!}
		{!! Html::script("js/projetoController.js") !!}
	@endpush

	<style>
		.fixed {
			position: fixed;
			top: 15px;
			padding-right: 39px;
		}
		#dia_atual, #dia_atual textarea {
			background: #f4f4f4;
		}
		.tarefa_finalizada {
			text-decoration: line-through;
		}
		.table-responsive {
			display: block;
			overflow-x: auto;
		}
		.dd-handle {
			overflow: hidden;
		}
		@media screen and (max-width: 400px) {
			.form-control, .single-line {
				padding: 6px 0px !important;
				font-size: 12px !important;
			}
		}
		.table-bottom {
			border-bottom:4px solid #fff;
		}
		.jstree-open > .jstree-anchor > .fa-folder:before {
			content: "\f07c";
		}
		.jstree-default .jstree-icon.none {
			width: 0;
		}
	</style>

	<div ng-controller="prescricaoController">
		<div class="row" style="margin-top:20px;">
			<div class="col-xs-12 col-md-4 pd0mg0">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>Modalidades</h5>
						<div class="ibox-tools">
							<a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
						</div>
					</div>
					<div class="ibox-content">
						<div class="table-responsive">
							<table class="table table-striped table-hover">
								<tbody>
									<tr>
										<td>
											<a class="client-link" data-toggle="tab" onclick="$('#show_treinos_padrao').show(100)">MUSCULAÇÃO</a>
											<div id="show_treinos_padrao" style="display:none;">
												<!-- <div class="ibox-title">
													<span class="text-muted small">
														<a class="btn btn-primary btn-sm"><i class="fa fa-plus-square"></i> Nova Objetivo</a>
														<a class="btn btn-primary btn-sm"><i class="fa fa-plus-square"></i>  Nível Hbilidade</a>
													</span>
												</div> -->
												<div class="ibox-content">
													<div id="jstree1">
													    <ul class="espaco_tree">
													        <li class="jstree-open">
													            <i aria-hidden="true" class="fa fa-folder-open mtr10">
													            </i>
													            Masculino
													            <ul class="espaco_tree">
													                <li class="jstree-open">
													                    <i aria-hidden="true" class="fa fa-folder-open mtr10">
													                    </i>
													                    Hipertrofia
													                    <ul class="espaco_tree">
													                        <li>
													                            <i aria-hidden="true" class="fa fa-folder-open mtr10">
													                            </i>
													                            Iniciante
													                            <ul class="espaco_tree">
													                                <li ng-repeat="treino in treinos" ng-click="changeTreino(treino.treino_id)">
																						<i class="fa fa-file-text mtr10" aria-hidden="true"></i>
																						@{{ treino.treino_nome }}
																					</li>
													                            </ul>
													                        </li>
													                    </ul>
													                </li>
													            </ul>
													        </li>
													    </ul>
													</div>
												</div>
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<a class="client-link" data-toggle="tab" href="#contact-3">
												CROSSFIT
											</a>
										</td>
									</tr>
									<tr >

										<td>
											<a class="client-link" data-toggle="tab" href="#contact-3">
												CICLISMO
											</a>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-8 pd0mg0 pull-right" ng-show="exibePrescricao">
				<input type="hidden" runat="server" ng-init="inputVal='<?= (isset($headers['client_id'])) ? $headers['client_id'] : '' ?>'" ng-model="inputVal">
				<div class="col-xs-12 pd0mg0" style="display:none;">
					@include('prescricao.psa')
				</div>
				<div class="col-xs-12 col-md-12 pd0mg0" style="display:none;">
					@include('prescricao.projeto')
				</div>
				<div class="col-xs-12 col-md-12 pd0mg0">
					@include('prescricao.musculacao')
				</div>
				<div class="col-xs-12 col-md-12 pd0mg0" style="display:none;">
					@include('prescricao.modalidades.crossTraining')
				</div>
				<div class="col-xs-12 col-md-12 pd0mg0" style="display:none;">
					@include('prescricao.modalidades.corrida')
				</div>
				<div class="col-xs-12 col-md-12 pd0mg0" style="display:none;">
					@include('prescricao.modalidades.ciclismo')
				</div>
				<div class="col-xs-12 col-md-12 pd0mg0" style="display:none;">
					@include('prescricao.modalidades.natacao')
				</div>
				<div class="col-xs-12 col-md-12 pd0mg0" style="display:none;">
					@include('prescricao.modalidades.nutricao')
				</div>
			</div>
			<div class="col-xs-12 col-sm-8 pd0mg0" ng-hide="exibePrescricao">
				<!-- <div class="alert alert-warning">Selecione um cliente ao lado para começar!</div> -->
			</div>
		</div>
		<div style="height:50px;"></div>
	</div>
@endsection
@section('css')
	<!-- Select 2 -->
	{!! Html::style("assets_admin/vendor/select2/select2.css") !!}
	<!-- jsTree -->
	{!! Html::style("tema_assets/css/plugins/jsTree/style.min.css") !!}
@endsection
@section('scripts')
	<!-- Mainly scripts -->
	{!! HTML::script("tema_assets/js/plugins/metisMenu/jquery.metisMenu.js") !!}
	<!-- jQuery UI custom -->
	{!! HTML::script("tema_assets/js/jquery-ui.custom.min.js") !!}
	<!-- iCheck -->
	{!! HTML::script("tema_assets/js/plugins/iCheck/icheck.min.js") !!}
	<!-- Nestable List -->
	{!! HTML::script("tema_assets/js/plugins/nestable/jquery.nestable.js") !!}
	<!-- Select 2 -->
	{!! Html::script("assets_admin/vendor/select2/select2.js") !!}
	<!-- jsTree -->
	{!! HTML::script("tema_assets/js/plugins/jsTree/jstree.min.js") !!}

	<?php if(isset($headers['client_id'])) { ?>
		<script type="text/javascript">
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')
				}
			});

			function pad(value, length) {
				return (value.toString().length < length) ? pad("0"+value, length):value;
			}

			function gravaCalendario(diaatual, desc, tipo) {
				var mesatual = $('#mesatual').val();
				var anoatual = $('#anoatual').val();
				diaatual = pad(diaatual, 2);
				var dataatual = diaatual + '/' + mesatual + '/' + anoatual;
				if(desc != '') {
					console.log('data: ' + dataatual + ' desc: ' + desc);

					$.post('../../api/prescricao/gravaCalendario', {
						desc: desc, dataatual: dataatual, tipo: tipo, client_id: <?= $headers['client_id'] ?>
					}, function(retorno) {
						console.log(retorno);
					});
				}
			}

			function btnprescicaoSalvar() {
				swal("Ok!", "Prescrição salva com sucesso!", "success");
			}

			function teste() {
				swal("Ok!", "teste", "success");
			}

			$(document).ready(function() {
				function buscaCalendario(newMes, newAno, tipo) {
					var classTipo = '';
					switch(tipo) {
						case 'cross':
							classTipo = $('.calendarioCross');
							break;
						case 'corrida':
							classTipo = $('.calendarioCorrida');
							break;
						case 'ciclismo':
							classTipo = $('.calendarioCiclismo');
							break;
						case 'natacao':
							classTipo = $('.calendarioNatacao');
							break;
					}
					$(classTipo).html('Aguarde, carregando calendario...');
					$(".btnprescicaoSalvar").hide();
					$.post('../../api/prescricao/getCaledario', {
						mes: newMes, ano: newAno, tipo: tipo, client_id: <?= $headers['client_id'] ?>
					}, function(retorno) {
						$(classTipo).html('');
						$(classTipo).append(retorno);
						$(".anteriorMes").prop("disabled", false);
						$(".proximoMes").prop("disabled", false);
						$(".btnprescicaoSalvar").show();
					});
				}

				$('.anteriorMes').click(function() {
					$(".proximoMes").prop("disabled", true);
					$(".anteriorMes").prop("disabled", true);
					var mesatual = $('#mesatual').val();
					var anoatual = $('#anoatual').val();
					mesatual = parseInt(mesatual)-1;
					if(mesatual < 1) {
						mesatual = '12';
						anoatual = parseInt(anoatual)-1;
					}
					buscaCalendario(mesatual, anoatual, 'cross');
					buscaCalendario(mesatual, anoatual, 'corrida');
					buscaCalendario(mesatual, anoatual, 'ciclismo');
					buscaCalendario(mesatual, anoatual, 'natacao');
				});

				$('.proximoMes').click(function() {
					$(".proximoMes").prop("disabled", true);
					$(".anteriorMes").prop("disabled", true);
					var mesatual = $('#mesatual').val();
					var anoatual = $('#anoatual').val();
					mesatual = parseInt(mesatual)+1;
					if(mesatual > 12) {
						mesatual = '01';
						anoatual = parseInt(anoatual)+1;
					}
					buscaCalendario(mesatual, anoatual, 'cross');
					buscaCalendario(mesatual, anoatual, 'corrida');
					buscaCalendario(mesatual, anoatual, 'ciclismo');
					buscaCalendario(mesatual, anoatual, 'natacao');
				});

				buscaCalendario(<?= date('m') ?>, <?= date('Y') ?>, 'cross');
				buscaCalendario(<?= date('m') ?>, <?= date('Y') ?>, 'corrida');
				buscaCalendario(<?= date('m') ?>, <?= date('Y') ?>, 'ciclismo');
				buscaCalendario(<?= date('m') ?>, <?= date('Y') ?>, 'natacao');

				$('#data_revisao_treino .input-group.date').datepicker({
					todayBtn: "linked",
					keyboardNavigation: false,
					forceParse: false,
					calendarWeeks: true,
					startDate: 'today',
					endDate: '+90d',
					autoclose: true
				});

				$('#data_1 .input-group.date').datepicker({
					todayBtn: "linked",
					keyboardNavigation: false,
					forceParse: false,
					calendarWeeks: true,
					startDate: 'today',
					autoclose: true
				});

				$('#data_2 .input-group.date').datepicker({
					todayBtn: "linked",
					keyboardNavigation: false,
					forceParse: false,
					calendarWeeks: true,
					startDate: 'today',
					autoclose: true
				});
			});
		</script>
	<?php } ?>
@endsection