<div class="ibox collapsed">
	<div class="ibox-title">
		<div class="ibox-tools">
			<a class="collapse-link"  ng-click="listaPsasAluno()">
				<h5>PSA - <span>Programa Semanal de Atividade</span></h5>
				<i class="fa fa-chevron-up" ng-click="listaPsasAluno()"></i>
			</a>
		</div>
	</div>
	<div class="ibox-content">
		<div class="row">
			<table class="table table-bordered table-hover table-condensed table-responsive">
				<thead>
					<tr>
						<td style="font-weight:bold;">Domingo</td>
						<td style="font-weight:bold;">Segunda</td>
						<td style="font-weight:bold;">Terça</td>
						<td style="font-weight:bold;">Quarta</td>
						<td style="font-weight:bold;">Quinta</td>
						<td style="font-weight:bold;">Sexta</td>
						<td style="font-weight:bold;">Sábado</td>
						<td></td>
					</tr>
				</thead>
				<tbody>
					<tr ng-repeat="psa in psas" class="table-bottom">
						<td style="padding:0;">
							<span>
								<select class="form-control" ng-model="psa.dom_atv">
									<option value="">Programa</option>
									<option ng-repeat="programa in programas" ng-value="@{{programa.id}}">@{{programa.nmprograma}}</option>
								</select>  
								<div class="input-group clockpicker" data-autoclose="true">
									<input type="text" ng-model="psa.dom_hora" class="form-control" value="" style="text-align:center;" placeholder="--:--">
								</div>
							</span>
						</td>
						<td style="padding:0;">
							<span>
								<select class="form-control" ng-model="psa.seg_atv">
									<option value="">Programa</option>
									<option ng-repeat="programa in programas" ng-value="@{{programa.id}}">@{{programa.nmprograma}}</option>
								</select>  
								<div class="input-group clockpicker" data-autoclose="true">
									<input type="text" ng-model="psa.seg_hora" class="form-control" value="" style="text-align:center;" placeholder="--:--">
								</div>
							</span>
						</td>
						<td style="padding:0;">
							<span>
								<select class="form-control" ng-model="psa.ter_atv">
									<option value="">Programa</option>
									<option ng-repeat="programa in programas" ng-value="@{{programa.id}}">@{{programa.nmprograma}}</option>
								</select>  
								<div class="input-group clockpicker" data-autoclose="true">
									<input type="text" ng-model="psa.ter_hora" class="form-control" value="" style="text-align:center;" placeholder="--:--">
								</div>
							</span>
						</td>
						<td style="padding:0;">
							<span>
								<select class="form-control" ng-model="psa.qua_atv">
									<option value="">Programa</option>
									<option ng-repeat="programa in programas" ng-value="@{{programa.id}}">@{{programa.nmprograma}}</option>
								</select>  
								<div class="input-group clockpicker" data-autoclose="true">
									<input type="text" ng-model="psa.qua_hora" class="form-control" value="" style="text-align:center;" placeholder="--:--">
								</div>
							</span>
						</td>
						<td style="padding:0;">
							<span>
								<select class="form-control" ng-model="psa.qui_atv">
									<option value="">Programa</option>
									<option ng-repeat="programa in programas" ng-value="@{{programa.id}}">@{{programa.nmprograma}}</option>
								</select>  
								<div class="input-group clockpicker" data-autoclose="true">
									<input type="text" ng-model="psa.qui_hora" class="form-control" value="" style="text-align:center;" placeholder="--:--">
								</div>
							</span>
						</td>
						<td style="padding:0;">
							<span>
								<select class="form-control" ng-model="psa.sex_atv">
									<option value="">Programa</option>
									<option ng-repeat="programa in programas" ng-value="@{{programa.id}}">@{{programa.nmprograma}}</option>
								</select>  
								<div class="input-group clockpicker" data-autoclose="true">
									<input type="text" ng-model="psa.sex_hora" class="form-control" value="" style="text-align:center;" placeholder="--:--">
								</div>
							</span>
						</td>
						<td style="padding:0;">
							<span>
								<select class="form-control" ng-model="psa.sab_atv">
									<option value="">Programa</option>
									<option ng-repeat="programa in programas" ng-value="@{{programa.id}}">@{{programa.nmprograma}}</option>
								</select>  
								<div class="input-group clockpicker" data-autoclose="true">
									<input type="text" ng-model="psa.sab_hora" class="form-control" value="" style="text-align:center;" placeholder="--:--">
								</div>
							</span>
						</td>
						<td>
							<button class="btn btn-danger pull-left" ng-click="delPsa(psa.id)" ng-disabled="delPsaBtn"><i class="fa fa-trash" aria-hidden="true"></i></button>
						</td>
					</tr>
				</tbody>
			</table>
			<div class="pull-left">
				<button class="btn btn-primary" ng-click="addPsa()" ng-disabled="addPsaBtn"><i class="fa fa-plus" aria-hidden="true"></i></button>
			</div>
			<span class="pull-right">
				<button class="btn btn-primary" ng-click="salvarPsa(psas)" ng-disabled="salvarPsaBtn"><i class="fa fa-check"></i> Salvar</button>
			</span>
		</div>
	</div>
</div>