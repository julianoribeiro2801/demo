@extends('layouts.app')



@section('content')

	@push('scripts')
        {!! Html::script("js/alunoController1.js") !!}      
		{!! Html::script("js/prescricaoController.js") !!}
		{!! Html::script("js/projetoController.js") !!}
        {!! Html::script("js/exercicioController.js") !!}                


	@endpush



	<style>

		.fixed {

			position: fixed;
			top: 15px;
			padding-right: 39px;

		}

		#dia_atual, #dia_atual textarea {

			background: #f4f4f4;

		}

		.tarefa_finalizada {

			text-decoration: line-through;

		}

		.table-responsive {

			display: block;

			overflow-x: auto;

		}

		.dd-handle {

			overflow: hidden;

		}

		@media screen and (max-width: 400px) {

			.form-control, .single-line {

				padding: 6px 3px !important;

				font-size: 12px !important;

			}

		}

		.table-bottom {

			border-bottom:4px solid #fff;

		}

		.jstree-open > .jstree-anchor > .fa-folder:before {

			content: "\f07c";

		}

		.jstree-default .jstree-icon.none {

			width: 0;

		}

		.divOculta {

			display: none;

		}

	</style>



	<div ng-controller="prescricaoController">

		<div class="row" style="margin-top:20px;">

			<div id="top-bar" class="col-xs-12 col-md-3 pd0mg0">

				<div class="ibox float-e-margins">

					<div class="ibox-title">

						<h5>Clientes</h5>

						<div class="ibox-tools">

							<a class="collapse-link"><i class="fa fa-chevron-up"></i></a>

						</div>

					</div>



	<div class="ibox-content">


	<input type="hidden" ng-model="selectTipoUser"  ng-init="selectTipoUser='<?= (isset($headers['funcionario'])) ? $headers['funcionario'] : '' ?>'" >

	<div class="row">
			<div class="col-xs-12 col-md-4">
                <div>Pesquisar por: </div>
            </div>
			 <div class="col-xs-12 col-md-8">
   
                <div class="pull-right">
                   <label class="radio-inline">
                   	<input type="radio"  
                   		ng-model="selectTipoUser"
	                   	value="clientes" 
	                   	ng-click="radioURL(selectTipoUser)"  
	               	<?= (isset($headers['funcionario']) && $headers['funcionario']=='clientes') ? 'checked' : ''; ?> 
	                <?= (isset($headers['funcionario'])) ? '' : 'checked'; ?> >

	                   Clientes
	               </label>

				   <label class="radio-inline">
				   	<input type="radio" 
				   	ng-model="selectTipoUser"
				   	  value="funcionarios"  <?= (isset($headers['funcionario']) && $headers['funcionario']=='funcionarios') ? 'checked' : ''; ?>
				   	 ng-click="radioURL(selectTipoUser)">
				   	Funcionários
				   </label>

                </div>
            
        </div>

	</div>

					



<div class="form-group" style="margin-top:10px">


 	<select class="form-control clienteSelect" ng-model="client.id" ng-change="selectClient(client.id,client.genero)" data-placeholder="Selecione..." id="clienteSelect" tabindex="2">

 		<option value="" selected>Selecione</option>
 		<option ng-repeat="client in clients" value="@{{client.id}}">@{{client.name}}</option>
 	</select>
</div>


							





						<div class="contact-box" ng-show="exibePrescricao" >

							

							<div class="col-xs-12" >

								<div class="img-circle"
									 style="background-image: url(/uploads/avatars/user-@{{client.avatar ? client.id : 'a4'}}.jpg);    
									 		 width: 120px;
											 height: 120px;
											 background-size: cover;
											 background-position: center top; 
											 margin: 0 auto 10px auto;">	 
									</div>

								<h3 style="text-align: center;"><strong>@{{client.name}}</strong></h3>

								Perfil: <strong>@{{client.description}}</strong><br>

								Busca: <strong>@{{client.objective}}</strong><br>

								Motivado por: <strong>@{{client.motivation}}</strong><br>

								Seu objetivo é: <strong>@{{client.nmobjetivo}}</strong><br>

								Genero: <strong>@{{client.dados.genero == 'M' ? 'Masculino' : 'Feminino'}}</strong><br>

								<hr>
								<address>

									<strong>Contato</strong><br>

									E-mail: @{{client.email}}<br>

									Telefone: @{{client.telefone}}

								</address>
                                                                
                                  {{-- $scope.observacao_medica  = @{{ observacao_medica.length   }}           --}}
                                <a ng-if="client.id" ng-click="selectTeste(client)" data-target="#editar_cliente" data-toggle="modal" class="btn btn-primary btn-sm btn-block ng-scope"><i class="fa fa-edit" style="margin-right:5px;"></i> Editar Dados</a>

                                <a  ng-if="client.id && (observacao_medica.length) > 0" ng-click="getObservacao(client.id)" 
                                	data-target="#obs_medica" data-toggle="modal" class="btn btn-danger btn-sm btn-block ng-scope">
                                	<i class="fa fa-medkit" style="margin-right:5px;"></i> Observações médicas</a>

                                <a ng-if="client.id && (observacao_medica.length) < 2"ng-click="getObservacao(client.id)" data-target="#obs_medica" data-toggle="modal" class="btn btn-default btn-sm btn-block ng-scope"><i class="fa fa-medkit" style="margin-right:5px;"></i> Observações médicas</a>
                             	

							</div>

							<div class="clearfix"></div>

						</div>

					</div>

				</div>

			</div>

			<div class="col-xs-12 col-sm-9 pd0mg0 pull-right" ng-show="exibePrescricao">

				<input type="hidden" runat="server" ng-init="inputVal='<?= (isset($headers['client_id'])) ? $headers['client_id'] : '' ?>'" ng-model="inputVal">



				<div class="col-xs-12 pd0mg0">

					@include('prescricao.psa')


				</div>



				<div class="col-xs-12 pd0mg0">

					@include('prescricao.programa')

				</div>



				<div class="col-xs-12 col-md-12 pd0mg0">

					@include('prescricao.projeto')

				</div>

				<div class="col-xs-12 col-md-12 pd0mg0">

					@include('prescricao.musculacao')

				</div>

				<div class="col-xs-12 col-md-12 pd0mg0">

					@include('prescricao.modalidades.crossTraining')

				</div>

				<div class="col-xs-12 col-md-12 pd0mg0">

					@include('prescricao.modalidades.corrida')

				</div>

				<div class="col-xs-12 col-md-12 pd0mg0">

					@include('prescricao.modalidades.ciclismo')

				</div>

				<div class="col-xs-12 col-md-12 pd0mg0">

					@include('prescricao.modalidades.natacao')

				</div>

				<div class="col-xs-12 col-md-12 pd0mg0">

					@include('prescricao.modalidades.nutricao')

				</div>

			</div>

			<div class="col-xs-12 col-sm-8 pd0mg0" ng-hide="exibePrescricao">

				<!-- <div class="alert alert-warning">Selecione um cliente ao lado para começar!</div> -->

			</div>

		</div>

            

		<div style="height:50px;"></div>
                @include('prescricao.configuracoes.modal_exercicios')
                <div ng-controller="alunoController1">
                @include('admin.alunos._form_cliente')</div>

	</div>

  



        

        
    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script> -->

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.js"></script> 



    

<script type="text/javascript">

  $('tbody').sortable();

</script>    

     

@endsection

@section('css')

	<!-- Select 2 -->

	{!! Html::style("assets_admin/vendor/select2/select2.css") !!}

	<!-- jsTree -->

	{!! Html::style("tema_assets/css/plugins/jsTree/style.min.css") !!}

	<!-- cadastro de exercicios -->

	{!! Html::style("assets_admin/vendor/pnotify/pnotify.custom.css") !!}

@endsection



@section('scripts')



		<!-- Data picker -->

	{!! Html::script("tema_assets/js/plugins/datapicker/bootstrap-datepicker.js") !!}

	{!! HTML::script("tema_assets/js/plugins/datapicker/locales/bootstrap-datepicker.pt-BR.js") !!}



		<!-- Clock picker -->

	{!! HTML::script("tema_assets/js/plugins/clockpicker/clockpicker.js") !!}

	<!-- Mainly scripts -->

	{!! HTML::script("tema_assets/js/plugins/metisMenu/jquery.metisMenu.js") !!}

	<!-- jQuery UI custom -->

	{!! HTML::script("tema_assets/js/jquery-ui.custom.min.js") !!}

	<!-- iCheck -->

	{!! HTML::script("tema_assets/js/plugins/iCheck/icheck.min.js") !!}

	<!-- Nestable List -->
	{!! HTML::script("tema_assets/js/plugins/nestable/jquery.nestable.js") !!}

	<!-- Select 2 -->
	{!! Html::script("assets_admin/vendor/select2/select2.js") !!}

	<!-- jsTree -->
	{!! HTML::script("tema_assets/js/plugins/jsTree/jstree.min.js") !!}

	<!-- cadastro de exercicios -->

	{!! HTML::script("assets_admin/vendor/jquery-form/jquery.form.min.js") !!}

	{!! HTML::script("assets_admin/vendor/jquery-validation/jquery.validate.js") !!}

	{!! HTML::script("assets_admin/javascripts/forms/exercicioTreino.validation.js") !!}

	{!! Html::script("assets_admin/vendor/pnotify/pnotify.custom.js") !!}



       {{--  @todo MASK Input estava dando conflito com date piker
       {!! HTML::script("assets_admin/javascripts/jQueryMask/jquery.mask.js") !!}

        {!! HTML::script("assets_admin/javascripts/jQueryMask/jquery.mask.min.js") !!}

        {!! HTML::script("assets_admin/javascripts/jQueryMask/jquery.mask.test.js") !!} --}}

      

        

	<?php if(isset($headers['client_id'])) { ?>

		<script type="text/javascript">

			$.ajaxSetup({

				headers: {

					'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')

				}

			});



			var mq = $(window).width();



			if(mq >= 560) {

				$(window).scroll(function(){

					var sticky = $('#top-bar'),

						scroll = $(window).scrollTop();



					if (scroll >= 180) sticky.addClass('fixed');

					else sticky.removeClass('fixed');

				});

			}



			function pad(value, length) {

				return (value.toString().length < length) ? pad("0"+value, length):value;

			}



			function gravaCalendario(diaatual, desc, tipo) {
                            
				var mesatual = $('#mesatual').val();

				var anoatual = $('#anoatual').val();

				diaatual = pad(diaatual, 2);

				var dataatual = diaatual + '/' + mesatual + '/' + anoatual;

				// if(desc != '') {

					console.log('data: ' + dataatual + ' desc: ' + desc);



					$.post('/admin/api/prescricao/gravaCalendario', {

						idtreino: 0, desc: desc, dataatual: dataatual, tipo: tipo, client_id: <?= $headers['client_id'] ?>

					}, function(retorno) {

						console.log(retorno);

					});

				// }

			}



			function btnprescricaoSalvar() {
                                //alert('' + nome_treino);
                                  /*  $.post('../../api/prescricao/upTreinoModealidade', {

					nome_treino: nome_treino, sexo: sexo, objetivo: objetivo, nivel: nivel ,client_id: <?= $headers['client_id'] ?>

					}, function(retorno) {

						console.log(retorno);

					});
                        */


				swal("Ok!", "Prescrição salva com sucesso!", "success");

			}
                        



			function maisProjetos() {

				$(".divOculta").each(function() {

					$(this).css("display","block");

				});

				$(".btn-mais-projetos").hide();

				$(".btn-voltar-projetos").show();

			}



			function menosProjetos() {

				$(".divOculta").each(function() {

					$(this).css("display","none");

				});

				$(".btn-mais-projetos").show();

				$(".btn-voltar-projetos").hide();



			}
                        
                                function getCalendario(tipo){
                                  
                                        buscaCalendarioNovo(<?= date('m') ?>, <?= date('Y') ?>, tipo);
                                    
                                }
    
                        	function buscaCalendarioNovo(newMes, newAno, tipo) {


					var classTipo = '';

					switch(tipo) {

						case 'cross':

							classTipo = $('.calendarioCross');

							break;

						case 'corrida':

							classTipo = $('.calendarioCorrida');

							break;

						case 'ciclismo':

							classTipo = $('.calendarioCiclismo');

							break;

						case 'natacao':

							classTipo = $('.calendarioNatacao');

							break;

					}

					$(classTipo).html('Aguarde, carregando calendario...');

					$(".btnprescicaoSalvar").hide();

					$.post('/admin/api/prescricao/getCaledario', {

						mes: newMes, ano: newAno, tipo: tipo, client_id: <?= $headers['client_id'] ?>

					}, function(retorno) {

						$(classTipo).html('');

						$(classTipo).append(retorno);

						$(".anteriorMes").prop("disabled", false);

						$(".proximoMes").prop("disabled", false);

						$(".btnprescicaoSalvar").show();

					});

				}
			$(document).ready(function() {



                        	/*function buscaCalendario(newMes, newAno, tipo) {

					var classTipo = '';

					switch(tipo) {

						case 'cross':

							classTipo = $('.calendarioCross');

							break;

						case 'corrida':

							classTipo = $('.calendarioCorrida');

							break;

						case 'ciclismo':

							classTipo = $('.calendarioCiclismo');

							break;

						case 'natacao':

							classTipo = $('.calendarioNatacao');

							break;

					}

					$(classTipo).html('Aguarde, carregando calendario...');

					$(".btnprescicaoSalvar").hide();

					$.post('/admin/api/prescricao/getCaledario', {

						mes: newMes, ano: newAno, tipo: tipo, client_id: <?= $headers['client_id'] ?>

					}, function(retorno) {

						$(classTipo).html('');

						$(classTipo).append(retorno);

						$(".anteriorMes").prop("disabled", false);

						$(".proximoMes").prop("disabled", false);

						$(".btnprescicaoSalvar").show();

					});

				}*/



				$('.anteriorMes').click(function() {

					$(".proximoMes").prop("disabled", true);

					$(".anteriorMes").prop("disabled", true);

					var mesatual = $('#mesatual').val();

					var anoatual = $('#anoatual').val();

					mesatual = parseInt(mesatual)-1;

					if(mesatual < 1) {

						mesatual = '12';

						anoatual = parseInt(anoatual)-1;

					}

					buscaCalendarioNovo(mesatual, anoatual, 'cross');

					buscaCalendarioNovo(mesatual, anoatual, 'corrida');

					buscaCalendarioNovo(mesatual, anoatual, 'ciclismo');

					buscaCalendarioNovo(mesatual, anoatual, 'natacao');

				});

				$('.proximoMes').click(function() {

					$(".proximoMes").prop("disabled", true);

					$(".anteriorMes").prop("disabled", true);

					var mesatual = $('#mesatual').val();

					var anoatual = $('#anoatual').val();

					mesatual = parseInt(mesatual)+1;

					if(mesatual > 12) {

						mesatual = '01';

						anoatual = parseInt(anoatual)+1;

					}

					buscaCalendarioNovo(mesatual, anoatual, 'cross');

					buscaCalendarioNovo(mesatual, anoatual, 'corrida');

					buscaCalendarioNovo(mesatual, anoatual, 'ciclismo');

					buscaCalendarioNovo(mesatual, anoatual, 'natacao');

				});



				/*buscaCalendarioNovo(<?= date('m') ?>, <?= date('Y') ?>, 'cross');

				buscaCalendarioNovo(<?= date('m') ?>, <?= date('Y') ?>, 'corrida');

				buscaCalendarioNovo(<?= date('m') ?>, <?= date('Y') ?>, 'ciclismo');

				buscaCalendarioNovo(<?= date('m') ?>, <?= date('Y') ?>, 'natacao');*/



				$('#data_revisao_treino .input-group.date').datepicker({

					todayBtn: "linked",

					keyboardNavigation: false,

					forceParse: false,

					calendarWeeks: true,

					startDate: 'today',

					endDate: '+90d',

					autoclose: true

				});



				$('#data_1 .input-group.date').datepicker({

					todayBtn: "linked",

					keyboardNavigation: false,

					forceParse: false,

					calendarWeeks: true,

					startDate: 'today',

					autoclose: true

				});



				$('#data_2 .input-group.date').datepicker({

					todayBtn: "linked",

					keyboardNavigation: false,

					forceParse: false,

					calendarWeeks: true,

					startDate: 'today',

					autoclose: true

				});



				/*$('#checkbox').click(function(){

					if($(this).is(':checked')){

						$('#modalNomeTreinoPadrao').modal('show');

					}

				});*/

                                

                                $('#data .input-group.date').datepicker({

                                        todayBtn: "linked",

                                        keyboardNavigation: false,

                                        forceParse: false,

                                        calendarWeeks: true,

                                        //endDate: 'today',

                                        autoclose: true

                                });                                

			}
                                );

		</script>









		 <script>

         $(document).ready(function(){



             var updateOutput = function (e) {

                 var list = e.length ? e : $(e.target),

                         output = list.data('output');

                 if (window.JSON) {

                     output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));



                     var jsonFica = window.JSON.stringify(list.nestable('serialize');

                     	

                 } else {

                     output.val('O suporte do navegador JSON é necessário para esta demo.');

                 }

             };



             // activate Nestable for list 1

             $('#nestable').nestable({

                 group: 1

             }).on('change', updateOutput);



             

             // saída de dados serializados iniciais

             updateOutput($('#nestable').data('output', $('#nestable-output')));

             



             $('#nestable-menu').on('click', function (e) {

                 var target = $(e.target),

                         action = target.data('action');

                 if (action === 'expand-all') {

                     $('.dd').nestable('expandAll');

                 }

                 if (action === 'collapse-all') {

                     $('.dd').nestable('collapseAll');

                 }

             });



         });

    </script>



	<?php } ?>

@endsection