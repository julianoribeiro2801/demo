<div class="ibox collapsed">
    <div class="ibox-title">
        <div class="ibox-tools">
            <a class="collapse-link" ng-click="listaProjetosAluno()">
                <h5>
                    Projeto / Etapas
                </h5>
                <i class="fa fa-chevron-up" ng-click="listaProjetosAluno()">
                </i>
            </a>
        </div>
    </div>
    <div class="ibox-content">
        <span class="pull-left" style="margin-top:-6px;">
            <button class="btn btn-primary btn-sm" ng-click="addProjeto()" ng-disabled="addProjetoBtn">
                <i class="fa fa-plus-square" style="margin-right:5px;">
                </i>
                Novo Projeto
            </button>
        </span>
       
        <div style="clear:both;">
        </div>
        <div class="dd">
            <ol class="dd-list">
                <li class="dd-item" ng-class="projeto.projeto_status == 'I' ? 'divOculta' : ''" ng-repeat="projeto in projetos | orderBy : 'id' : true">
                    <div class="dd-handle" ng-hide="editingData[projeto.id]">
                        <span ng-class="projeto.projeto_status == 'I' ? 'tarefa_finalizada' : ''">
                            @{{projeto.projeto_nome | uppercase}}
                        </span>
                        <span class="pull-right">
                            @{{projeto.created_at1}} até @{{projeto.projeto_data1}}
                            <button class="btn btn-sm" ng-class="projeto.projeto_status == 'I' ? '' : 'btn-primary'" ng-click="alterProjeto(projeto)">
                                <i class="fa fa-pencil">
                                </i>
                            </button>
                            <button class="btn btn-sm" ng-class="projeto.projeto_status == 'I' ? '' : 'btn-danger'" ng-click="delProjeto(projeto.id)">
                                <i class="fa fa-trash">
                                </i>
                            </button>

                            <span class="pull-right" style="    margin-left: 3px;">

                                <button class="btn btn-primary btn-sm visible-xs" ng-click="addEtapa(projeto.id)" ng-disabled="addProjetoBtn">
                                    <i class="fa fa-plus-square" style="margin-right:5px;">
                                    </i> Etapa
                                </button>

                                <button class="btn btn-primary btn-sm hidden-xs" ng-click="addEtapa(projeto.id)" ng-disabled="addProjetoBtn">
                                    <i class="fa fa-plus-square" style="margin-right:5px;">
                                    </i>
                                    Nova Etapa
                                </button>
                            </span>
                        </span>
                    </div>
                    <div class="dd-handle" ng-show="editingData[projeto.id]">
                        <div class="row">
                            <div class="col-xs-6 col-sm-8">
                                <div style="margin-bottom:0;">
                                    <input class="form-control" ng-model="projeto.projeto_nome" type="text" placeholder="Nome do projeto">
                                    </input>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-4">
                                <div class="form-group" id="data_4" style="margin-bottom:0; float: left">
                                    <div class="input-group date">
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar">
                                            </i>
                                        </span>
                                        <input class="form-control"  data-mask="99/99/9999" style="width: 87%;" ng-model="projeto.projeto_data" type="text">
                                        </input>
                                    </div>
                                </div>
                                <span class="input-group-btn" style="position: absolute;left: 84%;z-index: 2;top: 0;float: left;">
                                    <button class="btn" ng-class="projeto.projeto_status == 'I' ? '' : 'btn-primary'" ng-click="updateProjeto(projeto)" type="button">
                                        <i class="fa fa-save">
                                        </i>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <ol class="dd-list" ng-repeat="etapa in etapas | orderBy : 'id' : true" ng-show="projeto.projeto_status == 'A'">
                        <li class="dd-item">
                            <div class="dd-handle" ng-hide="editingDataEtapa[etapa.id]">
                                
                                <span ng-class="etapa.etapa_status == 'I' ? 'tarefa_finalizada' : ''" style="font-weight:normal;text-transform:capitalize;">
                                    @{{etapa.etapa_nome}}
                                </span>
                                <span class="pull-right">
                                    @{{etapa.created_at1}} até @{{etapa.etapa_data1}}


                                    <button class="btn btn-sm" ng-class="etapa.etapa_status == 'I' ? '' : 'btn-primary'" ng-click="alterEtapa(etapa, projeto.projeto_data)">
                                    <i class="fa fa-pencil">
                                    </i>
                                </button>

                                    <button class="btn btn-sm" ng-class="etapa.etapa_status == 'I' ? '' : 'btn-danger'" ng-click="delEtapa(etapa.id)">
                                        <i aria-hidden="true" class="fa fa-trash">
                                        </i>
                                    </button>


                                </span>
                            </div>
                            <div class="dd-handle" ng-show="editingDataEtapa[etapa.id]">
                                <div class="row">
                                    <div class="col-xs-3 col-sm-3">
                                        <div  style="margin-bottom:0;">
		
                                            <select class="form-control" ng-model="etapa.etapa_categoria">
                                                <option value="">
                                                    Selecione a categoria
                                                </option>
                                                <option ng-repeat="cat_etapa in cat_etapaVetor" ng-value="cat_etapa.valor">
                                                    @{{cat_etapa.texto}}
                                                </option>

                                            </select>

                                        </div>
                                    </div>
                                    <div class="col-xs-3 col-sm-5" style="padding:0">
                                        <div style="margin-bottom:0;">
                                            <input class="form-control" ng-model="etapa.etapa_nome" type="text" placeholder="Nome da etapa">
                                            </input>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-4">
                                        <div class="form-group" id="data_3" style="margin-bottom:0;">
                                            <div class="input-group date">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-calendar">
                                                    </i>
                                                </span>
                                                <input class="form-control"  data-mask="99/99/9999" style="width: 87%;" ng-model="etapa.etapa_data" type="text">
                                                </input>

                                            
                                            </div>
                                        </div>

                                         <span class="input-group-btn"  style="position: absolute;left: 84%;z-index: 2;top: 0;float: left;">
                                                <button class="btn" ng-class="etapa.etapa_status == 'I' ? '' : 'btn-primary'" ng-click="updateEtapa(etapa,projeto.id )">
                                                    <i class="fa fa-save">
                                                    </i>
                                                </button>
                                            </span>

                                    </div>
                                </div>
                            </div>
                        </li>
                    </ol>
                </li>
            </ol>
            <button class="btn btn-sm btn-mais-projetos btn-link" onclick="maisProjetos()" style="float:right;">
                mais projetos...
            </button>

             <button class="btn btn-sm btn-voltar-projetos btn-link" onclick="menosProjetos()" style="float:right; display: none;">
                < voltar
            </button>

            <div class="clearfix">
            </div>
        </div>
    </div>
</div>