@extends('layouts.app')
@section('content')	
	<style>
		.ibox-title h5 {
			color: #000;
		}
	</style>
        
	<div style="overflow: auto; height: 4000px">
		<div class="row" style="margin-top: 20px">
			<div class="col-xs-12 col-md-6">
				@include('prescricao.configuracoes.programas')
                                
				@include('prescricao.configuracoes.aulas')

				@include('prescricao.configuracoes.grupos_exercicios')
			</div>
			<div class="col-xs-12 col-md-6">
				@include('prescricao.configuracoes.exercicios')
			</div>
		</div>
	</div>
@endsection
@section('css')
	{!! Html::style("assets_admin/vendor/pnotify/pnotify.custom.css") !!}
@endsection
@section('scripts')
	{!! HTML::script("assets_admin/vendor/jquery-form/jquery.form.min.js") !!}
	{!! HTML::script("assets_admin/vendor/jquery-validation/jquery.validate.js") !!}
	{!! HTML::script("assets_admin/javascripts/forms/exercicio.validation.js") !!}
	{!! Html::script("assets_admin/vendor/pnotify/pnotify.custom.js") !!}
@endsection