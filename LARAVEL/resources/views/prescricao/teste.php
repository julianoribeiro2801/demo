
<div class="tabs-container">
    <ul class="nav nav-tabs">
        <li class="active" ng-show="tabsStatus.tab1" ng-click="testess('A')">
            <a data-toggle="tab" href="#tab-1">
                A
            </a>
        </li>
        <li ng-show="tabsStatus.tab2" ng-click="testess('B')">
            <a data-toggle="tab" href="#tab-2">
                B
            </a>
        </li>
        <li ng-show="tabsStatus.tab3" ng-click="testess('C')">
            <a data-toggle="tab" href="#tab-3">
                C
            </a>
        </li>
        <li ng-show="tabsStatus.tab4">
            <a data-toggle="tab" href="#tab-4">
                D
            </a>
        </li>
        <li ng-show="tabsStatus.tab5">
            <a data-toggle="tab" href="#tab-5">
                E
            </a>
        </li>
        <li ng-show="tabsStatus.tab6">
            <a data-toggle="tab" href="#tab-6">
                F
            </a>
        </li>
        <li ng-show="tabsStatus.tab7">
            <a data-toggle="tab" href="#tab-7">
                G
            </a>
        </li>
    </ul>
    <div class="tab-content">




        <!-- INICIO DA ABA 1 -->
        <div class="tab-pane active" id="tab-1" ng-show="tabsStatus.tab1">

            <div class="panel-body" style="padding:7px 20px;">
                <div class="row clearfix" style="margin-top: 6px;">

                    <table class="table table-bordered pagin-table" id="abaAaa">
                        <tbody ng-model="linhaA">

                            <tr ng-model="tabs" ng-repeat="linhaA in linhasA" style="background:#f5f5f5;" ng-disabled="addLinhaBtn">
                                <td id="ord" ng-hide="true">@{{linhaA.ficha}}</td>
                                <td id="exercicio" ng-hide="true">@{{linhaA.exercicio}}</td>
                                <td id="series" ng-hide="true">@{{linhaA.series}}</td>
                                <td id="repeticoes" ng-hide="true">@{{linhaA.repeticoes}}</td>
                                <td id="medida_duracao" ng-hide="true">@{{linhaA.medida_duracao}}</td>
                                <td id="carga" ng-hide="true">@{{linhaA.carga}}</td>
                                <td id="medida_intensidade" ng-hide="true">@{{linhaA.medida_intensidade}}</td>
                                <td id="intervalo" ng-hide="true">@{{linhaA.intervalo}}</td>   
                                <td style="padding: 0 ">

                                    <!-- EXIBE O NÚMERO de linhas -->
                                    <span class="marcador_exer">
                                        @{{linhaA.id}}
                                    </span>



                                    <!-- INICIO DA LINHA DO EXERCICIO -->
                                    <div class="row" style="margin-right: -6px;">

                                        <div class="col-xs-12 col-sm-1 hidden-xs" style="padding:0;     padding-right: 8px;">
                                            <!-- FOTO -->
                                            <div href="#" data-toggle="tooltip" title="Clique e arreste para mudar de posição" 
                                                 style="background-image:url('{{ url('/')}}/uploads/exercicios/@{{exercicios[changeExercicio(linhaA.exercicio)].dsurlminiatura}}'); 
                                                 height: 80px;
                                                 background-color: #fff;
                                                 background-size: cover;
                                                 background-position: center;
                                                 margin-left: 16px;
                                                 width: 74px;
                                                 float: left;
                                                 margin-right: 4px;
                                                 margin-top: 5px
                                                 ">
                                            </div>
                                        </div>

                                        <div class="col-xs-12 col-sm-11">

                                            {{-- INICIO DA TABELA NOVA  --}}

                                            <table class="table_treino">
                                                <thead>
                                                    <tr>
                                                        <th style="width: 50%;">      

                                                            <select class="form-control selectTreinoNew" required="required">
                                                                <option value="Exercicío Simples" > Exercicío Simples</option>
                                                                <option value="Bi-Série " > Bi-Série  </option>
                                                                <option value="Tri-Série" > Tri-Série </option>
                                                            </select>  
                                                        </th>
                                                        <th style="width: 8%;" class="nobold">Série</th>
                                                        <th style="width: 10%;"> 
                                                            <select class="form-control selectTreinoTransparent" ng-model="linhaA.medida_duracao" ng-disabled="addLinhaBtn" required="required">
                                                                <option ng-repeat="medida_duracao in medida_duracaosVetor" ng-value="medida_duracao.valor">
                                                                    @{{medida_duracao.texto}}
                                                                </option>                                                                
                                                            </select>  
                                                        </th>
                                                        <th style="width: 9%;"> 
                                                            <select class="form-control selectTreinoTransparent" ng-model="linhaA.medida_intensidade" ng-disabled="addLinhaBtn" required="required">
                                                                <option ng-repeat="medida_intensidade in medida_intensidadeVetor" ng-value="medida_intensidade.valor">
                                                                    @{{medida_intensidade.texto}}
                                                                </option>                                                                       
                                                            </select>  
                                                        </th>
                                                        <th style="width: 8.75%;" class="nobold">Intervalo</th>

                                                        <th style="width: 114px">

                                                        </th>

                                                    </tr>
                                                </thead>

                                                <tbody>
                                                    <tr>
                                                        <td> 
                                                            <div class="form-group" style="padding-bottom: 0">
                                                                <div class="input-group" style="width: 100%;">

                                                                    <select class="form-control" id="execicioSelectA@{{linhaA.id}}"  ng-change="changeExercicio(linhaA.exercicio, linhaA)" ng-model="linhaA.exercicio" ng-disabled="addLinhaBtn"  >
                                                                        <option disabled="" selected="" value="">
                                                                            Selecione o Exercício
                                                                        </option>
                                                                        <option ng-repeat="exercicio in exercicios" ng-value="exercicio.id">
                                                                            @{{exercicio.nome_exercicio}}
                                                                        </option>
                                                                    </select>

                                                                    <a class="btn btn-default input-group-addon" data-target="#m_exercicios" data-toggle="modal" ng-disabled="addLinhaBtn">
                                                                        <i aria-hidden="true" class="fa fa-plus">
                                                                        </i>
                                                                    </a>

                                                                </div>
                                                            </div>
                                                        </td>

                                                        <td>  <select class="form-control inputInlineTreinoNew" ng-change="calcTempoA()" ng-model="linhaA.series" ng-disabled="addLinhaBtn">
                                                                <option disabled="" selected="" value="">
                                                                    0x
                                                                </option>
                                                                <option ng-repeat="serie in seriesVetor" ng-value="serie.valor | number">
                                                                    @{{serie.texto}}
                                                                </option>
                                                            </select> </td>
                                                        <td>  <input class="form-control inputInlineTreinoNew" ng-blur="calcTempoA()" ng-model="linhaA.repeticoes" placeholder="0"
                                                                     type="text" ng-disabled="addLinhaBtn"></td>


                                                        <td>   <input class="form-control inputInlineTreinoNew" ng-model="linhaA.carga" placeholder="0" type="text" ng-disabled="addLinhaBtn"></td>
                                                        <td>  <select class="form-control inputInlineTreinoNew" ng-change="calcTempoA()" ng-model="linhaA.intervalo" ng-disabled="addLinhaBtn">
                                                                <option disabled="" selected="" value="">
                                                                    0 seg
                                                                </option>
                                                                <option ng-repeat="intervalo in intervalosVetor" ng-value="intervalo.valor | number">
                                                                    @{{intervalo.texto}}
                                                                </option>
                                                            </select>
                                                        </td>

                                                        <td style="width: 15%">

                                                            <!-- INICIO FECHAR E EXCLUIR -->
                                                            <span class="pull-right" style="    margin-top: -12px;">
                                                                <a class="btn btn-default " style="height: 31px; padding-top: 3px;"
                                                                   data-target="#modalObsA@{{linhaA.id}}" data-toggle="modal" ng-disabled="addLinhaBtn">
                                                                    <i aria-hidden="true" class="fa fa-comment"></i>
                                                                </a>
                                                                <a class="btn btn-danger " style="height: 31px;  padding-top: 3px; border-color: #e5e6e8;"
                                                                   ng-click="removeLinhaA($index, linhaA.ficha)" style=" border: 0;" ng-disabled="addLinhaBtn">
                                                                    <i aria-hidden="true" class="fa fa-trash"></i>
                                                                </a>

                                                            </span>
                                                            <!-- FIM FECHAR E EXCLUIR -->


                                                            <!-- OBSERVAÇÃO DO EXERCICIO TREINO A -->
                                                            <div aria-hidden="true" class="modal inmodal fade" id="modalObsA@{{linhaA.id}}" role="dialog" tabindex="-1">
                                                                <div class="modal-dialog">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <button class="close" data-dismiss="modal" type="button">
                                                                                <span aria-hidden="true">
                                                                                    ×
                                                                                </span>
                                                                                <span class="sr-only">
                                                                                    Fechar
                                                                                </span>
                                                                            </button>
                                                                            <h4 class="modal-title">
                                                                                Observação do Exercício
                                                                            </h4>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <div class="form-group">
                                                                                <input class="form-control" ng-model="linhaA.observacao" placeholder="Informe uma observação" type="text">
                                                                                </input>
                                                                            </div>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button class="btn btn-primary" data-dismiss="modal" type="button">
                                                                                OK
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- modal -->
                                                            <!-- OBSERVAÇÃO DO EXERCICIO TREINO A -->


                                                            <!-- modal -->

                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                            {{-- FIM DA TABELA NOVA  --}}


                                        </div>

                                    </div>
                                </td>
                            </tr>

                        </tbody>
                    </table>
                    <!-- fim tabele -->

                </div>
                <div class="form-group" style="margin-top:10px;">
                    <span style="text-align:left;margin-left:-15px;">
                        <a class="btn btn-primary" ng-click="addLinhaA()" ng-disabled="addLinhaBtn">
                            <i aria-hidden="true" class="fa fa-plus">
                            </i>
                        </a>
                    </span>
                    <span class="pull-right" style="margin-right:-15px;text-align:right;">
                        <b>
                            Tempo estimado @{{tempoestimadoA}}
                        </b>
                        <br> Musculação: @{{tempomusculacaoA}} + Aquecimento: 10 min + Alongamento: 4 min
                        </br>
                    </span>
                </div>
                <div class="clearfix">
                </div>
            </div>
        </div>
        <!-- fim table panel 1 -->



        <!-- INICIO DA ABA 2 -->
        <div class="tab-pane" id="tab-2" ng-show="tabsStatus.tab2">

            <div class="panel-body" style="padding:7px 20px;">
                <div class="row clearfix" style="margin-top: 6px;">

                    <table class="table table-bordered pagin-table" id="abaBbb">
                        <tbody ng-model="linhaB">

                            <tr ng-model="tabs" ng-repeat="linhaB in linhasB" style="background:#f5f5f5;" ng-disabled="salvarTreinoBtn">
                                <td id="ord" ng-hide="true">@{{linhaB.ficha}}</td>
                                <td id="exercicio" ng-hide="true">@{{linhaB.exercicio}}</td>
                                <td id="series" ng-hide="true">@{{linhaB.series}}</td>
                                <td id="repeticoes" ng-hide="true">@{{linhaB.repeticoes}}</td>
                                <td id="medida_duracao" ng-hide="true">@{{linhaB.medida_duracao}}</td>                                
                                <td id="carga" ng-hide="true">@{{linhaB.carga}}</td>
                                <td id="intervalo" ng-hide="true">@{{linhaB.intervalo}}</td>   
                                <td style="padding: 0 ">

                                    <!-- EXIBE O NÚMERO de linhas -->
                                    <span class="marcador_exer">
                                        @{{linhaB.id}}
                                    </span>

                                    <!-- INICIO DA LINHA DO EXERCICIO -->
                                    <div class="row" style="margin-right: -6px;">

                                        <div class="col-xs-12 col-sm-1 hidden-xs" style="padding:0;     padding-right: 8px;">
                                            <!-- FOTO -->
                                            <div href="#" data-toggle="tooltip" title="Clique e arreste para mudar de posição" style="height:50px; background-image:url('{{ url('/')}}/uploads/exercicios/@{{exercicios[changeExercicio(linhaB.exercicio)].dsurlminiatura}}'); background-color:#fff; background-size:cover;background-position:center;      margin-left: 6px;     width: 100%; float: left; margin-right: 4px;">
                                            </div>
                                        </div>

                                        <div class="col-xs-12 col-sm-11">
                                            <div style="height: 5px"></div>
                                            <div class="row">
                                                <div class="col-sm-6" style="padding:5px 2px 0 2px;">

                                                    <div class="form-group" style="padding-bottom: 0">
                                                        <div class="input-group">

                                                            <select class="form-control" id="execicioSelectB@{{linhaB.id}}" ng-change="changeExercicio(linhaB.exercicio)" ng-model="linhaB.exercicio" ng-disabled="addLinhaBtn">
                                                                <option disabled="" selected="" value="">
                                                                    Selecione o Exercício
                                                                </option>
                                                                <option ng-repeat="exercicio in exercicios" ng-value="exercicio.id">
                                                                    @{{exercicio.nome_exercicio}}
                                                                </option>
                                                            </select>

                                                            <a class="btn btn-default input-group-addon" data-target="#m_exercicios" data-toggle="modal" ng-disabled="addLinhaBtn">
                                                                <i aria-hidden="true" class="fa fa-plus">
                                                                </i>
                                                            </a>

                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 col-sm-6" style="padding:5px 2px 0 2px;">
                                                    <div class="form-group" style="padding-bottom: 0">
                                                        <select class="form-control inputInlineTreino" ng-change="calcTempoB()" ng-model="linhaB.series" ng-disabled="addLinhaBtn">
                                                            <option disabled="" selected="" value="">
                                                                Séries
                                                            </option>
                                                            <option ng-repeat="serie in seriesVetor" ng-value="serie.valor | number">
                                                                @{{serie.texto}}
                                                            </option>
                                                        </select>
                                                        <input class="form-control inputInlineTreino" ng-blur="calcTempoB()" ng-model="linhaB.repeticoes" placeholder="Repetições"
                                                               type="text" ng-disabled="addLinhaBtn">

                                                        <select class="form-control inputInlineTreino" ng-model="linhaB.medida_duracao" ng-disabled="addLinhaBtn">
                                                            <option ng-repeat="medida_duracao in medida_duracaosVetor" ng-value="medida_duracao.valor">
                                                                @{{medida_duracao.texto}}
                                                            </option>
                                                        </select>    

                                                        <input class="form-control inputInlineTreino" ng-model="linhaB.carga" placeholder="Carga" type="text" ng-disabled="addLinhaBtn">

                                                        <select class="form-control inputInlineTreino" ng-change="calcTempoB()" ng-model="linhaB.intervalo" ng-disabled="addLinhaBtn">
                                                            <option disabled="" selected="" value="">
                                                                Intervalo
                                                            </option>
                                                            <option ng-repeat="intervalo in intervalosVetor" ng-value="intervalo.valor | number">
                                                                @{{intervalo.texto}}
                                                            </option>
                                                        </select>

                                                        <!-- FECHA  -->

                                                        <!-- INICIO FECHAR E EXCLUIR -->
                                                        <span class="pull-right">
                                                            <a class="btn btn-default " data-target="#modalObsA@{{linhaB.id}}" data-toggle="modal" ng-disabled="addLinhaBtn">
                                                                <i aria-hidden="true" class="fa fa-comment"></i>
                                                            </a>

                                                            <a class="btn btn-danger " ng-click="removeLinhaB($index, linhaB.ficha)" style=" border: 0;" ng-disabled="addLinhaBtn" ng-disabled="addLinhaBtn">
                                                                <i aria-hidden="true" class="fa fa-trash"></i>
                                                            </a>

                                                        </span>
                                                        <!-- FIM FECHAR E EXCLUIR -->



                                                    </div>
                                                </div>

                                                <!-- OBSERVAÇÃO DO EXERCICIO TREINO A -->
                                                <div aria-hidden="true" class="modal inmodal fade" id="modalObsA@{{linhaB.id}}" role="dialog" tabindex="-1">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button class="close" data-dismiss="modal" type="button">
                                                                    <span aria-hidden="true">
                                                                        ×
                                                                    </span>
                                                                    <span class="sr-only">
                                                                        Fechar
                                                                    </span>
                                                                </button>
                                                                <h4 class="modal-title">
                                                                    Observação do Exercício
                                                                </h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="form-group">
                                                                    <input class="form-control" ng-model="linhaB.observacao" placeholder="Informe uma observação" type="text">
                                                                    </input>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button class="btn btn-primary" data-dismiss="modal" type="button">
                                                                    OK
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- modal -->




                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>

                        </tbody>
                    </table>

                    <!-- fim tabele -->

                </div>
                <div class="form-group" style="margin-top:10px;">
                    <span style="text-align:left;margin-left:-15px;">
                        <a class="btn btn-primary" ng-click="addLinhaB()" ng-disabled="addLinhaBtn">
                            <i aria-hidden="true" class="fa fa-plus">
                            </i>
                        </a>
                    </span>
                    <span class="pull-right" style="margin-right:-15px;text-align:right;">
                        <b>
                            Tempo estimado @{{tempoestimadoB}}
                        </b>
                        <br> Musculação: @{{tempomusculacaoB}} + Aquecimento: 10 min + Alongamento: 4 min
                        </br>
                    </span>
                </div>
                <div class="clearfix">
                </div>
            </div>
        </div>
        <!-- fim table panel 2 (B) -->

        <!-- INICIO DA ABA panel 3 (C)  -->
        <div class="tab-pane" id="tab-3" ng-show="tabsStatus.tab3" ng-disabled="salvarTreinoBtn">

            <div class="panel-body" style="padding:7px 20px;">
                <div class="row clearfix" style="margin-top: 6px;">

                    <table class="table table-bordered pagin-table" id="abaCcc">
                        <tbody ng-model="linhaC">

                            <tr ng-model="tabs" ng-repeat="linhaC in linhasC" style="background:#f5f5f5;">
                                <td id="ord" ng-hide="true">@{{linhaC.ficha}}</td>
                                <td id="exercicio" ng-hide="true">@{{linhaC.exercicio}}</td>
                                <td id="series" ng-hide="true">@{{linhaC.series}}</td>
                                <td id="repeticoes" ng-hide="true">@{{linhaC.repeticoes}}</td>
                                <td id="medida_duracao" ng-hide="true">@{{linhaC.medida_duracao}}</td>                                
                                <td id="carga" ng-hide="true">@{{linhaC.carga}}</td>
                                <td id="intervalo" ng-hide="true">@{{linhaC.intervalo}}</td>   
                                <td>

                                    <!-- EXIBE O NÚMERO de linhas -->
                                    <span class="marcador_exer">
                                        @{{linhaC.id}}
                                    </span>

                                    <!-- INICIO DA LINHA DO EXERCICIO -->
                                    <div class="row" style="margin-right: -6px;">

                                        <div class="col-xs-12 col-sm-1 hidden-xs" style="padding:0;     padding-right: 8px;">
                                            <!-- FOTO -->
                                            <div href="#" data-toggle="tooltip" title="Clique e arreste para mudar de posição" style="height:50px; background-image:url('{{ url('/')}}/uploads/exercicios/@{{exercicios[changeExercicio(linhaC.exercicio)].dsurlminiatura}}'); background-color:#fff; background-size:cover;background-position:center;      margin-left: 6px;     width: 100%; float: left; margin-right: 4px;">
                                            </div>
                                        </div>

                                        <div class="col-xs-12 col-sm-11">
                                            <div style="height: 5px"></div>
                                            <div class="row">
                                                <div class="col-sm-6" style="padding:5px 2px 0 2px;">

                                                    <div class="form-group" style="padding-bottom: 0">
                                                        <div class="input-group">

                                                            <select class="form-control" id="execicioSelectC@{{linhaC.id}}" ng-change="changeExercicio(linhaC.exercicio)" ng-model="linhaC.exercicio" ng-disabled="addLinhaBtn">
                                                                <option disabled="" selected="" value="">
                                                                    Selecione o Exercício
                                                                </option>
                                                                <option ng-repeat="exercicio in exercicios" ng-value="exercicio.id">
                                                                    @{{exercicio.nome_exercicio}}
                                                                </option>
                                                            </select>

                                                            <a class="btn btn-default input-group-addon" data-target="#m_exercicios" data-toggle="modal" ng-disabled="addLinhaBtn">
                                                                <i aria-hidden="true" class="fa fa-plus">
                                                                </i>
                                                            </a>

                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 col-sm-6" style="padding:5px 2px 0 2px;">
                                                    <div class="form-group" style="padding-bottom: 0">
                                                        <select class="form-control inputInlineTreino" ng-change="calcTempoC()" ng-model="linhaC.series" ng-disabled="addLinhaBtn">
                                                            <option disabled="" selected="" value="">
                                                                Séries
                                                            </option>
                                                            <option ng-repeat="serie in seriesVetor" ng-value="serie.valor | number">
                                                                @{{serie.texto}}
                                                            </option>
                                                        </select>
                                                        <input class="form-control inputInlineTreino" ng-blur="calcTempoC()" ng-model="linhaC.repeticoes" placeholder="Repetições"
                                                               type="text" ng-disabled="addLinhaBtn">

                                                        <select class="form-control inputInlineTreino" ng-model="linhaC.medida_duracao" ng-disabled="addLinhaBtn">
                                                            <option ng-repeat="medida_duracao in medida_duracaosVetor" ng-value="medida_duracao.valor">
                                                                @{{medida_duracao.texto}}
                                                            </option>
                                                        </select>    

                                                        <input class="form-control inputInlineTreino" ng-model="linhaC.carga" placeholder="Carga" type="text" ng-disabled="addLinhaBtn">

                                                        <select class="form-control inputInlineTreino" ng-change="calcTempoC()" ng-model="linhaC.intervalo" ng-disabled="addLinhaBtn">
                                                            <option disabled="" selected="" value="">
                                                                Intervalo
                                                            </option>
                                                            <option ng-repeat="intervalo in intervalosVetor" ng-value="intervalo.valor | number">
                                                                @{{intervalo.texto}}
                                                            </option>
                                                        </select>

                                                        <!-- FECHA  -->

                                                        <!-- INICIO FECHAR E EXCLUIR -->
                                                        <span class="pull-right">
                                                            <a class="btn btn-default " data-target="#modalObsA@{{linhaC.id}}" data-toggle="modal" ng-disabled="addLinhaBtn">
                                                                <i aria-hidden="true" class="fa fa-comment"></i>
                                                            </a>

                                                            <a class="btn btn-danger " ng-click="removeLinhaC($index, linhaC.ficha)" style=" border: 0;" ng-disabled="addLinhaBtn">
                                                                <i aria-hidden="true" class="fa fa-trash"></i>
                                                            </a>

                                                        </span>
                                                        <!-- FIM FECHAR E EXCLUIR -->



                                                    </div>
                                                </div>

                                                <!-- OBSERVAÇÃO DO EXERCICIO TREINO A -->
                                                <div aria-hidden="true" class="modal inmodal fade" id="modalObsA@{{linhaC.id}}" role="dialog" tabindex="-1">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button class="close" data-dismiss="modal" type="button">
                                                                    <span aria-hidden="true">
                                                                        ×
                                                                    </span>
                                                                    <span class="sr-only">
                                                                        Fechar
                                                                    </span>
                                                                </button>
                                                                <h4 class="modal-title">
                                                                    Observação do Exercício
                                                                </h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="form-group">
                                                                    <input class="form-control" ng-model="linhaC.observacao" placeholder="Informe uma observação" type="text">
                                                                    </input>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button class="btn btn-primary" data-dismiss="modal" type="button">
                                                                    OK
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- modal -->




                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>

                        </tbody>
                    </table>
                    <!-- fim tabele -->

                </div>
                <div class="form-group" style="margin-top:10px;">
                    <span style="text-align:left;margin-left:-15px;">
                        <a class="btn btn-primary" ng-click="addLinhaC()" ng-disabled="addLinhaCtn">
                            <i aria-hidden="true" class="fa fa-plus">
                            </i>
                        </a>
                    </span>
                    <span class="pull-right" style="margin-right:-15px;text-align:right;">
                        <b>
                            Tempo estimado @{{tempoestimadoC}}
                        </b>
                        <br> Musculação: @{{tempomusculacaoC}} + Aquecimento: 10 min + Alongamento: 4 min
                        </br>
                    </span>
                </div>
                <div class="clearfix">
                </div>
            </div>
        </div>
        <!-- fim table panel 3 (C) -->


        <!-- INICIO DA ABA panel 4 (D)  -->
        <div class="tab-pane" id="tab-4" ng-show="tabsStatus.tab4" ng-disabled="salvarTreinoBtn">

            <div class="panel-body" style="padding:7px 20px;">
                <div class="row clearfix" style="margin-top: 6px;">

                    <table class="table table-bordered pagin-table" id="abaDdd">
                        <tbody ng-model="linhaD">

                            <tr ng-model="tabs" ng-repeat="linhaD in linhasD" style="background:#f5f5f5;">
                                <td id="ord" ng-hide="true">@{{linhaD.ficha}}</td>
                                <td id="exercicio" ng-hide="true">@{{linhaD.exercicio}}</td>
                                <td id="series" ng-hide="true">@{{linhaD.series}}</td>
                                <td id="repeticoes" ng-hide="true">@{{linhaD.repeticoes}}</td>
                                <td id="medida_duracao" ng-hide="true">@{{linhaD.medida_duracao}}</td>                                
                                <td id="carga" ng-hide="true">@{{linhaD.carga}}</td>
                                <td id="intervalo" ng-hide="true">@{{linhaD.intervalo}}</td>   
                                <td>

                                    <!-- EXIBE O NÚMERO de linhas -->
                                    <span class="marcador_exer">
                                        @{{linhaD.id}}
                                    </span>

                                    <!-- INICIO DA LINHA DO EXERCICIO -->
                                    <div class="row" style="margin-right: -6px;">

                                        <div class="col-xs-12 col-sm-1 hidden-xs" style="padding:0;     padding-right: 8px;">
                                            <!-- FOTO -->
                                            <div href="#" data-toggle="tooltip" title="Clique e arreste para mudar de posição" style="height:50px; background-image:url('{{ url('/')}}/uploads/exercicios/@{{exercicios[changeExercicio(linhaD.exercicio)].dsurlminiatura}}'); background-color:#fff; background-size:cover;background-position:center;      margin-left: 6px;     width: 100%; float: left; margin-right: 4px;">
                                            </div>
                                        </div>

                                        <div class="col-xs-12 col-sm-11">
                                            <div style="height: 5px"></div>
                                            <div class="row">
                                                <div class="col-sm-6" style="padding:5px 2px 0 2px;">

                                                    <div class="form-group" style="padding-bottom: 0">
                                                        <div class="input-group">

                                                            <select class="form-control" id="execicioSelectD@{{linhaD.id}}" ng-change="changeExercicio(linhaD.exercicio)" ng-model="linhaD.exercicio" ng-disabled="addLinhaBtn">
                                                                <option disabled="" selected="" value="">
                                                                    Selecione o Exercício
                                                                </option>
                                                                <option ng-repeat="exercicio in exercicios" ng-value="exercicio.id">
                                                                    @{{exercicio.nome_exercicio}}
                                                                </option>
                                                            </select>

                                                            <a class="btn btn-default input-group-addon" data-target="#m_exercicios" data-toggle="modal" ng-disabled="addLinhaBtn">
                                                                <i aria-hidden="true" class="fa fa-plus">
                                                                </i>
                                                            </a>

                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 col-sm-6" style="padding:5px 2px 0 2px;">
                                                    <div class="form-group" style="padding-bottom: 0">
                                                        <select class="form-control inputInlineTreino" ng-change="calcTempoD()" ng-model="linhaD.series" ng-disabled="addLinhaBtn">
                                                            <option disabled="" selected="" value="">
                                                                Séries
                                                            </option>
                                                            <option ng-repeat="serie in seriesVetor" ng-value="serie.valor | number">
                                                                @{{serie.texto}}
                                                            </option>
                                                        </select>
                                                        <input class="form-control inputInlineTreino" ng-blur="calcTempoD()" ng-model="linhaD.repeticoes" placeholder="Repetições"
                                                               type="text" ng-disabled="addLinhaBtn">

                                                        <select class="form-control inputInlineTreino" ng-model="linhaD.medida_duracao" ng-disabled="addLinhaBtn">
                                                            <option ng-repeat="medida_duracao in medida_duracaosVetor" ng-value="medida_duracao.valor">
                                                                @{{medida_duracao.texto}}
                                                            </option>
                                                        </select>    

                                                        <input class="form-control inputInlineTreino" ng-model="linhaD.carga" placeholder="Carga" type="text">

                                                        <select class="form-control inputInlineTreino" ng-change="calcTempoD()" ng-model="linhaD.intervalo" ng-disabled="addLinhaBtn">
                                                            <option disabled="" selected="" value="">
                                                                Intervalo
                                                            </option>
                                                            <option ng-repeat="intervalo in intervalosVetor" ng-value="intervalo.valor | number">
                                                                @{{intervalo.texto}}
                                                            </option>
                                                        </select>

                                                        <!-- FECHA  -->

                                                        <!-- INICIO FECHAR E EXCLUIR -->
                                                        <span class="pull-right">
                                                            <a class="btn btn-default " data-target="#modalObsA@{{linhaD.id}}" data-toggle="modal" ng-disabled="addLinhaBtn">
                                                                <i aria-hidden="true" class="fa fa-comment"></i>
                                                            </a>

                                                            <a class="btn btn-danger " ng-click="removeLinhaD($index, linhaD.ficha)" style=" border: 0;" ng-disabled="addLinhaBtn">
                                                                <i aria-hidden="true" class="fa fa-trash"></i>
                                                            </a>

                                                        </span>
                                                        <!-- FIM FECHAR E EXCLUIR -->



                                                    </div>
                                                </div>

                                                <!-- OBSERVAÇÃO DO EXERCICIO TREINO A -->
                                                <div aria-hidden="true" class="modal inmodal fade" id="modalObsA@{{linhaD.id}}" role="dialog" tabindex="-1">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button class="close" data-dismiss="modal" type="button">
                                                                    <span aria-hidden="true">
                                                                        ×
                                                                    </span>
                                                                    <span class="sr-only">
                                                                        Fechar
                                                                    </span>
                                                                </button>
                                                                <h4 class="modal-title">
                                                                    Observação do Exercício
                                                                </h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="form-group">
                                                                    <input class="form-control" ng-model="linhaD.observacao" placeholder="Informe uma observação" type="text">
                                                                    </input>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button class="btn btn-primary" data-dismiss="modal" type="button">
                                                                    OK
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- modal -->




                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>

                        </tbody>
                    </table>
                    <!-- fim tabele -->

                </div>
                <div class="form-group" style="margin-top:10px;">
                    <span style="text-align:left;margin-left:-15px;">
                        <a class="btn btn-primary" ng-click="addLinhaD()" ng-disabled="addLinhaDtn">
                            <i aria-hidden="true" class="fa fa-plus">
                            </i>
                        </a>
                    </span>
                    <span class="pull-right" style="margin-right:-15px;text-align:right;">
                        <b>
                            Tempo estimado @{{tempoestimadoD}}
                        </b>
                        <br> Musculação: @{{tempomusculacaoD}} + Aquecimento: 10 min + Alongamento: 4 min
                        </br>
                    </span>
                </div>
                <div class="clearfix">
                </div>
            </div>
        </div>
        <!-- fim table panel 4 (D) -->





        <!-- INICIO DA ABA panel 5 (E)  -->
        <div class="tab-pane" id="tab-5" ng-show="tabsStatus.tab5" ng-disabled="salvarTreinoBtn">

            <div class="panel-body" style="padding:7px 20px;">
                <div class="row clearfix" style="margin-top: 6px;">

                    <table class="table table-bordered pagin-table" id="abaEee">
                        <tbody ng-model="linhaE">

                            <tr ng-model="tabs" ng-repeat="linhaE in linhasE" style="background:#f5f5f5;">
                                <td id="ord" ng-hide="true">@{{linhaE.ficha}}</td>
                                <td id="exercicio" ng-hide="true">@{{linhaE.exercicio}}</td>
                                <td id="series" ng-hide="true">@{{linhaE.series}}</td>
                                <td id="repeticoes" ng-hide="true">@{{linhaE.repeticoes}}</td>
                                <td id="medida_duracao" ng-hide="true">@{{linhaE.medida_duracao}}</td>                                
                                <td id="carga" ng-hide="true">@{{linhaE.carga}}</td>
                                <td id="intervalo" ng-hide="true">@{{linhaE.intervalo}}</td>   
                                <td>

                                    <!-- EXIBE O NÚMERO de linhas -->
                                    <span class="marcador_exer">
                                        @{{linhaE.id}}
                                    </span>

                                    <!-- INICIO DA LINHA DO EXERCICIO -->
                                    <div class="row" style="margin-right: -6px;">

                                        <div class="col-xs-12 col-sm-1 hidden-xs" style="padding:0;     padding-right: 8px;">
                                            <!-- FOTO -->
                                            <div href="#" data-toggle="tooltip" title="Clique e arreste para mudar de posição" style="height:50px; background-image:url('{{ url('/')}}/uploads/exercicios/@{{exercicios[changeExercicio(linhaE.exercicio)].dsurlminiatura}}'); background-color:#fff; background-size:cover;background-position:center;      margin-left: 6px;     width: 100%; float: left; margin-right: 4px;">
                                            </div>
                                        </div>

                                        <div class="col-xs-12 col-sm-11">
                                            <div style="height: 5px"></div>
                                            <div class="row">
                                                <div class="col-sm-6" style="padding:5px 2px 0 2px;">

                                                    <div class="form-group" style="padding-bottom: 0">
                                                        <div class="input-group">

                                                            <select class="form-control" id="execicioSelectE@{{linhaE.id}}" ng-change="changeExercicio(linhaE.exercicio)" ng-model="linhaE.exercicio" ng-disabled="addLinhaBtn">
                                                                <option disabled="" selected="" value="">
                                                                    Selecione o Exercício
                                                                </option>
                                                                <option ng-repeat="exercicio in exercicios" ng-value="exercicio.id">
                                                                    @{{exercicio.nome_exercicio}}
                                                                </option>
                                                            </select>

                                                            <a class="btn btn-default input-group-addon" data-target="#m_exercicios" data-toggle="modal">
                                                                <i aria-hidden="true" class="fa fa-plus">
                                                                </i>
                                                            </a>

                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 col-sm-6" style="padding:5px 2px 0 2px;">
                                                    <div class="form-group" style="padding-bottom: 0">
                                                        <select class="form-control inputInlineTreino" ng-change="calcTempoE()" ng-model="linhaE.series" ng-disabled="addLinhaBtn">
                                                            <option disabled="" selected="" value="">
                                                                Séries
                                                            </option>
                                                            <option ng-repeat="serie in seriesVetor" ng-value="serie.valor | number">
                                                                @{{serie.texto}}
                                                            </option>
                                                        </select>
                                                        <input class="form-control inputInlineTreino" ng-blur="calcTempoE()" ng-model="linhaE.repeticoes" placeholder="Repetições"
                                                               type="text" ng-disabled="addLinhaBtn">

                                                        <select class="form-control inputInlineTreino" ng-model="linhaE.medida_duracao" ng-disabled="addLinhaBtn">
                                                            <option ng-repeat="medida_duracao in medida_duracaosVetor" ng-value="medida_duracao.valor">
                                                                @{{medida_duracao.texto}}
                                                            </option>
                                                        </select>    

                                                        <input class="form-control inputInlineTreino" ng-model="linhaE.carga" placeholder="Carga" type="text" ng-disabled="addLinhaBtn">

                                                        <select class="form-control inputInlineTreino" ng-change="calcTempoE()" ng-model="linhaE.intervalo" ng-disabled="addLinhaBtn">
                                                            <option disabled="" selected="" value="">
                                                                Intervalo
                                                            </option>
                                                            <option ng-repeat="intervalo in intervalosVetor" ng-value="intervalo.valor | number">
                                                                @{{intervalo.texto}}
                                                            </option>
                                                        </select>

                                                        <!-- FECHA  -->

                                                        <!-- INICIO FECHAR E EXCLUIR -->
                                                        <span class="pull-right">
                                                            <a class="btn btn-default " data-target="#modalObsA@{{linhaE.id}}" data-toggle="modal" ng-disabled="addLinhaBtn">
                                                                <i aria-hidden="true" class="fa fa-comment"></i>
                                                            </a>

                                                            <a class="btn btn-danger " ng-click="removeLinhaE($index, linhaE.ficha)" style=" border: 0;" ng-disabled="addLinhaBtn">
                                                                <i aria-hidden="true" class="fa fa-trash"></i>
                                                            </a>

                                                        </span>
                                                        <!-- FIM FECHAR E EXCLUIR -->



                                                    </div>
                                                </div>

                                                <!-- OBSERVAÇÃO DO EXERCICIO TREINO A -->
                                                <div aria-hidden="true" class="modal inmodal fade" id="modalObsA@{{linhaE.id}}" role="dialog" tabindex="-1">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button class="close" data-dismiss="modal" type="button">
                                                                    <span aria-hidden="true">
                                                                        ×
                                                                    </span>
                                                                    <span class="sr-only">
                                                                        Fechar
                                                                    </span>
                                                                </button>
                                                                <h4 class="modal-title">
                                                                    Observação do Exercício
                                                                </h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="form-group">
                                                                    <input class="form-control" ng-model="linhaE.observacao" placeholder="Informe uma observação" type="text">
                                                                    </input>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button class="btn btn-primary" data-dismiss="modal" type="button">
                                                                    OK
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- modal -->




                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>

                        </tbody>
                    </table>
                    <!-- fim tabele -->

                </div>
                <div class="form-group" style="margin-top:10px;">
                    <span style="text-align:left;margin-left:-15px;">
                        <a class="btn btn-primary" ng-click="addLinhaE()" ng-disabled="addLinhaEtn">
                            <i aria-hidden="true" class="fa fa-plus">
                            </i>
                        </a>
                    </span>
                    <span class="pull-right" style="margin-right:-15px;text-align:right;">
                        <b>
                            Tempo estimado @{{tempoestimadoE}}
                        </b>
                        <br> Musculação: @{{tempomusculacaoE}} + Aquecimento: 10 min + Alongamento: 4 min
                        </br>
                    </span>
                </div>
                <div class="clearfix">
                </div>
            </div>
        </div>
        <!-- fim table panel 5 (E) -->

        <!-- INICIO DA ABA panel 6 (F)  -->
        <div class="tab-pane" id="tab-6" ng-show="tabsStatus.tab6" ng-disabled="salvarTreinoBtn">

            <div class="panel-body" style="padding:7px 20px;">
                <div class="row clearfix" style="margin-top: 6px;">

                    <table class="table table-bordered pagin-table" id="abaFff">
                        <tbody ng-model="linhaF">

                            <tr ng-model="tabs" ng-repeat="linhaF in linhasF" style="background:#f5f5f5;">
                                <td id="ord" ng-hide="true">@{{linhaF.ficha}}</td>
                                <td id="exercicio" ng-hide="true">@{{linhaF.exercicio}}</td>
                                <td id="series" ng-hide="true">@{{linhaF.series}}</td>
                                <td id="repeticoes" ng-hide="true">@{{linhaF.repeticoes}}</td>
                                <td id="medida_duracao" ng-hide="true">@{{linhaF.medida_duracao}}</td>                                
                                <td id="carga" ng-hide="true">@{{linhaF.carga}}</td>
                                <td id="intervalo" ng-hide="true">@{{linhaF.intervalo}}</td>   
                                <td>

                                    <!-- EXIBE O NÚMERO de linhas -->
                                    <span class="marcador_exer">
                                        @{{linhaF.id}}
                                    </span>

                                    <!-- INICIO DA LINHA DO EXERCICIO -->
                                    <div class="row" style="margin-right: -6px;">

                                        <div class="col-xs-12 col-sm-1 hidden-xs" style="padding:0;     padding-right: 8px;">
                                            <!-- FOTO -->
                                            <div href="#" data-toggle="tooltip" title="Clique e arreste para mudar de posição" style="height:50px; background-image:url('{{ url('/')}}/uploads/exercicios/@{{exercicios[changeExercicio(linhaF.exercicio)].dsurlminiatura}}'); background-color:#fff; background-size:cover;background-position:center;      margin-left: 6px;     width: 100%; float: left; margin-right: 4px;">
                                            </div>
                                        </div>

                                        <div class="col-xs-12 col-sm-11">
                                            <div style="height: 5px"></div>
                                            <div class="row">
                                                <div class="col-sm-6" style="padding:5px 2px 0 2px;">

                                                    <div class="form-group" style="padding-bottom: 0">
                                                        <div class="input-group">

                                                            <select class="form-control" id="execicioSelectF@{{linhaF.id}}" ng-change="changeExercicio(linhaF.exercicio)" ng-model="linhaF.exercicio" ng-disabled="addLinhaBtn">
                                                                <option disabled="" selected="" value="">
                                                                    Selecione o Exercício
                                                                </option>
                                                                <option ng-repeat="exercicio in exercicios" ng-value="exercicio.id">
                                                                    @{{exercicio.nome_exercicio}}
                                                                </option>
                                                            </select>

                                                            <a class="btn btn-default input-group-addon" data-target="#m_exercicios" data-toggle="modal">
                                                                <i aria-hidden="true" class="fa fa-plus">
                                                                </i>
                                                            </a>

                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 col-sm-6" style="padding:5px 2px 0 2px;">
                                                    <div class="form-group" style="padding-bottom: 0">
                                                        <select class="form-control inputInlineTreino" ng-change="calcTempoF()" ng-model="linhaF.series" ng-disabled="addLinhaBtn">
                                                            <option disabled="" selected="" value="">
                                                                Séries
                                                            </option>
                                                            <option ng-repeat="serie in seriesVetor" ng-value="serie.valor | number">
                                                                @{{serie.texto}}
                                                            </option>
                                                        </select>
                                                        <input class="form-control inputInlineTreino" ng-blur="calcTempoF()" ng-model="linhaF.repeticoes" placeholder="Repetições"
                                                               type="text" ng-disabled="addLinhaBtn">
                                                        <select class="form-control inputInlineTreino" ng-model="linhaF.medida_duracao" ng-disabled="addLinhaBtn">
                                                            <option ng-repeat="medida_duracao in medida_duracaosVetor" ng-value="medida_duracao.valor">
                                                                @{{medida_duracao.texto}}
                                                            </option>
                                                        </select>    

                                                        <input class="form-control inputInlineTreino" ng-model="linhaF.carga" placeholder="Carga" type="text" ng-disabled="addLinhaBtn">

                                                        <select class="form-control inputInlineTreino" ng-change="calcTempoF()" ng-model="linhaF.intervalo" ng-disabled="addLinhaBtn">
                                                            <option disabled="" selected="" value="">
                                                                Intervalo
                                                            </option>
                                                            <option ng-repeat="intervalo in intervalosVetor" ng-value="intervalo.valor | number" >
                                                                @{{intervalo.texto}}
                                                            </option>
                                                        </select>

                                                        <!-- FECHA  -->

                                                        <!-- INICIO FECHAR E EXCLUIR -->
                                                        <span class="pull-right">
                                                            <a class="btn btn-default " data-target="#modalObsA@{{linhaF.id}}" data-toggle="modal" ng-disabled="addLinhaBtn">
                                                                <i aria-hidden="true" class="fa fa-comment"></i>
                                                            </a>

                                                            <a class="btn btn-danger " ng-click="removeLinhaF($index, linhaF.ficha)" style=" border: 0;" ng-disabled="addLinhaBtn">
                                                                <i aria-hidden="true" class="fa fa-trash"></i>
                                                            </a>

                                                        </span>
                                                        <!-- FIM FECHAR E EXCLUIR -->



                                                    </div>
                                                </div>

                                                <!-- OBSERVAÇÃO DO EXERCICIO TREINO A -->
                                                <div aria-hidden="true" class="modal inmodal fade" id="modalObsA@{{linhaF.id}}" role="dialog" tabindex="-1">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button class="close" data-dismiss="modal" type="button">
                                                                    <span aria-hidden="true">
                                                                        ×
                                                                    </span>
                                                                    <span class="sr-only">
                                                                        Fechar
                                                                    </span>
                                                                </button>
                                                                <h4 class="modal-title">
                                                                    Observação do Exercício
                                                                </h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="form-group">
                                                                    <input class="form-control" ng-model="linhaF.observacao" placeholder="Informe uma observação" type="text">
                                                                    </input>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button class="btn btn-primary" data-dismiss="modal" type="button">
                                                                    OK
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- modal -->




                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>

                        </tbody>
                    </table>
                    <!-- fim tabele -->

                </div>
                <div class="form-group" style="margin-top:10px;">
                    <span style="text-align:left;margin-left:-15px;">
                        <a class="btn btn-primary" ng-click="addLinhaF()" ng-disabled="addLinhaFtn">
                            <i aria-hidden="true" class="fa fa-plus">
                            </i>
                        </a>
                    </span>
                    <span class="pull-right" style="margin-right:-15px;text-align:right;">
                        <b>
                            Tempo estimado @{{tempoestimadoF}}
                        </b>
                        <br> Musculação: @{{tempomusculacaoF}} + Aquecimento: 10 min + Alongamento: 4 min
                        </br>
                    </span>
                </div>
                <div class="clearfix">
                </div>
            </div>
        </div>
        <!-- fim table panel 6 (F) -->


        <!-- INICIO DA ABA panel 7(G)  -->
        <div class="tab-pane" id="tab-7" ng-show="tabsStatus.tab7" ng-disabled="salvarTreinoBtn">

            <div class="panel-body" style="padding:7px 20px;">
                <div class="row clearfix" style="margin-top: 6px;">

                    <table class="table table-bordered pagin-table" id="abaGgg">
                        <tbody ng-model="linhaG">

                            <tr ng-model="tabs" ng-repeat="linhaG in linhasG" style="background:#f5f5f5;">
                                <td id="ord" ng-hide="true">@{{linhaG.ficha}}</td>
                                <td id="exercicio" ng-hide="true">@{{linhaG.exercicio}}</td>
                                <td id="series" ng-hide="true">@{{linhaG.series}}</td>
                                <td id="repeticoes" ng-hide="true">@{{linhaG.repeticoes}}</td>
                                <td id="medida_duracao" ng-hide="true">@{{linhaG.medida_duracao}}</td>                                
                                <td id="carga" ng-hide="true">@{{linhaG.carga}}</td>
                                <td id="intervalo" ng-hide="true">@{{linhaG.intervalo}}</td>   
                                <td>

                                    <!-- EXIBE O NÚMERO de linhas -->
                                    <span class="marcador_exer">
                                        @{{linhaG.id}}
                                    </span>

                                    <!-- INICIO DA LINHA DO EXERCICIO -->
                                    <div class="row" style="margin-right: -6px;">

                                        <div class="col-xs-12 col-sm-1 hidden-xs" style="padding:0;     padding-right: 8px;">
                                            <!-- FOTO -->
                                            <div href="#" data-toggle="tooltip" title="Clique e arreste para mudar de posição" style="height:50px; background-image:url('{{ url('/')}}/uploads/exercicios/@{{exercicios[changeExercicio(linhaG.exercicio)].dsurlminiatura}}'); background-color:#fff; background-size:cover;background-position:center;      margin-left: 6px;     width: 100%; float: left; margin-right: 4px;">
                                            </div>
                                        </div>

                                        <div class="col-xs-12 col-sm-11">
                                            <div style="height: 5px"></div>
                                            <div class="row">
                                                <div class="col-sm-6" style="padding:5px 2px 0 2px;">

                                                    <div class="form-group" style="padding-bottom: 0">
                                                        <div class="input-group">

                                                            <select class="form-control" id="execicioSelectG@{{linhaG.id}}" ng-change="changeExercicio(linhaG.exercicio)" ng-model="linhaG.exercicio" ng-disabled="addLinhaBtn">
                                                                <option disabled="" selected="" value="">
                                                                    Selecione o Exercício
                                                                </option>
                                                                <option ng-repeat="exercicio in exercicios" ng-value="exercicio.id">
                                                                    @{{exercicio.nome_exercicio}}
                                                                </option>
                                                            </select>

                                                            <a class="btn btn-default input-group-addon" data-target="#m_exercicios" data-toggle="modal">
                                                                <i aria-hidden="true" class="fa fa-plus">
                                                                </i>
                                                            </a>

                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 col-sm-6" style="padding:5px 2px 0 2px;">
                                                    <div class="form-group" style="padding-bottom: 0">
                                                        <select class="form-control inputInlineTreino" ng-change="calcTempoG()" ng-model="linhaG.series" ng-disabled="addLinhaBtn">
                                                            <option disabled="" selected="" value="">
                                                                Séries
                                                            </option>
                                                            <option ng-repeat="serie in seriesVetor" ng-value="serie.valor | number">
                                                                @{{serie.texto}}
                                                            </option>
                                                        </select>
                                                        <input class="form-control inputInlineTreino" ng-blur="calcTempoG()" ng-model="linhaG.repeticoes" placeholder="Repetições"
                                                               type="text" ng-disabled="addLinhaBtn">

                                                        <select class="form-control inputInlineTreino" ng-model="linhaG.medida_duracao" ng-disabled="addLinhaBtn">
                                                            <option ng-repeat="medida_duracao in medida_duracaosVetor" ng-value="medida_duracao.valor">
                                                                @{{medida_duracao.texto}}
                                                            </option>
                                                        </select>    

                                                        <input class="form-control inputInlineTreino" ng-model="linhaG.carga" placeholder="Carga" type="text" ng-disabled="addLinhaBtn">

                                                        <select class="form-control inputInlineTreino" ng-change="calcTempoG()" ng-model="linhaG.intervalo" ng-disabled="addLinhaBtn">
                                                            <option disabled="" selected="" value="">
                                                                Intervalo
                                                            </option>
                                                            <option ng-repeat="intervalo in intervalosVetor" ng-value="intervalo.valor | number">
                                                                @{{intervalo.texto}}
                                                            </option>
                                                        </select>

                                                        <!-- FECHA  -->

                                                        <!-- INICIO FECHAR E EXCLUIR -->
                                                        <span class="pull-right">
                                                            <a class="btn btn-default " data-target="#modalObsA@{{linhaG.id}}" data-toggle="modal" ng-disabled="addLinhaBtn">
                                                                <i aria-hidden="true" class="fa fa-comment"></i>
                                                            </a>

                                                            <a class="btn btn-danger " ng-click="removeLinhaG($index, linhaG.ficha)" style=" border: 0;" ng-disabled="addLinhaBtn">
                                                                <i aria-hidden="true" class="fa fa-trash"></i>
                                                            </a>

                                                        </span>
                                                        <!-- FIM FECHAR E EXCLUIR -->



                                                    </div>
                                                </div>

                                                <!-- OBSERVAÇÃO DO EXERCICIO TREINO A -->
                                                <div aria-hidden="true" class="modal inmodal fade" id="modalObsA@{{linhaG.id}}" role="dialog" tabindex="-1">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button class="close" data-dismiss="modal" type="button">
                                                                    <span aria-hidden="true">
                                                                        ×
                                                                    </span>
                                                                    <span class="sr-only">
                                                                        Fechar
                                                                    </span>
                                                                </button>
                                                                <h4 class="modal-title">
                                                                    Observação do Exercício
                                                                </h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="form-group">
                                                                    <input class="form-control" ng-model="linhaG.observacao" placeholder="Informe uma observação" type="text">
                                                                    </input>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button class="btn btn-primary" data-dismiss="modal" type="button">
                                                                    OK
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- modal -->




                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>

                        </tbody>
                    </table>
                    <!-- fim tabele -->

                </div>
                <div class="form-group" style="margin-top:10px;">
                    <span style="text-align:left;margin-left:-15px;">
                        <a class="btn btn-primary" ng-click="addLinhaG()" ng-disabled="addLinhaGtn">
                            <i aria-hidden="true" class="fa fa-plus">
                            </i>
                        </a>
                    </span>
                    <span class="pull-right" style="margin-right:-15px;text-align:right;">
                        <b>
                            Tempo estimado @{{tempoestimadoG}}
                        </b>
                        <br> Musculação: @{{tempomusculacaoG}} + Aquecimento: 10 min + Alongamento: 4 min
                        </br>
                    </span>
                </div>
                <div class="clearfix">
                </div>
            </div>
        </div>
        <!-- fim table panel 7(G) -->




    </div>
    <!-- FIM PANEL -->
    <div style="height:20px;">
    </div>
    <span class="pull-right" style="margin:-8px 5px 5px 0;">
        <button class="btn btn-primary" ng-hide="treino_padrao == false" data-target="#modalNomeTreinoPadrao" data-toggle="modal" ng-disabled="salvarTreinoBtn">
            <i class="fa fa-check" style="margin-right:5px;">
            </i>
            Salvar
        </button>
        <button class="btn btn-primary" ng-hide="treino_padrao == true" ng-click="salvarTreino()" ng-disabled="salvarTreinoBtn">
            <i class="fa fa-check" style="margin-right:5px;">
            </i>
            Salvar
        </button>
    </span>
    <div style="float:right;margin-right:15px;">
        <div class="checkbox m-r-xs">
            <input id="checkbox" ng-model="treino_padrao" type="checkbox">
            <label for="checkbox">
                Utilizar como treino padrão
            </label>
            </input>
        </div>
    </div>
</div>


<div aria-hidden="true" class="modal inmodal fade" id="modalNomeTreinoPadrao" role="dialog" tabindex="-1" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" data-dismiss="modal" type="button">
                    <span aria-hidden="true">
                        ×
                    </span>
                    <span class="sr-only">
                        Fechar
                    </span>
                </button>
                <h4 class="modal-title">
                    Nome do Treino
                </h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <input class="form-control" ng-model="treino_nome_padrao" placeholder="Informe o nome do treino" type="text">
                    </input>
                </div>
            </div>
            <div class="modal-body">
                <select ng-model="client.dados.genero" data-placeholder="Selecione o Genero..." class="form-control" ng-app="" id="genero" name="genero" ng-model="prospect.genero" >
                    <option value="" disabled selected> </option>
                    <option value="M"> M</option>
                    <option value="F"> F</option>
                </select>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" data-dismiss="modal" ng-click="salvarTreino()" type="button">
                    OK
                </button>
            </div>
        </div>
    </div>
</div>
<div aria-hidden="true" class="modal inmodal fade" id="obs_medica" role="dialog" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" data-dismiss="modal" type="button">
                    <span aria-hidden="true">
                        ×
                    </span>
                    <span class="sr-only">
                        Fechar
                    </span>
                </button>
                <h4 class="modal-title">
                    Observações médicas 
                </h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <textarea class="form-control" ng-model="observacao_medica" placeholder="Observações médicas"  cols="200" rows="15">
                        
                    </textarea>

                </div>
            </div>

            <div class="modal-footer">
                <button class="btn btn-primary" data-dismiss="modal" ng-click="salvarObs(client.id)" type="button">
                    OK
                </button>
            </div>
        </div>
    </div>
</div>
