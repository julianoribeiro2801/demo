<div aria-hidden="true" class="modal inmodal fade" id="modal_preview" role="dialog" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Preview</h4>
            </div>
            <!--<form name="PostFormNivel" id="PostFormNivel" method="post" enctype="multipart/form-data">                    -->
            <div class="modal-body">


                <div class="row" style="margin-top: 10px">
                    <div class="col-lg-12">
                        <table class="table table-hover">
                                <!-- <thead>
                                        <tr>
                                                <th >Habilidade</th>
                                                <th style="width:100px"> Ações</th>
                                        </tr>
                                </thead> -->
                            <tbody>
                            <div width="80" height="60">
                                <video  width="80px" height="60px" ng-src="http://sistemapcb.com.br/public/uploads/exercicios/{{video_preview}}"   controls>

                                </video>

                            </div>                              


                            </tbody>
                        </table>
                    </div>


                </div>
            </div>

            <!--<div class="modal-footer">
                <div class="form-group">

                    <button type="submit" ng-click="addNivel()" class="btn btn-primary pull-right"><i class="fa fa-check"></i> Salvar</button>
                </div>
            </div>-->
            <!--</form>-->
        </div>
    </div>
</div>

<div aria-hidden="true" class="modal inmodal fade" id="modal_nivel_edit" role="dialog" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                <h4 class="modal-title">Editar nível de habilidade</h4>
            </div>
            <!--<form name="PostFormNivelUp" id="PostFormNivelUp" method="post" enctype="multipart/form-data">-->
            <div class="modal-body">
                <div class="form-group">
                    <label>Modalidade</label>
                    <input type="text" ng-show="false"  ng-model="nivel_.id" name="nivel_id">
                    <input class="form-control"  disabled="true" placeholder="{{nmmodalidade}}" name="nmmodalidades">
                </div>

                <div class="form-group">
                    <label>Objetivo</label>
                    <input class="form-control" disabled="true" placeholder="{{nmobjetivo}}" name="nmobjetivos">
                </div>
                <div class="form-group">
                    <label>Nome do Nível</label>
                    <input class="form-control"  placeholder="Descrição" ng-model="nivel_.nmnivel" name="nmnivels">
                </div>



                <div class="form-group">

                    <hr>

                    <div class="row">

                        <div class="col-xs-8 ">
                            <b>Habilidades do Nível</b>
                        </div>
                        <div class="col-xs-4 text-right">
                            <button type="button" class="btn btn-primary btn-xs" ng-click="addHabilidade()">
                                <i class="fa fa-plus" aria-hidden="true"></i> Nova Habilidade</button>
                        </div>
                    </div>


                    <div class="row" style="margin-top: 10px">
                        <div class="col-lg-12">
                            <table class="table table-hover">
                                    <!-- <thead>
                                            <tr>
                                                    <th >Habilidade</th>
                                                    <th style="width:100px"> Ações</th>
                                            </tr>
                                    </thead> -->
                                <tbody>
                                    <tr ng-repeat="habilidade in habilidades">
                                        <td>

                                            <i class="fa fa-star" style="color: #d79800; float: left; margin-right: 10px" aria-hidden="true" ></i> 
                                            <input type="text" ng-disabled="habilidade.st > 0"  placeholder="Informe o nome" class="form-control small" ng-model="habilidade.nmhabilidade" ng-value="habilidade.nmhabilidade" placeholder="Descrição" style="width: 90%;font-weight: normal;">                          

                                        </td>
                                        <td class="text-right">
                                            <button class="btn btn-primary btn-xs "ng-click="habilidade.st = -1" ng-show="habilidade.st > 0">
                                                <i class="fa fa-pencil-square"> </i> </button>
                                            <button class="btn btn-primary btn-xs" ng-click="habilidadeSave(nivel_.id, $index, habilidade.nmhabilidade, habilidade.id)" ng-show="habilidade.st == -1">
                                                <i class="fa fa-save"> </i> </button>
                                            <button class="btn btn-danger btn-xs " ng-click="habilidadeDel(nivel_.id, habilidade.id)">
                                                <i class="fa fa-trash-o"> </i>
                                            </button>
                                        </td>

                                    </tr>

                                </tbody>
                            </table>
                        </div>


                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="form-group">
                    <button type="button" ng-click="upNivel()" value="SendPostForm" class="btn btn-primary pull-right"><i class="fa fa-check"></i> Salvar</button>
                </div>
            </div>
            <!--</form>-->
        </div>
    </div>
</div>