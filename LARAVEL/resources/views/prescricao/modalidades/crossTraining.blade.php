<div class="ibox collapsed">
    <div class="ibox-title">
        <div class="ibox-tools">
            <a class="collapse-link"  onclick="getCalendario('cross')">
                <h5>Cross Training</h5>
                <i class="fa fa-chevron-up"  ng-click="getTreinosCrossAluno()" onclick="getCalendario('cross')"></i>
            </a>
        </div>
    </div>
    <div class="ibox-content" style="display: none;">
        <div class="row">
            <div class="col-xs-12 col-sm-4"  style="padding-right: 0;">
                <div class="form-group">
                    <select class="form-control" ng-model="objetivo_id_cross" ng-change="changeObjetivoCross(objetivo_id_cross)">
                        <option value=""  selected>Objetivo</option>
                        <option ng-repeat="objetivocross in objetivoscross" value="@{{objetivocross.id}}">@{{objetivocross.nmobjetivo}}</option>
                    </select>
                </div>
            </div>
            <div class="col-xs-12 col-sm-5">
                <div class="form-group">
                    <select class="form-control" ng-model="treino_nivel_cross">
                        <option value="" selected disabled>Nível de habilidade</option>
                        <option ng-repeat="nivelcross in niveiscross" value="@{{nivelcross.id}}">@{{nivelcross.nmnivel}}</option>
                    </select>
                </div>
            </div>
            <div class="col-xs-12 col-sm-3">
                <div class="form-group">
                    <button class="btn btn-defaut btn-sm"  data-target="#treino_modelo_modal_cross" data-toggle="modal" type="button">
                        <i class="fa fa-plus-square" style="margin-right: 5px"></i> Usar modelo padrão
                    </button>
                </div>
            </div>
        </div>
        {{-- INICIO CALENDARIO--}}
        <div  style="overflow:auto;">
            <div class='pull-right'>
                <button class='btn btn-primary anteriorMes'>
                    <i class='fa fa-chevron-left' aria-hidden='true'></i>
                </button>
                <button class='btn btn-primary proximoMes'>
                    <i class='fa fa-chevron-right' aria-hidden='true'></i>
                </button>
            </div>
            <div class="calendarioCross"></div>
        </div>
        {{-- FIM CALENDARIO --}}
        <div class="form-group">
            <span class="pull-right">
                <button class="btn btn-primary btnprescicaoSalvar" onclick="btnprescicaoSalvar()" type="button">
                    <i class="fa fa-check"></i> Salvar
                </button>
            </span>
            <div style="clear:both"></div>
        </div>
    </div>
</div>