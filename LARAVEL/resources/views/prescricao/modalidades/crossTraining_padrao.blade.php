<div class="ibox collapsed">
    <div class="ibox-title">
        <div class="ibox-tools">
            <a class="collapse-link" >
                <h5>Cross Training</h5>
                <i class="fa fa-chevron-up"></i>
            </a>
        </div>
    </div>
    <div class="ibox-content" style="display: none;">
        <div class="row">
            <div class="col-xs-12 col-md-4 ">
                @include('prescricao.include.treinos_cross')
            </div>
            <div class="col-xs-12 col-md-8" style=" padding-left: 35px">
                
                
                <div class="row">
                    <div  ng-show="treino_id_padrao > 0" class="col-xs-12 col-sm-5" style="padding:0;">
                        <div class="form-group">
                            <div style="display: none"><input type="text" name="treino_id_pad" id="treino_id_pad" ng-model="treino_id_padrao" placeholder="Informe o nome do treino" type="text"></div>
                            <input class="form-control" name="nome_treino_cross" id="nome_treino_cross" ng-model="treino_nome_cross" placeholder="Informe o nome do treino" type="text">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-7">
                        <div class="form-group">
                            <button class="btn btn-primary" ng-disabled="addTreinoBtnCross" ng-click="addTreinoCrossPadrao()">
                            <i class="fa fa-plus" aria-hidden="true"></i> <span class="hidden-xs"> Novo Treino</span>
                            </button>
                            <button class="btn btn-danger" ng-click="deleteTreinoCross(treino_id.treino_id)">
                            <a ng-click="delTreinoPadrao(treino.treino_id)"  >
                            <i class="fa fa-trash" aria-hidden="true" style="color: #fff"></i></a>
                            </button>
                        </div>
                    </div>
                </div>
                <div ng-show="treino_id_padrao > 0">
                    <div class="row">
                        <div class="col-xs-12 col-sm-5" style="padding:0;">
                            <div class="form-group">
                                <select class="form-control" name="sexo_treino_cross" id="sexo_treino_cross" >
                                    <option value="">Selecione o sexo</option>
                                    <!--<option value="M">Masculino</option>
                                    <option value="F">Feminino</option>-->
                                    <option ng-repeat="sexo in sexoVetor" ng-value="sexo.valor | Text">@{{sexo.texto}}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-5" style="padding:0;">
                            <div class="form-group">
                                <select  id="objetivo_treino_cross" name="objetivo_treino_cross" class="form-control" placeholder="Selecione o objetivo do treino" onchange="changeObjetivo('4', objetivo_treino_cross.value)">
                                    <option value="">Selecione o objetivo</option>-->
                                    <option ng-repeat="objetivoCross in objetivosCross" ng-value="objetivoCross.id">@{{objetivoCross.nmobjetivo}}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-5" style="padding:0;">
                            <div class="form-group">
                                <select id="nivel_treino_cross" name="nivel_treino_cross" class="form-control" placeholder="Selecione o nivel do treino">
                                    <option value="">Selecione o nível</option>-->                                    
                                    <option ng-repeat="nivelCross in niveis" ng-value="nivelCross.id">@{{nivelCross.nmnivel}}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- INICIO CALENDARIO--}}
                <div ng-show="treino_id_padrao > 0">
                    <div  style="overflow:auto;">
                        <div class='pull-right'>
                            <button class='btn btn-primary anteriorMes'>
                            <i class='fa fa-chevron-left' aria-hidden='true'></i>
                            </button>
                            <button class='btn btn-primary proximoMes'>
                            <i class='fa fa-chevron-right' aria-hidden='true'></i>
                            </button>
                        </div>
                        <div class="calendarioCross"></div>
                    </div>
                    {{-- FIM CALENDARIO --}}
                    <div class="form-group">
                        <span class="pull-right">
                            
                            <button class="btn btn-primary btnprescicaoSalvar" ng-click="salvarModalidade('cross')" type="button">
<!--                            <button class="btn btn-primary btnprescicaoSalvar" ng-click="btnprescricaoSalvarCross('cross')" type="button">-->
                            <i class="fa fa-check"></i> Salvar
                            </button>
                        </span>
                        <div style="clear:both"></div>
                    </div>
                </div>
                
                
            </div>
            
        </div>
        {{-- ######### --}}
    </div>
</div>