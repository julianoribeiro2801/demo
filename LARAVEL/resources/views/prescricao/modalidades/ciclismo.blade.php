<div class="ibox collapsed">
    <div class="ibox-title">
        <div class="ibox-tools">
            <a class="collapse-link"  onclick="getCalendario('ciclismo')">
                <h5>Ciclismo</h5>
                <i class="fa fa-chevron-up" ng-click="getTreinosCiclismoAluno()" onclick="getCalendario('ciclismo')"></i>
            </a>
        </div>
    </div>
    <div class="ibox-content" style="display: none;">
        <div class="row">
            <div class="col-xs-12 col-sm-4" style="padding-right: 0;">
                <div class="form-group">
                    <select class="form-control" ng-model="objetivo_id_ciclismo" ng-change="changeObjetivoCiclismo(objetivo_id_ciclismo)">
                        <option value=""  selected>Objetivo</option>
                        <option ng-repeat="objetivociclismo in objetivosciclismo" value="@{{objetivociclismo.id}}">@{{objetivociclismo.nmobjetivo}}</option>
                    </select>
                </div>
            </div>
            <div class="col-xs-12 col-sm-5">
                <div class="form-group">
                    <select class="form-control" ng-model="treino_nivel_ciclismo">
                        <option value="" selected disabled>Nível de habilidade</option>
                        <option ng-repeat="nivelciclismo in niveisciclismo" value="@{{nivelciclismo.id}}">@{{nivelciclismo.nmnivel}}</option>
                    </select>
                </div>
            </div>
            <div class="col-xs-12 col-sm-3">
                <div class="form-group">
                    <button class="btn btn-defaut btn-sm"  data-target="#treino_modelo_modal_ciclismo" data-toggle="modal" type="button">
                        <i class="fa fa-plus-square" style="margin-right: 5px"></i> Usar modelo padrão
                    </button>
                </div>
            </div>
        </div>
        {{-- INICIO CALENDARIO--}}
        <div  style="overflow:auto;">
            <div class='pull-right'>
                <button class='btn btn-primary anteriorMes'>
                    <i class='fa fa-chevron-left' aria-hidden='true'></i>
                </button>
                <button class='btn btn-primary proximoMes'>
                    <i class='fa fa-chevron-right' aria-hidden='true'></i>
                </button>
            </div>
            <div class="calendarioCiclismo"></div>
        </div>
        {{-- FIM CALENDARIO --}}
        <div class="form-group">
            <span class="pull-right">
                <button class="btn btn-primary btnprescicaoSalvar" onclick="btnprescicaoSalvar()" type="button">
                    <i class="fa fa-check"></i> Salvar
                </button>
            </span>
            <div style="clear:both"></div>
        </div>
    </div>
</div>