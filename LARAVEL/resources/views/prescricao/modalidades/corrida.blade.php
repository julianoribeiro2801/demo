<div class="ibox collapsed">
    <div class="ibox-title">
        <div class="ibox-tools">
            <a class="collapse-link"  onclick="getCalendario('corrida')">
                <h5>Corrida</h5>
                <i class="fa fa-chevron-up"  ng-click="getTreinosCorridaAluno()" onclick="getCalendario('corrida')"></i>
            </a>
        </div>
    </div>
    <div class="ibox-content" style="display: none;">
        <div class="row">
            <div class="col-xs-12 col-sm-4" style="padding-right: 0;">
                <div class="form-group">
                    <select class="form-control" ng-model="objetivo_id_corrida" ng-change="changeObjetivoCorrida(objetivo_id_corrida)">
                        <option value=""  selected>Objetivo</option>
                        <option ng-repeat="objetivocorrida in objetivoscorrida" value="@{{objetivocorrida.id}}">@{{objetivocorrida.nmobjetivo}}</option>
                    </select>
                </div>
            </div>
            <div class="col-xs-12 col-sm-5">
                <div class="form-group">
                    <select class="form-control" ng-model="treino_nivel_corrida">
                        <option value="" selected disabled>Nível de habilidade</option>
                        <option ng-repeat="nivelcorrida in niveiscorrida" value="@{{nivelcorrida.id}}">@{{nivelcorrida.nmnivel}}</option>
                    </select>
                </div>
            </div>
            <div class="col-xs-12 col-sm-3">
                <div class="form-group">
                            <!--<select ng-options="treino_corrida.nome_treino for treino_corrida in padroes_corrida" data-ng-model="treino_corrida_padrao" ng-change="salvarTreinoCross(treino_corrida_padrao.id)" class="form-control tdata_mob">

                            </select>                   -->
                    <button class="btn btn-defaut btn-sm"  data-target="#treino_modelo_modal_corrida" data-toggle="modal" type="button">
                        <i class="fa fa-plus-square" style="margin-right: 5px"></i> Usar modelo padrão
                    </button>
                </div>
            </div>
        </div>
        {{-- INICIO CALENDARIO--}}
        <div  style="overflow:auto;">
            <div class='pull-right'>
                <button class='btn btn-primary anteriorMes'>
                    <i class='fa fa-chevron-left' aria-hidden='true'></i>
                </button>
                <button class='btn btn-primary proximoMes'>
                    <i class='fa fa-chevron-right' aria-hidden='true'></i>
                </button>
            </div>
            <div class="calendarioCorrida"></div>
        </div>
        {{-- FIM CALENDARIO --}}
        <div class="form-group">
            <span class="pull-right">
                <button class="btn btn-primary btnprescicaoSalvar" onclick="btnprescricaoSalvarCross('corrida')" type="button">
                    <i class="fa fa-check"></i> Salvar
                </button>
            </span>
            <div style="clear:both"></div>
        </div>
    </div>
</div>