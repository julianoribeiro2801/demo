<div class="ibox collapsed">
    <div class="ibox-title">
        <div class="ibox-tools">
            <a class="collapse-link" >
                <h5>Natação</h5>
                <i class="fa fa-chevron-up"></i>
            </a>
        </div>
    </div>
    <div class="ibox-content" style="display: none;">
        
        <div class="row">
            <div class="col-xs-12 col-md-4 ">
                @include('prescricao.include.treinos_natacao')
            </div>
            <div class="col-xs-12 col-md-8" style=" padding-left: 35px">
                
                
                <div class="row">
                    <div class="col-xs-12 col-sm-5" ng-show="treino_id_padraonatacao > 0" style="padding:0;">
                        <div class="form-group">
                            <div style="display: none"><input type="text" name="treino_id_padnatacao" id="treino_id_padnatacao" ng-model="treino_id_padraonatacao" placeholder="Informe o nome do treino" type="text"></div>
                            <input class="form-control" name="nome_treino_natacao" id="nome_treino_natacao" ng-model="treino_nome_natacao" placeholder="Informe o nome do treino" type="text">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-7">
                        <div class="form-group">
                            <button class="btn btn-primary" ng-disabled="addTreinoBtnNatacao" ng-click="addTreinoNatacaoPadrao()">
                            <i class="fa fa-plus" aria-hidden="true"></i> <span class="hidden-xs"> Novo Treino</span>
                            </button>
                            <button class="btn btn-danger" ng-click="deleteTreinoNatacao(treino_id.treino_id)">
                            <a ng-click="delTreinoPadrao(treino.treino_id)"  >
                            <i class="fa fa-trash" aria-hidden="true" style="color: #fff"></i></a>
                            </button>
                        </div>
                    </div>
                </div>
                <div ng-show="treino_id_padraonatacao > 0">
                    <div class="row">
                        <div class="col-xs-12 col-sm-5" style="padding:0;">
                            <div class="form-group">
                                <select class="form-control" name="sexo_treino_natacao" id="sexo_treino_natacao" >
                                    <option value="">Selecione o sexo</option>
                                    <!--<option value="M">Masculino</option>
                                    <option value="F">Feminino</option>-->
                                    <option ng-repeat="sexo in sexoVetor" ng-value="sexo.valor | Text">@{{sexo.texto}}</option>                           

                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-5" style="padding:0;">
                            <div class="form-group">
                                <select  id="objetivo_treino_natacao" name="objetivo_treino_natacao" class="form-control" placeholder="Selecione o objetivo do treino" onchange="changeObjetivo('3', objetivo_treino_natacao.value)">
                                    <option value=""  selected>Objetivo</option>
                                    
                                    <option ng-repeat="objetivoNatacao in objetivosNatacao" ng-value="objetivoNatacao.id">@{{objetivoNatacao.nmobjetivo}}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-5" style="padding:0;">
                            <div class="form-group">
                                <select id="nivel_treino_natacao" name="nivel_treino_natacao" class="form-control" placeholder="Selecione o nivel do treino">
                                    <option value=""  selected>Nível de habilidade</option>

                                    <!--<option ng-repeat="nivelNatacao in niveis" ng-value="nivelNatacao.id">@{{nivelNatacao.nmnivel}}</option>-->
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div ng-show="treino_id_padraonatacao > 0">
                    <div  style="overflow:auto;">
                        <div class='pull-right'>
                            <button class='btn btn-primary anteriorMes'>
                            <i class='fa fa-chevron-left' aria-hidden='true'></i>
                            </button>
                            <button class='btn btn-primary proximoMes'>
                            <i class='fa fa-chevron-right' aria-hidden='true'></i>
                            </button>
                        </div>
                        <div class="calendarioNatacao"></div>
                    </div>
                    {{-- FIM CALENDARIO --}}
                    <div class="form-group">
                        <span class="pull-right">
                            <button class="btn btn-primary btnprescicaoSalvar" onclick="btnprescricaoSalvarCross('natacao')" type="button">
                            <i class="fa fa-check"></i> Salvar
                            </button>
                        </span>
                        <div style="clear:both"></div>
                    </div>
                </div>
            </div>
        </div>
        {{-- asjsalkjsalkjaskl --}}
    </div>
</div>