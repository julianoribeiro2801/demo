<div class="ibox collapsed">
	<div class="ibox-title">
		<div class="ibox-tools">
			<a class="collapse-link"  ng-click="getNutricaoPadrao()"><h5>Nutrição</h5><i class="fa fa-chevron-up" ng-click="getNutricaoPadrao()" ></i></a>
		</div>
	</div>
	<div class="ibox-content">
		
		<div class="row">
			<div class="col-xs-12 col-md-4 ">
				@include('prescricao.include.nutricao_padrao')
			</div>
			<div class="col-xs-12 col-md-8" style=" padding-left: 35px">
				<div class="row">
					<div class="col-xs-12 col-sm-4" style="padding:0;">
						<div class="form-group">
							<select class="form-control">
								<option value=""  selected>Objetivo</option>
								<option value=""  selected>Emagrecimento</option>
							</select>
						</div>
					</div>
					<div class="col-xs-12 col-sm-5">
						<div class="form-group">
							<select class="form-control">
								<option value="" selected disabled>Nível de habilidade</option>
								<option value="Desabilitado">Desabilitado</option>
								<option value="Habilitado">Habilitado</option>
							</select>
						</div>
					</div>
					<div class="col-xs-12 col-sm-3">
						<div class="form-group">
							<button class="btn btn-defaut btn-sm"  data-target="#treino_modelo_modal" data-toggle="modal" type="button">
							<i class="fa fa-plus-square" style="margin-right: 5px"></i> Usar modelo padrão
							</button>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-6">
						<div class="form-group">
							<select ng-options="nutricao.nutricao_nome for nutricao in nutricoes" data-ng-model="nutricao_id" ng-change="changeNutricao(nutricao_id.id)" class="form-control">
							</select>
							<span style="float:left;margin:6px 0 0 10px;"><p>@{{msgNutricao}}</p></span>
						</div>
					</div>
					<div class="col-xs-4 col-sm-3">
						<div class="form-group">
							<button data-toggle="modal" data-target="#treino_modelo_modal" class="btn btn-defaut" onclick="showCadProjeto()">
							<i class="fa fa-plus-square" style="margin-right:5px;"></i> Nutrição Padrão
							</button>
						</div>
					</div>
					<div class="col-xs-8 col-sm-3">
						<div class="form-group">
							<div class="pull-right">
								<a class="btn btn-primary" ng-disabled="addNutricaoBtn" ng-click="addNutricaoPadrao()"><i class="fa fa-plus" aria-hidden="true"></i> Nova </a>
								<a class="btn btn-danger" ng-disabled="delNutricaoBtn" ng-click="deleteNutricaoPadrao(nutricao_id.id)"><i class="fa fa-trash" aria-hidden="true"></i></a>
							</div>
						</div>
					</div>
				</div>
				<div class="row" >
					<div class="col-xs-12 col-sm-12">
						<div class="row">
							<div class="col-xs-12 col-sm-6">
								<div class="form-group" id="data_1">
									<label>Data Início</label>
									<div class="input-group date">
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
										<input type="text" class="form-control" ng-model="nutricao_datainicio" placeholder="Início">
									</div>
								</div>
							</div>
							<div class="col-xs-12 col-sm-6">
								<div class="form-group" id="data_2">
									<label>Data Revisão</label>
									<div class="input-group date">
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
										<input type="text" class="form-control" ng-model="nutricao_datatermino" placeholder="Fim">
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label>Descrição</label>
							<textarea placeholder="Descrição" ng-model="nutricao_descricao" class="form-control" rows="6"></textarea>
						</div>
					</div>
				</div>
				
				<div style="height:20px;"></div>
				<span class="pull-right" style="margin:-8px 5px 5px 0;">
					<button class="btn btn-primary" ng-disabled="salvarNutricaoBtn" ng-click="salvarNutricaoPadrao()"><i class="fa fa-check" style="margin-right:5px;"></i> Salvar</button>
				</span>
				<div class="pull-right" style="margin-right:15px;">
					<div class="checkbox m-r-xs">
						<input ng-model="nutricao_padrao" type="checkbox" id="checkbox">
						<label for="checkbox">Utilizar como nutrição padrão</label>
					</div>
				</div>
				<div style="clear:both"></div>
			</div>
			
		</div>
		{{-- askljsakl --}}
	</div>
</div>