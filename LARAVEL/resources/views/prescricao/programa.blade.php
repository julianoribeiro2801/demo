@push('scripts')

<script type="text/javascript">

    function mascaraDataPrograma(){
    $('.data_progama').datepicker({

    todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            startDate: 'today',
            endDate: '+90d',
            autoclose: true

    });
    }


    $(document).ready(function(){

    setTimeout(function(){ mascaraDataPrograma() }, 5000);
            setTimeout(function(){  $('.clockpicker').clockpicker(); }, 5000);
    });    </script>
@endpush


<div class="ibox collapsed">
    <div class="ibox-title">
        <div class="ibox-tools">
            <a class="collapse-link" ng-click="getProgramasAluno()">
                <h5>Programas de Treinamento personalizado<span></span></h5>
                <i class="fa fa-chevron-up" ng-click="getProgramasAluno()"></i>
            </a>
        </div>
    </div>

    <div class="ibox-content">
        <div class="row">
            <div class="col-xs-12 col-sm-12" style="margin-bottom:15px;">

                <div class="row">

                    <div class="pull-left" style="width: 60%">


                        <div class="form-group">      


                            <select ng-hide="delProgamaBtn" ng-model="programa_id_cod" ng-blur="selectAgendaPrograma(programa_id_cod)" ng-change="selectAgendaPrograma(programa_id_cod)" class="select2_demo_1 form-control">
                                <option ng-repeat="programaaluno in programasaluno"   ng-value="@{{programaaluno.idprogramatreinamento}}">Programa @{{$index + 1}} - @{{ programaaluno.nmprograma}} - 01/01/2018</option>  
                            </select>                  
                        </div>
                    </div>

                    <div class="pull-right" >


                        <button class="btn btn-primary" data-toggle="modal" ng-click="validaProgramaAberto()"> <!--data-target="#modalSelPrograma"--> 
                            <i class="fa fa-plus" aria-hidden="true"></i> 
                            <span class="hidden-xs"> Novo Programa </span>
                        </button>

                        <button class="btn btn-danger" ng-disabled="delProgamaBtn" ng-click="deletePrograma(programa_id_cod)">
                            <a ng-click="deletePrograma(programa_id_cod)"  >
                                <i class="fa fa-trash" aria-hidden="true" style="color: #fff"></i>
                            </a>
                        </button>

                    </div>
                </div>

            </div>
        </div>        
        <div class="row">


            <div ng-hide="delProgamaBtn"  id="outer_wrapper" style="overflow-x: auto;overflow-y: auto;">
                <div id="inner_wrapper">
                    <div  class="box"  ng-repeat="agenda in agendas">
                        <div class="divTabela" >
                            <div class="form-group" id="data_revisao_treino" style="margin-bottom: 0;">
                                <div class="pull-right" style="margin-top: 31px; margin-left: 3px">
                                    <!--<p>@{{agenda.nrdias}}</p>-->
                                </div>
                                <div style="text-align:center"><label class="font-normal">@{{agenda.nmagenda}}</label></div>
                                <input type="hidden" class="form-control tdata_mob_data" ng-model="agenda.nmagenda" value="@{{agenda.nmagenda}}">
                                <input type="hidden" class="form-control tdata_mob_data" ng-model="agenda.stagenda" value="@{{agenda.stagenda}}">


                                <div class="input-group ">

                                    <span class="input-group-addon hidden-xs"><i class="fa fa-calendar"></i></span>
                                    <input type="text" ng-disabled="agenda.stagenda=='R'" data-mask="99/99/9999" class="form-control tdata_mob_data date data_progama" ng-model="agenda.dtagenda" ng-change="verificarData(agenda, $index)" maxlength="10"  value="@{{agenda.dtagenda}}" style="width: 55%;">



                                    <button type="button" class="btn btn-primary btn-sm pull-right" style="margin-top: 3px;
                                            margin-right: -3px; border: 0;" ng-if="agenda.stagenda == 'P'" ng-click="atualizaAgenda(agenda.idprogramatreinamento, agenda.id, agenda.stagenda, agenda.dtagenda)">Pendente</button>

                                    <button type="button" class="btn btn-primary btn-sm pull-right" ng-if="agenda.stagenda == 'R'" style="    margin-top: 3px;
                                            margin-right: -3px; background: #eee; color: #333;border: 0;" ng-click="atualizaAgenda(agenda.idprogramatreinamento, agenda.id, agenda.stagenda, agenda.dtagenda)">Realizado</button>

                                </div>

                                <div class="input-group"  style="width: 100%">
                                    <input type="text" ng-disabled="true" ng-model="agenda.diasemana" value="@{{agenda.diasemana}}" class="form-control"   style="text-align:center;">
                                </div> 

                                <div class="input-group clockpicker" style="width: 100%" data-autoclose="true">
                                    <input type="tel"  ng-disabled="agenda.stagenda=='R'" ng-model="agenda.hragenda" value="@{{agenda.hragenda}}" 
                                           class="form-control time" maxlength="5"  style="text-align:center;" placeholder="--:--">
                                </div>


                            </div>
                            {{-- Fim do clock--}}

                            <div style="margin-top: 20px"></div>
                            {{-- Inicio Agendamentos--}}
                            <table class="table" ng-repeat="agendado in agenda.datas">
                                <tbody>
                                    <tr style="border: 0">
                                        <td style=" white-space: normal;   border: 1px solid #ccc;" class="btn btn-block">
                                            @{{agendado.hragenda}} - @{{agendado.name}} </td>
                                    </tr>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <span class="pull-right">
                <button ng-hide="delProgamaBtn" class="btn btn-primary" ng-click="atualizarAgendaAluno(agendas, programa_id_cod)"><i class="fa fa-check"></i> Salvar</button>
            </span>
        </div>
    </div>
    <div class="modal inmodal fade" id="modalSelPrograma" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                    <h4 class="modal-title">Selecionar programa</h4>
                </div>
                <div class="modal-body">
                    <span style="display:none;"><input type="text" ng-model="treinopadrao.treino_id"></span></span>
                    <div class="form-group">
                        <select ng-model="programa.id" ng-change="selectAgendaPrograma(programa.id)" class="select2_demo_1 form-control">
                            <option value="">Selecione o programa</option>
                            <option ng-repeat="programa in programastreinamento"   value="@{{ programa.id}}">@{{ programa.nmprograma}}</option>	
                        </select>                       
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" ng-click="salvarAgendaAluno(agendas)"><i class="fa fa-check"></i> Salvar</button>
                </div>
            </div>
        </div>
    </div>    
</div>

{{-- @todo MASK Input estava dando conflito com date piker
             @push('scripts')


             @endpush  --}}