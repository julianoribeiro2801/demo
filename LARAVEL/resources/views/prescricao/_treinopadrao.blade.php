
<div class="tabs-container">
    <ul class="nav nav-tabs">
        <li class="active" ng-show="tabsStatus.tab1">
            <a data-toggle="tab" href="#tab-1">
                A
            </a>
        </li>
        <li ng-show="tabsStatus.tab2">
            <a data-toggle="tab" href="#tab-2">
                B
            </a>
        </li>
        <li ng-show="tabsStatus.tab3">
            <a data-toggle="tab" href="#tab-3">
                C
            </a>
            
        </li>
        <li ng-show="tabsStatus.tab4">
            <a data-toggle="tab" href="#tab-4">
                D
            </a>
        </li>
        <li ng-show="tabsStatus.tab5">
            <a data-toggle="tab" href="#tab-5">
                E
            </a>
        </li>
        <li ng-show="tabsStatus.tab6">
            <a data-toggle="tab" href="#tab-6">
                F
            </a>
        </li>
        <li ng-show="tabsStatus.tab7">
            <a data-toggle="tab" href="#tab-7">
                G
            </a>
        </li>
    </ul>
    <div class="tab-content">


        <!-- INICIO DA ABA 1 -->
        <div class="tab-pane active" id="tab-1" ng-show="tabsStatus.tab1">

            <div class="panel-body" style="padding:7px 20px;">
                <div class="row clearfix ScrrolAba" style="margin-top: 6px;  ">

                    <table class="table table-bordered pagin-table" id="abaAaa">
                        <tbody ng-model="linhaA">

                            <tr class="ordena-treino" ng-model="tabs" ng-repeat="linhaA in linhasA" style="background:#f5f5f5;" ng-disabled="addLinhaBtn">
                                <td id="ord" ng-hide="true">@{{linhaA.ficha}}</td>
                                <td id="exercicio" ng-hide="true">@{{linhaA.exercicio}}</td>
                                <td id="series" ng-hide="true">@{{linhaA.series}}</td>
                                <td id="repeticoes" ng-hide="true">@{{linhaA.repeticoes}}</td>
                                <td id="medida_duracao" ng-hide="true">@{{linhaA.medida_duracao}}</td>
                                <td id="carga" ng-hide="true">@{{linhaA.carga}}</td>
                                <td id="medida_intensidade" ng-hide="true">@{{linhaA.medida_intensidade}}</td>
                                <td id="intervalo" ng-hide="true">@{{linhaA.intervalo}}</td>   
                                <td id="super_serie" ng-hide="true">@{{linhaA.super_serie}}</td>   
                                <td id="hash_code" ng-hide="true">@{{linhaA.hash_code}}</td>   
                                <td id="observacao" ng-hide="true">@{{linhaA.observacao}}</td>   
                                
                                <td style="padding: 0 ">
                                    <!-- EXIBE O NÚMERO de linhas -->
                                    <span class="marcador_exer">
                                        @{{linhaA.id}}
                                    </span>
                                    <!-- INICIO DA LINHA DO EXERCICIO -->
                                    <div class="row" style="margin-right: -6px;">
                                        <!-- {{-- <div class="col-xs-12 col-sm-1 hidden-xs" style="padding:0;     padding-right: 8px;">
                                         </div> --}}-->
                                        <div class="col-xs-12 col-sm-12"  >

                                            {{-- INICIO DA TABELA NOVA  --}}

                                            <table class="table_treino">
                                                <thead>
                                                    <tr>
                                                        <th style="width: 9%;">


                                                        </th>
                                                        <th style="width: 44%;">    
                                                            <select class="form-control selectTreinoNew" 
                                                                    ng-change="changeGenericoTesteA(linhaA.super_serie, linhaA)" ng-model="linhaA.super_serie" 
                                                                    required="required">
                                                                <option ng-repeat="super_serie in super_serieVetor" 
                                                                        ng-value="super_serie.valor">
                                                                    @{{super_serie.texto}}
                                                                </option>                                                              
                                                            </select>  
                                                        </th>
                                                        <th style="width: 8%;" class="nobold">Série</th>
                                                        <th style="width: 9.5%;"> 
                                                            <select class="form-control selectTreinoTransparent" ng-model="linhaA.medida_duracao" ng-disabled="addLinhaBtn" required="required">
                                                                <option ng-repeat="medida_duracao in medida_duracaosVetor" ng-value="medida_duracao.valor">
                                                                    @{{medida_duracao.texto}}
                                                                </option>                                                                
                                                            </select>  
                                                        </th>
                                                        <th style="width: 9.5%;"> 
                                                            <select class="form-control selectTreinoTransparent" 
                                                                    ng-model="linhaA.medida_intensidade"
                                                                    ng-disabled="addLinhaBtn" ng-change="changeGenericoTeste(linhaA.medida_intensidade)"
                                                                    required="required">
                                                                <option ng-repeat="medida_intensidade in medida_intensidadeVetor" 
                                                                        ng-value="medida_intensidade.valor">
                                                                    @{{medida_intensidade.texto}}
                                                                </option>                                                                       
                                                            </select>  
                                                        </th>
                                                        <th style="width: 8.75%;" class="nobold">Intervalo</th>

                                                        <th style="width: 94px">

                                                        </th>

                                                    </tr>
                                                </thead>

                                                <tbody>
                                                    {{-- LINHA Exercicio Simples --}}
                                                    <tr class="exercicio-simples">
                                                        <td style="padding:0"> 
                                                            <!-- FOTO -->
                                                            <div href="#" data-toggle="tooltip" title="Clique e arreste para mudar de posição" ng-click="rodaPreview(linhaA.video)"
                                                                 style="background-image:url('{{ url('/')}}/uploads/exercicios/@{{exercicios[changeExercicio(linhaA.exercicio)].dsurlminiatura}}');" class="imgex">
                                                            </div>
                                                        </td>
                                                        <td> 
                                                            <div class="form-group" style="padding-bottom: 0">
                                                                <div class="input-group" style="width: 100%;">

                                                                    <select class="chosen-select" id="execicioSelectA@{{linhaA.id}}"  ng-change="changeExercicio(linhaA.exercicio, linhaA)" ng-model="linhaA.exercicio" ng-disabled="addLinhaBtn"  >
                                                                        <option disabled="" selected="" value="">
                                                                            Selecione o Exercício
                                                                        </option>
                                                                        <option ng-repeat="exercicio in exercicios" ng-value="exercicio.id">
                                                                            @{{exercicio.nome_exercicio}}
                                                                        </option>
                                                                    </select>

                                                                    <a class="btn btn-default input-group-addon" data-target="#m_exercicios" data-toggle="modal" ng-disabled="addLinhaBtn">
                                                                        <i aria-hidden="true" class="fa fa-plus">
                                                                        </i>
                                                                    </a>

                                                                </div>
                                                            </div>
                                                        </td>

                                                        <td>  <select class="form-control inputInlineTreinoNew" ng-change="calcTempoA()" ng-model="linhaA.series" ng-disabled="addLinhaBtn">
                                                                <option disabled="" selected="" value="">
                                                                    0x
                                                                </option>
                                                                <option ng-repeat="serie in seriesVetor" ng-value="serie.valor | number">
                                                                    @{{serie.texto}}
                                                                </option>
                                                            </select> </td>
                                                        <td>  <input class="form-control inputInlineTreinoNew" ng-blur="calcTempoA()" ng-model="linhaA.repeticoes" placeholder="0"
                                                                     type="text" ng-disabled="addLinhaBtn"></td>


                                                        <td>   <input class="form-control inputInlineTreinoNew" ng-model="linhaA.carga" placeholder="0" type="text" ng-disabled="addLinhaBtn"></td>
                                                        <td>  <select class="form-control inputInlineTreinoNew" ng-change="calcTempoA()" ng-model="linhaA.intervalo" ng-disabled="addLinhaBtn"  ng-show="linhaA.super_serie == 1">
                                                                <option disabled="" selected="" value="">
                                                                    0 seg
                                                                </option>
                                                                <option ng-repeat="intervalo in intervalosVetor" ng-value="intervalo.valor | number">
                                                                    @{{intervalo.texto}}
                                                                </option>
                                                            </select>
                                                        </td>

                                                        <td style="padding:0">

                                                            <!-- INICIO FECHAR E EXCLUIR -->
                                                            <span class="pull-right" style="    margin-top: -17px;">
                                                                <a class="btn btn-default " style="height: 31px; padding-top: 3px;"
                                                                   data-target="#modalObsA@{{linhaA.id}}" data-toggle="modal" ng-disabled="addLinhaBtn">
                                                                    <i aria-hidden="true" class="fa fa-comment"></i>
                                                                </a>
                                                                <a class="btn btn-danger " style="height: 31px;  padding-top: 3px; border-color: #e5e6e8;"
                                                                   ng-click="removeLinhaA($index, linhaA.ficha)" style=" border: 0;" ng-disabled="addLinhaBtn">
                                                                    <i aria-hidden="true" class="fa fa-trash"></i>
                                                                </a>

                                                            </span>
                                                            <!-- FIM FECHAR E EXCLUIR -->


                                                            <!-- OBSERVAÇÃO DO EXERCICIO TREINO A -->
                                                            <div aria-hidden="true" class="modal inmodal fade" id="modalObsA@{{linhaA.id}}" role="dialog" tabindex="-1">
                                                                <div class="modal-dialog">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <button class="close" data-dismiss="modal" type="button">
                                                                                <span aria-hidden="true">
                                                                                    ×
                                                                                </span>
                                                                                <span class="sr-only">
                                                                                    Fechar
                                                                                </span>
                                                                            </button>
                                                                            <h4 class="modal-title">
                                                                                Observação do Exercício
                                                                            </h4>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <div class="form-group">
                                                                                <input class="form-control" ng-model="linhaA.observacao" placeholder="Informe uma observação" type="text">
                                                                                </input>
                                                                            </div>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button class="btn btn-primary" data-dismiss="modal" type="button">
                                                                                OK
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- modal -->
                                                            <!-- OBSERVAÇÃO DO EXERCICIO TREINO A -->


                                                            <!-- modal -->

                                                        </td>
                                                    </tr>

                                                    <tr ng-repeat="super in linhaA.superserie" class="bi-serie">
                                                        <td id="ord_super" ng-hide="true">@{{super.ficha}}</td>
                                                        <td id="exercicio_super" ng-hide="true">@{{super.exercicio}}</td>
                                                        <td id="series_super" ng-hide="true">@{{super.series}}</td>
                                                        <td id="repeticoes_super" ng-hide="true">@{{super.repeticoes}}</td>
                                                        <td id="medida_duracao_super" ng-hide="true">@{{super.medida_duracao}}</td>
                                                        <td id="carga_super" ng-hide="true">@{{super.carga}}</td>
                                                        <td id="medida_intensidade_super" ng-hide="true">@{{super.medida_intensidade}}</td>
                                                        <td id="intervalo_super" ng-hide="true">@{{super.intervalo}}</td>   
                                                        <td id="super_serie_super" ng-hide="true">@{{super.super_serie}}</td>                                                           
                                                        <td  class="nameex">
                                                            <!-- FOTO -->
                                                            <div href="#" data-toggle="tooltip" title="Clique e arreste para mudar de posição" ng-click="rodaPreview(super.video)" 
                                                                 style="background-image:url('{{ url('/')}}/uploads/exercicios/@{{exercicios[changeExercicio(super.exercicio)].dsurlminiatura}}');" class="imgex-bi">
                                                            </div>
                                                        </td>

                                                        <td > 

                                                            <div class="form-group" style="padding-bottom: 0">

                                                                <div class="input-group" style="width: 100%;">
                                                                    <!--<div ng-repeat="super in linhaA.superserie" ng-value="exercicio.id">
                                                                        @{{super.exercicio}}
                                                                    </div>    -->

                                                                    <select class="chosen-select" id="execicioSelectA-super@{{super.id}}"  ng-change="changeExercicio(super.exercicio, linhaA)" ng-model="super.exercicio" ng-disabled="addLinhaBtn"  >
                                                                        <option disabled="" selected="" value="">

                                                                            Selecione o Exercício @{{super.id}} - @{{linhaA.superserie.length}}
                                                                        </option>
                                                                        <option ng-repeat="exercicio in exercicios" ng-value="exercicio.id">
                                                                            @{{ exercicio.nome_exercicio}}
                                                                        </option>
                                                                    </select>

                                                                    <a class="btn btn-default input-group-addon" data-target="#m_exercicios" data-toggle="modal" ng-disabled="addLinhaBtn">
                                                                        <i aria-hidden="true" class="fa fa-plus">
                                                                        </i>
                                                                    </a>

                                                                </div>
                                                            </div>
                                                        </td>

                                                        <td>  <select class="form-control inputInlineTreinoNew" ng-change="calcTempoA()" ng-model="super.series" ng-disabled="addLinhaBtn">
                                                                <option disabled="" selected="" value="">
                                                                    0x
                                                                </option>
                                                                <option ng-repeat="serie in seriesVetor" ng-value="serie.valor | number">
                                                                    @{{serie.texto}}
                                                                </option>
                                                            </select> </td>
                                                        <td>  <input class="form-control inputInlineTreinoNew" ng-blur="calcTempoA()" ng-model="super.repeticoes" placeholder="0"
                                                                     type="text" ng-disabled="addLinhaBtn"></td>


                                                        <td>   <input class="form-control inputInlineTreinoNew" ng-model="super.carga" placeholder="0" type="text" ng-disabled="addLinhaBtn"></td>
                                                        <td>  <select class="form-control inputInlineTreinoNew" ng-change="calcTempoA()" ng-model="super.intervalo" ng-disabled="addLinhaBtn"  ng-show="super.id == linhaA.superserie.length">
                                                                <option disabled="" selected="" value="">
                                                                    0 seg
                                                                </option>
                                                                <option ng-repeat="intervalo in intervalosVetor" ng-value="intervalo.valor | number">
                                                                    @{{intervalo.texto}}
                                                                </option>
                                                            </select>
                                                        </td>

                                                        <td style="padding:0">

                                                            <!-- INICIO FECHAR E EXCLUIR -->
                                                            <span class="pull-right" style="    margin-top: -17px;">
                                                                <a class="btn btn-default " style="height: 31px; padding-top: 3px;"
                                                                   data-target="#modalObsSUPERA@{{super.id}}" data-toggle="modal" ng-disabled="addLinhaBtn">
                                                                    <i aria-hidden="true" class="fa fa-comment"></i>
                                                                </a>
                                                                <a class="btn btn-danger " style="height: 31px;  padding-top: 3px; border-color: #e5e6e8;"
                                                                   ng-click="removeLinhaSuper($index, super, linhaA.superserie, linhaA.ficha)" style=" border: 0;" ng-disabled="addLinhaBtn">
                                                                    <i aria-hidden="true" class="fa fa-trash"></i>
                                                                </a>

                                                            </span>
                                                            <!-- FIM FECHAR E EXCLUIR -->
                                                            
                                                            <div aria-hidden="true" class="modal inmodal fade" id="modalObsSUPERA@{{super.id}}" role="dialog" tabindex="-1">
                                                                <div class="modal-dialog">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <button class="close" data-dismiss="modal" type="button">
                                                                                <span aria-hidden="true">
                                                                                    ×
                                                                                </span>
                                                                                <span class="sr-only">
                                                                                    Fechar
                                                                                </span>
                                                                            </button>
                                                                            <h4 class="modal-title">
                                                                                Observação do Exercício
                                                                            </h4>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <div class="form-group">
                                                                                <input class="form-control" ng-model="super.observacao" placeholder="Informe uma observação" type="text">
                                                                                </input>
                                                                            </div>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button class="btn btn-primary" data-dismiss="modal" type="button">
                                                                                OK
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>                                                            
                                                            <!-- modal -->

                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            {{-- FIM DA TABELA NOVA  --}}

                                        </div>

                                    </div>
                                </td>
                            </tr>

                        </tbody>
                    </table>
                    <!-- fim tabele -->

                </div>
                <div class="form-group" style="margin-top:10px;">
                    <span style="text-align:left;margin-left:-15px;">
                        <a class="btn btn-primary" ng-click="addLinhaA()" ng-disabled="addLinhaBtn">
                            <i aria-hidden="true" class="fa fa-plus">
                            </i>
                        </a>
                    </span>
                    <span class="pull-right" style="margin-right:-15px;text-align:right;">
                        <b>
                            Tempo estimado @{{tempoestimadoA}}
                        </b>
                        <br> Musculação: @{{tempomusculacaoA}} + Aquecimento: 10 min + Alongamento: 4 min
                        </br>
                    </span>
                </div>
                <div class="clearfix">
                </div>
            </div>
        </div>
        <!-- fim table panel 1 -->



        <!-- INICIO DA ABA 2 -->
        <div class="tab-pane" id="tab-2" ng-show="tabsStatus.tab2">

            <div class="panel-body" style="padding:7px 20px;">
                <div class="row clearfix ScrrolAba" style="margin-top: 6px;  ">

                    <table class="table table-bordered pagin-table" id="abaBbb">
                        <tbody ng-model="linhaB">

                            <tr class="ordena-treino" ng-model="tabs" ng-repeat="linhaB in linhasB" style="background:#f5f5f5;" ng-disabled="addLinhaBtn">
                                <td id="ord" ng-hide="true">@{{linhaB.ficha}}</td>
                                <td id="exercicio" ng-hide="true">@{{linhaB.exercicio}}</td>
                                <td id="series" ng-hide="true">@{{linhaB.series}}</td>
                                <td id="repeticoes" ng-hide="true">@{{linhaB.repeticoes}}</td>
                                <td id="medida_duracao" ng-hide="true">@{{linhaB.medida_duracao}}</td>
                                <td id="carga" ng-hide="true">@{{linhaB.carga}}</td>
                                <td id="medida_intensidade" ng-hide="true">@{{linhaB.medida_intensidade}}</td>
                                <td id="intervalo" ng-hide="true">@{{linhaB.intervalo}}</td>   
                                <td id="super_serie" ng-hide="true">@{{linhaB.super_serie}}</td>   
                                <td id="hash_code" ng-hide="true">@{{linhaB.hash_code}}</td>   
                                <td id="observacao" ng-hide="true">@{{linhaB.observacao}}</td>   
                                
                                <td style="padding: 0 ">
                                    <!-- EXIBE O NÚMERO de linhas -->
                                    <span class="marcador_exer">
                                        @{{linhaB.id}}
                                    </span>
                                    <!-- INICIO DA LINHA DO EXERCICIO -->
                                    <div class="row" style="margin-right: -6px;">
                                        <!-- {{-- <div class="col-xs-12 col-sm-1 hidden-xs" style="padding:0;     padding-right: 8px;">
                                         </div> --}}-->
                                        <div class="col-xs-12 col-sm-12"  >

                                            {{-- INICIO DA TABELA NOVA  --}}

                                            <table class="table_treino">
                                                <thead>
                                                    <tr>
                                                        <th style="width: 9%;">


                                                        </th>
                                                        <th style="width: 44%;">    
                                                            <select class="form-control selectTreinoNew" 
                                                                    ng-change="changeGenericoTesteA(linhaB.super_serie, linhaB)" ng-model="linhaB.super_serie" 
                                                                    required="required">
                                                                <option ng-repeat="super_serie in super_serieVetor" 
                                                                        ng-value="super_serie.valor">
                                                                    @{{super_serie.texto}}
                                                                </option>                                                              
                                                            </select>  
                                                        </th>
                                                        <th style="width: 8%;" class="nobold">Série</th>
                                                        <th style="width: 9.5%;"> 
                                                            <select class="form-control selectTreinoTransparent" ng-model="linhaB.medida_duracao" ng-disabled="addLinhaBtn" required="required">
                                                                <option ng-repeat="medida_duracao in medida_duracaosVetor" ng-value="medida_duracao.valor">
                                                                    @{{medida_duracao.texto}}
                                                                </option>                                                                
                                                            </select>  
                                                        </th>
                                                        <th style="width: 9.5%;"> 
                                                            <select class="form-control selectTreinoTransparent" 
                                                                    ng-model="linhaB.medida_intensidade"
                                                                    ng-disabled="addLinhaBtn" ng-change="changeGenericoTeste(linhaB.medida_intensidade)"
                                                                    required="required">
                                                                <option ng-repeat="medida_intensidade in medida_intensidadeVetor" 
                                                                        ng-value="medida_intensidade.valor">
                                                                    @{{medida_intensidade.texto}}
                                                                </option>                                                                       
                                                            </select>  
                                                        </th>
                                                        <th style="width: 8.75%;" class="nobold">Intervalo</th>

                                                        <th style="width: 94px">

                                                        </th>

                                                    </tr>
                                                </thead>

                                                <tbody>
                                                    {{-- LINHA Exercicio Simples --}}
                                                    <tr class="exercicio-simples">
                                                        <td style="padding:0"> 
                                                            <!-- FOTO -->
                                                            <div href="#" data-toggle="tooltip" title="Clique e arreste para mudar de posição" ng-click="rodaPreview(linhaB.video)" 
                                                                 style="background-image:url('{{ url('/')}}/uploads/exercicios/@{{exercicios[changeExercicio(linhaB.exercicio)].dsurlminiatura}}');" class="imgex">
                                                            </div>
                                                        </td>
                                                        <td> 
                                                            <div class="form-group" style="padding-bottom: 0">
                                                                <div class="input-group" style="width: 100%;">

                                                                    <select class="chosen-select" id="execicioSelectB@{{linhaB.id}}"  ng-change="changeExercicio(linhaB.exercicio, linhaB)" ng-model="linhaB.exercicio" ng-disabled="addLinhaBtn"  >
                                                                        <option disabled="" selected="" value="">
                                                                            Selecione o Exercício
                                                                        </option>
                                                                        <option ng-repeat="exercicio in exercicios" ng-value="exercicio.id">
                                                                            @{{exercicio.nome_exercicio}}
                                                                        </option>
                                                                    </select>

                                                                    <a class="btn btn-default input-group-addon" data-target="#m_exercicios" data-toggle="modal" ng-disabled="addLinhaBtn">
                                                                        <i aria-hidden="true" class="fa fa-plus">
                                                                        </i>
                                                                    </a>

                                                                </div>
                                                            </div>
                                                        </td>

                                                        <td>  <select class="form-control inputInlineTreinoNew" ng-change="calcTempoB()" ng-model="linhaB.series" ng-disabled="addLinhaBtn">
                                                                <option disabled="" selected="" value="">
                                                                    0x
                                                                </option>
                                                                <option ng-repeat="serie in seriesVetor" ng-value="serie.valor | number">
                                                                    @{{serie.texto}}
                                                                </option>
                                                            </select> </td>
                                                        <td>  <input class="form-control inputInlineTreinoNew" ng-blur="calcTempoB()" ng-model="linhaB.repeticoes" placeholder="0"
                                                                     type="text" ng-disabled="addLinhaBtn"></td>


                                                        <td>   <input class="form-control inputInlineTreinoNew" ng-model="linhaB.carga" placeholder="0" type="text" ng-disabled="addLinhaBtn"></td>
                                                        <td>  <select class="form-control inputInlineTreinoNew" ng-change="calcTempoB()" ng-model="linhaB.intervalo" ng-disabled="addLinhaBtn"  ng-show="linhaB.super_serie == 1">
                                                                <option disabled="" selected="" value="">
                                                                    0 seg
                                                                </option>
                                                                <option ng-repeat="intervalo in intervalosVetor" ng-value="intervalo.valor | number">
                                                                    @{{intervalo.texto}}
                                                                </option>
                                                            </select>
                                                        </td>

                                                        <td style="padding:0">

                                                            <!-- INICIO FECHAR E EXCLUIR -->
                                                            <span class="pull-right" style="    margin-top: -17px;">
                                                                <a class="btn btn-default " style="height: 31px; padding-top: 3px;"
                                                                   data-target="#modalObsB@{{linhaB.id}}" data-toggle="modal" ng-disabled="addLinhaBtn">
                                                                    <i aria-hidden="true" class="fa fa-comment"></i>
                                                                </a>
                                                                <a class="btn btn-danger " style="height: 31px;  padding-top: 3px; border-color: #e5e6e8;"
                                                                   ng-click="removeLinhaB($index, linhaB.ficha)" style=" border: 0;" ng-disabled="addLinhaBtn">
                                                                    <i aria-hidden="true" class="fa fa-trash"></i>
                                                                </a>

                                                            </span>
                                                            <!-- FIM FECHAR E EXCLUIR -->


                                                            <!-- OBSERVAÇÃO DO EXERCICIO TREINO A -->
                                                            <div aria-hidden="true" class="modal inmodal fade" id="modalObsB@{{linhaB.id}}" role="dialog" tabindex="-1">
                                                                <div class="modal-dialog">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <button class="close" data-dismiss="modal" type="button">
                                                                                <span aria-hidden="true">
                                                                                    ×
                                                                                </span>
                                                                                <span class="sr-only">
                                                                                    Fechar
                                                                                </span>
                                                                            </button>
                                                                            <h4 class="modal-title">
                                                                                Observação do Exercício
                                                                            </h4>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <div class="form-group">
                                                                                <input class="form-control" ng-model="linhaB.observacao" placeholder="Informe uma observação" type="text">
                                                                                </input>
                                                                            </div>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button class="btn btn-primary" data-dismiss="modal" type="button">
                                                                                OK
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- modal -->
                                                            <!-- OBSERVAÇÃO DO EXERCICIO TREINO A -->


                                                            <!-- modal -->

                                                        </td>
                                                    </tr>

                                                    <tr ng-repeat="super in linhaB.superserie" class="bi-serie">
                                                        <td id="ord_super" ng-hide="true">@{{super.ficha}}</td>
                                                        <td id="exercicio_super" ng-hide="true">@{{super.exercicio}}</td>
                                                        <td id="series_super" ng-hide="true">@{{super.series}}</td>
                                                        <td id="repeticoes_super" ng-hide="true">@{{super.repeticoes}}</td>
                                                        <td id="medida_duracao_super" ng-hide="true">@{{super.medida_duracao}}</td>
                                                        <td id="carga_super" ng-hide="true">@{{super.carga}}</td>
                                                        <td id="medida_intensidade_super" ng-hide="true">@{{super.medida_intensidade}}</td>
                                                        <td id="intervalo_super" ng-hide="true">@{{super.intervalo}}</td>   
                                                        <td id="super_serie_super" ng-hide="true">@{{super.super_serie}}</td>                                                           
                                                        <td  class="nameex">
                                                            <!-- FOTO -->
                                                            <div href="#" data-toggle="tooltip" title="Clique e arreste para mudar de posição" ng-click="rodaPreview(super.video)" 
                                                                 style="background-image:url('{{ url('/')}}/uploads/exercicios/@{{exercicios[changeExercicio(super.exercicio)].dsurlminiatura}}');" class="imgex-bi">
                                                            </div>
                                                        </td>

                                                        <td > 

                                                            <div class="form-group" style="padding-bottom: 0">

                                                                <div class="input-group" style="width: 100%;">
                                                                    <!--<div ng-repeat="super in linhaB.superserie" ng-value="exercicio.id">
                                                                        @{{super.exercicio}}
                                                                    </div>    -->

                                                                    <select class="chosen-select" id="execicioSelectB-super@{{super.id}}"  ng-change="changeExercicio(super.exercicio, linhaB)" ng-model="super.exercicio" ng-disabled="addLinhaBtn"  >
                                                                        <option disabled="" selected="" value="">

                                                                            Selecione o Exercício
                                                                        </option>
                                                                        <option ng-repeat="exercicio in exercicios" ng-value="exercicio.id">
                                                                            @{{ exercicio.nome_exercicio}}
                                                                        </option>
                                                                    </select>

                                                                    <a class="btn btn-default input-group-addon" data-target="#m_exercicios" data-toggle="modal" ng-disabled="addLinhaBtn">
                                                                        <i aria-hidden="true" class="fa fa-plus">
                                                                        </i>
                                                                    </a>

                                                                </div>
                                                            </div>
                                                        </td>

                                                        <td>  <select class="form-control inputInlineTreinoNew" ng-change="calcTempoB()" ng-model="super.series" ng-disabled="addLinhaBtn">
                                                                <option disabled="" selected="" value="">
                                                                    0x
                                                                </option>
                                                                <option ng-repeat="serie in seriesVetor" ng-value="serie.valor | number">
                                                                    @{{serie.texto}}
                                                                </option>
                                                            </select> </td>
                                                        <td>  <input class="form-control inputInlineTreinoNew" ng-blur="calcTempoB()" ng-model="super.repeticoes" placeholder="0"
                                                                     type="text" ng-disabled="addLinhaBtn"></td>


                                                        <td><input class="form-control inputInlineTreinoNew" ng-model="super.carga" placeholder="0" type="text" ng-disabled="addLinhaBtn"></td>
                                                        <td><select class="form-control inputInlineTreinoNew" ng-change="calcTempoB()" ng-model="super.intervalo" ng-disabled="addLinhaBtn"  ng-show="super.id == linhaB.superserie.length">
                                                                <option disabled="" selected="" value="">
                                                                    0 seg
                                                                </option>
                                                                <option ng-repeat="intervalo in intervalosVetor" ng-value="intervalo.valor | number">
                                                                    @{{intervalo.texto}}
                                                                </option>
                                                            </select>
                                                        </td>

                                                        <td style="padding:0">

                                                            <!-- INICIO FECHAR E EXCLUIR -->
                                                            <span class="pull-right" style="    margin-top: -17px;">
                                                                <a class="btn btn-default " style="height: 31px; padding-top: 3px;"
                                                                   data-target="#modalObsSUPERB@{{super.id}}" data-toggle="modal" ng-disabled="addLinhaBtn">
                                                                    <i aria-hidden="true" class="fa fa-comment"></i>
                                                                </a>
                                                                <a class="btn btn-danger " style="height: 31px;  padding-top: 3px; border-color: #e5e6e8;"
                                                                   ng-click="removeLinhaSuper($index, super, linhaB.superserie, linhaB.ficha)" style=" border: 0;" ng-disabled="addLinhaBtn">
                                                                    <i aria-hidden="true" class="fa fa-trash"></i>
                                                                </a>

                                                            </span>
                                                            <!-- FIM FECHAR E EXCLUIR -->
                                                            
                                                            <div aria-hidden="true" class="modal inmodal fade" id="modalObsSUPERB@{{super.id}}" role="dialog" tabindex="-1">
                                                                <div class="modal-dialog">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <button class="close" data-dismiss="modal" type="button">
                                                                                <span aria-hidden="true">
                                                                                    ×
                                                                                </span>
                                                                                <span class="sr-only">
                                                                                    Fechar
                                                                                </span>
                                                                            </button>
                                                                            <h4 class="modal-title">
                                                                                Observação do Exercício
                                                                            </h4>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <div class="form-group">
                                                                                <input class="form-control" ng-model="super.observacao" placeholder="Informe uma observação" type="text">
                                                                                </input>
                                                                            </div>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button class="btn btn-primary" data-dismiss="modal" type="button">
                                                                                OK
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>                                                            
                                                            <!-- modal -->

                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            {{-- FIM DA TABELA NOVA  --}}

                                        </div>

                                    </div>
                                </td>
                            </tr>

                        </tbody>
                    </table>
                    <!-- fim tabele -->

                </div>
                <div class="form-group" style="margin-top:10px;">
                    <span style="text-align:left;margin-left:-15px;">
                        <a class="btn btn-primary" ng-click="addLinhaB()" ng-disabled="addLinhaBtn">
                            <i aria-hidden="true" class="fa fa-plus">
                            </i>
                        </a>
                    </span>
                    <span class="pull-right" style="margin-right:-15px;text-align:right;">
                        <b>
                            Tempo estimado @{{tempoestimadoA}}
                        </b>
                        <br> Musculação: @{{tempomusculacaoA}} + Aquecimento: 10 min + Alongamento: 4 min
                        </br>
                    </span>
                </div>
                <div class="clearfix">
                </div>
            </div>
        </div>
        <!-- fim table panel 2 (B) -->

        <!-- INICIO DA ABA panel 3 (C)  -->
        <div class="tab-pane" id="tab-3" ng-show="tabsStatus.tab3">

            <div class="panel-body" style="padding:7px 20px;">
                <div class="row clearfix ScrrolAba" style="margin-top: 6px;  ">

                    <table class="table table-bordered pagin-table" id="abaCcc">
                        <tbody ng-model="linhaC">

                            <tr class="ordena-treino" ng-model="tabs" ng-repeat="linhaC in linhasC" style="background:#f5f5f5;" ng-disabled="addlinhaCtn">
                                <td id="ord" ng-hide="true">@{{linhaC.ficha}}</td>
                                <td id="exercicio" ng-hide="true">@{{linhaC.exercicio}}</td>
                                <td id="series" ng-hide="true">@{{linhaC.series}}</td>
                                <td id="repeticoes" ng-hide="true">@{{linhaC.repeticoes}}</td>
                                <td id="medida_duracao" ng-hide="true">@{{linhaC.medida_duracao}}</td>
                                <td id="carga" ng-hide="true">@{{linhaC.carga}}</td>
                                <td id="medida_intensidade" ng-hide="true">@{{linhaC.medida_intensidade}}</td>
                                <td id="intervalo" ng-hide="true">@{{linhaC.intervalo}}</td>   
                                <td id="super_serie" ng-hide="true">@{{linhaC.super_serie}}</td>   
                                <td id="hash_code" ng-hide="true">@{{linhaC.hash_code}}</td>   
                                <td id="observacao" ng-hide="true">@{{linhaC.observacao}}</td>   
                                
                                <td style="padding: 0 ">
                                    <!-- EXIBE O NÚMERO de linhas -->
                                    <span class="marcador_exer">
                                        @{{linhaC.id}}
                                    </span>
                                    <!-- INICIO DA LINHA DO EXERCICIO -->
                                    <div class="row" style="margin-right: -6px;">
                                        <!-- {{-- <div class="col-xs-12 col-sm-1 hidden-xs" style="padding:0;     padding-right: 8px;">
                                         </div> --}}-->
                                        <div class="col-xs-12 col-sm-12"  >

                                            {{-- INICIO DA TABELA NOVA  --}}

                                            <table class="table_treino">
                                                <thead>
                                                    <tr>
                                                        <th style="width: 9%;">


                                                        </th>
                                                        <th style="width: 44%;">    
                                                            <select class="form-control selectTreinoNew" 
                                                                    ng-change="changeGenericoTesteA(linhaC.super_serie, linhaC)" ng-model="linhaC.super_serie" 
                                                                    required="required">
                                                                <option ng-repeat="super_serie in super_serieVetor" 
                                                                        ng-value="super_serie.valor">
                                                                    @{{super_serie.texto}}
                                                                </option>                                                              
                                                            </select>  
                                                        </th>
                                                        <th style="width: 8%;" class="nobold">Série</th>
                                                        <th style="width: 9.5%;"> 
                                                            <select class="form-control selectTreinoTransparent" ng-model="linhaC.medida_duracao" ng-disabled="addlinhaCtn" required="required">
                                                                <option ng-repeat="medida_duracao in medida_duracaosVetor" ng-value="medida_duracao.valor">
                                                                    @{{medida_duracao.texto}}
                                                                </option>                                                                
                                                            </select>  
                                                        </th>
                                                        <th style="width: 9.5%;"> 
                                                            <select class="form-control selectTreinoTransparent" 
                                                                    ng-model="linhaC.medida_intensidade"
                                                                    ng-disabled="addlinhaCtn" ng-change="changeGenericoTeste(linhaC.medida_intensidade)"
                                                                    required="required">
                                                                <option ng-repeat="medida_intensidade in medida_intensidadeVetor" 
                                                                        ng-value="medida_intensidade.valor">
                                                                    @{{medida_intensidade.texto}}
                                                                </option>                                                                       
                                                            </select>  
                                                        </th>
                                                        <th style="width: 8.75%;" class="nobold">Intervalo</th>

                                                        <th style="width: 94px">

                                                        </th>

                                                    </tr>
                                                </thead>

                                                <tbody>
                                                    {{-- LINHA Exercicio Simples --}}
                                                    <tr class="exercicio-simples">
                                                        <td style="padding:0"> 
                                                            <!-- FOTO -->
                                                            <div href="#" data-toggle="tooltip" title="Clique e arreste para mudar de posição" ng-click="rodaPreview(linhaC.video)" 
                                                                 style="background-image:url('{{ url('/')}}/uploads/exercicios/@{{exercicios[changeExercicio(linhaC.exercicio)].dsurlminiatura}}');" class="imgex">
                                                            </div>
                                                        </td>
                                                        <td> 
                                                            <div class="form-group" style="padding-bottom: 0">
                                                                <div class="input-group" style="width: 100%;">

                                                                    <select class="chosen-select" id="execicioSelectC@{{linhaC.id}}"  ng-change="changeExercicio(linhaC.exercicio, linhaC)" ng-model="linhaC.exercicio" ng-disabled="addlinhaCtn"  >
                                                                        <option disabled="" selected="" value="">
                                                                            Selecione o Exercício
                                                                        </option>
                                                                        <option ng-repeat="exercicio in exercicios" ng-value="exercicio.id">
                                                                            @{{exercicio.nome_exercicio}}
                                                                        </option>
                                                                    </select>

                                                                    <a class="btn btn-default input-group-addon" data-target="#m_exercicios" data-toggle="modal" ng-disabled="addlinhaCtn">
                                                                        <i aria-hidden="true" class="fa fa-plus">
                                                                        </i>
                                                                    </a>

                                                                </div>
                                                            </div>
                                                        </td>

                                                        <td>  <select class="form-control inputInlineTreinoNew" ng-change="calcTempoC()" ng-model="linhaC.series" ng-disabled="addlinhaCtn">
                                                                <option disabled="" selected="" value="">
                                                                    0x
                                                                </option>
                                                                <option ng-repeat="serie in seriesVetor" ng-value="serie.valor | number">
                                                                    @{{serie.texto}}
                                                                </option>
                                                            </select> </td>
                                                        <td>  <input class="form-control inputInlineTreinoNew" ng-blur="calcTempoC()" ng-model="linhaC.repeticoes" placeholder="0"
                                                                     type="text" ng-disabled="addlinhaCtn"></td>


                                                        <td>   <input class="form-control inputInlineTreinoNew" ng-model="linhaC.carga" placeholder="0" type="text" ng-disabled="addlinhaCtn"></td>
                                                        <td>  <select class="form-control inputInlineTreinoNew" ng-change="calcTempoC()" ng-model="linhaC.intervalo" ng-disabled="addlinhaCtn"  ng-show="linhaC.super_serie == 1">
                                                                <option disabled="" selected="" value="">
                                                                    0 seg
                                                                </option>
                                                                <option ng-repeat="intervalo in intervalosVetor" ng-value="intervalo.valor | number">
                                                                    @{{intervalo.texto}}
                                                                </option>
                                                            </select>
                                                        </td>

                                                        <td style="padding:0">

                                                            <!-- INICIO FECHAR E EXCLUIR -->
                                                            <span class="pull-right" style="    margin-top: -17px;">
                                                                <a class="btn btn-default " style="height: 31px; padding-top: 3px;"
                                                                   data-target="#modalObsC@{{linhaC.id}}" data-toggle="modal" ng-disabled="addlinhaCtn">
                                                                    <i aria-hidden="true" class="fa fa-comment"></i>
                                                                </a>
                                                                <a class="btn btn-danger " style="height: 31px;  padding-top: 3px; border-color: #e5e6e8;"
                                                                   ng-click="removeLinhaC($index, linhaC.ficha)" style=" border: 0;" ng-disabled="addlinhaCtn">
                                                                    <i aria-hidden="true" class="fa fa-trash"></i>
                                                                </a>

                                                            </span>
                                                            <!-- FIM FECHAR E EXCLUIR -->


                                                            <!-- OBSERVAÇÃO DO EXERCICIO TREINO A -->
                                                            <div aria-hidden="true" class="modal inmodal fade" id="modalObsC@{{linhaC.id}}" role="dialog" tabindex="-1">
                                                                <div class="modal-dialog">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <button class="close" data-dismiss="modal" type="button">
                                                                                <span aria-hidden="true">
                                                                                    ×
                                                                                </span>
                                                                                <span class="sr-only">
                                                                                    Fechar
                                                                                </span>
                                                                            </button>
                                                                            <h4 class="modal-title">
                                                                                Observação do Exercício
                                                                            </h4>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <div class="form-group">
                                                                                <input class="form-control" ng-model="linhaC.observacao" placeholder="Informe uma observação" type="text">
                                                                                </input>
                                                                            </div>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button class="btn btn-primary" data-dismiss="modal" type="button">
                                                                                OK
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- modal -->
                                                            <!-- OBSERVAÇÃO DO EXERCICIO TREINO A -->


                                                            <!-- modal -->

                                                        </td>
                                                    </tr>

                                                    <tr ng-repeat="super in linhaC.superserie" class="bi-serie">
                                                        <td id="ord_super" ng-hide="true">@{{super.ficha}}</td>
                                                        <td id="exercicio_super" ng-hide="true">@{{super.exercicio}}</td>
                                                        <td id="series_super" ng-hide="true">@{{super.series}}</td>
                                                        <td id="repeticoes_super" ng-hide="true">@{{super.repeticoes}}</td>
                                                        <td id="medida_duracao_super" ng-hide="true">@{{super.medida_duracao}}</td>
                                                        <td id="carga_super" ng-hide="true">@{{super.carga}}</td>
                                                        <td id="medida_intensidade_super" ng-hide="true">@{{super.medida_intensidade}}</td>
                                                        <td id="intervalo_super" ng-hide="true">@{{super.intervalo}}</td>   
                                                        <td id="super_serie_super" ng-hide="true">@{{super.super_serie}}</td>                                                           
                                                        <td  class="nameex">
                                                            <!-- FOTO -->
                                                            <div href="#" data-toggle="tooltip" title="Clique e arreste para mudar de posição" ng-click="rodaPreview(super.video)" 
                                                                 style="background-image:url('{{ url('/')}}/uploads/exercicios/@{{exercicios[changeExercicio(super.exercicio)].dsurlminiatura}}');" class="imgex-bi">
                                                            </div>
                                                        </td>

                                                        <td > 

                                                            <div class="form-group" style="padding-bottom: 0">

                                                                <div class="input-group" style="width: 100%;">
                                                                    <!--<div ng-repeat="super in linhaC.superserie" ng-value="exercicio.id">
                                                                        @{{super.exercicio}}
                                                                    </div>    -->

                                                                    <select class="chosen-select" id="execicioSelectC-super@{{super.id}}"  ng-change="changeExercicio(super.exercicio, linhaC)" ng-model="super.exercicio" ng-disabled="addlinhaCtn"  >
                                                                        <option disabled="" selected="" value="">

                                                                            Selecione o Exercício
                                                                        </option>
                                                                        <option ng-repeat="exercicio in exercicios" ng-value="exercicio.id">
                                                                            @{{ exercicio.nome_exercicio}}
                                                                        </option>
                                                                    </select>

                                                                    <a class="btn btn-default input-group-addon" data-target="#m_exercicios" data-toggle="modal" ng-disabled="addlinhaCtn">
                                                                        <i aria-hidden="true" class="fa fa-plus">
                                                                        </i>
                                                                    </a>

                                                                </div>
                                                            </div>
                                                        </td>

                                                        <td>  <select class="form-control inputInlineTreinoNew" ng-change="calcTempoC()" ng-model="super.series" ng-disabled="addlinhaCtn">
                                                                <option disabled="" selected="" value="">
                                                                    0x
                                                                </option>
                                                                <option ng-repeat="serie in seriesVetor" ng-value="serie.valor | number">
                                                                    @{{serie.texto}}
                                                                </option>
                                                            </select> </td>
                                                        <td>  <input class="form-control inputInlineTreinoNew" ng-blur="calcTempoC()" ng-model="super.repeticoes" placeholder="0"
                                                                     type="text" ng-disabled="addlinhaCtn"></td>


                                                        <td>   <input class="form-control inputInlineTreinoNew" ng-model="super.carga" placeholder="0" type="text" ng-disabled="addlinhaCtn"></td>
                                                        <td>  <select class="form-control inputInlineTreinoNew" ng-change="calcTempoC()" ng-model="super.intervalo" ng-disabled="addlinhaCtn"  ng-show="super.id == linhaC.superserie.length">
                                                                <option disabled="" selected="" value="">
                                                                    0 seg
                                                                </option>
                                                                <option ng-repeat="intervalo in intervalosVetor" ng-value="intervalo.valor | number">
                                                                    @{{intervalo.texto}}
                                                                </option>
                                                            </select>
                                                        </td>

                                                        <td style="padding:0">

                                                            <!-- INICIO FECHAR E EXCLUIR -->
                                                            <span class="pull-right" style="    margin-top: -17px;">
                                                                <a class="btn btn-default " style="height: 31px; padding-top: 3px;"
                                                                   data-target="#modalObsSUPERC@{{super.id}}" data-toggle="modal" ng-disabled="addLinhaBtn">
                                                                    <i aria-hidden="true" class="fa fa-comment"></i>
                                                                </a>
                                                                <a class="btn btn-danger " style="height: 31px;  padding-top: 3px; border-color: #e5e6e8;"
                                                                   ng-click="removeLinhaSuper($index, super, linhaC.superserie, linhaC.ficha)" style=" border: 0;" ng-disabled="addLinhaBtn">
                                                                    <i aria-hidden="true" class="fa fa-trash"></i>
                                                                </a>

                                                            </span>
                                                            <!-- FIM FECHAR E EXCLUIR -->

                                                            <div aria-hidden="true" class="modal inmodal fade" id="modalObsSUPERC@{{super.id}}" role="dialog" tabindex="-1">
                                                                <div class="modal-dialog">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <button class="close" data-dismiss="modal" type="button">
                                                                                <span aria-hidden="true">
                                                                                    ×
                                                                                </span>
                                                                                <span class="sr-only">
                                                                                    Fechar
                                                                                </span>
                                                                            </button>
                                                                            <h4 class="modal-title">
                                                                                Observação do Exercício
                                                                            </h4>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <div class="form-group">
                                                                                <input class="form-control" ng-model="super.observacao" placeholder="Informe uma observação" type="text">
                                                                                </input>
                                                                            </div>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button class="btn btn-primary" data-dismiss="modal" type="button">
                                                                                OK
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>                                                            
                                                            <!-- modal -->

                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            {{-- FIM DA TABELA NOVA  --}}

                                        </div>

                                    </div>
                                </td>
                            </tr>

                        </tbody>
                    </table>
                    <!-- fim tabele -->

                </div>
                <div class="form-group" style="margin-top:10px;">
                    <span style="text-align:left;margin-left:-15px;">
                        <a class="btn btn-primary" ng-click="addLinhaC()" ng-disabled="addlinhaCtn">
                            <i aria-hidden="true" class="fa fa-plus">
                            </i>
                        </a>
                    </span>
                    <span class="pull-right" style="margin-right:-15px;text-align:right;">
                        <b>
                            Tempo estimado @{{tempoestimadoA}}
                        </b>
                        <br> Musculação: @{{tempomusculacaoA}} + Aquecimento: 10 min + Alongamento: 4 min
                        </br>
                    </span>
                </div>
                <div class="clearfix">
                </div>
            </div>
        </div>
        <!-- fim table panel 3 (C) -->


        <!-- INICIO DA ABA panel 4 (D)  -->
        <div class="tab-pane" id="tab-4" ng-show="tabsStatus.tab4">

            <div class="panel-body" style="padding:7px 20px;">
                <div class="row clearfix ScrrolAba" style="margin-top: 6px;  ">

                    <table class="table table-bordered pagin-table" id="abaDdd">
                        <tbody ng-model="linhaD">

                            <tr class="ordena-treino" ng-model="tabs" ng-repeat="linhaD in linhasD" style="background:#f5f5f5;" ng-disabled="addlinhaDtn">
                                <td id="ord" ng-hide="true">@{{linhaD.ficha}}</td>
                                <td id="exercicio" ng-hide="true">@{{linhaD.exercicio}}</td>
                                <td id="series" ng-hide="true">@{{linhaD.series}}</td>
                                <td id="repeticoes" ng-hide="true">@{{linhaD.repeticoes}}</td>
                                <td id="medida_duracao" ng-hide="true">@{{linhaD.medida_duracao}}</td>
                                <td id="carga" ng-hide="true">@{{linhaD.carga}}</td>
                                <td id="medida_intensidade" ng-hide="true">@{{linhaD.medida_intensidade}}</td>
                                <td id="intervalo" ng-hide="true">@{{linhaD.intervalo}}</td>   
                                <td id="super_serie" ng-hide="true">@{{linhaD.super_serie}}</td>   
                                <td id="hash_code" ng-hide="true">@{{linhaD.hash_code}}</td>   
                                <td id="observacao" ng-hide="true">@{{linhaD.observacao}}</td>   
                                
                                <td style="padding: 0 ">
                                    <!-- EXIBE O NÚMERO de linhas -->
                                    <span class="marcador_exer">
                                        @{{linhaD.id}}
                                    </span>
                                    <!-- INICIO DA LINHA DO EXERCICIO -->
                                    <div class="row" style="margin-right: -6px;">
                                        <!-- {{-- <div class="col-xs-12 col-sm-1 hidden-xs" style="padding:0;     padding-right: 8px;">
                                         </div> --}}-->
                                        <div class="col-xs-12 col-sm-12"  >

                                            {{-- INICIO DA TABELA NOVA  --}}

                                            <table class="table_treino">
                                                <thead>
                                                    <tr>
                                                        <th style="width: 9%;">


                                                        </th>
                                                        <th style="width: 44%;">    
                                                            <select class="form-control selectTreinoNew" 
                                                                    ng-change="changeGenericoTesteA(linhaD.super_serie, linhaD)" ng-model="linhaD.super_serie" 
                                                                    required="required">
                                                                <option ng-repeat="super_serie in super_serieVetor" 
                                                                        ng-value="super_serie.valor">
                                                                    @{{super_serie.texto}}
                                                                </option>                                                              
                                                            </select>  
                                                        </th>
                                                        <th style="width: 8%;" class="nobold">Série</th>
                                                        <th style="width: 9.5%;"> 
                                                            <select class="form-control selectTreinoTransparent" ng-model="linhaD.medida_duracao" ng-disabled="addlinhaDtn" required="required">
                                                                <option ng-repeat="medida_duracao in medida_duracaosVetor" ng-value="medida_duracao.valor">
                                                                    @{{medida_duracao.texto}}
                                                                </option>                                                                
                                                            </select>  
                                                        </th>
                                                        <th style="width: 9.5%;"> 
                                                            <select class="form-control selectTreinoTransparent" 
                                                                    ng-model="linhaD.medida_intensidade"
                                                                    ng-disabled="addlinhaDtn" ng-change="changeGenericoTeste(linhaD.medida_intensidade)"
                                                                    required="required">
                                                                <option ng-repeat="medida_intensidade in medida_intensidadeVetor" 
                                                                        ng-value="medida_intensidade.valor">
                                                                    @{{medida_intensidade.texto}}
                                                                </option>                                                                       
                                                            </select>  
                                                        </th>
                                                        <th style="width: 8.75%;" class="nobold">Intervalo</th>

                                                        <th style="width: 94px">

                                                        </th>

                                                    </tr>
                                                </thead>

                                                <tbody>
                                                    {{-- LINHA Exercicio Simples --}}
                                                    <tr class="exercicio-simples">
                                                        <td style="padding:0"> 
                                                            <!-- FOTO -->
                                                            <div href="#" data-toggle="tooltip" title="Clique e arreste para mudar de posição" ng-click="rodaPreview(linhaD.video)" 
                                                                 style="background-image:url('{{ url('/')}}/uploads/exercicios/@{{exercicios[changeExercicio(linhaD.exercicio)].dsurlminiatura}}');" class="imgex">
                                                            </div>
                                                        </td>
                                                        <td> 
                                                            <div class="form-group" style="padding-bottom: 0">
                                                                <div class="input-group" style="width: 100%;">

                                                                    <select class="chosen-select" id="execicioSelectD@{{linhaD.id}}"  ng-change="changeExercicio(linhaD.exercicio, linhaD)" ng-model="linhaD.exercicio" ng-disabled="addlinhaDtn"  >
                                                                        <option disabled="" selected="" value="">
                                                                            Selecione o Exercício
                                                                        </option>
                                                                        <option ng-repeat="exercicio in exercicios" ng-value="exercicio.id">
                                                                            @{{exercicio.nome_exercicio}}
                                                                        </option>
                                                                    </select>

                                                                    <a class="btn btn-default input-group-addon" data-target="#m_exercicios" data-toggle="modal" ng-disabled="addlinhaDtn">
                                                                        <i aria-hidden="true" class="fa fa-plus">
                                                                        </i>
                                                                    </a>

                                                                </div>
                                                            </div>
                                                        </td>

                                                        <td>  <select class="form-control inputInlineTreinoNew" ng-change="calcTempoD()" ng-model="linhaD.series" ng-disabled="addlinhaDtn">
                                                                <option disabled="" selected="" value="">
                                                                    0x
                                                                </option>
                                                                <option ng-repeat="serie in seriesVetor" ng-value="serie.valor | number">
                                                                    @{{serie.texto}}
                                                                </option>
                                                            </select> </td>
                                                        <td>  <input class="form-control inputInlineTreinoNew" ng-blur="calcTempoD()" ng-model="linhaD.repeticoes" placeholder="0"
                                                                     type="text" ng-disabled="addlinhaDtn"></td>


                                                        <td>   <input class="form-control inputInlineTreinoNew" ng-model="linhaD.carga" placeholder="0" type="text" ng-disabled="addlinhaDtn"></td>
                                                        <td>  <select class="form-control inputInlineTreinoNew" ng-change="calcTempoD()" ng-model="linhaD.intervalo" ng-disabled="addlinhaDtn" ng-show="linhaD.super_serie == 1">
                                                                <option disabled="" selected="" value="">
                                                                    0 seg
                                                                </option>
                                                                <option ng-repeat="intervalo in intervalosVetor" ng-value="intervalo.valor | number">
                                                                    @{{intervalo.texto}}
                                                                </option>
                                                            </select>
                                                        </td>

                                                        <td style="padding:0">

                                                            <!-- INICIO FECHAR E EXCLUIR -->
                                                            <span class="pull-right" style="    margin-top: -17px;">
                                                                <a class="btn btn-default " style="height: 31px; padding-top: 3px;"
                                                                   data-target="#modalObsD@{{linhaD.id}}" data-toggle="modal" ng-disabled="addlinhaDtn">
                                                                    <i aria-hidden="true" class="fa fa-comment"></i>
                                                                </a>
                                                                <a class="btn btn-danger " style="height: 31px;  padding-top: 3px; border-color: #e5e6e8;"
                                                                   ng-click="removeLinhaD($index, linhaD.ficha)" style=" border: 0;" ng-disabled="addlinhaDtn">
                                                                    <i aria-hidden="true" class="fa fa-trash"></i>
                                                                </a>

                                                            </span>
                                                            <!-- FIM FECHAR E EXCLUIR -->


                                                            <!-- OBSERVAÇÃO DO EXERCICIO TREINO A -->
                                                            <div aria-hidden="true" class="modal inmodal fade" id="modalObsD@{{linhaD.id}}" role="dialog" tabindex="-1">
                                                                <div class="modal-dialog">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <button class="close" data-dismiss="modal" type="button">
                                                                                <span aria-hidden="true">
                                                                                    ×
                                                                                </span>
                                                                                <span class="sr-only">
                                                                                    Fechar
                                                                                </span>
                                                                            </button>
                                                                            <h4 class="modal-title">
                                                                                Observação do Exercício
                                                                            </h4>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <div class="form-group">
                                                                                <input class="form-control" ng-model="linhaD.observacao" placeholder="Informe uma observação" type="text">
                                                                                </input>
                                                                            </div>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button class="btn btn-primary" data-dismiss="modal" type="button">
                                                                                OK
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- modal -->
                                                            <!-- OBSERVAÇÃO DO EXERCICIO TREINO A -->


                                                            <!-- modal -->

                                                        </td>
                                                    </tr>

                                                    <tr ng-repeat="super in linhaD.superserie" class="bi-serie">
                                                        <td id="ord_super" ng-hide="true">@{{super.ficha}}</td>
                                                        <td id="exercicio_super" ng-hide="true">@{{super.exercicio}}</td>
                                                        <td id="series_super" ng-hide="true">@{{super.series}}</td>
                                                        <td id="repeticoes_super" ng-hide="true">@{{super.repeticoes}}</td>
                                                        <td id="medida_duracao_super" ng-hide="true">@{{super.medida_duracao}}</td>
                                                        <td id="carga_super" ng-hide="true">@{{super.carga}}</td>
                                                        <td id="medida_intensidade_super" ng-hide="true">@{{super.medida_intensidade}}</td>
                                                        <td id="intervalo_super" ng-hide="true">@{{super.intervalo}}</td>   
                                                        <td id="super_serie_super" ng-hide="true">@{{super.super_serie}}</td>                                                           
                                                        <td  class="nameex">
                                                            <!-- FOTO -->
                                                            <div href="#" data-toggle="tooltip" title="Clique e arreste para mudar de posição" ng-click="rodaPreview(super.video)" 
                                                                 style="background-image:url('{{ url('/')}}/uploads/exercicios/@{{exercicios[changeExercicio(super.exercicio)].dsurlminiatura}}');" class="imgex-bi">
                                                            </div>
                                                        </td>

                                                        <td > 

                                                            <div class="form-group" style="padding-bottom: 0">

                                                                <div class="input-group" style="width: 100%;">
                                                                    <!--<div ng-repeat="super in linhaD.superserie" ng-value="exercicio.id">
                                                                        @{{super.exercicio}}
                                                                    </div>    -->

                                                                    <select class="chosen-select" id="execicioSelectD-super@{{super.id}}"  ng-change="changeExercicio(super.exercicio, linhaD)" ng-model="super.exercicio" ng-disabled="addlinhaDtn"  >
                                                                        <option disabled="" selected="" value="">

                                                                            Selecione o Exercício
                                                                        </option>
                                                                        <option ng-repeat="exercicio in exercicios" ng-value="exercicio.id">
                                                                            @{{ exercicio.nome_exercicio}}
                                                                        </option>
                                                                    </select>

                                                                    <a class="btn btn-default input-group-addon" data-target="#m_exercicios" data-toggle="modal" ng-disabled="addlinhaDtn">
                                                                        <i aria-hidden="true" class="fa fa-plus">
                                                                        </i>
                                                                    </a>

                                                                </div>
                                                            </div>
                                                        </td>

                                                        <td>  <select class="form-control inputInlineTreinoNew" ng-change="calcTempoD()" ng-model="super.series" ng-disabled="addlinhaDtn">
                                                                <option disabled="" selected="" value="">
                                                                    0x
                                                                </option>
                                                                <option ng-repeat="serie in seriesVetor" ng-value="serie.valor | number">
                                                                    @{{serie.texto}}
                                                                </option>
                                                            </select> </td>
                                                        <td>  <input class="form-control inputInlineTreinoNew" ng-blur="calcTempoD()" ng-model="super.repeticoes" placeholder="0"
                                                                     type="text" ng-disabled="addlinhaDtn"></td>


                                                        <td>   <input class="form-control inputInlineTreinoNew" ng-model="super.carga" placeholder="0" type="text" ng-disabled="addlinhaDtn"></td>
                                                        <td>  <select class="form-control inputInlineTreinoNew" ng-change="calcTempoD()" ng-model="super.intervalo" ng-disabled="addlinhaDtn"  ng-show="super.id == linhaD.superserie.length">
                                                                <option disabled="" selected="" value="">
                                                                    0 seg
                                                                </option>
                                                                <option ng-repeat="intervalo in intervalosVetor" ng-value="intervalo.valor | number">
                                                                    @{{intervalo.texto}}
                                                                </option>
                                                            </select>
                                                        </td>

                                                        <td style="padding:0">

                                                            <!-- INICIO FECHAR E EXCLUIR -->
                                                            <span class="pull-right" style="    margin-top: -17px;">
                                                                <a class="btn btn-default " style="height: 31px; padding-top: 3px;"
                                                                   data-target="#modalObsSUPERD@{{super.id}}" data-toggle="modal" ng-disabled="addLinhaBtn">
                                                                    <i aria-hidden="true" class="fa fa-comment"></i>
                                                                </a>
                                                                <a class="btn btn-danger " style="height: 31px;  padding-top: 3px; border-color: #e5e6e8;"
                                                                   ng-click="removeLinhaSuper($index, super, linhaD.superserie, linhaD.ficha)" style=" border: 0;" ng-disabled="addLinhaBtn">
                                                                    <i aria-hidden="true" class="fa fa-trash"></i>
                                                                </a>

                                                            </span>
                                                            <!-- FIM FECHAR E EXCLUIR -->

                                                            <div aria-hidden="true" class="modal inmodal fade" id="modalObsSUPERD@{{super.id}}" role="dialog" tabindex="-1">
                                                                <div class="modal-dialog">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <button class="close" data-dismiss="modal" type="button">
                                                                                <span aria-hidden="true">
                                                                                    ×
                                                                                </span>
                                                                                <span class="sr-only">
                                                                                    Fechar
                                                                                </span>
                                                                            </button>
                                                                            <h4 class="modal-title">
                                                                                Observação do Exercício
                                                                            </h4>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <div class="form-group">
                                                                                <input class="form-control" ng-model="super.observacao" placeholder="Informe uma observação" type="text">
                                                                                </input>
                                                                            </div>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button class="btn btn-primary" data-dismiss="modal" type="button">
                                                                                OK
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>                                                            
                                                            <!-- modal -->

                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            {{-- FIM DA TABELA NOVA  --}}

                                        </div>

                                    </div>
                                </td>
                            </tr>

                        </tbody>
                    </table>
                    <!-- fim tabele -->

                </div>
                <div class="form-group" style="margin-top:10px;">
                    <span style="text-align:left;margin-left:-15px;">
                        <a class="btn btn-primary" ng-click="addLinhaD()" ng-disabled="addlinhaDtn">
                            <i aria-hidden="true" class="fa fa-plus">
                            </i>
                        </a>
                    </span>
                    <span class="pull-right" style="margin-right:-15px;text-align:right;">
                        <b>
                            Tempo estimado @{{tempoestimadoA}}
                        </b>
                        <br> Musculação: @{{tempomusculacaoA}} + Aquecimento: 10 min + Alongamento: 4 min
                        </br>
                    </span>
                </div>
                <div class="clearfix">
                </div>
            </div>
        </div>
        <!-- fim table panel 4 (D) -->

        <!-- INICIO DA ABA panel 5 (E)  -->
       <div class="tab-pane" id="tab-5" ng-show="tabsStatus.tab5">

            <div class="panel-body" style="padding:7px 20px;">
                <div class="row clearfix ScrrolAba" style="margin-top: 6px;  ">

                    <table class="table table-bordered pagin-table" id="abaEee">
                        <tbody ng-model="linhaE">

                            <tr class="ordena-treino" ng-model="tabs" ng-repeat="linhaE in linhasE" style="background:#f5f5f5;" ng-disabled="addlinhaEtn">
                                <td id="ord" ng-hide="true">@{{linhaE.ficha}}</td>
                                <td id="exercicio" ng-hide="true">@{{linhaE.exercicio}}</td>
                                <td id="series" ng-hide="true">@{{linhaE.series}}</td>
                                <td id="repeticoes" ng-hide="true">@{{linhaE.repeticoes}}</td>
                                <td id="medida_duracao" ng-hide="true">@{{linhaE.medida_duracao}}</td>
                                <td id="carga" ng-hide="true">@{{linhaE.carga}}</td>
                                <td id="medida_intensidade" ng-hide="true">@{{linhaE.medida_intensidade}}</td>
                                <td id="intervalo" ng-hide="true">@{{linhaE.intervalo}}</td>   
                                <td id="super_serie" ng-hide="true">@{{linhaE.super_serie}}</td>   
                                <td id="hash_code" ng-hide="true">@{{linhaE.hash_code}}</td>   
                                <td id="observacao" ng-hide="true">@{{linhaE.observacao}}</td>   
                                
                                <td style="padding: 0 ">
                                    <!-- EXIBE O NÚMERO de linhas -->
                                    <span class="marcador_exer">
                                        @{{linhaE.id}}
                                    </span>
                                    <!-- INICIO DA LINHA DO EXERCICIO -->
                                    <div class="row" style="margin-right: -6px;">
                                        <!-- {{-- <div class="col-xs-12 col-sm-1 hidden-xs" style="padding:0;     padding-right: 8px;">
                                         </div> --}}-->
                                        <div class="col-xs-12 col-sm-12"  >

                                            {{-- INICIO DA TABELA NOVA  --}}

                                            <table class="table_treino">
                                                <thead>
                                                    <tr>
                                                        <th style="width: 9%;">


                                                        </th>
                                                        <th style="width: 44%;">    
                                                            <select class="form-control selectTreinoNew" 
                                                                    ng-change="changeGenericoTesteA(linhaE.super_serie, linhaE)" ng-model="linhaE.super_serie" 
                                                                    required="required">
                                                                <option ng-repeat="super_serie in super_serieVetor" 
                                                                        ng-value="super_serie.valor">
                                                                    @{{super_serie.texto}}
                                                                </option>                                                              
                                                            </select>  
                                                        </th>
                                                        <th style="width: 8%;" class="nobold">Série</th>
                                                        <th style="width: 9.5%;"> 
                                                            <select class="form-control selectTreinoTransparent" ng-model="linhaE.medida_duracao" ng-disabled="addlinhaEtn" required="required">
                                                                <option ng-repeat="medida_duracao in medida_duracaosVetor" ng-value="medida_duracao.valor">
                                                                    @{{medida_duracao.texto}}
                                                                </option>                                                                
                                                            </select>  
                                                        </th>
                                                        <th style="width: 9.5%;"> 
                                                            <select class="form-control selectTreinoTransparent" 
                                                                    ng-model="linhaE.medida_intensidade"
                                                                    ng-disabled="addlinhaEtn" ng-change="changeGenericoTeste(linhaE.medida_intensidade)"
                                                                    required="required">
                                                                <option ng-repeat="medida_intensidade in medida_intensidadeVetor" 
                                                                        ng-value="medida_intensidade.valor">
                                                                    @{{medida_intensidade.texto}}
                                                                </option>                                                                       
                                                            </select>  
                                                        </th>
                                                        <th style="width: 8.75%;" class="nobold">Intervalo</th>

                                                        <th style="width: 94px">

                                                        </th>

                                                    </tr>
                                                </thead>

                                                <tbody>
                                                    {{-- LINHA Exercicio Simples --}}
                                                    <tr class="exercicio-simples">
                                                        <td style="padding:0"> 
                                                            <!-- FOTO -->
                                                            <div href="#" data-toggle="tooltip" title="Clique e arreste para mudar de posição" ng-click="rodaPreview(linhaE.video)"
                                                                 style="background-image:url('{{ url('/')}}/uploads/exercicios/@{{exercicios[changeExercicio(linhaE.exercicio)].dsurlminiatura}}');" class="imgex">
                                                            </div>
                                                        </td>
                                                        <td> 
                                                            <div class="form-group" style="padding-bottom: 0">
                                                                <div class="input-group" style="width: 100%;">

                                                                    <select class="chosen-select" id="execicioSelectE@{{linhaE.id}}"  ng-change="changeExercicio(linhaE.exercicio, linhaE)" ng-model="linhaE.exercicio" ng-disabled="addlinhaEtn"  >
                                                                        <option disabled="" selected="" value="">
                                                                            Selecione o Exercício
                                                                        </option>
                                                                        <option ng-repeat="exercicio in exercicios" ng-value="exercicio.id">
                                                                            @{{exercicio.nome_exercicio}}
                                                                        </option>
                                                                    </select>

                                                                    <a class="btn btn-default input-group-addon" data-target="#m_exercicios" data-toggle="modal" ng-disabled="addlinhaEtn">
                                                                        <i aria-hidden="true" class="fa fa-plus">
                                                                        </i>
                                                                    </a>

                                                                </div>
                                                            </div>
                                                        </td>

                                                        <td>  <select class="form-control inputInlineTreinoNew" ng-change="calcTempoE()" ng-model="linhaE.series" ng-disabled="addlinhaEtn">
                                                                <option disabled="" selected="" value="">
                                                                    0x
                                                                </option>
                                                                <option ng-repeat="serie in seriesVetor" ng-value="serie.valor | number">
                                                                    @{{serie.texto}}
                                                                </option>
                                                            </select> </td>
                                                        <td>  <input class="form-control inputInlineTreinoNew" ng-blur="calcTempoE()" ng-model="linhaE.repeticoes" placeholder="0"
                                                                     type="text" ng-disabled="addlinhaEtn"></td>


                                                        <td>   <input class="form-control inputInlineTreinoNew" ng-model="linhaE.carga" placeholder="0" type="text" ng-disabled="addlinhaEtn"></td>
                                                        <td>  <select class="form-control inputInlineTreinoNew" ng-change="calcTempoE()" ng-model="linhaE.intervalo" ng-disabled="addlinhaEtn"  ng-show="linhaE.super_serie == 1">
                                                                <option disabled="" selected="" value="">
                                                                    0 seg
                                                                </option>
                                                                <option ng-repeat="intervalo in intervalosVetor" ng-value="intervalo.valor | number">
                                                                    @{{intervalo.texto}}
                                                                </option>
                                                            </select>
                                                        </td>

                                                        <td style="padding:0">

                                                            <!-- INICIO FECHAR E EXCLUIR -->
                                                            <span class="pull-right" style="    margin-top: -17px;">
                                                                <a class="btn btn-default " style="height: 31px; padding-top: 3px;"
                                                                   data-target="#modalObsE@{{linhaE.id}}" data-toggle="modal" ng-disabled="addlinhaEtn">
                                                                    <i aria-hidden="true" class="fa fa-comment"></i>
                                                                </a>
                                                                <a class="btn btn-danger " style="height: 31px;  padding-top: 3px; border-color: #e5e6e8;"
                                                                   ng-click="removeLinhaE($index, linhaE.ficha)" style=" border: 0;" ng-disabled="addlinhaEtn">
                                                                    <i aria-hidden="true" class="fa fa-trash"></i>
                                                                </a>

                                                            </span>
                                                            <!-- FIM FECHAR E EXCLUIR -->


                                                            <!-- OBSERVAÇÃO DO EXERCICIO TREINO A -->
                                                            <div aria-hidden="true" class="modal inmodal fade" id="modalObsE@{{linhaE.id}}" role="dialog" tabindex="-1">
                                                                <div class="modal-dialog">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <button class="close" data-dismiss="modal" type="button">
                                                                                <span aria-hidden="true">
                                                                                    ×
                                                                                </span>
                                                                                <span class="sr-only">
                                                                                    Fechar
                                                                                </span>
                                                                            </button>
                                                                            <h4 class="modal-title">
                                                                                Observação do Exercício
                                                                            </h4>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <div class="form-group">
                                                                                <input class="form-control" ng-model="linhaE.observacao" placeholder="Informe uma observação" type="text">
                                                                                </input>
                                                                            </div>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button class="btn btn-primary" data-dismiss="modal" type="button">
                                                                                OK
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- modal -->
                                                            <!-- OBSERVAÇÃO DO EXERCICIO TREINO A -->


                                                            <!-- modal -->

                                                        </td>
                                                    </tr>

                                                    <tr ng-repeat="super in linhaE.superserie" class="bi-serie">
                                                        <td id="ord_super" ng-hide="true">@{{super.ficha}}</td>
                                                        <td id="exercicio_super" ng-hide="true">@{{super.exercicio}}</td>
                                                        <td id="series_super" ng-hide="true">@{{super.series}}</td>
                                                        <td id="repeticoes_super" ng-hide="true">@{{super.repeticoes}}</td>
                                                        <td id="medida_duracao_super" ng-hide="true">@{{super.medida_duracao}}</td>
                                                        <td id="carga_super" ng-hide="true">@{{super.carga}}</td>
                                                        <td id="medida_intensidade_super" ng-hide="true">@{{super.medida_intensidade}}</td>
                                                        <td id="intervalo_super" ng-hide="true">@{{super.intervalo}}</td>   
                                                        <td id="super_serie_super" ng-hide="true">@{{super.super_serie}}</td>                                                           
                                                        <td  class="nameex">
                                                            <!-- FOTO -->
                                                            <div href="#" data-toggle="tooltip" title="Clique e arreste para mudar de posição" ng-click="rodaPreview(super.video)" 
                                                                 style="background-image:url('{{ url('/')}}/uploads/exercicios/@{{exercicios[changeExercicio(super.exercicio)].dsurlminiatura}}');" class="imgex-bi">
                                                            </div>
                                                        </td>

                                                        <td > 

                                                            <div class="form-group" style="padding-bottom: 0">

                                                                <div class="input-group" style="width: 100%;">
                                                                    <!--<div ng-repeat="super in linhaE.superserie" ng-value="exercicio.id">
                                                                        @{{super.exercicio}}
                                                                    </div>    -->

                                                                    <select class="chosen-select" id="execicioSelectE-super@{{super.id}}"  ng-change="changeExercicio(super.exercicio, linhaE)" ng-model="super.exercicio" ng-disabled="addlinhaEtn"  >
                                                                        <option disabled="" selected="" value="">

                                                                            Selecione o Exercício
                                                                        </option>
                                                                        <option ng-repeat="exercicio in exercicios" ng-value="exercicio.id">
                                                                            @{{ exercicio.nome_exercicio}}
                                                                        </option>
                                                                    </select>

                                                                    <a class="btn btn-default input-group-addon" data-target="#m_exercicios" data-toggle="modal" ng-disabled="addlinhaEtn">
                                                                        <i aria-hidden="true" class="fa fa-plus">
                                                                        </i>
                                                                    </a>

                                                                </div>
                                                            </div>
                                                        </td>

                                                        <td>  <select class="form-control inputInlineTreinoNew" ng-change="calcTempoE()" ng-model="super.series" ng-disabled="addlinhaEtn">
                                                                <option disabled="" selected="" value="">
                                                                    0x
                                                                </option>
                                                                <option ng-repeat="serie in seriesVetor" ng-value="serie.valor | number">
                                                                    @{{serie.texto}}
                                                                </option>
                                                            </select> </td>
                                                        <td>  <input class="form-control inputInlineTreinoNew" ng-blur="calcTempoE()" ng-model="super.repeticoes" placeholder="0"
                                                                     type="text" ng-disabled="addlinhaEtn"></td>


                                                        <td>   <input class="form-control inputInlineTreinoNew" ng-model="super.carga" placeholder="0" type="text" ng-disabled="addlinhaEtn"></td>
                                                        <td>  <select class="form-control inputInlineTreinoNew" ng-change="calcTempoE()" ng-model="super.intervalo" ng-disabled="addlinhaEtn"  ng-show="super.id == linhaE.superserie.length">
                                                                <option disabled="" selected="" value="">
                                                                    0 seg
                                                                </option>
                                                                <option ng-repeat="intervalo in intervalosVetor" ng-value="intervalo.valor | number">
                                                                    @{{intervalo.texto}}
                                                                </option>
                                                            </select>
                                                        </td>

                                                        <td style="padding:0">

                                                            <!-- INICIO FECHAR E EXCLUIR -->
                                                            <span class="pull-right" style="    margin-top: -17px;">
                                                                <a class="btn btn-default " style="height: 31px; padding-top: 3px;"
                                                                   data-target="#modalObsSUPERE@{{super.id}}" data-toggle="modal" ng-disabled="addLinhaBtn">
                                                                    <i aria-hidden="true" class="fa fa-comment"></i>
                                                                </a>
                                                                <a class="btn btn-danger " style="height: 31px;  padding-top: 3px; border-color: #e5e6e8;"
                                                                   ng-click="removeLinhaSuper($index, super, linhaE.superserie, linhaE.ficha)" style=" border: 0;" ng-disabled="addLinhaBtn">
                                                                    <i aria-hidden="true" class="fa fa-trash"></i>
                                                                </a>

                                                            </span>
                                                            <!-- FIM FECHAR E EXCLUIR -->

                                                            <div aria-hidden="true" class="modal inmodal fade" id="modalObsSUPERE@{{super.id}}" role="dialog" tabindex="-1">
                                                                <div class="modal-dialog">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <button class="close" data-dismiss="modal" type="button">
                                                                                <span aria-hidden="true">
                                                                                    ×
                                                                                </span>
                                                                                <span class="sr-only">
                                                                                    Fechar
                                                                                </span>
                                                                            </button>
                                                                            <h4 class="modal-title">
                                                                                Observação do Exercício
                                                                            </h4>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <div class="form-group">
                                                                                <input class="form-control" ng-model="super.observacao" placeholder="Informe uma observação" type="text">
                                                                                </input>
                                                                            </div>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button class="btn btn-primary" data-dismiss="modal" type="button">
                                                                                OK
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>                                                            
                                                            <!-- modal -->

                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            {{-- FIM DA TABELA NOVA  --}}

                                        </div>

                                    </div>
                                </td>
                            </tr>

                        </tbody>
                    </table>
                    <!-- fim tabele -->

                </div>
                <div class="form-group" style="margin-top:10px;">
                    <span style="text-align:left;margin-left:-15px;">
                        <a class="btn btn-primary" ng-click="addLinhaE()" ng-disabled="addlinhaEtn">
                            <i aria-hidden="true" class="fa fa-plus">
                            </i>
                        </a>
                    </span>
                    <span class="pull-right" style="margin-right:-15px;text-align:right;">
                        <b>
                            Tempo estimado @{{tempoestimadoA}}
                        </b>
                        <br> Musculação: @{{tempomusculacaoA}} + Aquecimento: 10 min + Alongamento: 4 min
                        </br>
                    </span>
                </div>
                <div class="clearfix">
                </div>
            </div>
        </div>
        <!-- fim table panel 5 (E) -->

        <!-- INICIO DA ABA panel 6 (F)  -->
       <div class="tab-pane" id="tab-6" ng-show="tabsStatus.tab6">

            <div class="panel-body" style="padding:7px 20px;">
                <div class="row clearfix ScrrolAba" style="margin-top: 6px;  ">

                    <table class="table table-bordered pagin-table" id="abaFff">
                        <tbody ng-model="linhaF">

                            <tr class="ordena-treino" ng-model="tabs" ng-repeat="linhaF in linhasF" style="background:#f5f5f5;" ng-disabled="addlinhaFtn">
                                <td id="ord" ng-hide="true">@{{linhaF.ficha}}</td>
                                <td id="exercicio" ng-hide="true">@{{linhaF.exercicio}}</td>
                                <td id="series" ng-hide="true">@{{linhaF.series}}</td>
                                <td id="repeticoes" ng-hide="true">@{{linhaF.repeticoes}}</td>
                                <td id="medida_duracao" ng-hide="true">@{{linhaF.medida_duracao}}</td>
                                <td id="carga" ng-hide="true">@{{linhaF.carga}}</td>
                                <td id="medida_intensidade" ng-hide="true">@{{linhaF.medida_intensidade}}</td>
                                <td id="intervalo" ng-hide="true">@{{linhaF.intervalo}}</td>   
                                <td id="super_serie" ng-hide="true">@{{linhaF.super_serie}}</td>   
                                <td id="hash_code" ng-hide="true">@{{linhaF.hash_code}}</td>   
                                <td id="observacao" ng-hide="true">@{{linhaF.observacao}}</td>   
                                
                                <td style="padding: 0 ">
                                    <!-- EXIBE O NÚMERO de linhas -->
                                    <span class="marcador_exer">
                                        @{{linhaF.id}}
                                    </span>
                                    <!-- INICIO DA LINHA DO EXERCICIO -->
                                    <div class="row" style="margin-right: -6px;">
                                        <!-- {{-- <div class="col-xs-12 col-sm-1 hidden-xs" style="padding:0;     padding-right: 8px;">
                                         </div> --}}-->
                                        <div class="col-xs-12 col-sm-12"  >

                                            {{-- INICIO DA TABELA NOVA  --}}

                                            <table class="table_treino">
                                                <thead>
                                                    <tr>
                                                        <th style="width: 9%;">


                                                        </th>
                                                        <th style="width: 44%;">    
                                                            <select class="form-control selectTreinoNew" 
                                                                    ng-change="changeGenericoTesteA(linhaF.super_serie, linhaF)" ng-model="linhaF.super_serie" 
                                                                    required="required">
                                                                <option ng-repeat="super_serie in super_serieVetor" 
                                                                        ng-value="super_serie.valor">
                                                                    @{{super_serie.texto}}
                                                                </option>                                                              
                                                            </select>  
                                                        </th>
                                                        <th style="width: 8%;" class="nobold">Série</th>
                                                        <th style="width: 9.5%;"> 
                                                            <select class="form-control selectTreinoTransparent" ng-model="linhaF.medida_duracao" ng-disabled="addlinhaFtn" required="required">
                                                                <option ng-repeat="medida_duracao in medida_duracaosVetor" ng-value="medida_duracao.valor">
                                                                    @{{medida_duracao.texto}}
                                                                </option>                                                                
                                                            </select>  
                                                        </th>
                                                        <th style="width: 9.5%;"> 
                                                            <select class="form-control selectTreinoTransparent" 
                                                                    ng-model="linhaF.medida_intensidade"
                                                                    ng-disabled="addlinhaFtn" ng-change="changeGenericoTeste(linhaF.medida_intensidade)"
                                                                    required="required">
                                                                <option ng-repeat="medida_intensidade in medida_intensidadeVetor" 
                                                                        ng-value="medida_intensidade.valor">
                                                                    @{{medida_intensidade.texto}}
                                                                </option>                                                                       
                                                            </select>  
                                                        </th>
                                                        <th style="width: 8.75%;" class="nobold">Intervalo</th>

                                                        <th style="width: 94px">

                                                        </th>

                                                    </tr>
                                                </thead>

                                                <tbody>
                                                    {{-- LINHA Exercicio Simples --}}
                                                    <tr class="exercicio-simples">
                                                        <td style="padding:0"> 
                                                            <!-- FOTO -->
                                                            <div href="#" data-toggle="tooltip" title="Clique e arreste para mudar de posição" ng-click="rodaPreview(linhaF.video)" 
                                                                 style="background-image:url('{{ url('/')}}/uploads/exercicios/@{{exercicios[changeExercicio(linhaF.exercicio)].dsurlminiatura}}');" class="imgex">
                                                            </div>
                                                        </td>
                                                        <td> 
                                                            <div class="form-group" style="padding-bottom: 0">
                                                                <div class="input-group" style="width: 100%;">

                                                                    <select class="chosen-select" id="execicioSelectF@{{linhaF.id}}"  ng-change="changeExercicio(linhaF.exercicio, linhaF)" ng-model="linhaF.exercicio" ng-disabled="addlinhaFtn"  >
                                                                        <option disabled="" selected="" value="">
                                                                            Selecione o Exercício
                                                                        </option>
                                                                        <option ng-repeat="exercicio in exercicios" ng-value="exercicio.id">
                                                                            @{{exercicio.nome_exercicio}}
                                                                        </option>
                                                                    </select>

                                                                    <a class="btn btn-default input-group-addon" data-target="#m_exercicios" data-toggle="modal" ng-disabled="addlinhaFtn">
                                                                        <i aria-hidden="true" class="fa fa-plus">
                                                                        </i>
                                                                    </a>

                                                                </div>
                                                            </div>
                                                        </td>

                                                        <td>  <select class="form-control inputInlineTreinoNew" ng-change="calcTempoF()" ng-model="linhaF.series" ng-disabled="addlinhaFtn">
                                                                <option disabled="" selected="" value="">
                                                                    0x
                                                                </option>
                                                                <option ng-repeat="serie in seriesVetor" ng-value="serie.valor | number">
                                                                    @{{serie.texto}}
                                                                </option>
                                                            </select> </td>
                                                        <td>  <input class="form-control inputInlineTreinoNew" ng-blur="calcTempoF()" ng-model="linhaF.repeticoes" placeholder="0"
                                                                     type="text" ng-disabled="addlinhaFtn"></td>


                                                        <td>   <input class="form-control inputInlineTreinoNew" ng-model="linhaF.carga" placeholder="0" type="text" ng-disabled="addlinhaFtn"></td>
                                                            <td>  <select class="form-control inputInlineTreinoNew" ng-change="calcTempoF()" ng-model="linhaF.intervalo" ng-disabled="addlinhaFtn"  ng-show="linhaF.super_serie == 1">
                                                                <option disabled="" selected="" value="">
                                                                    0 seg
                                                                </option>
                                                                <option ng-repeat="intervalo in intervalosVetor" ng-value="intervalo.valor | number">
                                                                    @{{intervalo.texto}}
                                                                </option>
                                                            </select>
                                                        </td>

                                                        <td style="padding:0">

                                                            <!-- INICIO FECHAR E EXCLUIR -->
                                                            <span class="pull-right" style="    margin-top: -17px;">
                                                                <a class="btn btn-default " style="height: 31px; padding-top: 3px;"
                                                                   data-target="#modalObsF@{{linhaF.id}}" data-toggle="modal" ng-disabled="addlinhaFtn">
                                                                    <i aria-hidden="true" class="fa fa-comment"></i>
                                                                </a>
                                                                <a class="btn btn-danger " style="height: 31px;  padding-top: 3px; border-color: #e5e6e8;"
                                                                   ng-click="removeLinhaF($index, linhaF.ficha)" style=" border: 0;" ng-disabled="addlinhaFtn">
                                                                    <i aria-hidden="true" class="fa fa-trash"></i>
                                                                </a>

                                                            </span>
                                                            <!-- FIM FECHAR E EXCLUIR -->


                                                            <!-- OBSERVAÇÃO DO EXERCICIO TREINO A -->
                                                            <div aria-hidden="true" class="modal inmodal fade" id="modalObsF@{{linhaF.id}}" role="dialog" tabindex="-1">
                                                                <div class="modal-dialog">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <button class="close" data-dismiss="modal" type="button">
                                                                                <span aria-hidden="true">
                                                                                    ×
                                                                                </span>
                                                                                <span class="sr-only">
                                                                                    Fechar
                                                                                </span>
                                                                            </button>
                                                                            <h4 class="modal-title">
                                                                                Observação do Exercício
                                                                            </h4>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <div class="form-group">
                                                                                <input class="form-control" ng-model="linhaF.observacao" placeholder="Informe uma observação" type="text">
                                                                                </input>
                                                                            </div>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button class="btn btn-primary" data-dismiss="modal" type="button">
                                                                                OK
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- modal -->
                                                            <!-- OBSERVAÇÃO DO EXERCICIO TREINO A -->


                                                            <!-- modal -->

                                                        </td>
                                                    </tr>

                                                    <tr ng-repeat="super in linhaF.superserie" class="bi-serie">
                                                        <td id="ord_super" ng-hide="true">@{{super.ficha}}</td>
                                                        <td id="exercicio_super" ng-hide="true">@{{super.exercicio}}</td>
                                                        <td id="series_super" ng-hide="true">@{{super.series}}</td>
                                                        <td id="repeticoes_super" ng-hide="true">@{{super.repeticoes}}</td>
                                                        <td id="medida_duracao_super" ng-hide="true">@{{super.medida_duracao}}</td>
                                                        <td id="carga_super" ng-hide="true">@{{super.carga}}</td>
                                                        <td id="medida_intensidade_super" ng-hide="true">@{{super.medida_intensidade}}</td>
                                                        <td id="intervalo_super" ng-hide="true">@{{super.intervalo}}</td>   
                                                        <td id="super_serie_super" ng-hide="true">@{{super.super_serie}}</td>                                                           
                                                        <td  class="nameex">
                                                            <!-- FOTO -->
                                                            <div href="#" data-toggle="tooltip" title="Clique e arreste para mudar de posição" ng-click="rodaPreview(super.video)" 
                                                                 style="background-image:url('{{ url('/')}}/uploads/exercicios/@{{exercicios[changeExercicio(super.exercicio)].dsurlminiatura}}');" class="imgex-bi">
                                                            </div>
                                                        </td>

                                                        <td > 

                                                            <div class="form-group" style="padding-bottom: 0">

                                                                <div class="input-group" style="width: 100%;">
                                                                    <!--<div ng-repeat="super in linhaF.superserie" ng-value="exercicio.id">
                                                                        @{{super.exercicio}}
                                                                    </div>    -->

                                                                    <select class="chosen-select" id="execicioSelectF-super@{{super.id}}"  ng-change="changeExercicio(super.exercicio, linhaF)" ng-model="super.exercicio" ng-disabled="addlinhaFtn"  >
                                                                        <option disabled="" selected="" value="">

                                                                            Selecione o Exercício
                                                                        </option>
                                                                        <option ng-repeat="exercicio in exercicios" ng-value="exercicio.id">
                                                                            @{{ exercicio.nome_exercicio}}
                                                                        </option>
                                                                    </select>

                                                                    <a class="btn btn-default input-group-addon" data-target="#m_exercicios" data-toggle="modal" ng-disabled="addlinhaFtn">
                                                                        <i aria-hidden="true" class="fa fa-plus">
                                                                        </i>
                                                                    </a>

                                                                </div>
                                                            </div>
                                                        </td>

                                                        <td>  <select class="form-control inputInlineTreinoNew" ng-change="calcTempoF()" ng-model="super.series" ng-disabled="addlinhaFtn">
                                                                <option disabled="" selected="" value="">
                                                                    0x
                                                                </option>
                                                                <option ng-repeat="serie in seriesVetor" ng-value="serie.valor | number">
                                                                    @{{serie.texto}}
                                                                </option>
                                                            </select> </td>
                                                        <td>  <input class="form-control inputInlineTreinoNew" ng-blur="calcTempoF()" ng-model="super.repeticoes" placeholder="0"
                                                                     type="text" ng-disabled="addlinhaFtn"></td>


                                                        <td>   <input class="form-control inputInlineTreinoNew" ng-model="super.carga" placeholder="0" type="text" ng-disabled="addlinhaFtn"></td>
                                                        <td>  <select class="form-control inputInlineTreinoNew" ng-change="calcTempoF()" ng-model="super.intervalo" ng-disabled="addlinhaFtn"  ng-show="super.id == linhaF.superserie.length">
                                                                <option disabled="" selected="" value="">
                                                                    0 seg
                                                                </option>
                                                                <option ng-repeat="intervalo in intervalosVetor" ng-value="intervalo.valor | number">
                                                                    @{{intervalo.texto}}
                                                                </option>
                                                            </select>
                                                        </td>

                                                        <td style="padding:0">

                                                            <!-- INICIO FECHAR E EXCLUIR -->
                                                            <span class="pull-right" style="    margin-top: -17px;">
                                                                <a class="btn btn-default " style="height: 31px; padding-top: 3px;"
                                                                   data-target="#modalObsSUPERF@{{super.id}}" data-toggle="modal" ng-disabled="addLinhaBtn">
                                                                    <i aria-hidden="true" class="fa fa-comment"></i>
                                                                </a>
                                                                <a class="btn btn-danger " style="height: 31px;  padding-top: 3px; border-color: #e5e6e8;"
                                                                   ng-click="removeLinhaSuper($index, super, linhaF.superserie, linhaF.ficha)" style=" border: 0;" ng-disabled="addLinhaBtn">
                                                                    <i aria-hidden="true" class="fa fa-trash"></i>
                                                                </a>

                                                            </span>
                                                            <!-- FIM FECHAR E EXCLUIR -->

                                                            <div aria-hidden="true" class="modal inmodal fade" id="modalObsSUPERF@{{super.id}}" role="dialog" tabindex="-1">
                                                                <div class="modal-dialog">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <button class="close" data-dismiss="modal" type="button">
                                                                                <span aria-hidden="true">
                                                                                    ×
                                                                                </span>
                                                                                <span class="sr-only">
                                                                                    Fechar
                                                                                </span>
                                                                            </button>
                                                                            <h4 class="modal-title">
                                                                                Observação do Exercício
                                                                            </h4>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <div class="form-group">
                                                                                <input class="form-control" ng-model="super.observacao" placeholder="Informe uma observação" type="text">
                                                                                </input>
                                                                            </div>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button class="btn btn-primary" data-dismiss="modal" type="button">
                                                                                OK
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>                                                            
                                                            <!-- modal -->

                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            {{-- FIM DA TABELA NOVA  --}}

                                        </div>

                                    </div>
                                </td>
                            </tr>

                        </tbody>
                    </table>
                    <!-- fim tabele -->

                </div>
                <div class="form-group" style="margin-top:10px;">
                    <span style="text-align:left;margin-left:-15px;">
                        <a class="btn btn-primary" ng-click="addLinhaF()" ng-disabled="addlinhaFtn">
                            <i aria-hidden="true" class="fa fa-plus">
                            </i>
                        </a>
                    </span>
                    <span class="pull-right" style="margin-right:-15px;text-align:right;">
                        <b>
                            Tempo estimado @{{tempoestimadoA}}
                        </b>
                        <br> Musculação: @{{tempomusculacaoA}} + Aquecimento: 10 min + Alongamento: 4 min
                        </br>
                    </span>
                </div>
                <div class="clearfix">
                </div>
            </div>
        </div>
        <!-- fim table panel 6 (F) -->


        <!-- INICIO DA ABA panel 7(G)  -->
        <div class="tab-pane" id="tab-7" ng-show="tabsStatus.tab7">

            <div class="panel-body" style="padding:7px 20px;">
                <div class="row clearfix ScrrolAba" style="margin-top: 6px;  ">

                    <table class="table table-bordered pagin-table" id="abaGgg">
                        <tbody ng-model="linhaG">

                            <tr class="ordena-treino" ng-model="tabs" ng-repeat="linhaG in linhasG" style="background:#f5f5f5;" ng-disabled="addlinhaGtn">
                                <td id="ord" ng-hide="true">@{{linhaG.ficha}}</td>
                                <td id="exercicio" ng-hide="true">@{{linhaG.exercicio}}</td>
                                <td id="series" ng-hide="true">@{{linhaG.series}}</td>
                                <td id="repeticoes" ng-hide="true">@{{linhaG.repeticoes}}</td>
                                <td id="medida_duracao" ng-hide="true">@{{linhaG.medida_duracao}}</td>
                                <td id="carga" ng-hide="true">@{{linhaG.carga}}</td>
                                <td id="medida_intensidade" ng-hide="true">@{{linhaG.medida_intensidade}}</td>
                                <td id="intervalo" ng-hide="true">@{{linhaG.intervalo}}</td>   
                                <td id="super_serie" ng-hide="true">@{{linhaG.super_serie}}</td>   
                                <td id="hash_code" ng-hide="true">@{{linhaG.hash_code}}</td>   
                                <td id="observacao" ng-hide="true">@{{linhaG.observacao}}</td>   
                                
                                
                                <td style="padding: 0 ">
                                    <!-- EXIBE O NÚMERO de linhas -->
                                    <span class="marcador_exer">
                                        @{{linhaG.id}}
                                    </span>
                                    <!-- INICIO DA LINHA DO EXERCICIO -->
                                    <div class="row" style="margin-right: -6px;">
                                        <!-- {{-- <div class="col-xs-12 col-sm-1 hidden-xs" style="padding:0;     padding-right: 8px;">
                                         </div> --}}-->
                                        <div class="col-xs-12 col-sm-12"  >

                                            {{-- INICIO DA TABELA NOVA  --}}

                                            <table class="table_treino">
                                                <thead>
                                                    <tr>
                                                        <th style="width: 9%;">


                                                        </th>
                                                        <th style="width: 44%;">    
                                                            <select class="form-control selectTreinoNew" 
                                                                    ng-change="changeGenericoTesteA(linhaG.super_serie, linhaG)" ng-model="linhaG.super_serie" 
                                                                    required="required">
                                                                <option ng-repeat="super_serie in super_serieVetor" 
                                                                        ng-value="super_serie.valor">
                                                                    @{{super_serie.texto}}
                                                                </option>                                                              
                                                            </select>  
                                                        </th>
                                                        <th style="width: 8%;" class="nobold">Série</th>
                                                        <th style="width: 9.5%;"> 
                                                            <select class="form-control selectTreinoTransparent" ng-model="linhaG.medida_duracao" ng-disabled="addlinhaGtn" required="required">
                                                                <option ng-repeat="medida_duracao in medida_duracaosVetor" ng-value="medida_duracao.valor">
                                                                    @{{medida_duracao.texto}}
                                                                </option>                                                                
                                                            </select>  
                                                        </th>
                                                        <th style="width: 9.5%;"> 
                                                            <select class="form-control selectTreinoTransparent" 
                                                                    ng-model="linhaG.medida_intensidade"
                                                                    ng-disabled="addlinhaGtn" ng-change="changeGenericoTeste(linhaG.medida_intensidade)"
                                                                    required="required">
                                                                <option ng-repeat="medida_intensidade in medida_intensidadeVetor" 
                                                                        ng-value="medida_intensidade.valor">
                                                                    @{{medida_intensidade.texto}}
                                                                </option>                                                                       
                                                            </select>  
                                                        </th>
                                                        <th style="width: 8.75%;" class="nobold">Intervalo</th>

                                                        <th style="width: 94px">

                                                        </th>

                                                    </tr>
                                                </thead>

                                                <tbody>
                                                    {{-- LINHA Exercicio Simples --}}
                                                    <tr class="exercicio-simples">
                                                        <td style="padding:0"> 
                                                            <!-- FOTO -->
                                                            <div href="#" data-toggle="tooltip" title="Clique e arreste para mudar de posição" ng-click="rodaPreview(linhaG.video)"
                                                                 style="background-image:url('{{ url('/')}}/uploads/exercicios/@{{exercicios[changeExercicio(linhaG.exercicio)].dsurlminiatura}}');" class="imgex">
                                                            </div>
                                                        </td>
                                                        <td> 
                                                            <div class="form-group" style="padding-bottom: 0">
                                                                <div class="input-group" style="width: 100%;">

                                                                    <select class="chosen-select" id="execicioSelectG@{{linhaG.id}}"  ng-change="changeExercicio(linhaG.exercicio, linhaG)" ng-model="linhaG.exercicio" ng-disabled="addlinhaGtn"  >
                                                                        <option disabled="" selected="" value="">
                                                                            Selecione o Exercício
                                                                        </option>
                                                                        <option ng-repeat="exercicio in exercicios" ng-value="exercicio.id">
                                                                            @{{exercicio.nome_exercicio}}
                                                                        </option>
                                                                    </select>

                                                                    <a class="btn btn-default input-group-addon" data-target="#m_exercicios" data-toggle="modal" ng-disabled="addlinhaGtn">
                                                                        <i aria-hidden="true" class="fa fa-plus">
                                                                        </i>
                                                                    </a>

                                                                </div>
                                                            </div>
                                                        </td>

                                                        <td>  <select class="form-control inputInlineTreinoNew" ng-change="calcTempoG()" ng-model="linhaG.series" ng-disabled="addlinhaGtn">
                                                                <option disabled="" selected="" value="">
                                                                    0x
                                                                </option>
                                                                <option ng-repeat="serie in seriesVetor" ng-value="serie.valor | number">
                                                                    @{{serie.texto}}
                                                                </option>
                                                            </select> </td>
                                                        <td>  <input class="form-control inputInlineTreinoNew" ng-blur="calcTempoG()" ng-model="linhaG.repeticoes" placeholder="0"
                                                                     type="text" ng-disabled="addlinhaGtn"></td>


                                                        <td>   <input class="form-control inputInlineTreinoNew" ng-model="linhaG.carga" placeholder="0" type="text" ng-disabled="addlinhaGtn"></td>
                                                        <td>  <select class="form-control inputInlineTreinoNew" ng-change="calcTempoG()" ng-model="linhaG.intervalo" ng-disabled="addlinhaGtn" ng-show="linhaG.super_serie == 1">
                                                                <option disabled="" selected="" value="">
                                                                    0 seg
                                                                </option>
                                                                <option ng-repeat="intervalo in intervalosVetor" ng-value="intervalo.valor | number">
                                                                    @{{intervalo.texto}}
                                                                </option>
                                                            </select>
                                                        </td>

                                                        <td style="padding:0">

                                                            <!-- INICIO FECHAR E EXCLUIR -->
                                                            <span class="pull-right" style="    margin-top: -17px;">
                                                                <a class="btn btn-default " style="height: 31px; padding-top: 3px;"
                                                                   data-target="#modalObsG@{{linhaG.id}}" data-toggle="modal" ng-disabled="addlinhaGtn">
                                                                    <i aria-hidden="true" class="fa fa-comment"></i>
                                                                </a>
                                                                <a class="btn btn-danger " style="height: 31px;  padding-top: 3px; border-color: #e5e6e8;"
                                                                   ng-click="removeLinhaG($index, linhaG.ficha)" style=" border: 0;" ng-disabled="addlinhaGtn">
                                                                    <i aria-hidden="true" class="fa fa-trash"></i>
                                                                </a>

                                                            </span>
                                                            <!-- FIM FECHAR E EXCLUIR -->


                                                            <!-- OBSERVAÇÃO DO EXERCICIO TREINO A -->
                                                            <div aria-hidden="true" class="modal inmodal fade" id="modalObsG@{{linhaG.id}}" role="dialog" tabindex="-1">
                                                                <div class="modal-dialog">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <button class="close" data-dismiss="modal" type="button">
                                                                                <span aria-hidden="true">
                                                                                    ×
                                                                                </span>
                                                                                <span class="sr-only">
                                                                                    Fechar
                                                                                </span>
                                                                            </button>
                                                                            <h4 class="modal-title">
                                                                                Observação do Exercício
                                                                            </h4>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <div class="form-group">
                                                                                <input class="form-control" ng-model="linhaG.observacao" placeholder="Informe uma observação" type="text">
                                                                                </input>
                                                                            </div>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button class="btn btn-primary" data-dismiss="modal" type="button">
                                                                                OK
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- modal -->
                                                            <!-- OBSERVAÇÃO DO EXERCICIO TREINO A -->


                                                            <!-- modal -->

                                                        </td>
                                                    </tr>

                                                    <tr ng-repeat="super in linhaG.superserie" class="bi-serie">
                                                        <td id="ord_super" ng-hide="true">@{{super.ficha}}</td>
                                                        <td id="exercicio_super" ng-hide="true">@{{super.exercicio}}</td>
                                                        <td id="series_super" ng-hide="true">@{{super.series}}</td>
                                                        <td id="repeticoes_super" ng-hide="true">@{{super.repeticoes}}</td>
                                                        <td id="medida_duracao_super" ng-hide="true">@{{super.medida_duracao}}</td>
                                                        <td id="carga_super" ng-hide="true">@{{super.carga}}</td>
                                                        <td id="medida_intensidade_super" ng-hide="true">@{{super.medida_intensidade}}</td>
                                                        <td id="intervalo_super" ng-hide="true">@{{super.intervalo}}</td>   
                                                        <td id="super_serie_super" ng-hide="true">@{{super.super_serie}}</td>                                                           
                                                        <td  class="nameex">
                                                            <!-- FOTO -->
                                                            <div href="#" data-toggle="tooltip" title="Clique e arreste para mudar de posição" ng-click="rodaPreview(super.video)"
                                                                 style="background-image:url('{{ url('/')}}/uploads/exercicios/@{{exercicios[changeExercicio(super.exercicio)].dsurlminiatura}}');" class="imgex-bi">
                                                            </div>
                                                        </td>

                                                        <td > 

                                                            <div class="form-group" style="padding-bottom: 0">

                                                                <div class="input-group" style="width: 100%;">
                                                                    <!--<div ng-repeat="super in linhaG.superserie" ng-value="exercicio.id">
                                                                        @{{super.exercicio}}
                                                                    </div>    -->

                                                                    <select class="chosen-select" id="execicioSelectG-super@{{super.id}}"  ng-change="changeExercicio(super.exercicio, linhaG)" ng-model="super.exercicio" ng-disabled="addlinhaGtn"  >
                                                                        <option disabled="" selected="" value="">

                                                                            Selecione o Exercício
                                                                        </option>
                                                                        <option ng-repeat="exercicio in exercicios" ng-value="exercicio.id">
                                                                            @{{ exercicio.nome_exercicio}}
                                                                        </option>
                                                                    </select>

                                                                    <a class="btn btn-default input-group-addon" data-target="#m_exercicios" data-toggle="modal" ng-disabled="addlinhaGtn">
                                                                        <i aria-hidden="true" class="fa fa-plus">
                                                                        </i>
                                                                    </a>

                                                                </div>
                                                            </div>
                                                        </td>

                                                        <td>  <select class="form-control inputInlineTreinoNew" ng-change="calcTempoG()" ng-model="super.series" ng-disabled="addlinhaGtn">
                                                                <option disabled="" selected="" value="">
                                                                    0x
                                                                </option>
                                                                <option ng-repeat="serie in seriesVetor" ng-value="serie.valor | number">
                                                                    @{{serie.texto}}
                                                                </option>
                                                            </select> </td>
                                                        <td>  <input class="form-control inputInlineTreinoNew" ng-blur="calcTempoG()" ng-model="super.repeticoes" placeholder="0"
                                                                     type="text" ng-disabled="addlinhaGtn"></td>


                                                        <td>   <input class="form-control inputInlineTreinoNew" ng-model="super.carga" placeholder="0" type="text" ng-disabled="addlinhaGtn"></td>
                                                        <td>  <select class="form-control inputInlineTreinoNew" ng-change="calcTempoG()" ng-model="super.intervalo" ng-disabled="addlinhaGtn"  ng-show="super.id == linhaG.superserie.length">
                                                                <option disabled="" selected="" value="">
                                                                    0 seg
                                                                </option>
                                                                <option ng-repeat="intervalo in intervalosVetor" ng-value="intervalo.valor | number">
                                                                    @{{intervalo.texto}}
                                                                </option>
                                                            </select>
                                                        </td>

                                                        <td style="padding:0">

                                                            <!-- INICIO FECHAR E EXCLUIR -->
                                                            <span class="pull-right" style="    margin-top: -17px;">
                                                                <a class="btn btn-default " style="height: 31px; padding-top: 3px;"
                                                                   data-target="#modalObsSUPERG@{{super.id}}" data-toggle="modal" ng-disabled="addLinhaBtn">
                                                                    <i aria-hidden="true" class="fa fa-comment"></i>
                                                                </a>
                                                                <a class="btn btn-danger " style="height: 31px;  padding-top: 3px; border-color: #e5e6e8;"
                                                                   ng-click="removeLinhaSuper($index, super, linhaG.superserie, linhaG.ficha)" style=" border: 0;" ng-disabled="addLinhaBtn">
                                                                    <i aria-hidden="true" class="fa fa-trash"></i>
                                                                </a>

                                                            </span>
                                                            <!-- FIM FECHAR E EXCLUIR -->

                                                            <div aria-hidden="true" class="modal inmodal fade" id="modalObsSUPERG@{{super.id}}" role="dialog" tabindex="-1">
                                                                <div class="modal-dialog">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <button class="close" data-dismiss="modal" type="button">
                                                                                <span aria-hidden="true">
                                                                                    ×
                                                                                </span>
                                                                                <span class="sr-only">
                                                                                    Fechar
                                                                                </span>
                                                                            </button>
                                                                            <h4 class="modal-title">
                                                                                Observação do Exercício
                                                                            </h4>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <div class="form-group">
                                                                                <input class="form-control" ng-model="super.observacao" placeholder="Informe uma observação" type="text">
                                                                                </input>
                                                                            </div>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button class="btn btn-primary" data-dismiss="modal" type="button">
                                                                                OK
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>                                                            
                                                            <!-- modal -->

                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            {{-- FIM DA TABELA NOVA  --}}

                                        </div>

                                    </div>
                                </td>
                            </tr>

                        </tbody>
                    </table>
                    <!-- fim tabele -->

                </div>
                <div class="form-group" style="margin-top:10px;">
                    <span style="text-align:left;margin-left:-15px;">
                        <a class="btn btn-primary" ng-click="addLinhaG()" ng-disabled="addlinhaGtn">
                            <i aria-hidden="true" class="fa fa-plus">
                            </i>
                        </a>
                    </span>
                    <span class="pull-right" style="margin-right:-15px;text-align:right;">
                        <b>
                            Tempo estimado @{{tempoestimadoA}}
                        </b>
                        <br> Musculação: @{{tempomusculacaoA}} + Aquecimento: 10 min + Alongamento: 4 min
                        </br>
                    </span>
                </div>
                <div class="clearfix">
                </div>
            </div>
        </div>
        <!-- fim table panel 7(G) -->




    </div>
    <!-- FIM PANEL -->
    <div style="height:20px;">
    </div>
    <span ng-hide="semTreino==false" class="pull-right" style="margin:-8px 5px 5px 0;">
        <button class="btn btn-primary" ng-hide="treino_padrao == false" data-target="#modalNomeTreinoPadrao" data-toggle="modal" ng-disabled="salvarTreinoBtn">
            <i class="fa fa-check" style="margin-right:5px;">
            </i>
            Salvar
        </button>
        <button class="btn btn-primary" ng-hide="treino_padrao == true" ng-click="salvarTreino('S')" ng-disabled="salvarTreinoBtn">
            <i class="fa fa-check" style="margin-right:5px;">
            </i>
            Salvar
        </button>
    </span>
    <!--<div style="float:right;margin-right:15px;">
        <div class="checkbox m-r-xs">
            <input id="checkbox" ng-model="treino_padrao" type="checkbox">
            <label for="checkbox">
                Utilizar como treino padrão
            </label>
            </input>
        </div>
    </div>-->
</div>

<div aria-hidden="true" class="modal inmodal fade" id="modalNomeTreinoPadrao" role="dialog" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" data-dismiss="modal" type="button">
                    <span aria-hidden="true">
                        ×
                    </span>
                    <span class="sr-only">
                        Fechar
                    </span>
                </button>
                <h4 class="modal-title">
                    Nome do Treino
                </h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <input class="form-control" ng-model="treino_nome_padrao" placeholder="Informe o nome do treino" type="text">
                    </input>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" data-dismiss="modal" ng-click="salvarTreino()" type="button">
                    OK
                </button>
            </div>
        </div>
    </div>
</div>

