<!DOCTYPE html>
<html lang="pt-br" ng-app="personalApp">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>PersonalClubBrasil</title>
        {!! Html::style("tema_assets/css/bootstrap.min.css") !!}
        {!! Html::style("tema_assets/font-awesome/css/font-awesome.css") !!}
        {!! Html::style("tema_assets/css/animate.css") !!}
        {!! Html::style("tema_assets/css/style.css") !!}
        {!! Html::style("css/app.css") !!}
        {!! Html::style("dist/app.css") !!}
    </head>
    <body class="auth-page"  ng-controller="leadController">
        
        <div class="middle-box  animated fadeInDown">
            <div class="auth-2">
                <div class="text-center">
                    <img src="{{url('/')}}/tema_assets/img/logo_topo.png">
                </div>
                <div class="loader_home" style="margin-bottom: 20px">
                    <div class="sk-spinner sk-spinner-wave">
                        <div class="sk-rect1"></div>
                        <div class="sk-rect2"></div>
                        <div class="sk-rect3"></div>
                        <div class="sk-rect4"></div>
                        <div class="sk-rect5"></div>
                    </div>
                </div>
                <div class="body-message">
                    <h1>Testar o Aplicativo e Painel de Controle!</h1>
                    
                    <!-- <p>Para </p>  -->
                    <form id="form-prospect" role="form" >
                        <div class="row">
                            <div class="col-md-12">
                
           
                                    <div class="contact-message" style="text-align: center;"></div>
                                    
                                   <div class="form-group">
                                        <div class="form-group"><label> Nome do Resposável</label>
                                        <input ng-model="prospect.name" type="text" placeholder="" id="name"  class="form-control" >
                                        
                                    </div>

                                    <div class="form-group">
                                        <label>E-mail do Resposável</label>
                                        <input ng-model="prospect.email" id="email"
                                        type="email" placeholder=""
                                        class="form-control" >
                                        
                                    </div>


                            <div class="form-group">
                                <label>Celular (WhatsApp do Resposável )</label>
                                  <input ng-model="prospect.telefone" id="telefone" ui-br-phone-number type="text"
                                  placeholder="" class="form-control telefone"  />
                                </div>


                        
                       

                          <div class="form-group"><label>Nome Fantasia  <small>(da Academia)</small> </label>
                          <input ng-model="prospect.fantasia" id="fantasia" type="text"
                          placeholder="" class="form-control"  > </div>
                        
                       
                         {{--  <div class="form-group"><label>CNPJ:</label>
                          <input ng-model="prospect.cnpj" id="cnpj" type="text"
                          placeholder="" class="form-control"> </div> --}}
                        
                    </div>
                </div>  
{{-- 
                <div class="col-xs-6 col-sm-6" >
                        
                        <div class="form-group"><label>Estado</label>
                          <select name="estado" id="estado" class="form-control" onchange="carregaCidade(this.value)" required="required">
                            <option value="">Selecione</option>
                          </select> </div>
                        </div>

                        <div class="col-xs-6 col-sm-6" >
                          <div class="form-group"><label>Cidade</label>
                          <select name="cidade" id="cidade" class="form-control" required="required">
                            <option value="">Selecione</option>
                          </select> </div>
                        </div>
                </div> --}}
                               
             
                      <div class="form-group">
                            <button ng-click="cadastrar_lead()" ng-disabled="clicado==1" class="btn btn-lg btn-pr btn-block" >
                            <strong>INICIAR TESTE COMPLETO <i class="fa fa-arrow-right" aria-hidden="true"></i></strong></button>
                        </div>
                            
                            <div style="clear:both"></div>
                    
                </div>
                <!-- fim row -->
               
               
            </form>
            
        </div>



            

        <p class="m-t text-center" >
            <small style="color: #ccc">Copyright Personal Club Brasil © 2016-<?php echo date("Y"); ?> </small>
        </p>
    </div>
</div>
<!-- Mainly scripts -->
{!! HTML::script("https://ajax.googleapis.com/ajax/libs/angularjs/1.6.7/angular.min.js") !!}
{!! HTML::script("tema_assets/js/jquery-2.1.1.js") !!}
{!! HTML::script("tema_assets/js/bootstrap.min.js") !!}
{!! HTML::script("assets_admin/vendor/jquery-form/jquery.form.min.js") !!}
{!! Html::script("assets_admin/vendor/pnotify/pnotify.custom.js") !!}
{!! Html::script("assets_admin/vendor/jquery-maskedinput/jquery.maskedinput.min.js") !!}

</body>


<script type="text/javascript">
    
/*    

function validarCNPJ(cnpj) {
 
    cnpj = cnpj.replace(/[^\d]+/g,'');
 
    if(cnpj == '') return false;
     
    if (cnpj.length != 14)
        return false;
 
    // Elimina CNPJs invalidos conhecidos
    if (cnpj == "00000000000000" || 
        cnpj == "11111111111111" || 
        cnpj == "22222222222222" || 
        cnpj == "33333333333333" || 
        cnpj == "44444444444444" || 
        cnpj == "55555555555555" || 
        cnpj == "66666666666666" || 
        cnpj == "77777777777777" || 
        cnpj == "88888888888888" || 
        cnpj == "99999999999999")
        return false;
         
    // Valida DVs
    tamanho = cnpj.length - 2
    numeros = cnpj.substring(0,tamanho);
    digitos = cnpj.substring(tamanho);
    soma = 0;
    pos = tamanho - 7;
    for (i = tamanho; i >= 1; i--) {
      soma += numeros.charAt(tamanho - i) * pos--;
      if (pos < 2)
            pos = 9;
    }
    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (resultado != digitos.charAt(0))
        return false;
         
    tamanho = tamanho + 1;
    numeros = cnpj.substring(0,tamanho);
    soma = 0;
    pos = tamanho - 7;
    for (i = tamanho; i >= 1; i--) {
      soma += numeros.charAt(tamanho - i) * pos--;
      if (pos < 2)
            pos = 9;
    }
    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (resultado != digitos.charAt(1))
          return false;
           
    return true;
    
}    
    
//     */
   
// function carregaEstado() {

   

//     $.ajax({
//         type: 'get',
//         url: '/api/unidade/getEstados',
//         //data: { empresa: $("#empresa").val() },
//         dataType: 'json',
//         contentType: "application/json; charset=utf-8",
//         success: function (obj) {
//             if (obj != null) {
//                 var data = obj.data;
//                 var selectbox = $('#estado');

//                 selectbox.find('option').remove();
//                 for (var i = 0; i < obj.length; i++) {
//                     $('<option>').val(obj[i].id).text(obj[i].nome).appendTo(selectbox);
//                 }
//                 $('#clienteSelect').addClass('chosen-select');
//                 $('.chosen-select').chosen({width: "100%"});

//             }
//         }
//     });
// }          
// function carregaCidade(uf) {

//     $.ajax({
//         type: 'get',
//         url: '/api/unidade/getCidadesGet/' + uf,
//         //data: { empresa: $("#empresa").val() },
//         dataType: 'json',
//         contentType: "application/json; charset=utf-8",
//         success: function (obj) {
//             if (obj != null) {
//                 var data = obj.data;
//                 var selectbox = $('#cidade');

//                 selectbox.find('option').remove();
//                 for (var i = 0; i < obj.length; i++) {
//                     $('<option>').val(obj[i].id).text(obj[i].nome).appendTo(selectbox);
//                 }
//                 $('#clienteSelect').addClass('chosen-select');
//                 $('.chosen-select').chosen({width: "100%"});

//             }
//         }
//     });
// }          
    
    $(document).ready(function() {  
        
        $('.telefone').mask("(99) 99999-9999");
        // $('#cnpj').mask("99.999.999/9999-99");
        
        // carregaEstado();
        // carregaCidade(1);

    });

    var app =  angular.module('personalApp', []).run(function ($rootScope, $http) { });
        
        
        
    app.controller('leadController', function ($http, $scope) {
    

    

    $scope.clicado=0;
    $scope.prospect = {};
    $scope.cadastrar_lead = function() {



        $scope.prospect.idunidade = 3;
        $scope.prospect.role = 'cliente';
        $scope.prospect.avatar = 'user-a4.jpg';
        $scope.prospect.telefone = $('#telefone').val();
        $scope.prospect.cnpj = 0;
        $scope.prospect.fantasia = $('#fantasia').val();
        // $scope.prospect.estado = $('#estado').val();
        // $scope.prospect.cidade = $('#cidade').val();

        $scope.prospect.estado = 1;
        $scope.prospect.cidade = 1;
        
        
        
        // var x = validarCNPJ($scope.prospect.cnpj);

        
        console.log($scope.prospect);
        // if (x === false || $scope.prospect.name === '' ) {
        //     $(".contact-message").stop(true).html('<i class="fa fa-warning"></i> CNPJ inválido').css("color","#ef4b4b");
        //     $("input#cnpj").focus().addClass("error");
        // }          
        
        if ($scope.prospect.name === undefined || $scope.prospect.name === '' ) {
            $(".contact-message").stop(true).html('<i class="fa fa-warning"></i> O campo nome é obrigatório.').css("color","#ef4b4b");
            $("input#name").focus().addClass("error");
        }  
        else if( $scope.prospect.email === undefined || $scope.prospect.email === ''){
            $(".contact-message").stop(true).html('<i class="fa fa-warning"></i> O campo e-mail é obrigatório.').css("color","#ef4b4b");
            $("input#email").focus().addClass("error");
        }
        else if( $scope.prospect.telefone === undefined || $scope.prospect.email === ''){
        $(".contact-message").stop(true).html('<i class="fa fa-warning"></i> O campo celular é obrigatório.').css("color","#ef4b4b");
        $("input#telefone").focus().addClass("error");
        
        
        }  else {
    $http({

        url: 'https://sistemapcb.com.br/cadastrar_lead2',
        data: $scope.prospect,
        method: "POST"
        }).then(function (response) {
            $scope.resposta = response.data;
        console.log($scope.resposta);
        $scope.clicado=1;
        if($scope.resposta.status == 'success') {
            var id = $scope.resposta.user_id;
            window.location.assign("https://sistemapcb.com.br/parabens_lead2/"+id);
        }
    }, function (response) {
        console.log(response);
        // console.log('Opsss... Algo deu errado!');
    });
    
    

}
        
    
} // fim da funcition
    
    
    });
</script>

<style>


.auth-2 {
    padding: 15px;
    background: #fff;
    text-align: center;
}
.auth-2 h1 {
    font-size: 27px;
    text-align: center;
    border-bottom: 2px solid #d8d8d8;
    margin-bottom: 11px;
    padding-bottom: 10px;
    font-weight: bold;
}
.body-message {
    text-align: left;
}
.btn-pr {
    color: #fff;
    background-color: #35b729;
    border-color: #35b729;
}

.btn-pr:focus-within , .btn-pr:visited {
    color: #fff;
    background-color: #34c726;
    border-color: #35b729;
}

.btn-pr:active {
    color: #fff;
    background-color: #34c726;
    border-color: #35b729;
}

.btn-pr:hover {
    color: #fff;
    background-color: #34c726;
    border-color: #35b729;
}
   
.btn-pr:focus, .btn-pr.focus {
    color: #fff;
    background-color: #35b729;
    border-color: #35b729;
}
</style>
</html>