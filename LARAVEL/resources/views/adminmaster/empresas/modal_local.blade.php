

<div aria-hidden="true" class="modal inmodal fade" id="local_modal" role="dialog" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body " ng-class="{'is-loading' : loading}">
                <div class="loading"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></div>
                <button class="close" data-dismiss="modal" type="button">
                <span aria-hidden="true">
                    ×
                </span>
                <span class="sr-only">
                    Fechar
                </span>
                </button>
                {{-- inicio contuedo--}}
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h4>
                        Locais
                        </h4>
                        <div class="ibox-content">
                            <div class="row">
                                <div class="form-group">
                                    <label>Nome</label>
                                    <input class="form-control"  placeholder=" " ng-model="local.nmlocal">
                                </div>
                                <div class="form-group">
                                    <label>M²</label>
                                    <input class="form-control"  placeholder=" " ng-model="local.metros">
                                </div>
                                
                                
                                
                                <div class="form-group">
                                    <span class="pull-right">
                                        
                                        <button class="btn btn-primary " ng-click="register()" type="button">
                                        <i class="fa fa-check">
                                        </i>
                                        Salvar
                                        </button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>