
<div aria-hidden="true" class="modal inmodal fade" id="empresa_modal" role="dialog" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body " ng-class="{'is-loading' : loading}">
                <div class="loading"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></div>
                <button class="close" data-dismiss="modal" type="button">
                <span aria-hidden="true">
                    ×
                </span>
                <span class="sr-only">
                    Fechar
                </span>
                </button>
                {{-- inicio contuedo--}}
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h4>
                        Filial - Editar / Cadastrar
                        </h4>
                        <div class="ibox-content">
                          

                          <div class="row">
                                <ul class="list-group clear-list" style="margin-top:10px">
                                        
                                        <!-- item -->
                                     <li style="list-style: none;">
                                        <div class="col-lg-12" style="padding: 0; margin: 0">
                                            <div class="form-group">
                                                <label for="nome">Nome Fantasia:</label>
                                                <input class="form-control" ng-model="empresa.fantasia" type="text" readonly=""></input>
                                            </div>
                                        </div>
                                    </li>
                                    
                                       <!-- item -->
                                     <li style="list-style: none;">
                                        <div class="col-lg-12" style="padding: 0; margin: 0">
                                            <div class="form-group">
                                                <label class="font-noraml">Razão Social:</label>
                                      
                                            <input class="form-control" ng-model="empresa.razao_social" type="text" readonly=""> </input>
                                            </div>
                                        </div>
                                    </li>


                                       <!-- item -->
                                     <li style="list-style: none;">
                                        <div class="col-lg-12" style="padding: 0; margin: 0">
                                            <div class="form-group">
                                                 <label class="font-noraml">CNPJ:</label>
                                       
                                           
                                            <input class="form-control" ng-model="empresa.idunidade" type="hidden" value="{{$idunidade}}">
                                            <input class="form-control" ng-model="empresa.id" type="hidden" readonly="">
                                            <input class="form-control" ng-model="empresa.iddados" type="hidden" readonly="">
                                            <input class="form-control" ng-model="empresa.cnpj" type="text" readonly="">
                                            </input>
                                            </div>
                                        </div>
                                    </li>

                                      <!-- item -->
                                     <li style="list-style: none;">
                                        <div class="col-lg-12" style="padding: 0; margin: 0">
                                            <div class="form-group">
                                                <label class="font-noraml">Inscrição Estadual:</label>
                                      
                                            <input class="form-control"  type="text" readonly=""> </input>
                                            </div>
                                        </div>
                                    </li>

                                <hr>

                                   
                                    <div class="col-lg-12" style="padding: 0; margin: 0">
                                        <div class="form-group">
                                            {!! Form::label('Endereço', 'Endereço:') !!}
                                            <input type="text" class="form-control" ng-model="client.dados.endereco"
                                                   ng-readonly="!editing">
                                        </div>
                                    </div>



                                     <div class="col-xs-12 col-lg-4" style="padding: 0; margin: 0">
                                        <div class="form-group">
                                            {!! Form::label('Numero', 'Número:') !!}
                                             <input class="form-control"  type="text" readonly=""> </input>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-lg-8" style="  padding-right: 0;" >
                                        <div class="form-group">
                                            {!! Form::label('CEP', 'CEP:') !!}
                                           <input class="form-control"  type="text" readonly=""> </input>
                                        </div>
                                    </div>


                                    <div class="col-lg-12" style="padding: 0; margin: 0">
                                        <div class="form-group">
                                            {!! Form::label('Bairro', 'Bairro:') !!}
                                            <input type="text" class="form-control" ng-model="client.dados.bairro"
                                                   ng-readonly="!editing">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-lg-8" style="padding: 0; margin: 0">
                                        <div class="form-group">
                                            {!! Form::label('Cidade', 'Cidade:') !!}
                                            <input type="text" class="form-control" ng-model="client.dados.cidade"
                                                   ng-readonly="!editing">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-lg-4" style="  padding-right: 0;" >
                                        <div class="form-group">
                                            {!! Form::label('Estado', 'Estado:') !!}
                                            <input type="text" class="form-control" ng-model="client.dados.estado"
                                                   ng-readonly="!editing">
                                        </div>
                                    </div>
                                     




                                        <!-- item -->
                                     <li style="list-style: none;">
                                        <div class="col-xs-12 col-lg-6" style="padding: 0; margin: 0">
                                            <div class="form-group">
                                               
                                         <label class="font-noraml">Telefone:</label>
                                          
                     
                                            <input class="form-control" ng-model="empresa.telefone" type="text" readonly="">
                                            </input>
                                            </div>
                                        </div>
                                    </li>
                                         
                                    
                                      <!-- item -->
                                     <li style="list-style: none;">
                                        <div class="col-xs-12 col-lg-6" style="  padding-right: 0;" >
                                            <div class="form-group">
                                               
                                          <label class="font-noraml">Celular:</label>
                                        <input class="form-control" ng-model="empresa.celular" type="text" readonly=""> </input>
                                            </div>
                                        </div>
                                    </li>

                                     <!-- item -->
                                     <li style="list-style: none;">
                                        <div class="col-lg-12" style="padding: 0; margin: 0">
                                            <div class="form-group">
                                               
                                          <label class="font-noraml">E-mail:</label>
                                      <input class="form-control" ng-model="empresa.email" type="text" readonly=""> </input>
                                            </div>
                                        </div>
                                    </li>
                    

                                     <!-- item -->
                                     <li style="list-style: none;">
                                        <div class="col-lg-12" style="padding: 0; margin: 0">
                                            <div class="form-group">
                                                <label class="font-noraml">Site:</label>
                                      
                                        <input class="form-control" ng-model="empresa.site" type="text" readonly=""></input>
                                            </div>
                                        </div>
                                    </li>


                                   
                                        <li class="list-group-item fist-item" >
                                            <span class="pull-right">
                                                <button class="btn btn-primary display-no" id="salvar_aluno" ng-click="updateUnidade(empresa)" style="margin-bottom: 20px" type="button">
                                                    <i class="fa fa-check">
                                                    </i>
                                                    Salvar
                                                </button>
                                            </span>
                                            {{-- Write a letter to Sandra --}}
                                        </li>
                                    </ul>
                         </div>



                            <div class="row">
                                <span class="text-muted small pull-right" ng-click="registerUnidade()">
                                        <a class="btn btn-primary btn-sm" style="border-radius: 4px;">
                                        <i class="fa fa-plus-square">
                                        </i>
                                        Cadastrar
                                        </a>
                                    </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
