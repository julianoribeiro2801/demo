@extends('layouts.appmaster')

@section('content')
@push('scripts')
{!! Html::script("js/empresaController.js") !!}

@endpush


<div class="wrapper wrapper-content animated fadeInRight" ng-controller="empresaController">
    <div class="row">
        <div class="col-sm-4">
            <div class="ibox ">
                <div class="ibox-content" style="padding-bottom: 0">
                    <div class="tab-content">
                        <div class="tab-pane active" id="contact-1">
                            <div class="row m-b-lg">
                                <div class="col-lg-12 text-center">
                                    <div class="m-b-sm">
                                        <img alt="image" class="img-circle" src="{{ url('/') }}/icones/matix.png" style="width: 150px">
                                        </img>
                                    </div>
                                </div>
                              <!--   <div class="col-lg-8">
                                    <h2>
                                        @{{empresa.razao_social }}
                                    </h2>

                                </div> -->
                                <div style="padding: 10px 10px 0 10px; clear:both">
                                    <script type="text/javascript">
                                        function btnClick(){
                                             // $( "#salvar_aluno" ).toggleClass( "disabled", addOrRemove );
                                             $( "#salvar_aluno" ).toggle( "slow", function() {
                                                // Animation complete.

                                              });
                                             $('#editar_aluno').hide(120);
                                             $('.fist-item input').prop('readonly', function(i, v) { return !v; });
                                         }

                                        function reversobtnClick(){
                                             // $( "#salvar_aluno" ).toggleClass( "disabled", addOrRemove );
                                             $( "#editar_aluno" ).toggle( "slow", function() {
                                                // Animation complete.

                                              });
                                             $('#salvar_aluno').hide(120);
                                             $('.fist-item input').prop('readonly', function(i, v) { return !v; });
                                         }

                                    </script>

                                    <strong>
                                        Dados da Empresa
                                    </strong>
                                    <span class="pull-right">
                                       <!--  <button class="btn btn-link btn-sm" id="editar_aluno" onclick="btnClick()" type="button">
                                            <i class="fa fa-edit">
                                            </i>
                                            Editar
                                        </button> -->


                                        <button id="editar_aluno" onclick="btnClick()"  class="btn btn-primary btn-sm pull-right " id="editar_aluno"
                                                type="button">
                                            <i class="fa fa-edit"></i>&nbsp;Editar
                                        </button>


                                    </span>
                                    <div style="clear:both">
                                    </div>
                                    <ul class="list-group clear-list" style="margin-top:10px">

                                        <!-- item -->
                                     <li style="list-style: none;">
                                        <div class="col-lg-12" style="padding: 0; margin: 0">
                                            <div class="form-group">
                                                <label for="nome">Nome Fantasia:</label>
                                                <input class="form-control" ng-model="empresa.fantasia" type="text" readonly="">

                                            </div>
                                        </div>
                                    </li>

                                       <!-- item -->
                                     <li style="list-style: none;">
                                        <div class="col-lg-12" style="padding: 0; margin: 0">
                                            <div class="form-group">
                                                <label class="font-noraml">Razão Social:</label>

                                            <input class="form-control" ng-model="empresa.razao_social" type="text" readonly=""> </input>
                                            </div>
                                        </div>
                                    </li>


                                       <!-- item -->
                                     <li style="list-style: none;">
                                        <div class="col-lg-12" style="padding: 0; margin: 0">
                                            <div class="form-group">
                                                 <label class="font-noraml">CNPJ:</label>


                                            <input class="form-control" ng-model="empresa.idunidade" type="hidden" value="{{$idunidade}}">
                                            <input class="form-control" ng-model="empresa.id" type="hidden" readonly="">
                                            <input class="form-control" ng-model="empresa.iddados" type="hidden" readonly="">
                                            <input class="form-control" ng-model="empresa.cnpj" type="text" readonly="">
                                            </input>
                                            </div>
                                        </div>
                                    </li>

                                      <!-- item -->
                                     <li style="list-style: none;">
                                        <div class="col-lg-12" style="padding: 0; margin: 0">
                                            <div class="form-group">
                                                <label class="font-noraml">Inscrição Estadual:</label>

                                            <input class="form-control"  type="text" readonly=""> </input>
                                            </div>
                                        </div>
                                    </li>

                                <hr>


                                    <div class="col-lg-12" style="padding: 0; margin: 0">
                                        <div class="form-group">
                                            {!! Form::label('Endereço', 'Endereço:') !!}
                                            <input type="text" class="form-control" ng-model="client.dados.endereco"
                                                   ng-readonly="!editing">
                                        </div>
                                    </div>



                                     <div class="col-xs-12 col-lg-4" style="padding: 0; margin: 0">
                                        <div class="form-group">
                                            {!! Form::label('Numero', 'Número:') !!}
                                             <input class="form-control"  type="text" readonly=""> </input>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-lg-8" style="  padding-right: 0;" >
                                        <div class="form-group">
                                            {!! Form::label('CEP', 'CEP:') !!}
                                           <input class="form-control"  type="text" readonly=""> </input>
                                        </div>
                                    </div>


                                    <div class="col-lg-12" style="padding: 0; margin: 0">
                                        <div class="form-group">
                                            {!! Form::label('Bairro', 'Bairro:') !!}
                                            <input type="text" class="form-control" ng-model="client.dados.bairro"
                                                   ng-readonly="!editing">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-lg-8" style="padding: 0; margin: 0">
                                        <div class="form-group">
                                            {!! Form::label('Cidade', 'Cidade:') !!}
                                            <input type="text" class="form-control" ng-model="client.dados.cidade"
                                                   ng-readonly="!editing">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-lg-4" style="  padding-right: 0;" >
                                        <div class="form-group">
                                            {!! Form::label('Estado', 'Estado:') !!}
                                            <input type="text" class="form-control" ng-model="client.dados.estado"
                                                   ng-readonly="!editing">
                                        </div>
                                    </div>





                                        <!-- item -->
                                     <li style="list-style: none;">
                                        <div class="col-xs-12 col-lg-6" style="padding: 0; margin: 0">
                                            <div class="form-group">

                                         <label class="font-noraml">Telefone:</label>


                                            <input class="form-control" ng-model="empresa.telefone" type="text" readonly="">
                                            </input>
                                            </div>
                                        </div>
                                    </li>


                                      <!-- item -->
                                     <li style="list-style: none;">
                                        <div class="col-xs-12 col-lg-6" style="  padding-right: 0;" >
                                            <div class="form-group">

                                          <label class="font-noraml">Celular:</label>
                                        <input class="form-control" ng-model="empresa.celular" type="text" readonly=""> </input>
                                            </div>
                                        </div>
                                    </li>

                                     <!-- item -->
                                     <li style="list-style: none;">
                                        <div class="col-lg-12" style="padding: 0; margin: 0">
                                            <div class="form-group">

                                          <label class="font-noraml">E-mail:</label>
                                      <input class="form-control" ng-model="empresa.email" type="text" readonly=""> </input>
                                            </div>
                                        </div>
                                    </li>


                                     <!-- item -->
                                     <li style="list-style: none;">
                                        <div class="col-lg-12" style="padding: 0; margin: 0">
                                            <div class="form-group">
                                                <label class="font-noraml">Site:</label>

                                        <input class="form-control" ng-model="empresa.site" type="text" readonly=""></input>
                                            </div>
                                        </div>
                                    </li>



                                        <li class="list-group-item fist-item" >
                                            <span class="pull-right">
                                                <button class="btn btn-primary display-no" id="salvar_aluno" ng-click="updateUnidade(empresa)" style="margin-bottom: 20px" type="button">
                                                    <i class="fa fa-check">
                                                    </i>
                                                    Salvar
                                                </button>
                                            </span>
                                            {{-- Write a letter to Sandra --}}
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="contact-3">
                            <div class="row m-b-lg">
                                <div class="col-lg-4 text-center">
                                    <div class="m-b-sm">
                                        <img alt="image" class="img-circle" src="http://www.fanta.com.br/content/dam/GO/fanta/Brazil/brazil/seo/logo_fanta_seo.png" style="width: 62px">
                                        </img>
                                    </div>
                                </div>
                                <div class="col-lg-8">
                                    <h2>
                                        Fanta
                                    </h2>
                                </div>
                            </div>
                            <div class="client-detail">
                                <div class="full-height-scroll">
                                    <strong>
                                        Dados da Empresa
                                    </strong>
                                    <ul class="list-group clear-list" style="margin-top:10px">
                                        <li class="list-group-item fist-item">
                                            <span class="pull-right">
                                                {{--  --}}
                                            </span>
                                            <input class="form-control" pattern="" readonly="" required="required" title="" type="text" ng-model="empresa.cnpj">
                                            </input>
                                        </li>
                                        <li class="list-group-item fist-item">
                                            <span class="pull-right">
                                                {{--  --}}
                                            </span>
                                            <input class="form-control" ng-model="empresa.fantasia" type="text" readonly="">
                                            </input>
                                        </li>
                                        <li class="list-group-item fist-item">
                                            <span class="pull-right">
                                                {{--  --}}
                                            </span>
                                            <input class="form-control" id="inputRsocial" name="aluno" pattern="" readonly="" required="required" title="" type="text" value="Acamia Indside Corpo Perfeito LTDA">
                                            </input>
                                        </li>
                                        <li class="list-group-item fist-item">
                                            {{--
                                            <span class="pull-right">
                                                10:16 am
                                            </span>
                                            --}}
                                            <input class="form-control" id="inputAluno" name="aluno" pattern="" readonly="" required="required" title="" type="text" value="(45) 9 9911-0510">
                                            </input>
                                        </li>
                                        <li class="list-group-item fist-item">
                                            {{--
                                            <span class="pull-right">
                                                08:22 pm
                                            </span>
                                            --}}
                                            <input class="form-control" id="inputAluno" name="aluno" pattern="" readonly="" required="required" title="" type="text" value="ruiz@msn.com">
                                            </input>
                                        </li>
                                        <li class="list-group-item fist-item">
                                            <span class="pull-right">
                                                <button class="btn btn-primary display-no" id="salvar_aluno" onclick="reversobtnClick()" type="button">
                                                    <i class="fa fa-check">
                                                    </i>
                                                    Salvar
                                                </button>
                                            </span>
                                            {{-- Write a letter to Sandra --}}
                                        </li>
                                    </ul>
                                    <hr/>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="company-1">
                            <div class="m-b-lg">
                                <h2>
                                    Tellus Institute
                                </h2>
                                <p>
                                    Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero,written in 45 BC. This book is a treatise on.
                                </p>
                                <div>
                                    <small>
                                        Active project completion with: 48%
                                    </small>
                                    <div class="progress progress-mini">
                                        <div class="progress-bar" style="width: 48%;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="client-detail">
                                <div class="full-height-scroll">
                                    <strong>
                                        Last activity
                                    </strong>
                                    <ul class="list-group clear-list">
                                        <li class="list-group-item fist-item">
                                            <span class="pull-right">
                                                <span class="label label-primary">
                                                    NEW
                                                </span>
                                            </span>
                                            The point of using
                                        </li>
                                        <li class="list-group-item">
                                            <span class="pull-right">
                                                <span class="label label-warning">
                                                    WAITING
                                                </span>
                                            </span>
                                            Lorem Ipsum is that it has
                                        </li>
                                        <li class="list-group-item">
                                            <span class="pull-right">
                                                <span class="label label-danger">
                                                    BLOCKED
                                                </span>
                                            </span>
                                            If you are going
                                        </li>
                                    </ul>
                                    <strong>
                                        Notes
                                    </strong>
                                    <p>
                                        Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.
                                    </p>
                                    <hr/>
                                    <strong>
                                        Timeline activity
                                    </strong>
                                    <div class="vertical-container dark-timeline" id="vertical-timeline">
                                        <div class="vertical-timeline-block">
                                            <div class="vertical-timeline-icon gray-bg">
                                                <i class="fa fa-coffee">
                                                </i>
                                            </div>
                                            <div class="vertical-timeline-content">
                                                <p>
                                                    Conference on the sales results for the previous year.
                                                </p>
                                                <span class="vertical-date small text-muted">
                                                    2:10 pm - 12.06.2014
                                                </span>
                                            </div>
                                        </div>
                                        <div class="vertical-timeline-block">
                                            <div class="vertical-timeline-icon gray-bg">
                                                <i class="fa fa-briefcase">
                                                </i>
                                            </div>
                                            <div class="vertical-timeline-content">
                                                <p>
                                                    Many desktop publishing packages and web page editors now use Lorem.
                                                </p>
                                                <span class="vertical-date small text-muted">
                                                    4:20 pm - 10.05.2014
                                                </span>
                                            </div>
                                        </div>
                                        <div class="vertical-timeline-block">
                                            <div class="vertical-timeline-icon gray-bg">
                                                <i class="fa fa-bolt">
                                                </i>
                                            </div>
                                            <div class="vertical-timeline-content">
                                                <p>
                                                    There are many variations of passages of Lorem Ipsum available.
                                                </p>
                                                <span class="vertical-date small text-muted">
                                                    06:10 pm - 11.03.2014
                                                </span>
                                            </div>
                                        </div>
                                        <div class="vertical-timeline-block">
                                            <div class="vertical-timeline-icon navy-bg">
                                                <i class="fa fa-warning">
                                                </i>
                                            </div>
                                            <div class="vertical-timeline-content">
                                                <p>
                                                    The generated Lorem Ipsum is therefore.
                                                </p>
                                                <span class="vertical-date small text-muted">
                                                    02:50 pm - 03.10.2014
                                                </span>
                                            </div>
                                        </div>
                                        <div class="vertical-timeline-block">
                                            <div class="vertical-timeline-icon gray-bg">
                                                <i class="fa fa-coffee">
                                                </i>
                                            </div>
                                            <div class="vertical-timeline-content">
                                                <p>
                                                    Conference on the sales results for the previous year.
                                                </p>
                                                <span class="vertical-date small text-muted">
                                                    2:10 pm - 12.06.2014
                                                </span>
                                            </div>
                                        </div>
                                        <div class="vertical-timeline-block">
                                            <div class="vertical-timeline-icon gray-bg">
                                                <i class="fa fa-briefcase">
                                                </i>
                                            </div>
                                            <div class="vertical-timeline-content">
                                                <p>
                                                    Many desktop publishing packages and web page editors now use Lorem.
                                                </p>
                                                <span class="vertical-date small text-muted">
                                                    4:20 pm - 10.05.2014
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="company-2">
                            <div class="m-b-lg">
                                <h2>
                                    Penatibus Consulting
                                </h2>
                                <p>
                                    There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some.
                                </p>
                                <div>
                                    <small>
                                        Active project completion with: 22%
                                    </small>
                                    <div class="progress progress-mini">
                                        <div class="progress-bar" style="width: 22%;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="client-detail">
                                <div class="full-height-scroll">
                                    <strong>
                                        Last activity
                                    </strong>
                                    <ul class="list-group clear-list">
                                        <li class="list-group-item fist-item">
                                            <span class="pull-right">
                                                <span class="label label-warning">
                                                    WAITING
                                                </span>
                                            </span>
                                            Aldus PageMaker
                                        </li>
                                        <li class="list-group-item">
                                            <span class="pull-right">
                                                <span class="label label-primary">
                                                    NEW
                                                </span>
                                            </span>
                                            Lorem Ipsum, you need to be sure
                                        </li>
                                        <li class="list-group-item">
                                            <span class="pull-right">
                                                <span class="label label-danger">
                                                    BLOCKED
                                                </span>
                                            </span>
                                            The generated Lorem Ipsum
                                        </li>
                                    </ul>
                                    <strong>
                                        Notes
                                    </strong>
                                    <p>
                                        Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.
                                    </p>
                                    <hr/>
                                    <strong>
                                        Timeline activity
                                    </strong>
                                    <div class="vertical-container dark-timeline" id="vertical-timeline">
                                        <div class="vertical-timeline-block">
                                            <div class="vertical-timeline-icon gray-bg">
                                                <i class="fa fa-coffee">
                                                </i>
                                            </div>
                                            <div class="vertical-timeline-content">
                                                <p>
                                                    Conference on the sales results for the previous year.
                                                </p>
                                                <span class="vertical-date small text-muted">
                                                    2:10 pm - 12.06.2014
                                                </span>
                                            </div>
                                        </div>
                                        <div class="vertical-timeline-block">
                                            <div class="vertical-timeline-icon gray-bg">
                                                <i class="fa fa-briefcase">
                                                </i>
                                            </div>
                                            <div class="vertical-timeline-content">
                                                <p>
                                                    Many desktop publishing packages and web page editors now use Lorem.
                                                </p>
                                                <span class="vertical-date small text-muted">
                                                    4:20 pm - 10.05.2014
                                                </span>
                                            </div>
                                        </div>
                                        <div class="vertical-timeline-block">
                                            <div class="vertical-timeline-icon gray-bg">
                                                <i class="fa fa-bolt">
                                                </i>
                                            </div>
                                            <div class="vertical-timeline-content">
                                                <p>
                                                    There are many variations of passages of Lorem Ipsum available.
                                                </p>
                                                <span class="vertical-date small text-muted">
                                                    06:10 pm - 11.03.2014
                                                </span>
                                            </div>
                                        </div>
                                        <div class="vertical-timeline-block">
                                            <div class="vertical-timeline-icon navy-bg">
                                                <i class="fa fa-warning">
                                                </i>
                                            </div>
                                            <div class="vertical-timeline-content">
                                                <p>
                                                    The generated Lorem Ipsum is therefore.
                                                </p>
                                                <span class="vertical-date small text-muted">
                                                    02:50 pm - 03.10.2014
                                                </span>
                                            </div>
                                        </div>
                                        <div class="vertical-timeline-block">
                                            <div class="vertical-timeline-icon gray-bg">
                                                <i class="fa fa-coffee">
                                                </i>
                                            </div>
                                            <div class="vertical-timeline-content">
                                                <p>
                                                    Conference on the sales results for the previous year.
                                                </p>
                                                <span class="vertical-date small text-muted">
                                                    2:10 pm - 12.06.2014
                                                </span>
                                            </div>
                                        </div>
                                        <div class="vertical-timeline-block">
                                            <div class="vertical-timeline-icon gray-bg">
                                                <i class="fa fa-briefcase">
                                                </i>
                                            </div>
                                            <div class="vertical-timeline-content">
                                                <p>
                                                    Many desktop publishing packages and web page editors now use Lorem.
                                                </p>
                                                <span class="vertical-date small text-muted">
                                                    4:20 pm - 10.05.2014
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="company-3">
                            <div class="m-b-lg">
                                <h2>
                                    Ultrices Incorporated
                                </h2>
                                <p>
                                    Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text.
                                </p>
                                <div>
                                    <small>
                                        Active project completion with: 72%
                                    </small>
                                    <div class="progress progress-mini">
                                        <div class="progress-bar" style="width: 72%;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="client-detail">
                                <div class="full-height-scroll">
                                    <strong>
                                        Last activity
                                    </strong>
                                    <ul class="list-group clear-list">
                                        <li class="list-group-item fist-item">
                                            <span class="pull-right">
                                                <span class="label label-danger">
                                                    BLOCKED
                                                </span>
                                            </span>
                                            Hidden in the middle of text
                                        </li>
                                        <li class="list-group-item">
                                            <span class="pull-right">
                                                <span class="label label-primary">
                                                    NEW
                                                </span>
                                            </span>
                                            Non-characteristic words etc.
                                        </li>
                                        <li class="list-group-item">
                                            <span class="pull-right">
                                                <span class="label label-warning">
                                                    WAITING
                                                </span>
                                            </span>
                                            Bonorum et Malorum
                                        </li>
                                    </ul>
                                    <strong>
                                        Notes
                                    </strong>
                                    <p>
                                        There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour.
                                    </p>
                                    <hr/>
                                    <strong>
                                        Timeline activity
                                    </strong>
                                    <div class="vertical-container dark-timeline" id="vertical-timeline">
                                        <div class="vertical-timeline-block">
                                            <div class="vertical-timeline-icon gray-bg">
                                                <i class="fa fa-briefcase">
                                                </i>
                                            </div>
                                            <div class="vertical-timeline-content">
                                                <p>
                                                    Many desktop publishing packages and web page editors now use Lorem.
                                                </p>
                                                <span class="vertical-date small text-muted">
                                                    4:20 pm - 10.05.2014
                                                </span>
                                            </div>
                                        </div>
                                        <div class="vertical-timeline-block">
                                            <div class="vertical-timeline-icon gray-bg">
                                                <i class="fa fa-bolt">
                                                </i>
                                            </div>
                                            <div class="vertical-timeline-content">
                                                <p>
                                                    There are many variations of passages of Lorem Ipsum available.
                                                </p>
                                                <span class="vertical-date small text-muted">
                                                    06:10 pm - 11.03.2014
                                                </span>
                                            </div>
                                        </div>
                                        <div class="vertical-timeline-block">
                                            <div class="vertical-timeline-icon navy-bg">
                                                <i class="fa fa-warning">
                                                </i>
                                            </div>
                                            <div class="vertical-timeline-content">
                                                <p>
                                                    The generated Lorem Ipsum is therefore.
                                                </p>
                                                <span class="vertical-date small text-muted">
                                                    02:50 pm - 03.10.2014
                                                </span>
                                            </div>
                                        </div>
                                        <div class="vertical-timeline-block">
                                            <div class="vertical-timeline-icon gray-bg">
                                                <i class="fa fa-coffee">
                                                </i>
                                            </div>
                                            <div class="vertical-timeline-content">
                                                <p>
                                                    Conference on the sales results for the previous year.
                                                </p>
                                                <span class="vertical-date small text-muted">
                                                    2:10 pm - 12.06.2014
                                                </span>
                                            </div>
                                        </div>
                                        <div class="vertical-timeline-block">
                                            <div class="vertical-timeline-icon gray-bg">
                                                <i class="fa fa-briefcase">
                                                </i>
                                            </div>
                                            <div class="vertical-timeline-content">
                                                <p>
                                                    Many desktop publishing packages and web page editors now use Lorem.
                                                </p>
                                                <span class="vertical-date small text-muted">
                                                    4:20 pm - 10.05.2014
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- fim col-sm-4  aluno--}}
        @include('admin.empresa.modal_empresa')
        <div class="col-sm-4">
            <div class="ibox">
                <div class="ibox-content" style=" min-height: 345px;">
                    <span class="text-muted small pull-right">
                        <a class="btn btn-primary btn-sm" style="border-radius: 4px;" data-toggle="modal" data-target="#empresa_modal">
                            <i class="fa fa-plus-square">
                            </i>
                            Nova Filial
                        </a>
                    </span>
                    <h2>
                        Filiais
                    </h2>
                    {{--
                    <div class="input-group">
                        <span class="input-group-btn">
                        </span>
                    </div>
                    --}}
                    <div class="Clientes-list" style="margin-top: 20px;">
                        <div > <!-- class="full-height-scroll" -->
                            <div class="table-responsive">
                                <table class="table table-striped table-hover ">
                                    <tbody>
                                        <tr ng-if="!empresa.children">
                                            <td colspan="2" class="text-center"><span>...</span></td>
                                        </tr>
                                        <tr ng-if="empresa.children" ng-repeat="(key, unidade) in empresa.children"  >
                                            <td>


                                                <a class="client-link" data-toggle="tab" href="#contact-1" ng-click="carrega(unidade.id)">
                                                  <i class="fa fa-exclamation" ng-if="key==0" style="color: #f8ac59; font-size: 16px" aria-hidden="true"></i>  @{{ unidade.fantasia }}
                                                </a>
                                            </td>
                                            <td class="unidades-status" style="text-align: right;">
                                                <button  class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#empresa_modal" type="button">
                                                    <i class="fa fa-edit"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  {{-- fim box filiais --}}


    {{-- inicio Locais / Ambientes--}}
    @include('admin.empresa.modal_local')
            <div class="ibox">
                <div class="ibox-content">
                    <span class="text-muted small pull-right">
                        <a class="btn btn-primary btn-sm" style="border-radius: 4px;" data-toggle="modal" data-target="#local_modal">
                            <i class="fa fa-plus-square">
                            </i>
                            Novo local
                        </a>
                    </span>
                    <h2>
                        Locais / Ambientes
                    </h2>
                    <div class="Clientes-list" style="margin-top: 20px;">
                        <div class=""> <!-- full-height-scroll -->
                            <div class="table-responsive">
                                <table class="table table-striped table-hover">
                                    <tbody>
                                        <tr ng-repeat="local in locais" ng-dblclick="carregaLocal(local)">

                                            <td>
                                                <a class="client-link">
                                                   @{{ local.nmlocal }}
                                                </a>
                                            </td>
                                            <td>
                                                @{{ local.metros }}m2
                                            </td>
                                             <td class="unidades-status" style="text-align: right;">

                                                <button class="btn btn-defaut btn-sm  btn-sm pull-right" ><i class="fa fa-trash" aria-hidden="true"></i></button>

                                                <button  class="btn btn-primary btn-sm pull-right" type="button" style="margin-right: 5px">
                                                    <i class="fa fa-edit"></i>
                                                </button>
                                            </td>

                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div> {{-- fim box locais --}}






        </div>
        {{-- fim col
    --}}
     @include('admin.empresa.modal_funcionario')

        <div class="col-sm-4">
             {{-- inicio --}}
            <div class="ibox">
                <div class="ibox-content">
                    <span class="text-muted small pull-right">
                        <a class="btn btn-primary btn-sm" style="border-radius: 4px;" data-toggle="modal" data-target="#funcionario_modal">
                            <i class="fa fa-plus-square">
                            </i>
                            Novo Funcionário
                        </a>
                    </span>
                    <h2>
                        Funcionários
                    </h2>
                    <div class="Clientes-list" style="margin-top: 20px;">
                        <div class=""> <!-- full-height-scroll -->
                            <div class="table-responsive">
                                <table class="table table-striped table-hover">
                                    <tbody>

                                        <tr ng-repeat="funcionario in funcionarios" ng-dblclick="editarFuncionario(funcionario)">
                                            <td class="client-avatar">
                                              <img alt="image" class="img-circle" src="{{ url('tema_assets') }}/img/a5.jpg" />
                                            </td>
                                            <td>
                                                <a class="client-link" data-toggle="tab" href="#contact-3">
                                                    @{{ funcionario.name }}
                                                </a>
                                            </td>
                                            <td>
                                                @{{ funcionario.phone }}
                                            </td>
                                            <td class="unidades-status" style="text-align: right;">
                                                <button class="btn btn-defaut btn-sm  btn-sm pull-right" ><i class="fa fa-trash" aria-hidden="true"></i></button>

                                                <button  class="btn btn-primary btn-sm pull-right" type="button" style="margin-right: 5px">
                                                    <i class="fa fa-edit"></i> </button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
             </div>
{{-- fim box funcionarios --}}


 {{-- inicio modalidade --}}

             <div class="ibox">
                <div class="ibox-content">
                    {{-- <span class="text-muted small pull-right">
                        <a class="btn btn-primary btn-sm" href="{{ route('admin.clientes.alunos.create') }}" style="border-radius: 4px;">
                            <i class="fa fa-plus-square">
                            </i>
                            Nova Modalidade
                        </a>
                    </span> --}}
                    <h2>
                        Modalidades
                    </h2>
                    <div class="Clientes-list" style="margin-top: 20px;">
                        <div class="full-height-scroll">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover">
                                    <tbody>

                                        <tr ng-repeat="modalidade in modalidades">
                                            <!-- <td class="client-avatar">
                                                <i class="fa fa-futbol-o" aria-hidden="true"></i>

                                            </td> -->
                                            <td>
                                                <a class="client-link" data-toggle="tab" href="#contact-3">
                                                    @{{ modalidade.nmatividade }}
                                                </a>
                                            </td>
                                            <td class="unidades-status" >

                                                <div class="switch" style="float: right;">
                                                    <div class="onoffswitch">
                                                        <input type="checkbox" ng-checked="modalidade.stmenuapp=='S'" checked class="onoffswitch-checkbox" id="@{{ modalidade.id }}" ng-click="teste1(modalidade)">
                                                            <label class="onoffswitch-label" for="@{{ modalidade.id }}">
                                                                <span class="onoffswitch-inner" ></span>
                                                                <span class="onoffswitch-switch"></span>
                                                            </label>
                                                    </div>
                                                </div>

                                            </td>

                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- fim box modalidade --}}






        </div>
        {{-- fim col--}}

    </div> {{-- fim row --}}




</div>
{{-- fim de tudo --}}
<style>
    .ibox-title h5 {
        color: #000;
    }
</style>




@endsection



@section('scripts')

{{-- aqui vai os javascritp --}}
{!! Html::script("js/empresaController.js") !!}


@endsection
