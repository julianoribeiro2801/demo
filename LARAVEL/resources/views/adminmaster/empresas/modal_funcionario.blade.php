
<div aria-hidden="true" class="modal inmodal fade" id="funcionario_modal" role="dialog" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body " ng-class="{'is-loading' : loading}">
                <div class="loading"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></div>
                <button class="close" data-dismiss="modal" type="button">
                <span aria-hidden="true">
                    ×
                </span>
                <span class="sr-only">
                    Fechar
                </span>
                </button>
                {{-- inicio contuedo--}}
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h4>
                     Funcionário
                        </h4>
                        <div class="ibox-content">
     
                            <div class="row">
                            <div class="tabs-container">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a data-toggle="tab" href="#tab-3">Informações</a></li>
                                    <li class=""><a data-toggle="tab" href="#tab-4">Endereço</a></li>
                                    
                                </ul>
                                <div class="tab-content">
                                    <div id="tab-3" class="tab-pane active">
                                        <div class="panel-body">
                                           @include('admin.funcionarios._form')

                                        </div>
                                    </div>
                                    <div id="tab-4" class="tab-pane">
                                        <div class="panel-body">
                                           @include('admin.empresa.enderecof_form')
                                        </div>
                                    </div>
                                   
                                </div>

                            </div>
                            </div>

                            <div class="row">
                                <span class="text-muted small pull-right" >
                                <a class="btn btn-primary btn-sm" style="border-radius: 4px;" ng-click="registerFuncionario()">
                                        <i class="fa fa-plus-square">
                                        </i>
                                        Cadastrar
                                        </a>
                                    </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>