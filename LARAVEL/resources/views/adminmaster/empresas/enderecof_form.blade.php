


<div>
    <div class="col-lg-6">
       <div class="form-group"> 
            {!! Form::label('CEP', 'CEP:') !!}
            <div class="input-group">
                

                <input type="text" ng-model="pessoa.cep" class="input form-control"/>
                <span class="input-group-addon hidden-xs" ng-click="consultaEndereco()"><i class="fa fa-search " aria-hidden="true"></i></span> 
            </div>
        </div>
    </div> 
    <div class="col-lg-6">
        <div class="form-group">
                {!! Form::label('Endereco', 'Endereco:') !!}
                {!! Form::text('endereco', null, ['class'=>'form-control','ng-model'=>'pessoa.endereco']) !!}
        </div>
    </div> 
    <div class="col-lg-6">
        <div class="form-group">
                {!! Form::label('Bairro', 'Bairro:') !!}
                {!! Form::text('bairro', null, ['class'=>'form-control','id' => 'bairro', 'ng-model'=>'pessoa.bairro']) !!}
        </div>
    </div>
    <div class="col-lg-6">
        <div class="form-group">
            <select class="select2_demo_1 form-control" ng-model="pessoa.estado" ng-change="teste(pessoa.estado)">
                <option  value="1">Selecione o estado</option>
                <option  ng-repeat="estado in estados" value="@{{ estado.id }}" >@{{ estado.nome }}</option>
            </select>     
        </div>
    </div> 
    <div class="col-lg-6">
        <div class="form-group">
            <select class="select2_demo_1 form-control" ng-model="pessoa.idcidade">
                <option  value="1">Selecione a cidade</option>
                <option  ng-repeat="cidade in cidades" value="@{{ cidade.id }}">@{{ cidade.nome }}</option>
            </select>            
        </div>
    </div> 

    <div class="col-lg-6">
        <div class="form-group">
                {!! Form::label('Numero', 'Numero:') !!}
                {!! Form::text('numero', null, ['class'=>'form-control','ng-model'=>'pessoa.numero']) !!}
        </div>
    </div> 
</div>
