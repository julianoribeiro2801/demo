<div aria-hidden="true" class="modal inmodal fade" id="addRede" role="dialog" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body " ng-class="{'is-loading' : loading}">
                <div class="loading"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></div>
                <button class="close" data-dismiss="modal" type="button">
                    <span aria-hidden="true">
                        ×
                    </span>
                    <span class="sr-only">
                        Fechar
                    </span>
                </button>
                {{--inicio
                    contuedo--}}
                                <form name="PostFormEmpresa" id="PostFormEmpresaUp" method="post" enctype="multipart/form-data">                    
                <div class="ibox float-e-margins">
                    
                    
                    
                    <div class="ibox-title">
                        <h4>
                            Nova Rede
                        </h4>

                        <div class="ibox-content">



                            <h4>
                                Dados do Úsuario
                            </h4>
                            <hr>

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Nome Resonsavel:</label>
                                        <input type="text" class="form-control" ng-model="empresa.name" id="name" name="name" >
                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <label>E-mail:</label>
                                        <input type="text" class="form-control" ng-model="empresa.email" name="email" >
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Celular:</label>
                                        <input type="text" class="form-control" ng-model="empresa.celular" name="name" >
                                    </div>
                                </div>

                            </div>
                            {{-- fim de meus dados --}}


                            <hr>





                            <div style="display" id="dados_empresa">

                                <h4>
                                    Dados da Empresa
                                </h4>
                                <hr>


                                    <div class="col-lg-12 text-center">
                                        <!--<div class="form-group">
                                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                                <div class="fileupload-preview thumbnail img-circle" style="width:150px;height:150px;padding:0;">
                                                    <img src="../../uploads/unidade/@{{empresa.logo}}" id="imagem-empresa" style="width:150px;height:150px;">
                                                </div>
                                                <div class="input-append">
                                                    <span class="btn btn-default btn-file" style="border:0;">
                                                        <span class="fileupload-exists"><i class="fa fa-camera" aria-hidden="true"></i></span>

                                                    </span>
                                                </div>
                                            </div>
                                        </div>-->
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Nome Fantasia:</label>
                                                <input type="text" class="form-control" ng-model="empresa.fantasia" id="fantasia" name="fantasia" ng-disabled="inactive">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <div style="display:none"><input type="text" class="form-control" ng-model="empresa.parent_id" id="matriz" name="matriz" ng-disabled="inactive"></div>
                                                <label>Razão Social:</label>
                                                <input type="text" class="form-control" ng-model="empresa.razao_social" id="razao_social" name="razao_social" ng-disabled="inactive">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>CNPJ:</label>
                                                <input type="text" class="form-control" ng-model="empresa.cnpj" id="cnpj" name="cnpj" ng-disabled="inactive">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Inscrição Estadual:</label>
                                                <input type="text" class="form-control" ng-model="empresa.inscricao_estadual" id="inscricao_estadual" name="inscricao_estadual" ng-disabled="inactive">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Endereço:</label>
                                                <input type="text" class="form-control" ng-model="empresa.endereco" id="endereco" name="endereco" ng-disabled="inactive">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Número:</label>
                                                <input type="text" class="form-control" ng-model="empresa.numero" id="numero" name="numero" ng-disabled="inactive">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>CEP:</label>
                                                <input type="text" class="form-control" ng-model="empresa.cep" id="cep" name="cep" ng-disabled="inactive">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Bairro:</label>
                                                <input type="text" class="form-control" ng-model="empresa.bairro" id="bairro" name="bairro" ng-disabled="inactive">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" ng-hide="inactive">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Estado:</label>
                                                <select ng-model="empresa.idestado" name="idestado" ng-change="changeEstado(empresa.idestado)" data-placeholder="Selecione um estado..." id="estado" tabindex="2" class="form-control">
                                                    <option value="">Selecione</option>
                                                    <option ng-repeat="estado in estados" value="@{{estado.id}}">@{{estado.nome}}/@{{estado.uf}}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Cidade:</label>
                                                <select ng-model="empresa.idcidade" name="idcidade" data-placeholder="Selecione um cidade..." id="cidade" tabindex="2" class="form-control">
                                                    <option value="">Selecione</option>
                                                    <option ng-repeat="cidade in cidades" value="@{{cidade.id}}">@{{cidade.nome}}</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" ng-show="inactive">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Estado:</label>
                                                <select class="form-control" ng-disabled="inactive">
                                                    <option value="">Selecione</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Cidade:</label>
                                                <select class="form-control" ng-disabled="inactive">
                                                    <option value="">Selecione</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label>Site:</label>
                                                <input type="text" class="form-control" ng-model="empresa.site" id="site" name="site" ng-disabled="inactive">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row loadEmpresa" style="display:none;">
                                        <div class="col-sm-12">
                                            <div class="progress progress-striped light active">
                                                <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="" aria-valuemin="0" aria-valuemax="100">
                                                    <span class="sr-only"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button class="btn btn-primary" ng-click="salvar()" type="button"> Salvar</button>
                                        <!--<button type="submit" name="SendPostForm" id="SendPostForm" value="SendPostForm" class="btn btn-primary pull-right" ng-hide="inactive"><i class="fa fa-check"></i> Salvar</button>-->
                                    </div>							
                                    <div class="clearfix"></div>
                                </form>


                            </div>

                        </div>


                    </div>
                </div>
                </form>
                    
                    
            </div>
        </div>
    </div>

</div>

<div aria-hidden="true" class="modal inmodal fade" id="editRede" role="dialog" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body " ng-class="{'is-loading' : loading}">
                <div class="loading"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></div>
                <button class="close" data-dismiss="modal" type="button">
                    <span aria-hidden="true">
                        ×
                    </span>
                    <span class="sr-only">
                        Fechar
                    </span>
                </button>
                {{--inicio
                    contuedo--}}
                                <form name="PostFormEmpresaUp" id="PostFormEmpresaUp" method="post" enctype="multipart/form-data">                    
                <div class="ibox float-e-margins">
                    
                    
                    
                    <div class="ibox-title">
                        <h4>
                            Editar Rede
                        </h4>

                        <div class="ibox-content">



                            <h4>
                                Dados do Úsuario
                            </h4>
                            <hr>

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Nome Resonsavel:</label>
                                        <input type="text" class="form-control" ng-model="empresa.name" id="name" name="name" >
                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <label>E-mail:</label>
                                        <input type="text" class="form-control" ng-model="empresa.email" name="email" >
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Celular:</label>
                                        <input type="text" class="form-control" ng-model="empresa.celular" id="celular2" name="celular2" >
                                    </div>
                                </div>
                               <!-- <div class="col-sm-6">
                                    <div class="form-group">
                                        <button style="    margin-top: 25px;" 
                                                class="btn btn-block btn-primary pull-right" ng-click="sssalvar_dados()"  onclick="$('#dados_empresa').show()" type="button"> Salvar</button>
                                    </div>
                                </div>-->

                            </div>
                            {{-- fim de meus dados --}}


                            <hr>





                            <div style="display" id="dados_empresa">

                                <h4>
                                    Dados da Empresa
                                </h4>
                                <hr>


                                    <div class="col-lg-12 text-center">
                                        <!--<div class="form-group">
                                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                                <div class="fileupload-preview thumbnail img-circle" style="width:150px;height:150px;padding:0;">
                                                    <img src="../../uploads/unidade/@{{empresa.logo}}" id="imagem-empresa" style="width:150px;height:150px;">
                                                </div>
                                                <div class="input-append">
                                                    <span class="btn btn-default btn-file" style="border:0;">
                                                        <span class="fileupload-exists"><i class="fa fa-camera" aria-hidden="true"></i></span>

                                                    </span>
                                                </div>
                                            </div>
                                        </div>-->
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Nome Fantasia:</label>
                                                <input type="text" class="form-control" ng-model="empresa.fantasia" id="fantasia" name="fantasia" ng-disabled="inactive">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <div style="display:none"><input type="text" class="form-control" ng-model="empresa.parent_id" id="matriz" name="matriz" ng-disabled="inactive"></div>
                                                <label>Razão Social:</label>
                                                <input type="text" class="form-control" ng-model="empresa.razao_social" id="razao_social" name="razao_social" ng-disabled="inactive">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>CNPJ:</label>
                                                <input type="text" class="form-control" ng-model="empresa.cnpj" id="cnpj2" name="cnpj2" ng-disabled="inactive">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Inscrição Estadual:</label>
                                                <input type="text" class="form-control" ng-model="empresa.inscricao_estadual" id="inscricao_estadual" name="inscricao_estadual" ng-disabled="inactive">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Endereço:</label>
                                                <input type="text" class="form-control" ng-model="empresa.endereco" id="endereco" name="endereco" ng-disabled="inactive">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Número:</label>
                                                <input type="text" class="form-control" ng-model="empresa.numero" id="numero" name="numero" ng-disabled="inactive">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>CEP:</label>
                                                <input type="text" class="form-control" ng-model="empresa.cep" id="cep" name="cep" ng-disabled="inactive">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Bairro:</label>
                                                <input type="text" class="form-control" ng-model="empresa.bairro" id="bairro" name="bairro" ng-disabled="inactive">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" ng-hide="inactive">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Estado:</label>
                                                <select ng-model="empresa.idestado" name="idestado" ng-change="changeEstado(empresa.idestado)" data-placeholder="Selecione um estado..." id="estado" tabindex="2" class="form-control">
                                                    <option value="">Selecione</option>
                                                    <option ng-repeat="estado in estados" ng-value="estado.id">@{{estado.nome}}/@{{estado.uf}}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Cidade:</label>
                                                <select ng-model="empresa.idcidade" name="idcidade" data-placeholder="Selecione um cidade..." id="cidade" tabindex="2" class="form-control">
                                                    <option value="">Selecione</option>
                                                    <option ng-repeat="cidade in cidades" ng-value="@{{cidade.id}}">@{{cidade.nome}}</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" ng-show="inactive">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Estado:</label>
                                                <select class="form-control" ng-disabled="inactive">
                                                    <option value="">Selecione</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Cidade:</label>
                                                <select class="form-control" ng-disabled="inactive">
                                                    <option value="">Selecione</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label>Site:</label>
                                                <input type="text" class="form-control" ng-model="empresa.site" id="site" name="site" ng-disabled="inactive">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row loadEmpresa" style="display:none;">
                                        <div class="col-sm-12">
                                            <div class="progress progress-striped light active">
                                                <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="" aria-valuemin="0" aria-valuemax="100">
                                                    <span class="sr-only"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button class="btn btn-primary" ng-click="atualizar()" type="button"> Salvar</button>
                                        <!--<button type="submit" name="SendPostForm" id="SendPostForm" value="SendPostForm" class="btn btn-primary pull-right" ng-hide="inactive"><i class="fa fa-check"></i> Salvar</button>-->
                                    </div>							
                                    <div class="clearfix"></div>
                                </form>


                            </div>

                        </div>


                    </div>
                </div>
                </form>
                    
                    
            </div>
        </div>
    </div>

</div>