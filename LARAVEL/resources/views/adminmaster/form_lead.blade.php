<!DOCTYPE html>
<html lang="pt-br" ng-app="personalApp">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PersonalClubBrasil</title>
    {!! Html::style("tema_assets/css/bootstrap.min.css") !!}
    {!! Html::style("tema_assets/font-awesome/css/font-awesome.css") !!}

    {!! Html::style("tema_assets/css/animate.css") !!}
    {!! Html::style("tema_assets/css/style.css") !!}
    {!! Html::style("css/app.css") !!}
    {!! Html::style("dist/app.css") !!}

</head>

<body class="auth-page"  ng-controller="leadController">

<style>

    .btn-primary.active, .btn-primary:active, .btn-primary:hover {
        color: #fff;
        background-color: #35b729;
        border-color: #35b729;
    }
	.auth-2 {
		padding: 15px;
    	background: #fff;
    	text-align: center;
	}

	.auth-2 h1 {
		font-size: 27px;
	    text-align: center;
	    border-bottom: 2px solid #d8d8d8;
	    margin-bottom: 11px;
	    padding-bottom: 10px;
	    font-weight: bold;
	}
	.body-message{
		text-align: left;
	}
	.btn-primary {
        color: #fff;
        background-color: #35b729;
        border-color: #35b729;
    }

    .btn-primary:hover {
        color: #fff;
        background-color: #34c726;
        border-color: #35b729;
}
</style>

<div class="middle-box  animated fadeInDown">

    <div class="auth-2">
        <div class="text-center">
            <img src="{{url('/')}}/tema_assets/img/logo_topo.png">
        </div>

        <div class="loader_home" style="margin-bottom: 20px">
            <div class="sk-spinner sk-spinner-wave">
                <div class="sk-rect1"></div>
                <div class="sk-rect2"></div>
                <div class="sk-rect3"></div>
                <div class="sk-rect4"></div>
                <div class="sk-rect5"></div>
            </div>
        </div>
        <div class="body-message">
            <h1>Baixar o Aplicativo!</h1>
           
         <!-- <p>Para </p>  -->
            <div class="row">
            	<div class="col-md-12">
            		 <form id="form-prospect" role="form" >

                            <div class="row">
										<div class="contact-message" style="text-align: center;"></div>
                                     
     <div class="col-xs-12 col-lg-12" >
            <div class="form-group"><label>Nome</label> 
            <input ng-model="prospect.name" type="text" placeholder="" id="name"  class="form-control" >
        
        </div>

        <div class="form-group">
        	<label>Email</label> 
        	<input ng-model="prospect.email" id="email"
                    type="email" placeholder=""
                    class="form-control" >
     
        </div>
     </div> 
     <!-- fim col -->
                                    
                                    
                                  
         <div class="col-xs-12 col-lg-12" >
        	  <div class="form-group"><label>Celular (WhatsApp)</label>
                <input ng-model="prospect.telefone" id="telefone" ui-br-phone-number type="text"
                       placeholder="" class="form-control telefone"  >
              
            </div>
                
        </div>
                                   

								<div class="col-xs-12 " >
                                    <div>
                                        <button ng-click="cadastrar_lead()" ng-disabled="clicado==1" class="btn btn-lg btn-primary btn-block" >
                                            <strong>INICIAR TESTE <i class="fa fa-arrow-right" aria-hidden="true"></i></strong></button>
                                    </div>
                                </div>
                              
                            <!-- fim row -->
                            </div>
                    </form>
            		
            	</div>
               
            </div>
        </div>

        <p class="m-t">
            <small style="color: #ccc">Copyright Personal Club Brasil © 2016-<?php echo date("Y")?></small>
        </p>
    </div>
</div>


<!-- Mainly scripts -->
	{!! HTML::script("https://ajax.googleapis.com/ajax/libs/angularjs/1.6.7/angular.min.js") !!}
	{!! HTML::script("tema_assets/js/jquery-2.1.1.js") !!}
	{!! HTML::script("tema_assets/js/bootstrap.min.js") !!}
	{!! HTML::script("assets_admin/vendor/jquery-form/jquery.form.min.js") !!}
	{!! Html::script("assets_admin/vendor/pnotify/pnotify.custom.js") !!}
	{!! Html::script("assets_admin/vendor/jquery-maskedinput/jquery.maskedinput.min.js") !!}


	<script type="text/javascript">


		$(document).ready(function() {
			
			$('.telefone').mask("(99) 99999-9999");
			
		});

		var app =  angular.module('personalApp', []).run(function ($rootScope, $http) {

			});

		app.controller('leadController', function ($http, $scope) {
			
                       
                    $scope.clicado=0;
		    $scope.prospect = {};

		    $scope.cadastrar_lead = function() {
                        
                        
                        $scope.clicado=1;

		    	$scope.prospect.idunidade = 3;
		    	$scope.prospect.role = 'cliente';
		    	$scope.prospect.avatar = 'user-a4.jpg';
		    	$scope.prospect.telefone = $('.telefone').val();

		    	console.log($scope.prospect);

		    if ($scope.prospect.name === undefined || $scope.prospect.name === '' ) {
               $(".contact-message").stop(true).html('<i class="fa fa-warning"></i> O campo nome é obrigatório.').css("color","#ef4b4b");
               $("input#name").focus().addClass("error");
           }  else if( $scope.prospect.email === undefined || $scope.prospect.email === ''){
               $(".contact-message").stop(true).html('<i class="fa fa-warning"></i> O campo e-mail é obrigatório.').css("color","#ef4b4b");
               $("input#email").focus().addClass("error");
           }
           else if( $scope.prospect.telefone === undefined || $scope.prospect.email === ''){
               $(".contact-message").stop(true).html('<i class="fa fa-warning"></i> O campo celular é obrigatório.').css("color","#ef4b4b");
               $("input#telefone").focus().addClass("error");
           }  else {

           		$http({
		            url: 'https://sistemapcb.com.br/cadastrar_lead',
		            data: $scope.prospect,
		            method: "POST"
		        }).then(function (response) {

		        	$scope.resposta = response.data;
		            console.log($scope.resposta);
		           

		            if($scope.resposta.status == 'success') {
		            	var id = $scope.resposta.user_id;
		            	window.location.assign("https://sistemapcb.com.br/parabens_lead/"+id);
		            }
		        }, function (response) {
		            console.log(response);

		            // console.log('Opsss... Algo deu errado!');
		        });

           		
           		
         
           }

		    	
		      
} // fim da funcition
		    
		   
		}); 

	</script>

</body>


</html>

	







