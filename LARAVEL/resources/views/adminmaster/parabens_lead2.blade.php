<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PersonalClubBrasil</title>
    {!! Html::style("tema_assets/css/bootstrap.min.css") !!}
    {!! Html::style("tema_assets/font-awesome/css/font-awesome.css") !!}

    {!! Html::style("tema_assets/css/animate.css") !!}
    {!! Html::style("tema_assets/css/style.css") !!}
    {!! Html::style("css/app.css") !!}
    {!! Html::style("dist/app.css") !!}

</head>

<body class="auth-page">

<style>
	.auth-2 {
		padding: 15px;
    	background: #fff;
    	text-align: center;
	}

	.auth-2 h1 {
		font-size: 36px;
	    text-align: center;
	    font-weight: bold;
	}
	.body-message{
		text-align: center;
	}

	p.user-name {
    
    font-size: 22px;
}
	.btn-primary {
    color: #fff;
    background-color: #1ab394;
    border-color: #1ab394;
}

	.linha {
		    margin: 0 0 16px 0;
		 border-bottom:2px solid #ff9200
	}
.btn-primary:hover {
    color: #fff;
    background-color: #11cca5;
    border-color: #13ba97;
}
</style>

<div class="middle-box  animated fadeInDown">

    <div class="auth-2">
        <div class="text-center">
            <img src="{{url('/')}}/tema_assets/img/logo_topo.png">
        </div>

        <div class="loader_home" style="margin-bottom: 20px">
            <div class="sk-spinner sk-spinner-wave">
                <div class="sk-rect1"></div>
                <div class="sk-rect2"></div>
                <div class="sk-rect3"></div>
                <div class="sk-rect4"></div>
                <div class="sk-rect5"></div>
            </div>
        </div>

        <div class="body-message">
            
            <p class="user-name"> <b>Olá {{$user->name}},</b></p>

            <hr class="linha">
           
         <!-- <p>Para </p>  -->
            <div class="row">
            	<div class="col-md-12">
            		 
            		  <p class="user-name">Agora verifique seu e-mail e <br> cadastre sua senha!</p>
					  <p>* Verifique também o spam.</p>
            		  <p> <img src="http://www.g-ops.fr/wp-content/uploads/2017/08/Mail-Send-icon.png" class="img-responsive" style="    width: 100px;
    margin: auto;" alt="">  </p>


            <!-- <div class="row">
                <div class="col-md-6">
                    <a class="link-store" href="https://play.google.com/store/apps/details?id=br.aquafit.cliques" target="_blank">
                        <img src="{{ asset('icones/google-play-badge.png') }}" class="img-responsive"  alt="">
                    </a>
                </div>
                <div class="col-md-6">
                    <a class="link-store" href="https://itunes.apple.com/us/app/personalcb/id1290866858?ls=1&mt=8" target="_blank">
                        <img src="{{ asset('icones/app-store-badge.png') }}" class="img-responsive" alt="">
                    </a>
                </div>
            </div> -->
            		
            	</div>
               
            </div>
        </div>

        <p class="m-t">
            <small style="color: #ccc">Copyright Personal Club Brasil © 2016-<?php echo date("Y")?></small>
        </p>
    </div>
</div>

<!-- Mainly scripts -->
	{!! HTML::script("tema_assets/js/jquery-2.1.1.js") !!}
	{!! HTML::script("tema_assets/js/bootstrap.min.js") !!}
	{!! HTML::script("assets_admin/vendor/jquery-form/jquery.form.min.js") !!}
	{!! Html::script("assets_admin/vendor/pnotify/pnotify.custom.js") !!}
	{!! Html::script("assets_admin/vendor/jquery-maskedinput/jquery.maskedinput.min.js") !!}


	<script type="text/javascript">
		$(document).ready(function() {
			
			$('.telefone').mask("(99) 99999-9999");
			
		});
	</script>

</body>

</html>

	







