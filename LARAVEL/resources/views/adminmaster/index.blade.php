@extends('layouts.appmaster')
@section('content')
@push('scripts')
{!! Html::script("js/adminMasterController.js") !!}
@endpush
<div class="wrapper wrapper-content" ng-controller="adminMasterController">
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox float-e-margins" style="margin-bottom: -14px;" >
				<div class="ibox-title">
					<h5>Rede de Academias</h5>
				</div>
				<div class="ibox-content">
					<div class="col-xs-12 col-md-8 pd0">
						<div class="input-group"><span class="input-group-addon"><i class="fa fa-search" aria-hidden="true"></i></span>
						<input type="text" class="form-control" id="filter"
					placeholder="Pesquisar na tabela"> </div>
				</div>
				<div class="col-xs-12 col-md-4 pd0 al-right">
					<a href="#route('admin.empresa.unidades.create')"  class="btn btn-primary" data-toggle="modal" data-target="#addRede" ng-click="novaMatriz()"><i class="fa fa-plus-square"></i> Nova Rede</a>
				</div>
				
			
				<table class="table table-hover no-margins prof" data-filter=#filter>
					<thead>
						<tr>
							<th style="width: 40px">Id</th>
							<th style="width: 30%">Fantasia</th>
							<th style="width: 18%">Responsável</th>
							<th style="width: 20%">E-mail</th>
							<th style="width: 80px">Cl.ativos</th>
							<th style="width: 10%">Status</th>
							<th style="width: 10%">Licença</th>
							<th style="width: 10%">Data</th>
							<th style="width: 150px">Ações</th>
						</tr>
					</thead>
				</thead>
				<tbody>
					<tr ng-repeat="empresa in empresas">
						<td colspan="9">
							<table style="width: 100%" >
								<tr>
									<td style="width: 40px"> @{{empresa.id}}</td>
									<td style="width: 30%">
										<i ng-show="empresa.parent_id==0" class="fa fa-folder-open" aria-hidden="true"></i>  @{{empresa.fantasia}}
										<span  style="color: #1ab394">(Matriz)</span>
										<br> Modulo Cores: 
										<span  ng-show="empresa.cores=='S'" class="label label-primary" >SIM</span>
										
										<span ng-show="empresa.cores=='N'" class="label label-danger" >NÃO</span>
									</td>
									<td style="width: 18%"> @{{empresa.name | limitTo:14}}

									</td>
									<td style="width: 20%"> @{{empresa.email | limitTo: 20}}</td>
									<td align='center' style="width: 80px"> @{{empresa.ativos}}</td>

									<td style="width: 10%; text-align: center;">
										<span ng-show="empresa.situacao==1" class="label label-primary" ng-click="mudaStatus(empresa.id,0)">Ativo</span>
										<span ng-show="empresa.situacao==0" class="label label-danger" ng-click="mudaStatus(empresa.id,1)">Bloqueado</span>
									</td>
									<td style="width: 10%; text-align: center;">
										<span ng-show="empresa.licenca=='full'" class="label label-primary" ng-click="mudaLicenca(empresa.id,'trial')">Full</span>
										<span ng-show="empresa.licenca=='trial'" class="label label-info" ng-click="mudaLicenca(empresa.id,'full')">Trial</span>
									</td>                                                                        
									<td style="width: 10%"> @{{empresa.dtcriacao | limitTo: 10}}</td>
									<td style="width: 150px; ">
										<a href="" class="btn btn-primary"  data-toggle="modal" data-target="#editRede" ng-click="getMatriz(empresa.id)"><i class="fa fa-edit"></i> </a>
										<a href="#" class="btn btn-danger " ><i class="fa fa-times-circle"></i></a>
									</td>
									
								</tr>
								
							</table>

							{{-- inicio filial --}}
							<table style="width: 100%"  >
								<tr ng-repeat="filial in empresa.filiais" >
									<td style="width: 40px"> @{{filial.id}}</td>
									<td style="width: 30%">
										<i class="fa fa-folder-open" aria-hidden="true"></i>
										@{{filial.fantasia}}
										<span  style="color: #1dabc5">(Filial)</span>
										<br> Modulo Cores: 
										<span  ng-show="filial.cores=='S'" class="label label-primary" >SIM</span>
										
										<span ng-show="filial.cores=='N'" class="label label-danger" >NÃO</span>
										
										
									</td>
									<td style="width: 18%"> @{{filial.name | limitTo:14 }}</td>
									<td style="width: 20%"> @{{filial.email | limitTo: 20}}</td>
									<td align='center' style="width: 80px"> @{{filial.ativos}}</td>
									<td style="width: 10%; text-align: center;">
										<span ng-show="filial.situacao==1" class="label label-primary" ng-click="mudaStatus(filial.id,0)">Ativo</span>
										<span ng-show="filial.situacao==0" class="label label-danger" ng-click="mudaStatus(filial.id,1)">Bloqueado</span>
									</td>
									<td style="width: 10%; text-align: center;">
										<span ng-show="filial.licenca=='full'" class="label label-primary" ng-click="mudaLicenca(empresa.id,'trial')">Full</span>
										<span ng-show="filial.licenca=='trial'" class="label label-info" ng-click="mudaLicenca(empresa.id,'full')">Trial</span>
									</td>                                                                        
                                                                        <td style="width: 10%"> @{{empresa.dtcriacao | limitTo: 10}}</td>                                                                        
									<td style="width: 100px; ">
										<a href="" class="btn btn-primary"  data-toggle="modal" data-target="#editRede" ng-click="getMatriz(filial.id)"><i class="fa fa-edit"></i> </a>
										<a href="#" class="btn btn-danger " ><i class="fa fa-times-circle"></i></a>
									</td>
									
								</tr>
								
							</table>

						</td>
					</tr>
				</tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="6">
                                            <ul class="pagination pull-right"></ul>
                                        </td>
                                    </tr>
                                </tfoot>                                
			</table>
		</div>
	</div>
</div>
</div>

@include('adminmaster.modal')
</div>
@endsection
@section('css')
{!! Html::style("assets_admin/vendor/pnotify/pnotify.custom.css") !!}
{!! Html::style("assets_admin/vendor/select2/select2.css") !!}
{!! Html::style("assets_admin/vendor/bootstrap-fileupload/bootstrap-fileupload.min.css") !!}
@endsection
@section('scripts')
{!! HTML::script("assets_admin/vendor/jquery-form/jquery.form.min.js") !!}
{!! HTML::script("assets_admin/vendor/jquery-validation/jquery.validate.js") !!}
{!! HTML::script("assets_admin/javascripts/forms/empresa.validation.js") !!}
{!! Html::script("assets_admin/vendor/pnotify/pnotify.custom.js") !!}
{!! Html::script("assets_admin/vendor/jquery-maskedinput/jquery.maskedinput.min.js") !!}
{!! Html::script("assets_admin/vendor/select2/select2.js") !!}
{!! Html::script("assets_admin/vendor/bootstrap-fileupload/bootstrap-fileupload.min.js") !!}
<script type="text/javascript">
	$(document).ready(function() {
		$('#file-input').on('change', function(evt) {
			var file = evt.target.files[0];
			if(file) {
				$('#fantasia').removeAttr("disabled");
				$('#razao_social').removeAttr("disabled");
				$('#cnpj').removeAttr("disabled");
				$('#inscricao_estadual').removeAttr("disabled");
				$('#endereco').removeAttr("disabled");
				$('#numero').removeAttr("disabled");
				$('#cep').removeAttr("disabled");
				$('#bairro').removeAttr("disabled");
				$('#telefone').removeAttr("disabled");
				$('#celular').removeAttr("disabled");
				$('#email').removeAttr("disabled");
				$('#site').removeAttr("disabled");
				$("#PostFormEmpresaUp").submit();
				$('#fantasia').attr("disabled", "disabled");
				$('#razao_social').attr("disabled", "disabled");
				$('#cnpj').attr("disabled", "disabled");
				$('#inscricao_estadual').attr("disabled", "disabled");
				$('#endereco').attr("disabled", "disabled");
				$('#numero').attr("disabled", "disabled");
				$('#cep').attr("disabled", "disabled");
				$('#bairro').attr("disabled", "disabled");
				$('#telefone').attr("disabled", "disabled");
				$('#celular').attr("disabled", "disabled");
				$('#email').attr("disabled", "disabled");
				$('#site').attr("disabled", "disabled");
			}
		});
		
		$('#cpf').mask("999.999.999-99");
		$('#cpf2').mask("999.999.999-99");
		$('#cpf3').mask("999.999.999-99");
		$('#cpf4').mask("999.999.999-99");
		$('#cpf5').mask("999.999.999-99");
		$('#cnpj').mask("99.999.999/9999-99");
		$('#cnpj2').mask("99.999.999/9999-99");
		$('#cnpj3').mask("99.999.999/9999-99");
		$('#cnpj4').mask("99.999.999/9999-99");

		$('#cep').mask("99999-999");
		$('#cep2').mask("99999-999");
		$('#cep3').mask("99999-999");
		$('#cep4').mask("99999-999");
		$('#cep5').mask("99999-999");
		$('#cep6').mask("99999-999");
		$('#cep7').mask("99999-999");
		$('#cep8').mask("99999-999");
		
		{{-- $('#telefone').mask("(99) 99999-9999"); --}}
		{{-- $('#telefone2').mask("(99) 99999-9999");
		$('#celular').mask("(99) 99999-9999");
		$('.masktelefone').mask("(99) 99999-9999");
		
		
		{{-- $('#celular').mask("(99) 99999-9999");
		$('#celular2').mask("(99) 99999-9999");
		$('#celular3').mask("(99) 99999-9999");
		$('#celular4').mask("(99) 99999-9999"); --}}
		$('.input-group .dt_nascimento').mask("99/99/9999");
		{{-- $('#data .input-group.date').datepicker({
			todayBtn: "linked",
			keyboardNavigation: false,
			forceParse: false,
			calendarWeeks: true,
			endDate: 'today',
			autoclose: true
		}); --}}
	});
</script>
@endsection