@extends('layouts.app')
@section('content')


@push('scripts')

{!! Html::script("tema_assets/js/plugins/steps/jquery.steps.min.js") !!}
{!! Html::script("tema_assets/js/plugins/validate/jquery.validate.min.js") !!}


{!! Html::script("js/passosController.js") !!}
@endpush
<!--  wizard -->
{!! Html::style("tema_assets/css/plugins/steps/jquery.steps.css") !!}

<div class="wrapper wrapper-content" ng-controller="passosParabensController">
    <div class="ibox-content">
        <h2>
            Expirado!
        </h2>
        

          

        <div class="wizard">
           

       <div class="row">
            <div class="text-center" style="margin-top: 70px">

              
                <p style="font-size: 20px">
                    Seu tempo de avaliação acabou.
                    <br>
                       Mas você ainda pode cotratar a personalização agora do Personal Club Brasil.
                    </br>
                    <br>
                     <div style="text-align: center" >
                             <font face="ARIAL" style="color: #333" size="2">   
                               Dúvidas entre em contato: <br>
                                <a href="https://api.whatsapp.com/send?phone=5545998100018&text="> +55 45 99810-0018 através do WhatsApp </a>
                                    </font>
                                </div>
                       
                </p>
					

               
            </div>
        
        </div>


        {{--  inicio do confirme seus dados --}}
       
        <hr>
            {{--  fim do confirme seus dados --}}
        </hr>
    </div>
</div>


<style>
.btn-pr {
    color: #fff;
    background-color: #35b729;
    border-color: #35b729;
}
.btn-pr:active {
    color: #fff;
    background-color: #34c726 !important;
    border-color: #35b729 !important;
}
.btn-pr:hover {
    color: #fff;
    background-color: #34c726 !important;
    border-color: #35b729 !important;
}
   
.btn-pr:focus, .btn-pr.focus {
    color: #fff;
    background-color: #35b729 !important;
    border-color: #35b729 !important;
}
</style>

<style>
    .bts_google{
            margin-left: 34%;
    }
     .bts_google a{ 
            display: inline;
            float: left;
        }

.wizard-big.wizard > .content {
    min-height: 500px;
}
</style>
@endsection
