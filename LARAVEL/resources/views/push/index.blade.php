@extends('layouts.app')

@section('content')

 <div class="wrapper wrapper-content animated fadeInRight">
   <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Listagem de {{ $headers['title'] }} com paginação, ordernação e filtro</h5>

                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                               
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <div class="col-xs-12 col-md-8 pd0">
                                <div class="input-group"><span class="input-group-addon"><i class="fa fa-search" aria-hidden="true"></i></span> 
                                <input type="text" class="form-control" id="filter"
                           placeholder="Pesquisar na tabela"> </div>
                            </div>

                            <div class="col-xs-12 col-md-4 pd0 al-right">
                               <a href="{{ route('admin.clientes.mensagens.create') }}" class="btn btn-primary"><i class="fa fa-plus-square"></i> Nova Mensagem</a>
                            </div>
                             
                            
                            
                            

                            <table class="footable table table-stripped" data-page-size="8" data-filter=#filter>
                                <thead>
                                <tr>
                                   <th>Id</th>
                                    <th>Cliente </th>

									<th>Tipo </th>
                                    <th>Lida</th>

									<th>Ação</th>
                                </tr>
                                </thead>
                                <tbody>

                               @foreach ($mensagens as $mensagem)
			<tr>
                <td> {{$mensagem->id}}</td>
                <td>{{ $mensagem->dsmensagem }}</td>
			     <td> {{$mensagem->idtipomensagem}}</td>
				<td> {{$mensagem->stlida}}</td>
              
				<td> 
				<a href="{{route('admin.clientes.mensagens.edit', $mensagem->id)}}"  alt="Editar" class="btn btn-info" ><i class="fa fa-paste"></i> </a>
<a href="#" class="btn btn-danger " alt="Deletar"  ><i class="fa fa-times-circle"></i></a>

</td>
				
			</tr>
			@endforeach
                               

                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="5">
                                        <ul class="pagination pull-right"></ul>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>




@endsection


@section('scripts')

 <!-- FooTable -->
{!! Html::script("tema_assets/js/plugins/footable/footable.all.min.js") !!}
   
    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function() {

            $('.footable').footable();
            // $('.footable2').footable();

        });

    </script>

@endsection



