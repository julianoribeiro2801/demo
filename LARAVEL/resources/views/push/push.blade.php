@extends('layouts.app')

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">

     {{-- inicio bloco 1 --}}
        <div class="col-sm-12 col-md-6">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>
                       Enviar Push quando?
                       
                    </h5>
                    <div class="ibox-tools">
                        
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="tooltip-demo">
                	   <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <!-- <th>Qtde</th> -->
                                    <th>Quando</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($push_types as $push_type)
                                <tr>
                                    <td>{{ $push_type->id }}</td>
                                    <!-- <td>0</td> -->
                                    <td >
                                    <span data-toggle="tooltip" data-placement="top" title="{{ $push_type->tooltip }}"> {{ $push_type->quando }}</span>
                                   </td>
                                      <td>
                                        <div class="switch">
                                            <div class="onoffswitch">
                                                <input type="checkbox" checked class="onoffswitch-checkbox" id="type{{ $push_type->id }}" >
                                                <label class="onoffswitch-label" for="type{{ $push_type->id }}">
                                                    <span class="onoffswitch-inner" ></span>
                                                    <span class="onoffswitch-switch"></span>
                                                </label>
                                            </div>
                                        </div>
                                        {{-- <span class="label label-primary">Editar</span> --}}
                                         {{-- <span class="label label-danger">Desativar</span> --}}
                                    </td>
                                   
                                </tr>
                                @endforeach
                                
                                </tbody>
                            </table>
                        </div>
                   
                   
                </div>
            </div>
        </div>
    

    <div class="col-sm-12 col-md-6">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>
                       Enviar Medalha quando?
                       
                    </h5>
                    <div class="ibox-tools">
                        
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="tooltip-demo">
                       <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>#</th>
                                    <th>Quando</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($medalhas as $push_type)
                                <tr>
                                    <td>
                                        <img src="http://www.envolverde.com.br/wp-content/uploads/2014/11/medalha.png" alt="" width="60">
                                    </td>

                                    <td>{{ $push_type->id }}</td>
                                    
                                    <td >
                                    <span data-toggle="tooltip" data-placement="top" title="{{ $push_type->tooltip }}"> {{ $push_type->quando }}</span>
                                   </td>
                                    
                                   
                                </tr>
                                @endforeach
                                
                                </tbody>
                            </table>
                        </div>
                   
                   
                </div>
            </div>
        </div>

    {{-- INICIO Relatório de push enviados --}}

      {{--   <div class="col-sm-12 col-md-6">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>
                       Relatório de push enviados
                       
                    </h5>
                    <div class="ibox-tools">
                        
                    </div>
                </div>
                <div class="ibox-content">

                       <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Data</th>
                                    <th>Quando</th>
                                    <th>Qtde</th>

                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>01/02/2018</td>
                                    <td>Boas vindas! (Matrícula)</td>
                                    <td>94</td>
                                   
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>01/02/2018</td>
                                    <td>Troca de treino</td>
                                    <td>129</td>
                                  
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>01/02/2018</td>
                                    <td>Treino atualizado</td>
                                    <td>20</td>
                                   
                                </tr>

                                 <tr>
                                    <td>4</td>
                                    <td>03/02/2018</td>
                                    <td>Avaliação física</td>
                                    <td>20</td>
                                   
                                </tr>

                                 <tr>
                                    <td>5</td>
                                    <td>03/02/2018</td>
                                    <td>Agendamento personalizado</td>
                                   <td>20</td>
                                   
                                </tr>

                                 <tr>
                                    <td>5</td>
                                    <td>03/02/2018</td>
                                    <td>Confirmação de aula reservada</td>
                                   <td>20</td>
                                   
                                </tr>
                                </tbody>
                            </table>
                   
                   
                </div>
            </div>
        </div>
     --}}
    </div>
   

{{-- BACKUP --}}
  {{--  <div class="row">

      <div class="col-sm-12 col-md-6">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>
                       Criar modelo de Push
                        <small>
                           preencha os campos abaixo.
                        </small>
                    </h5>
                    <div class="ibox-tools">
                        
                    </div>
                </div>
                <div class="ibox-content">

                     <div class="form-group">
                        <label>
                            Remetente 
                        </label>
                        <div class="input-group" style="width:100%">
                            <select class="form-control" data-placeholder="Selecione o tipo...">
                                <option ng-value="" disabled >
                                    Selecione 
                                </option>
                               
                                <option selected value="1"> Academia Aquafit </option>
                                <option value="1"> Adriano Teste</option>
                                 <option value="1"> Jhon Deo </option>
                                 <option value="1">Karsten </option>
                                 <option value="1"> Antonio Fagundes  </option>
                                
                               
                            </select>
                        </div>
                    </div>
    
   
                     <div class="form-group">
                        <label>
                            Enviar quando
                        </label>
                        <div class="input-group" style="width:100%">
                            <select class="form-control" 
                            onchange="$('.alert-success').show()" >
                                <option  value="">
                                    Selecione quando
                                </option>
                               
                                 <option   value="1"> Boas vindas (Matrícula) </option>
                                 <option value="1"> Troca de treino </option>
                                 <option value="1"> Treino atualizado</option>
                                 <option value="1"> Avaliação física </option>
                                 <option value="1"> Agendamento personalizado </option>
                                 <option value="1"> Confirmação de aula reservada </option>
                               
                            </select>
                        </div>
                    </div>


                  <div class="alert alert-success" style="display:none">
                                <button aria-hidden="true" onclick ="$('.alert-success').hide()" class="close" type="button">×</button>
                                Existem 40 alunos que receberão esse <a class="alert-link" href="#">Push</a>.
                            </div>

                    <div class="form-group">
                        <label>
                            Título
                        </label>
                        <input class="form-control" placeholder="" 
                        value="Boas vindas (Matrícula)" type="text">
                        </input>
                    </div>
                    <div class="form-group">
                        <label>
                            Mensagem
                        </label>
                        <textarea class="form-control" 
                        placeholder="Mensagem" rows="3">Olá *Aluno* seja bem vindo a Aquafit
                        </textarea>
                    </div>
                   
                    <div>
                        <button class="btn btn-md btn-primary pull-right m-t-n-xs" type="submit">
                            ENVIAR <i class="fa fa-arrow-right"></i>
                        </button>
                        <div style="clear: both;"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-12 col-md-6">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>
                       Enviar Push - (em desenvolvimento).
                        <small>
                           preencha os campos abaixo.
                        </small>
                    </h5>
                    <div class="ibox-tools">
                        
                    </div>
                </div>
                <div class="ibox-content">

                     <div class="form-group">
                        <label>
                            Segmentação
                        </label>
                        <div class="input-group" style="width:100%">
                            <select class="form-control" data-placeholder="Selecione o tipo...">
                                <option ng-value="">
                                    Selecione
                                </option>
                                <option>
                                    Cliente
                                </option>
                                <option> Masculino </option>
                                <option> Feminino </option>
                                <option> Audiência GFM</option>
                                <option> Audiência SPM</option>

                                <option>Inativo a mais de 1 mês </option>
                            </select>
                        </div>
                    </div>


                     <div class="form-group" style="margin-left: 25px">
                        <label>
                           <i class="fa fa-level-down" aria-hidden="true"></i> Sub. Segmentação
                        </label>
                        <div class="input-group" style="width:100%">
                            <select class="form-control" data-placeholder="Selecione o tipo...">
                                <option ng-value="">
                                    Selecione Audiência
                                </option>                              
                                <option> Aulas de Jump </option>
                                <option> Aulas de Zumba </option>                               
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>
                            Título
                        </label>
                        <input class="form-control" placeholder="" type="text">
                        </input>
                    </div>
                    <div class="form-group">
                        <label>
                            Mensagem
                        </label>
                        <textarea class="form-control" placeholder="Mensagem" rows="3"></textarea>
                    </div>
                   
                    <div>
                        <button class="btn btn-sm btn-primary pull-right m-t-n-xs" type="submit">
                            Salvar
                        </button>
                        <div style="clear: both;"></div>
                    </div>
                </div>
            </div>
        </div>

    

       
   </div>  --}}
    {{-- fim linha --}}




  

</div>

 @endsection