@extends('layouts.app')

@section('content')

<div class="container">
	<h3> Editando Categoria: {{$category->name}} </h3>
	
	 @include('errors._check')

	{!!  Form::model($category, array('route' => array('admin.empresa.categories.update', $category->id))) !!}
   	
    @include('admin.categories._form')

   	<div class="form-group">   		
   		<button class="btn btn-primary " type="submit"><i class="fa fa-check"></i>&nbsp;Salvar</button> 
   	</div>
	
	{!! Form::close() !!}

	
	
</div>

@endsection

