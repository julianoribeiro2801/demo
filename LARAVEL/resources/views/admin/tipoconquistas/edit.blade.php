@extends('layouts.app')

@section('content')

<div class="container">
	<h3> Editando tipo conquista: {{$tipoconquista->name}} </h3>
	
	 @include('errors._check')
	
	{!!  Form::model($tipoconquista, array('route' => array('admin.clientes.tipoconquistas.update', $tipoconquista->id))) !!}
   	
    @include('admin.tipoconquistas._form')

   	<div class="form-group">   		
   		<button class="btn btn-primary " type="submit"><i class="fa fa-check"></i>&nbsp;Salvar</button> 
   	</div>
	
	{!! Form::close() !!}
	
	
</div>

@endsection

