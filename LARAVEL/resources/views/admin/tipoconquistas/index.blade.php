@extends('layouts.app')

@section('content')

<div class="container">
	<h3> Tipo conquistas</h3>
	<br>
	<a href="{{ route('admin.clientes.tipoconquistas.create') }}" class="btn btn-primary"> Novo tipo de conquista</a>

	<br><br>
	<table class="table table-bordered">
		<thead>
			<tr>
				<th>Id</th>
				<th>Nome</th>
				<th>Tipo de medida</th>
				<th>Ação</th>
			</tr>
		</thead>
		<tbody>
		@foreach ($tipoconquistas as $tipoconquista)
			<tr>
				<td> {{$tipoconquista->id}}</td>
				<td> {{$tipoconquista->dstipoconquista}}</td>
				<th> {{$tipoconquista->tpmedida}}</th>

				<td> <a href="{{route('admin.clientes.tipoconquistas.edit', $tipoconquista->id)}}" class="btn btn-info">Editar</a></td>
				
			</tr>
			@endforeach
		</tbody>
	</table>

	{{-- {{{ $tipoconquistas->render()}}} --}}

	
</div>

@endsection

