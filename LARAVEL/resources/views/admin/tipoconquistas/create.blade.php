@extends('layouts.app')

@section('content')

<div class="container">
	<h3> Novo tipo de categoria</h3>
	
	{{-- @include('errors._check') --}}

	{!! Form::open(array('route' => 'admin.clientes.tipoconquistas.store', 'class'=>'form')) !!}
   
    @include('admin.tipoconquistas._form')

   	<div class="form-group">   		
   	{!! Form::submit('Criar ', ['class'=>'btn btn-primary']) !!}
   	</div>
	
	{!! Form::close() !!}

	
	
</div>

@endsection

