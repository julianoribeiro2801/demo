@extends('layouts.app')

@section('content')

<div class="container">
	<h3> Editando programa: {{$programa->nmgrupo}} </h3>
	
	 @include('errors._check')
	
	{!!  Form::model($programa, array('route' => array('admin.empresa.programas.update', $programa->id))) !!}
   	
    @include('admin.programas._form')

   	<div class="form-group">   		
   		<button class="btn btn-primary " type="submit"><i class="fa fa-check"></i>&nbsp;Salvar</button> 
   	</div>
	
	{!! Form::close() !!}
	
	
</div>

@endsection

