@extends('layouts.app')

@section('content')

<div class="container">
	<h3> Programas</h3>
	<br>
	<a href="{{ route('admin.empresa.programas.create') }}" class="btn btn-primary"> Novo programa</a>

	<br><br>
	<table class="table table-bordered">
		<thead>
			<tr>
				<th>Id</th>
				<th>Nome</th>
				<th>Ação</th>
			</tr>
		</thead>
		<tbody>
		@foreach ($programas as $programa)
			<tr>
				<td> {{$programa->id}}</td>
				<td> {{$programa->nmprograma}}</td>

				<td> <a href="{{route('admin.empresa.programas.edit', $programa->id)}}" class="btn btn-info">Editar</a></td>
				
			</tr>
			@endforeach
		</tbody>
	</table>

	{{-- {{{ $programas->render()}}} --}}

	
</div>

@endsection

