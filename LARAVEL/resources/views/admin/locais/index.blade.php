@extends('layouts.app')

@section('content')

<div class="container">
	<h3>Locais</h3>
	<br>
	<a href="{{ route('admin.empresa.locais.create') }}" class="btn btn-primary"> Novo local</a>

	<br><br>
	<table class="table table-bordered">
		<thead>
			<tr>
				<th>Id</th>
				<th>Nome</th>
				<th>Ação</th>
			</tr>
		</thead>
		<tbody>
		@foreach ($locais as $local)
			<tr>
				<td> {{$local->id}}</td>
				<td> {{$local->nmlocal}}</td>

				<td> <a href="{{route('admin.empresa.locais.edit', $local->id)}}" class="btn btn-info">Editar</a></td>
				
			</tr>
			@endforeach
		</tbody>
	</table>

	{{-- {{{ $locais->render()}}} --}}

	
</div>

@endsection

