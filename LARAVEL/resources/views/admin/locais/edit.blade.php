@extends('layouts.app')

@section('content')

<div class="container">
	<h3> Editando local: {{$local->nmgrupo}} </h3>
	
	 @include('errors._check')
	
	{!!  Form::model($local, array('route' => array('admin.empresa.locais.update', $local->id))) !!}
   	
    @include('admin.locais._form')

   	<div class="form-group">   		
   		<button class="btn btn-primary " type="submit"><i class="fa fa-check"></i>&nbsp;Salvar</button> 
   	</div>
	
	{!! Form::close() !!}
	
	
</div>

@endsection

