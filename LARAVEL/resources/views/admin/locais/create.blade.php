@extends('layouts.app')

@section('content')

<div class="container">
	<h3> Novo local</h3>
	
	{{-- @include('errors._check') --}}

	{!! Form::open(array('route' => 'admin.empresa.locais.store', 'class'=>'form')) !!}
   
    @include('admin.locais._form')

   	<div class="form-group">   		
   	{!! Form::submit('Criar ', ['class'=>'btn btn-primary']) !!}
   	</div>
	
	{!! Form::close() !!}

	
	
</div>

@endsection

