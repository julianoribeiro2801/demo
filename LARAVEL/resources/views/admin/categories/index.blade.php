@extends('layouts.app')

@section('content')

<div class="container">
	<h3> Novo usuário</h3>
	<br>
	<a href="{{ route('admin.categories.create') }}" class="btn btn-primary"> Nova usuário</a>

	<br><br>
	<table class="table table-bordered">
		<thead>
			<tr>
				<th>Id</th>
				<th>Nome</th>
				<th>Email</th>
				<th>Ação</th>
			</tr>
		</thead>
		<tbody>
		@foreach ($categories as $categorie)
			<tr>
				<td> {{$categorie->id}}</td>
				<td> {{$categorie->name}}</td>
				<th>{{$categorie->email}}</th>

				<td> <a href="{{route('admin.categories.edit', $categorie->id)}}" class="btn btn-info">Editar</a></td>
				
			</tr>
			@endforeach
		</tbody>
	</table>

	{{-- {{{ $categories->render()}}} --}}

	
</div>

@endsection

