<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Dashboard | Painel Administrativo</title>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        {!! Html::style("dist/app.css") !!}
        {!! Html::style("assets/font-awesome/css/font-awesome.css") !!}
        {!! Html::style("assets/css/animate.css") !!}
        {!! Html::style("assets/css/style.css") !!}
        {!! Html::style("css/dist/app.css") !!}
        {{-- FAVICON --}}
            <link rel="icon" href="{{ URL::to('assets/img/favicon-32x32.png') }}" />
            <link rel="icon" href="{{ URL::to('assets/img/favicon-180x180.png') }}" />
        {{-- FECHA FAVICON --}}
    </head>
    <body>
        <div id="wrapper">
            <!-- MENU -->
            <nav class="navbar-default navbar-static-side" role="navigation">
                <div class="sidebar-collapse">
                    <ul class="nav metismenu" id="side-menu">
                        <li class="nav-header">
                            <div class="dropdown profile-element">
                                <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                    <span class="clear"><span class="block m-t-xs">
                                        <strong class="font-bold">{{ Auth::user()->name }}</strong>
                                    </span>
                                    <span class="text-muted text-xs block">Aquafit <b class="caret"></b></span></span>
                                </a>
                                <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                    <li><a href="{{ url('/logout') }}">Sair</a></li>
                                </ul>
                            </div>
                            <div class="logo-element">IN+</div>
                        </li>
                        <li>
                            <a href="{{ url('/') }}/painel"><i class="fa fa-dashboard"></i>
                                <span class="nav-label">Dashboard</span>
                            </a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-male"></i> <span class="nav-label">Grupos Musculares</span><span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level collapse">
                                <li><a href="#">Novo Grupo</a></li>
                                <li><a href="#">Gerir Grupos</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-binoculars"></i> <span class="nav-label">Objetivos</span><span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level collapse">
                                <li><a href="#">Novo Objetivo</a></li>
                                <li><a href="#">Gerir Objetivos</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-list-ol"></i> <span class="nav-label">Exercícios</span><span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level collapse">
                                <li><a href="#">Novo Exercício</a></li>
                                <li><a href="#">Gerir Exercícios</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-list-alt"></i> <span class="nav-label">Treinos</span><span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level collapse">
                                <li><a href="#">Novo Treino</a></li>
                                <li><a href="#">Gerir Treinos</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-graduation-cap"></i> <span class="nav-label">Professores</span><span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level collapse">
                                <li><a href="#">Novo Professor</a></li>
                                <li><a href="#">Gerir Professores</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-users"></i> <span class="nav-label">Alunos</span><span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level collapse">
                                <li><a href="#">Novo Aluno</a></li>
                                <li><a href="#">Gerir Alunos</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-building-o"></i> <span class="nav-label">Prescrições</span><span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level collapse">
                                <li><a href="#">Nova Prescrição</a></li>
                                <li><a href="#">Gerir Prescrições</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-building-o"></i>
                                <span class="nav-label">Ficha de Treino</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
            <!-- FECHA MENU -->
            <div id="page-wrapper" class="gray-bg">
                <!-- MENU HEADER -->
                <div class="row border-bottom">
                    <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom:0;">
                        <div class="navbar-header">
                            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary" href="#"><i class="fa fa-bars"></i></a>
                            <form role="search" class="navbar-form-custom" method="post" action="#">
                                <div class="form-group">
                                    <input type="text" placeholder="Pesquisa..." class="form-control" name="top-search" id="top-search">
                                </div>
                            </form>
                        </div>
                        <ul class="nav navbar-top-links navbar-right">
                            <li><a href="painel.php?logoff=true"><i class="fa fa-sign-out"></i> Sair</a></li>
                        </ul>
                    </nav>
                </div>
                <!-- FECHA MENU HEADER -->

                <!-- CONTEUDO -->
                @yield('content')
                <!-- FECHA CONTEUDO -->

                <!-- FOOTER -->
                <div class="footer">
                    <div class="pull-right"><strong>Personal Club Brasil</strong></div>
                    <div>&copy; Copyright <?= date('Y') ?>. Todos os direitos reservados.</div>
                </div>
                <!-- FECHA FOOTER -->
            </div>
        </div>
        <!-- ESTILOS ESPECÍFICOS -->
        @yield('css')
        <!-- Mainly scripts -->
        {!! Html::script('js/app.js') !!}
        {!! Html::script("assets/js/plugins/metisMenu/jquery.metisMenu.js" !!}
        {!! Html::script("assets/js/plugins/slimscroll/jquery.slimscroll.min.js" !!}
        <!-- Custom and plugin javascript -->
        {!! Html::script("assets/js/inspinia.js" !!}
        {!! Html::script("assets/js/plugins/pace/pace.min.js" !!}
        <!-- SCRIPTS ESPECÍFICOS -->
        @yield('script')
        <!-- SCRIPTS ESPECÍFICOS -->
        @yield('exeScript')
    </body>
</html>