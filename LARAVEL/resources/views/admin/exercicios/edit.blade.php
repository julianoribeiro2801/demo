@extends('layouts.app')

@section('content')

<div class="container">
	<h3> Editando exercicio: {{$exercicio->nmexercicio}} </h3>
	
	 @include('errors._check')
	
	{!!  Form::model($exercicio, array('route' => array('admin.clientes.exercicios.update', $exercicio->id))) !!}
   	
    @include('admin.exercicios._form')

   	<div class="form-group">   		
   		<button class="btn btn-primary " type="submit"><i class="fa fa-check"></i>&nbsp;Salvar</button> 
   	</div>
	
	{!! Form::close() !!}
	
	
</div>

@endsection

