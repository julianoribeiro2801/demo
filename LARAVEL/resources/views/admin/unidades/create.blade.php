@extends('layouts.app')

@section('content')


	 <div class="row" style="margin-top: 20px">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5> Novo Unidade </h5>
               
            </div>
            <div class="ibox-content">
                
	
                @include('errors._check') 

               {!! Form::open(array('route' => 'admin.empresa.unidades.store', 'class'=>'form')) !!}

                @include('admin.unidades._form')

                <div class="form-group">   		
                <button class="btn btn-primary " type="submit"><i class="fa fa-check"></i>&nbsp;Criar</button> 
                </div>
	
	{!! Form::close() !!}	
            </div>
        </div>
    </div>
</div>
	



@endsection







