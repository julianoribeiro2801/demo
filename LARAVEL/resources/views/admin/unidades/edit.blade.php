@extends('layouts.app')

@section('content')


	 <div class="row" style="margin-top: 20px">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h3> Editando tipo unidade: {{$unidade->nmpessoa}} </h3>
               
            </div>
            <div class="ibox-content">
                
	
               
	
	 @include('errors._check')
	
	{!!  Form::model($unidade, array('route' => array('admin.empresa.unidades.update', $unidade->id))) !!}
   	
    @include('admin.unidades._form')

   	<div class="form-group">   		
   		<button class="btn btn-primary " type="submit"><i class="fa fa-check"></i>&nbsp;Salvar</button> 
   	</div>
	
	{!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
	



@endsection













