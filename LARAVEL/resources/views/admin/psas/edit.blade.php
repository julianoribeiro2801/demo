@extends('layouts.app')

@section('content')

<div class="container">
	<h3> Editando psa: {{$psa->nmgrupo}} </h3>
	
	 @include('errors._check')
	
	{!!  Form::model($psa, array('route' => array('admin.clientes.locais.update', $psa->id))) !!}
   	
    @include('admin.locais._form')

   	<div class="form-group">   		
   		{!! Form::submit('Salvar', ['class'=>'btn btn-primary']) !!}
   	</div>
	
	{!! Form::close() !!}
	
	
</div>

@endsection

