@extends('layouts.app')

@section('content')




<a href="{{ route('admin.clientes.psas.create') }}" class="btn btn-primary"> Novo psa</a>
<div ng-controller="psaController">
  <table class="table table-bordered table-hover table-condensed">
	<tr style="font-weight: bold">
	  <td style="width:15%">Domingo</td>
	  <td style="width:15%">Segunda</td>
	  <td style="width:15%">Terça</td>
	  <td style="width:15%">Quarta</td>
	  <td style="width:15%">Quinta</td>
	  <td style="width:15%">Sexta</td>
	  <td style="width:15%">Sábado</td>
	  <td style="width:15%">Edit</td>
	</tr>
	<tr ng-repeat="user in users">
	  <td>
		<!-- editable username (text with validation) -->
		<span editable-text="users.name" e-name="name" e-form="rowform" onbeforesave="checkName($data, user.id)">
			 <select class="form-control" ng-model="psa.programa">
			 <option value="">Selecione o programa </option>
				@foreach ($programas as $programa)
				<option value="{{$programa->id}}">
					{{$programa->nmprograma}}
				</option>
				@endforeach
			</select>  
			<input type="text" class="form-control" value="  @{{ showStatus(user) }}" ng-model="psa.horario">
		  
		</span>
	  </td>
	  <td>
		<!-- editable username (text with validation) -->
		<span editable-text="users.name" e-name="name" e-form="rowform" onbeforesave="checkName($data, user.id)">
			 <select class="form-control" ng-model="psa.programa">
			 <option value="">Selecione o programa </option>
				@foreach ($programas as $programa)
				<option value="{{$programa->id}}">
					{{$programa->nmprograma}}
				</option>
				@endforeach
			</select>  
			<input type="text" class="form-control" value="  @{{ showStatus(user) }}" ng-model="psa.horario">
		  
		</span>
	  </td>
	  <td>
		<!-- editable username (text with validation) -->
		<span editable-text="users.name" e-name="name" e-form="rowform" onbeforesave="checkName($data, user.id)">
			 <select class="form-control" ng-model="psa.programa">
			 <option value="">Selecione o programa </option>
				@foreach ($programas as $programa)
				<option value="{{$programa->id}}">
					{{$programa->nmprograma}}
				</option>
				@endforeach
			</select>  
			<input type="text" class="form-control" value="  @{{ showStatus(user) }}" ng-model="psa.horario">
		  
		</span>
	  </td>
	  <td>
		<!-- editable username (text with validation) -->
		<span editable-text="users.name" e-name="name" e-form="rowform" onbeforesave="checkName($data, user.id)">
			 <select class="form-control" ng-model="psa.programa">
			 <option value="">Selecione o programa </option>
				@foreach ($programas as $programa)
				<option value="{{$programa->id}}">
					{{$programa->nmprograma}}
				</option>
				@endforeach
			</select>  
			<input type="text" class="form-control" value="  @{{ showStatus(user) }}" ng-model="psa.horario">
		  
		</span>
	  </td>
	  <td>
		<!-- editable username (text with validation) -->
		<span editable-text="users.name" e-name="name" e-form="rowform" onbeforesave="checkName($data, user.id)">
			 <select class="form-control" ng-model="psa.programa">
			 <option value="">Selecione o programa </option>
				@foreach ($programas as $programa)
				<option value="{{$programa->id}}">
					{{$programa->nmprograma}}
				</option>
				@endforeach
			</select>  
			<input type="text" class="form-control" value="  @{{ showStatus(user) }}" ng-model="psa.horario">
		  
		</span>
	  </td>
	  <td>
		<!-- editable username (text with validation) -->
		<span editable-text="users.name" e-name="name" e-form="rowform" onbeforesave="checkName($data, user.id)">
			 <select class="form-control" ng-model="psa.programa">
			 <option value="">Selecione o programa </option>
				@foreach ($programas as $programa)
				<option value="{{$programa->id}}">
					{{$programa->nmprograma}}
				</option>
				@endforeach
			</select>  
			<input type="text" class="form-control" value="  @{{ showStatus(user) }}" ng-model="psa.horario">
		  
		</span>
	  </td>
	  <td>
		<!-- editable username (text with validation) -->
		<span editable-text="users.name" e-name="name" e-form="rowform" onbeforesave="checkName($data, user.id)">
			 <select class="form-control" ng-model="psa.programa">
			 <option value="">Selecione o programa </option>
				@foreach ($programas as $programa)
				<option value="{{$programa->id}}">
					{{$programa->nmprograma}}
				</option>
				@endforeach
			</select>  
			<input type="text" class="form-control" value="  @{{ showStatus(user) }}" ng-model="psa.horario">
		  
		</span>
	  </td>

	  <td style="white-space: nowrap">
		<!-- form -->
		<form editable-form name="rowform" onbeforesave="saveUser($data, user.id)" ng-show="rowform.$visible" class="form-buttons form-inline" shown="inserted == user">
		  <button type="submit" ng-disabled="rowform.$waiting" class="btn btn-primary">
			save
		  </button>
		  <button type="button" ng-disabled="rowform.$waiting" ng-click="rowform.$cancel()" class="btn btn-default">
			cancel
		  </button>
		</form>
		<div class="buttons" ng-show="!rowform.$visible">
		  <button type="button" class="btn btn-primary" ng-click="rowform.$show()">edit</button>
		  <button type="button" class="btn btn-danger" ng-click="removeUser($index)">del</button>
		</div>  
	  </td>
	</tr>
  </table>

  <button type="button" class="btn btn-default" ng-click="addUser()">Add row</button>
</div>
@endsection


