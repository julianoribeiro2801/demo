@extends('layouts.app')

@section('content')
@push('scripts')
{!! Html::script("js/psaController.js") !!}

@endpush


<div class="container" ng-controller="psaController">  


 </script>   
        <div class="wrapper wrapper-content animated fadeInRight ecommerce">

            <div class="row">
                <div class="col-lg-12">
                    <div class="tabs-container">
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#tab-1">Psa</a></li>
                            </ul>
                            <div class="tab-content">
                                <div id="tab-1" class="tab-pane active">
                                    <div class="panel-body">

                                 <div class="form-group">
                                    
                                    
                                    <div class="input-group">
<!--                                        <input ng-model="prescricao.aluno" type="text"
                                            data-provide="typeahead"
                                            placeholder="Aluno"
                                            data-source='["item 1","item 2","item 3"]'
                                            class="form-control" />-->                 
                                        <select ng-model="psa.aluno" class="form-control">
                                            <option value="">Selecione o aluno</option>
                                          @foreach ($alunos as $aluno)
                                            <option value="{{$aluno->id}}">{{$aluno->name}}</option>
                                          @endforeach                       
                                        </select>                                            
                                        <span class="input-group-btn"> 
                                           <button type="button" class="btn btn-primary">+</button> 
                                            </span> 


                                    </div>
                                </div>
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            
<!--   inicio tabelas inserção-->
  <table class="table table-bordered table-hover table-condensed">
    <tr style="font-weight: bold">
      <td style="width:15%">Domingo</td>
      <td style="width:15%">Segunda</td>
      <td style="width:15%">Terça</td>
      <td style="width:15%">Quarta</td>
      <td style="width:15%">Quinta</td>
      <td style="width:15%">Sexta</td>
      <td style="width:15%">Sábado</td>
      <td style="width:15%">Edit</td>
    </tr>

    
    <tr ng-repeat="domingo in domingos">
      <td>
        <span editable-text="psa.horariodom" e-name="name">
             <select class="form-control" ng-model="domingos[$index].idatividade">
             <option value="">Selecione o programa </option>
             
                @foreach ($programas as $programa)
                <option value="{{$programa->id}}">
                    {{$programa->nmprograma}}
                </option>
                @endforeach
            </select>  
        
            <input type="text" class="form-control"  ng-model="domingos[$index].hratividade">
        </span>
      </td>
      
      <td>
        <span editable-text="users.name"  >
             <select class="form-control" ng-model="segunda.idatividade">
             <option value="">Selecione o programa </option>
                @foreach ($programas as $programa)
                <option value="{{$programa->id}}">
                    {{$programa->nmprograma}}
                </option>
                @endforeach
            </select>  
            <input type="text" class="form-control"  ng-model="segunda.hratividade">
        </span>
      </td>
      <td>
        <span editable-text="users.name"  >
             <select class="form-control" ng-model="terca.idatividade">
             <option value="">Selecione o programa </option>
                @foreach ($programas as $programa)
                <option value="{{$programa->id}}">
                    {{$programa->nmprograma}}
                </option>
                @endforeach
            </select>  
            <input type="text" class="form-control"  ng-model="terca.hratividade">
        </span>
      </td>
      <td>
        <span editable-text="users.name"  >
             <select class="form-control" ng-model="quarta.idatividade">
             <option value="">Selecione o programa </option>
                @foreach ($programas as $programa)
                <option value="{{$programa->id}}">
                    {{$programa->nmprograma}}
                </option>
                @endforeach
            </select>  
            <input type="text" class="form-control"  ng-model="quarta.hratividade">
        </span>
      </td>
      <td>
        <span editable-text="users.name"  >
             <select class="form-control" ng-model="quinta.idatividade">
             <option value="">Selecione o programa </option>
                @foreach ($programas as $programa)
                <option value="{{$programa->id}}">
                    {{$programa->nmprograma}}
                </option>
                @endforeach
            </select>  
            <input type="text" class="form-control"  ng-model="quinta.hratividade">
        </span>
      </td>
      <td>
        <span editable-text="users.name"  >
             <select class="form-control" ng-model="sexta.idatividade">
             <option value="">Selecione o programa </option>
                @foreach ($programas as $programa)
                <option value="{{$programa->id}}">
                    {{$programa->nmprograma}}
                </option>
                @endforeach
            </select>  
            <input type="text" class="form-control"  ng-model="sexta.hratividade">
        </span>
      </td>
      <td>
        <span editable-text="users.name"  >
             <select class="form-control" ng-model="sabado.idatividade">
             <option value="">Selecione o programa </option>
                @foreach ($programas as $programa)
                <option value="{{$programa->id}}">
                    {{$programa->nmprograma}}
                </option>
                @endforeach
            </select>  
            <input type="text" class="form-control"  ng-model="sabado.hratividade">
        </span>
      </td>

      <td style="white-space: nowrap">
        <form editable-form name="rowform" onbeforesave="saveUser($data, user.id)" ng-show="rowform.$visible" class="form-buttons form-inline" shown="inserted == user">
          <button type="submit" ng-disabled="rowform.$waiting" class="btn btn-primary">
            save
          </button>
          <button type="button" ng-disabled="rowform.$waiting" ng-click="rowform.$cancel()" class="btn btn-default">
            cancel
          </button>
        </form>
        <div class="buttons" ng-show="!rowform.$visible">
          <button type="button" class="btn btn-primary" ng-click="rowform.$show()">editar</button>
          <button type="button" class="btn btn-danger" ng-click="removeUser($index)">deletar</button>
        </div>  
      </td>
    </tr>
  </table>
 <button type="button" class="btn btn-default" ng-click="addUser()">Adicionar</button>
 <button class="btn btn-primary " ng-click="psaSalvar()" type="button"><i class="fa fa-check"></i>Salvar</button>
 <button class="btn btn-primary " ng-click="getPsa()" type="button"><i class="fa fa-check"></i>Editar</button>
                                        </div>
                                    </div>
                                </div>
                                            
                                       </fieldset>
                                    </div>
                                </div>

                            </div>
                    </div>
                </div>
            </div>

        </div>



@endsection


