<!--BREADCRUMBS-->
<header class="page-header">
    <h2>403</h2>
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ url('/') }}/painel"><i class="fa fa-home"></i></a>
            </li>
            <li><span>403</span></li>
        </ol>
        <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
    </div>
</header>
<!--FECHA BREADCRUMBS-->

<section class="body-error error-inside">
    <div class="center-error">
        <div class="row">
            <div class="col-md-12">
                <div class="main-error mb-xlg">
                    <h2 class="error-code text-dark text-center text-semibold m-none">403 <i class="fa fa-file"></i></h2>
                    <p class="error-explanation text-center"><b>Opsss:</b> Você não têm permissão para acessar essa página.</p>
                </div>
            </div>
        </div>
    </div>
</section>