@extends('layouts.app')

@section('content')

<div class="col-sm-6 b-r">
     @include('admin.funcionarios.tools_form')

    <div style="display: block;" class="ibox-content">
	<h3> Novo funcionário </h3>
	
	{{-- @include('errors._check') --}}

	{!! Form::open(array('route' => 'admin.empresa.funcionarios.store', 'class'=>'form')) !!}
   
    @include('admin.funcionarios._form')

   	<div class="form-group">   		
 	  	{!! Form::submit('Criar ', ['class'=>'btn btn-primary']) !!}
   	</div>
	
	{!! Form::close() !!}

	
    </div>
</div>
@endsection

