@extends('layouts.app')

@section('content')

<div class="ibox float-e-margins">
     @include('admin.funcionarios.tools_form')

    <div style="display: block;" class="ibox-content">
	<h3> Editando tipo funcionario: {{$funcionario->nmpessoa}} </h3>
	
	 @include('errors._check')
	
	{!!  Form::model($funcionario, array('route' => array('admin.empresa.funcionarios.update', $funcionario->id))) !!}
   	
    @include('admin.funcionarios._form')

   	<div class="form-group">   		
   		<button class="btn btn-primary " type="submit"><i class="fa fa-check"></i>&nbsp;Salvar</button> 
   	</div>
	
	{!! Form::close() !!}
	
	
    </div>
</div>

@endsection

