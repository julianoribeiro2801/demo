@extends('layouts.app')

@section('content')

<div class="container">
	<h3>Funcionarios</h3>
	<br>
	<a href="{{ route('admin.empresa.funcionarios.create') }}" class="btn btn-primary"> Novo funcionario</a>

	<br><br>
	<table class="table table-bordered">
		<thead>
			<tr>
				<th>Id</th>
				<th>Nome</th>
				<th>Pessoa</th>
				<th>Professor?</th>
				<th>Ações</th>
			</tr>
		</thead>
		<tbody>
		@foreach ($funcionarios as $funcionario)
			<tr>
				<td> {{$funcionario->id}}</td>
				<td> {{$funcionario->nmpessoa}}</td>
				<th> {{$funcionario->idpessoa}}</th>
				<th> {{$funcionario->stprofessor}}</th>

				<td> <a href="{{route('admin.empresa.funcionarios.edit', $funcionario->id)}}" class="btn btn-info">Editar</a></td>
				
			</tr>
			@endforeach
		</tbody>
	</table>

	{{-- {{{ $funcionarios->render()}}} --}}

	
</div>

@endsection

