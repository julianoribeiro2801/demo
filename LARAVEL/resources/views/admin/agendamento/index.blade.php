@extends('layouts.app')


@section('content')

@push('scripts')
{!! Html::script("js/agendamentoController.js") !!}
@endpush


<div class="wrapper wrapper-content animated fadeInRight" ng-controller="agendamentoController">
    <div class="row">
        <div class="col-xs-12 col-md-6">

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Agendamentos de Avaliações </h5>
                    <div class="ibox-tools">


                    </div>
                </div>
                <div class="ibox-content">
                    {{-- INICIO CALENDARIO--}}
                    <div  style="overflow:auto;">
                        <div class='pull-right'>
                            <button class='btn btn-primary anteriorMes'>
                                <i class='fa fa-chevron-left' aria-hidden='true'></i>
                            </button>
                            <button class='btn btn-primary proximoMes'>
                                <i class='fa fa-chevron-right' aria-hidden='true'></i>
                            </button>
                        </div>
                        <div class="calendarioCross"> <!-- aqui vem o calendario --> </div>
                    </div> 

                </div>
            </div>

            <!-- fim col -->
        </div>


        <div class="col-xs-12 col-md-3">


            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Agendamento Disponíveis</h5>
                    <div class="ibox-tools">

                        <button class="btn btn-primary" ng-click="getHorarios(1)" data-toggle="modal" data-target="#modalAgenda" >
                            <i class="fa fa-plus" aria-hidden="true"></i> 
                            <span class="hidden-xs">Agendamentos</span>
                        </button>


                    </div>
                </div>
                <div class="ibox-content">

                    <div style="clear: both"></div>
                    <!--  fim tb  -->
                    <div id="external-events">

                    </div>
                </div>
            </div>

        </div>

    </div>


    <!--inicio modal -->        
    <div class="modal inmodal fade" id="modalAgendar" tabindex="-1" role="dialog" aria-hidden="true" ng-controller="agendamentoController">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                    <h4 class="modal-title">Agendar Avaliação</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <input type="hidden" ng-model="agendaSel" id="agenda_id" name="agenda_id"/>
                        <input type="hidden" id="dtagenda" name="dtagenda"/>
                        <label>Pesquisar cliente:</label>

                        <select class="form-control select2_demo_1" ng-model="client.id" ng-change="selectClient(client.id, client.genero)" data-placeholder="Selecione um cliente..." id="clienteSelect" tabindex="2">



                        </select>
							<!--<select class="form-control" ng-model="client.id" data-placeholder="Selecione um cliente..." name="clienteSelect" id="clienteSelect" tabindex="2">

								<option value="">Selecione um cliente</option>

								<option ng-repeat="client in clients" value="@{{client.id}}">@{{client.name}}</option>

							</select>  -->
                        

                    </div>

                </div>
                <div class="modal-footer">
                    <div class="form-group">
                            <!--<button type="submit" name="SendPostForm" id="SendPostForm" value="SendPostForm" class="btn btn-primary pull-right"><i class="fa fa-check"></i> Salvar</button>-->
                        <!--<button class="btn btn-primary" data-dismiss="modal" ng-click="agendarAluno(client.id, agenda_id.value, dtagenda.value)" type="button">
                            <i class="fa fa-check"></i>Salvar
                        </button>                                               -->
                        <button class="btn btn-primary" data-dismiss="modal" onclick="agendaAluno(clienteSelect.value, agenda_id.value, dtagenda.value)" type="button">
                            <i class="fa fa-check"></i>Salvar
                        </button>                                              
                    </div>
                </div>


            </div>
        </div>        
    </div>        
    <div class="modal inmodal fade" id="modalAgenda" tabindex="-1" role="dialog" aria-hidden="true" >
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                    <h4 class="modal-title">Horários de avaliação</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12 col-sm-4">
                            <div class="form-group">                        
                                <label>Dia da semana:</label>


                                <input list="diaSelect" name="diaSelect"  placeholder="Selecione o dia..." >
                                <datalist  id="diaSelect"  >
                                    <option value="Domingo"></option>
                                    <option value="Segunda"></option>
                                    <option value="Terça"></option>
                                    <option value="Quarta"></option>
                                    <option value="Quinta"></option>
                                    <option value="Sexta"></option>
                                    <option value="Sábado"></option>
                                </datalist>

                            </div>
                        </div>
                        {{--fim
                                col --}}
                        <div class="col-xs-6 col-sm-4">
                            <div class="form-group">
                                <label>Horário:</label>
                                <input type="text" id="horario" ng-model="_horario" name="horario"/>
                                <!--<input type="text" data-mask="99:99" placeholder="Horário">-->
                            </div>
                        </div>     
                        <div class="col-xs-6 col-sm-4">

                            <label>Duração:</label> 
                            <br>

                            <input type="text" id="duracao" placeholder="min" name="duracao" ng-model="_duracao" style="width:48%; margin-right: 2px;" />


                            <button style="padding: 2px 10px; margin-top: -1px;" class="btn btn-xs btn-primary" 
                                    ng-click="addHorario(_horario, _duracao)" type="button">
                                <i class="fa fa-check"></i> Salvar
                            </button>      


                        </div>              





                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12">
                            <h2>Horários</h2>
                            <p>Verifique abaixo os horários disponíveis.</p>   
                            <div style="overflow: auto; height: 250px;">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Dia da Semana </th>
                                            <th>Horário  </th>
                                            <th>Duração</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="(key, horario) in horarios">
                                            <td>@{{ horario.diasemana}}</td>
                                            <td>@{{ horario.horario}}</td>
                                            <td>@{{ horario.duracao}} min</td>
                                            <td>
                                                <i class="fa fa-trash" aria-hidden="true" data-toggle="modal" data-target="#addCliente" ng-click="delHorario(horario.id)" ng-disabled="addClienteSpmBtn"></i>
                                            </td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>

                        </div>

                    </div>

                    {{--
                                    < div class = "modal-footer" >
                                    < div class = "form-group" >
                                    < button class = "btn btn-primary" data - dismiss = "modal" onclick = "gravaHorario(diaSelect.value, horario.value, duracao.value)" type = "button" >
                            < i class = "fa fa-check" > < /i>Salvar
                                            < /button>                                               
                                            < /div>
                                            < /div> --}}


                </div>
            </div>        
        </div>    


        <!-- fim modal -->    
    </div>

</div>





@push('scripts')


<!-- Nestable List -->
{!! Html::script("tema_assets/js/plugins/nestable/jquery.nestable.js") !!}

<!-- Select 2 -->
{!! Html::script("assets_admin/vendor/select2/select2.js") !!}


{!! Html::script("js/agendamentoController.js") !!}


<!-- mascara-->
{!! Html::script("assets_admin/javascripts/jQueryMask/jquery.mask.js") !!}
{!! Html::script("assets_admin/javascripts/jQueryMask/jquery.mask.min.js") !!}
<!--{!! Html::script("assets_admin/javascripts/jQueryMask/jquery.mask.test.js") !!}-->

<?php
$client_id = 76;
if (isset($client_id)) {
    ?>




    <script type="text/javascript">

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')
                    }
                });
        var mq = $(window).width();
        if (mq >= 560) {
            $(window).scroll(function () {
                var sticky = $('#top-bar'),
                        scroll = $(window).scrollTop();
                if (scroll >= 180)
                    sticky.addClass('fixed');
                else
                    sticky.removeClass('fixed');
            });
        }

        function pad(value, length) {
            return (value.toString().length < length) ? pad("0" + value, length) : value;
        }

        function gravaCalendario(diaatual, desc, tipo) {
            var mesatual = $('#mesatual').val();
            var anoatual = $('#anoatual').val();
            diaatual = pad(diaatual, 2);
            var dataatual = diaatual + '/' + mesatual + '/' + anoatual;
            console.log('data: ' + dataatual + ' desc: ' + desc);
        }

        function btnprescicaoSalvar() {
            swal("Ok!", "Prescrição salva com sucesso!", "success");
        }

        function maisProjetos() {
            $(".divOculta").each(function () {
                $(this).css("display", "block");
            });
            $(".btn-mais-projetos").hide();
            $(".btn-voltar-projetos").show();
        }

        function menosProjetos() {
            $(".divOculta").each(function () {
                $(this).css("display", "none");
            });
            $(".btn-mais-projetos").show();
            $(".btn-voltar-projetos").hide();
        }
        
        $(document).ready(function () {
            function buscaCalendario(newMes, newAno, tipo) {
                var classTipo = '';

                classTipo = $('.calendarioCross');

                $(classTipo).html('Aguarde, carregando calendário...');
                $(".btnprescicaoSalvar").hide();
                $.post('../admin/agendamento/getCaledario', {
                    mes: newMes,
                    ano: newAno,
                    tipo: tipo,
                    client_id: <?= $client_id ?>
                }, function (retorno) {
                    $(classTipo).html('');
                    $(classTipo).append(retorno);
                    $(".anteriorMes").prop("disabled", false);
                    $(".proximoMes").prop("disabled", false);
                    $(".btnprescicaoSalvar").show();
                });
            }

            $('.anteriorMes').click(function () {
                $(".proximoMes").prop("disabled", true);
                $(".anteriorMes").prop("disabled", true);
                var mesatual = $('#mesatual').val();
                var anoatual = $('#anoatual').val();
                mesatual = parseInt(mesatual) - 1;
                if (mesatual < 1) {
                    mesatual = '12';
                    anoatual = parseInt(anoatual) - 1;
                }
                buscaCalendario(mesatual, anoatual, 'cross');

            });

            $('.proximoMes').click(function () {
                $(".proximoMes").prop("disabled", true);
                $(".anteriorMes").prop("disabled", true);
                var mesatual = $('#mesatual').val();
                var anoatual = $('#anoatual').val();
                mesatual = parseInt(mesatual) + 1;
                if (mesatual > 12) {
                    mesatual = '01';
                    anoatual = parseInt(anoatual) + 1;
                }
                buscaCalendario(mesatual, anoatual, 'cross');

            });

            buscaCalendario(<?= date('m') ?>, <?= date('Y') ?>, 'cross');

           
        });

        $(document).ready(function () {
           $('#horario').mask('00:00');
        });

    </script>




<?php } ?>




@endpush



<style type="text/css"> 
    .table td{
        cursor:pointer;
    }
    .rotulos_h_agendamento {
        font-size: 16px;
        text-align: center;
        font-weight: bold;
    }
    .rotulos_h_agendado {
        background: #f6f5f6;
        font-size: 16px;
        text-align: center;
        color: #a4a6a7;
    }
    td#dia_comum {
        text-align: center;
        font-size: 24px;
        color: #1cb394;
        font-weight: bold;
    }
    td#dia_atual {
        background: #f6f5f6 ;
        font-size: 24px;
        text-align: center;
        font-weight: bold;
        color: #686b6d;
    }

    td.dia_click {
        background: #1cb394;
        font-size: 24px;
        text-align: center;
        font-weight: bold;
        color: #FFF !important;
    } </style>
@endsection






