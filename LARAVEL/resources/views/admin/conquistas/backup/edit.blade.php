@extends('layouts.app')

@section('content')

<div class="container">
	<h3> Editando tipo funcionario: {{$conquista->nmconquista}} </h3>
	
	 @include('errors._check')
	
	{!!  Form::model($conquista, array('route' => array('admin.clientes.conquistas.update', $conquista->id))) !!}
   	
    @include('admin.conquistas._form')

   	<div class="form-group">   		
   		<button class="btn btn-primary " type="submit"><i class="fa fa-check"></i>&nbsp;Salvar</button> 
   	</div>
	
	{!! Form::close() !!}
	
	
</div>

@endsection

