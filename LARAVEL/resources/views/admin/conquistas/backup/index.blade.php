@extends('layouts.app')

@section('content')

<div class="container">
	<h3> Conquistas</h3>
	<br>
	<a href="{{ route('admin.clientes.conquistas.create') }}" class="btn btn-primary"> Nova conquista</a>

	<br><br>
	<table class="table table-bordered">
		<thead>
			<tr>
				<th>Id</th>
				<th>Nome</th>
				<th>Tipo de conquista</th>
				<th>Atividade</th>
				<th>Ação</th>
			</tr>
		</thead>
		<tbody>
		@foreach ($conquistas as $conquista)
			<tr>
				<td> {{$conquista->id}}</td>
				<td> {{$conquista->nmconquista}}</td>
				<th> {{$conquista->idtipoconquista}}</th>
				<th> {{$conquista->idatividade}}</th>

				<td> <a href="{{route('admin.clientes.conquistas.edit', $conquista->id)}}" class="btn btn-info">Editar</a></td>
				
			</tr>
			@endforeach
		</tbody>
	</table>

	{{-- {{{ $conquistas->render()}}} --}}

	
</div>

@endsection

