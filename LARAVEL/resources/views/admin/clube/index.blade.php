@extends('layouts.app')
@section('content')

@push('scripts')
{!! Html::script("js/cvempresaController.js") !!}

@endpush


<div ng-controller="cvempresaController" class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5>Listagem de {{ $headers['title'] }} com paginação, ordernação e filtro</h5>
					<div class="ibox-tools">
						<a class="collapse-link">
							<i class="fa fa-chevron-up"></i>
						</a>
						
						<a class="close-link">
							<i class="fa fa-times"></i>
						</a>
					</div>
				</div>
				<div class="ibox-content">
					<div class="col-xs-12 col-md-8 pd0">
						<div class="input-group"><span class="input-group-addon"><i class="fa fa-search" aria-hidden="true"></i></span>
						<input type="text" class="form-control" id="filter"
					placeholder="Pesquisar na tabela"> </div>
				</div>
				<div class="col-xs-12 col-md-4 pd0 al-right">
					<a  data-toggle="modal" data-target="#modalNovaEmpresa" class="btn btn-primary"><i class="fa fa-plus-square"></i> Nova Empresa</a>
				</div>
				
				
				
				
				<table class="footable table table-stripped" data-page-size="8" data-filter=#filter>
					<thead>
						<tr>
							<th>Id</th>
							<th style="width: 40%">Empresa</th>
							<th style="width: 15%">Vencimento</th>
							<th style="width: 15%">CNPJ</th>
							<th style="width: 15%">Segmento</th>
							<th style="width: 15%">Perc. Desconto</th>
							<th>Total Vendas</th>
							
							<th> </th>
						</tr>
					</thead>
					<tbody>
						
						<tr ng-repeat="cvempresa in cvempresas | filter:searchText"> 
                                                        <td>@{{ cvempresa.id }}</td>
							<td >@{{ cvempresa.nmempresa }}</td>
							<td >@{{ cvempresa.vencimento }}</td>
							<td >@{{ cvempresa.cnpj }}</td>
							<td >@{{ cvempresa.nmsegmento }}</td>
							<td >@{{ cvempresa.percdesconto }} %</td>
							<td >@{{ cvempresa.totalvendas }}</td>
							
							
							<td>
								<!--{{--  href="{{route('admin.clube.edit', $compra->id)}}"  --}}
								<a data-toggle="modal" alt="Editar"  ng-click="select(cvempresa)" style="margin-right: 15px" >
								<i class="fa fa-pencil" style="color: #333"></i> </a>
								<a href="#"  alt="Deletar"  ng-click="destroy(cvempresa)"><i class="fa fa-trash" style="color: #333"></i></a>-->
                                                            <button  class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modalEditarCvempresa" ng-click="editCvempresa(cvempresa.id)">
								<i class="fa fa-edit"></i>
                                 			    </button>
                                                            <button class="btn btn-danger btn-sm" ng-click="deleteCvempresa(cvempresa.id)">
								<i class="fa fa-trash"></i>
                                                            </button>                                                                
							</td>
							
						</tr>
						
						
					</tbody>
					<tfoot>
					<tr>
						<td colspan="5">
						<ul class="pagination pull-right"></ul>
					</td>
				</tr>
				</tfoot>
			</table>
		</div>
	</div>
</div>
@include('admin.clube.modal')    
</div>
@endsection
@section('css')
	{!! Html::style("assets_admin/vendor/pnotify/pnotify.custom.css") !!}
	{!! Html::style("assets_admin/vendor/select2/select2.css") !!}
	{!! Html::style("assets_admin/vendor/bootstrap-fileupload/bootstrap-fileupload.min.css") !!}
@endsection
@section('scripts')
	{!! HTML::script("assets_admin/vendor/jquery-form/jquery.form.min.js") !!}
	{!! HTML::script("assets_admin/vendor/jquery-validation/jquery.validate.js") !!}
	{!! HTML::script("assets_admin/javascripts/forms/cvempresa.validation.js") !!}
	{!! Html::script("assets_admin/vendor/pnotify/pnotify.custom.js") !!}
	{!! Html::script("assets_admin/vendor/jquery-maskedinput/jquery.maskedinput.min.js") !!}
	{!! Html::script("assets_admin/vendor/select2/select2.js") !!}
	{!! Html::script("assets_admin/vendor/bootstrap-fileupload/bootstrap-fileupload.min.js") !!}
	<script type="text/javascript">
		$(document).ready(function() {
                    
                }
        </script>                
	<script type="text/javascript">
		$(document).ready(function() {
			$('#file-input').on('change', function(evt) {
				var file = evt.target.files[0];
				if(file) {
					$('[name="cvempresa_id"]').removeAttr("disabled");
					$('[name="nmempresa"]').removeAttr("disabled");
					$('[name="cnpj"]').removeAttr("disabled");
					$('[name="percdesconto"]').removeAttr("disabled");

					$("#PostFormCvempresaUp").submit();
				}
			});
			
			$('#cnpj').mask("99.999.999/9999-99");
			$('#cnpj1').mask("99.999.999/9999-99");
			
		});
	</script>
@endsection