<div class="modal inmodal fade" id="modalNovaEmpresa" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
				<h4 class="modal-title">Nova empresa</h4>
			</div>
			<form name="PostFormCvempresa" id="PostFormCvempresa" method="post" enctype="multipart/form-data">
				<div class="modal-body">
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
								<label>Nome Fantasia:</label>
								<input type="text" class="form-control" id="nmempresa" name="nmempresa">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label>(%) Desconto:</label>
								<input type="text" class="form-control" id="percdesconto" name="percdesconto">
							</div>
						</div>
                                                <div class="col-sm-6">
							<div class="form-group">
								<label>CNPJ:</label>
								<input type="text" class="form-control" id="cnpj" name="cnpj">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
                                                    <label>Segmento</label>
                                                        <div class="input-group m-b">
                                                                <select name="idsegmento" class="select2_demo_1 form-control">
									<option value="1">Selecione o segmento</option>
									<option ng-repeat="segmento in segmentos"  value="@{{ segmento.id }}">@{{ segmento.nmsegmento }}</option>	
								</select>
                                                        </div>
                                                </div>                                    
						<div class="col-sm-6">
                                                    <div class="form-group" id="data">
							<label>Data de vencimento:</label>
                                                        <div class="input-group date">
                                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                            <input ng-disabled="value1" type="text" class="form-control" name="dtvencimento">
							</div>
                                                        <div class="checkbox m-r-xs">
                                                            <input type="checkbox" ng-model="value1" value="true" name="value1" id="value1">
                                                            <label for="checkbox">Vencimento indeterminado</label>
                                                        </div>
                                                    </div>
        					</div>                                    
					</div>                                    
					<div class="row load" style="display:none;">
						<div class="col-sm-12">
							<div class="progress progress-striped light active">
								<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="" aria-valuemin="0" aria-valuemax="100">
									<span class="sr-only"></span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<div class="form-group">
						<button type="submit" name="SendPostForm" id="SendPostForm" value="SendPostForm" class="btn btn-primary pull-right"><i class="fa fa-check"></i> Salvar</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<div class="modal inmodal fade" id="modalEditarCvempresa" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
				<h4 class="modal-title">Editar empresa</h4>
			</div>
			<form name="PostFormCvempresaUp" id="PostFormCvempresaUp" method="post" enctype="multipart/form-data">
				<div class="modal-body">
                                       <div style="display:none;"><input type="text" ng-model="cvempresa.id" name="cvempresa_id" id="cvempresa_id"></div>
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
								<label>Nome Fantasia:</label>
								<input type="text" class="form-control" ng-model="cvempresa.nmempresa" name="nmempresa">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label>(%) Desconto:</label>
								<input type="text" class="form-control" ng-model="cvempresa.percdesconto" name="percdesconto">
							</div>
						</div>
                                                <div class="col-sm-6">
							<div class="form-group">
								<label>CNPJ:</label>
								<input type="text" class="form-control" ng-model="cvempresa.cnpj" id="cnpj1" name="cnpj1">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
                                                    <label>Segmento</label>
                                                    <div class="input-group m-b">
							<select class="form-control" name="idsegmento" ng-model="cvempresa.idsegmento">
								<option value="1">Selecione o segmento</option>
								<option ng-repeat="segmento in segmentos" value="@{{ segmento.id }}">@{{ segmento.nmsegmento }}</option>	
							</select>
                                                    </div>
        					</div>
						<div class="col-sm-6">
                                                    <div class="form-group" id="data">
							<label>Data de vencimento:</label>
                                                        <div class="input-group date">
                                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                            <input type="text" ng-disabled="value1" class="form-control" name="dtvencimento" ng-model="cvempresa.dtvencimento">
							</div>
                                                        <div class="checkbox m-r-xs">
                                                            <input type="checkbox" ng-model="value1" value="true" name="value1" id="value1">
                                                            <label for="checkbox">Vencimento indeterminado</label>
                                                        </div>
                                                        
                                                    </div>
        					</div>                                    
                                            
					</div>                                        
                                       
					<div class="row load" style="display:none;">
						<div class="col-sm-12">
							<div class="progress progress-striped light active">
								<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="" aria-valuemin="0" aria-valuemax="100">
									<span class="sr-only"></span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<div class="form-group">
						<button type="submit" name="SendPostForm" id="SendPostForm" value="SendPostForm" class="btn btn-primary pull-right"><i class="fa fa-check"></i> Salvar</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

