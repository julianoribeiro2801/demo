@extends('layouts.app')

@section('content')

<div class="container">
	<h3> Novo Cliente</h3>
	
	@include('errors._check')

	{!! Form::open(array('route' => 'admin.empresa.clients.store', 'class'=>'form')) !!}
   
    @include('admin.clients._form')

   	<div class="form-group">   		
   		{!! Form::submit('Criar cliente', ['class'=>'btn btn-primary']) !!}
   	</div>
	
	{!! Form::close() !!}

	
	
</div>

@endsection

