@extends('layouts.app')

@section('content')

<div class="container">
	<h3> Clientes</h3>
	<br>
	<a href="{{ route('admin.empresa.clients.create') }}" class="btn btn-primary"> Novo Cliente</a>
	<br><br>
	<table class="table table-bordered">
		<thead>
			<tr>
				<th>Id</th>
				<th>Nome</th>
				<th>Telefone</th>

				<th>Ação</th>
			</tr>
		</thead>
		<tbody>
		@foreach ($clients as $client)
			<tr>
				<td> {{$client->id}}</td>
				<td> {{$client->user->name}}</td>
				<td> {{$client->phone}}</td>

				<td> <a href="{{route('admin.empresa.clients.edit', $client->id)}}" class="btn btn-info">Editar</a></td>
			</tr>
			@endforeach
		</tbody>
	</table>

	{{{ $clients->render()}}}


</div>

@endsection

