@extends('layouts.app')

@section('content')
    <div class="wrapper wrapper-content">
        <div class="container">
            <h2>Sua busca por <b>{{ $query }}, teve {{ $results->count() }} resultado(s)</b></h2>
        </div>
    </div>
@endsection

