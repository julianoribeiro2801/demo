@extends('layouts.app')

@section('content')

<div class="container">
	<h3> Editando Cliente: {{$client->user->name}} </h3>
	
	 @include('errors._check')

	{!!  Form::model($client, array('route' => array('admin.empresa.clients.update', $client->id))) !!}
   	
    @include('admin.clients._form')

   	<div class="form-group">   		
   		<button class="btn btn-primary " type="submit"><i class="fa fa-check"></i>&nbsp;Salvar</button> 
   	</div>
	
	{!! Form::close() !!}

	
	
</div>

@endsection

