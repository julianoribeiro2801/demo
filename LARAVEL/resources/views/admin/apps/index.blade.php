@extends('layouts.app')
@section('content')

@push('scripts')
{!! Html::script("js/lojaAppController.js") !!}

@endpush
<div class="wrapper wrapper-content animated fadeInRight" ng-controller="lojaAppController">
    <div class="row">
        
        @foreach($modulos as $mod)
            
           
           

            <div class="col-md-3">
            <div class="ibox">

                <div class="ibox-content product-box" >
                    <div class="product-imitation" style="background-image: url( {{$mod->imagem}}); background-size: cover;     background-position: center top;">
                        &nbsp {{--[ INFO;--}}
                    </div>
                    <div class="product-desc" >
                        <span class="product-price" ng-show="statusPersonaliza==0">
                            {{$mod->valor}}
                        </span>
                        <span class="product-price" ng-show="statusPersonaliza==1">
                            INSTALADO
                        </span>    



                        {{-- <small class="text-muted">Categoria</small> --}}
                        <a href="#" class="product-name"> {{$mod->nome}}</a>


                        <div class="small m-t-xs">
                          {{$mod->descricao}}
                        </div>
                        
                        @if($mod->trial>0 && $mod->pago=='N')
                            <div class="m-t text-righ" >
                                @if($mod->dia_atraso>0)
                                {{--  aqui ja ta vencido --}}
                              
                                 <a href="#" class="btn btn-xs btn-outline btn-danger" >
                                     Vencido a {{$mod->dia_atraso}} dias <br>
                                  Receber novo link para contatação </a>
                               
                                 @else 
                                 {{--  aqui ja ta trial --}}
                                     <a href="#" class="btn btn-xs btn-outline btn-warning" >
                                    Trial finaliza em {{ ($mod->dia_atraso*-1) }} dias 
                                </a>
                                 @endif
                            </div>
                        @endif

                      
                    

                        
                        <div class="m-t text-righ" ng-show="statusPersonaliza==0">
                            <a href="#" class="btn btn-xs btn-outline btn-primary" ng-click="addModulo({{$mod->id}},1)">
                                Instalar @{{statusPersonaliza}} <i  class="fa fa-check"></i> </a>
                        </div>
                        
                        @if($mod->id > 1 )
                          <div class="m-t text-righ"  >
                            <a href="#" class="btn btn-xs btn-outline btn-primary" > <i  class="fa fa-check"></i> </a>
                          </div>
                       @endif

                        {{-- <div class="m-t text-righ" ng-show="statusPersonaliza==1">
                            <a href="#" class="btn btn-xs btn-outline btn-danger"  ng-click="addModulo({{$mod->id}},0)">Desativar <i class="fa fa-times-circle"></i> </a>
                        </div>
                        --}}

                        
                    </div>
                </div>
            </div>
        </div>


        @endforeach
        

         
       
    </div>
</div>
@endsection