@extends('layouts.app')

@section('content')
    <div id="app">
        <chat :user="'{{ auth()->user() }}'" :userid="'{{ auth()->user()->id }}'" :idunidade="'{{ session('id_unidade') }}'"></chat>
    </div>
@endsection

@section('scripts')

    <!-- FooTable -->
    {{-- {!! Html::script("tema_assets/js/plugins/footable/footable.all.min.js") !!} --}}

@endsection

@push('scripts')
    <script src="{{ asset('dist/chat.js') }}"></script>
@endpush

