@extends('layouts.app')

@section('content')

@push('scripts')
{!! Html::script("js/chatController.js") !!}
@endpush
<div class="wrapper wrapper-content animated fadeInRight" ng-controller="chatController">
    <div class="row">
        <div class="col-lg-12" style="overflow: auto;">
            <div class="ibox" style="overflow: auto; min-width: 400px;">
                <div class="ibox-title">
                    <div class="col-xs-3 col-md-3">
                        <div> Conversando com: </div>
                    </div>
                    <div class="col-xs-9 col-md-9">
                        <div>
                            <img src="https://sistemapcb.net.br/user-117.jpg" alt="" class="chat-avatar img-circle img-circle"
                                 style="width: 25px; height: 25px;">
                            <b>@{{alunonome}}</b>
                            <div class="pull-right">
                                <span style="margin-right: 5px; font-weight: bold;">CRM </span>
                                <div class="switch pull-right">
                                    <div class="onoffswitch">
                                        <input type="checkbox" ng-checked="exibetudo == 'S'" checked class="onoffswitch-checkbox" id="type1" ng-click="exibirTudo()">
                                        <label class="onoffswitch-label" for="type1" >
                                            <span class="onoffswitch-inner" ></span>
                                            <span class="onoffswitch-switch"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="ibox-content" style="overflow: auto; min-width: 400px;">
                    <div class="row" style="overflow: auto; min-width: 400px;">
                        <div class="col-xs-3 col-md-3">
                            {{-- buscador--}}
                            <div class="form-group">
                                <form ng-submit="search(event)" method="get">
                                    <div class="input-group">
                                        <input type="text" placeholder="Buscar cliente" ng-model="criteria" name="search" class="form-control">
                                        <div class="input-group-btn">
                                            <button class="btn btn-primary" type="submit">
                                                <span class="loading dots"></span>
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>


                            <div class="chat-users">
                                <div  class="users-list">

                                   {{--  <div  class="chat-user active" ng-repeat="teacher in teachers">
                                        <img  src="/uploads/avatars/@{{teacher.avatar}}" alt="" class="chat-avatar img-circle" ng-click="getMessages(teacher.id, teacher.name)">
                                        <div  class="chat-user-name" ng-click="pegaMensagens(teacher.id, teacher.name)">
                                            <a  href="#">@{{teacher.name}} ... @{{teacher.created_at}}</a>
                                        </div>
                                        <span class="new_message"ng-show="teacher.naolidas > 0">@{{teacher.naolidas}}</span>
                                    </div> --}}

                                    <div  class="chat-user" ng-repeat="uni in unity| filter:criteria">
                                        <img  src="/uploads/avatars/@{{uni.avatar}}" alt="" class="chat-avatar img-circle" ng-click="getMessages(uni.id, uni.name)">
                                        <div  class="chat-user-name" ng-click="pegaMensagens(uni.id, uni.name)">
                                            <a  href="#">@{{uni.name}} .. @{{uni.dtultima}}</a>
                                        </div>
                                        <span  class="new_message" ng-show="uni.naolidas > 0">@{{uni.naolidas}}</span>
                                    </div>

                                </div>
                            </div>

                        </div>

                        <div id="chat-box" class="col-xs-9 col-md-9 " >

                            <div class="chat-discussion">

                                <div  ng-if="messages.length == 0">   Selecione
                                    um contato ao lado para iniciar uma conversa </div> 

                                <div ng-class="{'chat-message left': message.sender_id == logued ,'chat-message right': message.sender_id !== logued }" 
                                     ng-repeat="message in messages">

                                    <div ng-class="{'message-eu': message.sender_id == logued ,'message_received': message.sender_id != logued }" >

                                        <div class="message_wrapper">
                                            <a href="#" class="message-author" ng-show="message.sender_id == logued">
                                                <span class="message-date"> Você disse: </span> </a>

                                            <a href="#" class="message-author" ng-show="message.sender_id != logued">
                                                <span class="message-date">  @{{message.sender_name}}: </span>

                                            </a>


                                            <span class="message-content">
                                                @{{ message.message}}
                                            </span>

                                            <div  class="visualized">
                                                <div style="  color:#898989;  float: left; font-size: 10px;">
                                                    @{{message.created_at}}
                                                </div>

                                                <i ng-show="message.opened == true" class="fa fa-check" aria-hidden="true"></i>
                                            </div>
                                            <!---->
                                        </div>

                                    </div>
                                </div>

                                {{--recebidas
                                                --}}
                                <!--<div class="chat-message right" ng-repeat="message in messages" ng-show="message.sender_id != loged">
                                    <div class="message_received">
                                        <div class="message_wrapper">
                                            <a href="#" class="message-author"> @{{conversando_com}}</a>
                                            <span class="message-date"> 3 meses atrás </span>
                                            <span class="message-content">
                                                @{{ message.message }} 
                                            </span>
                                            
                                            <div  class="visualized">
                                                <i class="fa fa-check" aria-hidden="true"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>-->
                            </div>





                            <div style="" class="">
                                <div class="row" talk="[object Object]" style="background: rgb(238, 238, 238); padding: 10px; margin: 0px;"
                                     user_talk="[object Object]">
                                    <div class="col-lg-10" style="margin: 0px;">
                                        <div class="chat-message-form">
                                            <div class="form-group">
                                                <form class="ng-pristine ng-valid">
                                                    <textarea ng-keypress="leTecla($event)" ng-model="mensagem" name="message" placeholder="Digite uma mensagem" class="form-control message-input"></textarea>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-2" style="text-align: center;">
                                        <i aria-hidden="true" class="fa fa-paper-plane-o" style="font-size: 24px; margin-top: 20px;" ng-click="storeMessage(mensagem, mensagem)"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<style >

    .chat-user {
        cursor: pointer;
        transition: 350ms;
        position: relative;
    }
    .chat-user:hover {
        background: #f3f3f4;
    }
    .chat-user.active {
        background: #19aa8c !important;
        color: #fff;

    }
    .chat-user.active span.new_message {
        background: #fff;
        color: #19aa8b;
    }
    .message_received .message_wrapper  {
        background: rgba(89, 180, 158, 0.26);

    }
    span.new_message {
        background: #19aa8b;
        color: #fff;
        /* padding: 3px; */
        height: 20px;
        min-width: 20px;
        position: absolute;
        right: 0;
        top: 50%;
        transform: translateY(-50%);
        text-align: center;
        border-radius: 50%;
    }

    .visualized {
        text-align: right;
        font-size: 10px;
        color: #149a7c;
    }

    .message_eu {
        background: transparent;
    }

    .chat-discussion.loading:before {
        content: 'Carregando mensagens...';
        position: absolute;
        left: 50%;
        top: 50%;
        color: #19aa8b;
        transform: translate(-50%, -50%);
        background: #fff;
        padding: 15px;
        box-shadow: 0px 2px 8px -3px #ccc;
    }
    .message_wrapper {
        display: inline-block;
        padding: 10px 15px;
        background: #fff;
        border-radius: 5px;
    }
    .message_received {
        text-align: right;
    }
    .message_eu .message_wrapper {
        background: #19aa8b14;
    }
    a.message-author {
        margin-right: 8px;
        font-size: 13px;
    }
    .message-content {
        display: block;
        font-size: 14px;
        padding: 3px;
        text-align: left;
    }
    .chat-discussion.loading {
        display: block;
        position: relative;
    }
</style>


@endsection



