


<div>
    <div class="col-lg-6">
       <div class="form-group"> 
            {!! Form::label('CEP', 'CEP:') !!}
            <div class="input-group">
                

                <input type="text" ng-model="endereco.cep" class="input form-control"/>
                <span class="input-group-addon hidden-xs" ng-click="consultaEndereco()"><i class="fa fa-search " aria-hidden="true"></i></span> 
            </div>
        </div>
    </div> 
    <div class="col-lg-6">
        <div class="form-group">
                {!! Form::label('Endereco', 'Endereco:') !!}
                {!! Form::text('endereco', null, ['class'=>'form-control','ng-model'=>'endereco.endereco']) !!}
        </div>
    </div> 
    <div class="col-lg-6">
        <div class="form-group">
                {!! Form::label('Bairro', 'Bairro:') !!}
                {!! Form::text('bairro', null, ['class'=>'form-control','id' => 'bairro', 'ng-model'=>'endereco.bairro']) !!}
        </div>
    </div>
    <div class="col-lg-6">
        <div class="form-group">
            <select class="select2_demo_1 form-control" ng-model="unidade.estado" ng-change="teste(unidade.estado)">
                <option  value="1">Selecione o estado</option>
                <option  ng-repeat="estado in estados" value="@{{ estado.id }}" >@{{ estado.nome }}</option>
            </select>     
        </div>
    </div> 
    <div class="col-lg-6">
        <div class="form-group">
            <select class="select2_demo_1 form-control" ng-model="unidade.idcidade">
                <option  value="1">Selecione a cidade</option>
                <option  ng-repeat="cidade in cidades" value="@{{ cidade.id }}">@{{ cidade.nome }}</option>
            </select>            
        </div>
    </div> 

    <div class="col-lg-6">
        <div class="form-group">
                {!! Form::label('Numero', 'Numero:') !!}
                {!! Form::text('numero', null, ['class'=>'form-control','ng-model'=>'endereco.numero']) !!}
        </div>
    </div> 
</div>
