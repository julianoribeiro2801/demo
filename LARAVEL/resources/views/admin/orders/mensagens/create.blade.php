@extends('layouts.app')
@section('content')
<div class="row" style="margin-top: 20px" ng-controller="alunoController">
	<div class="col-lg-12">
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5> Nova Mensagem </h5>
				
			</div>
			<div class="ibox-content">
				
				
				{{-- @include('errors._check') --}}
				{!! Form::open(array('route' => 'admin.clientes.mensagens.store', 'class'=>'form')) !!}
				
				@include('admin.mensagens._form')
				<div class="form-group">
					<a href="{{ url('admin/clientes/mensagens') }}" class="btn btn-default"><i class="fa fa-close"></i>&nbsp;Cancelar</a>
					<button class="btn btn-primary " type="submit"><i class="fa fa-check"></i>&nbsp;Criar</button>
					{{-- {!! Form::submit('Criar ', ['class'=>'btn btn-primary']) !!} --}}
				</div>
				
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>


@endsection