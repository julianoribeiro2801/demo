<div ng-controller="alunoController" >
<div aria-hidden="true" class="modal inmodal fade" id="addCliente" role="dialog" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                <h4 class="modal-title">Adicionar Cliente </h4>
            </div>
            <div class="modal-body">
                <span style="display:none;">
                    <input type="text" ng-model="cliente.spmId"></span>
                    <div class="row">
                        <div class="col-xs-6 col-sm-2" style="padding-right:0">
                            
                            <label style="font-size: 14px;">Vagas disp:</label>
                            <input ng-show="privado == 'Não'" type="number" name="name" ng-disabled="true" ng-model="qt_vagas" id="inputIDvagas"
                            class="form-control inputGFM" value="10" title=""
                            required="required" >
                            
                            
                            
                        </div>
                        <div class="col-xs-6 col-sm-10">
                           <h3 style="margin-top: 36px; margin-left: 10px">@{{aula}} - @{{horario}} - @{{dt_aula}} | <span style="font-size: 12px;"> @{{professor}} </span></h3>

                           <div class="pull-right" style="margin-top: 22px;">
                                <button type="button" ng-show="qt_vagas > 0" ng-click="addAlunoAula(aulaSelect)" class="btn btn-primary btn-sm" style="margin-bottom: 5px">
                                <i class="fa fa-plus"></i> Adicionar</button>
                            </div>

                             <div  class="pull-right" ng-show="privado == 'Não'">
                                <button  ng-show="qt_vagas == 0" type="button" class="btn btn-danger btn-sm">
                                <i class="fa fa-bell"></i> Lotada</button>
                            </div>

                        </div>
                    </div>
                    

            
                <div  style="margin-top:5px; overflow-y: scroll; height: 300px"
                    ng-show="privado == 'Não'">
                    
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th></th>
                                <th></th>
                                <th>Nome</th>
                                <th>Ação</th>
                            </tr>
                        </thead>
                        <tbody class="ibox-content" style="overflow-y: scroll; height: 300px">
                            <tr ng-repeat="lista in listareservas">
                                <td>@{{$index + 1}}</td>
                                <td width="50"> <img  src="{{ url('/')}}/uploads/avatars/@{{lista.avatar}}" alt="" class="chat-avatar img-circle img-circle" style="width: 25px; height: 25px;"></td>
                                <!--<td>@{{lista.name}}</td>-->
                                <td>
                                    <select class="form-control chosen-select" ng-model="lista.id" data-placeholder="Selecione um cliente..." tabindex="2" >
                                        <option ng-value="@{{lista.id}}">@{{lista.name}}</option>
                                        <option ng-repeat="client in clients" ng-value="@{{client.id}}">@{{client.name}}</option>
                                    </select>
                                </td>

                                <td class="text-center">

                                     <a ng-click="selectbyID(76)" data-toggle="modal" href='#editar_cliente' >
                                     <i style="color: #676a6c;" 
                                      class="fa fa-eye" aria-hidden="true" style="margin-right: 5px" ></i>
                                     </a>
                                   
                                      <i ng-click="reservaDel(aulaSelect, lista.registro,$index)" class="fa fa-trash" aria-hidden="true" ></i>

                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                
                <div class="modal-footer" style="padding-right: 0; padding-left: 0; border: 0;">
                  
                   
                    <div class="pull-right">
                        <button type="button" ng-click="reservaAddNovo(aulaSelect)" class="btn btn-primary"><i class="fa fa-check"></i> Salvar</button>
                    </div>
                </div>


                
                
            </div>
           
        </div>
    </div>

   

</div>


  @include('admin/alunos/_form_cliente')
</div>

<style type="text/css">
    .chosen-container-single .chosen-single span {
    margin-top: 2px;
}
</style>



