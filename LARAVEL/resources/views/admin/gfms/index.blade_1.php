@extends('layouts.app')

@section('content')

@push('scripts')
{!! Html::script("js/gfmController.js") !!}
@endpush

<div class="wrapper wrapper-content animated fadeInRight" ng-controller="gfmController">
    <div class="row  border-bottom white-bg dashboard-header">
        <span class="label label-primary pull-right"><font>últimas 7 semanas</font></span>
        <div class="col-md-3" style="overflow-y: scroll; height: 500px">
            <h4>Taxa de Ocupação da Academia</h4>
            <h2>@{{ atingidoTotal}}%</h2>
            <div class="progress progress-mini">
                <div style="width:@{{ atingidoTotal}}%;" class="progress-bar"></div>
            </div>
            <div class="m-t-sm small">@{{ taxaOcupacao.totalReservas}}/@{{ taxaOcupacao.totalCapacidade}} Audiências</div>
            <hr>
            <h4>Taxa de Ocupação por Professor</h4>
            <ul class="list-group clear-list m-t">
                <li class="list-group-item fist-item" ng-repeat="professor in professores| orderBy : 'atingido' : true">
                    <span class="pull-right" style="margin-right: 10px">@{{ professor.atingido}}%</span>
                    <span class="label @{{ ($index == 0) ? 'label-primary' : 'label-default'}}">@{{ $index + 1}}</span> @{{ professor.name}}
                </li>
            </ul>
        </div>
        <div class="col-md-7">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div>
                        <h3 class="font-bold no-margins">Audiência Média</h3>
                        <small>últimas 7 semanas</small>
                    </div>
                    <div><iframe class="chartjs-hidden-iframe" style="width: 100%; display: block; border: 0px; height: 0px; margin: 0px; position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px;"></iframe>
                        <canvas id="lineChart" height="410" style="display: block; width: 1356px; height: 316px;" width="1356"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="statistic-box text-center">
                <h4>Audiência por Professor</h4>
                <p>Veja o melhor professor</p>
                <div class="row text-center">
                    <div class="col-lg-8 col-sm-offset-2">
                        <canvas id="doughnutChart" width="180" height="180" style="margin:18px auto 0">></canvas>
                    </div>
                    <div class="col-sm-2"></div>
                </div>
            </div>
        </div>
    </div> <!---->
    <div style="margin-top: 30px"></div>

    @include('admin.gfms.modal')


    <div class="ibox-title">
        <h5>Gymnastics Frame Management</h5>
        <div class="ibox-tools">
            <select class="form-control pull-right" placeholder="Local" style="width:200px;">
                <option value="1">Última semana</option>
                <option value="2">Último mês</option>
            </select>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="ibox-content">
        <div class="table-responsive">
            <table class="footable table table-striped table-bordered table-hover dataTables-example dataTables-gfm-input">
                <thead>
                    <tr>
                        <th data-sort-ignore="true">Local</th>
                        <th data-sort-ignore="true">Hr. Início</th>
                        <th data-sort-ignore="true">Duração</th>
                        <th data-sort-ignore="true">Dia/Semana</th>
                        <th data-sort-ignore="true">Aula
                            <!--<button class="btn btn-primary btn-sm pull-right" aria-hidden="true" data-toggle="modal" data-target="#addAula">
                                <i class="fa fa-plus"></i>
                            </button>-->
                        </th>
                        <th data-sort-ignore="true">Professor</th>
                        <th data-sort-ignore="true">Valor</th>
                        <th data-sort-ignore="true">Capacidade</th>
                        <th data-sort-ignore="true">Fator</th>
                        <th>Taxa Ocupação</th>
                        <th>Média Real</th>
                        <th>Revisado</th>
                        <th data-sort-ignore="true">Del</th>
                        <th data-sort-ignore="true">Gravar</th>
                        <th data-sort-ignore="true">Cliente</th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="lcinza" ng-repeat="gfm in gfms| orderBy : 'id' : true">
                        <!-- EXIBE -->
                        <td ng-hide="editingData[gfm.id]">@{{ gfm.nmlocal}}</td>
                        <td ng-hide="editingData[gfm.id]">@{{ gfm.hora_inicio}}</td>
                        <td ng-hide="editingData[gfm.id]">@{{ gfm.duracao}} min</td>
                        <td ng-hide="editingData[gfm.id]">@{{ diaSemana(gfm.diasemana)}}</td>
                        <td ng-hide="editingData[gfm.id]">@{{ gfm.nmprograma}}</td>
                        <td ng-hide="editingData[gfm.id]">@{{ gfm.nmprofessor}}</td>
                        <td ng-hide="editingData[gfm.id]">@{{ gfm.valor | currency:'R$' }}</td>
                        <td ng-hide="editingData[gfm.id]">@{{ gfm.capacidade}}</td>
                        <td ng-hide="editingData[gfm.id]">@{{ gfm.fator}}</td>
                        <td ng-hide="editingData[gfm.id]">@{{ gfm.atingido}}%</td>
                        <td ng-hide="editingData[gfm.id]">@{{ gfm.media}}</td>
                        <td ng-hide="editingData[gfm.id]">@{{ gfm.revisado}}</td>
                        <td ng-hide="editingData[gfm.id]">
                            <i class="fa fa-trash" aria-hidden="true" ng-click="delGfm(gfm.id)" ng-disabled="delGfmBtn"></i>
                        </td>
                        <td ng-hide="editingData[gfm.id]">
                            <i class="fa fa-pencil" aria-hidden="true" ng-click="alterGfm(gfm)" ng-disabled="editGfmBtn"></i>
                        </td>
                        <!-- FECHA EXIBE -->
                        <!-- EDITA -->
                        <td ng-show="editingData[gfm.id]">
                            <select ng-model="gfm.idlocal" placeholder="Local" class="form-control">
                                <option value="" disabled selected>Selecione o local</option>
                                <option ng-repeat="local in locais" ng-value="local.id">@{{local.nmlocal}}</option>
                            </select>
                        </td>
                        <td ng-show="editingData[gfm.id]">
                            <input type="text" ng-model="gfm.hora_inicio" placeholder="Hora Início" class="form-control">
                        </td>
                        <td ng-show="editingData[gfm.id]">
                            <input type="text" ng-model="gfm.duracao" placeholder="Duração" class="form-control">
                        </td>
                        <td ng-show="editingData[gfm.id]">
                            <select ng-model="gfm.diasemana" placeholder="Local" class="form-control">
                                <option value="" disabled selected>Selecione o dia</option>
                                <option value="1">Domingo</option>
                                <option value="2">Segunda</option>
                                <option value="3">Terça</option>
                                <option value="4">Quarta</option>
                                <option value="5">Quinta</option>
                                <option value="6">Sexta</option>
                                <option value="7">Sábado</option>
                            </select>
                        </td>
                        <td ng-show="editingData[gfm.id]">
                            <!--<div class="input-group m-b" ng-hide="clickede">-->
                                <select ng-model="gfm.idprograma" placeholder="Local" class="form-control">
                                    <option value="" disabled selected>Programa</option>
                                    <option ng-repeat="programa in programas" ng-value="programa.id">@{{programa.nmprograma}}</option>
                                </select>
                                <!--<a ng-init="clickede = false" ng-click="clickede = !clickede" class="btn btn-primary input-group-addon"><i class="fa fa-plus" aria-hidden="true"></i></a>-->
                            <!--</div>-->
                            <!--<div class="input-group m-b" ng-show="clickede">
                                    <input class="form-control" ng-model="gfm.nmprograma" placeholder="Nome">
                                    <a ng-click="clickede = !clickede" class="btn btn-primary input-group-addon"><i class="fa fa-undo" aria-hidden="true"></i></a>
                            </div>-->
                        </td>
                        <td ng-show="editingData[gfm.id]"> 
                            <select ng-model="gfm.idfuncionario" placeholder="Professor" class="form-control">
                                <option value="" disabled selected>Selecione o professor</option>
                                <option ng-repeat="professor in professores" ng-value="professor.id">@{{professor.name}}</option>
                            </select>
                        </td>
                        <td ng-show="editingData[gfm.id]">
                            <input type="text" ng-model="gfm.valor" placeholder="Valor" class="form-control">
                        </td>
                        <td ng-show="editingData[gfm.id]">
                            <input type="text" ng-model="gfm.capacidade" placeholder="Capacidade" class="form-control">
                        </td>
                        <td ng-show="editingData[gfm.id]">
                            <input type="text" ng-model="gfm.fator" placeholder="Fator" class="form-control">
                        </td>
                        <td ng-show="editingData[gfm.id]">@{{ gfm.atingido}}%</td>
                        <td ng-show="editingData[gfm.id]">@{{ gfm.media}}</td>
                        <td ng-show="editingData[gfm.id]">@{{ gfm.revisado}}</td>
                        <td ng-show="editingData[gfm.id]">
                            <i class="fa fa-trash" aria-hidden="true" ng-click="delGfm(gfm.id)" ng-disabled="delGfmBtn"></i>
                        </td>
                        <td ng-show="editingData[gfm.id]">
                            <i class="fa fa-save" aria-hidden="true" ng-click="upGfm(gfm)" ng-disabled="saveGfmBtn"></i>
                        </td>

                        <td >
                            <i class="fa fa-plus" aria-hidden="true" data-toggle="modal" data-target="#addCliente" ng-click="getAulasPassadas(gfm.id, gfm.nmprofessor, gfm.nmprograma, gfm.hora_inicio)" ng-disabled="addClienteSpmBtn"></i>
                        </td>
                        <!-- FECHA EDITA -->
                    </tr>
                </tbody>
                <tfoot>
                <th></th>
                <th></th>
                <th class="text-center">@{{ duracaoTotal}} min</th>
                <th></th>
                <th></th>
                <th></th>
                <th class="text-center">@{{ valorTotal | currency:'R$' }}</th>
                <th class="text-center">@{{ capacidadeTotal}}</th>
                <th></th>
                <th class="text-center">@{{ atingidoTotal}}%</th>
                <th class="text-center">@{{ mediaTotal}}</th>
                <th></th>
                <th colspan="3">
                <div class="pull-right">
                    <button class="btn btn-primary btn-sm" type="button" ng-click="addGfm()" ng-disabled="addGfmBtn">
                        <i class="fa fa-plus"></i> Nova Aula
                    </button>
                </div>
                </th>
                </tfoot>
            </table>
        </div>
    </div>
    <div aria-hidden="true" class="modal inmodal fade" id="addAula" role="dialog" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                    <h4 class="modal-title">Adicionar Aula</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Nome da Aula:</label>
                        <input type="text" class="form-control" ng-model="gfm.nmprograma">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" ng-click="adicionaAula(gfm.nmprograma)" class="btn btn-primary"><i class="fa fa-check"></i> Adicionar</button>
                </div>
            </div>
        </div>
    </div>        
</div>
@endsection

@section('css')
{!! Html::style("tema_assets/css/plugins/clockpicker/clockpicker.css") !!}
@endsection

@section('scripts')
<!-- FooTable -->
{!! Html::script("tema_assets/js/plugins/footable/footable.all.min.js") !!}
<!-- ChartJS demo data-->
{!! Html::script("tema_assets/js/plugins/chartJs/Chart.min.js") !!}
<!-- Clock picker -->
{!! Html::script("tema_assets/js/plugins/clockpicker/clockpicker.js") !!}
<!-- sparkline -->
{!! Html::script("tema_assets/js/plugins/sparkline/jquery.sparkline.min.js") !!}
<!-- sparkline -->
{!! Html::script("tema_assets/js/demo/sparkline-demo.js") !!}
<!-- chartJs -->
{!! Html::script("tema_assets/js/plugins/chartJs/Chart.min.js") !!}
<!-- Page-Level Scripts -->
<script>
            $(document).ready(function () {
    $('.clockpicker').clockpicker();
            $('.footable').footable();
    });</script>
<script>
            $(document).ready(function() {
    var grafico1 = [];
            $.get('api/gfm/getGfmsGrafico1', function(retorno1) {
            grafico1 = retorno1;
                    console.log(grafico1);
            });
            var grafico2 = [];
            $.get('api/gfm/getGfmsGrafico2', function(retorno2) {
            grafico2 = retorno2;
                    console.log(grafico2);
            });
            setTimeout(function(){
            var lineData = {
            labels: ["-6 Sem", "-5 Sem", "-4 Sem", "-3 Sem", "-2 Sem", "-1 Sem", "Semana Atual"],
                    datasets: [
                    {
                    label: "Taxa de Ocupação",
                            backgroundColor: "rgba(26,179,148,0.5)",
                            borderColor: "rgba(26,179,148,0.7)",
                            pointBackgroundColor: "rgba(26,179,148,1)",
                            pointBorderColor: "#fff",
                            data: grafico1
                    },
                    {
                    label: "Capacidade",
                            backgroundColor: "rgba(220,220,220,0.5)",
                            borderColor: "rgba(220,220,220,1)",
                            pointBackgroundColor: "rgba(220,220,220,1)",
                            pointBorderColor: "#fff",
                            data: grafico2
                    }
                    ]
            };
                    var lineOptions = {
                    responsive: true
                    };
                    var ctx = document.getElementById("lineChart").getContext("2d");
                    new Chart(ctx, {type: 'line', data: lineData, options:lineOptions});
            }, 2000);
            var doughnutData = {
            labels: ["App", "Software", "Laptop"],
                    datasets: [{
                    data: [300, 50, 100],
                            backgroundColor: ["#a3e1d4", "#dedede", "#9CC3DA"]
                    }]
            };
            var doughnutOptions = {
            responsive: false,
                    legend: {
                    display: false
                    }
            };
            var ctx4 = document.getElementById("doughnutChart").getContext("2d");
            new Chart(ctx4, {type: 'doughnut', data: doughnutData, options:doughnutOptions});
    });
</script>
@endsection