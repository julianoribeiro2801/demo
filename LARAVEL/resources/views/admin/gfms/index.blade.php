@extends('layouts.app')
@section('content')




@push('scripts')
{!! Html::script("js/gfmService.js") !!}
{!! Html::script("js/gfmController.js") !!}
{!! Html::script("js/alunoController.js") !!}

<!-- d3 and c3 charts -->
{!! Html::script("tema_assets/js/plugins/d3/d3.min.js") !!}
{!! Html::script("tema_assets/js/plugins/c3/c3.min.js") !!}
@endpush

<!-- c3 Charts -->
{!! Html::style("tema_assets/css/plugins/c3/c3.min.css") !!}



<div class="wrapper wrapper-content animated fadeInRight" ng-controller="gfmController">
    
    {{-- askjhaskjashaksjhjk --}}
    <div class="row  border-bottom white-bg dashboard-header">
       
        <div class="col-xs-12 col-md-3 txpor_prof" >
            <h4>Taxa de Ocupação da Academia</h4>
            <h2>@{{ taxaOcupacao.media}}%</h2>
            <div class="progress progress-mini">
                <div style="width:@{{ taxaOcupacao.media }}%;" class="progress-bar"></div>
            </div>
            <div class="m-t-sm small">@{{ taxaOcupacao.totalReservas}}/@{{ taxaOcupacao.totalCapacidade}} Audiências</div>
            <hr>

            <h4>Taxa de Ocupação por Professor</h4>
            <ul class="list-group clear-list m-t">
                <li class="list-group-item fist-item" ng-repeat="ocupacao in ocupacaoProfs | orderBy : 'perc' : true">
                    <span class="pull-right" style="margin-right: 10px">@{{ ocupacao.perc}}%</span>
                    <span class="label @{{ ($index == 0) ? 'label-primary' : 'label-default'}}">@{{ $index + 1}}</span> @{{ ocupacao.name}}
                </li>
            </ul>
        </div>


        <div class="col-xs-12 col-md-9">
             <div class="titulo">
                 <h4>Audiência Média</h4>
               
                 <span class="label label-primary pull-right">
                    <font>últimas 7 semanas
                </font></span>
            </div>
            <div class="grafico_conteudo">
                <svg id="graficoAudiencia" class="chart" ></svg>
            
          
            </div>
        </div>
      
        </div> <!---->
        <div style="margin-top: 30px"></div>

        {{-- {!!  url()->current(); !!} --}}
<?php                
  $parameters = \Request::segment(2);
?>

 <input type="hidden" id="type_modalidade" value="{{$parameters}}">
 
        @include('admin.gfms.modal')
        @include('admin.gfms.modal_privado')
        <div class="row  border-bottom white-bg dashboard-header">
            
           <div class="col-xs-12 col-md-12">
                
                <div class="row">
                    <div class="col-xs-6 col-sm-8" style="padding:0">
                        <h4>Gymnastics Frame Management</h4>
                        
                    </div>
                    
                    <div class="col-xs-6 col-sm-4" style="padding:0">
                        
                        <select class="form-control pull-right" placeholder="Local" style="width:140px;">
                            <option value="1">Última semana</option>
                            <option value="2">Último mês</option>
                        </select>
                        
                    </div>
                </div>
                  
             <div class="row">   
               <hr>     

                <span class="ruim" style=" padding: 1px 10px; margin-right: 5px;">  </span> Ruim (até @{{configura.indice_inicial}}%)
                <span class="regular" style="padding: 1px 10px; margin-right: 5px;     margin-left: 12px;">  </span> Regular (até @{{configura.indice_final}}%)
                <span  class="boa" style=" padding: 1px 10px; margin-right: 5px;     margin-left: 12px;">  </span> Boa (acima de @{{configura.indice_final}}%)


                 <div class="table-responsive" style="margin-top:20px" >
            <table class="footable table table-striped table-bordered table-hover dataTables-example dataTables-gfm-input">

                <thead>
                    <tr>
                        <th data-sort-ignore="true">Cliente</th>

                        <th data-sort-ignore="true">Aula </th>
                        <th data-sort-ignore="true">Local</th>
                        <th data-sort-ignore="true">Dia/Semana</th>
                        <th data-sort-ignore="true">Hr. Início</th>
                        <th data-sort-ignore="true">Duração</th>
                        <th ng-hide="true" data-sort-ignore="true">Gfm?</th>
                        <th data-sort-ignore="true">Objetivo</th>
                        <th data-sort-ignore="true">Nível</th>
                        <th data-sort-ignore="true">Turma Privada</th>
                        <th data-sort-ignore="true">Professor</th>
                        <th data-sort-ignore="true">Valor</th>
                        <th data-sort-ignore="true">Capac. Min.</th>
                        <th data-sort-ignore="true">Capac. Max.</th>
                        {{-- <th>Ocup. atual</th> --}}
                        {{-- <th>Vacância</th> --}}
                        <th>Audiência Média</th>
                        {{-- <th data-sort-ignore="true">Faixa etária</th> --}}
                        <th data-sort-ignore="true">Equalizador</th>
                        <th>Revisado</th>
                        <th data-sort-ignore="true">Del</th>
                        <th data-sort-ignore="true">Editar</th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="lcinza" ng-repeat="gfm in gfms| orderBy : 'id' : true">
                         <td >
                            <i class="fa fa-plus" aria-hidden="true"  ng-click="getAulasPassadas(gfm.id, gfm.nmprofessor, gfm.nmprograma, gfm.hora_inicio, gfm.privado,'',gfm.objetivo)" ng-disabled="addClienteSpmBtn"></i>
                        </td>
                        <!-- EXIBE -->
                        <td ng-hide="editingData[gfm.id]">@{{ gfm.nmprograma}}</td>
                        <td ng-hide="editingData[gfm.id]">@{{ gfm.nmlocal}}</td>
                        <td ng-hide="editingData[gfm.id]">@{{ diaSemana(gfm.diasemana)}}</td>
                        <td ng-hide="editingData[gfm.id]">@{{ gfm.hora_inicio}}</td>
                        <td ng-hide="editingData[gfm.id]">@{{ gfm.duracao}} min</td>
                        <td ng-hide="true" class="text-center">@{{ gfm.gfmspm}}</td>
                        <td ng-hide="editingData[gfm.id]">@{{ gfm.nmobjetivo}}</td>
                        <td ng-hide="editingData[gfm.id]">@{{ gfm.nmnivel}}</td>
                        <td ng-hide="editingData[gfm.id]" class="text-center">@{{ gfm.privado}}</td>
                        <td ng-hide="editingData[gfm.id]">@{{ gfm.nmprofessor}}</td>
                        <td ng-hide="editingData[gfm.id]">@{{ gfm.valor | currency:'R$' }}</td>
                        <td ng-hide="editingData[gfm.id]">@{{ gfm.capacidademin}}</td>
                        <td ng-hide="editingData[gfm.id]">@{{ gfm.capacidade}}</td>
                        
                         {{-- <td ng-hide="editingData[gfm.id]"   >@{{ gfm.atingido}}</td> --}}
                        <!-- numero absoluto-->
                        {{-- <td ng-hide="editingData[gfm.id]">@{{ gfm.vacancia}}</td> --}}
                        <td ng-hide="editingData[gfm.id]"
                         ng-class="{ruim: gfm.media < 5, regular: gfm.media > 5 && gfm.media < 8 , boa: gfm.media > 8}"
                         >@{{ gfm.media}}</td>
                     {{--    <td ng-hide="editingData[gfm.id]">@{{ gfm.faixaetariaini}}/@{{ gfm.faixaetariafim}}</td> --}}
                        <td ng-hide="editingData[gfm.id]">@{{ gfm.fator}}</td>
                        <td ng-hide="editingData[gfm.id]">@{{ gfm.revisado}}</td>

                        <td ng-hide="editingData[gfm.id]">
                            <i class="fa fa-trash" aria-hidden="true" ng-click="delGfm(gfm.id)" ng-disabled="delGfmBtn"></i>
                        </td>
                        <td ng-hide="editingData[gfm.id]">
                            <i class="fa fa-pencil" aria-hidden="true" ng-click="alterGfm(gfm)" ng-disabled="editGfmBtn"></i>
                        </td>
                        <!-- FECHA EXIBE -->
                        <!-- EDITA -->
                        <td ng-show="editingData[gfm.id]">
                            <!--<div class="input-group m-b" ng-hide="clickede">-->
                            <select ng-model="gfm.idprograma" placeholder="Local" class="form-control">
                                <option value="" disabled selected>Programa</option>
                                <option ng-repeat="programa in programas" ng-value="programa.id">@{{programa.nmprograma}}</option>
                            </select>
                            <!--<a ng-init="clickede = false" ng-click="clickede = !clickede" class="btn btn-primary input-group-addon"><i class="fa fa-plus" aria-hidden="true"></i></a>-->
                            <!--</div>-->
                            <!--<div class="input-group m-b" ng-show="clickede">
                                    <input class="form-control" ng-model="gfm.nmprograma" placeholder="Nome">
                                    <a ng-click="clickede = !clickede" class="btn btn-primary input-group-addon"><i class="fa fa-undo" aria-hidden="true"></i></a>
                            </div>-->
                        </td>                        
                        <td ng-show="editingData[gfm.id]">
                            <select ng-model="gfm.idlocal" placeholder="Local" class="form-control">
                                <option value="" disabled selected>Selecione o local</option>
                                <option ng-repeat="local in locais" ng-value="local.id">@{{local.nmlocal}}</option>
                            </select>
                        </td>
                        <td ng-show="editingData[gfm.id]">
                            <label>
                                D <br><input type="checkbox" ng-init="checked = true" ng-model="gfm.domingo" value="true">
                            </label>
                            <label>
                                2ª <br><input type="checkbox" ng-init="checked = true" ng-model="gfm.segunda" value="true">
                            </label>
                            <label>
                                3ª <br><input type="checkbox" ng-init="checked = true" ng-model="gfm.terca" value="true">
                            </label>
                            <label>
                                4ª <br><input type="checkbox" ng-init="checked = true" ng-model="gfm.quarta" value="true">
                            </label>
                            <br>
                            <label>
                                5ª <br><input type="checkbox" ng-init="checked = true" ng-model="gfm.quinta" value="true">
                            </label>
                            <label>
                                6ª <br><input type="checkbox" ng-init="checked = true" ng-model="gfm.sexta" value="true">
                            </label>
                            <label>
                                S <br><input type="checkbox" ng-init="checked = true" ng-model="gfm.sabado" value="true">
                            </label>                            
                         
                        </td>                        
                        <td ng-show="editingData[gfm.id]">
                            <input type="text" data-mask="99:99" ng-model="gfm.hora_inicio" placeholder="Hora Início" class="form-control">
                        </td>
                        <td ng-show="editingData[gfm.id]">
                            <input type="text" ng-model="gfm.duracao" placeholder="Duração" class="form-control">
                        </td>
                        <td ng-hide="true">
                            <label>
                                <input type="radio" ng-model="gfm.gfmspm" value="Gfm"> Gfm
                            </label>
                            <label>
                                <input type="radio" ng-model="gfm.gfmspm" value="Spm"> Spm
                            </label>
                            <!--<label>
                                <input type="radio" ng-model="gfm.gfmspm" value="Ambos"> Ambos
                            </label>-->
                        </td>
                        <td ng-show="editingData[gfm.id]">
                            <select ng-show="gfm.gfmspm == 'Gfm'" ng-model="gfm.objetivo" placeholder="Objetivo" class="form-control" ng-change="changeObjetivo(2, gfm.objetivo)">
                                <option value="" disabled selected>Selecione o objetivo</option>
                                <option ng-repeat="objetivo in objetivosginastica" ng-value="objetivo.id">@{{objetivo.nmobjetivo}}</option>
                            </select>
                            <select ng-show="gfm.gfmspm == 'Spm'" ng-model="gfm.objetivo" placeholder="Objetivo" class="form-control" ng-change="changeObjetivo(3, gfm.objetivo)">
                                <option value="" disabled selected>Selecione o objetivo</option>
                                <option ng-repeat="objetivo in objetivosnatacao" ng-value="objetivo.id">@{{objetivo.nmobjetivo}}</option>
                            </select>
                        </td>
                        <td ng-show="editingData[gfm.id]">
                            <select ng-model="gfm.nivel" placeholder="Nível" class="form-control">
                                <option value="" disabled selected>Selecione o nível</option>
                                <option ng-repeat="nivel in niveis" ng-value="nivel.id">@{{nivel.nmnivel}}</option>
                            </select>
                        </td>
                        <td ng-show="editingData[gfm.id]">
                            <label>
                                <input type="radio" ng-model="gfm.privado" value="Sim"> Sim
                            </label>
                            <label>
                                <input type="radio"  ng-model="gfm.privado" value="Não"> Não
                            </label>
                        </td>



                        <td ng-show="editingData[gfm.id]"> 
                            <select ng-model="gfm.idfuncionario" placeholder="Professor" class="form-control">
                                <option value="" disabled selected>Selecione o professor</option>
                                <option ng-repeat="professor in professores" ng-value="professor.id">@{{professor.name}}</option>
                            </select>
                        </td>
                        <td ng-show="editingData[gfm.id]">
                            <input type="text" ng-model="gfm.valor" placeholder="Valor" class="form-control">
                        </td>
                        <td ng-show="editingData[gfm.id]">
                            <input type="text" ng-model="gfm.capacidademin" placeholder="Capacidade" class="form-control">
                        </td>
                        <td ng-show="editingData[gfm.id]">
                            <input type="text" ng-model="gfm.capacidade" placeholder="Capacidade" class="form-control">
                        </td>
                        {{-- <td ng-show="editingData[gfm.id]">@{{ gfm.atingido}}</td> --}}
                        {{-- <td ng-show="editingData[gfm.id]">@{{ gfm.vacancia}}</td> --}}
                        <td ng-show="editingData[gfm.id]">@{{ gfm.media}}</td>
                   {{--      <td ng-show="editingData[gfm.id]">@{{ gfm.faixaetariaini}}/@{{ gfm.faixaetariafim}}</td> --}}
                        <td ng-show="editingData[gfm.id]">
                            <input type="text" ng-model="gfm.fator" placeholder="Fator" class="form-control">
                        </td>

                        <td ng-show="editingData[gfm.id]">@{{ gfm.revisado}}</td>
                        <td ng-show="editingData[gfm.id]">
                            <i class="fa fa-trash" aria-hidden="true" ng-click="delGfm(gfm.id)" ng-disabled="delGfmBtn"></i>
                        </td>
                        <td ng-show="editingData[gfm.id]">
                            <i class="fa fa-save" aria-hidden="true" ng-click="upGfm(gfm, 'Gfm')" ng-disabled="saveGfmBtn"></i>
                        </td>

                       
                        <!-- FECHA EDITA -->
                    </tr>
                </tbody>
                <tfoot>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                 <th class="text-center">@{{ duracaoTotal}} min</th>
                <th></th>
               
                <th></th>
                <th class="text-center">@{{ valorTotal | currency:'R$' }}</th>

                <th></th>

                <th class="text-center"></th>
                <th></th>
                
                <th  class="text-center"> 8 </th>
                <th class="text-center">@{{ capacidadeTotal}}</th>
                {{-- <th class="text-center">20%</th> --}}
                {{-- <th class="text-center">@{{ atingidoTotal}}%</th> --}}
                <th class="text-center">@{{ mediaTotal}}</th>
                <th></th>
                <th></th>
                
                
                <th colspan="2">
                <div class="pull-right">
                    <button class="btn btn-primary btn-sm" type="button" ng-click="addGfm()" ng-disabled="addGfmBtn">
                        <i class="fa fa-plus"></i> Nova Aula
                    </button>
                </div>
                </th>
                </tfoot>
            </table>
        </div>
    </div>




            </div>
        </div>
      {{-- fim Gymnastics Frame Management blc --}}
     <div style="margin-top: 30px"></div>


        <div aria-hidden="true" class="modal inmodal fade" id="addAula" role="dialog" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                        <h4 class="modal-title">Adicionar Aula</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Nome da Aula:</label>
                            <input type="text" class="form-control" ng-model="gfm.nmprograma">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" ng-click="adicionaAula(gfm.nmprograma)" class="btn btn-primary"><i class="fa fa-check"></i> Adicionar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection
    @section('css')
    {!! Html::style("tema_assets/css/plugins/clockpicker/clockpicker.css") !!}
    @endsection
    @section('scripts')
    <!-- FooTable -->
    {!! Html::script("tema_assets/js/plugins/footable/footable.all.min.js") !!}
    <!-- ChartJS demo data-->
    {!! Html::script("tema_assets/js/plugins/chartJs/Chart.min.js") !!}
    <!-- Clock picker -->
    {!! Html::script("tema_assets/js/plugins/clockpicker/clockpicker.js") !!}
    <!-- sparkline -->
    {!! Html::script("tema_assets/js/plugins/sparkline/jquery.sparkline.min.js") !!}
    <!-- sparkline -->
    {!! Html::script("tema_assets/js/demo/sparkline-demo.js") !!}
    <!-- chartJs -->
    {!! Html::script("tema_assets/js/plugins/chartJs/Chart.min.js") !!}
    {!! HTML::script("assets_admin/javascripts/jQueryMask/jquery.mask.js") !!}
    {!! HTML::script("assets_admin/javascripts/jQueryMask/jquery.mask.min.js") !!}
    {!! HTML::script("assets_admin/javascripts/jQueryMask/jquery.mask.test.js") !!}
    <!-- Page-Level Scripts -->

<script>
    $(document).ready(function () {
        $('.clockpicker').clockpicker();
        $('.footable').footable();
    });
</script>

<style>
    
.c3-line {
    stroke-width: 2px;
}

.chart{
      width: 100%;
     min-height: 350px;
    }

@media (max-width:800px) {
    .chart{
      width: 110%;
      min-height: 300px;
      margin-left: -20px
    }
    /*
    .chart .c3-legend-item {
       
         margin-top: 10px;
         top:10px;
         padding-top:10px
    }*/
}

</style>

@endsection