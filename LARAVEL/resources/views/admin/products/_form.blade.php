<div class="form-group">
	{!! Form::label('Category', 'Categoria:') !!}
	{!! Form::select('category_id', $selectedCategories, null, ['class'=>'form-control']) !!}
	{{-- {!! Form::select('size', array('0' => 'Teste', 'S' => 'Small'), 'S') !!} --}}
</div>

<div class="form-group">
	{!! Form::label('Name', 'Nome:') !!}
	{!! Form::text('name', null, ['class'=>'form-control']) !!}
</div>
<div class="form-group">
	{!! Form::label('Description', 'Descricao:') !!}
	{!! Form::textarea('description', null, ['class'=>'form-control']) !!}
</div>
 <div class="form-group">
	{!! Form::label('Price', 'Preço:') !!}
	{!! Form::text('price', null, ['class'=>'form-control']) !!}
</div>