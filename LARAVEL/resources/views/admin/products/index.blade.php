@extends('layouts.app')

@section('content')

<div class="container">
	<h3> Produtos</h3>
	<br>
	<a href="{{ route('admin.empresa.products.create') }}" class="btn btn-primary"> Novo Produto</a>
	<br><br>
	<table class="table table-bordered">
		<thead>
			<tr>
				<th>ID</th>
				<th>Produto</th>
				<th>Categoria</th>
				<th>Preço</th>
				<th>Ação</th>
			</tr>
		</thead>
		<tbody>
		@foreach ($products as $product)
			<tr>
				<td> {{$product->id}}</td>
				<td> {{$product->name}}</td>
				<td> {{$product->category->name}} </td>	
				<td> {{$product->price}} </td>				

				<td> 
					<a href="{{route('admin.empresa.products.edit', $product->id)}}" class="btn btn-info">Editar</a>
					<a href="{{route('admin.empresa.products.destroy', $product->id)}}" class="btn btn-danger">Del</a>


				</td>
			</tr>
			@endforeach
		</tbody>
	</table>

	{{{ $products->render()}}}

	
</div>

@endsection

