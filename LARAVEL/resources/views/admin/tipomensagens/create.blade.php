@extends('layouts.app')

@section('content')

<div class="container">
	<h3> Novo tipo de mensagem</h3>
	
	{{-- @include('errors._check') --}}

	{!! Form::open(array('route' => 'admin.clientes.tipomensagens.store', 'class'=>'form')) !!}
   
    @include('admin.tipomensagens._form')

   	<div class="form-group">   		
   	{!! Form::submit('Criar ', ['class'=>'btn btn-primary']) !!}
   	</div>
	
	{!! Form::close() !!}

	
	
</div>

@endsection

