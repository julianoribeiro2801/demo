@extends('layouts.app')

@section('content')

<div class="container">
	<h3> Editando tipo conquista: {{$tipomensagem->dstipomensagem}} </h3>
	
	 @include('errors._check')
	
	{!!  Form::model($tipomensagem, array('route' => array('admin.clientes.tipomensagens.update', $tipomensagem->id))) !!}
   	
    @include('admin.tipomensagens._form')

   	<div class="form-group">   		
   		<button class="btn btn-primary " type="submit"><i class="fa fa-check"></i>&nbsp;Salvar</button> 
   	</div>
	
	{!! Form::close() !!}
	
	
</div>

@endsection

