@extends('layouts.app')

@section('content')
<div class="ibox float-e-margins">
     @include('admin.atividades.tools_form')

    <div style="display: block;" class="ibox-content">
     
	<h3> Editando atividade: {{$atividade->nmatividade}} </h3>
	
	 @include('errors._check')
	
	{!!  Form::model($atividade, array('route' => array('admin.empresa.atividades.update', $atividade->id))) !!}
   	
    @include('admin.atividades._form')

   	<div class="form-group">   		
   		<button class="btn btn-primary " type="submit"><i class="fa fa-check"></i>&nbsp;Salvar</button> 
   	</div>
	
	{!! Form::close() !!}
	
    </div>
</div>

@endsection

