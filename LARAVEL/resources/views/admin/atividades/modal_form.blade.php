<div id="modal-form" class="modal fade" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-6">
                         
                        <div class="ibox float-e-margins">
                            

                           <div style="display: block;" class="ibox-content">
                               <h3> Nova atividade</h3>

                               {{-- @include('errors._check') --}}

                               {!! Form::open(array('route' => 'admin.empresa.atividades.store', 'class'=>'form')) !!}

                               @include('admin.atividades._form')

                               <div class="form-group">   		
                               {!! Form::submit('Criar ', ['class'=>'btn btn-primary']) !!}
                               </div>

                               {!! Form::close() !!}


                           </div>
</div>
                    </div>

            </div>
        </div>
      </div>
    </div>
</div>