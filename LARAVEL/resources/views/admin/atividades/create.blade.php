@extends('layouts.app')

@section('content')
<div class="ibox float-e-margins">
     @include('admin.atividades.tools_form')

    <div style="display: block;" class="ibox-content">
        <h3> Nova atividade</h3>
	
	{{-- @include('errors._check') --}}

	{!! Form::open(array('route' => 'admin.empresa.atividades.store', 'class'=>'form')) !!}
   
        @include('admin.atividades._form')

   	<div class="form-group">   		
   	{!! Form::submit('Criar ', ['class'=>'btn btn-primary']) !!}
   	</div>
	
	{!! Form::close() !!}

	
    </div>
</div>

@endsection

