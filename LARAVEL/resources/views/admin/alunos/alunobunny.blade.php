@extends('layouts.app')

@section('content')


    {{-- INICIO TABALE. --}}
    <div class="wrapper wrapper-content  animated fadeInRight" ng-controller="alunoController">
        @include('admin.alunos.modal_contato')
        @include('admin.alunos.modal_novo_prospect')
        <div class="row">

            <div class="col-sm-12 col-md-12" ng-cloak>
                <div class="ibox sk-loading">
                    <div class="ibox-content">
                        {{--<h4>Registros @{{ totalRegistros }} </h4>--}}
                        <div class="ibox-tools">
                            <div class="btn-group">
                                <button ng-click="prospect_modal()" id="btn-novo-prospect" class="btn btn-primary" type="submit">
                                    <i class="fa fa-plus-square"></i>
                                     Nova Oportunidade / Cliente
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="ibox-content">

                        <div class="form-group">
                            <form ng-submit="search(event)" method="get">
                                <div class="input-group">
                                    <input type="text" placeholder="Buscar cliente" ng-model="query" 
                                    name="search" ng-change="search()" class="form-control">
                                    <div class="input-group-btn">
                                        <button class="btn btn-primary" type="submit">
                                            <span class="loading dots"></span>
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>


                        <div class="Clientes-list" style="margin-top: 20px;">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover display dt-responsive responsive no-wrap" style="width: 100%">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>#</th>
                                            <th>Nome</th>
                                            <th>Celular</th>
                                            <th>Email</th>
                                            <th style="text-align: center;" >App</th>
                                            <th style="text-align: center;" > Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <tr ng-if="!persons.total || searching">
                                        <td ng-if="!persons.total && !searching" colspan="7" class="text-center p-md text-danger"> Nenhum cliente para mostrar </td>
                                        <td ng-if="searching" colspan="7" class="text-center p-md text-primary"> Atualizando dados ....</td>
                                    </tr>
                                    <tr ng-if="persons.total && !searching" ng-repeat="(key, person)  in persons.data ">

                                        <td class="client-avatar">
                                            <img alt="image"
                                                 src="{{ url('/') }}/uploads/avatars/@{{ person.avatar || 'tema_assets/img/a4.jpg' }}">
                                        </td>
                                        <td>
                                            <a class="person-link"  data-toggle="modal" data-target="#editar_cliente">
                                                @{{ (persons.total - $index)  - page }}
                                                {{--@{{ (persons.data.length - $index) + page  }}--}}
                                            </a>
                                        </td>
                                        <td>
                                             <a ng-click="select(person)"  data-toggle="modal" data-target="#editar_cliente"               
                                               class="person-link">@{{ person.name }}
                                               <!--class="person-link textoMaiusculo">@{{ person.name }}-->
                                            </a>
                                        </td>
                                        <td > @{{ person.dados.celular }}</td>

                                        <td> @{{ person.email }}</td>

                                        <td style="text-align: center;"> 
                                            <i class="fa fa-check" aria-hidden="true" ng-if="person.device_id!==''" style="color: #1cb394;"></i>
                                            <i class="fa fa-exclamation" ng-if="person.device_id==''" aria-hidden="true" style="color: #d90000"></i>
                                        </td>

                                        <td ng-show="person.role == 'prospect'" style="text-align: center;" class="client-status" ng-click="convert(person)"  >
                                            <span class="label label-warning" style="cursor: pointer" ng-if="person.role == 'prospect'">Oportunidade</span>
                                        </td>
                                        <td ng-show="person.role == 'cliente'" style="text-align: center;" class="client-status" ng-click="convert(person)">
                                            <span class="label label-primary" style="cursor: pointer" ng-if="person.role == 'cliente'">Cliente</span>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>

                                <div class="text-center">
                                   
                                    <nav aria-label="Page navigation">
                                        <ul class="pagination">
                                            <li  ng-class="{'page-item' : true, 'disabled' : !persons.prev_page_url}">
                                                <a class="page-link" ng-click="paginateUrl(persons.prev_page_url)" aria-label="Voltar">
                                                    <span aria-hidden="true">&laquo;</span>
                                                    <span class="sr-only">Voltar</span>
                                                </a>
                                            </li>
                                            
                                            {{-- // com isso temos um limit de 1mil alunos por academia 
                                                porque são 50 alunos por pagina--}}
                                            <li ng-if="persons.last_page > $index"  ng-repeat="n in [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]"class="page-item">
                                                <a ng-click=paginateUrl("/api/cliente/search?page="+($index+1)) class="page-link" href="#@{{$index + 1}}">@{{ $index + 1 }}</a> 
                                            </li>
                                          

                                           {{--  <li ng-if="persons.total  >=  persons.per_page" class="page-item">
                                                <a ng-click=paginateUrl("/api/cliente/search?page="+persons.last_page) class="page-link" href="#">@{{ persons.last_page }}</a>
                                            </li> --}}



                                            <li ng-class="{'page-item' : true, 'disabled' : !persons.next_page_url}">
                                                <a class="page-link" ng-click="paginateUrl(persons.next_page_url)" aria-label="Next">
                                                    <span aria-hidden="true">&raquo;</span>
                                                    <span class="sr-only">Proximo</span>
                                                </a>
                                            </li>

                                        </ul>
                                    </nav>
                                </div>
                            </div>
                                    
                        </div>
                    </div>
                </div>
            </div> {{-- fim col --}}
            <div class="col-sm-12 col-md-4" ng-cloak>
                @include('admin.alunos._form_cliente')
            </div> {{-- fim col-sm-4  aluno--}}
        </div>
    </div>

@endsection
@push('scripts')
    {!! Html::script("js/alunoController.js") !!}
@endpush