@extends('layouts.app')
@section('content')
<!-- FooTable -->
{!! Html::style("tema_assets/css/plugins/footable/footable.core.css") !!}
{{-- INICIO TABALE. --}}

<div class="wrapper wrapper-content  animated fadeInRight" ng-controller="segmentacaoPublicoController">
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5>Segmentação de Público</h5>
					<div class="ibox-tools">
						<button class="btn btn-primary">
						<i class="fa fa-plus-square"></i>
						Nova Segmentação
						</button>
					</div>
				</div>
				<div class="ibox-content">
					
					<table class="personal-toggle table table-stripped toggle-arrow-tiny">
						<thead>
							<tr>
								
								<th > Público</th>
								<th>Tamanho </th>
								<th>Ativo</th>
								
							</tr>
						</thead>
						<tbody>
							{{-- Linha pai --}}
							<tr>
								<td class="clicavel" data-index="1">
									<span class="pull-left" style="margin-right: 10px">
										<i class="fa fa-plus bt-nivel_seg1"> </i>
									</span> <b> Natação Toledo </b>
								</td>
								<td>84</td>
								<td>
									<a href="#"><i class="fa fa-check text-navy"></i></a>
									
								</td>
							</tr>
							{{--
							####### subnivel --}}
							<tr class="oculta nivel_seg1">
								<td colspan="3" style="    background-color: #e7eaec;
									padding: 10px 20px;">
									
									<table class="table">
										
										<tr>
											<th width="150">
												Nome do Público:
											</th>
											<td >
												<input type="text" name="" id="input" class="form-control" value="Natação Toledo" required="required" pattern="" title="">
											</td>
										</tr>
										<tr>
											<th width="150">
												Status:
											</th>
											<td>
												<select name="input" id="input" class="form-control input-sm"  style="width:200px">
													<option value="">Cliente </option>
													<option value="">Oportunidade</option>
													
												</select>
											</td>
										</tr>
										<tr>
											<th width="150">
												Sexo:
											</th>
											<td>
												<select name="input" id="input" class="form-control input-sm" style="width:200px" >
													<option value="">Todos</option>
													<option value="">MASCULINO</option>
													<option value="">FEMININO</option>
												</select>
											</td>
										</tr>


										{{-- ajkdsjhaksjhdajkshdsakjhdsakj --}}
											<tr>
												<th width="150">
													Idade:
												</th>
												<td>
													<form action="" method="POST" class="form-inline" role="form">
														<div class="form-group">
															
															<select name="input" id="input" class="form-control input-sm" >
																<option value="">18</option>
																<option value="">19</option>
																<option value="">20</option>
																<option value="">21</option>
																<option value="">22</option>
															</select>
														</div>
														<div class="form-group">
															a
															<select name="input" id="input" class="form-control input-sm" >
																<option value="">35</option>
																<option value="">36</option>
																<option value="">37</option>
																<option value="">38</option>
																<option value="">39</option>
															</select> anos
														</form>
													</td>
												</tr>
											
				<tr>
					<th width="150">
						Com obs. médica :
					</th>
					<td>
						<div class="col-md-3"><label class="checkbox_v2"><input type="checkbox" value="">Sim </label></div>
						<div class="col-md-3"><label class="checkbox_v2"><input type="checkbox" value="">Não </label></div>
					</td>
				</tr>


				<tr>
					<th width="150">
						Perfil Disc :
					</th>
					<td>
						<div class="col-md-3"><label class="checkbox_v2"><input type="checkbox" value="">TODOS</label></div>
						<div class="col-md-3"><label class="checkbox_v2"><input type="checkbox" value="">Dominancia </label></div>
						<div class="col-md-3"><label class="checkbox_v2"><input type="checkbox" value="">Criativo </label></div>
						<div class="col-md-3"><label class="checkbox_v2"><input type="checkbox" value="">Carismatico </label></div>
					</td>
				</tr>


										
									
											{{-- fim da linha --}}
											

												<tr>
													<th width="150">
														Praticante da Modalidade:
													</th>
													<td>
														<div class="col-md-3">
															<label class="checkbox_v2"><input type="checkbox" value="">TODAS</label></div>
															<div class="col-md-3"><label class="checkbox_v2"><input type="checkbox" value="">MUSCULAÇÃO </label> </div>
															<div class="col-md-3"><label class="checkbox_v2"><input type="checkbox" value="">GINÁSTICA </label> </div>
															<div class="col-md-3"><label class="checkbox_v2"><input type="checkbox" value="">NATAÇÃO </label> </div>
															<div class="col-md-3"><label class="checkbox_v2"><input type="checkbox" value="">CROSS TRAINING
														</label> </div>
														<div class="col-md-3"><label class="checkbox_v2"><input type="checkbox" value="">CORRIDA </label> </div>
														<div class="col-md-3"><label class="checkbox_v2"><input type="checkbox" value="">CICLISMO </label> </div>
													</td>
												</tr>
												<tr>
													<th width="150">
														Praticante da Aula:
													</th>
													<td>
														<div class="col-md-3"><label class="checkbox_v2"><input type="checkbox" value="">TODAS</label> </div>
														<div class="col-md-3"><label class="checkbox_v2"><input type="checkbox" value="">Zumba </label> </div>
														<div class="col-md-3"><label class="checkbox_v2"><input type="checkbox" value="">Jump </label> </div>
														<div class="col-md-3"><label class="checkbox_v2"><input type="checkbox" value="">Alongamento </label> </div>
													</td>
												</tr>
												<tr>
													<th width="150">
														Objetivo:
													</th>
													<td>
														<div class="col-md-3"><label class="checkbox_v2"><input type="checkbox" value="">TODOS</label></div>
														<div class="col-md-3"><label class="checkbox_v2"><input type="checkbox" value="">Hipertrofia </label></div>
														<div class="col-md-3"><label class="checkbox_v2"><input type="checkbox" value="">Emagrecimento </label></div>
														<div class="col-md-3"><label class="checkbox_v2"><input type="checkbox" value="">Fortaleciomento </label></div>
													</td>
												</tr>
												<tr>
													<th width="150">
														Nível:
													</th>
													<td>
														<div class="col-md-3"><label class="checkbox_v2"><input type="checkbox" value="">TODOS</label></div>
														<div class="col-md-3"><label class="checkbox_v2"><input type="checkbox" value="">Iniciante </label></div>
														<div class="col-md-3"><label class="checkbox_v2"><input type="checkbox" value="">Intermediário </label></div>
														<div class="col-md-3"><label class="checkbox_v2"><input type="checkbox" value="">Avançado </label></div>
													</td>
												</tr>
												
												
												
												<tr>
													<th width="150">
														
													</th>
													<td>
														<button class="btn btn-primary pull-right" style="margin-top:10px">
														<i class="fa fa-check"></i>
														Salvar
														</button>
													</td>
												</tr>
												
											</table>
											{{-- fim meu subnivel --}}
										</td>
									</tr>
									
									{{--  incicio la linha 2  --}}
								</tbody>
							</table>
							
							{{-- fim ibox-content --}}
						</div>
					</div>
					{{-- fim do modelo 1 --}}
					
					
				</div>
			</div>
		</div>
		<script>
		$(document).ready(function() {
			$('.footable').footable();
		});
		</script>
		<style>
			.footable-row-detail-row {
				line-height: 3em;
			}
			.personal-toggle .clicavel {
				cursor:pointer
			}
			.personal-toggle .oculta {
				display: none;
			}
			.checkbox_v2 {
			position: relative;
			display: block;
			margin-top: 10px;
			margin-bottom: 10px;
				font-weight: normal;
			}
			.checkbox_v2 label {
			min-height: 20px;
			padding-left: 20px;
			margin-bottom: 0;
			font-weight: normal;
			cursor: pointer;
			}
			.checkbox_v2 input[type=checkbox] {
				margin-right: 4px;
			}
		</style>
@endsection
@push('scripts')
		{!! Html::script("js/segmentacaoPublicoController.js") !!}
		<!-- FooTable -->
		{!! Html::script("tema_assets/js/plugins/footable/footable.all.min.js") !!}
@endpush