@extends('layouts.app')

@section('content')
{!! Html::script("js/alunoController.js") !!}
 <div class="row" style="margin-top: 20px" ng-controller="alunoEditarController">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5> Editando aluno: {{$aluno->nmaluno}} </h5>
               
            </div>
            <div class="ibox-content">
                	{{-- @include('errors._check') --}}

					 @include('errors._check')
					
					{!!  Form::model($aluno, array('route' => array('admin.clientes.alunos.update', $aluno->id))) !!}
				   	
				    @include('admin.alunos._form')

				   	<div class="form-group">   		
				   		<button class="btn btn-primary " type="submit"><i class="fa fa-check"></i>&nbsp;Salvar</button> 
				   	</div>
					
					{!! Form::close() !!}
            </div>
        </div>
    </div>

@endsection

