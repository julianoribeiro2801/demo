@extends('layouts.app')

@section('content')

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <h2>
                            {{ $result->count() }} resultados encontrados para: <span class="text-navy">“{{ $query }}”</span>
                        </h2>
                        <div class="search-form">
                            <form action="{{ route('admin.clientes.clientes.search') }}" method="get">
                                <div class="input-group">
                                    <input type="text" placeholder="Buscar por usuario" name="search" class="form-control input-lg">
                                    <div class="input-group-btn">
                                        <button class="btn btn-lg btn-primary" type="submit">
                                            Buscar
                                        </button>
                                    </div>
                                </div>

                            </form>
                        </div>

                        <div class="hr-line-dashed"></div>
                        @forelse($result as $item)
                            <div class="search-result">
                                <div class="search-result-avatar"><img src="{{ $item->avatar }}" alt=""></div>
                                <b>{{ $item->role }}</b>
                                <h3><a href="{{ route('admin.clientes.clientes.index', ['user' => $item->id]) }}">{{ $item->name }}</a></h3>
                                <a href="{{ route('admin.clientes.clientes.index', ['user' => $item->id]) }}" class="search-link">{{ $item->email }}</a>

                                <p>
                                </p>
                                <p>Unidade: {{ $item->unidade->fantasia }}</p>
                                @if($item->dados)
                                    <p>Origem:  {{ $item->dados->origem }}</p>
                                    <p>Genero:  {{ $item->dados->genre() }}</p>
                                @endif
                            </div>
                           @unless($loop->last)
                                <div class="hr-line-dashed"></div>
                            @endunless
                        @empty
                        @endforelse
                        <div class="text-center">
                            {{ $result->appends(['search' => \Request::get('search')])->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


