<div aria-hidden="true" class="modal inmodal fade" id="contato_modal" role="dialog" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button class="close" data-dismiss="modal" type="button">
                <span aria-hidden="true">
                    ×
                </span>
                    <span class="sr-only">
                    Fechar
                </span>
                </button>
                {{-- inicio contuedo--}}
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h4>
                            Contato
                        </h4>
                    </div>
                    <div class="ibox-content">
                        <div class="row">

                            <p>Nome: <b> @{{ client.name }}</b><br>
                                E-mail: <b> @{{ client.email }} </b> |
                                Telefone: <b> @{{ client.phone }} </b></p>

                            <form role="form" style="margin-top:20px" ng-submit="addContact()" method="POST">
                                <div class="form-group">
                                    <label>Tipo de Contato</label>
                                    <div class="input-group" style="width:100%">
                                        <select ng-model="contact.crmacao" data-placeholder="Selecione o tipo..."
                                                class="form-control">
                                            <option ng-value="">Selecione o tipo</option>
                                            <option ng-value="acao.id" ng-repeat="acao in coreData.crm_acoes"> @{{ acao.titulo }}</option>
                                        </select>
                                    </div>
                                </div>


                                <div class="form-group"><label>Assunto</label>
                                    <input type="text" placeholder="" class="form-control" ng-model="contact.assunto">
                                </div>
                                <div class="form-group">
                                    <label>Efetuado / Frustrado</label>
                                    <div class="input-group" style="width:100%">
                                        <select ng-model="contact.efetuado" ng-change="abreCalendario()" data-placeholder="Selecione o tipo..."
                                                class="form-control">
                                            <option ng-value="">Selecione</option>
                                            <option ng-value="true"> Efetuado</option>
                                            <option ng-value="false"> Frustado</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group" id="dt_nascimento" ng-if="contact.efetuado == false">
                                    {!! Form::label('Reagendar', 'Reagendar (se frustado)') !!}
                                    <div class="form-group" id="data_4" style="margin-bottom:0;">
                                        <div class="input-group date">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input type="text" class="form-control" ng-model="contact.agendamento">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>O Contato esta quente ou frio?</label>

                                    <input type="checkbox" class="js-switch" ng-model="contact.hot"
                                           ng-checked="contact.hot"/>
                                </div>
                                <div class="form-group">
                                    <label>Descrição</label>
                                    <textarea ng-model="contact.observacao" class="form-control"
                                              placeholder="Observações" rows="3"> </textarea>
                                </div>
                                <div>
                                    <button class="btn btn-sm btn-primary pull-right m-t-n-xs" type="submit"><strong>Cadastarar</strong>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- footer --}}
    </div>

</div>



