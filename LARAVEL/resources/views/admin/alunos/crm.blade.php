@extends('layouts.app')

@section('content')


    {{-- INICIO TABALE. --}}
    <div class="wrapper wrapper-content  animated fadeInRight" ng-controller="alunoController">
     

         {{-- INICIO COLUNA 2 
       <div class="col-sm-4">

       	 <div class="row">

            <div class="col-sm-12 col-md-12" ng-cloak>
                <div class="ibox sk-loading">

                    <div class="ibox-content">

                    	<div class="row">
                		 <div class="col-sm-12 col-md-12">
                		 	  <h4 style="float:left; margin-right:10px">Segmentação: </h4>  
                		 	   <select name="lista" id="input" class="form-control" required="required" style=" width: 200px; float:left">
                            	<option value=""> Sem login no App </option>
                            	<option value=""> 15 dias sem vir </option>
                            	<option value=""> Aniversariantes </option>
                            </select>
                		 </div>
                		 
                	</div>

               
                        
                    </div>
                    <div class="ibox-content">
                    	Listagem de cliente com filtro <br>

                        <a href="/admin/clientes/crmsegmenta" class="btn btn-block btn-info">
                        Ver Tela de Segmentação de Público</a>
                    </div>
                  </div>
              </div> 
             
          </div> 
       </div> --}}


       <div class="col-xs-12 col-sm-8 ">
       	 <div class="row">

            <div class="col-sm-12 col-md-12" ng-cloak>
                <div class="ibox sk-loading">

                    <div class="ibox-content">

                    	<div class="row">
                		 <div class="col-sm-12 col-md-12">
                		 	  <h4 style="float:left; margin-right:10px">CRM (Customer Relationship Management) </h4>  
                		 	  
                		 </div>
                		 
                	</div>

               
                        
                    </div>
                    <div class="ibox-content">
                    	{{-- incicio Chat e componetes do crm individual --}}
                        <div id="tab-6" class="tab-pane">

    <div class="">
        <div class="client-detail" style=" margin-top:20px;">
            <strong>CRM </strong>
      
            <div style="clear:both"></div>
            <div style="margin-top:20px"></div>
                                  

                                        <!-- inicio form -->

 <div class="row" style="padding: 0 20px;">


     <div class="col-xs-12  col-md-5">


        {{-- formulario_crm --}}
<div class="formulario_crm">
    
            <input type="hidden" ng-model="contact.sender_id" ng-init="contact.sender_id = {{ Auth::id() }}">

        <p>E-mail: <b> @{{ client.email}} </b> 
            <br> Celular: <b> (@{{ client.dados.celular.substring(0,2) }}) 
            @{{ client.dados.celular.substring(2,7) }}-@{{ client.dados.celular.substring(7,15) }}  </b>
          <span ng-if="client.dados.telefone">  | Tel. Fixo: <b>
(@{{ client.dados.telefone.substring(0,2) }}) @{{ client.dados.telefone.substring(2,7) }}-@{{ client.dados.telefone.substring(7,15) }}  
         </b> </span>     

        </p>

        <form role="form" style="margin-top:20px" ng-submit="addContact()" method="POST">

        {{--     <li ng-repeat="acao in coreData.crm_acoes"> 
                @{{ acao.titulo}} - @{{ acao.id }}
            </li> --}}

            <div class="form-group">
                <label>Forma de Contato</label>
                <div class="input-group" style="width:100%">
                    <select ng-model="contact.crmacao" data-placeholder="Selecione o tipo..."
                            class="form-control">
                        <option ng-value="acao.id" ng-repeat="acao in coreData.crm_acoes"> @{{ acao.titulo}}</option>
                    </select>
                </div>
            </div>


            <div class="form-group"><label>Assunto</label>
                <input type="text" placeholder="" class="form-control" ng-model="contact.assunto">
            </div>
            <div class="form-group">
                <label>Efetuado / Frustrado</label>
                <div class="input-group" style="width:100%">
                    <select ng-model="contact.efetuado" ng-change="abreCalendario()" data-placeholder="Selecione o tipo..."
                            class="form-control">
                        <option ng-value="true"> Efetuado</option>
                        <option ng-value="false"> Frustado</option>
                    </select>
                </div>
            </div>

            <div class="form-group" id="dt_nascimento" ng-if="contact.efetuado == false">
                {!! Form::label('Reagendar', 'Reagendar (se frustado)') !!}
                <div class="form-group" id="data_4" style="margin-bottom:0;">
                    <div class="input-group date">
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        <input type="text" class="form-control" ng-model="contact.agendamento">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label>O Contato esta quente ou frio?</label>

                <input type="checkbox" class="js-switch" ng-model="contact.hot"
                       ng-checked="contact.hot"/>
            </div>
            <div class="form-group">
                <label>Mensagem / Descrição</label>
                <textarea ng-model="contact.observacao" class="form-control"
                          placeholder="Digite uma mensagem" rows="3"> </textarea>
            </div>
            <div>
                

                <button class="btn btn-sm btn-primary pull-right m-t-n-xs" type="submit">
                    Salvar / Enviar
                </button>

            </div>
        </form>
        <!-- fim formulario -->
        </div>
    </div>

<div class="col-xs-12  col-md-7">


<!-- inicio CHAT -->
<div class="chat-discussion" >

    <div ng-if="messages.length == 0">  Nenhuma mensagem. </div> 

<div ng-class="{'chat-message left': message.sender_id == logued ,'chat-message right': message.sender_id == client.id  }" 
                ng-repeat="message in messages">
              
                <div ng-class="{'message-eu': message.sender_id != client.id  ,'message_received': message.sender_id == client.id  }" >

                <div class="message_wrapper">
                  
                    <a href="#" class="message-author">
                        <span class="message-date"> @{{message.sender_name}}: </span>
                    </a>

                    <span class="message-content">
                        {{-- @{{ client.id }} ----- @{{ message.receiver_id }} ---- @{{ message.sender_id }} --}}
                       <div ng-if="message.acao"> @{{ message.acao }} </div> 
                       <div ng-if="message.title"> @{{ message.title }} </div> 

                        @{{ message.message }}
                    </span>

                    <div class="visualized">
                        <div style="  color:#898989;  float: left; font-size: 10px;">
                            @{{message.created_at}}
                        </div>

                        <i ng-show="message.opened==true" class="fa fa-check" aria-hidden="true"></i>
                    </div>
                    <!---->
                </div>

            </div>
        </div>


    </div>  
<!-- fim chat  -->


    </div>

                                           



                                        </div>

                                        <!-- fim formulario -->



                                    </div>
                                    <div style="clear:both"></div>
                                </div>
                            </div>
                      {{-- fim do chat crm --}}
                    </div>
                  </div>
              </div> 
              {{-- fim col --}}
          </div> {{-- fim row --}}
       	
       </div>
          




    </div>

@endsection
@push('scripts')
    {!! Html::script("js/alunoController.js") !!}
@endpush