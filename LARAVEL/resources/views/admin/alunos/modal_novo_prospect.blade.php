<div aria-hidden="true" class="modal inmodal fade" id="prospect_modal" role="dialog" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body ">
                <button class="close" data-dismiss="modal" type="button">
                    <span aria-hidden="true">
                        ×
                    </span>
                    <span class="sr-only">
                        Fechar
                    </span>
                </button>
                {{-- inicio contuedo --}}
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h4>
                            <i class="fa fa-filter">
                            </i>
                            Oportunidade / Cliente
                        </h4>
                        <div class="ibox-content">
                            <form id="form-prospect" ng-submit="register()" role="form">
                                <div class="row">
                                    <div class="col-xs-12 col-lg-12" style="padding:0 ">
                                        <div class="form-group">
                                            <label>
                                                Nome
                                            </label>
                                            <input class="form-control" ng-class="{ 'error' : errors.name }" ng-model="prospect.name" placeholder="" type="text">
                                                <span class="help-block error has-error" id="name" ng-if="errors.name">
                                                    @{{ errors.name[0] }}
                                                </span>
                                            </input>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>
                                            Email
                                        </label>
                                        <input class="form-control" ng-class="{ 'error' : errors.email }" ng-model="prospect.email" placeholder="" type="email">
                                            <span class="help-block error has-error" id="email" ng-if="errors.email">
                                                @{{ errors.email[0] }}
                                            </span>
                                        </input>
                                    </div>
                                    <div class="form-group">
                                        <label>
                                            Celular
                                        </label>
                                        {{--  data-mask="(99) 9 9999-9999"  --}}
                                        <input class="form-control" ng-class="{ 'error' : errors.celular }" ng-model="prospect.celular" placeholder="" type="text" ui-br-phone-number="">
                                            <span class="help-block error has-error" id="telefone" ng-if="errors.celular">
                                                @{{ errors.name[0] }}
                                            </span>
                                        </input>
                                    </div>

                              <div class="col-xs-12 col-lg-6" style="padding:0 ">
                                <div class="form-group" ng-class="{ 'error' : errors.genero }">
                                    {!! Form::label('Sexo', 'Sexo:') !!}
                                    <select class="form-control" data-placeholder="Selecione" id="genero1" name="genero" ng-model="prospect.genero">
                                       <option selected="" value="">
                                                Selecione
                                            </option>
                                        <option value="M" selected>
                                            M
                                        </option>
                                        <option value="F">
                                            F
                                        </option>
                                    </select>
                                    <span class="help-block error has-error" id="telefone" ng-if="errors.genero">
                                        @{{ errors.genero[0] }}
                                    </span>
                                </div>
                            </div>
                            {{-- fim col --}}
                            <div class="col-xs-12 col-lg-6" style="padding-right:0">
                                <div class="form-group">
                                    <label>
                                        Origem do cliente
                                    </label>
                                    <div class="input-group" style="width:100%">
                                        <select class="form-control" ng-class="{ 'error' : errors.origem }" ng-model="prospect.origem">
                                            <option selected="" value="">
                                                Selecione
                                            </option>
                                            <option value="Google">
                                                Google
                                            </option>
                                            <option value="Facebook">
                                                Facebook
                                            </option>
                                            <option value="Indicação de Amigo">
                                                Indicação de Amigo
                                            </option>
                                            <option value="Outros">
                                                Outros
                                            </option>
                                        </select>
                                        <span class="help-block error has-error" id="telefone" ng-if="errors.origem">
                                            @{{ errors.origem[0] }}
                                        </span>
                                    </div>
                                </div>
                            </div>
                      {{-- fim col --}}
                                  
                                   
                                    <div>
                                        <button class="btn btn-sm btn-primary pull-right m-t-n-xs" type="submit">
                                            <strong>
                                                Cadastrar
                                            </strong>
                                        </button>
                                    </div>
                                    <!-- fim row -->
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                {{-- footer --}}
            </div>
        </div>
    </div>
</div>