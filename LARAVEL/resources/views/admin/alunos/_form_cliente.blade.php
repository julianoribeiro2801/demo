<div class="modal inmodal fade" id="editar_cliente" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Fechar</span>
                </button>
                <h4 class="modal-title">Editar cliente</h4>
            </div>

            <form name="PostFormClienteUp" id="PostFormFuncionario" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="tabs-container">
                        <div class="row m-b-lg">
                            <div class="col-lg-3 text-center">
                                <h2 class="textoMaiusculo">@{{ client.name}}</h2>
                                <div class="m-b-sm">
                                    <div class="profile-avatar" ngf-select="updateAvatar($files, $file)" ng-model="client.avatar" ngf-pattern="'image/*'" ngf-accept="'image/*'"
                                         ngf-max-size="2MB">
                                        <img ngf-size="{width: 80, height: 80, quality: 1}"
                                             src="{{ url('/')}}/uploads/avatars/@{{ client.avatar || '/tema_assets/img/a4.jpg' }}"
                                             alt="image" class="img-circle" style="width: 80px; height: 80px">
                                        <br>
                                        <i class="fa fa-camera" aria-hidden="true"></i>
                                        <br>

                                        <a  ng-show="client.dados.situacaomatricula !== null">
                                           Matrícula:  @{{client.dados.situacaomatricula}}
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-8">
                                <div style="margin-top:20px"></div>
                                <p>
                                    Perfil: 
                                    <b>@{{client.dados.type ? client.dados.type.description : ''}}</b>
                                    <br> Busca:
                                    <b>@{{client.dados.type ? client.dados.type.objective : '' }}</b>
                                    <br> Motivado por:
                                    <b>@{{client.dados.type ? client.dados.type.motivation : ''}}</b>    
                                
                                 <br> <table>
                                    <tr ng-show="client.role=='cliente'">
                                        <td>Premium:</td>
                                        <td>

                                            {{-- inicio --}}
                                            <!--<div class="switch" style="margin-left: 10px">
                                                <div class="onoffswitch">
                                                    <input type="checkbox" checked class="onoffswitch-checkbox" id="example1">
                                                    <label class="onoffswitch-label" for="example1">
                                                        <span class="onoffswitch-inner"></span>
                                                        <span class="onoffswitch-switch"></span>
                                                    </label>
                                                </div>
                                            </div>-->
                                            <div class="switch" style="margin-left: 10px">
                                                <div class="onoffswitch">
                                                    <input type="checkbox" ng-checked="client.idcategoria=='2'" checked class="onoffswitch-checkbox" id="@{{ client.id }}" ng-click="tipoCliente(client)">
                                                    <label class="onoffswitch-label" for="@{{ client.id }}">
                                                        <span class="onoffswitch-inner" ></span>
                                                        <span class="onoffswitch-switch"></span>
                                                    </label>
                                                </div>
                                            </div>                                            
                                            {{-- fim switch --}}
                                        </td>
                                    </tr>
                                </table>
                                     

   

                                </p>
                                <a ng-if="client.role == 'prospect'" ng-click="convert(client)" id="bt_treino"  class="btn btn-warning btn-sm btn-block">
                                    <i class="fa fa-filter" style="margin-right: 5px"></i>
                                    Conveter
                                </a>
                                <div class="row"  ng-if="client.role == 'cliente'">
                                    <div class="col-xs-12 col-sm-4 "  style="padding-right: 0;">
                                         <div class="visible-xs" style="margin-top:5px"></div>
                                         <a href="/admin/prescricao/nova/@{{client.id}}" id="bt_treino" class="btn btn-primary btn-sm btn-block">
                                            <i class="fa fa-edit" style="margin-right:5px;"></i> Treino
                                        </a>
                                    
                                    </div>
                                     <div class="col-xs-12 col-sm-4 " style="padding-right: 0;">
                                         <div class="visible-xs" style="margin-top:5px"></div>
                                         <a href="/admin/clientes/chat" id="bt_treino" class="btn btn-primary btn-sm btn-block">
                                            <i class="fa fa-comment" style="margin-right:5px;"></i> Chat
                                        </a>
                                    
                                    </div>
                                     <div class="col-xs-12 col-sm-4 " style="padding-right: 0;">
                                        <div class="visible-xs" style="margin-top:5px"></div>
                                         <a href="/admin/agendamento" id="bt_treino" class="btn btn-primary btn-sm btn-block">
                                            <i class="fa fa-calendar" style="margin-right:5px;"></i> Avaliação
                                        </a>
                                    
                                    </div>
                                    
                                </div>
                                
                               


                            </div>
                        </div>


                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a data-toggle="tab" href="#tab-5">Dados Cadastrais</a>
                            </li>
                            <li class="">
                                <a data-toggle="tab" href="#tab-6">CRM</a>
                            </li>
                           {{--  <li class="">
                                <a data-toggle="tab" href="#tab-7">Pagamentos</a>
                            </li> --}}
                        </ul>



                        <div class="tab-content">
                            <div id="tab-7" class="tab-pane">
                                <div class="wrapper wrapper-content animated fadeInRight">

                                        
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="payment-card ">
                                                <i class="fa fa-credit-card-alt payment-icon-big " style="color: #8a8e90" ></i>
                                                <br>

                                                <span style="cursor: pointer;">
                                                    <small onclick="$('#met_pagamento').toggle(120)" style="color: red">
                                                        Alterar Cartão</small>

                                                </span>

                                                <h2 class="text-right">
                                                    **** **** **** 1060
                                                </h2>
                                                <div class="row">
                                                    <div class="col-sm-12 text-right">
                                                        <small>
                                                            <strong>Data expiração:</strong> **/*9
                                                        </small>
                                                    </div>

                                                </div>


                                            </div>
                                        </div>
                                        <div class="col-md-7">

                                            <div class="panel-group payments-method"   style="display: none;" id="met_pagamento">

                                                <div class="panel panel-default">
                                                    <div class="panel-heading">

                                                        <h5 class="panel-title">
                                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" class="">Cartão de Credito</a>
                                                        </h5>
                                                    </div>
                                                    <div id="collapseTwo" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                        <div class="panel-body">

                                                            <div class="row">

                                                                <div class="col-md-12">

                                                                    <form role="form" id="payment-form">
                                                                        <div class="row">
                                                                            <div class="col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label>NÚMERO DO CARTÃO</label>
                                                                                    <div class="input-group">
                                                                                        <input type="text" class="form-control" name="Number" placeholder="Número do cartão" required="">
                                                                                        <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-xs-7 col-md-7">
                                                                                <div class="form-group">
                                                                                    <label>DATA DE VALIDADE</label>
                                                                                    <input type="text" class="form-control" name="Expiry" placeholder="mês / ano" required="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xs-5 col-md-5 pull-right">
                                                                                <div class="form-group">
                                                                                    <label>CÓDIGO CV </label>
                                                                                    <input type="text" class="form-control" name="CVC" placeholder="CVC" required="">
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="row">
                                                                            <div class="col-xs-12">
                                                                                <button class="btn btn-primary pull-right" type="submit">Confirmar</button>
                                                                            </div>
                                                                        </div>
                                                                    </form>

                                                                </div>

                                                            </div>






                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>


                                    </div>




                                </div>
                            </div>
                            <!-- fim pagamentos -->
                            <div id="tab-5" class="tab-pane active">
                                <br>

                                <!-- 
                                 <div class="pull-right">
                                  <button ng-if="!editing" ng-click="openForm()" class="btn btn-primary btn-sm pull-right " id="editar_aluno"  type="button">
                                          <i class="fa fa-edit"></i> Editar
                                      </button>
                                 </div> -->



                                <div style="clear: both;"></div>

                                {{--  --}}
{{--     <button ng-click="validarDependente('adrianoruiz@gmail.com')" class="btn btn-primary btn-sm btn-block">
        <i class="fa fa-users" style="margin-right:5px;"></i> Validar Dependente
    </button> --}}
{{--  --}}


                                <div class="row" style="margin-top: 20px">
                                    <div class="col-xs-12 col-lg-2">
                                        <div class="form-group">
                                            <label for="email">Identificador:</label>
                                            <input type="controle" ng-disabled="true" name="controle" id="controle" ng-model="client.dados.controle_id"  class="form-control" >
                                        </div>
                                    </div>


                                    <div class="col-xs-12 col-lg-5">
                                        <div class="form-group">
                                            <label>Nome:</label>
                                            <input type="text" name="name" id="nome" ng-model="client.name" class="form-control" >
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-lg-2">
                                        <div class="form-group" id="dt_nascimento">
                                            {!! Form::label('Data Nasc.', 'Data Nasc.:') !!}
                                            <input type="text" data-mask="99/99/9999" class="form-control" ng-model="client.dados.dt_nascimento"  placeholder="Data Nasc.">
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-lg-1" style="padding:0 ">

                                        <div class="form-group" ng-class="{ 'has-error':errors['genero'] }">
                                            {!! Form::label('Sexo', 'Sexo:') !!}
                                            <select data-placeholder="Selecione o Genero..." class="form-control"  id="genero" name="genero" ng-model="client.dados.genero" >
                                                <option value="null">Sexo</option>
                                                <option value="M"> M</option>
                                                <option value="F"> F</option>
                                            </select>
                                            <span ng-if="errors['perfil']" class="help-block">
                                                @{{ errors['perfil'][0]}}
                                            </span>
                                        </div>
                                    </div>


                                    <div class="col-xs-12 col-lg-2" >
                                        <div class="form-group" ng-class="{ 'has-error':errors['perfil'] }">
                                            {!! Form::label('Perfil', 'Perfil:') !!}
                                            <select data-placeholder="Selecione o Perfil..." class="form-control"  id="profile_type" name="profile_type" ng-model="client.dados.profile_type" >
                                                <option value="null">Selecione um perfill</option>
                                                <option ng-selected="'@{{profile.id == client.dados.profile_type}}'" ng-value="@{{ profile.id}}"  ng-repeat="profile in client.profiles">
                                                    @{{ profile.description}}</option>
                                            </select>
                                            <span ng-if="errors['perfil']" class="help-block">
                                                @{{ errors['perfil'][0]}}
                                            </span>
                                        </div>
                                    </div>



                                </div>



                                <div class="row">

                                    <div class="col-xs-12 col-lg-4">
                                        <div class="form-group">
                                            <label for="email">Email:</label>
                                            <input type="email" name="email" id="email" ng-model="client.email"  class="form-control" >
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-lg-3">
                                        <div class="form-group">
                                            <label for="telefone">Celular:</label>
                                            <input type="text" class="form-control" ng-model="client.dados.celular"  ui-br-phone-number>
                                        </div>                                        
                                    </div>

                                    <div class="col-xs-12 col-lg-3">
                                        <div class="form-group">
                                            <label for="telefone">Tel. Fixo:</label>
                                            <input type="text" class="form-control" ng-model="client.dados.telefone"  ui-br-phone-number>
                                        </div>
                                    </div>




                                    <div class="col-xs-12 col-lg-2" style="padding-left:0">
                                        <div class="form-group">                                           
                                            <label for="cpf">CPF:</label>
                                            <input type="text" class="form-control" ui-br-cpf-mask name="cpf"  ng-model="client.dados.cpf" >
                                        </div>
                                    </div>




                                </div>

                                <hr>
                                <div class="row">

                                    <div class="col-xs-12 col-lg-3">
                                        <div class="form-group">
                                            {!! Form::label('CEP', 'CEP:') !!}
<input type="text"   ng-model="client.dados.cep"  ng-blur="getCep(client.dados.cep)"
class="input form-control"  ui-br-cep-mask  />   
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-lg-6">
                                        <div class="form-group">
                                            {!! Form::label('Endereco', 'Endereço:') !!}
                                            <input type="text" class="form-control" ng-model="client.dados.endereco"  >
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-lg-3">
                                        <div class="form-group">
                                            {!! Form::label('Numero', 'Número:') !!}
                                            <input type="text" class="form-control" ng-model="client.dados.numero"  >
                                        </div>
                                    </div>


                                </div>

                                <div class="row">

                                    <div class="col-xs-12 col-sm-3">
                                        <div class="form-group">
                                            {!! Form::label('Bairro', 'Bairro:') !!}
                                            <input type="text" class="form-control" ng-model="client.dados.bairro"  >
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-3">
                                        <div class="form-group">
                                            {!! Form::label('Complemento', 'Complemento:') !!}
                                            <input type="text" class="form-control" ng-model="client.dados.complemento"  >
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-3">
                                        <div class="form-group">
                                            {!! Form::label('Cidade', 'Cidade:') !!}
                                            <input type="text" class="form-control" ng-model="client.dados.cidade"  >
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-1" style="padding:0 ">
                                        <div class="form-group">
                                            {!! Form::label('Estado', 'Estado:') !!}
                                            <input type="text" class="form-control" ng-model="client.dados.estado"  >
                                        </div>
                                    </div>

                                    <div class="col-xs-6 col-sm-2" >
                                        <div class="form-group">
                                            <label for="senha">
                                                Senha:
                                            </label>
                                            <div class="input-group">
                                                <input class="form-control" id="senha" name="senha" ng-model="client.password"  placeholder="*****" type="password" readonly>
                                                <span class="input-group-addon btn btn-primary bg-primary" ng-click="refreshClientPassword(client.id)" >
                                                    <i aria-hidden="true" class="fa fa-refresh">
                                                    </i>
                                                </span>
                                                </input>
                                            </div>
                                        </div>
                                    </div>

                                </div>




                                <li class="list-group-item fist-item" style="overflow: hidden; border: 0; padding:0; margin-top:15px" >

                                    <div class="pull-left">
                                        <button  class="btn btn-danger btn-sm" ng-click="excluirCliente(client)">
                                            <i class="fa fa-trash"></i> Excluir
                                        </button>
                                    </div>

                                    <span class="pull-right">
                                        <button ng-if="client.name" id="atualizar_aluno" class="btn btn-primary" ng-click="atualizar()" type="button">
                                            <i class="fa fa-repeat"></i>
                                        </button>
                                        <button ng-if="client.name" id="salvar_aluno" class="btn btn-primary" ng-click="update()" type="button">
                                            <i class="fa fa-check"></i>Salvar
                                        </button>
                                    </span>

                                </li>
                            </div>
                            <div id="tab-6" class="tab-pane">

    <div class="">
        <div class="client-detail" style=" margin-top:20px;">
            <strong>CRM </strong>
      
            <div style="clear:both"></div>
            <div style="margin-top:20px"></div>
                                  

                                        <!-- inicio form -->

 <div class="row" style="padding: 0 20px;">


     <div class="col-xs-12  col-md-5">


        {{-- formulario_crm --}}
<div class="formulario_crm">
    
            <input type="hidden" ng-model="contact.sender_id" ng-init="contact.sender_id = {{ Auth::id() }}">

        <p>E-mail: <b> @{{ client.email}} </b> 
            <br> Celular: <b> (@{{ client.dados.celular.substring(0,2) }}) 
            @{{ client.dados.celular.substring(2,7) }}-@{{ client.dados.celular.substring(7,15) }}  </b>
          <span ng-if="client.dados.telefone">  | Tel. Fixo: <b>
(@{{ client.dados.telefone.substring(0,2) }}) @{{ client.dados.telefone.substring(2,7) }}-@{{ client.dados.telefone.substring(7,15) }}  
         </b> </span>     

        </p>

        <form role="form" style="margin-top:20px" ng-submit="addContact()" method="POST">

        {{--     <li ng-repeat="acao in coreData.crm_acoes"> 
                @{{ acao.titulo}} - @{{ acao.id }}
            </li> --}}

            <div class="form-group">
                <label>Forma de Contato</label>
                <div class="input-group" style="width:100%">
                    <select ng-model="contact.crmacao" data-placeholder="Selecione o tipo..."
                            class="form-control">
                        <option ng-value="acao.id" ng-repeat="acao in coreData.crm_acoes"> @{{ acao.titulo}}</option>
                    </select>
                </div>
            </div>


            <div class="form-group"><label>Assunto</label>
                <input type="text" placeholder="" class="form-control" ng-model="contact.assunto">
            </div>
            <div class="form-group">
                <label>Efetuado / Frustrado</label>
                <div class="input-group" style="width:100%">
                    <select ng-model="contact.efetuado" ng-change="abreCalendario()" data-placeholder="Selecione o tipo..."
                            class="form-control">
                        <option ng-value="true"> Efetuado</option>
                        <option ng-value="false"> Frustado</option>
                    </select>
                </div>
            </div>

            <div class="form-group" id="dt_nascimento" ng-if="contact.efetuado == false">
                {!! Form::label('Reagendar', 'Reagendar (se frustado)') !!}
                <div class="form-group" id="data_4" style="margin-bottom:0;">
                    <div class="input-group date">
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        <input type="text" class="form-control" ng-model="contact.agendamento">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label>O Contato esta quente ou frio?</label>

                <input type="checkbox" class="js-switch" ng-model="contact.hot"
                       ng-checked="contact.hot"/>
            </div>
            <div class="form-group">
                <label>Mensagem / Descrição</label>
                <textarea ng-model="contact.observacao" class="form-control"
                          placeholder="Digite uma mensagem" rows="3"> </textarea>
            </div>
            <div>
                

                <button class="btn btn-sm btn-primary pull-right m-t-n-xs" type="submit">
                    Salvar / Enviar
                </button>

            </div>
        </form>
        <!-- fim formulario -->
        </div>
    </div>

<div class="col-xs-12  col-md-7">


<!-- inicio CHAT -->
<div class="chat-discussion" >

    <div ng-if="messages.length == 0">  Nenhuma mensagem. </div> 

<div ng-class="{'chat-message left': message.sender_id == logued ,'chat-message right': message.sender_id == client.id  }" 
                ng-repeat="message in messages">
              
                <div ng-class="{'message-eu': message.sender_id != client.id  ,'message_received': message.sender_id == client.id  }" >

                <div class="message_wrapper">
                  
                    <a href="#" class="message-author">
                        <span class="message-date"> @{{message.sender_name}}: </span>
                    </a>

                    <span class="message-content">
                        {{-- @{{ client.id }} ----- @{{ message.receiver_id }} ---- @{{ message.sender_id }} --}}
                       <div ng-if="message.acao"> @{{ message.acao }} </div> 
                       <div ng-if="message.title"> @{{ message.title }} </div> 

                        @{{ message.message }}
                    </span>

                    <div class="visualized">
                        <div style="  color:#898989;  float: left; font-size: 10px;">
                            @{{message.created_at}}
                        </div>

                        <i ng-show="message.opened==true" class="fa fa-check" aria-hidden="true"></i>
                    </div>
                    <!---->
                </div>

            </div>
        </div>


    </div>  
<!-- fim chat  -->


        <!-- inicio historico -->
       {{--  <div class="crmhistory">
            <div id="vertical-timeline" data-height="50vh" class="vertical-container dark-timeline" style="max-height: 50vh; overflow-y: auto">

                <div class="vertical-timeline-block" ng-repeat="($key, $msg) in crm" 
                ng-click="showHistorico(client.id)">
                    <div class="vertical-timeline-icon gray-bg">
                        @{{ $msg.date}}
                    </div>
                    <div class="vertical-timeline-content">
                        <p>
                            @{{ $msg.text}}
                        </p>
                    </div>
                </div>
            </div>
        </div> --}}
        <!-- fim historico -->
    </div>

                                           



                                        </div>

                                        <!-- fim formulario -->



                                    </div>
                                    <div style="clear:both"></div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="row load" style="display:none;">
                        <div class="col-sm-12">
                            <div class="progress progress-striped light active">
                                <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="" aria-valuemin="0" aria-valuemax="100">
                                    <span class="sr-only"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>





<style >

    .chat-user {
        cursor: pointer;
        transition: 350ms;
        position: relative;
    }
    .chat-user:hover {
        background: #f3f3f4;
    }
    .chat-user.active {
        background: #19aa8c !important;
        color: #fff;

    }
    .chat-user.active span.new_message {
        background: #fff;
        color: #19aa8b;
    }
    .message_received .message_wrapper  {
            background: rgba(89, 180, 158, 0.26);
            width:80%
           
    }
    span.new_message {
        background: #19aa8b;
        color: #fff;
        /* padding: 3px; */
        height: 20px;
        min-width: 20px;
        position: absolute;
        right: 0;
        top: 50%;
        transform: translateY(-50%);
        text-align: center;
        border-radius: 50%;
    }

    .visualized {
        text-align: right;
        font-size: 10px;
        color: #149a7c;
    }

    .message_eu {
        background: transparent;
    }

    .chat-discussion.loading:before {
        content: 'Carregando mensagens...';
        position: absolute;
        left: 50%;
        top: 50%;
        color: #19aa8b;
        transform: translate(-50%, -50%);
        background: #fff;
        padding: 15px;
        box-shadow: 0px 2px 8px -3px #ccc;
    }
    .message_wrapper {
        display: inline-block;
        padding: 10px 15px;
        background: #fff;
        border-radius: 5px;
        margin-bottom: 10px;
    }
    .message_received {
        text-align: right;
    }
    .message_eu .message_wrapper {
        background: #19aa8b14;
    }
    a.message-author {
        margin-right: 8px;
        font-size: 13px;
    }
    .message-content {
        display: block;
        font-size: 14px;
        padding: 3px;
        text-align: left;
    }
    .chat-discussion.loading {
        display: block;
        position: relative;
    }
    .chat-discussion {
        background: transparent;
        height: 422px;
        padding: 0
    }
</style>