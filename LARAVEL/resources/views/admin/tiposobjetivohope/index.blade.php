@extends('layouts.app')

@section('content')

<div class="container">
	<h3>Tipo objetivo</h3>
	<br>
	<a href="{{ route('admin.clientes.tiposobjetivohope.create') }}" class="btn btn-primary"> Novo objetivo</a>

	<br><br>
	<table class="table table-bordered">
		<thead>
			<tr>
				<th>Id</th>
				<th>Nome</th>
				<th>Ação</th>
			</tr>
		</thead>
		<tbody>
		@foreach ($tiposobjetivohope as $tipoobjetivohope)
			<tr>
				<td> {{$tipoobjetivohope->id}}</td>
				<td> {{$tipoobjetivohope->dstipoobjetivo}}</td>

				<td> <a href="{{route('admin.clientes.tiposobjetivohope.edit', $tipoobjetivohope->id)}}" class="btn btn-info">Editar</a></td>
				
			</tr>
			@endforeach
		</tbody>
	</table>

	{{-- {{{ $tiposobjetivohope->render()}}} --}}

	
</div>

@endsection

