@extends('layouts.app')

@section('content')

<div class="container">
	<h3> Novo objetivo hope</h3>
	
	{{-- @include('errors._check') --}}

	{!! Form::open(array('route' => 'admin.clientes.tiposobjetivohope.store', 'class'=>'form')) !!}
   
    @include('admin.tiposobjetivohope._form')

   	<div class="form-group">   		
   	{!! Form::submit('Criar ', ['class'=>'btn btn-primary']) !!}
   	</div>
	
	{!! Form::close() !!}

	
	
</div>

@endsection

