@extends('layouts.app')

@section('content')

<div class="container">
	<h3> Editando tipoobjetivohope: {{$tipoobjetivohope->nmgrupo}} </h3>
	
	 @include('errors._check')
	
	{!!  Form::model($tipoobjetivohope, array('route' => array('admin.clientes.tiposobjetivohope.update', $tipoobjetivohope->id))) !!}
   	
    @include('admin.tiposobjetivohope._form')

   	<div class="form-group">   		
   		{!! Form::submit('Salvar', ['class'=>'btn btn-primary']) !!}
   	</div>
	
	{!! Form::close() !!}
	
	
</div>

@endsection

