<div aria-hidden="true" class="modal inmodal fade" id="addClientePrivate" role="dialog" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                <h4 class="modal-title">Adicionar Cliente  </h4>
            </div>

 <h4 style="
    background: #ed5563;
    margin: 0;
    color: #fff;
    padding: 7px;
    text-align: center;
    margin-top: -20px;"> Aula Privada  </h4>

<div class="modal-body">
                <span style="display:none;">
                    <input type="text" ng-model="cliente.spmId"></span>
                <div class="row">
                    <div class="col-xs-6 col-sm-2" style="padding-right:0">
                        <label style="font-size: 14px;">Vagas disp:</label>
                       
                       <input ng-show="privado == 'Sim'" type="text" name="name" ng-disabled="true"  id="inputIDvagas"
                               class="form-control inputGFM" value="2/5" title=""
                               required="required" >

                        {{-- Aqui é seu cod. original é so descomentar q volta a funcionar
                            FAVOR COLCOAR  value="2/5"
                        <input ng-show="privado == 'Sim'" type="text" name="name" ng-disabled="true" ng-model="sobra" id="inputIDvagas"
                               class="form-control inputGFM" value="10" title=""
                               required="required" > --}}


                    </div>
                    <div class="col-xs-6 col-sm-10">
                        {{-- @TODO A DATA TAMBÉM NÃO ESTA PUXANDO VERIFICAR --}}
                           <h3 style="margin-top: 36px; margin-left: 10px">@{{aula}} - @{{horario}} - @{{dt_aula}} | <span style="font-size: 12px;"> @{{professor}} </span></h3>

                        <div class="pull-right" style="margin-top: 22px;">
                            <button type="button" ng-show="sobra > 0" ng-click="addAlunoMatricula()" 
                                class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Adicionar</button>
                            <button ng-disabled="true" ng-show="sobra === 0" type="button"  class="btn btn-danger btn-sm">
                                <i class="fa fa-check"></i>Lotada</button>
                        </div>
                    </div>
                </div>

                <div class="tabs-container" style="overflow-y: scroll; height: 300px">
                    <ul class="nav nav-tabs">
                        <li class="active" ng-show="true">
                            <a data-toggle="tab" href="#tab-matricula">
                                Matriculados
                            </a>
                        </li>
                        <li ng-show="true">
                            <a data-toggle="tab" href="#tab-espera" >
                                Lista de espera
                            </a>
                        </li>

                    </ul>
                    <div class="tab-content">
                        <!-- INICIO DA ABA 1 -->
                        <div class="tab-pane active" id="tab-matricula" ng-show="true" >



                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>Nome</th>
                                            <th></th>
                                            <th></th>
                                            <th>Presença</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="matricula in matriculas">
                                            <td width="50"> <img  src="{{ url('/')}}/uploads/avatars/user-@{{matricula.idaluno}}.jpg" alt="" class="chat-avatar img-circle img-circle" style="width: 25px; height: 25px;"></td>
                                            <!--<td>@{{lista.name}}</td>-->

                                            <td>
                                              <select class="form-control chosen-select" ng-model="matricula.idaluno" data-placeholder="Selecione um cliente..." tabindex="2" >
                                         <option ng-value="@{{matricula.idaluno}}">@{{matricula.name}}</option>
                                                    <option ng-repeat="client in clients" ng-value="@{{client.id}}">@{{client.name}}</option>
                                            </select>

                                               
                                            </td>

                                            <td class="text-center">
                                                <i class="fa fa-save" aria-hidden="true" ng-click="matriculaAdd(aulaSelect, matricula.idaluno)" ng-disabled="saveGfmBtn"></i>
                                            </td>

                                            <td class="text-center">
                                                <i ng-click="matriculaDel(aulaSelect, matricula.idaluno)" class="fa fa-trash" aria-hidden="true" ></i>
                                            </td>

                                            <td ng-click="checa($index, matricula.idlog, matricula.idaula)" class="text-center">
                                                <i ng-show="matricula.presente == 1" class="fa fa-check"  style="color:#1bb394"  aria-hidden="true"></i>
                                                <i ng-show="matricula.presente == 0" class="fa fa-times" style="color:#ed5565" aria-hidden="true"></i>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>

                <!--<button type="button" ng-click="addAlunoMatricula()" class="btn btn-primary"><i class="fa fa-check"></i> Adicionar</button>-->
                <!--                        <button type="button" ng-show="vacancia > 0" ng-click="addAlunoMatricula()" class="btn btn-primary"><i class="fa fa-check"></i> Adicionar</button>-->
                <!--<button ng-disabled="true" ng-show="vacancia === 0" type="button" ng-click="addAlunoMatricula()" class="btn btn-danger"><i class="fa fa-check"></i>Lotada</button>-->

                        </div>
                        <div class="tab-pane" id="tab-espera" ng-show="true" >
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th></th>
                                            <th>Nome</th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <button type="button" ng-show="sobra > 0" ng-click="addAlunoEspera()" class="btn btn-primary"><i class="fa fa-plus"></i> Adicionar</button>
                                        
                                        <tr ng-repeat="listaespera in listaesperas">
                                            <td>@{{$index + 1}}</td>
                                            <td width="50"> <img  src="{{ url('/')}}/uploads/avatars/user-@{{listaespera.idaluno}}.jpg" alt="" class="chat-avatar img-circle img-circle" style="width: 25px; height: 25px;"></td>
                                            <!--<td>@{{lista.name}}</td>-->
                                            <td>
                                                 <select class="form-control chosen-select" ng-model="listaespera.idaluno" data-placeholder="Selecione um cliente..." tabindex="2" >
                                                  <option ng-value="@{{listaespera.idaluno}}">@{{listaespera.name}}</option>
                                                    <option ng-repeat="client in clients" ng-value="@{{client.id}}">@{{client.name}}</option>
                                                </select>

                                               
                                            </td>
                                            <td class="text-center">
                                                <i class="fa fa-save" aria-hidden="true" ng-click="esperaAdd(aulaSelect, listaespera.idaluno)" ng-disabled="saveGfmBtn"></i>
                                            </td>
                                            <td class="text-center">
                                                <i ng-click="esperaDel(aulaSelect, listaespera.idaluno)" class="fa fa-trash" aria-hidden="true" ></i>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>                            
                            
                        </div>
                    </div>
                </div>





                <div class="modal-footer" style="padding-right: 0; padding-left: 0; border: 0;">
                    <div  class="pull-left" ng-show="privado == 'Não'">
                        <button ng-disabled="true" ng-show="qt_vagas == 0" type="button" ng-click="addAlunoAula()" class="btn btn-danger">
                            <i class="fa fa-bell"></i> Lotada</button>
                    </div>

                    <div class="pull-right">
                        <button type="button" ng-show="qt_vagas > 0" ng-click="reservaAddNovo(aulaSelect)" class="btn btn-primary"><i class="fa fa-check"></i> Salvar</button>
                    </div>
                </div>



            </div>

        </div>
    </div>
</div>
<style type="text/css">
    .chosen-container-single .chosen-single span {
    margin-top: 2px;
}
</style>