@extends('layouts.app')

@section('content')

        <div class="wrapper wrapper-content  animated fadeInRight">

<div class="row">
    <div class="col-md-12">
                        <h2>Tipos de oportunidades</h2>
                        <small>Você tem <b>23</b> oportunidades.</small>
<div style="margin-top:20px"></div>

                        <table style="width: 100%">
                            <tr>
                                <td style="margin-right: 15px">
                                <span class="label label-primary">1º</span> Expontaneo (12)</td>
                                 <td style="margin-right: 15px">
                                <span class="label label-primary">2º</span> Renovação (8)</td>
                                 <td style="margin-right: 15px">
                                <span class="label label-primary">3º</span> Rematricula (3)</td>
                                 <td style="margin-right: 15px">
                                <span class="label label-primary">4º</span> Indicação (12)</td>
                            
                             <td style="margin-right: 15px">
                                <span class="label label-primary">5º</span> Pós venda (12)</td>
                                 <td style="margin-right: 15px">
                                <span class="label label-primary">6º</span> Agendamentos (12) </td>
                                <td> 
                                <span class="label label-primary">7º</span> Lista de espera (12)</td>
                               
                            </tr>
                        </table>
                       

                    </div>
</div>

<div style="margin-top:40px"></div>

            <div class="row">



                <div class="col-lg-3">
                    <div class="ibox">
                        <div class="ibox-content">
                            <h3>Lista de Oportunidades</h3>
                            <p class="small"><i class="fa fa-hand-o-up"></i> Arraste o contato entre a lista</p>

                               <button data-toggle="modal" data-target="#prospect_modal" id="btn-novo-prospect" class="btn btn-primary btn-block btn-sm" type="submit">
                                    <i class="fa fa-plus-square"></i>
                                     Nova Oportunidade 
                                </button>

                            <ul class="sortable-list connectList agile-list" id="todo">
                                <li class="" id="task1">
                                    Pedro Teste
                                     <div class="agile-detail">
                                        <a href="#" class="pull-right btn btn-xs neutroCRM" ></a>
                                        <i class="fa fa-clock-o"></i> 12/10/2017 - 10:30h
                                        <a href="#" class="pull-right neutroTexto " >
                                            Renovação</a>
                                    </div>
                                </li>
                                <li class="" id="task2">
                                    Kevin Miguel Gomes
                                     <div class="agile-detail">
                                        <a href="#" class="pull-right btn btn-xs neutroCRM" ></a>
                                        <i class="fa fa-clock-o"></i> 12/10/2017 - 10:30h
                                        <a href="#" class="pull-right neutroTexto " >
                                            Renovação</a>
                                    </div>
                                </li>
                                <li class="" id="task3">
                                    Renan Vitor Joaquim Pinto
                                    <div class="agile-detail">
                                        <a href="#" class="pull-right btn btn-xs neutroCRM" ></a>
                                        <i class="fa fa-clock-o"></i> 12/10/2017 - 10:30h
                                        <a href="#" class="pull-right neutroTexto " >
                                            Renovação</a>
                                    </div>
                                </li>
                                <li class="" id="task4">
                                    Kevin Augusto Tomás Gomes
                                     <div class="agile-detail">
                                        <a href="#" class="pull-right btn btn-xs neutroCRM" ></a>
                                        <i class="fa fa-clock-o"></i> 12/10/2017 - 10:30h
                                        <a href="#" class="pull-right neutroTexto " >
                                            Renovação</a>
                                    </div>
                                </li>
                                <li class="defaut-element" id="task5">
                                    Guilherme Enzo Kevin Monteiro
                                    <div class="agile-detail">
                                        <a href="#" class="pull-right btn btn-xs neutroCRM" ></a>
                                        <i class="fa fa-clock-o"></i> 12/10/2017 - 10:30h
                                        <a href="#" class="pull-right neutroTexto " >
                                            Renovação</a>
                                    </div>
                                </li>
                                <li class="" id="task6">
                                    Diogo Francisco Ribeiro
                                    <div class="agile-detail">
                                        <a href="#" class="pull-right btn btn-xs neutroCRM" ></a>
                                        <i class="fa fa-clock-o"></i> 12/10/2017 - 10:30h
                                        <a href="#" class="pull-right neutroTexto " >
                                            Renovação</a>
                                    </div>
                                </li>
                                <li class="" id="task7">
                                    Fernanda Ribeiro
                                     <div class="agile-detail">
                                        <a href="#" class="pull-right btn btn-xs neutroCRM" ></a>
                                        <i class="fa fa-clock-o"></i> 12/10/2017 - 10:30h
                                        <a href="#" class="pull-right neutroTexto " >
                                            Renovação</a>
                                    </div>
                                </li>
                                <li class="" id="task8">
                                    Benício Erick Bruno da Silva
                                     <div class="agile-detail">
                                        <a href="#" class="pull-right btn btn-xs neutroCRM" ></a>
                                        <i class="fa fa-clock-o"></i> 12/10/2017 - 10:30h
                                        <a href="#" class="pull-right neutroTexto " >
                                            Renovação</a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="ibox">
                        <div class="ibox-content">
                            <h3>Em Negociação</h3>
                            <p class="small"><i class="fa fa-hand-o-up"></i> Arraste o contato entre a lista</p>
                            <ul class="sortable-list connectList agile-list" id="inprogress">
                                <li class="" id="task9">
                                   Camila Yasmin Monteiro
                                    <div class="agile-detail">
                                        <a href="#" class="pull-right btn btn-xs  quenteCRM" ></a>

                                        <i class="fa fa-clock-o"></i> 12/10/2017 - 10:30h
                                        <a href="#" class="pull-right quentTexto " >
                                            Renovação</a>
                                    </div>
                                </li>
                                <li class="" id="task10">
                                    Fernanda Ribeiro
                                    <div class="agile-detail">
                                        <a href="#" class="pull-right btn btn-xs neutroCRM" ></a>
                                        <i class="fa fa-clock-o"></i> 12/10/2017 - 10:30h
                                        <a href="#" class="pull-right neutroTexto " >
                                            Renovação</a>
                                    </div>
                                </li>
                                <li class="" id="task11">
                                    Benício Erick Bruno da Silva
                                    <div class="agile-detail">
                                        <a href="#" class="pull-right btn btn-xs neutroCRM" ></a>
                                        <i class="fa fa-clock-o"></i> 12/10/2017 - 10:30h
                                        <a href="#" class="pull-right neutroTexto " >
                                            Renovação</a>
                                    </div>
                                </li>
                                <li class="" id="task12">
                                    Isadora Clara Heloisa Barros
                                    <div class="agile-detail">
                                        <a href="#" class="pull-right btn btn-xs btn-danger quenteCRM" ></a>
                                        <i class="fa fa-clock-o"></i> 12/10/2017 - 10:30h
                                        <a href="#" class="pull-right quentTexto " >
                                            Renovação</a>
                                    </div>
                                </li>
                                <li class="" id="task13">
                                   Bryan André Monteiro
                                    <div class="agile-detail">
                                        <a href="#" class="pull-right btn btn-xs btn-danger quenteCRM" ></a>
                                        <i class="fa fa-clock-o"></i> 12/10/2017 - 10:30h
                                        <a href="#" class="pull-right quentTexto " >
                                            Renovação</a>
                                    </div>
                                </li>
                                <li class="" id="task14">
                                    Miguel Ricardo Bryan Alves
                                    <div class="agile-detail">
                                         <a href="#" class="pull-right btn btn-xs btn-danger quenteCRM" ></a>
                                        <i class="fa fa-clock-o"></i> 12/10/2017 - 10:30h
                                        <a href="#" class="pull-right quentTexto " >
                                            Renovação</a>
                                    </div>
                                </li>
                                <li class="" id="task15">
                                    Pedro Henrique Souza
                                    <div class="agile-detail">
                                         <a href="#" class="pull-right btn btn-xs btn-danger quenteCRM" ></a>
                                        <i class="fa fa-clock-o"></i> 12/10/2017 - 10:30h
                                        <a href="#" class="pull-right quentTexto " >
                                            Renovação</a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="ibox">
                        <div class="ibox-content">
                            <h3>Visistas Agendadas</h3>
                            <p class="small"><i class="fa fa-hand-o-up"></i> Arraste o contato entre a lista</p>
                            <ul class="sortable-list connectList agile-list" id="completed">
                                <li class="" id="task16">
                                   Isaac Diego Fernandes
                                     <div class="agile-detail">
                                        <a href="#" class="pull-right btn btn-xs neutroCRM" ></a>
                                        <i class="fa fa-clock-o"></i> 12/10/2017 - 10:30h
                                        <a href="#" class="pull-right neutroTexto " >
                                            Renovação</a>
                                    </div>
                                </li>
                                <li class="" id="task17">
                                   Renato Caio Murilo Rocha
                                     <div class="agile-detail">
                                        <a href="#" class="pull-right btn btn-xs neutroCRM" ></a>
                                        <i class="fa fa-clock-o"></i> 12/10/2017 - 10:30h
                                        <a href="#" class="pull-right neutroTexto " >
                                            Indicação</a>
                                    </div>
                                </li>
                              
                                <li class="" id="task18">
                                    Alícia Julia Fernandes
                                    <div class="agile-detail">
                                        <a href="#" class="pull-right btn btn-xs neutroCRM" ></a>
                                        <i class="fa fa-clock-o"></i> 12/10/2017 - 10:30h
                                        <a href="#" class="pull-right neutroTexto " >
                                            Renovação</a>
                                    </div>
                                </li>

                            </ul>
                        </div>
                    </div>


                </div>


                  <div class="col-md-3">

                    
                    <!-- venceu -->
                    <div class="ibox">
                        <div class="ibox-content">
                            <h3 style="color: #27b392"><i class="fa fa-check" style="color: #27b392"></i> Ganhou 54 Clientes este mês  </h3>
                            <!-- <p class="small"><i class="fa fa-check" style="color: #27b392"></i> <b>3</b> clientes conquistados esse mês.</p> -->
                            <ul class="sortable-list connectList agile-list" id="completed">
                                <li class="" id="task16">
                                   Isaac Diego Fernandes
                                    <i class="fa fa-check pull-right" style="color: #27b392"></i>
                                     <div class="agile-detail">                                      
                                        <i class="fa fa-user"></i> Karsten                                       
                                    </div>
                                </li>
                                <li class="" id="task17">
                                   Renato Caio Murilo Rocha
                                     <i class="fa fa-check pull-right" style="color: #27b392"></i>
                                     <div class="agile-detail">                                      
                                        <i class="fa fa-user"></i> Karsten                                       
                                    </div>
                                </li>
                              
                                <li class="" id="task18">
                                    Alícia Julia Fernandes
                                     <i class="fa fa-check pull-right" style="color: #27b392"></i>
                                     <div class="agile-detail">                                      
                                        <i class="fa fa-user"></i> Karsten                                       
                                    </div>
                                </li>

                            </ul>
                        </div>
                    </div>
                    <!-- fim -->
                      

                </div>

            </div>


        </div>

   



<!-- inicio modal -->

<div aria-hidden="true" class="modal inmodal fade" id="prospect_modal" role="dialog" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body " ng-class="{'is-loading' : loading}">
                <div class="loading"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></div>
                <button class="close" data-dismiss="modal" type="button">
                <span aria-hidden="true">
                    ×
                </span>
                    <span class="sr-only">
                    Fechar
                </span>
                </button>
                {{-- inicio contuedo--}}
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h4>
                            <i class="fa fa-filter"></i> Oportunidade / Cliente
                        </h4>
                        <div class="ibox-content">
                            <div class="row">

                                <form id="form-prospect" role="form" ng-submit="register()">
                                    <div class="form-group"><label>Nome</label> <input ng-model="prospect.name" type="text" placeholder=""  class="form-control" ng-class="{ 'error' : errors.name }">
                                        <span ng-if="errors.name" class="help-block error has-error"
                                              id="name">@{{ errors.name[0] }}</span>
                                    </div>
                                    <div class="form-group"><label>Email</label> <input ng-model="prospect.email"
                                                                                        type="email" placeholder=""
                                                                                        class="form-control"
                                                                                        ng-class="{ 'error' : errors.email }">
                                        <span ng-if="errors.email" class="help-block error has-error"
                                              id="email">@{{ errors.email[0] }}</span>
                                    </div>
                                    <div class="form-group"><label>Telefone</label>
                                        {{--  data-mask="(99) 9 9999-9999"  --}}
                                        <input ng-model="prospect.telefone" ui-br-phone-number type="text"
                                               placeholder="" class="form-control"
                                               ng-class="{ 'error' : errors.telefone }">
                                        <span ng-if="errors.telefone" class="help-block error has-error"
                                              id="telefone">@{{ errors.name[0] }}</span>
                                    </div>


                                    <div class="form-group">
                                        <label>Origem do cliente</label>
                                        <div class="input-group" style="width:100%">
                                            <select ng-model="prospect.origem" data-placeholder="Selecione a origem..."
                                                    class="form-control" ng-class="{ 'error' : errors.origem }">
                                                <option value="null" ng-selected="true">Selecione a origem</option>
                                                <option value="Google"> Google</option>
                                                <option value="Facebook"> Facebook</option>
                                                <option value="Indicação de Amigo"> Indicação de Amigo</option>
                                                <option value="Outros"> Outros</option>
                                            </select>
                                            <span ng-if="errors.origem" class="help-block error has-error"
                                                  id="telefone">@{{ errors.origem[0] }}</span>
                                        </div>
                                    </div>

                                    <div>
                                        <button class="btn btn-sm btn-primary pull-right m-t-n-xs" type="submit">
                                            <strong>Cadastrar</strong></button>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
                {{-- footer --}}
            </div>

        </div>
    </div>
</div>

<!-- fim modal -->

@endsection






