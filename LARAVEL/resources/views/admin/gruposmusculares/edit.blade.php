@extends('layouts.app')

@section('content')

<div class="container">
	<h3> Editando grupomuscular: {{$grupomuscular->nmgrupo}} </h3>
	
	 @include('errors._check')
	
	{!!  Form::model($grupomuscular, array('route' => array('admin.clientes.gruposmusculares.update', $grupomuscular->id))) !!}
   	
    @include('admin.gruposmusculares._form')

   	<div class="form-group">   		
   		<button class="btn btn-primary " type="submit"><i class="fa fa-check"></i>&nbsp;Salvar</button> 
   	</div>
	
	{!! Form::close() !!}
	
	
</div>

@endsection

