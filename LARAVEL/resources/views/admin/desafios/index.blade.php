@extends('layouts.app')
@section('content')


@push('scripts')
{!! Html::script("js/rankingController.js") !!}
@endpush

<div ng-controller="rankingController">



    <div class="row" style="margin-top: 20px">
        <div class="col-xs-12 col-md-12">
            @include('admin.desafios.desafios-musculacao')
        </div>
        <div class="col-xs-12 col-md-12">
            @include('admin.desafios.desafios-ginastica')
        </div>
        <div class="col-xs-12 col-md-12">
            @include('admin.desafios.desafios-cross-training')
        </div>
        <div class="col-xs-12 col-md-12">
            @include('admin.desafios.desafios-corrida')
        </div>
        <div class="col-xs-12 col-md-12">
            @include('admin.desafios.desafios-ciclimos')
        </div>
        <div class="col-xs-12 col-md-12">
            @include('admin.desafios.desafios-natacao')
        </div>
        <div class="col-xs-12 col-md-12">
            @include('admin.desafios.modal')
        </div>
        <div class="col-xs-12 col-md-12">
            @include('admin.desafios.notificacao')
        </div>

    </div>




    <div aria-hidden="true" class="modal inmodal fade" id="modal_denuncia" role="dialog" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body " ng-class="{'is-loading' : loading}">
                    <div class="loading"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></div>
                    <button class="close" data-dismiss="modal" type="button">
                <span aria-hidden="true">
                    ×
                </span>
                        <span class="sr-only">
                    Fechar
                </span>
                    </button>
                    {{-- inicio contuedo--}}
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h4>
                                Denuncia
                            </h4>
                            <div class="ibox-content">
                                <div class="row">

                                    {{--feed--}}

                                    <div class="social-feed-box">

                                        <div class="pull-right social-action dropdown">
                                            <button data-toggle="dropdown" class="dropdown-toggle btn-white">
                                                <i class="fa fa-angle-down"></i>
                                            </button>
                                            <ul class="dropdown-menu m-t-xs">
                                                <li><a href="#">Config</a></li>
                                            </ul>
                                        </div>
                                        <div class="social-avatar">
                                            <a href="" class="pull-left">
                                                <img alt="image" src="/tema_assets/img/a1.jpg">
                                            </a>
                                            <div class="media-body">
                                                <a href="#">
                                                    Jeandro Couto
                                                </a>
                                                <small class="text-muted">4:21 pm - 12.06.2017</small>
                                            </div>
                                        </div>
                                        <div class="social-body">
                                            <p>
                                                O Adriano levanta menos peso que eu como que ele está em primeiro, nunca na vida isso ta de
                                                sacanagem!
                                            </p>


                                        </div>


                                        <div class="btn-group pull-right">
                                            <button class="btn btn-white btn-xs"><i class="fa fa-exclamation-triangle"></i> Ignorar</button>
                                            <button class="btn btn-white btn-xs"><i class="fa fa-check"></i> Verificado</button>
                                            <button class="btn btn-white btn-xs"><i class="fa fa-ban"></i> Bloquear</button>
                                        </div>
                                        <div style="clear: both;"></div>

                                    </div>

                                </div>

                                {{--fim--}}

                                <div>
                                    <button class="btn btn-sm btn-primary pull-right m-t-n-xs" type="button">
                                        <strong>Fechar Chamado</strong></button>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                {{-- footer --}}
            </div>

        </div>
    </div>
</div>




@endsection


<style>
    .ibox-title h5 {
        color: #000;
    }
</style>

@section('css')
	{!! Html::style("assets_admin/vendor/pnotify/pnotify.custom.css") !!}
	{!! Html::style("assets_admin/vendor/select2/select2.css") !!}
	{!! Html::style("assets_admin/vendor/bootstrap-fileupload/bootstrap-fileupload.min.css") !!}
@endsection
@section('scripts')
	{!! HTML::script("assets_admin/vendor/jquery-form/jquery.form.min.js") !!}
	{!! HTML::script("assets_admin/vendor/jquery-validation/jquery.validate.js") !!}
	{!! HTML::script("assets_admin/javascripts/forms/notificacao.validation.js") !!}
	{!! Html::script("assets_admin/vendor/pnotify/pnotify.custom.js") !!}
	{!! Html::script("assets_admin/vendor/jquery-maskedinput/jquery.maskedinput.min.js") !!}
	{!! Html::script("assets_admin/vendor/select2/select2.js") !!}
	{!! Html::script("assets_admin/vendor/bootstrap-fileupload/bootstrap-fileupload.min.js") !!}

@endsection

