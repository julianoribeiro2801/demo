<div class="ibox collapsed">
    <div class="ibox-title">
        <div class="ibox-tools">
            <a class="collapse-link">
                <h5>
                   Natação
                </h5>
                <i class="fa fa-chevron-up">
                </i>
            </a>
        </div>
    </div>
    <div class="ibox-content">
        
        {{-- explicacao --}}
        <div class="col-xs-12  pd0" style="padding-bottom: 20px">
            <span class="label label-warning" style=" padding: 5px; background: #676a6c;
                            float: left; margin-right: 5px">00</span> Denúncia <span style="margin-right:10px"></span> 
            <i class="fa fa-exclamation-triangle"  style="color: #676a6c; font-size:20px"></i>  Notificação <span style="margin-right:10px"></span> 
            <i class="fa fa-ban" style="color: #676a6c; font-size:20px"></i> Banido
        </div>
        {{-- fim--}}
        <div class="col-xs-12 col-md-12 pd0">
            <div class="input-group"><span class="input-group-addon"><i class="fa fa-search" aria-hidden="true"></i></span>
            <input type="text" class="form-control" id="filter"
        placeholder="Pesquisar na tabela"> </div>
    </div>
   
    
    
    <table class="footable table table-stripped" data-page-size="8" data-filter=#filter>
        <thead>
            <tr>
                <th></th>
                <td></td>
                
                <th style="width: 55%">Nome</th>
                
                <th style="width:15%">Qtde</th>
                
               
                <th > </th>
                <th > </th>
                <th > </th>

            </tr>
        </thead>
        <tbody>
            
            <tr ng-repeat="natacao in natacaos">
                <td>@{{natacao.NRPOSICAO}}</td>
                <td> 
                    <img ngf-size="{width: 60, height: 60, quality: 1}"
                        src="@{{natacao.FOTOALUNO}}"
                        alt="image" class="img-circle" style="width: 60px; height: 60px">
                </td>

                <td >@{{natacao.NMALUNO}}</td>

                <td>@{{natacao.QTCONQUISTA}} @{{natacao.UNIDADE}}</td>
                <td>
                    {{--  href="{{route('admin.clube.edit', $compra->id)}}"  --}}
                    <a href="#" alt="denuncia" style="margin-right: 15px"  >
                        <span class="label label-warning" style=" padding: 5px;
                        float: left;">@{{natacao.QTDDENUNCIA}}</span></a>


                </td>
                <td><a data-toggle="modal" data-target="#modalNotifica" ng-click="notificar(natacao.IDCOQNUISTA,natacao.IDALUNO)" alt="Editar" style="margin-right: 15px" >
                        <i class="fa fa-exclamation-triangle"  style="color: #fc0000; font-size:20px"></i> </a></td>
                <td>   <a href="#"  alt="Deletar"  ><i class="fa fa-ban" style="color: #676a6c; font-size:20px"></i></a></td>


            </tr>
            </tbody>
            <tfoot>
            <tr>
                <td colspan="7">
                <ul class="pagination pull-right"></ul>
            </td>
        </tr>
        </tfoot>
    </table>
</div>
</div>
