<div aria-hidden="true" class="modal inmodal fade" id="modalNotifica" role="dialog" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
				<h4 class="modal-title">Notificação</h4>
			</div>
			<form name="PostFormNotificacao" id="PostFormNotificacao" method="post" enctype="multipart/form-data">                    
                            <div class="modal-body">
                                <div style="display:none;"><input type="text" ng-model="notificacao.idaluno" name="aluno_id" id="aluno_id"></div>                                
                                <div style="display:none;"><input type="text" ng-model="notificacao.idconquista" name="conquista_id" id="conquista_id"></div>
                                <div class="form-group">
                                    <label>Descrição</label>
                                    <textarea lines="5" cols="100" class="form-control" name="dsnotificacao"></textarea>
                                </div>
                            </div>
                            <div class="row load" style="display:none;">
				<div class="col-sm-12">
                                    <div class="progress progress-striped light active">
					<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="" aria-valuemin="0" aria-valuemax="100">
                                            <span class="sr-only"></span>
					</div>
                                    </div>
				</div>
                            </div>
                            <div class="modal-footer">
                                <div class="form-group">
                                    <button type="submit" name="SendPostForm" id="SendPostForm" value="SendPostForm" class="btn btn-primary pull-right"><i class="fa fa-check"></i> Salvar</button>
				</div>
                            </div>
			</form>
		</div>
	</div>
</div>