<div class="modal inmodal fade" id="modalExibeDenuncia" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
				<h4 class="modal-title">Denuncias</h4>
			</div>
			<div class="modal-body">
					<div class="table-responsive">
                                <table class="table table-striped table-hover">
					<thead>
						<tr>
							<th>Id</th>
							
							<th style="width: 40%">Nome</th>
							<th style="width: 40%">data</th>
							<th> </th>
						</tr>
					</thead>

                                    <tbody>

                                        <tr ng-repeat="denuncia in denuncias">

                                            <td>
                                                @{{ denuncia.idalunodenuncia }}
                                            </td>
                                            <td>
                                                @{{ denuncia.name }}
                                            </td>
                                            <td>
                                                <!--@{{ denuncia.dtdenuncia}}-->
                                                @{{denuncia.dtdenuncia | date:'MM/dd/yyyy @ h:mma'}}
                                            </td>

                                        </tr>
                                    </tbody>
                                </table>
						</div>
			</div>

		</div>
	</div>
</div>