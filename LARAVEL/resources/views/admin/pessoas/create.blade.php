@extends('layouts.app')

@section('content')

<div class="container">
	<h3> Nova pessoa   </h3>
	
	{{-- @include('errors._check') --}}

	{!! Form::open(array('route' => 'admin.empresa.pessoas.store', 'class'=>'form')) !!}
   
    @include('admin.pessoas._form')

   	<div class="form-group">   		
   	{!! Form::submit('Criar ', ['class'=>'btn btn-primary']) !!}
   	</div>
	
	{!! Form::close() !!}

	
	
</div>

@endsection

