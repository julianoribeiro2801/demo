@extends('layouts.app')

@section('content')

<div class="container">
	<h3> Editando pessoa: {{$pessoa->nmpessoa}} </h3>
	
	 @include('errors._check')
	
	{!!  Form::model($pessoa, array('route' => array('admin.empresa.pessoas.update', $pessoa->id))) !!}
   	
    @include('admin.pessoas._form')

   	<div class="form-group">   		
   		<button class="btn btn-primary " type="submit"><i class="fa fa-check"></i>&nbsp;Salvar</button> 
   	</div>
	
	{!! Form::close() !!}
	
	
</div>

@endsection

