@extends('layouts.app')

@section('content')

<div class="container">
	<h3>Pessoas</h3>
	<br>
	<a href="{{ route('admin.empresa.pessoas.create') }}" class="btn btn-primary"> Novo pessoa</a>

	<br><br>
	<table class="table table-bordered">
		<thead>
			<tr>
				<th>Id</th>
				<th>Nome</th>
				<th>Data de nascimento</th>
				<th>Unidade</th>
				<th>Ações</th>

			</tr>
		</thead>
		<tbody>
		@foreach ($pessoas as $pessoa)
			<tr>
				<td> {{$pessoa->id}}</td>
				<td> {{$pessoa->nmpessoa}}</td>
				<td> {{$pessoa->dtnascimento}}</td>
				<th> {{$pessoa->idunidade}}</th>

				<td> <a href="{{route('admin.empresa.pessoas.edit', $pessoa->id)}}" class="btn btn-info">Editar</a></td>
				
			</tr>
			@endforeach
		</tbody>
	</table>

	{{-- {{{ $pessoas->render()}}} --}}

	
</div>

@endsection

