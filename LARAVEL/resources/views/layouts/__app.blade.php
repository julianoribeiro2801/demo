<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- CSRF Token -->
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<title>{{ config('app.name', 'Personal Club Brasil') }}</title>
		<!-- Styles -->
		<link href="{{ url('/') }}/css/app.css" rel="stylesheet">
		<!-- Scripts -->
		<script>
			window.Laravel = <?php echo json_encode(['csrfToken' => csrf_token(),]); ?>
		</script>
	</head>
	<body>
		<div id="app">
			<nav class="navbar navbar-default navbar-static-top">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
							<span class="sr-only">Menu</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="{{ url('/') }}">
						   Personal Club Brasil {{-- {{ config('app.name', 'Personal Club Brasil') }} --}}
						</a>
					</div>
					<div class="collapse navbar-collapse" id="app-navbar-collapse">
						<ul class="nav navbar-nav">
							<li><a href="{{ url('admin') }}"> Home</a></li>
						</ul>
						<ul class="nav navbar-nav navbar-left">
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
									Cadastros <span class="caret"></span>
								</a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="{{ url('admin/funcionarios') }}">Funcionarios</a></li>
									<li><a href="{{ url('admin/unidades') }}">Unidades</a></li>
									<li><a href="{{ url('admin/pessoas') }}">Pessoas</a></li>
									<li><a href="{{ url('admin/alunos') }}">Alunos</a></li>
									<li><a href="{{ url('admin') }}"> Hope</a></li>
									<li><a href="{{ url('treino') }}"> Treino</a></li>
									<li><a href="{{ url('admin/tipoconquistas') }}"> Tipo de conquistas</a></li>
									<li><a href="{{ url('admin/gruposmusculares') }}"> Grupos musculares</a></li>
									<li><a href="{{ url('admin/atividades') }}"> Atividades</a></li>
									<li><a href="{{ url('admin/tipomensagens') }}">Tipo de mensagem</a></li>
									<li><a href="{{ url('admin/conquistas') }}">Conquistas</a></li>
								</ul>
							</li>
						</ul>
						<ul class="nav navbar-nav navbar-right">
							@if (Auth::guest())
								<li><a href="{{ url('/login') }}">Login</a></li>
								<li><a href="{{ url('/register') }}">Register</a></li>
							@else
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
										{{ Auth::user()->name }} <span class="caret"></span>
									</a>
									<ul class="dropdown-menu" role="menu">
										<li>
											<a href="{{ url('/logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout</a>
											<form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
												{{ csrf_field() }}
											</form>
										</li>
									</ul>
								</li>
							@endif
						</ul>
					</div>
				</div>
			</nav>
			@yield('content')
		</div>
		<!-- Scripts -->
		<script src="{{ url('/') }}/js/app.js"></script>
	</body>
</html>