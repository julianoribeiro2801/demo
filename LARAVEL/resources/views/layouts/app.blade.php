<!DOCTYPE html>
<html lang="pt-br" ng-app="personalApp">
	<head>
		<meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="csrf_token" content="{{ csrf_token() }}">
		<meta name="user_logued" content="{{ Auth::user() ? Auth::user()->id : null }}">
		<meta name="unidade" content="{{ $idunidade }}">
		<meta name="user_email" content="{{ Auth::user() ? Auth::user()->email : null }}">
		<title>Personal Club Brasil | Admin</title>
		<script src="https://use.fontawesome.com/f684d81203.js"></script>
		{{-- {!! Html::style(elixir('dist/app.css')) !!} --}}
		{!! Html::style(elixir('dist/sweetalert.css')) !!}
		{!! Html::style("tema_assets/css/bootstrap.min.css") !!}

		<script>
			window.Laravel = <?php echo json_encode(['csrfToken' => csrf_token()]); ?>
		</script>
		<!-- Latest compiled and minified CSS & JS -->
		<script src="//code.jquery.com/jquery.js"></script>

		{{-- forms e componentes --}}
		{!! Html::style("tema_assets/css/plugins/iCheck/custom.css") !!}
		{!! Html::style("tema_assets/css/plugins/chosen/chosen.css") !!}
		{!! Html::style("tema_assets/css/plugins/colorpicker/bootstrap-colorpicker.min.css") !!}
		{!! Html::style("tema_assets/css/plugins/cropper/cropper.min.css") !!}
		{!! Html::style("tema_assets/css/plugins/switchery/switchery.css") !!}
		{!! Html::style("tema_assets/css/plugins/jasny/jasny-bootstrap.min.css") !!}
		{!! Html::style("tema_assets/css/plugins/nouslider/jquery.nouislider.css") !!}
		{!! Html::style("tema_assets/css/plugins/datapicker/datepicker3.css") !!}
		{!! Html::style("tema_assets/css/plugins/ionRangeSlider/ion.rangeSlider.css") !!}
		{!! Html::style("tema_assets/css/plugins/ionRangeSlider/ion.rangeSlider.skinFlat.css") !!}
		{!! Html::style("tema_assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css") !!}
		{!! Html::style("tema_assets/css/plugins/switchery/switchery.css") !!}
		{!! Html::style("tema_assets/css/plugins/clockpicker/clockpicker.css") !!}
		{!! Html::style("tema_assets/css/plugins/daterangepicker/daterangepicker-bs3.css") !!}
		{!! Html::style("tema_assets/css/plugins/select2/select2.min.css") !!}

		<!-- Padrão -->
		{!! Html::style("tema_assets/css/animate.css") !!}
		{!! Html::style("tema_assets/css/style.css") !!}
		{!! Html::style("tema_assets/css/plugins/chosen/bootstrap-chosen.css") !!}
		<!-- FooTable -->
		{!! Html::style("tema_assets/css/plugins/footable/footable.core.css") !!}
		<!-- Select2 -->
		{!! Html::style("tema_assets/css/plugins/select2/select2.min.css") !!}

		{!! Html::style("css/app.css") !!}

		<!--CSS ESPECÃFICOS-->
			@yield('css')
		<!--CSS ESPECÃFICOS-->
	</head>

	<body class="mini-navbar">
		

		<div id="wrapper">

			@if (Session::get('dias') < 0 &&  Session::get('licenca') === 'TRIAL' )
				{{--  redirecionar para a licenca acabou --}}
				@if (Request::is('admin/trial'))
					{{-- nao faz nada --}}
				@else
					{{-- {{ Redirect::to('/admin/trial') }}  --}}
					<script>
						 window.location = '/admin/trial';
					</script>
				@endif
					
				 {{-- remover menu --}}
					<style>
						#wrapper { margin-left: -38px; }
					</style>
					
			@else
			
				@if(Auth::user()->role=='admin'  && Auth::user()->passo < 3)
				   {{-- nao fez o passo a passo --}}
					<style>
						#wrapper { margin-left: -38px; }
					</style>			   
				@elseif(Auth::user()->role=='admin'  && Auth::user()->passo > 2)
					@include('layouts.menus.menuAdmin')
				@endif
				
				@if(Auth::user()->role=='adminfilial')
					@include('layouts.menus.menuAdmin')
				@endif

				@if(Auth::user()->role=='coordenador')
					@include('layouts.menus.menuCoordenador')
				@endif

				@if(Auth::user()->role=='professor')
					@include('layouts.menus.menuProfessor')
				@endif

				@if(Auth::user()->role=='nutricionista')
					@include('layouts.menus.menuNutricionista')
				@endif

				@if(Auth::user()->role=='recepcionista')
					@include('layouts.menus.menuRecepcionista')
				@endif

			@endif  
			 {{--  fim do if trial--}}
			<div id="page-wrapper" class="gray-bg">
				<div class="row border-bottom">
					<nav class="navbar navbar-static-top" role="navigation" style="margin-bottom:0;">
						<div class="navbar-header visible-xs "> {{--  --}}
							<a class="navbar-minimalize minimalize-styl-2 btn btn-primary" href="#"><i class="fa fa-bars"></i></a>
						</div>
						<div class="navbar-header mgacad_top">
							<select class="form-control" id="myEmpresa" onchange="mudar_empresa()">
								@forelse($unidades_combo as $unidade)
									@if (Auth::user()->idunidade == $unidade->id && Auth::user()->role!='admin')
										
									 	<option value="{{$unidade->id}}"
										 @if($unidade->id == $idunidade) selected @endif>
										 {{$unidade->fantasia}}
										</option>

									@endif
									@if (Auth::user()->role == 'admin')
										<option value="{{$unidade->id}}"
											 @if($unidade->id == $idunidade) selected @endif>
											 {{$unidade->fantasia}}
										</option>
									@endif
								

								@empty
								@endforelse
							</select>
						</div>
						<script>
							function mudar_empresa() {
								var idemp = document.getElementById("myEmpresa").value;
								var url_laravel =  "{{url('admin')}}/changeEmp/";
								var redir =  url_laravel + idemp;
								window.location.href = redir;
							}
						</script>
						@include('layouts.top-esquerdo')
					</nav>
				</div>
				@include('layouts.header_title')

				@yield('content')
				<div class="footer">
					<div><strong>Copyright</strong> Personal Club Brasil &copy; 2016 - <?php echo date("Y")?></div>
				</div>
			</div>
		</div>
		
		{!! HTML::script("js/app.js") !!}

		<script>
			var app =  angular.module('personalApp', ['ui.utils.masks','ngMaterial','ngFileUpload']).run(function ($rootScope, $http) {

			});
		</script>
		@stack('script')
		<!-- Custom and plugin javascript -->
		{!! HTML::script("tema_assets/js/inspinia.js") !!}
		{!! HTML::script("tema_assets/js/plugins/pace/pace.min.js") !!}
		{!! HTML::script("tema_assets/js/plugins/slimscroll/jquery.slimscroll.min.js") !!}

		<!-- Chosen -->
		{!! HTML::script("tema_assets/js/plugins/chosen/chosen.jquery.js") !!}

		{!! HTML::script("tema_assets/js/plugins/metisMenu/jquery.metisMenu.js") !!}
		{!! HTML::script("tema_assets/js/plugins/jeditable/jquery.jeditable.js") !!}
		{!! HTML::script("tema_assets/js/plugins/switchery/switchery.js") !!}

		<!-- JSKnob -->
		{!! Html::script("tema_assets/js/plugins/jsKnob/jquery.knob.js") !!}

		<!-- Input Mask-->
		{!! Html::script("tema_assets/js/plugins/jasny/jasny-bootstrap.min.js") !!}
		{!! Html::script("tema_assets/js/plugins/select2/select2.full.min.js") !!}


		<!-- Data picker -->
		{!! Html::script("tema_assets/js/plugins/datapicker/bootstrap-datepicker.js") !!}
		{!! HTML::script("tema_assets/js/plugins/datapicker/locales/bootstrap-datepicker.pt-BR.js") !!}

		<!-- Clock picker -->
		{!! HTML::script("tema_assets/js/plugins/clockpicker/clockpicker.js") !!}

		<!-- NouSlider -->
		{!! Html::script("tema_assets/js/plugins/nouslider/jquery.nouislider.min.js") !!}

		@yield('scripts')
		@stack('scripts')
		<script>
			var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));

			elems.forEach(function(html) {
				var switchery = new Switchery(html, {color: 'red'});
			});
		</script>
	</body>
</html>