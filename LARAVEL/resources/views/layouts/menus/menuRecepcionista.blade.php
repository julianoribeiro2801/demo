<?php
	$url = explode("admin/", Request::url()); 
	if(count($url)>1) {
		$cat = explode("/", $url[1]); 
	} else {
		$cat[0] = '';
	}
?>
<nav class="navbar-default navbar-static-side" role="navigation">
	<div class="sidebar-collapse">
		<ul class="nav metismenu" id="side-menu">
			<li class="nav-header">
				<div class="dropdown profile-element"><span>
					<img alt="image" class="img-circle" src="{{ url('tema_assets/img/profile_small.jpg') }}">				   
					<a data-toggle="dropdown" class="dropdown-toggle" href="#">
						<span class="clear">
							<span class="block m-t-xs"><strong class="font-bold">Adriano Ruiz</strong></span>
							<span class="text-muted text-xs block">Professor <b class="caret"></b></span>
						</span>
					</a>
					<ul class="dropdown-menu animated fadeInRight m-t-xs">
						<li><a href="#">Perfil</a></li>
						<li><a href="#">Contatos</a></li>
						<li><a href="#">Caixa de Entrada</a></li>
						<li class="divider"></li>
						<li><a href="#">Sair</a></li>
					</ul>
				</div>
				<div class="logo-element">PClub</div>
			</li>

			<li class="{{ Request::is('admin') ? 'active' : '' }} pd20bt">
				<a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i>
					<span class="nav-label">PAINEL</span>
				</a>
			</li>
		
			{{-- <li class="{{ $cat[0] == 'empresas' ? 'active' : '' }} pd20bt">
				<a href="{{ url('admin/empresas/unidades') }}">
					<i class="fa fa-bars"></i>
					<span class="nav-label">EMPRESA</span> <span class="fa arrow"></span>
				</a>
			</li> --}}
		
			<li class="{{ $cat[0] == 'clientes' ? 'active' : '' }} pd20bt">
				<a href="#"><i class="fa fa-users"></i>
				 <span class="nav-label">CLIENTES</span> <span class="fa arrow"></span></a>
				<ul class="nav nav-second-level collapse">
					<li class="{{ Request::is('admin/clientes/clientes') ? 'active' : '' }}">
						<a href="{{ url('admin/clientes/clientes') }}">Clientes</a>
					</li>
					
				{{-- 	<li class="{{ Request::is('admin/clientes/mensagens') ? 'active' : '' }}">
						<a href="{{ url('admin/clientes/mensagens') }}">Mensagens</a>
					</li>
					<li class="{{ Request::is('admin/clientes/tipomensagens') ? 'active' : '' }}">
						<a href="{{ url('admin/clientes/tipomensagens') }}">Tipo de mensagem</a>
					</li> --}}
				</ul>
			</li>
			<li class="{{ $cat[0] == 'prescricao' ? 'active' : '' }} pd20bt">
				<a href="#"><i class="fa fa-edit"></i>
					<span class="nav-label">TREINO</span>
					<span class="fa arrow"></span>
				</a>
				<ul class="nav nav-second-level collapse">
					<li class="{{ Request::is('admin/prescricao/nova') ? 'active' : '' }}">
						<a href="{{ url('admin/prescricao/nova') }}"> Prescrição</a>
					</li>
					<li class="{{ Request::is('admin/prescricao/configuracoes') ? 'active' : '' }}">
						<a href="{{ url('admin/prescricao/configuracoes') }}"> Configurações</a>
					</li>
					<li class="{{ Request::is('admin/prescricao/treinopadrao') ? 'active' : '' }}">
						<a href="{{ url('admin/prescricao/treinopadrao') }}"> Treino Padrão</a>
					</li>                                        

				</ul>
			</li>

			<li class="{{ Request::is('admin/gfms') ? 'active' : '' }} pd20bt">
				<a href="{{ url('admin/gfms') }}">
					<i class="fa fa-table"></i>
					<span class="nav-label"> GFM</span>
				</a>
			</li>


                        
        
			<li class="pd20bt">
			    <a href="#"><i class="fa fa-puzzle-piece" aria-hidden="true"></i>
			      <span class="nav-label"> Apps</span>
			   </a>
			    <ul class="nav nav-second-level collapse" style="width: 180px">
			     
				  <li class="{{ Request::is('admin/agendamento') ? 'active' : '' }}" ><a href="{{ url('admin/agendamento') }}"> Agenda Avaliação</a></li>

				  <li class="{{ Request::is('admin/clientes/chat') ? 'active' : '' }}">
						<a href="{{ url('admin/clientes/chat') }}">Chat</a>
					</li>

			        <li class="{{ Request::is('admin/clube') ? 'active' : '' }}" ><a href="{{ url('admin/clube') }}"> Clube de Vantagens</a></li>
			        <li class="{{ Request::is('admin/desafios') ? 'active' : '' }}" ><a href="{{ url('admin/desafios') }}"> Desafios</a></li>
			       
			    
                    <li class="{{ Request::is('admin/push') ? 'active' : '' }}" ><a href="{{ url('admin/push') }}"> Push</a></li>


			    </ul>
			</li>

{{-- 
			<li class="{{ Request::is('apps') ? 'active' : '' }} pd20bt">
				<a href="{{ url('apps') }}">
					<i class="fa fa-shopping-cart"></i>
					<span class="nav-label"> Loja Apps</span>
				</a>
			</li> --}}
                   
		</ul>
	</div>
</nav>
