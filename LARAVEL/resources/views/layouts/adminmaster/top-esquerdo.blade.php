<ul class="nav navbar-top-links navbar-right">
    <li>
        <span class="m-r-sm text-muted welcome-message">Bem Vindo <?php echo Auth::user()->name;?></span>
    </li>
    <li class="dropdown">
        <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
            <i class="fa fa-gift" style="font-size: 18px"></i>  {{-- <span style="right: -1px;" class="label label-warning">16</span> --}}
        </a>

    </li>
    <li class="dropdown">
        <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
            <i class="fa fa-bell"></i>  {{--<span class="label label-primary">8</span>--}}
        </a>

    </li>


    <li>
        <a href="{{ url('/logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
            <i class="fa fa-sign-out" style="font-size:20px" title="Logout" alt="Logout"></i>
        </a>
        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>
    </li>
</ul>