<?php
	$url = explode("admin/", Request::url()); 
	if(count($url)>1) {
		$cat = explode("/", $url[1]); 
	} else {
		$cat[0] = '';
	}
?>
<nav class="navbar-default navbar-static-side" role="navigation">
	<div class="sidebar-collapse">
		<ul class="nav metismenu" id="side-menu">
			<li class="nav-header">
				<div class="dropdown profile-element"><span>
					<img alt="image" class="img-circle" src="{{ url('tema_assets/img/profile_small.jpg') }}">				   
					<a data-toggle="dropdown" class="dropdown-toggle" href="#">
						<span class="clear">
							<span class="block m-t-xs"><strong class="font-bold">Adriano Ruiz</strong></span>
							<span class="text-muted text-xs block">Professor <b class="caret"></b></span>
						</span>
					</a>
					<ul class="dropdown-menu animated fadeInRight m-t-xs">
						<li><a href="#">Perfil</a></li>
						<li><a href="#">Contatos</a></li>
						<li><a href="#">Caixa de Entrada</a></li>
						<li class="divider"></li>
						<li><a href="#">Sair</a></li>
					</ul>
				</div>
				<div class="logo-element">PClub</div>
			</li>

			<li class="{{ Request::is('adminmaster') ? 'active' : '' }} pd20bt">
				<a href="{{ url('adminmaster') }}"><i class="fa fa-dashboard"></i>
					<span class="nav-label">PAINEL</span>
				</a>
			</li>
			

			<li class="{{ $cat[0] == 'clientes' ? 'active' : '' }} pd20bt">
				<a href="index.html"><i class="fa fa-users"></i>
				 <span class="nav-label">CLIENTES</span> <span class="fa arrow"></span></a>
				<ul class="nav nav-second-level collapse">
					<li class="{{ Request::is('adminmaster/') ? 'active' : '' }}">
						<a href="{{ url('adminmaster/') }}">Clientes</a>
					</li>
				{{-- 	<li class="{{ Request::is('admin/clientes/chat') ? 'active' : '' }}">
						<a href="{{ url('admin/clientes/chat') }}">Chat</a>
					</li>
					<li class="{{ Request::is('admin/clientes/mensagens') ? 'active' : '' }}">
						<a href="{{ url('admin/clientes/mensagens') }}">Mensagens</a>
					</li>
					<li class="{{ Request::is('admin/clientes/tipomensagens') ? 'active' : '' }}">
						<a href="{{ url('admin/clientes/tipomensagens') }}">Tipo de mensagem</a>
					</li> --}}
				</ul>
			</li>
			
			<li class="{{ $cat[0] == 'empresas' ? 'active' : '' }} pd20bt">
				<a href="{{ url('adminmaster/empresas') }}">
					<i class="fa fa-bars"></i>
					<span class="nav-label">Cli Info</span> <span class="fa arrow"></span>
				</a>
			</li>


		</ul>
	</div>
</nav>
