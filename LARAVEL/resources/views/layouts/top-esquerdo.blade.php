<ul class="nav navbar-top-links navbar-right">
    <li>
        <span class="m-r-sm text-muted welcome-message">Bem Vindo, <?php echo Auth::user()->name;?></span>
    </li>

    <li class="dropdown">
        {{--  
        <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
            <i class="fa fa-gift" style="font-size: 18px"></i>  <span style="right: -1px;" class="label label-warning">16</span> 
        </a>--}}
         {{-- 
        <ul class="dropdown-menu dropdown-messages">
            <li>
                <div class="dropdown-messages-box">
                    <a href="profile.html" class="pull-left">
                        <img alt="image" class="img-circle" src="{{ url('tema_assets') }}/img/a7.jpg">
                    </a>
                    <div class="media-body">
                        <small class="pull-right">46h atrás</small>
                        <strong>Adriano Ruiz</strong> atualizou o treino de <strong>Monica Smith</strong>. <br>
                        <small class="text-muted">3 dias atrás em 7:58 pm - 10.06.2017</small>
                    </div>
                </div>
            </li>
            <li class="divider"></li>
            <li>
                <div class="dropdown-messages-box">
                    <a href="profile.html" class="pull-left">
                        <img alt="image" class="img-circle" src="{{ url('tema_assets') }}/img/a4.jpg">
                    </a>
                    <div class="media-body ">
                        <small class="pull-right text-navy">5h atrás</small>
                        <strong>Chris</strong> atualizou o treino de <strong>Monica Smith</strong>. <br>
                        <small class="text-muted">Ontem 1:21 pm - 11.06.2017</small>
                    </div>
                </div>
            </li>
            <li class="divider"></li>
            <li>
                <div class="dropdown-messages-box">
                    <a href="profile.html" class="pull-left">
                        <img alt="image" class="img-circle" src="{{ url('tema_assets') }}/img/profile.jpg">
                    </a>
                    <div class="media-body ">
                        <small class="pull-right">23h atrás</small>
                        <strong>Monica Smith</strong> mudou de treino de <strong>Kim Smith</strong>. <br>
                        <small class="text-muted">2 dias atrás em  2:30 am - 11.06.2017</small>
                    </div>
                </div>
            </li>
            <li class="divider"></li>
            <li>
                <div class="text-center link-block">
                    <a href="mailbox.html">
                        <i class="fa fa-envelope"></i> <strong>Ler todas</strong>
                    </a>
                </div>
            </li>
        </ul> --}}
    </li>


    <li class="dropdown">
        <a class="count-info"  href="{{ url('/admin/clientes/chat') }}">
            <i class="fa fa-comment" style="font-size: 18px;"></i>  
            @if($totalmsg[0]->total >0)
            <span class="label label-primary"> {{ $totalmsg[0]->total }}</span>
            @endif
        </a>
        {{-- 
        <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
            <i class="fa fa-comment" style="font-size: 18px;"></i>  
            <span class="label label-primary">3</span>
        </a>
        <ul class="dropdown-menu dropdown-alerts">
            <li>
                <a href="mailbox.html">
                    <div>
                        <i class="fa fa-envelope fa-fw"></i> Você tem 16 mensagens
                        <span class="pull-right text-muted small">4 minutos atrás</span>
                    </div>
                </a>
            </li>
            <li class="divider"></li>
            <li>
                <a href="profile.html">
                    <div>
                        <i class="fa fa-twitter fa-fw"></i> 3 Novas Mensagens
                        <span class="pull-right text-muted small">12 minutos atrás</span>
                    </div>
                </a>
            </li>
            <li class="divider"></li>
            <li>
                <a href="grid_options.html">
                    <div>
                        <i class="fa fa-upload fa-fw"></i> Servidor reiniciado
                        <span class="pull-right text-muted small">4 minutos atrás</span>
                    </div>
                </a>
            </li>
            <li class="divider"></li>
            <li>
                <div class="text-center link-block">
                    <a href="notifications.html">
                        <strong>Ver todos</strong>
                        <i class="fa fa-angle-right"></i>
                    </a>
                </div>
            </li>
        </ul>  --}}
    </li>



    <li>
        <a href="{{ url('/logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
            <i class="fa fa-sign-out" style="font-size:20px" title="Logout" alt="Logout"></i>
        </a>
        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>
    </li>
</ul>