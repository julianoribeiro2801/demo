@extends('layouts.app')

@section('content')

<div class="container">
    <h3> Lp</h3>

    <a href="{{ url('lp/adriano')}}"><i class="fa fa-heart"></i>
        <span class="nav-label"> Ver LP</span>
    </a>

    <div style="clear:both">
    </div>

    <div class="ibox">
        <div class="ibox-content">
            <h2>Modalidades</h2>
            <table class="table table-striped table-hover">
                <tbody>
                    <tr>
                        <td><label>Resultados</label></td>
                        <td>
                            <div class="switch">
                                <div class="onoffswitch">
                                    <input type="checkbox" checked class="onoffswitch-checkbox">
                                    <label class="onoffswitch-label">
                                        <span class="onoffswitch-inner" ></span>
                                        <span class="onoffswitch-switch"></span>
                                    </label>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td><label>Profissionais</label></td>
                        <td>
                            <div class="switch">
                                <div class="onoffswitch">
                                    <input type="checkbox" checked class="onoffswitch-checkbox">
                                    <label class="onoffswitch-label">
                                        <span class="onoffswitch-inner" ></span>
                                        <span class="onoffswitch-switch"></span>
                                    </label>
                                </div>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

</div>

@endsection




