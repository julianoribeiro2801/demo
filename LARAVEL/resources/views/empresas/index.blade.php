@extends('layouts.app')

@section('content')
	@push('scripts')
		{!! Html::script("js/empresasController.js") !!}
		<!-- NouSlider -->
{!! Html::script("tema_assets/js/plugins/nouslider/nouislider.min.js") !!}
{!! Html::script("tema_assets/js/plugins/nouslider/wNumb.js") !!}
	@endpush

<!-- NouSlider -->
{!! Html::style("tema_assets/css/plugins/nouslider/nouislider.min.css") !!}

	<div class="wrapper wrapper-content animated fadeInRight" ng-controller="empresasController">
		<div class="row">
			<div class="col-sm-4">
				<div class="ibox">
					<div class="ibox-content">
						<form name="PostFormEmpresaUp" id="PostFormEmpresaUp" method="post" enctype="multipart/form-data">
							<div class="col-lg-12 text-center">
								<div class="form-group">
									<div class="fileupload fileupload-new" data-provides="fileupload">
										<div class="fileupload-preview thumbnail img-circle" style="width:150px;height:150px;padding:0;">
											<img src="../../uploads/unidade/@{{empresa.logo}}" id="imagem-empresa" style="width:150px;height:150px;">
										</div>
										<div class="input-append">
											<span class="btn btn-default btn-file" style="border:0;">
												<span class="fileupload-exists"><i class="fa fa-camera" aria-hidden="true"></i></span>
												<span class="fileupload-new"><i class="fa fa-camera" aria-hidden="true"></i></span>
												<input type="file" name="logo" id="file-input">
											</span>
										</div>
									</div>
								</div>
							</div>
							<span class="pull-right">
								<button type="button" ng-click="changeStatus()" ng-show="inactive" class="btn btn-primary btn-sm pull-right">
									<i class="fa fa-edit"></i> Editar
								</button>
							</span>
							<strong>Dados da Empresa </strong>
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<label>Nome Fantasia:</label>
										<input type="text" class="form-control fantasia" ng-model="empresa.fantasia"  name="fantasia" ng-disabled="inactive">
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label>Razão Social:</label>
										<input type="text" class="form-control razao_social" ng-model="empresa.razao_social"  name="razao_social" ng-disabled="inactive">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<label>CNPJ:</label>
										<input type="text" class="form-control cnpj" ng-model="empresa.cnpj"  name="cnpj" ng-disabled="inactive">
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label>Inscrição Estadual:</label>
										<input type="text" class="form-control inscricao_estadual" ng-model="empresa.inscricao_estadual"name="inscricao_estadual" ng-disabled="inactive">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<label>Endereço:</label>
										<input type="text" class="form-control endereco" ng-model="empresa.endereco"  name="endereco" ng-disabled="inactive">
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label>Número:</label>
										<input type="text" class="form-control numero" ng-model="empresa.numero"  name="numero" ng-disabled="inactive">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<label>CEP:</label>
										<input type="text" class="form-control cep" 
										ng-model="empresa.cep" ng-change="getCep(empresa.cep)" name="cep" ng-disabled="inactive">
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label>Bairro:</label>
										<input type="text" class="form-control bairro" ng-model="empresa.bairro" ng-disabled="inactive">
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<label>Estado:</label>
										<select class="form-control" ng-model="empresa.idestado" name="idestado" ng-change="changeEstado(empresa.idestado)" ng-disabled="inactive">
											<option value="">Selecione</option>
											<option ng-repeat="estado in estados" ng-value="estado.id">@{{estado.nome}}/@{{estado.uf}}</option>
										</select>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label>Cidade:</label>
										<select class="form-control" ng-model="empresa.idcidade" name="idcidade" ng-disabled="inactive" >
											<option value="">Selecione</option>
											<option ng-repeat="cidade in cidades" ng-value="cidade.id">@{{cidade.nome}}</option>
										</select>
									</div>
								</div>
							</div>

							

							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<label>Telefone:</label>
										<input type="text" class="form-control masktelefonefixo" ng-model="empresa.telefone" name="telefone" ng-disabled="inactive">
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label>Celular:</label>
										<input type="text" class="form-control masktelefone" ng-model="empresa.celular" name="celular" ng-disabled="inactive">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<label>E-mail:</label>
										<input type="text" class="form-control email" ng-model="empresa.email" name="email" ng-disabled="inactive">
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label>Site:</label>
										<input type="text" class="form-control site" ng-model="empresa.site" name="site" ng-disabled="inactive">
									</div>
								</div>
							</div>
							<div class="row loadEmpresa" style="display:none;">
								<div class="col-sm-12">
									<div class="progress progress-striped light active">
										<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="" aria-valuemin="0" aria-valuemax="100">
											<span class="sr-only"></span>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
                                                                <button class="btn btn-primary" ng-click="upEmpresa(empresa)" type="button"> Salvar</button>
								<!--<button type="submit" name="SendPostForm" id="SendPostForm" value="SendPostForm" class="btn btn-primary pull-right" ng-hide="inactive"><i class="fa fa-check"></i> Salvar</button>-->
							</div>							
							<div class="clearfix"></div>
						</form>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="ibox">
					<div class="ibox-content">
						<span class="text-muted small pull-right">
							<a class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modalNovaFilial">
								<i class="fa fa-plus-square"></i> Nova Filial
							</a>
						</span>
						<h2>Filiais</h2>
						<div class="table-responsive">
							<table class="table table-striped table-hover">
								<tbody>
									<tr ng-repeat="filial in empresa.children">
										<td><label><i class="fa fa-exclamation" ng-if="filial.situacao == 0" style="color:#f8ac59;font-size:16px;"></i> @{{filial.fantasia | uppercase}}</label></td>
										<td>
											<div class="pull-right">
												<button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modalEditalFilial" ng-click="editFilial(filial.id)">
													<i class="fa fa-edit"></i>
												</button>
												<button class="btn btn-danger btn-sm" ng-click="deleteFilial(filial.id)">
													<i class="fa fa-trash"></i>
												</button>
											</div>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="ibox">
					<div class="ibox-content">
						<span class="text-muted small pull-right">
							<a class="btn btn-primary btn-sm" data-toggle="modal" ng-click="novo()" data-target="#modalNovoLocal">
								<i class="fa fa-plus-square"></i> Novo Local
							</a>
						</span>
						<h2>Locais / Ambientes</h2>
						<div class="table-responsive">
							<table class="table table-striped table-hover">
								<tbody>
									<tr ng-repeat="local in locais">
										<td><label>@{{ local.nmlocal }}</label></td>
										<td>@{{ local.metros }}m2</td>
										<td>
											<div class="pull-right">
												<button  class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modalEditalLocal" ng-click="editLocal(local.id)">
													<i class="fa fa-edit"></i>
												</button>
												<button class="btn btn-danger btn-sm" ng-click="deleteLocal(local.id)">
													<i class="fa fa-trash"></i>
												</button>
											</div>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="ibox">
					<div class="ibox-content">
						<span class="text-muted small pull-right">
							<a class="btn btn-primary btn-sm" data-toggle="modal" ng-click="novo()" data-target="#modalNovoNutricionista">
								<i class="fa fa-plus-square"></i> Novo Coach
							</a>
						</span>
						<h2>Coach</h2>
						<div class="table-responsive">
							<table class="table table-striped table-hover">
								<tbody>
									<tr ng-repeat="nutricionista in nutricionistas">
										<td><label>@{{ nutricionista.name }}</label></td>
										<td>
											<div class="pull-right">
												<button  class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modalEditalNutricionista" ng-click="editNutricionista(nutricionista.id)">
													<i class="fa fa-edit"></i>
												</button>
												<button class="btn btn-danger btn-sm" ng-click="deleteNutricionista(nutricionista.id)">
													<i class="fa fa-trash"></i>
												</button>
											</div>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				 </div>
			</div>

                    <div class="col-sm-4">

                        {{-- configurações GFM --}}
                        <div class="ibox">
                            <div class="ibox-content">
                                <h2>Configurações GFM</h2>
                                <div style="margin-top:20px"></div>
                                <b>1- Permitir reservas com quantos dias antecedência?</b><br>
                                
                                <select name="" id="input" ng-model="configura.diasantecedencia" class="form-control" required="required" 
                                        style="margin-top:5px;     width: 90%; float: right;"
                                        ng-change="upConfiguraGfm(configura.diasantecedencia,'diasantecedencia')">
                                    <option ng-repeat="antes in antecede" ng-value="antes.valor">@{{antes.texto}}</option>
                                    
                                </select>

                                <div style="clear:both"></div>
                                <div style="margin-top:20px"></div>
                                <b>2- Permitir quantas reservas de aulas antecipadas por dia?</b>
                                <select name="" id="input" ng-model="configura.aulasantecipadas" class="form-control" required="required" 
                                        style="margin-top:5px;     width: 90%; float: right;"
                                        ng-change="upConfiguraGfm(configura.aulasantecipadas,'aulasantecipadas')" onchange="$('.sm_termino').toggle(100)">
                                    <option ng-repeat="antec in antecipa" ng-value="antec.valor">@{{antec.texto}}</option>
                                    
                                </select>


                                <div style="clear:both"></div>
                                <div class="sm_termino" style="display:none; margin-left:20px;">
                                    <div style="margin-top:20px"></div>
                                    <b>2.1- Intervalo de tempo entre as aulas</b>
                                    <!--<b>2.1- Permitir reserva somente após o término da reserva anterior?</b>-->
                                    <select ng-model="configura.intervalo" ng-change="upConfiguraGfm(configura.intervalo,'intervalo')" name="" id="input" class="form-control" required="required" 
                                        <option ng-repeat="intervalo in intervalos" ng-value="intervalo.valor">@{{intervalo.texto}}</option>

                                    </select>
                                </div>


                                <div style="clear:both"></div>
                                <div style="margin-top:20px"></div>

                                <b>3- Remover reserva sem confirmação até 15 minutos antes ?</b>

                                <select ng-model="configura.remove" name="" id="input" class="form-control" required="required" 
                                        style="margin-top:5px;  width: 90%; float: right;"
                                        ng-change="upConfiguraGfm(configura.remove,'remove')">
                                        <!--onchange="$('.sm_semelhante').toggle(100)" >-->

                                    <!--<option value="1" selected>SIM</option>
                                    <option value="2">NÃO</option>-->
                                    <option ng-repeat="sn in simnao" ng-value="sn.valor">@{{sn.texto}}</option>

                                </select>

                                <div style="clear:both"></div>
                                <div style="margin-top:20px"></div>


                                <div style="clear:both"></div>
                                <div class="sm_semelhante" style="margin-left:20px;">
                                    <div style="margin-top:20px"></div>
                                    <b>3.1- Oferecer esta vaga para clientes semelhantes?</b>
                                    <select ng-model="configura.oferecevaga" name="" id="input" class="form-control" required="required" 
                                            style="margin-top:5px;     width: 88%; float: right;" ng-change="upConfiguraGfm(configura.oferecevaga,'oferecevaga')">>
                                        <option ng-repeat="sn in simnao" ng-value="sn.valor">@{{sn.texto}}</option>

                                    </select>
                                </div>
                                {{-- inicio 4 --}}
                                <div style="clear:both"></div>
                                <div style="margin-top:20px"></div>

                                <b>4- Habilitar campo "Vagas para clientes Premium"?</b>

                                <select ng-model="configura.premium" name="" id="input" class="form-control" required="required" 
                                        style="margin-top:5px;  width: 90%; float: right;" ng-change="upConfiguraGfm(configura.premium,'premium')">>

                                    <!--<option value="1" selected>SIM</option>
                                    <option value="2">NÃO</option>-->
                                    <option ng-repeat="sn in simnao" ng-value="sn.valor">@{{sn.texto}}</option>

                                </select>


                                <div style="clear:both"></div>
                                <div style="margin-top:20px"></div>

                                <div class="row">
                                    <div class="col-lg-12"> 
                                        <b>5- Indíce de Clasificação Visual de audiência</b> 
                                        (<span style="color: #ed5565">ruim</span>,
                                        <span style="color: #fed862">regular</span>,
                                        <span style="color: #67b293">bom</span>)
                                        <div style="margin-top:40px"></div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div id="range_slider"></div>
                                    </div>

                                    <!--<div class="col-lg-12">
                                        <input type="text" name="input-format" id="input-format">
                                    	<div class="v_range"> @{{ v_range }} </div>
                                       <button type="button" ng-click="qualValor()" class="btn btn-default">Qual valor</button>
                                    </div>-->
                                </div>

                            </div>
                        </div>



                        <style>  
                        {{-- cores do Indíce de Clasificação Visual --}}
                        .c-18deg-color { background: #ed5565; }
                        .c-22deg-color { background: #fed862; }
                        .c-24deg-color { background: #67b293; }

                        </style>

                        <div class="ibox">
                            <div class="ibox-content">
            <span class="text-muted small pull-right">
                <a class="btn btn-primary btn-sm" data-toggle="modal" ng-click="novo()" data-target="#modalNovoFuncionario">
                    <i class="fa fa-plus-square"></i> Novo Funcionário
                </a>
            </span>
                                <h2>Funcionários </h2>
                                <div class="table-responsive">
                                    <table class="table table-striped table-hover">
                                        <tbody>
                                            <tr ng-repeat="funcionario in funcionarios">
                                                <td class="client-avatar">
                                                    <img alt="image"
                                                         src="{{ url('/') }}/uploads/avatars/@{{ funcionario.avatar || 'tema_assets/img/a4.jpg' }}">
                                                </td>
                                                <td><label>@{{ funcionario.name }} </label></td>
                                                <td>
                                                    <div class="pull-right">
                                                        <button  class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modalEditalFuncionario" ng-click="editFuncionario(funcionario.id)">
                                                            <i class="fa fa-edit"></i>
                                                        </button>
                                                        <button class="btn btn-danger btn-sm" ng-click="deleteFuncionario(funcionario.id)">
                                                            <i class="fa fa-trash"></i>
                                                        </button>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="ibox">
                            <div class="ibox-content">
                                <h2>Modalidades</h2>
                                <table class="table table-striped table-hover">
                                    <tbody>
                                        <tr ng-repeat="modalidade in modalidades">
                                            <td><label>@{{ modalidade.nmatividade }}</label></td>
                                            <td>
                                                <div class="switch">
                                                    <div class="onoffswitch">
                                                        <input type="checkbox" ng-checked="modalidade.stmenuapp=='S'" checked class="onoffswitch-checkbox" id="@{{ modalidade.id }}" ng-click="statusModal(modalidade.id)">
                                                        <label class="onoffswitch-label" for="@{{ modalidade.id }}">
                                                            <span class="onoffswitch-inner" ></span>
                                                            <span class="onoffswitch-switch"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>





                    </div>
		</div>
		@include('empresas.modal')
	</div>
@endsection
@section('css')
	{!! Html::style("assets_admin/vendor/pnotify/pnotify.custom.css") !!}
	{!! Html::style("assets_admin/vendor/select2/select2.css") !!}
	{!! Html::style("assets_admin/vendor/bootstrap-fileupload/bootstrap-fileupload.min.css") !!}
@endsection
@section('scripts')
	{!! HTML::script("assets_admin/vendor/jquery-form/jquery.form.min.js") !!}
	{!! HTML::script("assets_admin/vendor/jquery-validation/jquery.validate.js") !!}
	{!! HTML::script("assets_admin/javascripts/forms/empresa.validation.js") !!}
	{!! Html::script("assets_admin/vendor/pnotify/pnotify.custom.js") !!}
	{!! Html::script("assets_admin/vendor/jquery-maskedinput/jquery.maskedinput.min.js") !!}

	{!! Html::script("assets_admin/vendor/select2/select2.js") !!}
	{!! Html::script("assets_admin/vendor/bootstrap-fileupload/bootstrap-fileupload.min.js") !!}

	<script type="text/javascript">
		$(document).ready(function() {
			$('#file-input').on('change', function(evt) {
				var file = evt.target.files[0];

				if(file) {
					$('.fantasia').removeAttr("disabled");
					$('.razao_social').removeAttr("disabled");
					$('.cnpj').removeAttr("disabled");
					$('.inscricao_estadual').removeAttr("disabled");
					$('.endereco').removeAttr("disabled");
					$('.numero').removeAttr("disabled");
					$('.cep').removeAttr("disabled");
					$('.bairro').removeAttr("disabled");
					$('.telefone').removeAttr("disabled");
					$('.celular').removeAttr("disabled");
					$('.email').removeAttr("disabled");
					$('.site').removeAttr("disabled");

					// disabled
					$('.fantasia').attr("disabled", "disabled");
					$('.razao_social').attr("disabled", "disabled");
					$('.cnpj').attr("disabled", "disabled");
					$('.inscricao_estadual').attr("disabled", "disabled");
					$('.endereco').attr("disabled", "disabled");
					$('.numero').attr("disabled", "disabled");
					$('.cep').attr("disabled", "disabled");
					$('.bairro').attr("disabled", "disabled");
					$('.telefone').attr("disabled", "disabled");
					$('.celular').attr("disabled", "disabled");
					$('.email').attr("disabled", "disabled");
					$('.site').attr("disabled", "disabled");

					$("#PostFormEmpresaUp").submit();
				}

			});
			
			$('.cpf').mask("999.999.999-99");
			$('.cnpj').mask("99.999.999/9999-99");
			$('.cep').mask("99999-999");

			$('.masktelefone').mask("(99) 99999-9999");
			$('.masktelefonefixo').mask("(99) 9999-9999");

			$('.input-group .dt_nascimento').mask("99/99/9999");

			{{-- $('#data .input-group.date').datepicker({
				todayBtn: "linked",
				keyboardNavigation: false,
				forceParse: false,
				calendarWeeks: true,
				endDate: 'today',
				autoclose: true
			}); --}}
		});
	</script>
@endsection