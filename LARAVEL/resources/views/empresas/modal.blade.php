<div aria-hidden="true" class="modal inmodal fade" id="modalNovaFilial" role="dialog" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" data-dismiss="modal" type="button">
                    <span aria-hidden="true">
                        ×
                    </span>
                    <span class="sr-only">
                        Fechar
                    </span>
                </button>
                <h4 class="modal-title">
                    Nova Filial
                </h4>
            </div>
            <form enctype="multipart/form-data" id="PostFormFilial" method="post" name="PostFormFilial">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>
                                    Nome Fantasia:
                                </label>
                                <input class="form-control" id="fantasia" name="fantasia" ng-model="filial.fantasia" required="" type="text">
                                </input>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>
                                    Razão Social:
                                </label>
                                <input class="form-control" id="razao_social" name="razao_social" ng-model="filial.razao_social" required="" type="text">
                                </input>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>
                                    CNPJ:
                                </label>
                                <input class="form-control cnpj" name="cnpj" ng-model="filial.cnpj" type="text">
                                </input>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>
                                    Inscrição Estadual:
                                </label>
                                <input class="form-control" id="inscricao_estadual" name="inscricao_estadual" ng-model="filial.inscricao_estadual" placeholder="ISENTO" type="text">
                                </input>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>
                                    Endereço:
                                </label>
                                <input class="form-control" id="endereco" name="endereco" ng-model="filial.endereco" type="text">
                                </input>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>
                                    Número:
                                </label>
                                <input class="form-control" id="numero" name="numero" ng-model="filial.numero" type="text">
                                </input>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>
                                    CEP:
                                </label>
                                <input class="form-control cep" name="cep" ng-model="filial.cep" type="text">
                                </input>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>
                                    Bairro:
                                </label>
                                <input class="form-control bairro" name="bairro" ng-model="filial.bairro" type="text">
                                </input>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>
                                    Estado:
                                </label>
                                <select class="form-control" data-placeholder="Selecione um estado..." id="estadoFilial" name="idestado" ng-change="changeEstadoFilial(filial.idestado)" ng-model="filial.idestado" required="" tabindex="2">
                                    <option value="">
                                        Selecione
                                    </option>
                                    <option ng-repeat="estadoFilial in estadosFiliais" ng-value="estadoFilial.id">
                                        @{{estadoFilial.nome}}/@{{estadoFilial.uf}}
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>
                                    Cidade:
                                </label>
                                <select class="form-control cidadeFilial" data-placeholder="Selecione um cidade..." id="idcidade" name="idcidade" ng-model="filial.idcidade" required="" tabindex="2">
                                    <option value="">
                                        Selecione
                                    </option>
                                    <option ng-repeat="cidadeFilial in cidadesFiliais" ng-value="cidadeFilial.id">
                                        @{{cidadeFilial.nome}}
                                    </option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>
                                    Telefone:
                                </label>
                                <input class="form-control masktelefone" name="telefone" ng-model="filial.telefone" type="text">
                                </input>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>
                                    Celular:
                                </label>
                                <input class="form-control masktelefone" name="celular" ng-model="filial.celular" type="text">
                                </input>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>
                                    E-mail:
                                </label>
                                <input class="form-control" id="email" name="email" ng-model="filial.email" type="text">
                                </input>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>
                                    Site:
                                </label>
                                <input class="form-control" id="site" name="site" ng-model="filial.site" type="text">
                                </input>
                            </div>
                        </div>
                    </div>
                    <div class="row loadFilial" style="display:none;">
                        <div class="col-sm-12">
                            <div class="progress progress-striped light active">
                                <div aria-valuemax="100" aria-valuemin="0" aria-valuenow="" class="progress-bar progress-bar-primary" role="progressbar">
                                    <span class="sr-only">
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="form-group">
                        <!--<button type="submit" name="SendPostForm" id="SendPostForm" value="SendPostForm" class="btn btn-primary pull-right"><i class="fa fa-check"></i> Salvar</button>-->
                        <button "="" class="btn btn-primary" ng-click="addFilial(filial)" type="button">
                            <i class="fa fa-check">
                            </i>
                            Salvar
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div aria-hidden="true" class="modal inmodal fade" id="modalEditalFilial" role="dialog" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" data-dismiss="modal" type="button">
                    <span aria-hidden="true">
                        ×
                    </span>
                    <span class="sr-only">
                        Fechar
                    </span>
                </button>
                <h4 class="modal-title">
                    Editar Filial
                </h4>
            </div>
            <form enctype="multipart/form-data" id="PostFormFilialUp" method="post" name="PostFormFilialUp">
                <div class="modal-body">
                    <div style="display:none;">
                        <input name="filial_id" ng-model="filial.id" type="text"/>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>
                                    Nome Fantasia:
                                </label>
                                <input class="form-control" name="fantasia" ng-model="filial.fantasia" required="" type="text">
                                </input>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>
                                    Razão Social:
                                </label>
                                <input class="form-control" name="razao_social" ng-model="filial.razao_social" required="" type="text">
                                </input>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>
                                    CNPJ:
                                </label>
                                <input class="form-control cnpj" name="cnpj" ng-model="filial.cnpj" type="text">
                                </input>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>
                                    Inscrição Estadual:
                                </label>
                                <input class="form-control" name="inscricao_estadual" ng-model="filial.inscricao_estadual" placeholder="ISENTO" type="text">
                                </input>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>
                                    Endereço:
                                </label>
                                <input class="form-control" name="endereco" ng-model="filial.endereco" type="text">
                                </input>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>
                                    Número:
                                </label>
                                <input class="form-control" name="numero" ng-model="filial.numero" type="text">
                                </input>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>
                                    CEP:
                                </label>
                                <input class="form-control cep" name="cep" ng-model="filial.cep" type="text">
                                </input>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>
                                    Bairro:
                                </label>
                                <input class="form-control" name="bairro" ng-model="filial.bairro" type="text">
                                </input>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>
                                    Estado:
                                </label>
                                <select class="form-control" name="idestado" ng-change="changeEstado(empresa.idestado)" ng-model="filial.idestado">
                                    <option value="">
                                        Selecione
                                    </option>
                                    <option ng-repeat="estado in estados" ng-value="estado.id">
                                        @{{estado.nome}}/@{{estado.uf}}
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>
                                    Cidade:
                                </label>
                                <select class="form-control" name="idcidade" ng-model="filial.idcidade">
                                    <option value="">
                                        Selecione
                                    </option>
                                    <option ng-repeat="cidade in cidades" ng-value="cidade.id">
                                        @{{cidade.nome}}
                                    </option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>
                                    Telefone:
                                </label>
                                <input class="form-control masktelefone" name="telefone" ng-model="filial.telefone" type="text">
                                </input>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>
                                    Celular:
                                </label>
                                <input class="form-control masktelefone" name="celular" ng-model="filial.celular" type="text">
                                </input>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>
                                    E-mail:
                                </label>
                                <input class="form-control email" name="email" ng-model="filial.email" type="text">
                                </input>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>
                                    Site:
                                </label>
                                <input class="form-control" name="site" ng-model="filial.site" type="text">
                                </input>
                            </div>
                        </div>
                    </div>
                    <div class="row loadFilialUp" style="display:none;">
                        <div class="col-sm-12">
                            <div class="progress progress-striped light active">
                                <div aria-valuemax="100" aria-valuemin="0" aria-valuenow="" class="progress-bar progress-bar-primary" role="progressbar">
                                    <span class="sr-only">
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="form-group">
                        <!--<button type="submit" name="SendPostForm" id="SendPostForm" value="SendPostForm" class="btn btn-primary pull-right"><i class="fa fa-check"></i> Salvar</button>-->
                        <button class="btn btn-primary" ng-click="updateFilial(filial)" type="button">
                            <i class="fa fa-check">
                            </i>
                            Salvar
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div aria-hidden="true" class="modal inmodal fade" id="modalNovoLocal" role="dialog" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" data-dismiss="modal" type="button">
                    <span aria-hidden="true">
                        ×
                    </span>
                    <span class="sr-only">
                        Fechar
                    </span>
                </button>
                <h4 class="modal-title">
                    Novo Local/Ambiente
                </h4>
            </div>
            <form enctype="multipart/form-data" id="PostFormLocal" method="post" name="PostFormLocal">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>
                                    Nome:
                                </label>
                                <input class="form-control" id="nmlocal" name="nmlocal" ng-model="local.nmlocal" ng-value="" required="" type="text">
                                </input>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>
                                    M²:
                                </label>
                                <input class="form-control" id="metros" name="metros" ng-model="local.metros" ng-value="" type="text">
                                </input>
                            </div>
                        </div>
                    </div>
                    <div class="row load" style="display:none;">
                        <div class="col-sm-12">
                            <div class="progress progress-striped light active">
                                <div aria-valuemax="100" aria-valuemin="0" aria-valuenow="" class="progress-bar progress-bar-primary" role="progressbar">
                                    <span class="sr-only">
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="form-group">
                        <!--<button type="submit" name="SendPostForm" id="SendPostForm" value="SendPostForm" class="btn btn-primary pull-right"><i class="fa fa-check"></i> Salvar</button>-->
                        <button class="btn btn-primary" ng-click="addLocal(local)" type="button">
                            <i class="fa fa-check">
                            </i>
                            Salvar
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div aria-hidden="true" class="modal inmodal fade" id="modalEditalLocal" role="dialog" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" data-dismiss="modal" type="button">
                    <span aria-hidden="true">
                        ×
                    </span>
                    <span class="sr-only">
                        Fechar
                    </span>
                </button>
                <h4 class="modal-title">
                    Editar Local/Ambiente
                </h4>
            </div>
            <form enctype="multipart/form-data" id="PostFormLocalUp" method="post" name="PostFormLocalUp">
                <div class="modal-body">
                    <div style="display:none;">
                        <input id="local_id" name="id" ng-model="local.id" type="text"/>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>
                                    Nome:
                                </label>
                                <input class="form-control" name="nmlocal" ng-model="local.nmlocal" type="text">
                                </input>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>
                                    M²:
                                </label>
                                <input class="form-control" name="metros" ng-model="local.metros" type="text">
                                </input>
                            </div>
                        </div>
                    </div>
                    <div class="row load" style="display:none;">
                        <div class="col-sm-12">
                            <div class="progress progress-striped light active">
                                <div aria-valuemax="100" aria-valuemin="0" aria-valuenow="" class="progress-bar progress-bar-primary" role="progressbar">
                                    <span class="sr-only">
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="form-group">
                        <!--<button type="submit" name="SendPostForm" id="SendPostForm" value="SendPostForm" class="btn btn-primary pull-right"><i class="fa fa-check"></i> Salvar</button>-->
                        <button class="btn btn-primary" ng-click="upLocal(local)" type="button">
                            <i class="fa fa-check">
                            </i>
                            Salvar
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- INICIO NOVO FUNCIONARIO --}}
<div aria-hidden="true" class="modal inmodal fade" id="modalNovoFuncionario" role="dialog" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" data-dismiss="modal" type="button">
                    <span aria-hidden="true">
                        ×
                    </span>
                    <span class="sr-only">
                        Fechar
                    </span>
                </button>
                <h4 class="modal-title">
                    Novo Funcionário 
                </h4>
            </div>
            <form enctype="multipart/form-data" id="PostFormFuncionario" method="post" name="PostFormFuncionario">
                <div class="modal-body">
                    {{-- jassajlkasjkl adr novo --}}
                    <div class="row m-b-lg">
                        <div class="col-lg-4 text-center">
                            <h2 class="textoMaiusculo">
                                @{{ funcionario.name}}
                            </h2>
                            <div class="m-b-sm">
                                <div class="profile-avatar" ng-model="funcionario.avatar" ngf-accept="'image/*'" ngf-max-size="5MB" ngf-pattern="'image/*'" ngf-select="updateAvatar($files, $file, funcionario.user_id)">
                                    <img alt="image" class="img-circle" ngf-size="{width: 80, height: 80, quality: 1}" src="{{ url('/')}}/uploads/avatars/user-a4.jpg" style="width: 80px; height: 80px; margin:0">
                                        <br>
                                            <i aria-hidden="true" class="fa fa-camera">
                                            </i>
                                            <br>
                                            </br>
                                        </br>
                                    </img>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div style="margin-top:20px">
                            </div>
                            <p>
                                Perfil:
                                <b>
                                    @{{funcionario.dados.type ? funcionario.dados.type.description : ''}}
                                </b>
                                <br>
                                    Busca:
                                    <b>
                                        @{{funcionario.dados.type ? funcionario.dados.type.objective : '' }}
                                    </b>
                                    <br>
                                        Motivado por:
                                        <b>
                                            @{{funcionario.dados.type ? funcionario.dados.type.motivation : ''}}
                                        </b>
                                    </br>
                                </br>
                            </p>
                          
                            <a ng-show="funcionario.dados.situacaomatricula !== null">
                                @{{funcionario.dados.situacaomatricula}}
                            </a>
                        </div>
                    </div>
                    {{-- askjslakjsalkjaskljaklsjnovo --}}
                    <div class="tabs-container">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a aria-expanded="true" data-toggle="tab" href="#tab-5">
                                    Informações
                                </a>
                            </li>
                            <li>
                                <a aria-expanded="false" data-toggle="tab" href="#tab-6">
                                    Endereço
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab-5">
                                <br>
                                    {{-- ajsklsajsalk  --}}
                                    <div class="row" style="margin-top: 20px">
                                        <div class="col-xs-12 col-lg-9">
                                            <div class="form-group">
                                                <label>
                                                    Nome:
                                                </label>
                                                <input class="form-control" name="name" ng-model="funcionario.name" type="text">
                                                </input>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-lg-3">
                                            <div class="form-group">
                                                {!! Form::label('Sexo', 'Sexo:') !!}
                                                <select class="form-control" data-placeholder="Selecione o Genero..." name="generox" ng-model="funcionario.genero">
                                                    <option ng-repeat="sex in sexo" ng-value="sex.valor">
                                                        @{{sex.texto}}
                                                    </option>
                                                    <!--<option ng-value="null">Sexo</option>
                                                <option ng-value="M"> M</option>
                                                <option ng-value="F"> F</option>-->
                                                </select>
                                            </div>
                                        </div>
                                        <!--
                                    <div class="col-xs-12 col-lg-3" >
                                        <div class="form-group" >
                                            {!! Form::label('Perfil', 'Perfil:') !!}
                                            <select data-placeholder="Selecione o Perfil..." class="form-control"  id="profile_type" name="profile_type" ng-model="funcionario.dados.profile_type" >
                                                <option value="null">Selecione um perfill</option>
                                                <option ng-selected="'@{{profile.id == funcionario.dados.profile_type}}'" ng-value="@{{ profile.id}}"  ng-repeat="profile in funcionario.profiles">
                                                    @{{ profile.description}}</option>
                                            </select>
                                           
                                        </div>
                                    </div>-->
                                    </div>
                                    {{-- asjksalj --}}
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>
                                                    E-mail:
                                                </label>
                                                <input class="form-control" name="email" ng-model="funcionario.email" type="text">
                                                </input>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>
                                                    Senha:
                                                </label>
                                                <div class="input-group">
                                                    <input class="form-control" name="password" ng-model="funcionario.password" type="password">
                                                        <span class="input-group-addon btn btn-primary bg-primary" ng-click="refreshPassword(funcionario.user_id)" ng-if="funcionario">
                                                            <i aria-hidden="true" class="fa fa-refresh">
                                                            </i>
                                                        </span>
                                                    </input>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>
                                                    Telefone:
                                                </label>
                                                <input class="form-control masktelefone" name="telefone" ng-model="funcionario.telefone" type="text">
                                                </input>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group" id="data">
                                                <label>
                                                    Data de Nascimento:
                                                </label>
                                                <div class="input-group date">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-calendar">
                                                        </i>
                                                    </span>
                                                    <input class="form-control dt_nascimento" name="dt_nascimento" ng-model="funcionario.dt_nascimento" type="text">
                                                    </input>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>
                                                    CPF:
                                                </label>
                                                <input class="form-control cpf" name="cpf" ng-model="funcionario.cpf" type="text">
                                                </input>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>
                                                    Tipo:
                                                </label>
                                                <div class="input-group" style="width:100%;">
                                                    <select class="form-control" data-placeholder="Selecione a função..." id="roles" name="roles" ng-model="funcionario.role">
                                                        <option ng-value="">
                                                            Selecione o tipo
                                                        </option>
                                                        <option ng-repeat="origem in origemVetor" ng-value="origem.valor">
                                                            @{{origem.texto}}
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </br>
                            </div>
                            <div class="tab-pane" id="tab-6">
                                <br>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>
                                                    Endereço:
                                                </label>
                                                <input class="form-control" name="endereco" ng-model="funcionario.endereco" type="text">
                                                </input>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>
                                                    Número:
                                                </label>
                                                <input class="form-control" name="numero" ng-model="funcionario.numero" type="text">
                                                </input>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>
                                                    CEP:
                                                </label>
                                                <input class="form-control cep" name="cep" ng-model="funcionario.cep" type="text">
                                                </input>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>
                                                    Bairro:
                                                </label>
                                                <input class="form-control" name="bairro" ng-model="funcionario.bairro" type="text">
                                                </input>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>
                                                    Estado:
                                                </label>
                                                <select data-placeholder="Selecione um estado..." id="estadoFuncionario" name="idestado" ng-change="changeEstadoFuncionario(funcionario.idestado)" ng-model="funcionario.idestado" tabindex="2">
                                                    <option value="">
                                                        Selecione
                                                    </option>
                                                    <option ng-repeat="estadoFuncionario in estadosFuncionarios" value="@{{estadoFuncionario.id}}">
                                                        @{{estadoFuncionario.nome}}/@{{estadoFuncionario.uf}}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>
                                                    Cidade:
                                                </label>
                                                <select class="cidadeFuncionario" data-placeholder="Selecione um cidade..." name="idcidade" ng-model="funcionario.idcidade" tabindex="2">
                                                    <option value="">
                                                        Selecione
                                                    </option>
                                                    <option ng-repeat="cidadeFuncionario in cidadesFuncionarios" value="@{{cidadeFuncionario.id}}">
                                                        @{{cidadeFuncionario.nome}}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </br>
                            </div>
                        </div>
                    </div>
                    <div class="row load" style="display:none;">
                        <div class="col-sm-12">
                            <div class="progress progress-striped light active">
                                <div aria-valuemax="100" aria-valuemin="0" aria-valuenow="" class="progress-bar progress-bar-primary" role="progressbar">
                                    <span class="sr-only">
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="form-group">
                      
                        <button class="btn btn-primary"  ng-click="addFuncionario(funcionario)" ng-disabled="desabilita==true" >
                            <i class="fa fa-check" type="button"> </i> Salvar
                        </button>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>

<div aria-hidden="true" class="modal inmodal fade" id="modalEditalFuncionario" role="dialog" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" data-dismiss="modal" type="button">
                    <span aria-hidden="true">
                        ×
                    </span>
                    <span class="sr-only">
                        Fechar
                    </span>
                </button>
                <h4 class="modal-title">
                    Editar Funcionário
                </h4>
            </div>
            <form enctype="multipart/form-data" id="PostFormFuncionarioUp" method="post" name="PostFormFuncionarioUp">
                <div class="modal-body">
                    <div style="display:none;">
                        <input id="user_id" name="user_id" ng-model="funcionario.user_id" type="text"/>
                    </div>
                    {{-- jassajlkasjkl adr novo --}}
                    <div class="row m-b-lg">
                        <div class="col-lg-4 text-center">
                            <h2 class="textoMaiusculo">
                                @{{ funcionario.name}}
                            </h2>
                            <div class="m-b-sm">
                                <div class="profile-avatar" ng-model="funcionario.avatar" ngf-accept="'image/*'" ngf-max-size="5MB" ngf-pattern="'image/*'" ngf-select="updateAvatar($files, $file, funcionario.user_id)">
                                    <img alt="image" class="img-circle" ngf-size="{width: 80, height: 80, quality: 1}" src="{{ url('/')}}/uploads/avatars/@{{ funcionario.avatar || '/tema_assets/img/a4.jpg' }}" style="width: 80px; height: 80px; margin:0">
                                        <br>
                                            <i aria-hidden="true" class="fa fa-camera">
                                            </i>
                                            <br>
                                            </br>
                                        </br>
                                    </img>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div style="margin-top:20px">
                            </div>
                            <p>
                                Perfil:
                                <b>
                                    @{{funcionario.dados.type ? funcionario.dados.type.description : ''}}
                                </b>
                                <br>
                                    Busca:
                                    <b>
                                        @{{funcionario.dados.type ? funcionario.dados.type.objective : '' }}
                                    </b>
                                    <br>
                                        Motivado por:
                                        <b>
                                            @{{funcionario.dados.type ? funcionario.dados.type.motivation : ''}}
                                        </b>
                                    </br>
                                </br>
                            </p>
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 " style="padding-right: 0;">
                                    <div class="visible-xs" style="margin-top:5px">
                                    </div>
                                    <a class="btn btn-primary btn-sm btn-block" href="/admin/prescricao/nova/@{{funcionario.user_id}}" id="bt_treino">
                                        <i class="fa fa-edit" style="margin-right:5px;">
                                        </i>
                                        Treino
                                    </a>
                                </div>
                                <div class="col-xs-12 col-sm-4 " style="padding-right: 0;">
                                    <div class="visible-xs" style="margin-top:5px">
                                    </div>
                                    <a class="btn btn-primary btn-sm btn-block" href="/admin/funcionarioes/chat" id="bt_treino">
                                        <i class="fa fa-comment" style="margin-right:5px;">
                                        </i>
                                        Chat
                                    </a>
                                </div>
                                <div class="col-xs-12 col-sm-4 " style="padding-right: 0;">
                                    <div class="visible-xs" style="margin-top:5px">
                                    </div>
                                    <a class="btn btn-primary btn-sm btn-block" href="/admin/agendamento" id="bt_treino">
                                        <i class="fa fa-calendar" style="margin-right:5px;">
                                        </i>
                                        Avaliação
                                    </a>
                                </div>
                            </div>
                            <a ng-show="funcionario.dados.situacaomatricula !== null">
                                @{{funcionario.dados.situacaomatricula}}
                            </a>
                        </div>
                    </div>
                    {{-- askjslakjsalkjaskljaklsjnovo --}}
                    <div class="tabs-container">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a aria-expanded="true" data-toggle="tab" href="#tab-7">
                                    Informações
                                </a>
                            </li>
                            <li>
                                <a aria-expanded="false" data-toggle="tab" href="#tab-8">
                                    Endereço
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab-7">
                                <br>
                                    {{-- ajsklsajsalk  --}}
                                    <div class="row" style="margin-top: 20px">
                                        <div class="col-xs-12 col-lg-9">
                                            <div class="form-group">
                                                <label>
                                                    Nome:
                                                </label>
                                                <input class="form-control" name="name" ng-model="funcionario.name" type="text">
                                                </input>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-lg-3">
                                            <div class="form-group">
                                                {!! Form::label('Sexo', 'Sexo:') !!}
                                                <select class="form-control" data-placeholder="Selecione o Genero..." name="genero" ng-model="funcionario.genero">
                                                    <option ng-repeat="sex in sexo" ng-value="sex.valor">
                                                        @{{sex.texto}}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    {{-- asjksalj --}}
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>
                                                    E-mail:
                                                </label>
                                                <input class="form-control" name="email" ng-model="funcionario.email" type="text">
                                                </input>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>
                                                    Senha:
                                                </label>
                                                <div class="input-group">
                                                    <input class="form-control" name="password" ng-model="funcionario.password" type="password">
                                                        <span class="input-group-addon btn btn-primary bg-primary" ng-click="refreshPassword(funcionario.user_id)" ng-if="funcionario">
                                                            <i aria-hidden="true" class="fa fa-refresh">
                                                            </i>
                                                        </span>
                                                    </input>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>
                                                    Telefone:
                                                </label>
                                                <input class="form-control masktelefone" name="telefone" ng-model="funcionario.telefone" type="text">
                                                </input>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group" id="data">
                                                <label>
                                                    Data de Nascimento:
                                                </label>
                                                <div class="input-group date">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-calendar">
                                                        </i>
                                                    </span>
                                                    <input class="form-control dt_nascimento" name="dt_nascimento" ng-model="funcionario.dt_nascimento" type="text">
                                                    </input>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>
                                                    CPF:
                                                </label>
                                                <input class="form-control cpf" name="cpf5" ng-model="funcionario.cpf" type="text">
                                                </input>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>
                                                    Tipo:
                                                </label>
                                                <div class="input-group" style="width:100%;">
                                                    <select class="form-control" data-placeholder="Selecione o tipo..." name="role" ng-model="funcionario.role">
                                                        <option disabled="" selected="" value="">
                                                            Selecione o tipo
                                                        </option>
                                                        <option ng-repeat="origem in origemVetor" ng-value="origem.valor">
                                                            @{{origem.texto}}
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </br>
                            </div>
                            <div class="tab-pane" id="tab-8">
                                <br>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>
                                                    Endereço:
                                                </label>
                                                <input class="form-control" name="endereco" ng-model="funcionario.endereco" type="text">
                                                </input>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>
                                                    Número:
                                                </label>
                                                <input class="form-control" name="numero" ng-model="funcionario.numero" type="text">
                                                </input>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>
                                                    CEP:
                                                </label>
                        <input class="form-control cep" name="cep" ng-model="funcionario.cep" ng-change="getCepFuncionario(funcionario.cep)" type="text">
                        </input>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>
                                                    Bairro:
                                                </label>
                                                <input class="form-control" name="bairro" ng-model="funcionario.bairro" type="text">
                                                </input>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>
                                                    Estado:
                                                </label>
                                                <select class="form-control" data-placeholder="Selecione um estado..." id="estadoFuncionario" name="idestado" ng-change="changeEstadoFuncionario(funcionario.idestado)" ng-model="funcionario.idestado" tabindex="2">
                                                    <option value="">
                                                        Selecione
                                                    </option>
                                                    <option ng-repeat="estadoFuncionario in estadosFuncionarios" ng-value="estadoFuncionario.id">
                                                        @{{estadoFuncionario.nome}}/@{{estadoFuncionario.uf}}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>
                                                    Cidade:
                                                </label>
                                                <select class="form-control cidadeFuncionario" data-placeholder="Selecione um cidade..." name="idcidade" ng-model="funcionario.idcidade" tabindex="2">
                                                    <option value="">
                                                        Selecione
                                                    </option>
                                                    <option ng-repeat="cidadeFuncionario in cidadesFuncionarios" ng-value="cidadeFuncionario.id">
                                                        @{{cidadeFuncionario.nome}}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </br>
                            </div>
                        </div>
                    </div>
                    <div class="row load" style="display:none;">
                        <div class="col-sm-12">
                            <div class="progress progress-striped light active">
                                <div aria-valuemax="100" aria-valuemin="0" aria-valuenow="" class="progress-bar progress-bar-primary" role="progressbar">
                                    <span class="sr-only">
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="form-group">
                        <!--<button type="submit" name="SendPostForm" id="SendPostForm" value="SendPostForm" class="btn btn-primary pull-right"><i class="fa fa-check"></i> Salvar</button>-->
                        <button class="btn btn-primary" ng-click="upFuncionario(funcionario)" type="button">
                            <i class="fa fa-check">
                            </i>
                            Salvar
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- INICIO NOVO Coach --}}
<div aria-hidden="true" class="modal inmodal fade" id="modalNovoNutricionista" role="dialog" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" data-dismiss="modal" type="button">
                    <span aria-hidden="true">
                        ×
                    </span>
                    <span class="sr-only">
                        Fechar
                    </span>
                </button>
                <h4 class="modal-title">
                    Novo Coach
                </h4>
            </div>
            <form enctype="multipart/form-data" id="PostFormNutricionista" method="post" name="PostFormNutricionista">
                <div class="modal-body">
                    <div class="tabs-container">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a aria-expanded="true" data-toggle="tab" href="#tab-1">
                                    Informações
                                </a>
                            </li>
                            <li>
                                <a aria-expanded="false" data-toggle="tab" href="#tab-2">
                                    Endereço
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab-1">
                                <br>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>
                                                    Tipo:
                                                </label>
                                                <input class="form-control" id="tipo" name="tipo" ng-model="funcionario.tipo" placeholder="Ex: Nutricionista" type="text">
                                                </input>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>
                                                    Nome do Coach:
                                                </label>
                                                <input class="form-control" id="name" name="name" ng-model="funcionario.name" type="text">
                                                </input>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>
                                                    E-mail:
                                                </label>
                                                <input class="form-control" id="email" name="email" ng-model="funcionario.email" type="text">
                                                </input>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>
                                                    Senha:
                                                </label>
                                                <div class="input-group">
                                                    <input class="form-control" name="password" ng-model="funcionario.password" type="password">
                                                        <span class="input-group-addon btn btn-primary bg-primary" ng-click="refreshPassword(funcionario.user_id)" ng-if="funcionario">
                                                            <i aria-hidden="true" class="fa fa-refresh">
                                                            </i>
                                                        </span>
                                                    </input>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>
                                                    Telefone:
                                                </label>
                                                <input class="form-control masktelefone" name="telefone" ng-model="funcionario.telefone" type="text">
                                                </input>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group" id="data">
                                                <label>
                                                    Data de Nascimento:
                                                </label>
                                                <div class="input-group date">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-calendar">
                                                        </i>
                                                    </span>
                                                    <input class="form-control dt_nascimento" name="dt_nascimento" ng-model="funcionario.dt_nascimento" type="text">
                                                    </input>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>
                                                    CPF:
                                                </label>
                                                <input class="form-control cpf" name="cpf" ng-model="funcionario.cpf" type="text">
                                                </input>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label>
                                                    Site:
                                                </label>
                                                <input class="form-control site" name="site" ng-model="funcionario.site" type="text">
                                                </input>
                                            </div>
                                        </div>
                                    </div>
                                </br>
                            </div>
                            <div class="tab-pane" id="tab-2">
                                <br>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>
                                                    Endereço:
                                                </label>
                                                <input class="form-control" name="endereco" ng-model="funcionario.endereco" type="text">
                                                </input>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>
                                                    Número:
                                                </label>
                                                <input class="form-control" name="numero" ng-model="funcionario.numero" type="text">
                                                </input>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>
                                                    CEP:
                                                </label>
                                                <input class="form-control cep" name="cep" ng-model="funcionario.cep" type="text">
                                                </input>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>
                                                    Bairro:
                                                </label>
                                                <input class="form-control" name="bairro" ng-model="funcionario.bairro" type="text">
                                                </input>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>
                                                    Estado:
                                                </label>
                                                <select class="estadoFuncionario" data-placeholder="Selecione um estado..." name="idestado" ng-change="changeEstadoFuncionario(funcionario.idestado)" ng-model="funcionario.idestado" tabindex="2">
                                                    <option value="">
                                                        Selecione
                                                    </option>
                                                    <option ng-repeat="estadoFuncionario in estadosFuncionarios" value="@{{estadoFuncionario.id}}">
                                                        @{{estadoFuncionario.nome}}/@{{estadoFuncionario.uf}}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>
                                                    Cidade:
                                                </label>
                                                <select class="cidadeFuncionario" data-placeholder="Selecione um cidade..." name="idcidade" ng-model="funcionario.idcidade" tabindex="2">
                                                    <option value="">
                                                        Selecione
                                                    </option>
                                                    <option ng-repeat="cidadeFuncionario in cidadesFuncionarios" value="@{{cidadeFuncionario.id}}">
                                                        @{{cidadeFuncionario.nome}}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </br>
                            </div>
                        </div>
                    </div>
                    <div class="row load" style="display:none;">
                        <div class="col-sm-12">
                            <div class="progress progress-striped light active">
                                <div aria-valuemax="100" aria-valuemin="0" aria-valuenow="" class="progress-bar progress-bar-primary" role="progressbar">
                                    <span class="sr-only">
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="form-group">
                        <!--<button type="submit" name="SendPostForm" id="SendPostForm" value="SendPostForm" class="btn btn-primary pull-right"><i class="fa fa-check"></i> Salvar</button>-->
                        <button class="btn btn-primary" ng-click="addNutricionista(funcionario)" type="button">
                            <i class="fa fa-check">
                            </i>
                            Salvar
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div aria-hidden="true" class="modal inmodal fade" id="modalEditalNutricionista" role="dialog" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" data-dismiss="modal" type="button">
                    <span aria-hidden="true">
                        ×
                    </span>
                    <span class="sr-only">
                        Fechar
                    </span>
                </button>
                <h4 class="modal-title">
                    Editar Coach
                </h4>
            </div>
            <form enctype="multipart/form-data" id="PostFormNutricionistaUp" method="post" name="PostFormNutricionistaUp">
                <div class="modal-body">
                    <div style="display:none;">
                        <input id="user_id" name="user_id" ng-model="funcionario.user_id" type="text"/>
                    </div>
                    <div class="tabs-container">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a aria-expanded="true" data-toggle="tab" href="#tab-3">
                                    Informações
                                </a>
                            </li>
                            <li>
                                <a aria-expanded="false" data-toggle="tab" href="#tab-4">
                                    Endereço
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab-3">
                                <br>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>
                                                    Tipo:
                                                </label>
                                                <input class="form-control" id="tipo" name="tipo" ng-model="funcionario.tipo" placeholder="Ex: Nutricionista" type="text">
                                                </input>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>
                                                    Nome:
                                                </label>
                                                <input class="form-control" name="name" ng-model="funcionario.name" type="text">
                                                </input>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>
                                                    E-mail:
                                                </label>
                                                <input class="form-control" name="email" ng-model="funcionario.email" type="text">
                                                </input>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>
                                                    Senha:
                                                </label>
                                                <div class="input-group">
                                                    <input class="form-control" name="password" ng-model="funcionario.password" type="password">
                                                        <span class="input-group-addon btn btn-primary bg-primary" ng-click="refreshPassword(funcionario.user_id)" ng-if="funcionario">
                                                            <i aria-hidden="true" class="fa fa-refresh">
                                                            </i>
                                                        </span>
                                                    </input>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>
                                                    Telefone:
                                                </label>
                                                <input class="form-control masktelefone" name="telefone" ng-model="funcionario.telefone" type="text">
                                                </input>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group" id="data">
                                                <label>
                                                    Data de Nascimento:
                                                </label>
                                                <div class="input-group date">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-calendar">
                                                        </i>
                                                    </span>
                                                    <input class="form-control dt_nascimento" name="dt_nascimento" ng-model="funcionario.dt_nascimento" type="text">
                                                    </input>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>
                                                    CPF:
                                                </label>
                                                <input class="form-control cpf" name="cpf" ng-model="funcionario.cpf" type="text">
                                                </input>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label>
                                                    Site:
                                                </label>
                                                <input class="form-control site" id="site" name="site" ng-model="funcionario.site" type="text">
                                                </input>
                                            </div>
                                        </div>
                                    </div>
                                </br>
                            </div>
                            <div class="tab-pane" id="tab-4">
                                <br>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>
                                                    Endereço:
                                                </label>
                                                <input class="form-control" name="endereco" ng-model="funcionario.endereco" type="text">
                                                </input>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>
                                                    Número:
                                                </label>
                                                <input class="form-control" name="numero" ng-model="funcionario.numero" type="text">
                                                </input>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>
                                                    CEP:
                                                </label>
                                                <input class="form-control cep" id="" name="cep" ng-model="funcionario.cep" type="text">
                                                </input>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>
                                                    Bairro:
                                                </label>
                                                <input class="form-control" name="bairro" ng-model="funcionario.bairro" type="text">
                                                </input>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>
                                                    Estado:
                                                </label>
                                                <select class="idestado" data-placeholder="Selecione um estado..." name="idestado" ng-change="changeEstadoFuncionario(funcionario.idestado)" ng-model="funcionario.idestado" tabindex="2">
                                                    <option value="">
                                                        Selecione
                                                    </option>
                                                    <option ng-repeat="estadoFuncionario in estadosFuncionarios" ng-value="estadoFuncionario.id">
                                                        @{{estadoFuncionario.nome}}/@{{estadoFuncionario.uf}}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>
                                                    Cidade:
                                                </label>
                                                <select class="idcidade" data-placeholder="Selecione um cidade..." name="idcidade" ng-model="funcionario.idcidade" tabindex="2">
                                                    <option value="">
                                                        Selecione
                                                    </option>
                                                    <option ng-repeat="cidadeFuncionario in cidadesFuncionarios" ng-value="cidadeFuncionario.id">
                                                        @{{cidadeFuncionario.nome}}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </br>
                            </div>
                        </div>
                    </div>
                    <div class="row load" style="display:none;">
                        <div class="col-sm-12">
                            <div class="progress progress-striped light active">
                                <div aria-valuemax="100" aria-valuemin="0" aria-valuenow="" class="progress-bar progress-bar-primary" role="progressbar">
                                    <span class="sr-only">
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="form-group">
                        <!--<button type="submit" name="SendPostForm" id="SendPostForm" value="SendPostForm" class="btn btn-primary pull-right"><i class="fa fa-check"></i> Salvar</button>-->
                        <button class="btn btn-primary" ng-click="upNutricionista(funcionario)" type="button">
                            <i class="fa fa-check">
                            </i>
                            Salvar
                        </button>
                    </div>
                </div>
            </form>
            {{-- askjdhakjshjkd estava aqui--}}
        </div>
    </div>
</div>
