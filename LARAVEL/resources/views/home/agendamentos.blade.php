<div class="ibox">
    <div class="ibox-title">
        <h5>Agendamentos</h5>
        <div class="ibox-tools">
            <a class="collapse-link">
                <i class="fa fa-chevron-up"></i>
            </a>
            <a class="close-link">
                <i class="fa fa-times"></i>
            </a>
        </div>
    </div>
    <div class="ibox-content">



        <div style="margin-top: 20px"></div>

        <ul class="nav nav-tabs">
            <li class="active" ng-show="true">
                <a data-toggle="tab" href="#tab-8">
                    Pendente
                </a>
            </li>

            <li ng-show="true">
                <a data-toggle="tab" href="#tab-9">
                    Realizado
                </a>
            </li>

            <li ng-show="true">
                <a data-toggle="tab" href="#tab-10">
                    Hoje
                </a>
            </li>
        </ul>

        <div class="tab-content">
            <!-- INICIO DA ABA 1 -->
            <div class="tab-pane active" id="tab-8" ng-show="true">


                {{-- FIM div professores --}}
                <div style="width: 98%; overflow-x: scroll;">
                    <div style="  min-width: 500px; ">
                        <table class="table table-hover no-margins" style="width:100%">
                            <thead style="width:100%">
                                <tr style="width:100%">
                                    <th style="width: 25%">
                                        Clientes
                                    </th>
                                    <th style="width: 25%">
                                        Professor
                                    </th>
                                    <th style="width: 15%">
                                        Data
                                    </th>
                                    <th style="width: 10%">
                                        Hora
                                    </th>
                                    <th style="width: 15%">
                                        Agenda
                                    </th>
                                    <th style="width: 10%">
                                        Dias
                                    </th>
                                </tr>
                            </thead>
                        </table>
                        <div style=" height: 285px; overflow-y: scroll;  width:100% ">
                            <table class="table table-hover no-margins" style=" width:100% ">
                                <tbody>
                                    <tr ng-repeat="agenda in agendas| filter:{stagenda:'P'} | orderBy:- dtagenda" style="width:100%">
                                        <td style="width: 25%">
                                            <a href="@{{agenda.url}}">@{{agenda.name}}</a>
                                        </td>
                                        <td style="width: 25%">
                                            @{{agenda.nomeprof}}
                                        </td>
                                        <td style="width: 15%">
                                            @{{agenda.dtagenda}}
                                        </td>
                                        <td style="width: 10%">
                                            @{{agenda.hragenda}}
                                        </td>
                                        <td style="width: 15%">
                                            @{{agenda.nmagenda}}
                                        </td>
                                        <td style="width: 10%" ng-show="agenda.dtf >= 0" class="text-success">@{{agenda.dtf}}</td>
                                        <td style="width: 10%" ng-show="agenda.dtf < 0" class="text-danger">@{{agenda.dtf}}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                {{-- askdjklasjdaslkj --}}





            </div>




            <div class="tab-pane" id="tab-9" ng-show="true">


                {{-- FIM div professores --}}
                <div style="width: 98%; overflow-x: scroll;">
                    <div style="  min-width: 500px; ">
                        <table class="table table-hover no-margins" style="width:100%">
                            <thead style="width:100%">
                                <tr style="width:100%">
                                    <th style="width: 25%">
                                        Clientes
                                    </th>
                                    <th style="width: 25%">
                                        Professor
                                    </th>
                                    <th style="width: 15%">
                                        Data
                                    </th>
                                    <th style="width: 15%">
                                        Hora
                                    </th>
                                    <th style="width: 20%">
                                        Agenda
                                    </th>
                                </tr>
                            </thead>
                        </table>
                        <div style=" height: 285px; overflow-y: scroll;  width:100% ">
                            <table class="table table-hover no-margins" style=" width:100% ">
                                <tbody>
                                    <tr ng-repeat="agenda in agendas| filter:{stagenda:'R'} | orderBy:'realizado_d'" style="width:100%">
                                        <td style="width: 25%">
                                            <a href="@{{agenda.url}}">@{{agenda.name}}</a>
                                        </td>
                                        <td style="width: 25%">
                                            @{{agenda.nomeprof}}
                                        </td>
                                        <td style="width: 15%">
                                            @{{agenda.realizado_d}}
                                        </td>
                                        <td style="width: 15%">
                                            @{{agenda.realizado_h}}
                                        </td>
                                        <td style="width: 20%">
                                            @{{agenda.nmagenda}}
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                {{-- askdjklasjdaslkj --}}

            </div>
<!-- fim tab -->
<div class="tab-pane" id="tab-10" ng-show="true">


    {{-- FIM div professores --}}
    <div style="width: 98%; overflow-x: scroll;">
        <div style="  min-width: 500px; ">
            <table class="table table-hover no-margins" style="width:100%">
                <thead style="width:100%">
                    <tr style="width:100%">
                        <th style="width: 25%">
                            Clientes
                        </th>
                        <th style="width: 25%">
                            Professor
                        </th>
                        <th style="width: 15%">
                            Data
                        </th>
                        <th style="width: 15%">
                            Hora
                        </th>
                        <th style="width: 20%">
                            Agenda
                        </th>
                    </tr>
                </thead>
            </table>
            <div style=" height: 285px; overflow-y: scroll;  width:100% ">
                <table class="table table-hover no-margins" style=" width:100% ">
                    <tbody>
                        <tr ng-repeat="agenda in agendas| filter:{stagenda:'R'} | orderBy:'realizado_d'" style="width:100%">
                            <td style="width: 25%">
                                <a href="@{{agenda.url}}">@{{agenda.name}}</a>
                            </td>
                            <td style="width: 25%">
                                @{{agenda.nomeprof}}
                            </td>
                            <td style="width: 15%">
                                @{{agenda.realizado_d}}
                            </td>
                            <td style="width: 15%">
                                @{{agenda.realizado_h}}
                            </td>
                            <td style="width: 20%">
                                @{{agenda.nmagenda}}
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    {{-- askdjklasjdaslkj --}}

</div>
<!-- fim tab -->


        </div>
    </div>
</div>
<!-- fim de ibox -->