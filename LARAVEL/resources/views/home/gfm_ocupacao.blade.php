 <div class="ibox">

    <div class="ibox-title">
        <h5>GFM - Taxa de Ocupação</h5>
        <span class="text-muted small pull-right tooltip-demo text-center"">
            <i class="fa fa-info-circle" 

               data-toggle="popover" data-placement="auto top" 
               data-content="Acompanhe aqui a Taxa de ocupação do GFM Geral e por Professor"

               aria-hidden="true" style="    font-size: 18px;
               margin-left: 10px;"></i>
        </span>
        <span class="label label-primary pull-right"><font>Últimos 7 dias</font></span>
    </div>
    <div class="ibox-content" style="overflow-y: scroll; height: 300px">

        {{-- < h4 > Taxa de Ocupação da Academia < /h4> --}}
        <h2>@{{ taxaOcupacao.media}}%</h2>
        <div class="progress progress-mini">
            <div style="width:@{{ taxaOcupacao.media}}%;" class="progress-bar"></div>
        </div>
        <div class="m-t-sm small">@{{ taxaOcupacao.totalReservas}}/@{{ taxaOcupacao.totalCapacidade}} Audiências</div>
        <hr>


        {{--  --}}
        <ul class="list-group clear-list m-t">
            <li class="list-group-item fist-item" ng-repeat="ocupacao in ocupacaoProfs| orderBy : 'perc' : true">
                <span class="pull-right" style="margin-right: 10px">@{{ ocupacao.perc}}%</span>
                <span style="margin-right: 10px;" class="label @{{ ($index == 0) ? 'label-primary' : 'label-default'}}">@{{ $index + 1}}</span> @{{ ocupacao.name}}
            </li>
        </ul>
    </div>
</div>
<!-- fim de bloco  ibox-->