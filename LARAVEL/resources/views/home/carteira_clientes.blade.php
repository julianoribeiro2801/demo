 <div class="ibox"  >
            <div class="ibox-title">
                <h5>Carteira de Clientes por Professor </h5>
            </div>

            <div class="ibox-content carteira_clientes" >      

                {{-- CARTIRA DE CLIENTE A PARTE DA TURMA --}} 
                <table class="table table-hover no-margins prof">
                    <thead >
                        <tr>
                            <th style="width:70%">Professor</th>
                            <th style="width:10%">Clientes</th>
                             <th style="width:20%">Freq. Méd. Sem.</th>
                        </tr>
                    </thead>
                </table>

        <div class="rolagem_carteira" >
            <table class="table table-hover no-margins"  style="width:100%">
                <tbody class="turmas"  >

                    <tr ng-repeat="professor in professores" >
                        <td colspan="2" >

    <table style="width:100%">
        <tr>
            <td style="width:80%">

        <div class="avt_prof" ng-if="professor.id > 0"
            style="background-image: url({{ url('/') }}/uploads/avatars/@{{ professor.avatar || 'user-a4.jpg' }});">
         </div>
        
         <div class="avt_prof" ng-if="professor.id==0">
            <i class="fa fa-exclamation-circle" style="font-size: 36px; color: #ed5564;" aria-hidden="true"></i>
             </div>

                <a  ng-click="getTurmasProf(professor.id)">
                    <div style="margin-top: 10px">
                     @{{ professor.name}}
                 </div>
            </a>
     </td>
            <td style="width:10%">

                @{{ professor.total}}
            </td>

            <td style="width:20%">
                2.3
            </td>

        </tr>
    </table>

                                    <div style="margin-top:10px"></div>

                                    <table ng-if="turmasel == professor.id" class="table no-margins trocar_carteira"
                                           style="border: 1px solid #e8eaec; display:none">
                                        <thead>
                                            <tr>
                                                <td>Cliente</td>
                                                <td>Trocar carteira</td>
                                                <td></td>
                                            </tr>
                                        </thead>
                                        <tbody class="alunos">
                                            <tr ng-repeat="turma in turmas">
                                                <td><a href="@{{turma.url}}">@{{turma.name}}</a> </td>
                                                <td ng-show="edita == true">
                                                    <select ng-model="turma.professor_id" data-placeholder="Selecione um professor.." class="form-control tdata_mob">
                                                        <option ng-repeat="professor in professores" ng-value="professor.id">
                                                            @{{professor.name}} 
                                                        </option>
                                                    </select>
                                                </td>
                                                <td ng-show="edita == false">
                                                    @{{turma.nomeprof}}
                                                </td>
                                                <td ng-show="edita == false">
                                                    <i class="fa fa-edit" aria-hidden="true" ng-click="edita = true"></i>
                                                </td>
                                                <td ng-show="edita == true">
                                                    <i class="fa fa-save" aria-hidden="true" ng-click="upTurma(turma.aluno_id, prof_anterior, turma.professor_id)"></i>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>


                        </tbody>
                    </table>
                </div>



            </div>
        </div>