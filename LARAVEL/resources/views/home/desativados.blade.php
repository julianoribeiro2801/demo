<div class="ibox">
    <div class="ibox-title">
        <h5>
            Desativados / Rotatividade
        </h5> 
        <span class="label label-primary pull-right">
                    <font>últimos 30 dias
                </font></span>
    </div>
    <div class="ibox-content" >
      
         <div style="width: 98%; overflow-x: scroll;">
             <table> <!-- ng-init="profes=['Controllers','Models','Filters','Filters3','Filters4','Filters5','Filter6','Filter7','Filter8','Filter9','Filter10']">-->
            <tr>
              <td ng-repeat="rot in profs_rotatividade">

                  <div class="text-center" style="margin-right: 25px">
                    <div class="verprof">
                        <img alt="" src="https://d3iw72m71ie81c.cloudfront.net/female-17.jpg">
                            <br>
                                <div class="indicador_prof" style="background: rgba(237, 85, 100, 0.71);">
                                   <span> Rotatividade </span> <br> @{{rot.percentual}}%
                                </div>
                            </br>
                        </img>
                    </div>
                    @{{rot.name}}
                </div>


              </td>

          </table>

        </div>
          
 <div style="margin-top: 20px"></div>
  <div style=" height: 356px; overflow-y: hidden ; overflow-x: auto;">
    <div style="  min-width: 500px; ">
    
           


        {{-- FIM div professores --}}
        <table class="table table-hover no-margins" style="width:100%">
            <thead style="width:100%">
                <tr style="width:100%">
                    <th style="width: 30%">
                        Clientes Desativados
                    </th>
                    <th style="width: 15%">
                        Data
                    </th>
                    <th style="width: 15%">
                        Professor
                    </th>
                    <th style="width: 30%">
                        Motivo
                    </th>
                </tr>
            </thead>
        </table>
        <div style=" height: 285px; overflow-y: scroll;  width:100% ">
            <table class="table table-hover no-margins" style=" width:100% ">
                <tbody>
                    <tr ng-repeat="desativado in desativados" style="width:100%">
                        <td style="width: 30%">
                             <a>
                                @{{ desativado.name}}
                            </a>
                        </td>
                        <td style="width:15%">
                              @{{ desativado.dtalteracao }}
                        </td>
                        <td class="text-danger" style="width: 15%">
                            @{{ desativado.professor }}
                        </td>
                         <td class="text-danger" style="width: 30%">
                            @{{ desativado.motivo }}
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>


        




            {{-- FIM div professores --}}
            
      


            </div>
        </div>
    </div>
</div>