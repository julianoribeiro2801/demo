<div class="ibox">
    <div class="ibox-title">
        <h5>
            Projetos / Etapas
        </h5>
        <div class="ibox-tools">
            <a class="collapse-link">
                <i class="fa fa-chevron-up">
                </i>
            </a>
            <a class="close-link">
                <i class="fa fa-times">
                </i>
            </a>
        </div>
    </div>
    <div class="ibox-content">

 {{-- inicio div professores --}}
          <div style="width: 98%; overflow-x: scroll;">
             <table> <!-- ng-init="profes=['Controllers','Models','Filters','Filters3','Filters4','Filters5','Filter6','Filter7','Filter8','Filter9','Filter10']">-->
            <tr>
              <td ng-repeat="prof in profs_indicador">

                  <div class="text-center" style="margin-right: 25px">
                    <div class="verprof">
                        {{-- <img alt="" src="https://d3iw72m71ie81c.cloudfront.net/female-17.jpg"> --}}
                        <img alt="" src="/uploads/avatars/@{{prof.avatar}}">

                            <br>
                                <div class="indicador_prof" style="background: rgba(237, 85, 100, 0.71);">
                                   <span>
                                        Atrasados
                                    </span>
                                    <br>
                                      @{{prof.ttatraso}}
                                </div>
                            </br>
                        </img>
                    </div>
                    @{{prof.name}}
                </div>


              </td>

          </table>

        </div>



        <div style="margin-top: 20px">
        </div>


        {{-- FIM div professores --}}
        <table class="table table-hover no-margins" style="width:100%">
            <thead style="width:100%">
                <tr style="width:100%">
                    <th style="width: 30%">
                        Cliente
                    </th>
                    <th style="width: 30%">
                        Projeto
                    </th>
                    <th style="width: 15%">
                        Fim
                    </th>
                    <th style="width: 15%">
                        Dias
                    </th>
                </tr>
            </thead>
        </table>
        <div style=" height: 265px; overflow-y: scroll;  width:100% ">
            <table class="table table-hover no-margins" style=" width:100% ">
                <tbody>
                    <tr ng-repeat="projeto in projetos_etapas" style="width:100%">
                        <td style="width: 30%">
                            <a href="@{{projeto.url}}">
                                @{{projeto.name}}
                            </a>
                        </td>
                        <td style="width:30%">
                            @{{projeto.projeto_nome}}
                        </td>
                        <td class="text-danger" style="width: 15%">
                            @{{projeto.projeto_data}}
                        </td>
                        <td class="text-success" ng-show="projeto.dia_atraso >= 0" style="width: 15%">
                            @{{projeto.dia_atraso}}
                        </td>
                        <td class="text-danger" ng-show="projeto.dia_atraso < 0" style="width: 15%">
                            @{{projeto.dia_atraso}}
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>


        
    </div>
</div>
<!-- fim de ibox -->
