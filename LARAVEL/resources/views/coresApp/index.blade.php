@extends('layouts.app')
@section('content')
<style>
    .bg-roxo{
        background-color: #443752;
        border: 0px solid;
        pointer-events: none;
    }
    .bg-roxo:hover{
        background-color: #443752;
        border: 0px solid;
        pointer-events: none;
    }
    .mt-30{
        margin-top: 30px;
    }
    .mb-30{
        margin-bottom: 30px;
    }

    .alinha_mudar {
        position:relative; display:inline-block
    }
    .btcolor_oculta {
        opacity:0; position:absolute; left:0;top:0;width:100%
    }
</style>

<!-- icomoon -->
{!! Html::style("coresApp/icomoon/style.css") !!}

{!! Html::style("tema_assets/css/plugins/colorpicker/bootstrap-colorpicker.min.css") !!}

{!! Html::style("coresApp/css/geral.css") !!}

<script>
    var tela_atual = '{{$tela}}';
</script>
@push('scripts')
{!! Html::script("js/temasController.js") !!}
<!-- Color picker -->
{!! Html::script("tema_assets/js/plugins/colorpicker/bootstrap-colorpicker.min.js") !!}
{!! Html::script("tema_assets/js/plugins/clockpicker/clockpicker.js") !!}

@endpush



{{-- inicio topo fake wizard --}}
@if(Auth::user()->role=='admin'  && Auth::user()->passo < 3)

{!! Html::style("tema_assets/css/plugins/steps/jquery.steps.css") !!}
<div class="wrapper wrapper-content">
    <div class="ibox-content">
        <h2>
            Personalizar meu Aplicativo!
        </h2>
        <p>
            Complete seus dados para iniciar o teste com o aplicativo. 
        </p>
        <div class="wizard">
            <div class="steps clearfix">
                <ul role="tablist">
                    <li aria-disabled="false" aria-selected="false" class="first done" role="tab">
                        <a aria-controls="form-p-0" href="#form-h-0" id="form-t-0">
                            <span class="number">
                                1.
                            </span>
                            Meus Dados
                        </a>
                    </li>
                    <li aria-disabled="false" aria-selected="true" class="current" role="tab">
                        <a aria-controls="form-p-1" href="#form-h-1" id="form-t-1">
                            <span class="current-info audible">
                                current step:
                            </span>
                            <span class="number">
                                2.
                            </span>
                            Personaliar App
                        </a>
                    </li>
                    <li aria-disabled="true" class="disabled last" role="tab">
                        <a aria-controls="form-p-2" href="#form-h-2" id="form-t-2">
                            <span class="number">
                                3.
                            </span>
                            Parabéns!
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        {{--  inicio do confirme seus dados --}}
        <div class="row" style="margin-top:80px">
            <div class="col-md-6">
            </div>
            <div class="col-md-3">
                <a class="btn btn-lg btn-primary btn-block btn-pr" href="/admin/passos">
                    <i class="fa fa-chevron-left">
                    </i>
                    Anterior
                </a>
            </div>
            <div class="col-md-3">
                <div style="margin-top:10px" class="visible-xs"></div>
                <a class="btn btn-lg btn-primary btn-block btn-pr" href="/admin/passos/parabens">
                    Próximo
                    <i class="fa fa-chevron-right">
                    </i>
                </a>
            </div>
        </div>
        {{--  fim do confirme seus dados --}}
    </div>
</div>


<style>
.btn-pr {
    color: #fff;
    background-color: #35b729;
    border-color: #35b729;
}
.btn-pr:active {
    color: #fff;
    background-color: #34c726 !important;
    border-color: #35b729 !important;
}
.btn-pr:hover {
    color: #fff;
    background-color: #34c726 !important;
    border-color: #35b729 !important;
}
   
.btn-pr:focus, .btn-pr.focus {
    color: #fff;
    background-color: #35b729 !important;
    border-color: #35b729 !important;
}
</style>

@endif

{{-- fim --}}
<div ng-controller="temasController">
    <div style="padding: 0 20px">
        @if(Auth::user()->role=='admin'  && Auth::user()->passo < 3)
          {{--  inicio do confirme seus dados --}}
        <div class="row" style="margin-top: -30px;">
            <div class="col-md-6">
                <h2>
                    Confirme as cores
                </h2>
                <p>
                    Não se preocupe, você pode editar informações depois.
                </p>
            </div>
        </div>
        @endif     
{{--  fim do confirme seus dados --}}



        {{--  caso seja o tema padrao Roxo --}}
        <div class="row" ng-if="tema_id==1" style="margin-top: 20px">
            <div class="col-xs-12  col-md-4">
                
                {{-- <div class="form-group">
    <label>
        Selecione o tema
    </label>
    <select class="form-control" name="tema" ng-change="changeTema(tema_id)" ng-model="tema_id">
        <option value="">
            Selecione o tema
        </option>
        <option ng-repeat="tema in temas" ng-value="tema.id">
            @{{tema.nome}}
        </option>
    </select>
</div> --}}

                  <p>
                     Utilize o botão abaixo para fazer personalização do tema da sua academia!
                 </p>
                <button class="btn btn-primary btn-rounded btn-block" style="margin-top: 21px;" 
                 ng-click="gravarTema(tela.id, tela.nometela,2)">
                   Iniciar personalização<i class="fa fa-arrow-right"></i>
                </button>
                <div class="text-center visible-xs">
                      <button class="btn btn-primary btn-rounded btn-block" style="margin-top: 21px;" 
                         ng-click="gravarTema(tela.id, tela.nometela,2)">
                           Iniciar peronalização <i class="fa fa-arrow-right"></i>
                    </button>
                </div>
                

                <div style="margin-top:20px"></div>

                  <div class="menu_tema ">
                     <div style="text-align: center;">
                        <img src="{{ url('/')}}/coresApp/img/home_ex.png" class="img-responsive" style="margin:auto" >
                    </div>
                </div>

            </div>

            <div class="col-xs-12 col-md-4">
                
            </div>

           
        </div>
        {{--  FIM caso seja o tema padrao Roxo --}}



        <!-- inicio do tema -->
        <div class="row" ng-show="tema_id==2" style="margin-top: 20px">
            <div class="col-md-4">
            

                {{-- <div class="form-group">
                    <label>
                        Selecione o tema
                    </label>
                    <select class="form-control" name="tema" ng-change="changeTema(tema_id)" ng-model="tema_id">
                        <option value="">
                            Selecione o tema
                        </option>
                        <option ng-repeat="tema in temas" ng-value="tema.id">
                            @{{tema.nome}}
                        </option>
                    </select>
                </div> --}}
                {{--  fim de seleciona tema --}}
                <input class="form-group" id="qualTela" type="hidden" value="{{$tela}}">
                    <div class="form-group">
                        <label>
                            Selecione a Tela
                        </label>
                        <!--  ng-selected="14 == tela.id" -->
                        <select class="select2_demo_1 form-control" ng-change="changeTela(telset)" ng-model="telset" ng-options="tela as tela.nometela for tela  in telas">
                            <option value="">
                                Selecione a Tela
                            </option>
                        </select>
                    </div>
                    
                @if($tela == 'Home')
                    <!-- home.json -->
                    @include('coresApp.telas.home')
                @endif
                @if($tela == 'Mensagem')
    
                    @include('coresApp.telas.mensagem')
                @endif
                @if($tela == 'Gfm')
    
                    @include('coresApp.telas.gfm')
                @endif
                @if($tela == 'Natação')
    
                    @include('coresApp.telas.natacao')
                @endif
                @if($tela == 'Cross')
    
                    @include('coresApp.telas.cross')
                @endif
                @if($tela == 'Musculação')
    
                    @include('coresApp.telas.musculacao')
                @endif
                @if($tela == 'Clube')
    
                    @include('coresApp.telas.clubedevantagens')
                @endif
                @if($tela == 'Menu')
    
                    @include('coresApp.telas.menu')
                @endif
                @if($tela == 'Treino')
    
                    @include('coresApp.telas.treino')
                @endif
                @if($tela == 'Ciclismo')
    
                    @include('coresApp.telas.ciclismo')
                @endif
                @if($tela == 'Chat')
    
                    @include('coresApp.telas.chat')
                @endif
                @if($tela == 'Contato')
    
                    @include('coresApp.telas.contato')
                @endif
                @if($tela == 'Reserva')
    
                    @include('coresApp.telas.reserva')
                @endif
                @if($tela == 'Empresa')
    
                    @include('coresApp.telas.empresa')
                @endif

                @if($tela == 'Selecionar_Treino')
    
                    @include('coresApp.telas.seleciona_treino')
                @endif
                
                @if($tela == 'Corrida')
                    @include('coresApp.telas.corrida')
                @endif
                

                @if($tela == 'Planejamento')
                    @include('coresApp.telas.planejamento')
                @endif
                
                @if($tela == 'Exercicio')
                    @include('coresApp.telas.exercicio')
                @endif
                
                @if($tela == 'Conquistas')
                    @include('coresApp.telas.conquistas')
                @endif
                
                @if($tela == 'Modal')
                    @include('coresApp.telas.modal')
                @endif
                @if($tela == 'Coach')
    
                    @include('coresApp.telas.coach')
                @endif
                
                </input>
            </div>
            @if($tela == 'Home')
            @include('coresApp.menus.home')
            @endif
            @if($tela =='Mensagem')
            @include('coresApp.menus.mensagem')
            @endif
            @if($tela == 'Gfm')
            @include('coresApp.menus.gfm')
            @endif
            @if($tela == 'Natação')
            @include('coresApp.menus.natacao')
            @endif
            @if($tela == 'Cross')
            @include('coresApp.menus.cross')
            @endif
            @if($tela == 'Musculação')
                @include('coresApp.menus.musculacao')
            @endif
            @if($tela == 'Clube')
            @include('coresApp.menus.clubedevantagens')
            @endif
            @if($tela == 'Menu')
            @include('coresApp.menus.menu')
            @endif
            @if($tela == 'Treino')
            @include('coresApp.menus.treino')
            @endif
            @if($tela == 'Ciclismo')
            @include('coresApp.menus.ciclismo')
            @endif
            @if($tela == 'Corrida')
            @include('coresApp.menus.corrida')
            @endif
            @if($tela == 'Chat')
            @include('coresApp.menus.chat')
            @endif
            @if($tela == 'Contato')
                @include('coresApp.menus.contato')
            @endif
            @if($tela == 'Reserva')
                @include('coresApp.menus.reserva')
            @endif
            @if($tela == 'Empresa')
                @include('coresApp.menus.empresa')
            @endif

            @if($tela == 'Selecionar_Treino')
                @include('coresApp.menus.seleciona_treino')
            @endif
            @if($tela == 'Planejamento')
                @include('coresApp.menus.planejamento')
            @endif
            
            @if($tela == 'Exercicio')
                @include('coresApp.menus.exercicio')
            @endif

            @if($tela == 'Conquistas')
                @include('coresApp.menus.conquistas')
            @endif

            @if($tela == 'Modal')
                @include('coresApp.menus.modal')
            @endif
            @if($tela == 'Coach')
                @include('coresApp.menus.coach')
            @endif

            <div class="btn_salvar_como">
                <div class="row">
                    <div class="col-md-6">
                    </div>
                    <div class="col-md-3">
                      
                        <button class="btn btn-default btn-rounded btn-block"   ng-click="gravarTema(tela.id, tela.nometela,1)">
                            <i class="fa fa-repeat" ></i> Restaurar Tema Padrão 
                        </button>
                     {{--     <button class="btn btn-default btn-rounded btn-block" ng-click="gravarTema(tela.id, tela.nometela,tema_id)">
                            <i class="fa fa-repeat" ></i> Salvar
                        </button> --}}
                    </div>
                    <div class="col-md-3" ng-hide="tema_id!==2">
                        <button class="btn btn-primary btn-rounded btn-block espaco_btncor" ng-click="gravarTema(tela.id, selectedItem,tema_id)">
                            <i class="fa fa-check">
                            </i>
                            Salvar e publicar
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <!-- fim row-->
    </div>
</div>
@endsection
