     {!! Html::style("coresApp/css/conquistas.css") !!}

       <div class="menu_tema ">
                <header>
                    <div class="row">
                        <div class="col-xs-2">
                            <i aria-hidden="true" class="fa fa-bars menu-bar">
                            </i>
                        </div>
                        <div class="col-xs-8">
                            <h2>
                                CONQUISTAS
                            </h2>
                        </div>
                        <div class="col-xs-2">
                            <!-- right -->
                        </div>
                    </div>
                </header>
                <div class="conteudo page_background">
                    <div class="conteudo-overlay">
                        <div class="cont">
                            <div class="texto">
                                CONQUISTAS RECENTES
                            </div>
                            <div class="conquista_linha corllll">
                                <table>
                                    <tr class="row ons-row-inner" style="padding: 10px 0">
                                        <td style="-webkit-box-flex: 0; flex: 0 0 13%; max-width: 13%;" width="13%">
                                            <img alt="" src="http://www.envolverde.com.br/wp-content/uploads/2014/11/medalha.png" style="width: 90%">
                                            </img>
                                        </td>
                                        <td class="col td-inner">
                                            <h2 style="" class="texto">
                                                Parabéns Bruna Moreira, fez login no APP
                                            </h2>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <!--  fim dilinha conquista_linha -->
                            <div class="conquista_linha corllll">
                                <table>
                                    <tr class="row ons-row-inner" style="padding: 10px 0">
                                        <td style="-webkit-box-flex: 0; flex: 0 0 13%; max-width: 13%;" width="13%">
                                            <img alt="" src="http://www.envolverde.com.br/wp-content/uploads/2014/11/medalha.png" style="width: 90%">
                                            </img>
                                        </td>
                                        <td class="col td-inner">
                                            <h2 style="  " class="texto">
                                                Parabéns Bruna Moreira, iniciou um projeto
                                            </h2>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <!--  fim dilinha conquista_linha -->
                            <div class="conquista_linha corllll">
                                <table>
                                    <tr class="row ons-row-inner" style="padding: 10px 0">
                                        <td style="-webkit-box-flex: 0; flex: 0 0 13%; max-width: 13%;" width="13%">
                                            <img alt="" src="http://www.envolverde.com.br/wp-content/uploads/2014/11/medalha.png" style="width: 90%">
                                            </img>
                                        </td>
                                        <td class="col td-inner">
                                            <h2 style="  " class="texto">
                                                Parabéns Bruna Moreira, realizou o treino de musculação dentro do tempo esperado.
                                            </h2>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <!--  fim dilinha conquista_linha -->
                            <hr>
                                <h2 class="conquista_titulo" class="texto">
                                    GRÁFICO DE PESAGEM
                                    <small style="color: #fff">
                                        (Últimos 10)
                                    </small>
                                </h2>
                                <div style="text-align:center">
                                    <img alt="" src="{{ url('/')}}/coresApp/img/gfm_grafico.png" style="width: 90%; margin: auto">
                                    </img>
                                </div>
                            </hr>
                        </div>
                        <!-- inicio da listagem das aulas -->
                    </div>
                    <!--  fim conteudo-overlay -->
                </div>
            </div>
            <!--  fim meu tema -->