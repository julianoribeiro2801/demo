{!! Html::style("coresApp/css/seleciona_treino.css") !!}
<!-- icomoon -->
{!! Html::style("coresApp/icomoon/style.css") !!}

<div class="menu_tema ">

            <header>
                <div class="row">

                    <div class="col-xs-2">
                        <i class="fa fa-bars menu-bar" aria-hidden="true"></i>
                    </div>
                    <div class="col-xs-8">
                        <h2>
                            Treino
                        </h2>

                    </div>
                    <div class="col-xs-2">
                        <!-- right -->
                    </div>

                </div>

            </header>

            <div class="conteudo page_background">
                <div class="conteudo-overlay">
                    <div class="musc">
                        <div class="cor_linha cor_linha_impar" >
                            <p style="padding-left: 70px;">MUSCULAÇÃO</p>
                        </div>
                        <div class="colun">
                            <div class="trophy_inside">
                                 <span class="icon-trophy" ></span>
                            </div>
                            <div class="num">
                                1°
                            </div>              
                            <div>
                                <i class="fa fa-times-circle xx" aria-hidden="true"></i>
                            </div>
                        </div>
                    </div>
                    <div class="ginas">
                        <div class="cor_linha cor_linha_par" >
                            <p style="padding-left: 30px;">GINÁSTICA</p>
                        </div>
                        <div class="colun">
                             <div class="trophy_inside">
                                 <span class="icon-trophy" ></span>
                            </div>
                            <div class="num">
                                1°
                            </div>
                            <div>
                                <i class="fa fa-times-circle xx" aria-hidden="true"></i>
                            </div>
                        </div>
                    </div>
                    <div class="nat">
                        <div class="cor_linha cor_linha_impar" >
                            <p style="padding-left: 20px;">NATAÇÃO</p>
                        </div>
                        <div class="colun">
                             <div class="trophy_inside">
                                 <span class="icon-trophy" ></span>
                            </div>
                            <div class="num">
                                4°
                            </div>
                            <div>
                                <i class="fa fa-times-circle xx" aria-hidden="true"></i>
                            </div>
                        </div>
                    </div>
                    <!-- inicio da listagem das aulas -->
                </div>
                <!--  fim conteudo-overlay -->
            </div>
        </div>
        <!--  fim meu tema -->