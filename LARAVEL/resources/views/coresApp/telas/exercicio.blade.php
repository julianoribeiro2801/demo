 {!! Html::style("coresApp/css/exercicio.css") !!}

 <div class="menu_tema ">

            <header>
                <div class="row">

                    <div class="col-xs-2">
                        <i class="fa fa-bars menu-bar" aria-hidden="true"></i>
                    </div>
                    <div class="col-xs-8">
                        <h2>
                            TREINO B - Sex.25
                        </h2>
                    </div>
                    <div class="col-xs-2">
                        <!-- right -->
                    </div>
                </div>
            </header>

            <div class="conteudo page_background">
                <div class="conteudo-overlay">
                    <div class="top">
                        <div style="text-align: center;">
                            <img src="{{ url('/')}}/coresApp/img/novideo.png" class="img">
                        </div>
                    </div>
                    <div class="conte textoexer">
                        <table>
                            <tr style="width: 100%;">
                                <td style="width: 50%; color: #fff; padding-top: 10px; padding-left: 15px;">
                                    <p class="textoexer">Agachamento bulgaro</p>
                                    <p style="font-size: 12px;" class="textoexer">Séries e Repetições</p>
                                    <p class="textoexer">3 x 15</p>
                                </td>
                                <td style="width: 37.5%; text-align: right; color: #fff; padding-top: 12px;">
                                    <p class="textoexer">Agrupar</p>
                                    <p class="textoexer">Intervalo:</p>
                                    <p class="textoexer">0 seg.</p>                     
                                </td>
                                <td style="width: 37.5%; text-align: right; ">
                                    <div style="height: 40px; padding-top: 5px;">
                                        <i class="fa fa-toggle-on" aria-hidden="true" style="font-size: 20; color:#fff;"></i>
                                    </div>
                                    <div style="height: 40px; padding-top: 20px;">
                                        <img src="{{ url('/')}}/coresApp/img/estrutura/relogio.png" style="width: 20px;">
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <div class="um textoexer">
                            <table>
                                <tr style="width: 100%;">
                                    <td style="width: 25%;">
                                        <div class="one textoexer">
                                            1
                                        </div>
                                    </td>
                                    <td style="width: 37.5%;">
                                        <div class="two textoexer">
                                            15 min
                                        </div>
                                    </td>
                                    <td style="width: 37.5%;">
                                        <div class="three textoexer">
                                            1 bpm
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="dois textoexer">
                            <table>
                                <tr style="width: 100%;">
                                    <td style="width: 25%;">
                                        <div class="one textoexer">
                                            2
                                        </div>
                                    </td>
                                    <td style="width: 37.5%;">
                                        <div class="two textoexer">
                                            15 min
                                        </div>
                                    </td>
                                    <td style="width: 37.5%;">
                                        <div class="three textoexer">
                                            1 bpm
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="tres textoexer">
                            <table>
                                <tr style="width: 100%;">
                                    <td style="width: 25%;">
                                        <div class="one textoexer">
                                            3
                                        </div>
                                    </td>
                                    <td style="width: 37.5%;">
                                        <div class="two textoexer">
                                            15 min
                                        </div>
                                    </td>
                                    <td style="width: 37.5%;">
                                        <div class="three textoexer">
                                            1 bpm
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    

                    <!-- inicio da listagem das aulas -->
                </div>
                <!--  fim conteudo-overlay -->
            </div>
        </div>
        <!--  fim meu tema -->