{!! Html::style("coresApp/css/empresa.css") !!}
<div class="menu_tema ">
    <header>
        <div class="row">
            <div class="col-xs-2">
                <i class="fa fa-bars menu-bar" aria-hidden="true"></i>
            </div>
            <div class="col-xs-8">
                <h2>
                Empresa
                </h2>
            </div>
            <div class="col-xs-2">
                <!-- right -->
            </div>
        </div>
    </header>
    <div class="conteudo page_background teste">
        <div class="conteudo-overlay">
            <div class="top">
                <div style="text-align: center;">
                    <img src="{{ url('/')}}/coresApp/img/pers.png" class="img">
                </div>
                <div class="aqua">
                    <p>Personal Aqua Test</p>
                </div>
            </div>
            <div class="equip">
                <div>
                    <h4> Equipe</h4>
                </div>
                <table style="width: 100%; text-align: center; color: #fff;">
                    <tr>
                        <td>
                            <div>
                                <img src="{{ url('/')}}/coresApp/img/louize.jpg" class="iimg">
                                <p>Louize</p>
                            </div>
                        </td>
                        <td>
                            <div>
                                <img src="{{ url('/')}}/coresApp/img/mari.jpg" class="iimg">
                                <p>Janaina</p>
                            </div>
                        </td>
                        <td>
                            <div>
                                <img src="{{ url('/')}}/coresApp/img/james.jpg" class="iimg">
                                <p>Fernando</p>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="cont">
                <h4>Contate-nos:</h4>
                <table class="cnt">
                    <tr>
                        <td>
                            <i class="fa fa-phone ico" aria-hidden="true"></i>
                        </td>
                        <td>
                            <p>(45) 3038-7777</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <i class="fa fa-globe ico" aria-hidden="true"></i>
                        </td>
                        <td>
                            <p>http://academiaaquafit.com.br</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <i class="fa fa-envelope-o ico" aria-hidden="true"></i>
                        </td>
                        <td>
                            <p>karstenschewab@gmail.com</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <i class="fa fa-map-marker ico" aria-hidden="true"></i>
                        </td>
                        <td>
                            <p>R. XV de Novembro 419, Toledo PR</p>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="mod">
                <p style="font-size: 18px; text-align: center;">MODALIDADES:</p>
                <p>MUSCULAÇÃO, GINÁSTICA, NATAÇÃO, CROSS TRANING, CORRIDA, CICLISMO.</p>
            </div>
            <!-- inicio da listagem das aulas -->
        </div>
        <!--  fim conteudo-overlay -->
    </div>
</div>
<!--  fim meu tema -->