{!! Html::style("coresApp/css/treino.css") !!}

  <div class="menu_tema ">

            <header>
                <div class="row">

                    <div class="col-xs-2">
                        <i class="fa fa-bars menu-bar" aria-hidden="true"></i>
                    </div>
                    <div class="col-xs-8">
                        <h2>
                            TREINO B
                        </h2>

                    </div>
                    <div class="col-xs-2">
                        <!-- right -->
                    </div>

                </div>

            </header>

            <div class="conteudo page_background">
                <div class="conteudo-overlay">
                    

                    <!-- inicio da listagem das aulas -->
                    <div>
                        <table class="tab">
                            <tr>
                                <td>
                                    <div class="img"> 
                                        <img src="{{ url('/')}}/coresApp/img/abdom.jpg" class="abdom">
                                    </div> 
                                </td>
                                <td>
                                    <div> 
                                        <p class="p txt-treino">Abdominal com bola</p>
                                        <p class="p txt-treino">1 x 3</p>
                                    </div>
                                </td>
                                <td>
                                    <div>
                                        <i class="fa fa-chevron-right iconi" aria-hidden="true"></i>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <table class="tabb">
                            <tr>
                                <td>
                                    <div class="img"> 
                                        <img src="{{ url('/')}}/coresApp/img/bulg.png" class="abdom">
                                    </div> 
                                </td>
                                <td>
                                    <div> 
                                        <p class="p txt-treino">Agachamento bugaro</p>
                                        <p class="p txt-treino">3 x 15</p>
                                    </div>
                                </td>
                                <td>
                                    <div>
                                        <i class="fa fa-chevron-right iconi" aria-hidden="true"></i>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <table class="tabbb">
                            <tr>
                                <td>
                                    <div class="img"> 
                                        <img src="{{ url('/')}}/coresApp/img/smith.jpg" class="abdom">
                                    </div> 
                                </td>
                                <td>
                                    <div> 
                                        <p class="p txt-treino">Desenvolvimento Smith frontal</p>
                                        <p class="p txt-treino">5 x 3</p>
                                    </div>
                                </td>
                                <td>
                                    <div>
                                        <i class="fa fa-chevron-right iconi" aria-hidden="true"></i>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>

                </div>
                <!--  fim conteudo-overlay -->

            </div>


        </div>
        <!--  fim meu tema -->