{!! Html::style("coresApp/css/clubedevantagens.css") !!}

    <div class="menu_tema ">

            <header>
                <div class="row">

                    <div class="col-xs-2">
                        <i class="fa fa-bars menu-bar" aria-hidden="true"></i>
                    </div>
                    <div class="col-xs-8">
                        <h2>
                            Clube de Vantagens
                        </h2>

                    </div>
                    <div class="col-xs-2">
                        <!-- right -->
                    </div>

                </div>

            </header>

            <div class="conteudo page_background">
                <div class="conteudo-overlay">

                    <div class="clube_vantagens">
                        <table>
                            <tr>
                                <td style="width: 140px">
                                    <div class="divTopClub">
                                        <div class="foto_perfil  imgperf" style="background-image: url('{{ url('/')}}/coresApp/img/user.gif');
                                        margin:20px auto 10px auto; width: 80px; height: 80px; ">
                                        </div>
                                        <div class="labelname_clube"> Adriano Ruiz</div>
                                    </div>
                                </td>
                                <td>
                                    <div class="nome_mes">Maio</div>

                                    <table>
                                        <tr>
                                            <td>
                                                <img src="{{ url('/')}}/coresApp/img/ico_novos/carteira.png" style="height: 50px" />
                                            </td>
                                            <td>
                                                <h2 class="alinha_middle" >
                                                    Você já economizou:
                                                </h2>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="row" style="padding-top: 8px">

                                        <div class="col-xs-12 ">

                                            <div style="float:right">

                                                <div class="dinherio_clube">
                                                    R$ 00,00
                                                </div>

                                            </div>


                                        </div>

                                    </div>
                                </td>
                            </tr>
                        </table>

                    </div>
                    <!-- inicio da listagem  -->


                    <div class="empresas">
                        <!-- aqui vai a listagem da empresas -->
                        <table>
                            <tr class="cor_impar_clube">
                                <td class="td">
                                    <img src="{{ url('/')}}/coresApp/img/diversao.png" class="diver">
                                    <p class="aa">DIVERSÃO</p>
                                </td>
                                <td class="tdd"> 
                                    <p>Cinema Filmax</p>
                                </td>
                                <td class="td"> 
                                    <div class="dez">
                                        10%
                                    </div>
                                </td>
                                <td class="td"> 
                                    <i class="fa fa-chevron-right iconi" aria-hidden="true"></i>
                                </td>
                            </tr>
                             <tr class="cor_par_clube">
                                <td class="ttd">
                                    <img src="{{ url('/')}}/coresApp/img/nutri.png" class="diver">
                                    <p class="aa">NUTRIÇÃO</p>
                                </td>
                                <td class="tttd"> 
                                    <p>Cáudia nutrição </br>esportiva</p>
                                </td>
                                <td class="ttd"> 
                                    <div class="dez">
                                        10%
                                    </div>
                                </td>
                                <td class="ttd"> 
                                    <i class="fa fa-chevron-right iconi" aria-hidden="true"></i>
                                </td>
                            </tr>
                            <tr class="cor_impar_clube">
                                <td class="td">
                                    <img src="{{ url('/')}}/coresApp/img/educacao.png" class="diver">
                                    <p class="aa">EDUCAÇÃO</p>
                                </td>
                                <td class="tdd"> 
                                    <p>Escola de Inglês</p>
                                </td>
                                <td class="td"> 
                                    <div class="dez">
                                        10%
                                    </div>
                                </td>
                                <td class="td"> 
                                    <i class="fa fa-chevron-right iconi" aria-hidden="true"></i>
                                </td>
                            </tr>
                             <tr class="cor_par_clube">
                                <td class="ttd">
                                    <img src="{{ url('/')}}/coresApp/img/saude.png" class="diver">
                                    <p class="aa">SAÚDE</p>
                                </td>
                                <td class="tttd"> 
                                   <p>Farmácia da esquina</p>
                                </td>
                                <td class="ttd"> 
                                    <div class="dez">
                                        10%
                                    </div>
                                </td>
                                <td class="ttd"> 
                                    <i class="fa fa-chevron-right iconi" aria-hidden="true"></i>
                                </td>
                            </tr>
                        </table>
                    </div>


                </div>
                <!--  fim conteudo-overlay -->

            </div>


        </div>
