    {!! Html::style("coresApp/css/reserva.css") !!}
 <div class="menu_tema ">

    <header>
        <div class="row">

            <div class="col-xs-2">
                <i class="fa fa-bars menu-bar" aria-hidden="true"></i>
            </div>
            <div class="col-xs-8">
                <h2>
                    RESERVA GINÁSTICA
                </h2>
            </div>
            <div class="col-xs-2">
                <!-- right -->
            </div>
        </div>
    </header>

    <div class="conteudo page_background">
        <div class="conteudo-overlay">
            <div class="top">
                <div style="text-align: center;">
                   <span class="icon-fitness" style="font-size: 130px; color: #ff9130;"></span>
                </div>
                <div class="princ texto_reserva">
                    <p style="font-size: 20px;">14:00 PILATES</p>
                    <p>Professor: Antonio Fagundes</p>
                    <p style="font-size: 20px;">5</p>
                    <p>vagas disponíveis</p>
                </div>
            </div>
            <div>
                <div class="reservar">
                    RESERVAR
                </div>
            </div>
            
            <!-- inicio da listagem das aulas -->
        </div>
        <!--  fim conteudo-overlay -->
    </div>
</div>
<!--  fim meu tema -->
