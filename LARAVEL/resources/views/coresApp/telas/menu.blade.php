{!! Html::style("coresApp/css/menu.css") !!}


  <div class="menu_tema ">
            <div class="conteudo page_background">
                <div class="conteudo-overlay">

                    <div>
                        <table class="barra">
                            <tr class="col-img">
                                <td class="td">
                                    <div class="div">
                                        <img src="{{ url('/')}}/coresApp/img/bruna.jpeg" class="img">
                                    </div>
                                </td>
                                 <td class="ttd">
                                    <p class="textomenu">Bruna Moreira</p>
                                </td>
                            </tr>
                             <tr class="col">
                                <td class="td">
                                     <div class="div">
                                        <span class="icon-55240 coment" style="font-size: 38px;"></span>
                                    </div>
                                </td>
                                 <td class="ttd">
                                    <p class="textomenu">Treino</p>
                                </td>
                            </tr>
                            <tr class="col">
                                <td class="td">
                                    <div class="div">
                                        <span class="icon-if_calendar11_216218 coment" style="font-size: 38px;"></span>
                                    </div>
                                </td>
                                 <td class="ttd">
                                    <p class="textomenu">Planejamento Semanal</p>
                                </td>
                            </tr>
                            <tr class="col">
                                <td class="td">
                                    <div class="div">
                                        <i class="fa fa-star coment" aria-hidden="true"></i>
                                    </div>
                                </td>
                                 <td class="ttd">
                                    <p class="textomenu">Conquistas</p>
                                </td>
                            </tr>
                            <tr class="col">
                                <td class="td">
                                     <div class="div">
                                        <span class="icon-balanca coment" style="font-size: 38px;"></span>
                                    </div>
                                </td>
                                 <td class="ttd">
                                    <p class="textomenu">Pesagem/Fotos</p>
                                </td>
                            </tr>
                            <tr class="col">
                                <td class="td">
                                    <div class="div">
                                        <i class="fa fa-comment-o coment" aria-hidden="true"></i>
                                    </div>
                                </td>
                                 <td class="ttd">
                                    <p class="textomenu">Chat</p>
                                </td>
                            </tr>
                            <tr class="col">
                                <td class="td">
                                    <div class="div">
                                        <span class="icon-if_88_111104 coment" style="font-size: 38px;"></span>
                                    </div>
                                </td>
                                 <td class="ttd">
                                    <p class="textomenu">Coach</p>
                                </td>
                            </tr>
                            <tr class="col">
                                <td class="td">
                                     <div class="div">
                                        <span class="icon-coins-icon-70600 coment" style="font-size: 38px;"></span>
                                    </div>
                                </td>
                                 <td class="ttd">
                                    <p class="textomenu">Clube de Vantagens</p>
                                </td>
                            </tr>
                            
                           
                            
                        </table>

                    </div>
                    <!-- inicio da listagem  -->


                    <div class="empresas">
                        <!-- aqui vai a listagem da empresas -->
                        
                    </div>


                </div>
                <!--  fim conteudo-overlay -->

            </div>


        </div>