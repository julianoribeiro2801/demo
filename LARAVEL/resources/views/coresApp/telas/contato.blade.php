    {!! Html::style("coresApp/css/contato.css") !!}
 <div class="menu_tema ">

            <header>
                <div class="row">

                    <div class="col-xs-2">
                        <i class="fa fa-bars menu-bar" aria-hidden="true"></i>
                    </div>
                    <div class="col-xs-8">
                        <h2>
                            Ajude-nos a Melhorar
                        </h2>

                    </div>
                    <div class="col-xs-2">
                        <!-- right -->
                    </div>

                </div>

            </header>

            <div class="conteudo page_background">
                <div class="conteudo-overlay">
                    <div class="cont">
                        <div class="texto">
                            <p>
                                Ajude-nos a melhorar o </br>aplicativo. </br>Dê sua opinião.
                            </p>
                        </div>
                        <div class="nome texto">
                            <p>
                                Bruna Moreira
                            </p>
                        </div>
                        <div class="escr texto">
                            <p>Escreva aqui...</p>
                        </div>
                        <div class="iniciar">
                            ENVIAR
                        </div>
                    </div>
                    <!-- inicio da listagem das aulas -->
                </div>
                <!--  fim conteudo-overlay -->
            </div>
        </div>