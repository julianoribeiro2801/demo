{!! Html::style("coresApp/css/ciclismo.css") !!}


<div class="menu_tema ">

    <header>
        <div class="row">

            <div class="col-xs-2">
                <i class="fa fa-bars menu-bar" aria-hidden="true"></i>
            </div>
            <div class="col-xs-8">
                <h2>
                    CICLISMO
                </h2>

            </div>
            <div class="col-xs-2">
                <!-- right -->
            </div>

        </div>

    </header>

    <div class="conteudo page_background">
        <div class="conteudo-overlay">

            <div class="texto_desc fundo-desc-ciclismo">
                <p class="texto-cicl">
                    Nenhum treino cadastrado.
                </p>
            </div>
            <div class="cont">
                <div class="partiu partiuciclismo">
                #PARTIU!
                </div>
                <table>
                    <tr>
                        <td>
                            <div class="regis texto-cicl">
                                <p>REGISTROS</p>
                            </div>
                        </td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td>
                            <div class="circ texto-cicl">
                                <p>1</p>
                            </div>
                        </td>
                        <td class="dist texto-cicl">
                            <p>Distância</p>
                        </td>
                        <td>
                            <div class="km texto-cicl">
                            0 Km<span class="zero_min"></span>
                            </div>
                        </td>
                        <td>
                            <div>
                                <i class="fa fa-check-circle branco" aria-hidden="true"></i>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            
        
           
           

            <!-- inicio da listagem das aulas -->
            <div id="aulas">
                <table class="one linha_ciclismo_sim">
                      <tr>
                        <td>
                            <div class="dia_aula texto-cicl"> SAB. </div>
                        </td>
                        <td>
                            <div class="data_aula texto-cicl">26/05/2018 11:45</div>
                        </td>
                        <td class="al_direito">
                            <div class="checkicone">
                                <i class="fa fa-share-alt-square ico" aria-hidden="true"></i>
                            </div>
                        </td>
                        <td>
                            <div class="min texto-cicl">
                                <p>180km</p>
                            </div>
                        </td>
                    </tr>
                </table>
                <table class="one linha_ciclismo_nao">
                      <tr>
                        <td>
                            <div class="dia_aula texto-cicl"> TER. </div>
                        </td>
                        <td>
                            <div class="data_aula texto-cicl">26/05/2018 11:45</div>
                        </td>
                        <td class="al_direito">
                            <div class="checkicone">
                                <i class="fa fa-share-alt-square ico" aria-hidden="true"></i>
                            </div>
                        </td>
                        <td>
                            <div class="min texto-cicl">
                                <p>350km</p>
                            </div>
                        </td>
                    </tr>
                </table>
                   <table class="one linha_ciclismo_sim">
                      <tr>
                        <td>
                            <div class="dia_aula texto-cicl"> TER. </div>
                        </td>
                        <td>
                            <div class="data_aula texto-cicl">26/05/2018 12:45</div>
                        </td>
                        <td class="al_direito">
                            <div class="checkicone">
                                <i class="fa fa-share-alt-square ico" aria-hidden="true"></i>
                            </div>
                        </td>
                        <td>
                            <div class="min">
                                <p>270km</p>
                            </div>
                        </td>
                    </tr>
                </table>
                   <table class="one linha_ciclismo_nao">
                      <tr>
                        <td>
                            <div class="dia_aula texto-cicl"> QUI. </div>
                        </td>
                        <td>
                            <div class="data_aula texto-cicl">08/06/2018 11:45</div>
                        </td>
                        <td class="al_direito">
                            <div class="checkicone">
                                <i class="fa fa-share-alt-square ico" aria-hidden="true"></i>
                            </div>
                        </td>
                        <td>
                            <div class="min texto-cicl">
                                <p>980km</p>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <!--  fim conteudo-overlay -->

    </div>


</div>
<!--  fim meu tema -->
