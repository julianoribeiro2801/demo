   {!! Html::style("coresApp/css/gfm.css") !!}

 <div class="menu_tema ">

            <header>
                <div class="row">

                    <div class="col-xs-2">
                        <i class="fa fa-bars menu-bar" aria-hidden="true"></i>
                    </div>
                    <div class="col-xs-8">
                        <h2>
                            AULA DO DIA
                        </h2>

                    </div>
                    <div class="col-xs-2">
                        <!-- right -->
                    </div>

                </div>

            </header>

            <div class="conteudo page_background">
                <div class="conteudo-overlay">
                    <div id="menu_aulas_gfm">
                        <table class="menu_gmf">
                            <tr>
                                <td class="fundo_dia textogfm">D</td>
                                <td class="fundo_dia textogfm">2º</td>
                                <td class="fundo_dia textogfm">3º</td>
                                <td class="fundo_dia textogfm">4º</td>
                                <td class="fundo_dia textogfm">5º</td>
                                <td class="fundo_dia textogfm">6º</td>
                                <td class="fundo_dia textogfm" style="border: 0">S</td>
                            </tr>
                        </table>
                    </div>

                    <!-- inicio da listagem das aulas -->
                    <div id="aulas_gfm">
                        <table class="cor_impar_gfm">
                            <tr>
                                <td class="esp_img">
                                     <span class="icon-fitness branco" style="font-size: 80px; color: #ff9130;"></span>
                                </td>
                                <td>
                                    <div class="hr_aula textogfm"> 13:30 </div>
                                    <div class="nome_aula textogfm">HIDROGNASTICA</div>
                                </td>
                                <td class="al_direito">
                                    <div class="checkicone">
                                        <i class="fa fa-check-circle branco" aria-hidden="true"></i>
                                    </div>
                                    <p class="textogfm">5 VAGAS</p>
                                </td>
                            </tr>
                        </table>

                        <table class="cor_par_gfm">
                            <tr>
                                <td class="esp_img">
                                    <span class="icon-fitness branco" style="font-size: 80px; color: #ff9130;"></span>
                                </td>
                                <td>
                                    <div class="hr_aula textogfm"> 13:30 </div>
                                    <div class="nome_aula textogfm">HIDROGNASTICA</div>
                                </td>
                                <td class="al_direito">
                                    <div class="checkicone">
                                        <i class="fa fa-check-circle branco" aria-hidden="true"></i>
                                    </div>
                                    <p class="textogfm">5 VAGAS</p>
                                </td>
                            </tr>
                        </table>

                        <table class="cor_impar_gfm">
                            <tr>
                                <td class="esp_img">
                                    <span class="icon-fitness branco" style="font-size: 80px; color: #ff9130;"></span>
                                </td>
                                <td>
                                    <div class="hr_aula textogfm"> 13:30 </div>
                                    <div class="nome_aula textogfm">HIDROGNASTICA</div>
                                </td>
                                <td class="al_direito">
                                    <div class="checkicone">
                                        <i class="fa fa-check-circle branco" aria-hidden="true"></i>
                                    </div>
                                    <p class="textogfm">5 VAGAS</p>
                                </td>
                            </tr>
                        </table>

                        <table class="cor_par_gfm">
                            <tr>
                                <td class="esp_img">
                                    <span class="icon-fitness branco" style="font-size: 80px; color: #ff9130;"></span>
                                </td>
                                <td>
                                    <div class="hr_aula textogfm"> 13:30 </div>
                                    <div class="nome_aula textogfm">HIDROGNASTICA</div>
                                </td>
                                <td class="al_direito">
                                    <div class="checkicone">
                                        <i class="fa fa-check-circle branco" aria-hidden="true"></i>
                                    </div>
                                    <p class="textogfm">5 VAGAS</p>
                                </td>
                            </tr>
                        </table>
                    </div>

                </div>
                <!--  fim conteudo-overlay -->

            </div>


        </div>