{!! Html::style("coresApp/css/musculacao.css") !!}
<div class="menu_tema">
    <header>
        <div class="row">
            <div class="col-xs-2">
                <i aria-hidden="true" class="fa fa-bars menu-bar">
                </i>
            </div>
            <div class="col-xs-8">
                <h2>
                    MUSCULAÇÃO
                </h2>
            </div>
            <div class="col-xs-2">
                <!-- right -->
            </div>
        </div>
    </header>
    <div class="conteudo page_background">
        <div class="conteudo-overlay">
            <div class="musculacao">


                <!-- exercicio linha -->
                <div class="treino_a">
                    <!-- treino A   -->
                    <table>
                        <tr class="linha linhagbg">
                            <td>
                                <div class="circle-out" width="160px">
                                    <div class="circulo-laranja">
                                        <span style=" text-align: center; padding-left: 25px;">
                                            A
                                        </span>
                                    </div>
                                </div>
                            <td class="meio" style="padding-top: 43px;" width="95%">
                                <div class="linha_treino">
                                    <table>
                                        <tr>
                                            <td>
                                                <h2 style="margin-top: 7px;">
                                                    <i aria-hidden="true" class="fa fa-comment" style="margin-left: 22px; color:#9c8fb5"></i>
                                                </h2>
                                            </td>
                                            <td style=" width: 35%">
                                                <!-- aqui vai o partiu      -->
                                                <button class="btnovo2 partiumusc" style="float: right; margin-right:15px; background: #ff7d2a; color: #fff; width: 100%; ">
                                                    <i aria-hidden="true" class="fa fa-share-alt-square">
                                                    </i>
                                                    #PARTIU!
                                                </button>
                                            </td>
                                            <td style=" width: 35%">
                                                <!-- ng-show="play" -->
                                                <button class="btnovo2 iniciarmusc" style="float: right; margin-right:7%;">
                                                    INICIAR
                                                </button>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <table>
                                    <tr style="padding-right: 10px">
                                        <td width="60%">
                                            <div class="bicps">
                                                ABDOMEN, OMBRO
                                            </div>
                                            <div class="bicps">
                                                Tempo Estimado: 16m 54s
                                                <br>
                                                </br>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            </td>
                        </tr>
                    </table>
                </div>


                <!-- exercicio linha -->
                <div class="treino_a">
                    <!-- treino A   -->
                    <table>
                        <tr class="linha linhagbg2">
                            <td>
                                <div class="circle-out" width="160px">
                                    <div class="circulo-laranja circulo-roxo">
                                        <span style=" text-align: center; padding-left: 25px;">
                                            B
                                        </span>
                                    </div>
                                </div>
                            <td class="meio" style="padding-top: 43px;" width="95%">
                                <div class="linha_treino">
                                    <table>
                                        <tr>
                                            <td>
                                                <h2 style="margin-top: 7px;">
                                                    <i aria-hidden="true" class="fa fa-comment" style=" margin-left: 22px; color:#9c8fb5">
                                                    </i>
                                                </h2>
                                            </td>
                                            <td style=" width: 35%">
                                                <!-- aqui vai o partiu      -->
                                                <button class="bt_iniciar_opaco" style="float: right; margin-right:15px; width: 100%; margin-top: 2px;">
                                                    <i class="fa fa-share-alt-square" aria-hidden="true"></i> #PARTIU!
                                                </button>
                                            </td>
                                            <td style=" width: 35%">
                                                <!-- ng-show="play" -->
                                                <button class="bt_iniciar_opaco" style="float: right; margin-right:7%;     margin-top: 2px;">
                                                    INICIAR
                                                </button>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <table>
                                    <tr style="padding-right: 10px">
                                        <td width="60%">
                                            <div class="bicps">
                                                ABDOMEN, OMBRO
                                            </div>
                                            <div class="bicps">
                                                Tempo Estimado: 16m 54s
                                                <br>
                                                </br>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <!-- -->
        </div>
    </div>
</div>
<!--  fim meu tema -->