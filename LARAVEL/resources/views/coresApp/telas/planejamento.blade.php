{!! Html::style("coresApp/css/planejamento.css") !!}
 <div class="menu_tema ">
            <header>
                <div class="row">
                    <div class="col-xs-2">
                        <i class="fa fa-bars menu-bar" aria-hidden="true"></i>
                    </div>
                    <div class="col-xs-8">
                        <h2>
                            PLANEJAMENTO SEMANAL

                        </h2>
                    </div>
                    <div class="col-xs-2">
                        <!-- right -->
                    </div>
                </div>
            </header>

            <div class="conteudo page_background">
                <div class="conteudo-overlay">

                    <!-- inicio da listagem das aulas -->
                    <div class="linha_psa">
                        <!-- ngIf: key==0 -->
                        <div class="dia_semana">
                            Domingo
                        </div>
                        <!-- end ngIf: key==0 -->
                        <table class="alinhar_table">
                            <tr class="dia_linha_psa ">
                                <td class="hora_semana" width="20%">
                                    12:05
                                </td>
                                <td class="des_aula_psa">
                                    BIRIBOL
                                </td>
                            </tr>
                        </table>

                    </div>
                    <!-- end ngIf:  -->

<!-- inicio da listagem das aulas -->
<div class="linha_psa">
    <!-- ngIf: key==0 -->
    <div class="dia_semana">
        Segunda
    </div>
    <!-- end ngIf: key==0 -->
    <table class="alinhar_table">
        <tr class="dia_linha_psa ">
            <td class="hora_semana" width="20%">
                12:05
            </td>
            <td class="des_aula_psa">
                BIRIBOL
            </td>
        </tr>
    </table>

</div>
<!-- end ngIf: atividade.dom_atv != '0' -->

<!-- inicio da listagem das aulas -->
<div class="linha_psa">
    <!-- ngIf: key==0 -->
    <div class="dia_semana">
        Quarta
    </div>
    <!-- end ngIf: key==0 -->
    <table class="alinhar_table">
        <tr class="dia_linha_psa ">
<td class="hora_semana diahoje" width="20%">
                12:05
            </td>
            <td class="des_aula_psa">
                BIRIBOL
            </td>
        </tr>
    </table>

</div>
<!-- end ngIf: atividade.dom_atv != '0' -->

<!-- inicio da listagem das aulas -->
<div class="linha_psa">
    <!-- ngIf: key==0 -->
    <div class="dia_semana">
        Quinta
    </div>
    <!-- end ngIf: key==0 -->
    <table class="alinhar_table">
        <tr class="dia_linha_psa ">
            <td class="hora_semana" width="20%">
                12:05
            </td>
            <td class="des_aula_psa">
                BIRIBOL
            </td>
        </tr>
    </table>

</div>
<!-- end ngIf: atividade.dom_atv != '0' -->




                </div>
                <!--  fim conteudo-overlay -->
            </div>
        </div>
        <!--  fim meu tema -->