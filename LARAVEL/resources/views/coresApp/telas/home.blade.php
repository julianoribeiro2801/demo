
{!! Html::style("coresApp/css/home.css") !!}




    <div class="menu_tema ">

      <header>
        <div class="row">

          <div class="col-xs-2">
            <i class="fa fa-bars menu-bar" aria-hidden="true"></i>
          </div>
          <div class="col-xs-8">
            <h2>
             <div class="academia-name">   
              Nome da Academia
              </div>
            </h2>

          </div>
          <div class="col-xs-2">
            <!-- right -->
          </div>

        </div>

      </header>

      <div class="conteudo page_background">
        <div class="conteudo-overlay">

          <div class="profile-card">

            <div class="profile_pic">
              <div class="profile_pic_inner">
                <div class="change_pic">
                  <div class="foto_perfil" style="background-image: url('{{ url('/')}}/coresApp/img/user.gif');"></div>
                </div>
                <div class="mdfoto">
                  <i class="fa fa-camera"></i>
                </div>
              </div>
            </div>

            <div class="profile-name">
              Aluno Aluno

            </div>

          </div>
          <!-- fim profile-card -->

          <div style="text-align: center; margin-top: 20px">
         {{--    <img src="/uploads/logomarcas/@{{telaSel.logomarca_home }}" alt="">
 --}}


              <img alt="Image" class="img-responsive" ng-if="telaSel.logomarca_home" src="/uploads/logomarcas/@{{telaSel.logomarca_home}}" style="width: 60%; margin: auto;" />

              <img alt="" class="img-responsive" ng-if="!telaSel.logomarca_home" src="http://personalclubbrasil.com.br/wp-content/uploads/2016/09/aplicativo-para-academia-personal-club-brasil-e1473375648219.png" style="width: 60%; margin: auto;" />
               


          </div>

        </div>
        <!--  fim conteudo-overlay -->
        
        <footer>
            <div class="texto-rodape">
                <i class="fa fa-share" aria-hidden="true"></i> TOTEM
            </div>
        </footer>
      </div>


    </div>
    <!--  fim meu tema -->
