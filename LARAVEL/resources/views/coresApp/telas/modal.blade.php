{!! Html::style("coresApp/css/home.css") !!}
{!! Html::style("coresApp/css/modal.css") !!}     
      <!-- myModal -->
    <div id="myModal" class="modals">
        <!-- Modal content -->
        <div class="modal-content">

            <p>Enviar treino para o TOTEM?</p>
            <table class="tabela">
                <tr>
                    <td>Cancelar</td>
                    <td>
                        <b>OK</b>
                    </td>
                </tr>
            </table>
        </div>


    </div>
    <!--  -->

    <div class="menu_tema " style="display: none">

            <header>
                <div class="row">

                    <div class="col-xs-2">
                        <i class="fa fa-bars menu-bar" aria-hidden="true"></i>
                    </div>
                    <div class="col-xs-8">
                        <h2>
                            Nome da Academia
                        </h2>

                    </div>
                    <div class="col-xs-2">
                        <!-- right -->
                    </div>

                </div>

            </header>

            <div class="conteudo page_background" >
                <div class="conteudo-overlay">

                    <div class="profile-card">

                        <div class="profile_pic">
                            <div class="profile_pic_inner">
                                <div class="change_pic">
                                    <div class="foto_perfil" style="background-image: url('img/user.gif');"></div>
                                </div>
                                <div class="mdfoto">
                                    <i class="fa fa-camera"></i>
                                </div>
                            </div>
                        </div>

                        <div class="profile-name">
                            Aluno Teste

                        </div>

                    </div>
                    <!-- fim profile-card -->

                    <div style="text-align: center;">
                        <img src="img/logo.png" style="width: 60%; margin: auto;" alt="">
                    </div>

                </div>
                <!--  fim conteudo-overlay -->
                <footer>
                    <i class="fa fa-share" aria-hidden="true"></i> TOTEM
                </footer>
            </div>


        </div>
        <!--  fim meu tema -->