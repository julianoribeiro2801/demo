  
  {!! Html::style("coresApp/css/cross.css") !!}

   <div class="menu_tema ">

            <header>
                <div class="row">

                    <div class="col-xs-2">
                        <i class="fa fa-bars menu-bar" aria-hidden="true"></i>
                    </div>
                    <div class="col-xs-8 ">
                        <h2>
                            CROSS TRAINING
                        </h2>

                    </div>
                    <div class="col-xs-2">
                        <!-- right -->
                    </div>

                </div>

            </header>

            <div class="conteudo page_background">
                <div class="conteudo-overlay">

                    <div class="texto_desc fundo-desc-cross">
                        <p>
                            Texto descritivo do treino.
                        </p>    
                        
                    </div>
                   

                    <div class="partiu">
                        #PARTIU
                    </div>

                    <div class="crnmetro">
                        00:00:<span class="zero_min">00</span>
                    </div>

                    <div class="iniciar">
                        INICIAR
                    </div>
                   

                    <!-- inicio da listagem das aulas -->
                    <div id="aulas_cross">
                        <table class="linha_cross_sim">
                              <tr>
                                <td>
                                    <div class="dia_aula"> SAB. </div>
                                </td>
                                <td>
                                    <div class="data_aula">26/05/2018 11:45</div>
                                </td>
                                <td class="al_direito">
                                    <div class="checkicone">
                                        <i class="fa fa-share-alt-square branco" aria-hidden="true"></i>
                                    </div>
                                </td>
                                <td>
                                    <div class="min">
                                        <p>0:48</p>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <table class="linha_cross_nao">
                              <tr>
                                <td>
                                    <div class="dia_aula"> SAB. </div>
                                </td>
                                <td>
                                    <div class="data_aula">26/05/2018 10:45</div>
                                </td>
                                <td class="al_direito">
                                    <div class="checkicone">
                                        <i class="fa fa-share-alt-square branco" aria-hidden="true"></i>
                                    </div>
                                </td>
                                <td>
                                    <div class="min">
                                        <p>2:30</p>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <table class="linha_cross_sim">
                              <tr>
                                <td>
                                    <div class="dia_aula"> SAB. </div>
                                </td>
                                <td>
                                    <div class="data_aula">26/05/2018 09:45</div>
                                </td>
                                <td class="al_direito">
                                    <div class="checkicone">
                                        <i class="fa fa-share-alt-square branco" aria-hidden="true"></i>
                                    </div>
                                </td>
                                <td>
                                    <div class="min">
                                        <p>5:50</p>
                                    </div>
                                </td>
                            </tr>
                        </table>

                        

                     
                    </div>

                </div>
                <!--  fim conteudo-overlay -->

            </div>


        </div>