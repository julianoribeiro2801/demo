     {!! Html::style("coresApp/css/chat.css") !!}
 <div class="menu_tema ">

            <header>
                <div class="row">

                    <div class="col-xs-2">
                        <i class="fa fa-bars menu-bar" aria-hidden="true"></i>
                    </div>
                    <div class="col-xs-8">
                        <h2>
                            RESERVA GINÁSTICA
                        </h2>
                    </div>
                    <div class="col-xs-2">
                        <!-- right -->
                    </div>
                </div>
            </header>

            <div class="conteudo page_background">
                <div class="conteudo-overlay">
                    <div class="fundo">
                        <div class="um texto_chat">

                            <h4>Oi Bruna, tudo certo pra aula de PILATES daqui a pouco às 10:20?</h4>
                            <p style="text-align: right; font-size: 11px;">03/05/2018 - 10:08
                            <img src="{{ url('/')}}/coresApp/img/estrutura/double-check-blue-ios-hi.png" style="width: 12px" alt="">
                            </p>
                        </div>
                        <div class="dois texto_chat">
                            <h4>Ok, confirmado!</h4>
                            <p style="text-align: right; font-size: 11px;">29/05/2018 - 13:40
                            <img src="{{ url('/')}}/coresApp/img/estrutura/double-check-blue-ios-hi.png" style="width: 12px" alt="">
                            </p>
                        </div>
                        <div class="um texto_chat">
                            <h4>Oi Bruna, tudo certo pra aula de PILATES daqui a pouco às 14:00?</h4>
                            <p style="text-align: right; font-size: 11px;">29/05/2018 - 13:30
                            <img src="{{ url('/')}}/coresApp/img/estrutura/double-check-blue-ios-hi.png" style="width: 12px" alt="">
                            </p>
                        </div>
                        <div class="dois texto_chat">
                            <h4>Infelizmente não vou poder ir, por favor cancelar.</h4>
                            <p style="text-align: right; font-size: 11px;">29/05/2018 - 13:40
                            <img src="{{ url('/')}}/coresApp/img/estrutura/double-check-blue-ios-hi.png" style="width: 12px" alt="">
                            </p>
                        </div>
                        <div class="resp  texto_chat">
                            <p>Normalmente responde rápido (até 30 min).</p>
                        </div>
                        <div>
                            <table style="width: 100%;">
                                <tr style="width: 100%;">
                                    <td>
                                        <div class="mens">
                                            <h4>Digite uma mensagem</h4>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="avia">
                                            <i class="fa fa-paper-plane cht" aria-hidden="true"></i>
                                        </div>
                                        
                                    </td>
                                </tr>
                            </table>
                            
                            
                        </div>
                    </div>
                </div>
                <!--  fim conteudo-overlay -->
            </div>
        </div>