    {!! Html::style("coresApp/css/natacao.css") !!}
 <div class="menu_tema ">

            <header>
                <div class="row">

                    <div class="col-xs-2">
                        <i class="fa fa-bars menu-bar" aria-hidden="true"></i>
                    </div>
                    <div class="col-xs-8">
                        <h2>
                            AULA DO DIA
                        </h2>

                    </div>
                    <div class="col-xs-2">
                        <!-- right -->
                    </div>

                </div>

            </header>

            <div class="conteudo page_background">
                <div class="conteudo-overlay">
                    <div id="menu_aulas_gfm">
                        <table class="menu_gmf">
                            <tr>
                                <td class="fundo_diaspm">D</td>
                                <td class="fundo_diaspm">2º</td>
                                <td class="fundo_diaspm">3º</td>
                                <td class="fundo_diaspm">4º</td>
                                <td class="fundo_diaspm">5º</td>
                                <td class="fundo_diaspm">6º</td>
                                <td class="fundo_diaspm" style="border: 0">S</td>
                            </tr>
                        </table>
                    </div>

                    <!-- inicio da listagem das aulas -->
                    <div id="aulas_gfm">
                        <table class="cor_impar_spm">
                            <tr>
                                <td class="esp_img">
                                   <span class="icon-swimming branco" style="font-size: 80px; color: #ff9130;"></span>
                                </td>
                                <td>
                                    <div class="hr_aula textospm"> 13:30 </div>
                                    <div class="nome_aula textospm">CONDICIONAMENTO</div>
                                </td>
                                <td class="al_direito">
                                    <div class="checkicone">
                                        <i class="fa fa-check-circle branco" aria-hidden="true"></i>
                                    </div>
                                    <p  class="textospm">5 VAGAS</p>
                                </td>
                            </tr>
                        </table>

                        <table class="cor_par_spm">
                            <tr>
                                <td class="esp_img">
                                   <span class="icon-swimming branco" style="font-size: 80px; color: #ff9130;"></span>
                                </td>
                                <td>
                                    <div class="hr_aula textospm"> 13:30 </div>
                                    <div class="nome_aula textospm">HIDROGINÁSTICA</div>
                                </td>
                                <td class="al_direito">
                                    <div class="checkicone">
                                        <i class="fa fa-check-circle branco" aria-hidden="true"></i>
                                    </div>
                                    <p class="textospm">5 VAGAS</p>
                                </td>
                            </tr>
                        </table>

                        <table class="cor_impar_spm">
                            <tr>
                                <td class="esp_img">
                                   <span class="icon-swimming branco" style="font-size: 80px; color: #ff9130;"></span>
                                </td>
                                <td>
                                    <div class="hr_aula textospm"> 13:30 </div>
                                    <div class="nome_aula textospm">HIDROGINÁSTICA</div>
                                </td>
                                <td class="al_direito">
                                    <div class="checkicone">
                                        <i class="fa fa-check-circle branco" aria-hidden="true"></i>
                                    </div>
                                    <p class="textospm">5 VAGAS</p>
                                </td>
                            </tr>
                        </table>

                        <table class="cor_par_spm">
                            <tr>
                                <td class="esp_img">
                                   <span class="icon-swimming branco" style="font-size: 80px; color: #ff9130;"></span>
                                </td>
                                <td>
                                    <div class="hr_aula textospm"> 13:30 </div>
                                    <div class="nome_aula textospm">HIDROGINÁSTICA</div>
                                </td>
                                <td class="al_direito">
                                    <div class="checkicone">
                                        <i class="fa fa-check-circle branco" aria-hidden="true"></i>
                                    </div>
                                    <p class="textospm">5 VAGAS</p>
                                </td>
                            </tr>
                        </table>
                    </div>

                </div>
                <!--  fim conteudo-overlay -->

            </div>


        </div>