<div class="col-md-4">
   
     <div class="ibox float-e-margins">
        <div class="ibox-title">
            Cor dos icones
        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <input class="txticonesel form-control" id="txticonesel" name="txticonesel" ng-model="telaSel.corsenha" type="text">
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-primary  btn-block bt-iconesel espaco_btncor">
                            Cor dos icones
                        </button>
                            <input class="btcolor_oculta" name="favcolor3" onchange="testaCor(this.value,'.bt-iconesel','.icon-trophy','color','.txticonesel')" type="color" value="#ff0000"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
         
    </div>
     <div class="ibox float-e-margins">
        <div class="ibox-title">
            Cor fundo icone
        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <input class="txtfundoiconesel form-control" id="txtfundoiconesel" name="txtfundoiconesel" ng-model="telaSel.corsenha" type="text">
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-primary  btn-block bt-fundoiconesel espaco_btncor">
                            
                        </button>
                            <input class="btcolor_oculta" name="favcolor3" onchange="testaCor(this.value,'.bt-fundoiconesel','.trophy_inside','color','.txtfundoiconesel')" type="color" value="#ff0000"/>
                        </div>
                    </div>
              
                </div>

            </div>
        </div>
         
    </div>
     <div class="ibox float-e-margins">
        <div class="ibox-title">
            Cor linha impar
        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <input class="txtcorimparsel form-control" id="txtcorimparsel" name="txtcorimparsel" ng-model="telaSel.corsenha" type="text">
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-primary  btn-block bt-corimparsel espaco_btncor">
                            
                        </button>
                            <input class="btcolor_oculta" name="favcolor3" onchange="testaCor(this.value,'.bt-corimparsel','.cor_linha_impar','backgroundColor','.txtcorimparsel')" type="color" value="#ff0000"/>
                        </div>
                      
                    </div>
                    
                </div>
            </div>
        </div>
         
    </div>    
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            Cor linha par
        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <input class="txtcorparsel form-control" id="txtcorparsel" name="txtcorparsel" ng-model="telaSel.corsenha" type="text">
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-primary  btn-block bt-corparsel espaco_btncor">
                            
                        </button>
                            <input class="btcolor_oculta" name="favcolor3" onchange="testaCor(this.value,'.bt-corparsel','.cor_linha_par','backgroundColor','.txtcorparsel')" type="color" value="#ff0000"/>
                        </div>
                    </div>

                </div>

            </div>
        </div>
         
    </div>        
     <div class="ibox float-e-margins">
        <div class="ibox-title">
            Transparencia
        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                        </div>
                        <div class="col-md-6">
                        </div>
                            <p style="margin-top:30px">
                                Transparência:
                                <span id="slider-value-sel">
                                </span>
                                %
                            </p>
                            <input class="txttranspsel" id="txttranspsel" max="100" min="0" name="txttranspsel" onchange="showValSel(this.value)" type="range" value="80">
                            </input>                            
                    </div>
                    
                </div>
            </div>
        </div>
         
    </div>    

       
</div>






<div class="col-md-4">
   

</div>