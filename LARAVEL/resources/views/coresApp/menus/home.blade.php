<div class="col-md-4">
    {{--
    <div class="ibox float-e-margins">
        <div class="ibox-title back-change">
            <h5>
                Color picker
                <small>
                    http://mjolnic.github.io/bootstrap-colorpicker/
                </small>
            </h5>
            <div class="ibox-tools">
                <a class="collapse-link">
                    <i class="fa fa-chevron-up">
                    </i>
                </a>
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-wrench">
                    </i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li>
                        <a href="#">
                            Config option 1
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            Config option 2
                        </a>
                    </li>
                </ul>
                <a class="close-link">
                    <i class="fa fa-times">
                    </i>
                </a>
            </div>
        </div>
        <div class="ibox-content">
            <h3>
                Colorpicker
            </h3>
            <p>
                Colorpicker plugin for the Twitter Bootstrap toolkit.
            </p>
            <h5>
                As normal input
            </h5>
            <input class="form-control demo1" type="text" value="#5367ce"/>
            <h5>
                As a link
            </h5>
            <a class="btn btn-white btn-block colorpicker-element" data-color="rgb(255, 255, 255)" href="#" id="demo_apidemo">
                Change background color
            </a>
            <br/>
        </div>
    </div>
    <script>
        $(document).ready(function(){

             $('.demo1').colorpicker();

               var divStyle = $('.back-change')[0].style;
            $('#demo_apidemo').colorpicker({
                color: divStyle.backgroundColor
            }).on('changeColor', function(ev) {
                        divStyle.backgroundColor = ev.color.toHex();
                    });

        });
    </script>
    --}}

    {{--fim --}}
    <div class="ibox float-e-margins" style="min-height:158px;">
        <div class="ibox-title">
            Minha marca
        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-9">
                    <img alt="Image" class="img-responsive" ng-if="telaSel.logomarca_home" src="/uploads/logomarcas/@{{telaSel.logomarca_home}}" style="margin:5px auto 10px auto" width="250"> </img>

                    <img alt="" class="img-responsive" ng-if="!telaSel.logomarca_home" src="http://personalclubbrasil.com.br/wp-content/uploads/2016/09/aplicativo-para-academia-personal-club-brasil-e1473375648219.png">
                    </img>
                    
                </div>
                <div class="col-md-3" style="text-align: center;">
                    <div class="alinha_mudar">
                        <button class="btn btn-primary btn-circle ">
                            <i class="fa fa-pencil-square-o">
                            </i>
                        </button>
                        Mudar
                        <input class="btcolor_oculta image" id="image" name="image" ng-model="telaSel.image" onchange="angular.element(this).scope().uploadFile(this.files)" placeholder="Logo Marca" type="file"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            Background
        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <input class="txtcorfundo form-control" id="txtcorfundo" name="txtcorfundo" onblur="testaCor(this.value, '.btn-home', '.conteudo-overlay', 'background-color', '.txtcorfundo')" type="text">
                            </input>


                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-primary btn-block btn-home espaco_btncor" >
                                
                            </button>
                             <input class="btcolor_oculta" id="txtcorfundo" name="txtcorfundo" onchange="testaCor(this.value, '.btn-home', '.conteudo-overlay', 'background-color', '.txtcorfundo')" type="color" value="rgba(255, 5, 5, 0.5)">
                            </input>


                            <p style="margin-top:30px">
                                Transparência:
                                <span id="slider-value">
                                </span>
                                %
                            </p>
                            <input class="txttransparencia" id="txttransparencia" max="100" min="0" name="txttransparencia" onchange="showVal(this.value)" type="range" value="80">
                            </input>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
    <!-- -->
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            Topo
        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <input class="txtcortopo form-control" id="txtcortopo" name="txtcortopo" onblur="testaCor(this.value, '.bt-roxo2', '.menu_tema header', 'backgroundColor', '.txtcortopo')" type="text">
                            </input>
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-primary btn-block espaco_btncor bt-roxo2" >
                            </button>
                             <input class="btcolor_oculta" name="favcolor1" onchange="testaCor(this.value, '.bt-roxo2', '.menu_tema header', 'backgroundColor', '.txtcortopo')" type="color" value="#ff0000"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- -->
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            Rodapé
        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                      {{-- <input class="txtcorrodape form-control" id="txtcorrodape" name="txtcorrodape" 
            onchange="testaCorChange(this.value, '.bt-roxo3', '.menu_tema footer', 'backgroundColor', '.txtcorrodape')" type="text"> --}}
                            <input class="txtcorrodape form-control" id="txtcorrodape" name="txtcorrodape" 
          onkeyup="testaCor(this.value, '.bt-roxo3', '.menu_tema footer', 'backgroundColor', '.txtcorrodape')"  type="text">
                            </input>
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-primary btn-block bt-roxo3 espaco_btncor" >
                            </button>
                            <input class="btcolor_oculta"  
onchange="testaCor(this.value, '.bt-roxo3', '.menu_tema footer', 'backgroundColor', '.txtcorrodape')" type="color" value="#ff0000"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>
<div class="col-md-4">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            Texto Topo
        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <input class="txtcortextotopo form-control" id="txtcortextotopo" name="txtcortextotopo" onblur="testaCorChange(this.value, '.bt-texto-topo', '.academia-name', 'color', '.txtcortextotopo')" type="text">
                            </input>
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-primary btn-block bt-texto-topo espaco_btncor" >
                                Nome Academia
                            </button>
                            <input class="btcolor_oculta" name="favcolor3" onchange="testaCor(this.value, '.bt-texto-topo', '.academia-name', 'color', '.txtcortextotopo')" type="color" value="#ff0000"/>
                        </div>
                    </div>
                </div>
               
            </div>
        </div>
    </div>
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            Texto Nome Aluno
        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <input class="txtnomealuno form-control" id="txtnomealuno" name="txtnomealuno" onblur="testaCorChange(this.value, '.bt-texto-nome', '.profile-name', 'color', '.txtnomealuno')" type="text">
                            </input>
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-primary btn-block bt-texto-nome">
                                Aluno Nome
                            </button>
                             <input class="btcolor_oculta" name="favcolor3" onchange="testaCor(this.value, '.bt-texto-nome', '.profile-name', 'color', '.txtnomealuno')" type="color" value="#ff0000"/>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>


    <div class="ibox float-e-margins">
        <div class="ibox-title">
            Texto Rodapé
        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <input class="txtcortextorodape form-control" id="txtcortextorodape" name="txtcortextorodape" onblur="testaCorChange(this.value, '.bt-texto-rodape', '.texto-rodape', 'color', '.txtcortextorodape')" type="text">
                            </input>
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-primary btn-block  bt-texto-rodape espaco_btncor" >
                                Totem
                            </button>
                             <input class="btcolor_oculta" name="favcolor3" onchange="testaCor(this.value, '.bt-texto-rodape', '.texto-rodape', 'color', '.txtcortextorodape')" type="color" value="#ff0000"/>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>