<div class="col-md-4">
   
     <div class="ibox float-e-margins">
        <div class="ibox-title">
            Cor dos texto 
        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <input class="txttextogfm form-control" id="txttextogfm" name="txttextogfm" ng-model="telaSel.corsenha" type="text">
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-primary  btn-block bt-textogfm espaco_btncor">
                            Cor do texto
                        </button>
                            <input class="btcolor_oculta" name="favcolor3" onchange="testaCor(this.value,'.bt-textogfm','.textogfm','color','.txttextogfm')" type="color" value="#ff0000"/>
                        </div>
                    </div>    
                </div>
            </div>
        </div>
         
    </div>
     <div class="ibox float-e-margins">
        <div class="ibox-title">
            Cor fundo dia da semana 
        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-12">

                    <div class="row">
                        <div class="col-md-6">
                            <input class="txtfundodia form-control" id="txtfundodia" name="txtfundodia" ng-model="telaSel.corsenha" type="text">
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-primary  btn-block bt-fundodia espaco_btncor">
                                 <input class="btcolor_oculta" name="favcolor3" onchange="testaCor(this.value,'.bt-fundodia','.fundo_dia','backgroundColor','.txtfundodia')" type="color" value="#ff0000"/>
                        </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
         
    </div>

     <div class="ibox float-e-margins">
        <div class="ibox-title">
            Cor linha impar
        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-12">

                    <div class="row">
                        <div class="col-md-6">
                            <input class="txtcorimpargfm form-control" id="txtcorimpargfm" name="txtcorimpargfm" ng-model="telaSel.corsenha" type="text">
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-primary  btn-block bt-corimpargfm espaco_btncor">
                            
                            </button>
                                 <input class="btcolor_oculta" name="favcolor3" onchange="testaCor(this.value,'.bt-corimpargfm','.cor_impar_gfm','backgroundColor','.txtcorimpargfm')" type="color" value="#ff0000"/>
                        </div>
                    </div>

                </div>
            </div>
        </div>
         
    </div>    
     <div class="ibox float-e-margins">
        <div class="ibox-title">
            Cor linha par
        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-12">

                    <div class="row">
                        <div class="col-md-6">
                            <input class="txtcorpargfm form-control" id="txtcorpargfm" name="txtcorpargfm" ng-model="telaSel.corsenha" type="text">
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-primary  btn-block bt-corimpargfm espaco_btncor">
                        </button>
                            <input class="btcolor_oculta" name="favcolor3" onchange="testaCor(this.value,'.bt-corpargfm','.cor_par_gfm','backgroundColor','.txtcorpargfm')" type="color" value="#ff0000"/>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>    
     <div class="ibox float-e-margins">
        <div class="ibox-title">
            Cor icones
        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-12">

                    <div class="row">
                        <div class="col-md-6">
                            <input class="txtcoriconegfm form-control" id="txtcoriconegfm" name="txtcoriconegfm" ng-model="telaSel.corsenha" type="text">
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-primary  btn-block bt-coriconegfm espaco_btncor">
                        </button>
                            <input class="btcolor_oculta" name="favcolor3" onchange="testaCor(this.value,'.bt-coriconegfm','.branco','color','.txtcoriconegfm')" type="color" value="#ff0000"/>
                        </div>
                    </div>

                </div>
            </div>
        </div>
         
    </div>       

       
</div>






<div class="col-md-4">
   

</div>