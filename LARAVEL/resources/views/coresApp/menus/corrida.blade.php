<div class="col-md-4">
   
     <div class="ibox float-e-margins">
        <div class="ibox-title">
            Cor texto
        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <input class="txttextotreinocorrida form-control" id="txttextotreinocorrida" name="txttextotreinocorrida" ng-model="telaSel.corsenha" type="text">
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-primary  btn-block bt-txttextotreinocorrida espaco_btncor">
                            
                        </button>
                            <input class="btcolor_oculta" name="favcolor3" onchange="testaCor(this.value,'.bt-txttextotreinocorrida','.geral','color','.txttextotreinocorrida')" type="color" value="#ff0000"/>
                        </div>
                    </div>
                    
                </div>

            </div>
        </div>
    </div>
     <div class="ibox float-e-margins">
        <div class="ibox-title">
            Cor fundo descrição do treino 
        </div>
        <div class="ibox-content">
            
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <input class="txtdesccorrida form-control" id="txtdesccorrida" name="txtdesccorrida" ng-model="telaSel.corsenha" type="text">
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-primary  btn-block bt-fundodesccorrida espaco_btncor">
                            
                        </button>
                            <input class="btcolor_oculta" name="favcolor3" onchange="testaCorChange(this.value,'.bt-fundodesccorrida','.fundo-desc-corrida','backgroundColor','.txtdesccorrida')" type="color" value="#ff0000"/>
                        </div>
                    </div>
                    
                       
                </div>

            </div>
        </div>
    </div>    
  <div class="ibox float-e-margins">
        <div class="ibox-title">
            Cor botão partiu
        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <input class="txtcorbotaopartiu form-control" id="txtcorbotaopartiu" name="txtcorbotaopartiu" ng-model="telaSel.corsenha" type="text">
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-primary  btn-block bt-corbotaopartiu espaco_btncor">
                            Totem
                        </button>
                            <input class="btcolor_oculta" name="favcolor3" <input class="btcolor_oculta" name="favcolor3" onchange="testaCor(this.value,'.bt-corbotaopartiu','.partiu','backgroundColor','.txtcorbotaopartiu')" type="color" value="#ff0000"/>
                        </div>
                    </div>
                
                </div>
            </div>
        </div>
    </div>
  <div class="ibox float-e-margins">
        <div class="ibox-title">
            Cor botão iniciar
        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <input class="txtcorbotaoiniciar form-control" id="txtcorbotaoiniciar" name="txtcorbotaoiniciar" ng-model="telaSel.corsenha" type="text">
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-primary  btn-block bt-corbotaoiniciar espaco_btncor">
                            Totem
                        </button>
                            <input class="btcolor_oculta" name="favcolor3" <input class="btcolor_oculta" name="favcolor3" onchange="testaCor(this.value,'.bt-corbotaoiniciar','.iniciar','backgroundColor','.txtcorbotaoiniciar')" type="color" value="#ff0000"/>
                        </div>
                    </div>
                
                </div>
            </div>
        </div>
    </div>
  <div class="ibox float-e-margins">
        <div class="ibox-title">
            Fundo das linhas ímpares
        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                                                <input class="txtcorridacorsim form-control" id="txtcorridacorsim" name="txtcorridacorsim" ng-model="telaSel.corsenha" type="text">
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-primary  btn-block bt-corrida_corsim espaco_btncor">
                            Totem
                        </button>
                            <input class="btcolor_oculta" name="favcolor3" <input class="btcolor_oculta" name="favcolor3" onchange="testaCor(this.value,'.bt-corrida_corsim','.linha_corrida_sim','backgroundColor','.txtcorridacorsim')" type="color" value="#ff0000"/>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>

    <div class="ibox float-e-margins">
        <div class="ibox-title">
             Fundo das linhas pares
        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <input class="txtcorridacornao form-control" id="txtcorridacornao" name="txtcorridacornao" ng-model="telaSel.corsenha" type="text">
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-primary  btn-block bt-corrida_cornao espaco_btncor">
                            
                        </button>
                            <input class="btcolor_oculta" name="favcolor3" <input class="btcolor_oculta" name="favcolor3" onchange="testaCor(this.value,'.bt-corrida_cornao','.linha_corrida_nao','backgroundColor','.txtcorridacornao')" type="color" value="#ff0000"/>
                        </div>
                    </div>
                
                </div>
            </div>
        </div>
    </div>
       
</div>


<div class="col-md-4">
   

</div>