<div class="col-md-4">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            Cor dos texto
        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <input class="txttextomusc form-control" id="txttextomusc" name="txttextomusc" ng-model="telaSel.corsenha" type="text">
                        </div>
                        <div class="col-md-6">
                                                    <button class="btn btn-primary  btn-block bt-cortextomensagem espaco_btncor">
                        </button>
                            <input class="btcolor_oculta" name="favcolor3" onchange="testaCor(this.value, '.bt-cortextomensagem', '.bicps', 'color', '.txttextomusc')" type="color" value="#ff0000"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            Circulo Treino do Dia 
        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <input class="txtcirculolaranja form-control" id="txtcirculolaranja" name="txtcirculolaranja" ng-model="telaSel.corsenha" type="text">
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-primary  btn-block bt-circulolaranja espaco_btncor">
                        </button>
                            <input class="btcolor_oculta" name="favcolor3" onchange="testaCor(this.value, '.bt-circulolaranja', '.circulo-laranja', 'backgroundColor', '.txtcirculolaranja')" type="color" value="#ff0000"/>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            Circulo Outros Treinos
        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <input class="txtcirculoroxo form-control" id="txtcirculoroxo" name="txtcirculoroxo" ng-model="telaSel.corsenha" type="text">
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-primary  btn-block bt-circuloroxo espaco_btncor">
                        </button>
                                                <input class="btcolor_oculta" name="favcolor3" onchange="testaCor(this.value, '.bt-circuloroxo', '.circulo-roxo', 'backgroundColor', '.txtcirculoroxo')" type="color" value="#ff0000"/>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            Cor botão partiu
        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <input class="txtpartiumusc form-control" id="txtpartiumusc" name="txtpartiumusc" ng-model="telaSel.corsenha" type="text">
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-primary  btn-block bt-partiumusc espaco_btncor">
                            
                        </button>
                            <input <input="" class="btcolor_oculta" name="favcolor3" onchange="testaCor(this.value, '.bt-partiumusc', '.partiumusc', 'backgroundColor', '.txtpartiumusc')" type="color" value="#ff0000"/>
                        </div>
                    </div>
                    
                        
                    </input>
                </div>
            </div>
        </div>
    </div>
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            Cor botão iniciar
        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <input class="txtiniciarmusc form-control" id="txtiniciarmusc" name="txtiniciarmusc" ng-model="telaSel.corsenha" type="text">
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-primary  btn-block bt-iniciarmusc espaco_btncor">
                        </button>
                            <input <input="" class="btcolor_oculta" name="favcolor3" onchange="testaCor(this.value, '.bt-iniciarmusc', '.iniciarmusc', 'backgroundColor', '.txtiniciarmusc')" type="color" value="#ff0000"/>
                        </div>
                    </div>
                    
                        
                    </input>
                </div>

            </div>
        </div>
    </div>
    <div class="ibox float-e-margins">
        
        <div class="ibox-content">
            <p style="margin-top:30px">
                Transparência das linhas:
                <span id="slider-value-musc">
                </span>
                %
            </p>
            <input class="txttranspmusc" id="txttransptreino" max="100" min="0" name="txttranspmusc" onchange="showValMusc(this.value)" type="range" value="80">
            </input>
        </div>
    </div>
</div>
