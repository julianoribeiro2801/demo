<div class="col-md-4">
   
     <div class="ibox float-e-margins">
        <div class="ibox-title">
            Cor texto
        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <input class="txttextotreinociclismo form-control" id="txttextotreinociclismo" name="txttextotreinociclismo" ng-model="telaSel.corsenha" type="text">
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-primary  btn-block bt-treinociclismo espaco_btncor">
                            Cor texto
                        </button>
                            <input class="btcolor_oculta" name="favcolor3" onchange="testaCor(this.value,'.bt-treinociclismo','.texto-cicl','color','.txttextotreinociclismo')" type="color" value="#ff0000"/>
                        </div>
                    </div>
                    
                  
                </div>

            </div>
        </div>
    </div>
     <div class="ibox float-e-margins">
        <div class="ibox-title">
            Cor fundo descrição do treino 
        </div>
        <div class="ibox-content">
            
            <div class="row">
                <div class="col-md-9">
                    <div class="row">
                        <div class="col-md-6">
                            <input class="txtdescciclismo form-control" id="txtdescciclismo" name="txtdescciclismo" ng-model="telaSel.corsenha" type="text">
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-primary  btn-block bt-fundodescciclismo espaco_btncor">
                            
                        </button>
                            <input class="btcolor_oculta" name="favcolor3" onchange="testaCorChange(this.value,'.bt-fundodescciclismo','.fundo-desc-ciclismo','backgroundColor','.txtdescciclismo')" type="color" value="#ff0000"/>
                        </div>
                    </div>
                    
                    
                </div>
            </div>
        </div>
    </div>    
  <div class="ibox float-e-margins">
        <div class="ibox-title">
            Cor botão partiu
        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-9">
                    <div class="row">
                        <div class="col-md-6">
                            <input class="txtcorbotaopartiuciclismo form-control" id="txtcorbotaopartiuciclismo" name="txtcorbotaopartiuciclismo" ng-model="telaSel.corsenha" type="text">
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-primary  btn-block bt-corbotaopartiuciclismo espaco_btncor">
                      
                        </button>
                            <input class="btcolor_oculta" name="favcolor3" <input class="btcolor_oculta" name="favcolor3" onchange="testaCor(this.value,'.bt-corbotaopartiuciclismo','.partiuciclismo','backgroundColor','.txtcorbotaopartiuciclismo')" type="color" value="#ff0000"/>
                        </div>
                    </div>
                    
                       
                </div>

            </div>
        </div>
    </div>
  <div class="ibox float-e-margins">
        <div class="ibox-title">
            Cor botão iniciar
        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <input class="txtcorbotaoiniciarciclismo form-control" id="txtcorbotaoiniciarciclismo" name="txtcorbotaoiniciarciclismo" ng-model="telaSel.corsenha" type="text">
                        </div>
                        <div class="col-md-6">
                             <button class="btn btn-primary  btn-block bt-corbotaoiniciar espaco_btncor">
                           
                        </button>
                            <input class="btcolor_oculta" name="favcolor3" <input class="btcolor_oculta" name="favcolor3" onchange="testaCor(this.value,'.bt-corbotaoiniciar','.iniciar','backgroundColor','.txtcorbotaoiniciarciclismo')" type="color" value="#ff0000"/>
                        </div>
                    </div>
                    
                 
                </div>

            </div>
        </div>
    </div>
  <div class="ibox float-e-margins">
        <div class="ibox-title">
            Fundo das linhas ímpares
        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <input class="txtciclismocorsim form-control" id="txtciclismocorsim" name="txtciclismocorsim" ng-model="telaSel.corsenha" type="text">
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-primary  btn-block bt-ciclismo_corsim espaco_btncor">
                     
                        </button>
                            <input class="btcolor_oculta" name="favcolor3" <input class="btcolor_oculta" name="favcolor3" onchange="testaCor(this.value,'.bt-ciclismo_corsim','.linha_ciclismo_sim','backgroundColor','.txtciclismocorsim')" type="color" value="#ff0000"/>
                        </div>
                    </div>
                
                </div>

            </div>
        </div>
    </div>

    <div class="ibox float-e-margins">
        <div class="ibox-title">
             Fundo das linhas pares
        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <input class="txtciclismocornao form-control" id="txtciclismocornao" name="txtciclismocornao" ng-model="telaSel.corsenha" type="text">
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-primary  btn-block bt-ciclismo_cornao espaco_btncor">
                            
                        </button>
                            <input class="btcolor_oculta" name="favcolor3" <input class="btcolor_oculta" name="favcolor3" onchange="testaCor(this.value,'.bt-ciclismo_cornao','.linha_ciclismo_nao','backgroundColor','.txtciclismocornao')" type="color" value="#ff0000"/>
                        </div>
                    </div>
               
                </div>

            </div>
        </div>
    </div>
       
</div>


<div class="col-md-4">
   

</div>