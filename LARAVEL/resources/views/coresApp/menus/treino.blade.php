<div class="col-md-4">
   
     <div class="ibox float-e-margins">
        <div class="ibox-title">
            Cor dos textos
        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <input class="txttextotreino form-control" id="txttextotreino" name="txttextotreino" ng-model="telaSel.corsenha" type="text">
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-primary  btn-block bt-textotreino espaco_btncor">
                            
                        </button>
                            <input class="btcolor_oculta" name="favcolor3" onchange="testaCor(this.value,'.bt-textotreino','.txt-treino','color','.txttextotreino')" type="color" value="#ff0000"/>
                        </div>
                    </div>
                    
                        
                    </input>
                </div>
            </div>
        </div>
         
    </div>


     <div class="ibox float-e-margins">
        <div class="ibox-title">
            Cor linha impar
        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <input class="txttreinocorsim form-control" id="txttreinocorsim" name="txttreinocorsim" ng-model="telaSel.corsenha" type="text">
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-primary  btn-block bt-treinocorsim espaco_btncor">
                            
                        </button>
                            <input class="btcolor_oculta" name="favcolor3" onchange="testaCor(this.value,'.bt-treinocorsim','.tabb','backgroundColor','.txttreinocorsim')" type="color" value="#ff0000"/>
                        </div>
                    </div>
                    
                </div>

            </div>
        </div>
         
    </div>    
     <div class="ibox float-e-margins">
        <div class="ibox-title">
            Cor linha par
        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                                                <input class="txttreinocornao form-control" id="txttreinocornao" name="txttreinocornao" ng-model="telaSel.corsenha" type="text">
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-primary  btn-block bt-treinocornao espaco_btncor">
                            
                        </button>
                                                <input class="btcolor_oculta" name="favcolor3" onchange="testaCor(this.value,'.bt-treinocornao','.tabbb','backgroundColor','.txttreinocornao')" type="color" value="#ff0000"/>
                        </div>
                    </div>

                </div>

            </div>
        </div>
         
    </div>    
     <div class="ibox float-e-margins">
        <div class="ibox-title">
            Cor linha selecionada
        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                                                <input class="txttreinosel form-control" id="txttreinosel" name="txttreinosel" ng-model="telaSel.corsenha" type="text">
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-primary  btn-block bt-treinosel espaco_btncor">
                            
                        </button>
                                                <input class="btcolor_oculta" name="favcolor3" onchange="testaCor(this.value,'.bt-treinosel','.tab','backgroundColor','.txttreinosel')" type="color" value="#ff0000"/>
                        </div>
                    </div>

                        
                    </input>
                </div>

            </div>
        </div>
         
    </div> 
     <div class="ibox float-e-margins">
        <div class="ibox-title">
            Cor linha selecionada
        </div>
        <div class="ibox-content">
                      <p style="margin-top:30px">Transparência: <span id="slider-value-treino"> </span>% </p>

                    <input type="range" min="0" max="100" value="80"
                        class="txttransptreino" id="txttransptreino" name="txttransptreino"
                            onchange="showValTreino(this.value)" >
    
        </div>
         
    </div>       


       
</div>






<div class="col-md-4">
   

</div>