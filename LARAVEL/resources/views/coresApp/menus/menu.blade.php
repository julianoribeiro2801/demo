<div class="col-md-4">
   
     <div class="ibox float-e-margins">
        <div class="ibox-title">
            Cor dos texto 
        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                      <input class="txttextomenu form-control" id="txttextomenu" name="txttextomenu" onchange="testaCor(this.value,'.bt-textomenu','.textomenu','color','.txttextomenu')" ng-model="telaSel.corsenha" type="text">
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-primary  btn-block bt-textomenu espaco_btncor">
                            Cor dos textos
                        </button>
                         <input class="btcolor_oculta" name="favcolor3" onchange="testaCor(this.value,'.bt-textomenu','.textomenu','color','.txttextomenu')" type="color" value="#ff0000"/>
                        </div>
                    </div>

                        
                    </input>
                </div>
            </div>
        </div>
         
    </div>
     <div class="ibox float-e-margins">
        <div class="ibox-title">
            Cor fundo 
        </div>
        <div class="ibox-content">
            
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <input class="txtfundomenu form-control" id="txtfundomenu" name="txtfundomenu" onchange="testaCor(this.value,'.bt-fundomenu','.barra','backgroundColor','.txtfundomenu')" ng-model="telaSel.corsenha" type="text">
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-primary  btn-block bt-fundomenu espaco_btncor">         
                        </button>
                        <input class="btcolor_oculta" name="favcolor3" onchange="testaCor(this.value,'.bt-fundomenu','.barra','backgroundColor','.txtfundomenu')" type="color" value="#ff0000"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
         
    </div>
     <div class="ibox float-e-margins">
        <div class="ibox-title">
            Cor dos icones
        </div>
        <div class="ibox-content">
            <div class="row">
                
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <input class="txticonemenu form-control" id="txticonemenu" name="txticonemenu" onblur="testaCor(this.value,'.bt-iconemenu','.coment','color','.txticonemenu')" ng-model="telaSel.corsenha" type="text">
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-primary  btn-block bt-iconemenu espaco_btncor">
                            Cor dos icones
                        </button>
                         <input class="btcolor_oculta" name="favcolor3" onchange="testaCor(this.value,'.bt-iconemenu','.coment','color','.txticonemenu')" type="color" value="#ff0000"/>
                        </div>
                    </div>
                    
                        
                    </input>
                </div>

            </div>
        </div>
         
    </div>    
       
</div>

