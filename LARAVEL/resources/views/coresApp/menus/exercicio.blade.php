<div class="col-md-4">
   
     <div class="ibox float-e-margins">
        <div class="ibox-title">
            Cor dos texto 
        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <input class="txttextoexer form-control" id="txttextoexer" name="txttextoexer" onchange="testaCor(this.value,'.bt-textoexer,'.textoexer','color','.txttextoexer')" ng-model="telaSel.corsenha" type="text">
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-primary  btn-block bt-textoexer espaco_btncor">
                            Cor dos textos
                        </button>
                            <input class="btcolor_oculta" name="favcolor3" onchange="testaCor(this.value,'.bt-textoexer','.textoexer','color','.txttextoexer')" type="color" value="#ff0000"/>
                        </div>
                    </div>
                  
                </div>
            </div>
        </div>
         
    </div>
     <div class="ibox float-e-margins">
        <div class="ibox-title">
            Cor fundo 
        </div>
        <div class="ibox-content">
            
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <input class="txtfundoexer form-control" id="txtfundoexer" name="txtfundoexer" onchange="testaCor(this.value,'.bt-fundomenu','.barra','backgroundColor','.txtfundoexer')" ng-model="telaSel.corsenha" type="text">
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-primary  btn-block bt-fundomenu espaco_btncor">
                            
                        </button>
                            <input class="btcolor_oculta" name="favcolor3" onchange="testaCor(this.value,'.bt-fundomenu','.barra','backgroundColor','.txtfundoexer')" type="color" value="#ff0000"/>
                        </div>
                    </div>
                   
                </div>
            </div>
        </div>
         
    </div>
     <div class="ibox float-e-margins">
        <div class="ibox-title">
            Cor linha impar
        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <input class="txtcorimparexer form-control" id="txtcorimparexer" name="txtcorimparexer" ng-model="telaSel.corsenha" type="text">
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-primary  btn-block bt-corimparexer espaco_btncor">
                            
                        </button>
                            <input class="btcolor_oculta" name="favcolor3" onchange="testaCor(this.value,'.bt-corimparexer','.cor_impar_exer','backgroundColor','.txtcorimparexer')" type="color" value="#ff0000"/>
                        </div>
                    </div>
               
                </div>

            </div>
        </div>
         
    </div>    
     <div class="ibox float-e-margins">
        <div class="ibox-title">
            Cor linha par
        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <input class="txtcorparexer form-control" id="txtcorparexer" name="txtcorparexer" ng-model="telaSel.corsenha" type="text">
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-primary  btn-block bt-corparexer espaco_btncor">
                            
                        </button>
                            <input class="btcolor_oculta" name="favcolor3" onchange="testaCor(this.value,'.bt-corparexer','.cor_par_exer','backgroundColor','.txtcorparexer')" type="color" value="#ff0000"/>
                        </div>
                    </div>

                </div>

            </div>
        </div>
         
    </div>    
       
</div>






<div class="col-md-4">
   

</div>