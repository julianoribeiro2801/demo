<div class="col-md-4">
   
     <div class="ibox float-e-margins">
        <div class="ibox-title">
            Cor dos texto 
        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <input class="txttextoclube form-control" id="txttextoclube" name="txttextoclube" ng-model="telaSel.corsenha" type="text">
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-primary  btn-block bt-textoclube espaco_btncor">
                             Cor dos textos 
                        </button>
                            <input class="btcolor_oculta" name="favcolor3" onchange="testaCor(this.value,'.bt-textoclube','.empresas','color','.txttextoclube')" type="color" value="#ff0000"/>
                        </div>
                    </div>
                    
             
                </div>

            </div>
        </div>
         
    </div>
     <div class="ibox float-e-margins">
        <div class="ibox-title">
            Cor cabeçalho
        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <input class="txtcorcabclube form-control" id="txtcorcabclube" name="txtcorcabclube" ng-model="telaSel.corsenha" type="text">
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-primary  btn-block bt-corcabclube espaco_btncor">
                            
                        </button>
                            <input class="btcolor_oculta" name="favcolor3" onchange="testaCor(this.value,'.bt-corcabclube','.clube_vantagens','backgroundColor','.txtcorcabclube')" type="color" value="#ff0000"/>
                        </div>
                    </div>
                    
                      
                </div>
            </div>
        </div>
         
    </div> 

     <div class="ibox float-e-margins">
        <div class="ibox-title">
            Cor linha impar
        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <input class="txtcorimparclube form-control" id="txtcorimparclube" name="txtcorimparclube" ng-model="telaSel.corsenha" type="text">
                        </div>
                        <div class="col-md-6">
                                                    <button class="btn btn-primary  btn-block bt-corimparclube espaco_btncor">
                            
                        </button>
                            <input class="btcolor_oculta" name="favcolor3" onchange="testaCor(this.value,'.bt-corimparclube','.cor_impar_clube','backgroundColor','.txtcorimparclube')" type="color" value="#ff0000"/>
                        </div>
                    </div>
             
                </div>
            </div>
        </div>
         
    </div>    
     <div class="ibox float-e-margins">
        <div class="ibox-title">
            Cor linha par
        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                             <input class="txtcorparclube form-control" id="txtcorparclube" name="txtcorparclube" ng-model="telaSel.corsenha" type="text">
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-primary  btn-block bt-corparclube espaco_btncor">
                            
                        </button>
                            <input class="btcolor_oculta" name="favcolor3" onchange="testaCor(this.value,'.bt-corparclube','.cor_par_clube','backgroundColor','.txtcorparclube')" type="color" value="#ff0000"/>
                        </div>
                    </div>
             
                </div>

            </div>
        </div>
    </div>    

       
</div>






<div class="col-md-4">
   

</div>