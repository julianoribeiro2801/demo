<div class="col-md-4">

    <div class="ibox float-e-margins">
        <div class="ibox-title">
            Cor dos texto 
        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-12">

                    <div class="row">
                        <div class="col-md-6">
                            <input class="form-control txttextomensagem" id="txttextomensagem" name="txttextomensagem" ng-model="telaSel.corsenha" type="text" onchange="testaCorChange(this.value, '.bt-cortextomensagem', '.texto_msg', 'color', '.txttextomensagem')">

                            </input>
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-primary  btn-block bt-cortextomensagem espaco_btncor" >
                                Cor do texto
                            </button>
                               <input class="btcolor_oculta" name="favcolor3" onchange="testaCor(this.value, '.bt-cortextomensagem', '.texto_msg', 'color', '.txttextomensagem')" type="color" value="#ff0000"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="ibox float-e-margins">
        <div class="ibox-title">
            Fundo das linhas ímpares
        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-12">

                    <div class="row">
                        <div class="col-md-6">
                            <input class="txtcorsim form-control" id="txtcorsim" name="txtcorsim" ng-model="telaSel.corsenha" type="text">
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-primary  btn-block bt-corsim espaco_btncor" >

                            </button>
                                                    <input class="btcolor_oculta" name="favcolor3" <input class="btcolor_oculta" name="favcolor3" onchange="testaCor(this.value, '.bt-corsim', '.linha_corsim', 'backgroundColor', '.txtcorsim')" type="color" value="#ff0000"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="ibox float-e-margins">
        <div class="ibox-title">
            Fundo das linhas pares
        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <input class="txtcornao form-control" id="txtcornao" name="txtcornao" ng-model="telaSel.corsenha" type="text">
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-primary  btn-block bt-cornao espaco_btncor"  ></button>
                                                    <input class="btcolor_oculta" name="favcolor3" <input class="btcolor_oculta" name="favcolor3" onchange="testaCor(this.value, '.bt-cornao', '.linha_cornao', 'backgroundColor', '.txtcornao')" type="color" value="#ff0000"/>
                        </div>
                      
                      
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>


<div class="col-md-4">


</div>