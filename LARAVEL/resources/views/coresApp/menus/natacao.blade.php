<div class="col-md-4">
   
     <div class="ibox float-e-margins">
        <div class="ibox-title">
            Cor dos texto 
        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <input class="txttextospm form-control" id="txttextospm" name="txttextospm" ng-model="telaSel.corsenha" type="text">
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-primary  btn-block bt-textospm espaco_btncor">
                            Cor do texto
                        </button>
                            <input class="btcolor_oculta" name="favcolor3" onchange="testaCor(this.value,'.bt-textospm','.textospm','color','.txttextospm')" type="color" value="#ff0000"/>
                        </div>
                    </div>
                    
                        
                    </input>
                </div>

            </div>
        </div>
         
    </div>
     <div class="ibox float-e-margins">
        <div class="ibox-title">
            Cor fundo dia da semana 
        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <input class="txtfundodiaspm form-control" id="txtfundodiaspm" name="txtfundodiaspm" ng-model="telaSel.corsenha" type="text">
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-primary  btn-block bt-fundodiaspm espaco_btncor">
                            
                        </button>
                            <input class="btcolor_oculta" name="favcolor3" onchange="testaCor(this.value,'.bt-fundodiaspm','.fundo_diaspm','backgroundColor','.txtfundodiaspm')" type="color" value="#ff0000"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
         
    </div>

     <div class="ibox float-e-margins">
        <div class="ibox-title">
            Cor linha impar
        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <input class="txtcorimparspm form-control" id="txtcorimparspm" name="txtcorimparspm" ng-model="telaSel.corsenha" type="text">
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-primary  btn-block bt-corimparspm espaco_btncor">
                            
                        </button>
                            <input class="btcolor_oculta" name="favcolor3" onchange="testaCor(this.value,'.bt-corimparspm','.cor_impar_spm','backgroundColor','.txtcorimparspm')" type="color" value="#ff0000"/>
                        </div>
                    </div>
                    
                        
                    </input>
                </div>
            </div>
        </div>
         
    </div>    
     <div class="ibox float-e-margins">
        <div class="ibox-title">
            Cor linha par
        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-12">
                     <div class="row">
                        <div class="col-md-6">
                            <input class="txtcorparspm form-control" id="txtcorparspm" name="txtcorparspm" ng-model="telaSel.corsenha" type="text">
                        </div>
                        <div class="col-md-6">
                                                    <button class="btn btn-primary  btn-block bt-corparspm espaco_btncor">
                            
                        </button>
                            <input class="btcolor_oculta" name="favcolor3" onchange="testaCor(this.value,'.bt-corparspm','.cor_par_spm','backgroundColor','.txtcorparspm')" type="color" value="#ff0000"/>
                        </div>
                    </div>

                    </input>
                </div>
 
            </div>
        </div>
         
    </div>  
     <div class="ibox float-e-margins">
        <div class="ibox-title">
            Cor icones
        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-12">

                    <div class="row">
                        <div class="col-md-6">
                            <input class="txtcoriconespm form-control" id="txtcoriconespm" name="txtcoriconespm" ng-model="telaSel.corsenha" type="text">
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-primary  btn-block bt-coriconespm espaco_btncor">
                        </button>
                            <input class="btcolor_oculta" name="favcolor3" onchange="testaCor(this.value,'.bt-coriconespm','.branco','color','.txtcoriconespm')" type="color" value="#ff0000"/>
                        </div>
                    </div>

                </div>
            </div>
        </div>
         
    </div>      

       
</div>






<div class="col-md-4">
   

</div>