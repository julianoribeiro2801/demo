
<style>
    .bg-roxo{
background-color: #443752;
border: 0px solid;
pointer-events: none;
}
.bg-roxo:hover{
background-color: #443752;
border: 0px solid;
pointer-events: none;
}
.mt-30{
margin-top: 30px;
}
.mb-30{
margin-bottom: 30px;
}
</style>
<style>
    .alinha_mudar {
position:relative; display:inline-block
}
.btcolor_oculta {
opacity:0; position:absolute; left:0;top:0;width:100%
}
</style>
<script>
    var tela_atual = '{{$tela}}';
</script>
@push('scripts')
{!! Html::script("js/temasController.js") !!}
@endpush
<div ng-controller="temasController">
    <div style="margin-top: 20px">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label>
                        Selecione o tema
                    </label>
                    <select class="form-control" name="tema" ng-change="changeTema(tema_id)" ng-model="tema_id">
                        <option value="">
                            Selecione o tema
                        </option>
                        <option ng-repeat="tema in temas" ng-value="tema.id">
                            @{{tema.nome}} / @{{tema.id}}
                        </option>
                    </select>
                </div>
            </div>
            <div class="col-md-2">
            </div>
            <div class="col-md-3">
                <button class="btn btn-default btn-rounded btn-block" ng-click="gravarTema(tela.id, tela.nometela,tema_id)">
                    <i class="fa fa-check">
                    </i>
                    Salvar meu tema
                </button>
            </div>
            <div class="col-md-3" ng-hide="tema_id!==2">
                <button class="btn btn-primary btn-rounded btn-block" ng-click="gravarTema(tela.id, selectedItem,tema_id)">
                    <i class="fa fa-check">
                    </i>
                    Salvar e publicar
                </button>
            </div>
        </div>
        <!-- salvar -->
        <!-- inicio do tema -->
        <div class="row" style="margin-top: 20px" ng-hide="tema_id!==2">
            <div class="col-md-4">
                <input class="form-group" id="qualTela" type="hidden" value="{{$tela}}">
                    <div class="form-group">
                        <label>
                            Selecione a Tela
                        </label>
                        <!--  ng-selected="14 == tela.id" -->
                        <select class="select2_demo_1 form-control" ng-change="changeTela(telset)" ng-model="telset" 
                        ng-options="tela as tela.nometela for tela  in telas">
                            <option value="">
                                Selecione a Tela
                            </option>
                        </select>
                    </div>
                    {!! Html::style("coresApp/css/geral.css") !!}
                @if($tela == 'Home')
                    <!-- home.json -->
                    @include('coresApp.telas.home')
                @endif
                @if($tela == 'Mensagem')
    
                    @include('coresApp.telas.mensagem')
                @endif
                @if($tela == 'Gfm')
    
                    @include('coresApp.telas.gfm')
                @endif
                @if($tela == 'Natação')
    
                    @include('coresApp.telas.natacao')
                @endif
                @if($tela == 'Cross')
    
                    @include('coresApp.telas.cross')
                @endif
                @if($tela == 'Musculação')
    
                    @include('coresApp.telas.musculacao')
                @endif
                @if($tela == 'Clube')
    
                    @include('coresApp.telas.clubedevantagens')
                @endif
                @if($tela == 'Menu')
    
                    @include('coresApp.telas.menu')
                @endif
                @if($tela == 'Treino')
    
                    @include('coresApp.telas.treino')
                @endif
                @if($tela == 'Ciclismo')
    
                    @include('coresApp.telas.ciclismo')
                @endif
                @if($tela == 'Chat')
    
                    @include('coresApp.telas.chat')
                @endif
                @if($tela == 'Contato')
    
                    @include('coresApp.telas.contato')
                @endif
                @if($tela == 'Reserva')
    
                    @include('coresApp.telas.reserva')
                @endif
                @if($tela == 'Empresa')
    
                    @include('coresApp.telas.empresa')
                @endif

                @if($tela == 'Selecionar_Treino')
    
                    @include('coresApp.telas.seleciona_treino')
                @endif
                 @if($tela == 'Corrida')
    
                    @include('coresApp.telas.corrida')
                @endif
                @if($tela == 'Coach')
    
                    @include('coresApp.telas.coach')
                @endif

             @if($tela == 'Planejamento')
                @include('coresApp.telas.planejamento')
            @endif
            
            @if($tela == 'Exercicio')
                @include('coresApp.telas.exercicio')
            @endif
            
            @if($tela == 'Conquistas')
                @include('coresApp.telas.conquistas')
            @endif
            
            @if($tela == 'Modal')
                @include('coresApp.telas.modal')
            @endif
          
               
            </div>



            @if($tela == 'Home')
            @include('coresApp.menus.home')
            @endif
            @if($tela =='Mensagem')
            @include('coresApp.menus.mensagem')
            @endif
            @if($tela == 'Gfm')
            @include('coresApp.menus.gfm')
            @endif
            @if($tela == 'Natação')
            @include('coresApp.menus.natacao')
            @endif
            @if($tela == 'Cross')
            @include('coresApp.menus.cross')
            @endif
            @if($tela == 'Musculação')
                @include('coresApp.menus.musculacao')
            @endif
            @if($tela == 'Clube')
            @include('coresApp.menus.clubedevantagens')
            @endif
            @if($tela == 'Menu')
            @include('coresApp.menus.menu')
            @endif
            @if($tela == 'Treino')
            @include('coresApp.menus.treino')
            @endif
            @if($tela == 'Ciclismo')
            @include('coresApp.menus.ciclismo')
            @endif
            @if($tela == 'Corrida')
            @include('coresApp.menus.corrida')
            @endif
            @if($tela == 'Chat')
            @include('coresApp.menus.chat')
            @endif
            @if($tela == 'Contato')
                @include('coresApp.menus.contato')
            @endif
            @if($tela == 'Reserva')
                @include('coresApp.menus.reserva')
            @endif
            @if($tela == 'Empresa')
                @include('coresApp.menus.empresa')
            @endif

            @if($tela == 'Selecionar_Treino')
                @include('coresApp.menus.seleciona_treino')
            @endif


            @if($tela == 'Coach')
                @include('coresApp.menus.coach')
            @endif
    
            @if($tela == 'Planejamento')
                @include('coresApp.menus.planejamento')
            @endif
            
            @if($tela == 'Exercicio')
                @include('coresApp.menus.exercicio')
            @endif

            @if($tela == 'Conquistas')
                @include('coresApp.menus.conquistas')
            @endif

            @if($tela == 'Modal')
                @include('coresApp.menus.modal')
            @endif
        </div>
        <!-- fim row-->
    </div>
</div>