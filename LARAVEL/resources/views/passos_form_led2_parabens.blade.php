@extends('layouts.app')
@section('content')


@push('scripts')

{!! Html::script("tema_assets/js/plugins/steps/jquery.steps.min.js") !!}
{!! Html::script("tema_assets/js/plugins/validate/jquery.validate.min.js") !!}


{!! Html::script("js/passosController.js") !!}
@endpush
<!--  wizard -->
{!! Html::style("tema_assets/css/plugins/steps/jquery.steps.css") !!}

<div class="wrapper wrapper-content" ng-controller="passosParabensController">
    <div class="ibox-content">
        <h2>
            Personalizar meu Aplicativo!
        </h2>
        <p>
            Complete seus dados para iniciar o teste com o aplicativo.
        </p>

          

        <div class="wizard">
            <div class="steps clearfix">
                <ul role="tablist">
                    <li aria-disabled="false" aria-selected="false" class="first done" role="tab">
                        <a aria-controls="form-p-0" href="#form-h-0" id="form-t-0">
                            <span class="number">
                                1.
                            </span>
                            Meus Dados
                        </a>
                    </li>
                    <li aria-disabled="false" aria-selected="true" class="first done" role="tab">
                        <a aria-controls="form-p-1" href="#form-h-1" id="form-t-1">
                            <span class="current-info audible">
                                current step:
                            </span>
                            <span class="number">
                                2.
                            </span>
                            Personaliar App
                        </a>
                    </li>
                    <li aria-disabled="true" class="current" role="tab">
                        <a aria-controls="form-p-2" href="#form-h-2" id="form-t-2">
                            <span class="number">
                                3.
                            </span>
                            Parabéns!
                        </a>
                    </li>
                </ul>
            </div>
        </div>

       <div class="row">
            <div class="text-center" style="margin-top: 70px">

                <h2 style="font-size: 42px">
                    Parabéns!
                </h2>
                <p style="font-size: 20px">
                    Agora é só baixar o App
                    <br>
                        e fazer login com seu úsuario e senha!
                    </br>
                </p>



                <div class="text-center visible-xs">
                    <a href="https://play.google.com/store/apps/details?id=br.aquafit.cliques" style=" margin-right: 20px;" target="_blank">
                        <img alt="" src="{{ asset('icones/google-play-badge.png') }}" style="width: 180px">
                        </img>
                    </a>
                    <a href="https://itunes.apple.com/us/app/personalcb/id1290866858?ls=1&mt=8" target="_blank">
                        <img alt="" src="{{ asset('icones/app-store-badge.png') }}" style="width: 180px;     margin-top: -100px;">
                        </img>
                    </a>
                </div>



                <div class="text-center bts_google hidden-xs">
                    <a href="https://play.google.com/store/apps/details?id=br.aquafit.cliques" style=" margin-right: 20px;" target="_blank">
                        <img alt="" src="{{ asset('icones/google-play-badge.png') }}" style="width: 180px">
                        </img>
                    </a>
                    <a href="https://itunes.apple.com/us/app/personalcb/id1290866858?ls=1&mt=8" target="_blank">
                         <img alt="" src="{{ asset('icones/app-store-badge.png') }}" style="width: 180px;     margin-top: 0;">
                        </img>
                    </a>
                </div>
            </div>
        
        </div>


        {{--  inicio do confirme seus dados --}}
        <div class="row">
            <div class="col-md-6">
            </div>
            <div class="col-md-3">
                <a class="btn btn-primary btn-block btn-pr " href="/admin/coresApp" ng-click="gravarTema(tela.id, tela.nometela,tema_id)">
                    <i class="fa fa-chevron-left">
                    </i>
                    Anterior
                </a>
            </div>
            <div class="col-md-3">
                <div class="visible-xs" style="margin-top:10px">
                </div>
                <a class="btn btn-primary btn-block btn-pr " href="/admin">
                    Ir para o Painel de Controle
                    <i class="fa fa-chevron-right">
                    </i>
                </a>
            </div>
        </div>
        <hr>
            {{--  fim do confirme seus dados --}}
        </hr>
    </div>
</div>


<style>
.btn-pr {
    color: #fff;
    background-color: #35b729;
    border-color: #35b729;
}
.btn-pr:active {
    color: #fff;
    background-color: #34c726 !important;
    border-color: #35b729 !important;
}
.btn-pr:hover {
    color: #fff;
    background-color: #34c726 !important;
    border-color: #35b729 !important;
}
   
.btn-pr:focus, .btn-pr.focus {
    color: #fff;
    background-color: #35b729 !important;
    border-color: #35b729 !important;
}
</style>

<style>
    .bts_google{
            margin-left: 34%;
    }
     .bts_google a{ 
            display: inline;
            float: left;
        }

.wizard-big.wizard > .content {
    min-height: 500px;
}
</style>
@endsection
