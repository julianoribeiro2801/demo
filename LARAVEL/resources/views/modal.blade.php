    <div aria-hidden="true" class="modal inmodal fade" id="addCliente" role="dialog" tabindex="-1" >
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                    <h4 class="modal-title">Adicionar Cliente </h4>
                </div>
                <div class="modal-body">
                    <span style="display:none;"><input type="text" ng-model="cliente.spmId"></span>
                    {{-- < p > {{ msgAddCliente}}</p>--}}
                    <div class="form-group">

                        <label>QTD Alunos:</label>
                        <input type="number" name="name" id="inputID" ng-model="qt_alunos" class="form-control" ng-value="@{{listareservas.length}}" title=""
                               required="required" style="    font-size: 28px;
                               width: 95px;
                               text-align: center;
                               margin-bottom: 18px;
                               padding: 21px;">

                        <label>Vagas disp:</label>
                        <input type="number" name="name" ng-disabled="true" ng-model="qt_vagas" id="inputIDvagas" class="form-control" value="10" title=""
                               required="required" style="    font-size: 28px;
                               width: 95px;
                               text-align: center;
                               margin-bottom: 18px;
                               padding: 21px;">                                                
                        <label>@{{professor}}</label>
                        <label> - @{{aula}}</label>
                        <label> - @{{horario}}</label>
                        <label> - @{{dt_aula}}</label>
                        <select ng-model="aula_atual" ng-change="getListareserva(aula_atual)" class="select2_demo_1 form-control">
                            <option ng-repeat="aulaant in aulasant"   ng-value="aulaant.id">@{{aulaant.dataAula}}</option>  
                        </select> 
                        <hr>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Nome</th>
                                    <th>Ação</th>
                                    <th>Ação</th>
                                </tr>
                            </thead>
                            <tbody>

                                {{-- < tr ng - repeat = "inscricao in incricoes" > --}}
                                <tr ng-repeat="lista in listareservas">
                                    <td width="50"> <img  src="{{ url('/')}}/uploads/avatars/user-@{{lista.id}}.jpg" alt="" class="chat-avatar img-circle img-circle" style="width: 25px; height: 25px;"></td>
                                        <!--<td>@{{lista.name}}</td>-->
                                    <td>
                                        <select class="form-control" ng-model="lista.id" data-placeholder="Selecione um cliente..." tabindex="2">
                                            <option ng-value="@{{lista.id}}">@{{lista.name}}</option>
                                            <option ng-repeat="client in clients" ng-value="@{{client.id}}">@{{client.name}}</option>

                                        </select>
                                    </td>    
                                    <td><i class="fa fa-save" aria-hidden="true" ng-click="reservaAdd(aulaSelect,lista.id)" ng-disabled="saveGfmBtn"></i></td>
                                    
                                    <td><i ng-click="reservaDel(aulaSelect, lista.id)" class="fa fa-trash" aria-hidden="true" ></i></td>
                                </tr>
                            </tbody>
                        </table>
                        <button type="button" ng-show="qt_vagas>0" ng-click="addAlunoAula()" class="btn btn-primary"><i class="fa fa-check"></i> Adicionar</button>
                        <button ng-disabled="true" ng-show="qt_vagas==0" type="button" ng-click="addAlunoAula()" class="btn btn-danger"><i class="fa fa-check"></i>Lotada</button>
                        
                    </div>
                </div>
                <!--<div class="modal-footer">
                    <button type="button" ng-click="addReserva(aulaSelect, cliente.clienteId)" class="btn btn-primary"><i class="fa fa-check"></i> Adicionar</button>
                    <button type="button" ng-click="validarVagas(qt_alunos, qt_vagas)" class="btn btn-primary"><i class="fa fa-check"></i> Adicionar anonimos</button>
                </div>-->
            </div>
        </div>
    </div>
