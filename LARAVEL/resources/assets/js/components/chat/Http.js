window.axios = require('axios')

axios.defaults.headers.common['unity'] = $('meta[name="unidade"]').attr('content');
axios.defaults.headers.common['user'] = $('meta[name="user_logued"]').attr('content');
axios.defaults.baseURL = '/api/chat/';


export function unity(idunidade) {
    return axios.get(`unity`,{
        headers :{
            unity: idunidade
        }
    })
}

export function findPersonMessages(idPerson) {
    return axios.get(`person/${idPerson}/messages`)
}

export function checkNewsUserMessages(idPerson) {
    return axios.get(`person/${idPerson}/news`)
}

export function setRead(message) {
    return axios.put(`messages/read`, {message: message})
}

export function sendMessage(message, person) {
    return axios.post(`messages`, {message: message, person: person})
}