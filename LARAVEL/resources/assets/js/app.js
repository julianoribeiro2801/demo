require('./bootstrap');

import 'datatables.net'
import 'moment'


window.moment = moment
moment.locale('pt-br')

const $datatables = $('table.datatable')

$( document ).ready(() => {
    activeDataTables()
})

function activeDataTables() {
    let options = {
        responsive: true
    }

    $datatables.dataTable(options)
}
