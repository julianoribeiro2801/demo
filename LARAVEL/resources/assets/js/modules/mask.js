$.ready(function(){
    $('body [data-mask-date]').mask('00/00/0000');
    $('body [data-mask-time]').mask('00:00:00');
    $('body [data-mask-date_time]').mask('00/00/0000 00:00:00');
    $('body [data-mask-cep]').mask('00000-000');
    $('body [data-mask-cpf]').mask('000.000.000-00', {reverse: true});

    var SPMaskBehavior = function (val) {
            return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
        },
        spOptions = {
            onKeyPress: function(val, e, field, options) {
                field.mask(SPMaskBehavior.apply({}, arguments), options);
            }
        };

    $('[data-mask-phone]').mask(SPMaskBehavior, spOptions);
})