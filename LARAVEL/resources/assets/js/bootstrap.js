
window._ = require('lodash');

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

window.$ = window.jQuery = require('jquery');
window.moment = require('moment')
require('angular')
require('angular-input-masks/br')
require('bootstrap-sass');
require('angular-material');
require('ng-file-upload');
require('sweetalert');
window.toastr = require('toastr')
require('./modules/messages')


moment.locale('pt-br');