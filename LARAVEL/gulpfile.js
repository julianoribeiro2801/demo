const elixir = require('laravel-elixir');

require('laravel-elixir-vue-2');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */
 
 // mix.copy('node_modules/sweetalert/dist/sweetalert.css','./public/dist/sweetalert.css')
 
elixir(mix => {
    mix.sass(['app.scss'], './public/dist/app.css')
        .webpack('app.js')
        .webpack('chat.js', './public/dist/chat.js')
});
