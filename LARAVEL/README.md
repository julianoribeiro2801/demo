# README #

## CHAT API ##

BASE URL = sitemapcb.com.br/api/chat

### Autenticação
    todas as chamadas devem conter no HEADER
    user = id do usuario
    unity = id da unidade
    
## Usuarios
  ### Alunos
    Metodo: GET
    Url: /unity
    Json de retorno:
      {
          "data": [
              {
                  "id": 1,
                  "logo": "1506102480-seloqualidade_corel11_aquafit.png",
                  "fantasia": "Aquafit",
                  "persons": {
                      "data": [
                          {
                              "id": 1,
                              "name": "KarstenAdmin",
                              "email": "ruiz@7cliques.com.br",
                              "avatar": "http://laravel.testuser-a4.jpg",
                              "role": "admin",
                              "new_message": 0
                          }...
                      ]
                  }
              }
          ]
      }

### Professores
    Metodo: GET
    Url: /unity/teachers
    Json de retorno:
      {
          "data": [
              {
                  "id": 1,
                  "logo": "1506102480-seloqualidade_corel11_aquafit.png",
                  "fantasia": "Aquafit",
                  "persons": {
                      "data": [
                          {
                              "id": 1,
                              "name": "KarstenAdmin",
                              "email": "ruiz@7cliques.com.br",
                              "avatar": "http://laravel.testuser-a4.jpg",
                              "role": "admin",
                              "new_message": 0
                          }...
                      ]
                  }
              }
          ]
      }

    
### Recuperar mensagens de um usuario
        Metodo: GET
        Url: /person/{person}/messages // passando id do usuario como parametro
        Json de retorno:
        {
            "data": [
                {
                    "id": 118,
                    "sender_id": 193,
                    "receiver_id": 74,
                    "message": "jhgg",
                    "active": true,
                    "opened": true,
                    "created_at": {
                        "date": "2017-11-30 14:14:29.000000",
                        "timezone_type": 3,
                        "timezone": "America/Sao_Paulo"
                    },
                    "updated_at": {
                        "date": "2017-11-30 14:14:29.000000",
                        "timezone_type": 3,
                        "timezone": "America/Sao_Paulo"
                    },
                    "sender": {
                        "id": 193,
                        "idunidade": 3,
                        "name": "Joao das coves",
                        "email": "joaocoves@mail.com",
                        "role": "funcionario",
                        "remember_token": null,
                        "device_id": "",
                        "created_at": "2017-10-16 21:20:47",
                        "updated_at": "2017-11-30 04:46:11",
                        "avatar": "user-a4.jpg"
                    },
                    "receiver": {
                        "id": 74,
                        "idunidade": 3,
                        "name": "Jeandro Couto",
                        "email": "jeandro.couto@gmail.com.br",
                        "role": "admin",
                        "remember_token": "VxajOUhCldN4diduPybf7vNQu8VGO2IHiSkonqd4omUykTkJcj1t9CvO7faE",
                        "device_id": "",
                        "created_at": "2017-05-15 22:48:21",
                        "updated_at": "2017-11-07 16:51:15",
                        "avatar": "user-a4.jpg"
                    }
                },
            } 
            
### Vereficar se existem novass mensagens de um usuario
        Metodo: GET
        Url: /person/{person}/news // passando id do usuario como parametro
        Json de retorno:
        {
            "data": [
                {
                    "id": 118,
                    "sender_id": 193,
                    "receiver_id": 74,
                    "message": "jhgg",
                    "active": true,
                    "opened": true,
                    "created_at": {
                        "date": "2017-11-30 14:14:29.000000",
                        "timezone_type": 3,
                        "timezone": "America/Sao_Paulo"
                    },
                    "updated_at": {
                        "date": "2017-11-30 14:14:29.000000",
                        "timezone_type": 3,
                        "timezone": "America/Sao_Paulo"
                    },
                    "sender": {
                        "id": 193,
                        "idunidade": 3,
                        "name": "Joao das coves",
                        "email": "joaocoves@mail.com",
                        "role": "funcionario",
                        "remember_token": null,
                        "device_id": "",
                        "created_at": "2017-10-16 21:20:47",
                        "updated_at": "2017-11-30 04:46:11",
                        "avatar": "user-a4.jpg"
                    },
                    "receiver": {
                        "id": 74,
                        "idunidade": 3,
                        "name": "Jeandro Couto",
                        "email": "jeandro.couto@gmail.com.br",
                        "role": "admin",
                        "remember_token": "VxajOUhCldN4diduPybf7vNQu8VGO2IHiSkonqd4omUykTkJcj1t9CvO7faE",
                        "device_id": "",
                        "created_at": "2017-05-15 22:48:21",
                        "updated_at": "2017-11-07 16:51:15",
                        "avatar": "user-a4.jpg"
                    }
                },
            }
            
### Enviar mensagem para um usuairo
    Metodo: POST
    Url: /messages 
    Parametros: {
        person // id do usuario que vai receber a mensagem
        message // Mensagem a ser enviada
    }
    Sucess Status code: 200
    
    
### Marcar mensagem como lida
    Metodo: POST
    Url: /messages/read
    Parametros: {
        message // id da mensagem que vai ser setada como lida
    }
    Sucess Status code: 200