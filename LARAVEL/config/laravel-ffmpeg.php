<?php

  //no mac deu certo assim
//'ffmpeg.binaries' => '/usr/local/bin/ffmpeg',
  //  'ffprobe.binaries' => '/usr/local/bin/ffprobe',

return [
    'default_disk' => 'local',
    'ffmpeg.binaries' => '/usr/bin/ffmpeg',
    'ffmpeg.threads'  => 12,
    'ffprobe.binaries' => '/usr/bin/ffprobe',
    'timeout' => 3600,
];
