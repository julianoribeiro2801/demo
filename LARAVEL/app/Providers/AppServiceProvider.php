<?php

namespace App\Providers;

use App\Services\FilialService;
// teste PushObserver
use App\Observers\PushObserver;
use App\Models\User;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;


class AppServiceProvider extends ServiceProvider
{
    public function boot()
    {

        //@todo verificar para deixar no observe
         //User::observe(PushObserver::class);
         $filialService = new FilialService();
        

        view()->composer('*', function ($view) use ($filialService) {
            $view->with('unidades_combo', $filialService->unidadesComboTop());
            $view->with('idunidade', Session::get('id_unidade'));
            $view->with('totalmsg', $filialService->totalmsg());

        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
