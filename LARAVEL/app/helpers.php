<?php

/**
 * Classe responável por manipular e validar dados do sistema
 * @copyright (c) 2016, Genesson Andrei Sauer
 */

function setData($Data)
{
    $Format = explode(' ', $Data);
    $Data = explode('/', $Format[0]);
    if (empty($Format[1])):
        $Format[1] = date('H:i:s');
    endif;
    $Data = $Data[2] . '-' . $Data[1] . '-' . $Data[0] . ' ' . $Format[1];
    return $Data;
}

function setDataProjeto($Data)
{
    $Format = explode(' ', $Data);
    $Data = explode('/', $Format[0]);

    $Data = $Data[0] . '/' . $Data[1] . '/' . substr($Data[2], 2, 2);
    return $Data;
}
function setDataTimeStamp($Data)
{
    
    
    
    
    
  
    $Format = explode(' ', substr($Data, 2, 2));
    //$Data1 =  substr($Format[0], 0, 10);
    //$Data = $Data[0] . '/' . $Data[1] . '/' . substr($Data[2], 2, 2);
    return $Format[0];
}
function getData($Data)
{
    if ($Data != '0000-00-00 00:00:00'):
        $data = date("d/m/Y", strtotime($Data));
    return $data;
    endif;
}

function getDataHora($Data)
{
    if ($Data != '0000-00-00 00:00:00'):
        $data = date("d/m/Y H:i", strtotime($Data));
    return $data;
    endif;
}

function getSts($status)
{
    switch ($status) {
        case 'A':
            $retorno = 'Ativo';
            break;
        case 'I':
            $retorno = 'Inativo';
            break;
        default:
            $retorno = 'Inativo';
            break;
    }
    return $retorno;
}

function getStsInt($status)
{
    switch ($status) {
        case 1:
            $retorno = 'Ativo';
            break;
        case 0:
            $retorno = 'Inativo';
            break;
        default:
            $retorno = 'Inativo';
            break;
    }
    return $retorno;
}

function getStsPgto($status)
{
    switch ($status) {
        case 1:
            $retorno = 'Atendido';
            break;
        case 0:
            $retorno = 'Pendente';
            break;
        default:
            $retorno = 'Pendente';
            break;
    }
    return $retorno;
}

function getTipo($tipo)
{
    switch ($tipo) {
        case 1:
            $retorno = 'Cor';
            break;
        case 2:
            $retorno = 'Imagem';
            break;
    }
    return $retorno;
}

function setMoeda($Valor)
{
    $source = array('R$ ', '.', ',');
    $replace = array('', '', '.');
    $valor = str_replace($source, $replace, $Valor);
    return $valor;
}

function getMoeda($Valor)
{
    if (isset($Valor)):
        $valor = 'R$ ' . number_format(floatval($Valor), 2, ',', '.');
    return $valor; else:
        $valor = '';
    endif;
    return $valor;
}

function Image($ImageUrl, $ImageW = null, $ImageH = null)
{
    $Data = 'uploads/'.$ImageUrl;
    if (file_exists($Data) && !is_dir($Data)):
        $imagem = $Data;
    return "<img class=\"img-responsive\" src=\"../../{$imagem}\" width=\"$ImageW\" height=\"$ImageH\"/>"; else:
        return '';
    endif;
}

function setName($Name)
{
    $Format = array();
    $Format['a'] = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜüÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿRr"!@#$%&*()_-+={[}]/?;:.,\\\'<>°ºª';
    $Format['b'] = 'aaaaaaaceeeeiiiidnoooooouuuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr                                 ';
    $Data = strtr(utf8_decode($Name), utf8_decode($Format['a']), $Format['b']);
    $Data = strip_tags(trim($Data));
    $Data = str_replace(' ', '-', $Data);
    $Data = str_replace(array('-----', '----', '---', '--'), '-', $Data);
    return strtolower(utf8_encode($Data));
}
