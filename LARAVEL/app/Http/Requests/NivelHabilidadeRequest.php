<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class NivelHabilidadeRequest extends FormRequest {

    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            'nmnivel' => 'required|min:3'
        ];
    }

    public function messages() {
        return [
            'nmnivel.required' => 'O campo nome é obrigatório'
        ];
    }

}
