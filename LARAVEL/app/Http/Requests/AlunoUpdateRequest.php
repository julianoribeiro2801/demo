<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class AlunoUpdateRequest extends FormRequest {

    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            'name' => 'required|min:3',
            'dados.genero' => 'required',
            //'dados.profile_type' => 'required',
        ];
    }

    public function messages() {
        return [
            'name.required' => 'O campo nome é obrigatório',
            'dados.genero.required' => 'O campo sexo é obrigatório',
            //'dados.profile_type.required' => 'O campo perfil é obrigatório'
        ];
    }

}
