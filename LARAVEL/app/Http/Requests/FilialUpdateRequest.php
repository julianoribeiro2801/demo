<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FilialUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'fantasia'=>'required|min:3'
        ];
    }
}
