<?php
namespace App\Http\Middleware;
use Closure;
use Illuminate\Http\Request;
use Validator;

class ChatGuardian
{
    public function handle(Request $request, Closure $next, $guard = null)
    {

        if (! $this->checkHeaders($request))
        {
            return response()->json(['message' => 'Não foi possivel autenticar sua requisição'], 301);
        }

        return $next($request);
    }

    private function checkHeaders($request)
    {
       if($request->header('user') && $request->header('unity')) {
           return true;
        }
    }
}