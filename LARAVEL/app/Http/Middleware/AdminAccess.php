<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

use App\Models\User;
use App\Models\Unidade;


class AdminAccess
{
    private $roles = [
        "admin",
        "adminfilial",
        "coordenador",
        "professor",
        "recepcionista",
        "nutricionista",
    ];
    // nutricionista vai usar o menu do professor

    public function handle($request, Closure $next)
    {
        if(!Auth::check()) {
             auth()->logout();
            return redirect('/login');
        }

        $academias = User::select('id','role','idunidade')->where('id', Auth::user()->id)->first();
        $valida = Unidade::where('id', $academias->idunidade)->first();
        // dd($valida->situacao);
       
        if (in_array(Auth::user()->role, $this->roles)) {
            if($valida->situacao == 1) {
                 return $next($request);
             }  else {
                auth()->logout();
                return redirect('/acesso_negado_dono');
             }
        }

       

       
        
        auth()->logout();
        return redirect('/login');
    }
}
