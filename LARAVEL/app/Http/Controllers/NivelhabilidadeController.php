<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
//use App\Http\Requests\NivelHabilidadeRequest;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use DB;
use Input;
// use Request;
use Image;
use File;

use App\Models\Unidade;
use App\Models\Nivelhabilidade;
use App\Models\Habilidade;
use \App\Models\Habilidadealuno;
use App\Services\SendPushService;

// Precisa para funcionar o combo
use Auth;
use App\Services\FilialService;


class NivelhabilidadeController extends Controller
{
    public function __construct(FilialService $filialService, SendPushService $pushService)
    {
        $this->filialService = $filialService;
        $this->pushService = $pushService;      
    }

    public function changeEmp($id_unidade)
    {
        $this->filialService->changeEmp($id_unidade);

        return redirect(url()->previous());
    }

    public function index()
    {
        $headers = ['category' => 'Empresa', 'title' => 'Unidades'];

        $unidades = Unidade::with(['parent', 'children', 'matriz'])->get();

        return view('empresas.index', compact('unidades', 'headers'));
    }


    public function getNiveis($id)
    {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        $niveis = Nivelhabilidade::select('id', 'nmnivel', 'dscriterio')->where('idmodalidade', $id)->orderby('nmnivel')->get();
        return $niveis;
        //return response()->json(compact('niveis'));
    }


    public function getNivel($id)
    {
        $nivel = CVEmpresa::select('id', 'nmempresa', 'cnpj', 'percdesconto', 'idsegmento', 'dtvencimento')->where('id', $id)->get();
                
        return response()->json(compact('nivel'));
    }
    public function addNivel() {
            //if(Request::ajax()) {
        $data = Input::all();
            
        
        //echo sizeof($data['habilidades']);
        $validator = Validator::make(Input::all(), [
                            'nmnivel' => 'required|min:3'/*,
                            'dscriterio' => 'required|min:3'*/
                        ],
                        [
                            'nmnivel.required' => 'O campo nome é obrigatório',
                         /*   'dscriterio.required' => 'O campo critério é obrigatório'*/
                        ]
        );
        if ($validator->fails()) {
            return response()->json($validator->errors()->getMessages(), 400);
        } else {


            $idunidade = Auth::user()->idunidade;
            if (session()->get('id_unidade')) {
                $idunidade = session()->get('id_unidade');
            }
            $dataNivel['idunidade'] = $idunidade;
            $dataNivel['nmnivel'] = $data['nmnivel'];
            $dataNivel['idmodalidade'] = $data['idmodalidade'];
            $dataNivel['idobjetivo'] = $data['idobjetivo'];
            //$dataNivel['dscriterio'] = $data['dscriterio'];
            $nivel = new Nivelhabilidade();
            $ret=$nivel->create($dataNivel);
            /*if ($nivel->create($dataNivel)):*/
                $retorno['title'] = 'Sucesso!';
                $retorno['type'] = 'success';
                $retorno['text'] = 'Nível cadastrado com sucesso!';
            /*else:
                $retorno['title'] = 'Erro!';
                $retorno['type'] = 'error';
                $retorno['text'] = 'Erro ao cadastrar nível!';
            endif;*/

            foreach ($data['habilidades'] as $key => $value) {
                if (isset($data['habilidades'][$key]['nmhabilidade'])):


                    $dataNivel['idunidade'] = $idunidade;
                    $dataNivel['idnivel'] = $ret->id;
                    $dataNivel['nmhabilidade'] = $data['habilidades'][$key]['nmhabilidade'];
                    $nivel = new Habilidade();
                    if ($nivel->create($dataNivel)):
                        $retorno['title'] = 'Sucesso!';
                        $retorno['type'] = 'success';
                        $retorno['text'] = 'Habilidade cadastrada com sucesso!';
                    //return $retorno;
                    else:
                        $retorno['title'] = 'Erro!';
                        $retorno['type'] = 'error';
                        $retorno['text'] = 'Erro ao cadastrar habilidade!';
                    // return $retorno;
                    endif;
                endif;
            }
        }
            
        return $retorno;
       
    }
    public function delHabilidade($id) {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }

        $habilidade = new Habilidade();
        $habilidade->where('id', '=', $id)->where('idunidade', '=', $idunidade)->delete();
    }
    public function addHabilidadeAluno() {
        $data = Input::all();
        $idunidade = Auth::user()->idunidade;
        
        
        $habs = Habilidadealuno::select('id')
                       ->where('idaluno', $data['idaluno'])
                       ->where('idhabilidade', $data['idhabilidade'])
                       ->where('idunidade', $idunidade)->first();
        
        
            if (session()->get('id_unidade')) {
                $idunidade = session()->get('id_unidade');
            }
            $dataNivel['idunidade'] = $idunidade;
            $dataNivel['idhabilidade'] = $data['idhabilidade'];
            $dataNivel['idaluno'] = $data['idaluno'];
            $dataNivel['checado'] = $data['checado'];
            $nivel = new Habilidadealuno();
            if (sizeof($habs)<= 0):
                if ($nivel->create($dataNivel)):
                    $retorno['title'] = 'Sucesso!';
                    $retorno['type'] = 'success';
                    $retorno['text'] = 'Habilidade cadastrada com sucesso!';
                    //return $retorno;
                else:
                    $retorno['title'] = 'Erro!';
                    $retorno['type'] = 'error';
                    $retorno['text'] = 'Erro ao cadastrar habilidade!';
                    //return $retorno;
                endif;
            else:
                if ($nivel->where('id', '=', $habs->id)->update($dataNivel)):
                    $retorno['title'] = 'Sucesso!';
                    $retorno['type'] = 'success';
                    $retorno['text'] = 'Habilidade atualizada com sucesso!';
                   // return $retorno;
                else:
                    $retorno['title'] = 'Erro!';
                    $retorno['type'] = 'error';
                    $retorno['text'] = 'Erro ao atualizar habilidade!';
                   // return $retorno;                    
                endif;
                
            endif;     
            if ($data['checado'] == "S"):
                $dados['sender_id'] = 1; //$this->pushService->getSenderID($aulasdia[$key]->idunidade);
                $dados['receiver_id'] = $data['idaluno'];
                $dados['push_type'] = 10;
                $dados['reserva_id'] = 0;
                $dados['hr_aviso'] = 0;

                $dados['push_dtaula'] = '';
                $dados['push_horario'] = date('H:i:s');
                $dados['push_habilidade'] = $data['idhabilidade'];
                $this->pushService->enviaPushProgressoSpm($dados);

            endif;



        return $retorno;
    }    

    public function addHabilidade() {
        
        $data = Input::all();
        
        
        $validator = Validator::make(Input::all(), [
                            'nmhabilidade' => 'required|min:3'
                        ],
                        [
                            'nmhabilidade.required' => 'O campo nome é obrigatório'
                        ]
        );
        if ($validator->fails()) {
            return response()->json($validator->errors()->getMessages(), 400);
        } else {

            
            
            
            

            $idunidade = Auth::user()->idunidade;
            if (session()->get('id_unidade')) {
                $idunidade = session()->get('id_unidade');
            }
            $dataNivel['idunidade'] = $idunidade;
            $dataNivel['idnivel'] = $data['nivel'];
            $dataNivel['nmhabilidade'] = $data['nmhabilidade'];
            $nivel = new Habilidade();
            if ($data['id'] <= 0):
                if ($nivel->create($dataNivel)):
                    $retorno['title'] = 'Sucesso!';
                    $retorno['type'] = 'success';
                    $retorno['text'] = 'Habilidade cadastrada com sucesso!';
                    return $retorno;
                else:
                    $retorno['title'] = 'Erro!';
                    $retorno['type'] = 'error';
                    $retorno['text'] = 'Erro ao cadastrar habilidade!';
                    return $retorno;
                endif;
            else:
                
                $dataNivel['id'] = $data['id'];
                if ($nivel->where('id', '=', $data['id'])->update($dataNivel)):
                    $retorno['title'] = 'Sucesso!';
                    $retorno['type'] = 'success';
                    $retorno['text'] = 'Habilidade atualizada com sucesso!';
                    return $retorno;
                else:
                    $retorno['title'] = 'Erro!';
                    $retorno['type'] = 'error';
                    $retorno['text'] = 'Erro ao atualizar habilidade!';
                    return $retorno;
                endif;

            endif;
        }

    }
    public function getHabilidade($id)
    {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        
        if ($id > 0):
            $habilidades = Habilidade::select('id', 'nmhabilidade', 'idunidade')
                            ->where('idnivel', $id)
                            ->where('idunidade', $idunidade)->orderby('nmhabilidade')->get();
        else:
            $habilidades = Habilidade::select('id', 'nmhabilidade', 'idunidade')
                            ->where('idunidade', $idunidade)->orderby('nmhabilidade')->get();
        endif;

        foreach ($habilidades as $key=>$value){
            $habilidades[$key]->st=1;
            
            
            
        }
        
        return $habilidades;
        //return response()->json(compact('niveis'));
    }
    public function getHabilidadeAluno($id,$idnivel)
    {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }


        
        $sql = "SELECT h.id, h.nmhabilidade, h.idunidade, coalesce(ah.checado,'N') as checado FROM habilidade h left join aluno_habilidade ah 
                on h.id = ah.idhabilidade and h.idunidade = ah.idunidade
                and ah.idaluno = " . $id . " where h.idnivel = " . $idnivel . " order by h.id";
        
        $habilidades =DB::select($sql);

        /*$habilidades = Habilidade::select('habilidade.id', 'habilidade.nmhabilidade', 'habilidade.idunidade', 'aluno_habilidade.checado')
                        ->leftJoin('aluno_habilidade', 'aluno_habilidade.idhabilidade', 'habilidade.id')
                        //->where('aluno_habilidade.idaluno', $id)
                        ->where('habilidade.idunidade', $idunidade)
                        ->orderby('habilidade.nmhabilidade')->get();*/


        foreach ($habilidades as $key => $value) {
            $habilidades[$key]->st = 1;
        }

        return $habilidades;
        //return response()->json(compact('niveis'));
    }
    public function upNivel()
    {
        $data = Input::all();

        $validator = Validator::make(Input::all(), [
                    'nmnivel' => 'required|min:3'
                        ], [
                    'nmnivel.required' => 'O campo nome é obrigatório'
                            /*,
                    'dscriterio.required' => 'O campo critério é obrigatório'*/
                        ]
        );
        if ($validator->fails()) {
            return response()->json($validator->errors()->getMessages(), 400);
        } else {
            $idunidade = Auth::user()->idunidade;
            if (session()->get('id_unidade')) {
                $idunidade = session()->get('id_unidade');
            }

            $dataNivel['idunidade'] = $idunidade;
            $dataNivel['nmnivel'] = $data['nmnivel'];
            //$dataNivel['dscriterio'] = $data['dscriterio'];
            $nivel = new Nivelhabilidade();
            if ($nivel->where('id', '=', $data['id'])->update($dataNivel)):
                $retorno['title'] = 'Sucesso!';
                $retorno['type'] = 'success';
                $retorno['text'] = 'Nível atualizado com sucesso!';
            else:
                $retorno['title'] = 'Erro!';
                $retorno['type'] = 'error';
                $retorno['text'] = 'Erro ao atualizar nível!' + $data['nivel_id'];
            endif;
            return $retorno;
            //}
        }
    }
    // DELETAR PSA
    public function deleteNivel($id)
    {
        $local = new Nivelhabilidade();
        $local->where('id', '=', $id)->delete();
    }
}
