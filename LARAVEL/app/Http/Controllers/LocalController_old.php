<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Local;
use Hash;
use App\Http\Requests\LocalRequest;

class LocalController extends Controller
{
    public function index()
    {
        $headers = ['category' => 'Empresa', 'title' => 'Locais'];
        $locais = Local::all();
        return view('admin.locais.index', compact('locais', 'headers'));
    }

    public function lista()
    {
        return Local::all();
    }

    public function edit($id)
    {
        $headers = ['category' => 'Empresa', 'title' => 'Locais'];
        $local = Local::where('id', $id)->first();
        return view('admin.locais.edit', compact('local', 'headers'));
    }

    public function create()
    {
        $headers = ['category' => 'Empresa', 'title' => 'Locais'];
        return view('admin.locais.create', compact('headers'));
    }

    public function update(LocalRequest $request)
    {
        $data = $request->all();

        Local::where('id', $data['id'])->update(['nmlocal' => $data['nmlocal'],
                     'idunidade' => 1,
                     'metros' =>  $data['mtlocal'],
                     'situacao' => 'Inativo']);
     
        return response()->json(["codigo"=>"1","descricao"=>"Atualizado com sucesso"]);
    }
    public function remove($id)
    {
        Local::where('id', $id)->delete();

        return response()->json(["codigo"=>"1","descricao"=>"Removido com sucesso"]);
    }

    public function store(LocalRequest $request)
    {
        $data = $request->all();
       
        Local::create(['nmlocal' => $data['nmlocal'],
                     'idunidade' => 1,
                     'metros' =>  $data['mtlocal'],
                     'situacao' => 'Inativo'
      ]);
        //return redirect()->route('admin.empresa.locais.index');
        return "Salvo com sucesso";
    }


    // public function create_user(){

          //  User::create( [
    //          'email' => 'kk@msn.com' ,
    //          'password' => Hash::make( '123456' ) ,
    //          'name' => 'KK Ruiz' ,
    //          'role' => 'admin'
    //         ] );
    // }
}
