<?php


//header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");

namespace App\Http\Controllers;

use DateTime;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\AtividadeBlacklist;
use SoapClient;

use Input;
use App\Models\Unidade;


use DB;

use Hash;
use Mail;
use Redirect;
// Precisa para funcionar o combo
use Auth;
use App\Services\FilialService;
use App\Services\ApiEgoiService;

class ApiEgoiController extends Controller
{
    public function __construct(FilialService $filialService,ApiEgoiService $egoi)
    {
        $this->filialService = $filialService;
        $this->egoi = $egoi;
    }

    public function changeEmp($id_unidade)
    {
        $this->filialService->changeEmp($id_unidade);

        return redirect(url()->previous());
    }

    public function getListas()
    {
        $params = array(
                    'apikey'    => '3f25ba1ab90e1197d71ad95f3998e1d813cd642e'
            );
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        $client = new SoapClient('http://api.e-goi.com/v2/soap.php?wsdl');
        $listas = $client->getLists($params);

        return response()->json(compact('listas'));
    }

    public function addSubscriber() {///criar subscriber
        $dataAll = Input::all();
        
        $egoi_dados['nome']=$dataAll['nome'];
        $egoi_dados['sobrenome']=$dataAll['sobrenome'];
        $egoi_dados['email']=$dataAll['email'];
        $egoi_dados['celular']=$dataAll['celular'];
        
        $result=$this->egoi->addSubscriber($egoi_dados);
        
        return response()->json(compact('result'));
        
    }
    public function createCampaignFax()
    {///criar campanha faz
        $params = array(	'apikey'    => '3f25ba1ab90e1197d71ad95f3998e1d813cd642e',
                'subject'	=> 'my_subject',
                'zipfile'	=> base64_encode(file_get_contents('/path/to/my_file')));
        // using Soap with SoapClient
        $client = new SoapClient('http://api.e-goi.com/v2/soap.php?wsdl');
        $result = $client->createCampaignFAX($params);
        //var_dump($result);
        return response()->json(compact('result'));
    }
}
