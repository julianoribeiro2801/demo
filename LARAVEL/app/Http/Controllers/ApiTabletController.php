<?php

//header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");

namespace App\Http\Controllers;

use File;
use FFMpeg;
use Storage;
use DateTime;
use Input;
use Illuminate\Http\Request;
use App\Models\User;
use \App\Models\Listaespera;
use App\Models\Temaacademia;
use App\Models\Agendaalunoprograma;
use App\Models\PsaAtividade;
use App\Models\Agendaavalaluno;
use App\Models\PrescricaoCalendario;
use App\Models\Movatividade;
use App\Models\Versaoapp;
use App\Models\PrescricaoCalendarioPadrao;
use App\Models\PrescricaoEtapa;
use App\Models\TreinoMusculacao;
use App\Models\TreinoFicha;
use App\Models\PrescricaoProjeto;
use App\Models\Programa;
use App\Models\Atividade;
use App\Models\Messages;
use App\Models\Historicotreino;
use App\Models\AtividadeBlacklist;
use App\Models\Totem;
use App\Models\Gfm;
use App\Models\Gfmconfigura;
use App\Models\Reserva;
use App\Models\LogReserva;
use App\Models\LogCancelamento;
use App\Models\Evolucaotreino;
use App\Models\Serietreino;
use App\Models\TempoEstimado;
use App\Models\Rankingconquista;
use App\Models\Conquistausuario;
use App\Models\Spm;
use App\Models\ReservaSpm;
use App\Models\LogReservaSpm;
use App\Models\Unidade;
use App\Models\Funcionario;
use App\Models\Matriculaturma;
use App\Models\Fotouser;
use App\Models\Nutricao;
use App\Models\Processoaluno;
use DB;
use Hash;
use Auth;
use App\Services\FilialService;
use Mail;
use Redirect;
use Image;
use App\Services\SendPushService;
use App\Services\MedalhaService;
use App\Services\ProcessoService;

class ApiTabletController extends Controller {

    public function __construct(FilialService $filialService, SendPushService $pushService, MedalhaService $medalhaService, ProcessoService $processoService) {

        $this->filialService = $filialService;
        $this->pushService = $pushService;
        $this->medalhaService = $medalhaService;
        $this->processoService = $processoService;
    }

    public function getClientes($unidade, $name) {
        $usuario = User::wherenotin('role', ['prospect'])
                        ->where('idunidade', $unidade)
                        ->where('excluido', '=', 'N')
                        ->where('name', 'LIKE', '%' . $name . '%')
                        ->take(10)->get();
        return $usuario;
    }

    public function getAcademia($idunidade) {
        $unidade = Unidade::where('id', $idunidade)->first();
        return $unidade;
    }

    public function listarDiaTreino($id) {
        $treinodia = TreinoMusculacao::select('treino_musculacao.treino_id', 'treino_musculacao.treino_observacao', 'treino_musculacao.aluno_id', 'treino_musculacao.treino_nome', 'treino_musculacao.treino_qtddias', 'treino_musculacao.aluno_id', 'grupomuscular.nmgrupo', 'treino_ficha.*')
                ->JOIN('treino_ficha', 'treino_musculacao.treino_id', '=', 'treino_ficha.treino_id')
                ->JOIN('exercicio', 'treino_ficha.exercicio_id', '=', 'exercicio.id')
                ->JOIN('grupomuscular', 'exercicio.idgrupomuscular', '=', 'grupomuscular.id')
                ->where('treino_musculacao.aluno_id', $id)
                ->where('treino_musculacao.treino_atual', '1')
                ->orderBy('treino_ficha.ficha_letra', 'ASC')
                ->orderBy('treino_ficha.ord', 'ASC')
                ->groupBy('treino_ficha.ficha_letra')
                ->get();
        $treino = $treinodia->toArray();

        foreach ($treinodia as $key => $value) {
            $treino[$key]['tempo_treino'] = $this->listarTempoEstimado($value->treino_id, $value->ficha_letra, $value->aluno_id);
            $treino[$key]['nmgrupo'] = $this->listarMuscalarTreino($value->treino_id, $value->ficha_letra);
        }

        return $treino;
    }

    public function confirmaAvalReserva($idmsg, $id, $idaluno, $tipo, $stat = 'S') {



        switch ($tipo) {
            case '7':
                Agendaalunoprograma::where('id', $id)->where('idaluno', $idaluno)->update(['stconfirmado' => $stat]);
                break;
            case '6':
                if ($stat == 'N') {
                    LogReserva::where('id', $id)->where('idaluno', $idaluno)->delete();
                } else {
                    LogReserva::where('id', $id)->where('idaluno', $idaluno)->update(['stconfirmado' => $stat]);

                    $dadosAula = LogReserva::select('gfm.idunidade', 'gfm.hora_inicio', 'gfm.duracao', 'gfm.gfmspm')
                            ->leftjoin('gfm', 'gfm.id', 'gfm_reserva_log.idgfm')
                            ->where('gfm_reserva_log.id', $id)
                            ->get();


                    if (sizeof($dadosAula)):

                        $tpatividade = $dadosAula[0]->gfmspm;
                        $tp = "";
                        if ($tpatividade == "Gfm") :
                            $tp = "G";
                        else:
                            $tp = "N";
                        endif;

                        $dtinicio = date('Y-m-d') . " " . $dadosAula[0]->hora_inicio;

                        $duracao = $dadosAula[0]->duracao;
                        $dtfim = date('Y-m-d H:i:s', strtotime("$dtinicio + $duracao minutes"));

                        ////pega horario da aula
                        /////inicia e grava o historicotreino
                        Historicotreino::create([
                            'IDALUNO' => $idaluno,
                            'IDUNIDADE' => $dadosAula[0]->idunidade,
                            'IDTREINO' => 0,
                            'NRDIATREINO' => 0,
                            'TPATIVIDADE' => $tp,
                            'DTINICIO' => $dtinicio,
                            'DTFIM' => $dtfim,
                            'DTREGISTRO' => $dtfim,
                            'NRDIASUGESTAO' => 0,
                            'STTREINO' => "F"
                        ]);

                    endif;
                }
                break;
            case '4':
                Agendaavalaluno::where('id', $id)->where('idaluno', $idaluno)->update(['stconfirmado' => $stat]);
                break;
        }

        Messages::where('id', $idmsg)->where('receiver_id', $idaluno)->update(['confirmado' => $stat]);
        if ($stat == 'S'):
            return 'Confirmação efetuada com sucesso';
        else:
            return 'Cancelamento efetuado com sucesso';

        endif;
    }

    public function pushAvisaAvaliacao() {

        $dtHoje = date('Y-m-d');
        $novaHora1 = date('H:i:s');
        $horaNova = strtotime("$novaHora1 + 60 minutes");
        $horaFormatada = date("H:i:s", $horaNova);

        $sql = "SELECT a.idunidade,a.idaluno,a.id, a.horario, a.dtagenda FROM agendaavalaluno a where"
                . " dtagenda = '" . $dtHoje . "'"
                . " and horario between '" . $novaHora1 . "'"
                . " and '" . $horaFormatada . "' and a.staviso not in ('S')";

        $agendadia = DB::select($sql);
        foreach ($agendadia as $key => $value) {
            Agendaavalaluno::where('id', $agendadia[$key]->id)->update(['staviso' => 'S']);
            // $aula = Gfm::find($aulasdia[$key]->idgfm);
            /// $aulanome = Programa::find($aula['idprograma']);
            $dados['sender_id'] = $this->pushService->getSenderID($agendadia[$key]->idunidade);
            $dados['receiver_id'] = $agendadia[$key]->idaluno;
            $dados['push_type'] = 4;
            $dados['reserva_id'] = $agendadia[$key]->id;
            $dados['hr_aviso'] = $agendadia[$key]->horario;

            $dados['push_dtagenda'] = $agendadia[$key]->dtagenda;
            $dados['push_horario'] = date('H:i', strtotime($agendadia[$key]->horario));
            // pegar o id da unidade
            // $sender_id = $this->pushService->getSenderID($unidade);
            $this->pushService->enviaPushAvisoAval($dados);
        }
        return 'sucesso';
    }

    //Esse aqui Adriano colocar no cron jobs para rodar a cada 2 min

    public function getReservasAluno($idaluno) {

        $reservas = LogReserva::select('gfm_reserva_log.*', 'programa.nmprograma', 'users.idunidade', 'users.id', 'users.avatar')
                ->leftJOIN('users', 'users.id', '=', 'gfm_reserva_log.idaluno')
                ->leftJOIN('gfm', 'gfm.id', '=', 'gfm_reserva_log.idgfm')
                ->leftJOIN('programa', 'programa.id', '=', 'gfm.idprograma')
                ->where('gfm_reserva_log.idaluno', '=', $idaluno)
                ->orderBy('gfm_reserva_log.dtaula', 'desc')
                ->get();
        return $reservas;
    }

    public function getCoachs($idunidade, $id_aluno = 0) {

        $nutricionistas = Funcionario::select('users.id', 'users.name', 'user_dados.site', 'user_dados.situacao', 'users.tipo')
                ->where('users.idunidade', $idunidade)
                ->wherein('users.role', ['coach', 'nutricionista'])
                ->where('user_dados.situacao', 'A')
                ->leftJoin('user_dados', 'user_dados.user_id', '=', 'users.id')
                ->get();



        foreach ($nutricionistas as $nutricionista) {

            $nutricionista->name = ucwords(strtolower($nutricionista->name));
            $nutricionista->prescricao = '';

            if ($id_aluno > 0) {
                $nutricionista->prescricao = Nutricao::where('aluno_id', $id_aluno)
                        ->where('professor_id', $nutricionista->id)
                        ->where('unidade_id', $idunidade)
                        ->orderby('id', 'DESC')
                        ->first();
            }
        }

        return $nutricionistas;
    }

    public function getCoachPrescricao($id) {

        $prescricao = Nutricao::where('id', $id)->first();

        return $prescricao;
    }

    public function executaAvisos() {

        $avaliacoes = $this->pushAvisaAvaliacao();
        if ($avaliacoes == 'sucesso') {
            $aula = $this->pushAvisaAula();
            if ($aula == 'sucesso') {

                $personalizado = $this->pushAvisaPersonalizado();
                if ($personalizado == 'sucesso') {
                    return 'sucesso';
                }
            }
        }
    }

    public function gravarVersaoApp($versaoapp, $tipo) {

        $versao = Versaoapp::create([ 'versao' => $versaoapp, 'tipo' => $tipo]);

        if (sizeof($versao)) {
            $retorno['cod'] = '0';
            $retorno['text'] = 'Versão atualizada com sucesso!';
        } else {
            $retorno['cod'] = '-1';
            $retorno['text'] = 'Ocorreu um erro ao atualizar versão, tente novamente!';
        }
        return $retorno;
    }

    public function getVersaoApp($aluno, $tipo) {

        $versaoAluno = User::select('versaoapp')
                ->where('id', $aluno)
                ->get();

        $sql = "SELECT versao from historico_versaoapp where tipo = '" . $tipo . "'"
                . " and id = (select max(id) from historico_versaoapp where tipo = '" . $tipo . "') ";

        $versaoApp = DB::select($sql);

        $alunoV = number_format($versaoAluno[0]->versaoapp, 2, '.', '.');
        $app = number_format($versaoApp[0]->versao, 2, '.', '.');
        if ($alunoV < $app):
            return 'true';
        else:
            return 'false';

        endif;
    }

    public function getTemaApp($unidade) {

        $tema = Temaacademia::select('tema_academia.idtema', 'tema_padrao_app.descricao')
                ->leftjoin('tema_padrao_app', 'tema_padrao_app.id', 'tema_academia.idtema')
                ->where('tema_academia.idunidade', $unidade)
                ->get();

        return $tema;
    }

    public function atualizaVersaoApp($id, $versaoapp, $versaoso) {
        $versao = User::where('id', $id)->update(['versaoapp' => $versaoapp, 'versaoso' => $versaoso]);

        if (sizeof($versao)) {
            $retorno['cod'] = '0';
            $retorno['text'] = 'Versão atualizada com sucesso!';
        } else {
            $retorno['cod'] = '-1';
            $retorno['text'] = 'Ocorreu um erro ao atualizar versão, tente novamente!';
        }
        return $retorno;
    }

    public function verificarVersaoApp($id) {
        //$versao = User::where('id', $id)->update(['versaoapp' => $versaoapp, 'versaoso' => $versaoso]);

        $versao = User::select('versaoso', 'versaoapp')
                ->where('id', $id)
                ->get();
        return $versao;
    }

    public function geraRegistroAula() {



        $dtHj = date('Y-m-d');
        $ano = substr($dtHj, 0, 4);
        $mes = substr($dtHj, 5, 2);
        $dia = substr($dtHj, 8, 2);
        $ds = date("w", mktime($ano, $dia, $mes));
        $diasemana = $ds + 1;


        $gfms = Gfm::select('gfm.id', 'gfm.idunidade', 'gfm.diasemana', 'gfm.hora_inicio', 'gfm.capacidade', 'users.name as nmprofessor', 'programa.nmprograma', 'local.nmlocal', 'gfm_reserva.n_reservas', 'gfm_reserva.id as reserva_id')
                ->leftJoin('users', 'users.id', '=', 'gfm.idfuncionario')
                ->leftJoin('programa', 'programa.id', '=', 'gfm.idprograma')
                ->leftJoin('local', 'local.id', '=', 'gfm.idlocal')
                ->leftJoin('gfm_reserva', 'gfm_reserva.idgfm', '=', 'gfm.id')
                //->where('gfm.idunidade', $idunidade)
                ->where('gfm.diasemana', 'LIKE', '%' . $diasemana . '%')
                ->orderBy('gfm.hora_inicio', 'ASC')
                ->get();


        foreach ($gfms as $key => $value) {
            $reservas = Reserva::select('id', 'idunidade', 'capacidade')->where('idgfm', $gfms[$key]['id'])->where('dtaula', $dtHj)->get();
            if (sizeof($reservas) <= 0):
                $dataReserva['idunidade'] = $gfms[$key]->idunidade;
                $dataReserva['idgfm'] = $gfms[$key]->id;
                $dataReserva['n_reservas'] = $gfms[$key]->capacidade;
                $dataReserva['capacidade'] = $gfms[$key]->capacidade;
                $dataReserva['dtaula'] = $dtHj;
                $reservaNew = Reserva::create($dataReserva);
                if (sizeof($reservaNew)) {
                    
                } else {
                    
                }
            endif;
        }

        return $gfms;
    }

    public function pushAvisaAula() {

        $dtHoje = date('Y-m-d');
        $novaHora1 = date('H:i:s');
        $horaNova = strtotime("$novaHora1 + 60 minutes");
        $horaFormatada = date("H:i:s", $horaNova);


        $sql = "SELECT g.idfuncionario, grl.id,g.idunidade,g.idprograma,g.hora_inicio, grl.idaluno, grl.idgfm, grl.dtaula"
                . " FROM gfm_reserva_log grl , gfm g where grl.dtaula='" . $dtHoje . "' and "
                . "g.hora_inicio between '" . $novaHora1 . "' and '" . $horaFormatada . "' and grl.idgfm = g.id and grl.staviso not in ('S')";

        $aulasdia = DB::select($sql);

        foreach ($aulasdia as $key => $value) {

            LogReserva::where('id', $aulasdia[$key]->id)->update(['staviso' => 'S']);

            $aula = Gfm::find($aulasdia[$key]->idgfm);
            $aulanome = Programa::find($aula['idprograma']);

            $dados['sender_id'] = $aulasdia[$key]->idfuncionario; //$this->pushService->getSenderID($aulasdia[$key]->idunidade);
            $dados['receiver_id'] = $aulasdia[$key]->idaluno;
            $dados['push_type'] = 6;
            $dados['reserva_id'] = $aulasdia[$key]->id;
            $dados['hr_aviso'] = $aulasdia[$key]->hora_inicio;

            $dados['push_dtaula'] = $aulasdia[$key]->dtaula;
            $dados['push_horario'] = date('H:i', strtotime($aulasdia[$key]->hora_inicio));
            $dados['push_aula'] = $aulanome['nmprograma'];

            // pegar o id da unidade
            //$sender_id = $this->pushService->getSenderID($unidade);

            $this->pushService->enviaPushReserva($dados);
        }


        return 'sucesso';
    }

    function getProfessor($idaluno) {
        $treinodia = TreinoMusculacao::select('treino_musculacao.professor_id')
                ->where('treino_musculacao.aluno_id', $idaluno)
                ->where('treino_musculacao.treino_atual', '1')
                ->get();


        $treinodia[0]['professor_id'];

        return $treinodia[0]['professor_id'];
    }

    function listarPesagemGrafico($idaluno, $idunidade) {
        $pesagens = Fotouser::select('foto_user.user_peso', 'foto_user.created_at')
                        ->where('foto_user.user_id', $idaluno)
                        ->where('foto_user.unidade_id', $idunidade)
                        ->orderBY('id', 'desc')
                        ->take(10)->get();

        foreach ($pesagens as $key => $value) {

            $pesagens[$key]->dtpesagem = date('Y-m-d', strtotime($pesagens[$key]->created_at));
        }

        return $pesagens;
    }

    function listarTonelagemGrafico($idaluno, $idunidade) {
        $pesagens = Historicotreino::select('historicotreino.dtinicio', 'qtton')
                        ->where('historicotreino.idaluno', $idaluno)
                        ->where('historicotreino.idunidade', $idunidade)
                        ->where('historicotreino.tpatividade', 'M')
                        ->orderBY('id', 'desc')
                        ->take(10)->get();

        foreach ($pesagens as $key => $value) {

            $pesagens[$key]->dttreino = date('Y-m-d', strtotime($pesagens[$key]->dtinicio));
        }

        return $pesagens;
    }

    public function pushAvisaPersonalizado() {

        /* $dtHoje = date('Y-m-d');
          $novaHora1 = '13:00:00';
          $horaNova = strtotime("$novaHora1 + 60 minutes");
          $horaFormatada = '18:00:00'; */
        $dtHoje = date('Y-m-d');
        $novaHora1 = date('H:i:s');
        $horaNova = strtotime("$novaHora1 + 60 minutes");
        $horaFormatada = date("H:i:s", $horaNova);


        $sql = "SELECT grl.id,grl.idunidade,grl.hragenda, grl.idaluno, grl.dtagenda"
                . " FROM agenda_alunoprograma grl where grl.dtagenda = '" . $dtHoje . "' and "
                . " grl.staviso not in ('S')"
                . " and grl.hragenda between '" . $novaHora1 . "' and '" . $horaFormatada . "'";

        $aulasdia = DB::select($sql);

        foreach ($aulasdia as $key => $value) {

            Agendaalunoprograma::where('id', $aulasdia[$key]->id)->update(['staviso' => 'S']);

            $dados['sender_id'] = $this->getProfessor($aulasdia[$key]->idaluno);
            $dados['receiver_id'] = $aulasdia[$key]->idaluno;
            $dados['push_type'] = 7;
            $dados['reserva_id'] = $aulasdia[$key]->id;
            $dados['hr_aviso'] = $aulasdia[$key]->hragenda;

            $dados['push_dtagenda'] = $aulasdia[$key]->dtagenda;
            $dados['push_horario'] = date('H:i', strtotime($aulasdia[$key]->hragenda));

            // pegar o id da unidade
            //$sender_id = $this->pushService->getSenderID($unidade);
            $this->pushService->enviaPushPersonalizado($dados);
        }


        return 'sucesso';
    }

    public function verificarArquivo($name_img) {


        $diskimagem = Storage::disk('avatars');

        // SE A FOTO EXISTIR COLOCO
        if ($diskimagem->exists($name_img)) {
            return $name_img;
        } else {
            return "user-a4.jpg";
        }
    }

    public function atualizaAvatar($idunidade) {

        $alunos = User::select('users.*')
                ->where('idunidade', $idunidade)
                ->get();

        foreach ($alunos as $key => $value) {
            $avatar = 'user-' . $alunos[$key]->id . '.jpg';
            User::where('idunidade', $idunidade)->where('id', $alunos[$key]->id)->where('avatar', 'user-a4.jpg')->update(['avatar' => $avatar]);
        }

        return $alunos;
    }

    public function agruparSeries($idficha, $st) {
        // $data = $request->all();
        // dd($request->all());
        if (Treinoficha::where('ficha_id', $idficha)
                        ->update(['agrupado' => $st])) {
            // $retorno['cod'] = '0';
            // $retorno['text'] = 'Aluno atualizado com sucesso!';
            return 'Cadastro atualizado com sucesso!';
        } else {
            // $retorno['cod'] = '-1';
            // $retorno['text'] = 'Erro ao atualizar aluno!';
            return'Erro ao atualizar cadastro!';
        }

        return $treino;
    }

    public function listarSeries($idaluno, $idunidade, $idprescricao, $fichatreino, $idexercicio, $nrlancamento) {


        $treinodia = TreinoFicha::select(DB::raw('coalesce(treino_ficha.ficha_observacao, "") as ficha_observacao'), DB::raw('coalesce(exercicio.nmexercicio, "NÃO INFORMADO") as nmexercicio'), DB::raw('coalesce(exercicio.dsurlminiatura, "") as dsurlminiatura'), DB::raw('coalesce(exercicio.video, "") as video'), 'exercicio.video', 'treino_ficha.*')
                ->LEFTJOIN('exercicio', 'treino_ficha.exercicio_id', '=', 'exercicio.id')
                ->where('treino_ficha.ficha_letra', $fichatreino)
                ->where('treino_ficha.ficha_id', $nrlancamento)
                ->where('exercicio.id', $idexercicio)
                ->first();

        $diatreino = 0;

        if ($fichatreino == "A") {
            $diatreino = 1;
        }
        if ($fichatreino == "B") {
            $diatreino = 2;
        }
        if ($fichatreino == "C") {
            $diatreino = 3;
        }
        if ($fichatreino == "D") {
            $diatreino = 4;
        }
        if ($fichatreino == "E") {
            $diatreino = 5;
        }
        if ($fichatreino == "F") {
            $diatreino = 6;
        }
        if ($fichatreino == "G") {
            $diatreino = 7;
        }


        $dia_hoje = date('Y-m-d'); //////////////////////ate aqui grava historico treino
        ///SELECT Q BUSCA DADOS DO ULTIMO TREINO REALIZADO
        $ultLct = 0;
        $sql = "select coalesce(max(nrlancamentodia),0) as max from serietreino where idaluno = " . $idaluno;
        $sql .= " and nrdiatreino = " . $diatreino;
        $sql .= " and nrlancamento = " . $nrlancamento;
        $sql .= " and idunidade = " . $idunidade;
        $sql .= " and idprescricao = " . $idprescricao;

        $maximo = DB::select($sql);
        $ultLct = $maximo[0]->max;

        if ($ultLct == 0) {
            $sql = "select l.treino_id,l.exercicio_id, l.ficha_letra, l.ficha_id, l.ficha_series, l.ficha_repeticoes from treino_ficha l, "
                    . " treino_musculacao p where p.unidade_id = " . $idunidade
                    . " and p.aluno_id = " . $idaluno
                    . " and l.treino_id = " . $idprescricao
                    . " and l.ficha_letra = '" . $fichatreino . "'"
                    . " and l.ficha_id = " . $nrlancamento
                    . " and p.treino_id = l.treino_id";

            $c = DB::select($sql);
            $nrlancExer = 0;
            $idInserido = 0;
            $qtSerie = 0;
            $cargaAnt = 0;
            foreach ($c as $key => $value) {
                $nrlancExer = $c[$key]->ficha_id;
                $qtSerie = 0;

                $evo = Evolucaotreino::create([ 'IDALUNO' => $idaluno, 'IDUNIDADE' => $idunidade, 'IDPRESCRICAO' => $c[$key]->treino_id, 'NRDIATREINO' => $diatreino, 'NRLANCAMENTO' => $c[$key]->ficha_id, 'QTREPETICAO' => $c[$key]->ficha_repeticoes, 'QTSERIE' => $c[$key]->ficha_series, 'QTCARGA' => '0', 'DTREGISTRO' => $dia_hoje, 'IDEXERCICIO' => $c[$key]->exercicio_id]);
                $idInserido = $evo->id;
                $ultLct = $idInserido;
                $y = 1;
                while ($y <= $c[$key]->ficha_series) {
                    $serie = Serietreino::create([ 'IDALUNO' => $idaluno, 'IDUNIDADE' => $idunidade, 'IDPRESCRICAO' => $c[$key]->treino_id, 'NRDIATREINO' => $diatreino, 'NRLANCAMENTO' => $c[$key]->ficha_id, 'NRLANCAMENTODIA' => $idInserido, 'NRSERIE' => $y, 'QTREPETICAO' => $c[$key]->ficha_repeticoes, 'QTCARGA' => $cargaAnt, 'DTREGISTRO' => $dia_hoje]);

                    $y++;
                }
            }
        }
        //////////////////////////////////////////////////////////////////////
        $sql = "select coalesce(le.ficha_observacao,'') as ficha_observacao,s.qtcarga, s.qtrepeticao, s.idaluno, s.idunidade, s.idprescricao, s.nrdiatreino, s.nrserie, s.nrlancamentodia, "
                . "le.ficha_series as qts, coalesce(le.medida_duracao,'rep') as medida, "
                . "e.id as idexercicio,"
                . "s.nrlancamento,";
        $sql .= " e.nmexercicio ";
        $sql .= "from serietreino as s, treino_ficha as le ,exercicio as e where";
        $sql .= " le.treino_id = s.idprescricao ";
        $sql .= " and s.idaluno = " . $idaluno;
        $sql .= " and s.idunidade = " . $idunidade;
        $sql .= " and le.treino_id = s.idprescricao ";
        $sql .= " and le.ficha_letra = '" . $fichatreino . "'";
        $sql .= " and le.ficha_id = s.nrlancamento ";
        $sql .= " and s.nrlancamento = " . $nrlancamento;
        $sql .= " and s.nrlancamentodia = " . $ultLct;
        $sql .= " and e.id = " . $idexercicio;
        $sql .= " and s.idprescricao = " . $idprescricao;
        $sql .= " order by s.nrdiatreino, s.nrserie";

        $srs = DB::select($sql);
        $aluno = 0;
        $unidade = 0;
        $prescricao = 0;
        $nrdiatreino = 0;
        $nrlancamentodia = 0;
        $serie = 0;
        $exercicio = 0;
        $nmexercicio = 0;
        $medida = '';
        $x = 0;
        $qts = 0;
        $series = [];

        foreach ($srs as $key => $value) {
            $qts = $srs[$key]->qts;
            $aluno = $srs[$key]->idaluno;
            $unidade = $srs[$key]->idunidade;
            $prescricao = $srs[$key]->idprescricao;
            $nrdiatreino = $srs[$key]->nrdiatreino;
            $serie = $srs[$key]->nrserie;
            $exercicio = $srs[$key]->idexercicio;
            $nrlancamentodia = $srs[$key]->nrlancamentodia;
            $nmexercicio = $srs[$key]->nmexercicio;
            $medida = $srs[$key]->medida;
            $series[$key]['IDALUNO'] = $srs[$key]->idaluno;
            $series[$key]['NRLANCAMENTODIA'] = $srs[$key]->nrlancamentodia;
            $series[$key]['IDUNIDADE'] = $srs[$key]->idunidade;
            $series[$key]["IDPRESCRICAO"] = $srs[$key]->idprescricao;
            $series[$key]["NRDIATREINO"] = $srs[$key]->nrdiatreino;
            $series[$key]["NRSERIE"] = $srs[$key]->nrserie;
            $series[$key]["IDEXERCICIO"] = $srs[$key]->idexercicio;
            $series[$key]["NRLANCAMENTO"] = $srs[$key]->nrlancamento;
            $series[$key]["NMEXERCICIO"] = $srs[$key]->nmexercicio;
            $series[$key]["QTREPETICAO"] = $srs[$key]->qtrepeticao;
            $series[$key]["QTCARGA"] = $srs[$key]->qtcarga;
            $series[$key]["agrupado"] = $treinodia->agrupado;
            $series[$key]["MEDIDA"] = $treinodia->medida;
            $series[$key]["MEDIDA_INTENSIDADE"] = $treinodia->medida_intensidade;
            $series[$key]["MEDIDA_DURACAO"] = $treinodia->medida_duracao;
            $series[$key]["OBSERVACAO"] = $treinodia->ficha_observacao;
            $series[$key]["ficha_observacao"] = $treinodia->ficha_observacao;
            $x++;
        }
        $y = $qts - $x;
        for ($i = 1; $i <= $y; $i++) {
            $series[$i]['IDALUNO'] = $aluno;
            $series[$i]['NRLANCAMENTODIA'] = $nrlancamentodia;
            $series[$i]['IDUNIDADE'] = $unidade;
            $series[$i]["IDPRESCRICAO"] = $prescricao;
            $series[$i]["NRDIATREINO"] = $nrdiatreino;
            $series[$i]["NRSERIE"] = $x + $i;
            $series[$i]["IDEXERCICIO"] = $exercicio;
            $series[$i]["NRLANCAMENTO"] = $nrlancamento;
            $series[$i]["NMEXERCICIO"] = $nmexercicio;
            $series[$i]["QTREPETICAO"] = 0;
            $series[$i]["QTCARGA"] = '';
            $series[$key]["agrupado"] = $treinodia->agrupado;
            $series[$key]["MEDIDA"] = $medida;
        }

        return $series;
    }

    public function listarTempoEstimado($id_treino, $letra, $id_cliente) {
        // $treinodia = TreinoFicha::where('treino_id', $id_treino)->get();
        $treinodia = TempoEstimado::select('tempo_estimado.tempoestimado')
                ->where('tempo_estimado.treino_id', $id_treino)
                ->where('tempo_estimado.client_id', $id_cliente)
                ->where('tempo_estimado.letra', $letra)
                ->get();
        $ret = $treinodia[0]['tempoestimado'];
        return $ret;
    }

    // listar grupos musculares de um treino
    public function listarMuscalarTreino($id_treino, $letra) {
        // $treinodia = TreinoFicha::where('treino_id', $id_treino)->get();
        $treinodia = TreinoFicha::select('grupomuscular.nmgrupo')
                ->JOIN('exercicio', 'treino_ficha.exercicio_id', '=', 'exercicio.id')
                ->JOIN('grupomuscular', 'exercicio.idgrupomuscular', '=', 'grupomuscular.id')
                ->where('treino_ficha.treino_id', $id_treino)
                ->where('treino_ficha.ficha_letra', $letra)
                ->groupBy('grupomuscular.nmgrupo')
                ->get();
        $ret = '';


        foreach ($treinodia as $key => $value) {
            $virgula = ($key < (count($treinodia) - 1)) ? ', ' : '';
            $ret .= $value->nmgrupo . $virgula;
        }

        return $ret;
    }

    public function registraAtividade(Request $request) {

        $aluno = $request->idaluno;
        $unidade = $request->idunidade;
        $atividade = $request->idatividade;
        $qtatividade = str_replace(':', '.', $request->qtatividade);
        $stmovatividade = $request->stmovatividade;

        $tp = "";
        switch ($atividade) {
            case '4':
                $tp = 'T';
                break;
            case '5':
                $tp = 'C';
                break;
            case '6':
                $tp = 'B';
                break;
        }


        $atividades = Movatividade::create([
                    'idaluno' => $aluno,
                    'idunidade' => $unidade,
                    'idatividade' => $atividade,
                    'qtatividade' => $qtatividade,
                    'stmovatividade' => $stmovatividade,
                    'dtregistro' => date('Y-m-d H:i:s')
        ]);

        $dtfim = date('Y-m-d') . " " . "23:59:59";


        Historicotreino::create([
            'IDALUNO' => $aluno,
            'IDUNIDADE' => $unidade,
            'IDTREINO' => 0,
            'NRDIATREINO' => 0,
            'TPATIVIDADE' => $tp,
            'DTINICIO' => date('Y-m-d H:i:s'),
            'DTFIM' => $dtfim,
            'DTREGISTRO' => $dtfim,
            'NRDIASUGESTAO' => 0,
            'STTREINO' => "F"
        ]);



        return 'Registro efetuado com sucesso';
    }

    public function listarEvolucao($idficha) {
        // $treinodia = TreinoFicha::where('treino_id', $id_treino)->get();
        $evolucao = Serietreino::select('serietreino.*')
                ->where('NRLANCAMENTO', $idficha)
                ->orderBy('serietreino.DTREGISTRO', 'ASC')
                ->get();

        return response()->json(compact('evolucao'));
    }

    public function setDeviceIdGet(Request $request, $id) {
        $academia['teste'] = 'sdjkfsdjkfjdfh';
        $academia['teste1'] = 'sdjkfsdjkfjdfh';
        $academia['teste2'] = $id;


        return response()->json(compact('academia'));
        //return $retorno;
    }

    public function testeVideo() {
        return $duration;
    }

    public function setDeviceId(Request $request) {
        // $data = $request->all();
        // dd($request->all());
        if (User::where('id', $request->idaluno)
                        ->where('idunidade', $request->idunidade)
                        ->update(['device_id' => $request->registrationId])) {
            // $retorno['cod'] = '0';
            // $retorno['text'] = 'Aluno atualizado com sucesso!';
            return 'Aluno atualizado com sucesso!';
        } else {
            // $retorno['cod'] = '-1';
            // $retorno['text'] = 'Erro ao atualizar aluno!';
            return'Erro ao atualizar aluno!';
        }
    }

    public function pausarTempo(Request $request) {
        // $data = $request->all();
        // dd($request->all());
        if (Historicotreino::where('idaluno', $request->idaluno)
                        ->where('idunidade', $request->idunidade)
                        ->where('sttreino', 'A')
                        ->update(['pausado' => $request->pausar])) {
            // $retorno['cod'] = '0';
            // $retorno['text'] = 'Aluno atualizado com sucesso!';
            return 'Registro atualizado com sucesso!';
        } else {
            // $retorno['cod'] = '-1';
            // $retorno['text'] = 'Erro ao atualizar aluno!';
            return'Erro ao atualizar dados!';
        }
    }

    public function participarRanking(Request $request/* $aluno,$unidade,$atividade,$participa */) {



        $aluno = $request->idaluno;
        $unidade = $request->idunidade;
//      $conquista = $request->idconquista;
        $atividade = $request->idatividade;
        $participa = $request->participa;

        $totConquista = 0;
        $posicao = 0;




        $sql = "select idatividade, id as idconquista from conquista where ";
        $sql .= " idatividade = " . $atividade;
        $consulta = DB::select($sql);

        foreach ($consulta as $key => $value) {
            $conquista = $consulta[$key]->idconquista;
        }


        $part = "";
        $sql = "select idaluno from conquistausuario where ";
        $sql .= " idaluno = " . $aluno;
        $sql .= " and idunidade = " . $unidade;
        $sql .= " and idconquista = " . $atividade;
        $consulta = DB::select($sql);

        foreach ($consulta as $key => $value) {
            $part = "S";
        } /// total do aluno
//////////////////////////////////////////////////////////////////////////////////////////      
        if ($part != "") {

            $sql = "SELECT max(nrposicao) maxima FROM rankingconquista WHERE idconquista = $atividade and idunidade = $unidade ";

            $consulta = DB::select($sql);


            $posi = 0;

            if (sizeof($consulta) > 0):
                $posi = $consulta[0]->maxima + 1;
            else:
                $posi = $posi + 1;
            endif;

            if (Conquistausuario::where('idunidade', $unidade)
                            ->where('idaluno', $aluno)
                            ->where('idconquista', $atividade)
                            ->update(['stparticipa' => $participa])) {
                $retorno['text'] = 'Ranking iniciado!';
            } else {
                $retorno['text'] = 'Ocorreu um erro ao iniciar!';
            }
            if ($participa == 'S'):
                Rankingconquista::create([
                    'idunidade' => $unidade,
                    'idaluno' => $aluno,
                    'idconquista' => $atividade,
                    'nrposicao' => $posi,
                    'qtconquista' => 0,
                    'dtregistro' => date('Y-m-d H:i:s')
                ]);
            else:
            //devo deletar qdo deixa de participar do ranking
            // Rankingconquista::where('idunidade', $unidade)->where('idaluno', $aluno)->where('idconquista', $atividade)->delete();                
            endif;
        } else {



            $sql = "SELECT max(nrposicao) maxima FROM rankingconquista WHERE idconquista = $atividade and idunidade = $unidade ";

            $consulta = DB::select($sql);


            $posi = 0;

            if (sizeof($consulta) > 0):
                $posi = $consulta[0]->maxima + 1;
            else:
                $posi = $posi + 1;
            endif;

            //  echo $atividade . " -  pau" ;
            Conquistausuario::create([
                'idunidade' => $unidade,
                'idaluno' => $aluno,
                'idconquista' => $atividade,
                'stconquista' => 'S',
                'dtconquista' => date('Y-m-d'),
                'qtconquista' => 0,
                'stparticipa' => $participa,
                'dtregistro' => date('Y-m-d H:i:s')
            ]);

            if ($participa == 'S'):
                Rankingconquista::create([
                    'idunidade' => $unidade,
                    'idaluno' => $aluno,
                    'idconquista' => $atividade,
                    'nrposicao' => $posi,
                    'qtconquista' => 0,
                    'dtregistro' => date('Y-m-d H:i:s')
                ]);
            else:
            //   Rankingconquista::where('idunidade', $unidade)->where('idaluno', $aluno)->where('idconquista', $atividade)->delete();                
            endif;
        }
        $retorno['MENSAGEM'] = 'Registro efetuado com sucesso';



        return $retorno;
    }

    public function getPausado(Request $request) {
        // $data = $request->all();
        // dd($request->all());
        // $treinodia = TreinoFicha::where('treino_id', $id_treino)->get();
        $pausado = Historicotreino::select('pausado')
                ->where('idaluno', $request->idaluno)
                ->where('idunidade', $request->idunidade)
                ->where('sttreino', 'A')
                ->get();

        return $pausado;
    }

    public function exibirRanking_old(Request $request) {
        // aqui    
        //		$modalidades = Atividade::select('id', 'nmatividade', 'stmenuapp', 'linkapp')->get();
        $sql = "select max(dtregistro)  as dtult from movatividade where idunidade = " . $request->idunidade;
        $sql .= " and idatividade = " . $request->idconquista;

        $dt = DB::select($sql);


        $monthUlt = date("m", strtotime($dt[0]->dtult));
        $yearUlt = date("Y", strtotime($dt[0]->dtult));

        $mesAtual = date("m", strtotime(date('Y-m-d')));
        $anoAtual = date("Y", strtotime(date('Y-m-d')));

        $zera = 'N';
        if ($request->idconquista != 4) {
            if ($yearUlt < $anoAtual) {
                $zera = 'S';
            } else {
                if ($yearUlt == $anoAtual) {
                    if ($monthUlt < $mesAtual) {
                        $zera = 'S';
                    } else {
                        $zera = 'N';
                    }
                }
            }
        } else {
            /////////////////////////////////////////////


            $dtHj = date('Y-m-d', strtotime(date('Y-m-d')));
            $anoUlt = substr($dtHj, 0, 4);
            $mesUlt = substr($dtHj, 5, 2);
            $diaUlt = substr($dtHj, 8, 2);
            $dsUlt = date("w", mktime($anoUlt, $diaUlt, $mesUlt));
            $diasemanaUlt = $dsUlt + 1;
            $difDias = ($diasemanaUlt - 1);

            $dt = date('Y-m-d', strtotime("-" . $difDias . " days"));
            /////////////////////////////////////////////////////////////////////////

            $dateStart = $dt;

            if ($dateStart > $dtult) {
                $zera = 'S';
            } else {
                $zera = 'N';
            }
        }
        if ($zera == 'S') { //zera ranking caso seja virada do mes
            if (Rankingconquista::where('idunidade', $request->idunidade)->update(['nrposicao' => 0], ['qtconquista' => 0])) {
                $retorno['text'] = 'Ranking iniciado!';
            } else {
                $retorno['text'] = 'Ocorreu um erro ao iniciar!';
            }

            if (Conquistausuario::where('idunidade', $request->idunidade)->update(['qtconquista' => 0])) {
                $retorno['text'] = 'Conquistas usuarios iniciado!';
            } else {
                $retorno['text'] = 'Ocorreu um erro ao iniciar!';
            }
        }
        ///atualiza
        $posNova = 1;
        $sql = "select rk.idaluno from rankingconquista rk, conquistausuario cqu where";
        $sql .= " rk.idunidade = " . $request->idunidade;
        $sql .= " and rk.idconquista = " . $request->idconquista;
        $sql .= " and rk.idunidade = cqu.idunidade";
        $sql .= " and rk.idaluno = cqu.idaluno";
        $sql .= " and rk.idconquista = cqu.idconquista";
        $sql .= " and cqu.stparticipa = 'S'";
        $sql .= " order by rk.qtconquista desc";

        $ranks = DB::select($sql);


        foreach ($ranks as $key => $value) {
            $aluno = $ranks[$key]->idaluno;
            if (Rankingconquista::where('idunidade', $request->idunidade)
                            ->where('idaluno', $aluno)
                            ->where('idconquista', $request->idconquista)
                            ->update(['nrposicao' => $posNova])) {
                $retorno['text'] = 'Ranking iniciado!';
            } else {
                $retorno['text'] = 'Ocorreu um erro ao iniciar!';
            }
            $posNova = $posNova + 1;
        }

        //aqui
        $zera = 'N'; // QDO DESTRAVAR REMOVER ESSA LINHA
        $sql1 = "select tp.tpmedida,rk.nrposicao, rk.idaluno, rk.idunidade, rk.idconquista, rk.qtconquista, a.name";
        $sql1 .= " from rankingconquista as rk,conquistausuario as cqu, conquista as c,tipoconquista as tp, users as a";
        $sql1 .= " where rk.idunidade = " . $request->idunidade;
        $sql1 .= " and rk.idunidade = cqu.idunidade";
        $sql1 .= " and rk.idaluno = cqu.idaluno";
        $sql1 .= " and rk.idconquista = cqu.idconquista";
        $sql1 .= " and cqu.stparticipa = 'S'";
        $sql1 .= " and rk.idconquista = " . $request->idconquista;
        $sql1 .= " and rk.idunidade = a.idunidade";
        $sql1 .= " and c.id = rk.idconquista";
        $sql1 .= " and c.idtipoconquista = tp.id and rk.qtconquista > 0 ";
        $sql1 .= " and a.id = rk.idaluno order by rk.nrposicao asc";


        $rks = DB::select($sql1);
        $rankings = [];
        foreach ($rks as $key => $value) {
            $rankings[$key]['FOTOALUNO'] = 'http://sistemapcb.com.br/uploads/avatars/user-' . $rks[$key]->idaluno . '.jpg';
            $rankings[$key]['IDALUNO'] = $rks[$key]->idaluno;
            $rankings[$key]['NMALUNO'] = $rks[$key]->name;
            $rankings[$key]['IDUNIDADE'] = $rks[$key]->idunidade;
            $rankings[$key]["IDCONQUISTA"] = $rks[$key]->idconquista;
            if ($zera == 'S') {
                $rankings[$key]["NRPOSICAO"] = 0;
            } else {
                $rankings[$key]["NRPOSICAO"] = $rks[$key]->nrposicao;
            }

            $numer = $rks[$key]->qtconquista;

            if ($rks[$key]->tpmedida == "ton") {
                $rankings[$key]["UNIDADE"] = $rks[$key]->tpmedida;
                $numer = $rks[$key]->qtconquista;
                $rankings[$key]["QTCONQUISTA"] = str_replace(".", ",", number_format($numer, 1));
            } else {
                if ($rks[$key]->tpmedida == "m") {
                    $rankings[$key]["UNIDADE"] = $rks[$key]->tpmedida;
                    $rankings[$key]["QTCONQUISTA"] = number_format($rks[$key]->qtconquista, 0, ',', '.');
                } else {
                    if ($rks[$key]->tpmedida == "min") {
                        $rankings[$key]["UNIDADE"] = $rks[$key]->tpmedida;
                        $rankings[$key]["QTCONQUISTA"] = number_format($rks[$key]->qtconquista, 2, ':', ',');
                    } else {
                        if ($rks[$key]->tpmedida == "km") {
                            $rankings[$key]["UNIDADE"] = $rks[$key]->tpmedida;
                            $rankings[$key]["QTCONQUISTA"] = number_format($rks[$key]->qtconquista, 2, ',', '.');
                        } else {
                            $rankings[$key]["UNIDADE"] = " " . $rks[$key]->tpmedida;
                            $rankings[$key]["QTCONQUISTA"] = number_format($rks[$key]->qtconquista, 0, '', ',');
                        }
                    }
                }
            }
        }

        return $rankings;
    }

    public function exibirRanking(Request $request) {
        // aqui    
        // 
        //		$modalidades = Atividade::select('id', 'nmatividade', 'stmenuapp', 'linkapp')->get();

        $dtHj = date('Y-m-d H:i:s');

        $sql = "select max(dtregistro)  as dtult from movatividade where idunidade = " . $request->idunidade;
        $sql .= " and idatividade = " . $request->idconquista;

        $dt = DB::select($sql);


        $dtult = $dt;
        $monthUlt = date("m", strtotime($dt[0]->dtult));
        $yearUlt = date("Y", strtotime($dt[0]->dtult));

        $mesAtual = date("m", strtotime(date('Y-m-d')));
        $anoAtual = date("Y", strtotime(date('Y-m-d')));

        $zera = 'N';
        if ($request->idconquista != 4) {
            if ($yearUlt < $anoAtual) {
                $zera = 'S';
            } else {
                if ($yearUlt == $anoAtual) {
                    if ($monthUlt < $mesAtual) {
                        $zera = 'S';
                    } else {
                        $zera = 'N';
                    }
                }
            }
        } else {
            /////////////////////////////////////////////


            $dtHj = date('Y-m-d', strtotime(date('Y-m-d')));
            $anoUlt = substr($dtHj, 0, 4);
            $mesUlt = substr($dtHj, 5, 2);
            $diaUlt = substr($dtHj, 8, 2);
            $dsUlt = date("w", mktime($anoUlt, $diaUlt, $mesUlt));
            $diasemanaUlt = $dsUlt + 1;
            $difDias = ($diasemanaUlt - 1);

            $dt = date('Y-m-d', strtotime("-" . $difDias . " days"));
            /////////////////////////////////////////////////////////////////////////

            $dateStart = $dt;

            /* if ($dateStart > $dtult) {
              $zera = 'S';
              } else {
              $zera = 'N';
              } */
        }
        //$zera='S';
        //echo $zera;
        if ($zera == 'S') { //zera ranking caso seja virada do mes
            if (Rankingconquista::where('idunidade', $request->idunidade)->where('idconquista', $request->idconquista)->update(['nrposicao' => 0], ['qtconquista' => 0])) {
                $retorno['text'] = 'Ranking iniciado!';
            } else {
                $retorno['text'] = 'Ocorreu um erro ao iniciar!';
            }

            if (Conquistausuario::where('idunidade', $request->idunidade)->where('idconquista', $request->idconquista)->update(['qtconquista' => 0])) {
                $retorno['text'] = 'Conquistas usuarios iniciado!';
            } else {
                $retorno['text'] = 'Ocorreu um erro ao iniciar!';
            }
        }
        ///atualiza
        $posNova = 1;
        $sql = "select rk.idaluno from rankingconquista rk, conquistausuario cqu where";
        $sql .= " rk.idunidade = " . $request->idunidade;
        $sql .= " and rk.idconquista = " . $request->idconquista;
        $sql .= " and rk.idunidade = cqu.idunidade";
        $sql .= " and rk.idaluno = cqu.idaluno";
        $sql .= " and rk.idconquista = cqu.idconquista";
        $sql .= " and cqu.stparticipa = 'S'";
        $sql .= " order by rk.qtconquista desc";

        $ranks = DB::select($sql);


        foreach ($ranks as $key => $value) {
            $aluno = $ranks[$key]->idaluno;
            if (Rankingconquista::where('idunidade', $request->idunidade)
                            ->where('idaluno', $aluno)
                            ->where('idconquista', $request->idconquista)
                            ->update(['nrposicao' => $posNova])) {
                $retorno['text'] = 'Ranking iniciado!';
            } else {
                $retorno['text'] = 'Ocorreu um erro ao iniciar!';
            }
            $posNova = $posNova + 1;
        }

        //aqui
        $zera = 'N'; // QDO DESTRAVAR REMOVER ESSA LINHA


        $sql1 = "select tp.tpmedida,rk.nrposicao, rk.idaluno, rk.idunidade, rk.idconquista, rk.qtconquista, a.name";
        $sql1 .= " from rankingconquista as rk,conquistausuario as cqu, conquista as c,tipoconquista as tp, users as a";
        $sql1 .= " where rk.idunidade = " . $request->idunidade;
        $sql1 .= " and rk.idunidade = cqu.idunidade";
        $sql1 .= " and rk.idaluno = cqu.idaluno";
        $sql1 .= " and rk.idconquista = cqu.idconquista";
        $sql1 .= " and cqu.stparticipa = 'S'";
        $sql1 .= " and rk.idconquista = " . $request->idconquista;
        $sql1 .= " and rk.idunidade = a.idunidade";
        $sql1 .= " and c.id = rk.idconquista";
        $sql1 .= " and c.idtipoconquista = tp.id and rk.qtconquista >= 0 ";
        $sql1 .= " and a.id = rk.idaluno order by rk.nrposicao asc";

        $rks = DB::select($sql1);

        $rankings = [];

        /* if (sizeof( $rks==0)):

          Conquistausuario::create([ 'idaluno' => $request->idaluno, 'idunidade' => $request->idunidade, 'idconquista' => $request->idconquista,'stconquista' =>'S', 'dtconquista' => 1, 'qtconquista' => 0, 'stparticipa' => 'S','dtregistro' => $dtHj]);
          Rankingconquista::create([ 'idaluno' => $request->idaluno, 'idunidade' => $request->idunidade, 'idconquista' => $request->idconquista, 'nrposicao' => 1, 'qtconquista' => 0, 'dtregistro' => $dtHj]);



          endif; */


        foreach ($rks as $key => $value) {
            $rankings[$key]['TPMEDIDA'] = $rks[$key]->tpmedida;
            $rankings[$key]['FOTOALUNO'] = 'http://sistemapcb.com.br/uploads/avatars/user-' . $rks[$key]->idaluno . '.jpg';

            if (file_exists('uploads/avatars/user-' . $rks[$key]->idaluno . '.jpg')) {
                $rankings[$key]['FOTOALUNO'] = $rankings[$key]['FOTOALUNO'];
            } else {
                $rankings[$key]['FOTOALUNO'] = 'http://sistemapcb.com.br/uploads/avatars/a4.jpg';
            }
            $rankings[$key]['IDALUNO'] = $rks[$key]->idaluno;
            $rankings[$key]['NMALUNO'] = ucwords(strtolower($rks[$key]->name));
            $rankings[$key]['IDUNIDADE'] = $rks[$key]->idunidade;
            $rankings[$key]["IDCONQUISTA"] = $rks[$key]->idconquista;
            if ($zera == 'S') {
                $rankings[$key]["NRPOSICAO"] = 0;
            } else {
                $rankings[$key]["NRPOSICAO"] = $rks[$key]->nrposicao;
            }

            $numer = $rks[$key]->qtconquista;

            if ($rks[$key]->tpmedida == "ton") {
                $rankings[$key]["UNIDADE"] = $rks[$key]->tpmedida;
                $numer = $rks[$key]->qtconquista;
                $rankings[$key]["QTCONQUISTA"] = str_replace(".", ",", number_format($numer, 1));
            } else {
                if ($rks[$key]->tpmedida == "m") {
                    $rankings[$key]["UNIDADE"] = $rks[$key]->tpmedida;
                    $rankings[$key]["QTCONQUISTA"] = number_format($rks[$key]->qtconquista, 0, ',', '.');
                } else {
                    if ($rks[$key]->tpmedida == "min") {
                        $rankings[$key]["UNIDADE"] = $rks[$key]->tpmedida;
                        $rankings[$key]["QTCONQUISTA"] = number_format($rks[$key]->qtconquista, 2, ':', ',');
                    } else {
                        if ($rks[$key]->tpmedida == "km") {
                            $rankings[$key]["UNIDADE"] = $rks[$key]->tpmedida;
                            $rankings[$key]["QTCONQUISTA"] = number_format($rks[$key]->qtconquista, 2, ',', '.');
                        } else {
                            $rankings[$key]["UNIDADE"] = " " . $rks[$key]->tpmedida;
                            $rankings[$key]["QTCONQUISTA"] = number_format($rks[$key]->qtconquista, 0, '', ',');
                        }
                    }
                }
            }
        }
        $pos = 0;
        $idx = 0;
        foreach ($rankings as $key => $value) {
            if ($rankings[$key]['IDALUNO'] == $request->idaluno) {
                $pos = $rankings[$key]['NRPOSICAO'];
                $idx = $key;
            }
        }
        $rx1 = [];
        $ini = 0;
        $fim = 2;
        if ($pos > 6) {
            for ($n = $ini; $n <= $fim; $n++) {
                $rx1[$n]['TPMEDIDA'] = $rankings[$n]['TPMEDIDA'];
                $rx1[$n]['UNIDADE'] = $rankings[$n]['UNIDADE'];
                $rx1[$n]['FOTOALUNO'] = $rankings[$n]['FOTOALUNO'];
                $rx1[$n]['IDALUNO'] = $rankings[$n]['IDALUNO'];
                $rx1[$n]['NMALUNO'] = $rankings[$n]['NMALUNO'];
                $rx1[$n]['IDUNIDADE'] = $rankings[$n]['IDUNIDADE'];
                $rx1[$n]["IDCONQUISTA"] = $rankings[$n]['IDCONQUISTA'];
                $rx1[$n]["NRPOSICAO"] = $rankings[$n]['NRPOSICAO'];
                $rx1[$n]["QTCONQUISTA"] = $rankings[$n]['QTCONQUISTA'];
            }
            //separador
            $rx1[3]['FOTOALUNO'] = 0;
            $rx1[3]['IDALUNO'] = 0;
            $rx1[3]['NMALUNO'] = '';
            $rx1[3]['IDUNIDADE'] = $rankings[$n]['IDUNIDADE'];
            $rx1[3]["IDCONQUISTA"] = 0;
            $rx1[3]["NRPOSICAO"] = 0;
            $rx1[3]["QTCONQUISTA"] = 0;
        }
        $rx2 = [];
        $ini = 0;
        $fim = $idx;
        if ($idx >= 6) {
            $ini = $idx - 2;
            $fim = $idx + 2;
        }
        /* echo $ini . '-';
          echo $fim . '-';
          echo $idx . '-'; */
        $rx3 = [];
        if (sizeof($rankings) > 0) {
            for ($n = $ini; $n <= $fim; $n++) {
                $rx2[$n]['TPMEDIDA'] = $rankings[$n]['TPMEDIDA'];
                $rx2[$n]['UNIDADE'] = $rankings[$n]['UNIDADE'];
                $rx2[$n]['FOTOALUNO'] = $rankings[$n]['FOTOALUNO'];
                $rx2[$n]['IDALUNO'] = $rankings[$n]['IDALUNO'];
                $rx2[$n]['NMALUNO'] = $rankings[$n]['NMALUNO'];
                $rx2[$n]['IDUNIDADE'] = $rankings[$n]['IDUNIDADE'];
                $rx2[$n]["IDCONQUISTA"] = $rankings[$n]['IDCONQUISTA'];
                $rx2[$n]["NRPOSICAO"] = $rankings[$n]['NRPOSICAO'];
                $rx2[$n]["QTCONQUISTA"] = $rankings[$n]['QTCONQUISTA'];
            }
            $fim = $fim + 1;


            for ($n = $fim; $n < sizeof($rankings); $n++) {

                $rx3[$n]['FOTOALUNO'] = $rankings[$n]['FOTOALUNO'];
                $rx3[$n]['UNIDADE'] = $rankings[$n]['UNIDADE'];
                $rx3[$n]['IDALUNO'] = $rankings[$n]['IDALUNO'];
                $rx3[$n]['NMALUNO'] = $rankings[$n]['NMALUNO'];
                $rx3[$n]['IDUNIDADE'] = $rankings[$n]['IDUNIDADE'];
                $rx3[$n]["IDCONQUISTA"] = $rankings[$n]['IDCONQUISTA'];
                $rx3[$n]["NRPOSICAO"] = $rankings[$n]['NRPOSICAO'];
                $rx3[$n]["QTCONQUISTA"] = $rankings[$n]['QTCONQUISTA'];
            }
        }
        $rankings = [];
        $rankings = array_merge($rx1, $rx2, $rx3);




        return $rankings;
    }

    public function exibirPosicao(Request $request) {
        $aluno = $request->idaluno;
        $unidade = $request->idunidade;
        $conquista = $request->idconquista;

        $totConquista = 0;
        $posicao = 0;


////////////////////////////////////ATUALIZA RANKING      
        /*  $posNova=1;
          $sql="select rk.idaluno from rankingconquista rk, conquistausuario cqu where";
          $sql .= " rk.idunidade = " . $unidade;
          $sql .= " and rk.idconquista = " . $conquista;
          $sql .= " and rk.idunidade = cqu.idunidade";
          $sql .= " and rk.idaluno = cqu.idaluno";
          $sql .= " and rk.idconquista = cqu.idconquista";
          $sql .= " and cqu.stparticipa = 'S'";
          $sql .= " order by rk.qtconquista desc";

          $con = new PDO("mysql:host={$dbhost};dbname={$dbname}", $dbuser, $dbpass);
          $con->query('SET NAMES utf8');
          $consulta = $con->query($sql);

          while($c = $consulta->fetch(PDO::FETCH_OBJ)) {
          $aluno=$c->idaluno;
          $stmt = $pdo->prepare("UPDATE rankingconquista set nrposicao = :posicao where idaluno = :aluno and idunidade = :unidade and idconquista = :conquista ");
          $stmt->execute(array(
          ':aluno' => $aluno,
          ':unidade' => $unidade,
          ':conquista' => $conquista,
          ':posicao' => $posNova

          ));
          $posNova = $posNova + 1;
          } */


        /*  $sql="select nrposicao, idaluno, idunidade, idconquista from rankingconquista where ";
          $sql .= " idaluno = " . $aluno;
          $sql .= " and idunidade = " . $unidade;
          $sql .= " and idconquista = " . $conquista; */

        $posicao = Rankingconquista::select('nrposicao', 'idaluno', 'idunidade', 'idconquista')
                ->where('idaluno', $aluno)
                ->where('idunidade', $unidade)
                ->where('idconquista', $conquista)
                ->get();

        /*   while($c = $consulta->fetch(PDO::FETCH_OBJ)) {  
          $row_array['IDALUNO'] = $c->idaluno;
          $row_array['IDUNIDADE'] = $c->idunidade;
          $row_array["IDCONQUISTA"]= $c->idconquista;
          $row_array["NRPOSICAO"]= $c->nrposicao;
          array_push($return_arr,$row_array);
          }


          printf( json_encode($return_arr)); */


        return $posicao;
    }

    public function listarTreino($id_treino, $letra) {
        // $treinodia = TreinoFicha::where('treino_id', $id_treino)->get();
        $treinodia = TreinoFicha::select(DB::raw('coalesce(exercicio.nmexercicio, "NÃO INFORMADO") as nmexercicio'), DB::raw('coalesce(exercicio.dsurlminiatura, "") as dsurlminiatura'), DB::raw('coalesce(exercicio.video, "") as video'), 'exercicio.video', 'treino_ficha.*')
                ->LEFTJOIN('exercicio', 'treino_ficha.exercicio_id', '=', 'exercicio.id')
                ->where('treino_ficha.treino_id', $id_treino)
                ->where('treino_ficha.ficha_letra', $letra)
                ->where('treino_ficha.ficha_id_vinculo', 0)
                ->orderby('treino_ficha.ord')
                ->get();


        foreach ($treinodia as $key => $value) {

            $treinodia[$key]['superserie'] = TreinoFicha::select(DB::raw('coalesce(exercicio.nmexercicio, "NÃO INFORMADO") as nmexercicio'), DB::raw('coalesce(exercicio.dsurlminiatura, "") as dsurlminiatura'), DB::raw('coalesce(exercicio.video, "") as video'), 'exercicio.video', 'treino_ficha.*')
                    ->LEFTJOIN('exercicio', 'treino_ficha.exercicio_id', '=', 'exercicio.id')
                    ->where('treino_ficha.treino_id', $id_treino)
                    ->where('treino_ficha.ficha_id_vinculo', $treinodia[$key]['ficha_id'])
                    ->where('treino_ficha.ficha_letra', $letra)
                    ->orderby('treino_ficha.ord')
                    ->get();
        }


        return $treinodia;
    }

    public function atualizaUpdateAt($idaluno, $idunidade) {


        Totem::where('idunidade', $idunidade)
                ->where('idaluno', $idaluno)
                ->where('dtentrada', date('Y-m-d'))
                ->update(['updated_at' => date('Y-m-d H:i:s')]);


        $alunosTotem = Totem::where('dtentrada', date('Y-m-d'))
                ->where('idunidade', $idunidade)
                ->get();

        foreach ($alunosTotem as $key => $value) {


            $dateStart = new \DateTime($alunosTotem[$key]->updated_at);
            $dateNow = new \DateTime(date('Y-m-d H:i:s'));
            $dateDiff = $dateStart->diff($dateNow);


            $alunosTotem[$key]->tempoinativo = ($dateDiff->h * 60 * 60) + ($dateDiff->h * 60) + $dateDiff->s;
            if ($alunosTotem[$key]->tempoinativo > 3600) {
                $ret = $this->deleteClienteTotem($alunosTotem[$key]->idaluno, $idunidade);
            }
        }

        return $alunosTotem;
    }

    public function prescricaoCalendario($atividade, $idaluno) {
        $prescicoesAluno = PrescricaoCalendario::where('user_id_aluno', $idaluno)
                ->where('tipo_prescricao', $atividade)
                ->where('data_prescricao', DB::raw('CURRENT_DATE'))
                ->get();
        foreach ($prescicoesAluno as $key => $value) {
            if ($prescicoesAluno[$key]->treino_padrao_id > 0) {
                $var_prescr = PrescricaoCalendarioPadrao::where('idtreino', $prescicoesAluno[$key]->treino_padrao_id)
                        ->where('tipo_prescricao', $atividade)
                        ->where('data_prescricao', DB::raw('CURRENT_DATE'))
                        ->get();
                $prescicoesAluno[$key]->desc_prescricao = $var_prescr[0]->desc_prescricao;
            }
        }

        return $prescicoesAluno;
    }

    public function authenticate(Request $request) {
        //dd($request->all());
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            $user = Auth::user();
            // Authentication passed...
            return $user;
        }
        return ['msg' => 'Email ou senha incorretos'];
    }

    public function psaListNames($idAtv) {
        $ret = Programa::where('id', $idAtv)->first();

        return $ret->nmprograma;
    }

    public function psaList($idaluno) {
        $psas = PsaAtividade::select('id', 'dom_atv', DB::raw('DATE_FORMAT(dom_hora, "%H:%i") as dom_hora'), 'seg_atv', DB::raw('DATE_FORMAT(seg_hora, "%H:%i") as seg_hora'), 'ter_atv', DB::raw('DATE_FORMAT(ter_hora, "%H:%i") as ter_hora'), 'qua_atv', DB::raw('DATE_FORMAT(qua_hora, "%H:%i") as qua_hora'), 'qui_atv', DB::raw('DATE_FORMAT(qui_hora, "%H:%i") as qui_hora'), 'sex_atv', DB::raw('DATE_FORMAT(sex_hora, "%H:%i") as sex_hora'), 'sab_atv', DB::raw('DATE_FORMAT(sab_hora, "%H:%i") as sab_hora'))
                        ->where('idaluno', $idaluno)->orderby('id', 'asc')->get();

        if (count($psas) < 1) {
            return '';
        }

        foreach ($psas as $key => $value) {
            // $arr[3] será atualizado com cada valor de $arr...
            if ($value->dom_atv) {
                $psas[$key]->dom_atv = $this->psaListNames($value->dom_atv);
            }
            if ($value->seg_atv) {
                $psas[$key]->seg_atv = $this->psaListNames($value->seg_atv);
            }
            if ($value->ter_atv) {
                $psas[$key]->ter_atv = $this->psaListNames($value->ter_atv);
            }
            if ($value->qua_atv) {
                $psas[$key]->qua_atv = $this->psaListNames($value->qua_atv);
            }
            if ($value->qui_atv) {
                $psas[$key]->qui_atv = $this->psaListNames($value->qui_atv);
            }
            if ($value->sex_atv) {
                $psas[$key]->sex_atv = $this->psaListNames($value->sex_atv);
            }
            if ($value->sab_atv) {
                $psas[$key]->sab_atv = $this->psaListNames($value->sab_atv);
            }
        }



        return $psas;
    }

    public function enviarContato(Request $request) {

//        dd($request->all());
        $academia = Unidade::select('unidade.fantasia', 'unidade_dados.email')
                ->where('unidade.id', $request->unidade)
                ->JOIN('unidade_dados', 'unidade_dados.idunidade', '=', 'unidade.id')
                ->first();


        $data = array(
            'name' => $request->name,
            'mensagem' => $request->mensagem,
            'email' => $request->email,
            'academia' => $academia->fantasia
        );

//        dd($data);   'email_proprietario' => $academia->email
        $dados_use = [
            'name_cliente' => $request->name,
            'email_cliente' => $request->email,
            'academia' => $academia->fantasia,
            'email_proprietario' => 'karstenschwab@gmail.com'
        ];


        Mail::send('formstemplates.contato_app', $data, function ($message) use ($dados_use) {

            $message->from('contato@personalclubbrasil.com.br', $name = 'Academia ' . $dados_use['academia']);
            $message->replyTo($dados_use['email_cliente'], $name = $dados_use['name_cliente']);
            $message->to($dados_use['email_proprietario'])->subject('Contato do Cliente - App');
        });

        $sucesso = 'Sua mensagem foi enviada com sucesso!';

        return $sucesso;
    }

    public function enviarqueroentraremforma(Request $request) {

//        dd($request->all());
        $academia = Unidade::select('unidade.fantasia', 'unidade_dados.email')
                ->where('unidade.id', $request->unidade)
                ->JOIN('unidade_dados', 'unidade_dados.idunidade', '=', 'unidade.id')
                ->first();


        $data = array(
            'name' => $request->name,
            'mensagem' => $request->mensagem,
            'email' => $request->email,
            'academia' => $academia->fantasia
        );

//        dd($data);   'email_proprietario' => $academia->email
        $dados_use = [
            'name_cliente' => $request->name,
            'email_cliente' => $request->email,
            'academia' => $academia->fantasia,
            'email_proprietario' => $academia->email
        ];


        Mail::send('formstemplates.contato_app', $data, function ($message) use ($dados_use) {

            $message->from('contato@personalclubbrasil.com.br', $name = 'Academia ' . $dados_use['academia']);
            $message->replyTo($dados_use['email_cliente'], $name = $dados_use['name_cliente']);
            $message->to($dados_use['email_proprietario'])->subject('Contato do Cliente - App');
        });

        $sucesso = 'Sua mensagem foi enviada com sucesso!';

        return $sucesso;
    }

    public function getNutricionista($idunidade) {
        $funcionarios = Funcionario::select('users.id', 'users.name', 'user_dados.situacao', 'user_dados.site')
                ->where('users.idunidade', $idunidade)
                ->where('users.role', 'coach')
                ->where('user_dados.situacao', 'A')
                ->leftJoin('user_dados', 'user_dados.user_id', '=', 'users.id')
                ->get();
        foreach ($funcionarios as $funcionario) {
            $funcionario->name = ucwords(strtolower($funcionario->name));
        }
        return $funcionarios;
    }

    /**

     */
    public function projetoListEtapa($projeto_id) {
        if ($projeto_id) {
            $etapas = PrescricaoEtapa::select('id', 'etapa_nome', 'etapa_data', 'etapa_status', 'created_at')
                    ->where('projeto_id', $projeto_id)
                    ->orderby('id', 'DESC')
                    ->get();


            return $etapas;
        }
        return false;
    }

    public function inverteData($data) {
        if (count(explode("/", $data)) > 1) {
            return implode("-", array_reverse(explode("/", $data)));
        } elseif (count(explode("-", $data)) > 1) {
            return implode("/", array_reverse(explode("-", $data)));
        }
    }

    public function projetoList($idaluno) {
        $projetos = PrescricaoProjeto::select('id', 'projeto_nome', 'projeto_data', 'projeto_status', DB::raw('DATE_FORMAT(created_at, "%d/%m/%Y") as dt_cadastro'))
                ->where('aluno_id', $idaluno)
                ->where('projeto_status', 'A')
                ->get();


        if (count($projetos) > 0) {
            $data1 = $this->inverteData($projetos[0]->dt_cadastro);
//        echo '<hr>';
            $data2 = $this->inverteData($projetos[0]->projeto_data);
//        echo '<hr>';

            $date1 = date_create($data1);
            $date2 = date_create($data2);
            $diff = date_diff($date1, $date2);

            $total_dias = $diff->format("%a");
//        echo '<hr>';
            $hoje = date('Y-m-d');
            $data3 = date_create($hoje);
            $diff2 = date_diff($date1, $data3);

            $dias_hoje = $diff2->format("%a");
            $x = 0;
            if ($total_dias > 0) {
                $x = ($dias_hoje * 100) / $total_dias;
            }

            $x = ($x > 100) ? 100 : $x;


            $media = number_format($x, 0, '.', '');
            $projetos[0]['media'] = $media;


            return $projetos;
        }
        return $projetos;
    }
    
    

    public function getModalidades($idunidade, $idaluno) {
        //		$modalidades = Atividade::select('id', 'nmatividade', 'stmenuapp', 'linkapp')->get();

        $modalidades = DB::select("SELECT atividade.id, nmatividade, stmenuapp, linkapp, conquista.id,coalesce(rankingconquista.idaluno," . $idaluno . ") as idaluno, coalesce(stparticipa,'N') as stparticipa, "
                        . " coalesce(rankingconquista.nrposicao,0) as nrposicao FROM atividade left join conquista on atividade.id = conquista.idatividade "
                        . " left join conquistausuario on conquistausuario.idconquista = conquista.id"
                        . " and conquistausuario.idaluno = " . $idaluno
                        . " and conquistausuario.idunidade = " . $idunidade
                        . " left join rankingconquista on rankingconquista.idconquista = conquistausuario.idconquista"
                        . " and rankingconquista.idaluno = conquistausuario.idaluno"
                        . " and rankingconquista.idunidade = conquistausuario.idunidade order by atividade.id");

        $blacklists = AtividadeBlacklist::select('atividade_id')->where('unidade_id', $idunidade)->get();

        foreach ($modalidades as $modalidade) {
            foreach ($blacklists as $blacklist) {
                if ($blacklist->atividade_id == $modalidade->id) {
                    $modalidade->stmenuapp = 'N';
                }
            }
        }

        return $modalidades;
    }

    // JSON GFM
    public function getAulasDia($idunidade) {
        $diasemana = date('w', strtotime(date('Y-m-d'))) + 1;
        $gfms = Gfm::select('gfm.id', 'gfm.hora_inicio', 'gfm.capacidade', 'users.name as nmprofessor', 'programa.nmprograma', 'local.nmlocal', 'gfm_reserva.n_reservas')
                        ->leftJoin('users', 'users.id', '=', 'gfm.idfuncionario')
                        ->leftJoin('programa', 'programa.id', '=', 'gfm.idprograma')
                        ->leftJoin('local', 'local.id', '=', 'gfm.idlocal')
                        ->leftJoin('gfm_reserva', 'gfm_reserva.idgfm', '=', 'gfm.id')
                        ->where('gfm.idunidade', $idunidade)
                        ->where('gfm.diasemana', $diasemana)->get();
        return $gfms;
    }

    public function getAulasDiaSemana($idunidade, $dia) {
        switch ($dia) {
            case 'Dom':
                $diasemana = 1;
                break;
            case 'Seg':
                $diasemana = 2;
                break;
            case 'Ter':
                $diasemana = 3;
                break;
            case 'Qua':
                $diasemana = 4;
                break;
            case 'Qui':
                $diasemana = 5;
                break;
            case 'Sex':
                $diasemana = 6;
                break;
            case 'Sab':
                $diasemana = 7;
                break;
        }
        $gfms = Gfm::select('gfm.id', 'gfm.hora_inicio', 'gfm.capacidade', 'users.name as nmprofessor', 'programa.nmprograma', 'local.nmlocal', 'gfm_reserva.n_reservas', 'gfm_reserva.id as reserva_id')
                ->leftJoin('users', 'users.id', '=', 'gfm.idfuncionario')
                ->leftJoin('programa', 'programa.id', '=', 'gfm.idprograma')
                ->leftJoin('local', 'local.id', '=', 'gfm.idlocal')
                ->leftJoin('gfm_reserva', 'gfm_reserva.idgfm', '=', 'gfm.id')
                ->where('gfm.idunidade', $idunidade)
                ->where('gfm.diasemana', 'LIKE', '%' . $diasemana . '%')
                ->orderBy('gfm.hora_inicio', 'ASC')
                ->get();


        return $gfms;
    }

    public function getAulasDiaSemanaPainel() {

        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        //'gfm_reserva.n_reservas', 'gfm_reserva.id as reserva_id')
        $gfms = Gfm::select('gfm.id', 'gfm.hora_inicio', 'gfm.capacidade', 'users.name as nmprofessor', 'programa.nmprograma', 'local.nmlocal')
                ->leftJoin('users', 'users.id', '=', 'gfm.idfuncionario')
                ->leftJoin('programa', 'programa.id', '=', 'gfm.idprograma')
                ->leftJoin('local', 'local.id', '=', 'gfm.idlocal')
                ->where('gfm.idunidade', $idunidade)
                ->orderBy('gfm.hora_inicio', 'ASC')
                ->get();

        foreach ($gfms as $key => $value) {
            $dtAula = $this->getDataAula($gfms[$key]['id']);
            $gfms[$key]['dtaula'] = $dtAula;

            $reservas = Reserva::select('n_reservas')->where('idgfm', $gfms[$key]['id'])->where('dtaula', $dtAula)->get();
            $gfms[$key]['n_reservas'] = isset($reservas[0]['n_reservas']) ? $reservas[0]['n_reservas'] : $gfms[$key]['capacidade'];
            $reservou = LogReserva::select('idaluno')->where('idaluno', $idaluno)->where('idgfm', $gfms[$key]['id'])->where('dtaula', $dtAula)->get();
            if (sizeof($reservou) > 0) {
                $gfms[$key]['selecionou'] = 'S';
            } else {
                $gfms[$key]['selecionou'] = 'N';
            }
        }

        return $gfms;
    }

    public function getAulasDiaSemana_New($idunidade, $dia, $idaluno, $tipo = 'Gfm') {
        switch ($dia) {
            case 'Dom':
                $diasemana = 1;
                break;
            case 'Seg':
                $diasemana = 2;
                break;
            case 'Ter':
                $diasemana = 3;
                break;
            case 'Qua':
                $diasemana = 4;
                break;
            case 'Qui':
                $diasemana = 5;
                break;
            case 'Sex':
                $diasemana = 6;
                break;
            case 'Sab':
                $diasemana = 7;
                break;
        }
        //'gfm_reserva.n_reservas', 'gfm_reserva.id as reserva_id')
        $gfms = Gfm::select('gfm.id', 'gfm.privado', 'gfm.gfmspm', 'gfm.hora_inicio', 'gfm.capacidade', 'users.name as nmprofessor', 'programa.nmprograma', 'local.nmlocal')
                ->leftJoin('users', 'users.id', '=', 'gfm.idfuncionario')
                ->leftJoin('programa', 'programa.id', '=', 'gfm.idprograma')
                ->leftJoin('local', 'local.id', '=', 'gfm.idlocal')
                ->where('gfm.idunidade', $idunidade)
                ->whereIn('gfm.gfmspm', ['Ambos', $tipo])
                ->where('gfm.diasemana', 'LIKE', '%' . $diasemana . '%')
                ->where('gfm.privado', 'LIKE', '%N%')
                ->orderBy('gfm.hora_inicio', 'ASC')
                ->get();



        foreach ($gfms as $key => $value) {
            $dtAula = $this->getDataAula($gfms[$key]['id'], $diasemana);
            $gfms[$key]['dtaula'] = $dtAula;
            $qtdReserva = 0;
            $reservas = Reserva::select('n_reservas')->where('idgfm', $gfms[$key]['id'])->where('dtaula', $dtAula)->get();
            if (sizeof($reservas) > 0):
                $qtdReserva = $reservas[0]['n_reservas'];
            else:
                $qtdReserva = $gfms[$key]['capacidade'];

            endif;

            $gfms[$key]['n_reservas'] = $qtdReserva; //isset($reservas[0]['n_reservas']) ? $reservas[0]['n_reservas'] : $gfms[$key]['capacidade'];
            $reservou = LogReserva::select('idaluno')->where('idaluno', $idaluno)->where('idgfm', $gfms[$key]['id'])->where('dtaula', $dtAula)->get();
            if (sizeof($reservou) > 0) {
                $gfms[$key]['selecionou'] = 'S';
            } else {
                $gfms[$key]['selecionou'] = 'N';
            }
        }

        return $gfms;
    }

    public function getListaespera($idgfm) {





        $espera = Listaespera::select('lista_espera_gfm.*', 'users.name', 'users.id', 'users.avatar')
                ->leftJOIN('users', 'users.id', '=', 'lista_espera_gfm.idaluno')
                ->where('lista_espera_gfm.idgfm', '=', $idgfm)
                ->orderBy('lista_espera_gfm.id')
                ->get();


        return response()->json(compact('espera'));
    }

    public function getListareserva($id, $dias = 'XXX') {

        switch ($dias) {
            case 'Dom':
                $diasemana = 1;
                break;
            case 'Seg':
                $diasemana = 2;
                break;
            case 'Ter':
                $diasemana = 3;
                break;
            case 'Qua':
                $diasemana = 4;
                break;
            case 'Qui':
                $diasemana = 5;
                break;
            case 'Sex':
                $diasemana = 6;
                break;
            case 'Sab':
                $diasemana = 7;
                /* case 'XXX':
                  $diasemana = 0; */
                break;
        }

        $dtHj = date('Y-m-d');
        $ano = substr($dtHj, 0, 4);
        $mes = substr($dtHj, 5, 2);
        $dia = substr($dtHj, 8, 2);
        $ds = date("w", mktime($ano, $dia, $mes));
        $diasem = $ds + 1;

        if ($diasemana == 0) {
            $diasemana = $diasem;
        }


        $dtAula = $this->getDataAula($id, $diasemana);
        //echo $dtAula;
        //$dtAula = date('Y-m-d');//$this->getDataAula($id,0);

        $reservas = LogReserva::select('gfm_reserva_log.*', 'gfm_reserva_log.id as log_id', 'users.name', 'users.id', 'users.avatar')
                ->leftJOIN('users', 'users.id', '=', 'gfm_reserva_log.idaluno')
                ->where('gfm_reserva_log.idgfm', '=', $id)
                ->where('gfm_reserva_log.dtaula', '=', $dtAula)
                ->orderBy('gfm_reserva_log.created_at')
                ->get();

        $reservasdados = Reserva::select('gfm_reserva.*')
                ->where('gfm_reserva.idgfm', '=', $id)
                ->where('gfm_reserva.dtaula', '=', $dtAula)
                ->get();

        foreach ($reservasdados as $key => $value) {
            $reservasdados[$key]->dtaula = date('d/m/Y', strtotime($reservasdados[$key]->dtaula));
        }

        foreach ($reservas as $key => $value) {
            $reservas[$key]->avatar = $this->verificarArquivo($reservas[$key]->avatar);
            $reservas[$key]->name = ucwords(strtolower($reservas[$key]->name));
            if ($reservas[$key]->name == ''):
                $reservas[$key]->name = "ANÔNIMO";
            endif;
        }

        return response()->json(compact('reservas', 'reservasdados'));
    }

    public function getListareservaPainel($id, $dias = 'XXX') {

        switch ($dias) {
            case 'Dom':
                $diasemana = 1;
                break;
            case 'Seg':
                $diasemana = 2;
                break;
            case 'Ter':
                $diasemana = 3;
                break;
            case 'Qua':
                $diasemana = 4;
                break;
            case 'Qui':
                $diasemana = 5;
                break;
            case 'Sex':
                $diasemana = 6;
                break;
            case 'Sab':
                $diasemana = 7;
            case 'XXX':
                $diasemana = 0;
                break;
        }
        $dtHj = date('Y-m-d');
        $ano = substr($dtHj, 0, 4);
        $mes = substr($dtHj, 5, 2);
        $dia = substr($dtHj, 8, 2);
        $ds = date("w", mktime($ano, $dia, $mes));
        $diasem = $ds + 1;

        if ($diasemana == 0) {
            $diasemana = $diasem;
        }

        $gfm = Gfm::select('gfm.id', 'gfm.idunidade', 'gfm.capacidade', 'gfm.diasemana')->where('gfm.id', $id)->first();
        $diasx = explode(",", $gfm->diasemana);

        foreach ($diasx as $key => $value) {
            if ($diasx[$key] < $diasemana) {
                $dif = ($diasx[$key] - $diasemana) + 7;
            }
            if ($diasx[$key] > $diasemana) {
                $dif = ($diasx[$key] - $diasemana);
            }
            if ($diasx[$key] == $diasemana) {
                if (date('H:i:s') <= date('H:i:s', strtotime($gfm->hora_inicio))) {
                    $dif = ($diasx[$key] - $diasemana);
                } else {
                    $dif = ($diasx[$key] - $diasemana) + 7;
                }
            }

            $dt = date('Y-m-d', strtotime("+" . $dif . " days"));

            $datas[$key] = $dt;
        }


        sort($datas);
        $dtAula = $datas[0]; //$this->getDataAula($id, $diasemana);
        //if ($temAula == "S") { trava p n reservar ffora do dia

        $reserva = Reserva::select('n_reservas', 'id', 'capacidade')->where('idgfm', $id)->where('dtaula', $dtAula)->first();

        if (sizeof($reserva) == 0) :
            $dataReserva['idunidade'] = $gfm->idunidade;
            $dataReserva['idgfm'] = $gfm->id;
            $dataReserva['n_reservas'] = $gfm->capacidade;
            $dataReserva['capacidade'] = $gfm->capacidade;
            $dataReserva['dtaula'] = $dtAula;
            $reservaNew = Reserva::create($dataReserva);
            if (sizeof($reservaNew)) {

                $retorno['cod'] = '0';
                $retorno['text'] = 'Aula reservada com sucesso!';
            } else {
                $retorno['cod'] = '-1';
                $retorno['text'] = 'Ocorreu um erro ao reservar aula, tente novamente!';
            }
        endif;



        //$dtAula = date('Y-m-d');//$this->getDataAula($id,0);

        $reservas = LogReserva::select('gfm_reserva_log.*', 'users.name', 'users.id', 'users.avatar', 'gfm_reserva_log.id as registro')
                ->leftJOIN('users', 'users.id', '=', 'gfm_reserva_log.idaluno')
                ->where('gfm_reserva_log.idgfm', '=', $id)
                ->where('gfm_reserva_log.dtaula', '=', $dtAula)
                ->orderBy('gfm_reserva_log.created_at')
                ->get();

        $reservasdados = Reserva::select('gfm_reserva.*')
                ->where('gfm_reserva.idgfm', '=', $id)
                ->where('gfm_reserva.dtaula', '=', $dtAula)
                ->get();

        foreach ($reservasdados as $key => $value) {
            $reservasdados[$key]->dtaula = date('d/m/Y', strtotime($reservasdados[$key]->dtaula));
        }

        foreach ($reservas as $key => $value) {
            $reservas[$key]->avatar = $this->verificarArquivo($reservas[$key]->avatar);
            $reservas[$key]->name = ucwords(strtolower($reservas[$key]->name));
            if ($reservas[$key]->name == ''):
                $reservas[$key]->name = "ANÔNIMO";
            endif;
        }

        return response()->json(compact('reservas', 'reservasdados'));
    }

    public function getListamatricula($id, $dataaula = '2018-01-01') {


        $dtHj = date('Y-m-d');
        $ano = substr($dtHj, 0, 4);
        $mes = substr($dtHj, 5, 2);
        $dia = substr($dtHj, 8, 2);
        $ds = date("w", mktime($ano, $dia, $mes));
        $diasemana = $ds + 1;


        $gfm = Gfm::select('gfm.id', 'gfm.idunidade', 'gfm.capacidade', 'gfm.diasemana')->where('gfm.id', $id)->first();
        $diasx = explode(",", $gfm->diasemana);

        foreach ($diasx as $key => $value) {
            if ($diasx[$key] < $diasemana) {
                $dif = ($diasx[$key] - $diasemana) + 7;
            }
            if ($diasx[$key] > $diasemana) {
                $dif = ($diasx[$key] - $diasemana);
            }
            if ($diasx[$key] == $diasemana) {
                if (date('H:i:s') <= date('H:i:s', strtotime($gfm->hora_inicio))) {
                    $dif = ($diasx[$key] - $diasemana);
                } else {
                    $dif = ($diasx[$key] - $diasemana) + 7;
                }
            }

            $dt = date('Y-m-d', strtotime("+" . $dif . " days"));

            $datas[$key] = $dt;
        }


        sort($datas);
        $dtAula = $datas[0]; //$this->getDataAula($id, $diasemana);


        $sql = "select nh.nmnivel ,mt.*,u.name,u.id, grl.id as idlog,grl.dtaula, CASE WHEN grl.id is null THEN 0 WHEN grl.id > 0 THEN 1 END as presente"
                . " from matricula_turma mt left join users u "
                . " on u.id = mt.idaluno"
                . " and u.idunidade = mt.idunidade"
                . " left join gfm_reserva_log grl"
                . " on  mt.idaluno = grl.idaluno"
                . " and mt.idaula = grl.idgfm "
                . " and grl.dtaula = '" . $dtAula . "'"
                . " left join nivelhabilidade nh on nh.id = mt.idnivel"
                . " and nh.idunidade = mt.idunidade"
                . " where mt.idaula = " . $id
                . " and mt.stmatricula  ='A'"
                . " order by u.name";

        $matriculas = DB::select($sql);
        $matriculados = sizeof($matriculas);
        return response()->json(compact('matriculas', 'gfm', 'matriculados'));
    }

    function corrigeSaldo($idaula, $dtAula) {

        $total = DB::select($sql = "select coalesce(count(*),0) as total from gfm_reserva_log where idgfm = " . $idaula . " and dtaula = '" . $dtAula . "'");

        $reserva = Reserva::select('n_reservas', 'id', 'capacidade')->where('idgfm', $idaula)->where('dtaula', $dtAula)->first();
        if (sizeof($reserva) > 0) {
            if (intVal($total) > 0) {
                //$gfm = Gfm::select('gfm.capacidade')->where('gfm.id', $idaula)->first();

                $resto = intval($reserva->capacidade) - intval($total[0]->total); //--calcula capacidade menos total reservado
                if (Reserva::where('idgfm', $idaula)->update(['n_reservas' => $resto])) {
//                        LogReserva::create(['idaluno' => $idaluno, 'idgfm' => $idaula, 'reserva_id' => $reserva->id, 'dtaula' => $dtAula]);
                    $retorno['cod'] = '0';
                    $retorno['text'] = 'saldo atualizado!';
                } else {
                    $retorno['cod'] = '-1';
                    $retorno['text'] = 'Ocorreu um erro ao reservar aula, tente novamente!';
                }
            } else {
                $retorno['cod'] = '-1';
                $retorno['text'] = 'Desculpe, mas infelizmente não há mais vagas para esta aula!';
            }
        } else {
            $gfm = Gfm::select('gfm.id', 'gfm.idunidade', 'gfm.capacidade', 'gfm.diasemana')->where('gfm.id', $idaula)->first();
            $dias = explode(",", $gfm->diasemana);

            $dtHj = date('Y-m-d');
            $ano = substr($dtHj, 0, 4);
            $mes = substr($dtHj, 5, 2);
            $dia = substr($dtHj, 8, 2);
            $ds = date("w", mktime($ano, $dia, $mes));
            $diasemana = $ds + 1;
            $temAula = "N";
            foreach ($dias as $key => $value) {
                if ($dias[$key] == $diasemana):
                    $temAula = "S";
                endif;
            }

            $dataReserva['idunidade'] = $gfm->idunidade;
            $dataReserva['idgfm'] = $gfm->id;
            $dataReserva['n_reservas'] = $gfm->capacidade - intval($total);
            $dataReserva['capacidade'] = $gfm->capacidade;
            $dataReserva['dtaula'] = $dtAula;
            $reservaNew = Reserva::create($dataReserva);
            if (sizeof($reservaNew)) {
                //LogReserva::create(['idaluno' => $idaluno, 'idgfm' => $idaula, 'dtaula' => $dtAula, 'reserva_id' => $reservaNew->id]);
                $retorno['cod'] = '0';
                $retorno['text'] = 'Aula reservada com sucesso!';
            } else {
                $retorno['cod'] = '-1';
                $retorno['text'] = 'Ocorreu um erro ao reservar aula, tente novamente!';
            }
        }
        return $retorno;
    }

    public function getAula($idaula, $idaluno, $dia) {

        $diasemana = 0;

        switch ($dia) {
            case 'Dom':
                $diasemana = 1;
                break;
            case 'Seg':
                $diasemana = 2;
                break;
            case 'Ter':
                $diasemana = 3;
                break;
            case 'Qua':
                $diasemana = 4;
                break;
            case 'Qui':
                $diasemana = 5;
                break;
            case 'Sex':
                $diasemana = 6;
                break;
            case 'Sab':
                $diasemana = 7;
                break;
        }

        $dtAula = $this->getDataAula_App($idaula, $diasemana);

        $ret = $this->corrigeSaldo($idaula, $dtAula);


        $gfm = Gfm::select('gfm.*', 'users.name as nmprofessor', 'programa.nmprograma', 'local.nmlocal')
                        ->leftJoin('users', 'users.id', '=', 'gfm.idfuncionario')
                        ->leftJoin('programa', 'programa.id', '=', 'gfm.idprograma')
                        ->leftJoin('local', 'local.id', '=', 'gfm.idlocal')
                        ->where('gfm.id', $idaula)->first();

        $selecionou = LogReserva::select('id')->where('idaluno', $idaluno)->where('idgfm', $idaula)->where('dtaula', $dtAula)->get();
        $reservaid = 0;
        if (sizeof($selecionou) > 0):
            $reservaid = $selecionou[0]->id;
        endif;
        //echo $reservaid;
        $contador = count($selecionou);
        $gfm->selecionou = ($contador > 0) ? 'S' : 'N';

        $hrHj = date('H:i');
        $dtHj = date('Y-m-d');
        $ano = substr($dtHj, 0, 4);
        $mes = substr($dtHj, 5, 2);
        $dia = substr($dtHj, 8, 2);
        $ds = date("w", mktime($ano, $dia, $mes));
        $diasemanaAtual = $ds + 1;






        if ($gfm->privado == 'Sim') {
            $gfm->matriculados = DB::table('matricula_turma')->where('idaula', $idaula)->where('stmatricula', 'A')->count();
        } else {
            $gfm->matriculados = 0;
        }

        $dias = explode(",", $gfm->diasemana);
        $dif = 0;
        $datas = [];

        $difx = 0;
        $gfm['aulaemandamento'] = "N";
        if ($diasemana > $diasemanaAtual) {
            $difx = ($diasemana - $diasemanaAtual);
        }
        if ($diasemana < $diasemanaAtual) {
            $difx = ( $diasemana - $diasemanaAtual) + 7;
        }
        if ($diasemanaAtual == $diasemana) {
            $difx = 0;
        }

        if ($difx == 0) {

            if (date('H:i:s') <= date('H:i:s', strtotime($gfm->hora_inicio))) {

                $difx = 0;
            } else {
                $difx = 7;
            }

            $hrInicio = $gfm->hora_inicio;
            $hrFim = $horaNova = strtotime("$hrInicio + $gfm->duracao  minutes");

            if ($hrHj <= $gfm->hora_inicio):
                $gfm['aulaemandamento'] = 'N';
            else:
                if ($hrHj <= date("H:i", $hrFim)):
                    $gfm['aulaemandamento'] = 'S';
                else:
                    $gfm['aulaemandamento'] = 'N';
                endif;
            endif;
        }
        //$dt = date('Y-m-d', strtotime("+" . $dif . " days"));
        $dt = $dtAula;

        $dateStart = new \DateTime(date($dt . ' ' . $gfm->hora_inicio));
        $dateNow = new \DateTime(date('Y-m-d H:i:s'));
        $dateDiff = $dateStart->diff($dateNow);

        $result = ((($dateDiff->d * 24) * 60) * 60) + (($dateDiff->h * 60) * 60) + ($dateDiff->i * 60) + $dateDiff->s;


        $reservas = Reserva::select('n_reservas')->where('idgfm', $gfm['id'])->where('dtaula', $dtAula)->get();
        $gfm['n_reservas'] = isset($reservas[0]['n_reservas']) ? $reservas[0]['n_reservas'] : $gfm['capacidade'];

        $datas[0] = $result;

        /*  foreach ($dias as $key => $value) {


          $gfm['aulaemandamento'] = "N";

          if ($dias[$key] < $diasemana) {
          $dif = ($dias[$key] - $diasemana) + 7;
          }
          if ($dias[$key] > $diasemana) {
          $dif = ($dias[$key] - $diasemana);
          }
          if ($dias[$key] == $diasemana) {

          if (date('H:i:s') <= date('H:i:s', strtotime($gfm->hora_inicio))) {

          $dif = ($dias[$key] - $diasemana);
          } else {
          $dif = ($dias[$key] - $diasemana) + 7;
          }

          $hrInicio = $gfm->hora_inicio;
          $hrFim = $horaNova = strtotime("$hrInicio + $gfm->duracao  minutes");

          if ($hrHj <= $gfm->hora_inicio):
          $gfm['aulaemandamento'] = 'N';
          else:
          if ($hrHj <= date("H:i", $hrFim)):
          $gfm['aulaemandamento'] = 'S';
          else:
          $gfm['aulaemandamento'] = 'N';
          endif;
          endif;
          }
          //$dt = date('Y-m-d', strtotime("+" . $dif . " days"));
          $dt = $dtAula;

          $dateStart = new \DateTime(date($dt . ' ' . $gfm->hora_inicio));
          $dateNow = new \DateTime(date('Y-m-d H:i:s'));
          $dateDiff = $dateStart->diff($dateNow);

          $result = ((($dateDiff->d * 24) * 60) * 60) + (($dateDiff->h * 60) * 60) + ($dateDiff->i * 60) + $dateDiff->s;


          $reservas = Reserva::select('n_reservas')->where('idgfm', $gfm['id'])->where('dtaula', $dtAula)->get();
          $gfm['n_reservas'] = isset($reservas[0]['n_reservas']) ? $reservas[0]['n_reservas'] : $gfm['capacidade'];

          $datas[$key] = $result;

          } */


        sort($datas);

        if ($datas[0] <= 3600 && $gfm['selecionou'] == 'S'):
            $gfm['liberacancelar'] = 'S';
        else:
            $gfm['liberacancelar'] = 'N';
        endif;

        $ret = Messages::select('id')->where('push_types', '=', 6)->where('receiver_id', $idaluno)->where('confirmado', '=', 'I')->where('reserva_id', $reservaid)->get();

        if (sizeOf($ret) > 0):
            $gfm['idmensagem'] = $ret[0]->id;
        else:
            $gfm['idmensagem'] = 0;
        endif;

        if ($gfm['idmensagem'] == 0):
            $gfm['liberacancelar'] = 'N';
        endif;



        $gfm['reservalog_id'] = $reservaid;
        $gfm['diferencatempo'] = $datas[0] - 1800; // ---> remover 1800 apara acertar??
        $gfm['dtaula'] = date('d/m/Y', strtotime($dt));
        $gfm['dataaula'] = $dtAula;
        $gfm['difdias'] = $difx;
        if ($gfm['aulaemandamento'] == 'S') {
            $gfm['BlockReserveDia'] = 'N';
        } else {

            if ($difx == 7) {
                $gfm['BlockReserveDia'] = 'S';
            } else {
                $gfm['BlockReserveDia'] = 'N';
            }
        }
        return $gfm;
    }

    function getDataAula_App($idaula, $diax) {


        $gfm = Gfm::select('gfm.*')->where('gfm.id', $idaula)->first();

        $hrHj = date('H:i');
        $dtHj = date('Y-m-d');

        $ano = substr($dtHj, 0, 4);
        $mes = substr($dtHj, 5, 2);
        $dia = substr($dtHj, 8, 2);
        $ds = date('w', strtotime($dtHj));
        $diasemana = $ds + 1;




//        $dias = $gfm->diasemana;
        $dias = explode(",", $gfm->diasemana);
        $dif = 0;
        $datas = [];
        $numerodias = sizeof($dias);

//        $diax=intval($dias);

        if ($diax == 0) {
            $diax = $diasemana;
        }
        $difere = 0;
        if ($diax >= $diasemana) {
            $difere = ($diax - $diasemana );

            if ($diax == $diasemana) {
                if (date('H:i:s') >= date('H:i:s', strtotime($gfm->hora_inicio))) {
                    //$difere=($diax - $diasemana ) + 7;  
                    $difere = 0;
                }
            }
        } else {
            $difere = ($diax - $diasemana ) + 7;
        }


        //echo 'diasemana  - ' . $diasemana . " - diax - " .  $diax . "| - |"  . $difere . "|";
        //echo $diasemana . " - " . $diax ;
        $proximaData = date('Y-m-d', strtotime("+" . $difere . " days"));
        //echo $proximaData;
        /* foreach ($dias as $key => $value) {
          if ($dias[$key] < $diasemana) {
          if ($numerodias==$key+1){
          $dif = ($dias[0] - $diasemana) + 7;
          }else{
          $dif = ($dias[$key] - $diasemana) + 7;
          }
          }
          if ($dias[$key] > $diasemana) {
          $dif = ($dias[$key] - $diasemana);


          }


          if ($dias[$key] == $diasemana) {
          if (date('H:i:s') <= date('H:i:s', strtotime($gfm->hora_inicio))) {
          $dif = ($dias[$key] - $diasemana);
          } else {
          if ($numerodias==$key+1){
          $dif = ($dias[0] - $diasemana) + 7;
          }else{
          $dif = ($dias[$key] - $diasemana) + 7;
          }

          }
          $hrInicio = $gfm->hora_inicio;
          $hrFim = $horaNova = strtotime("$hrInicio + $gfm->duracao  minutes");
          }

          $dt = date('Y-m-d', strtotime("+" . $dif . " days"));
          $datas[$key]['dia']=$dt;
          $dsz = date('w', strtotime($datas[$key]['dia']));

          $datas[$key]['diasem']=$dsz+1;

          //echo '|  '  . $datas[$key]['dia'] . 'x ' . $datas[$key]['diasem']  .  '|->';



          $dateStart = new \DateTime(date($dt . ' ' . $gfm->hora_inicio));
          $dateNow = new \DateTime(date('Y-m-d H:i:s'));
          $dateDiff = $dateStart->diff($dateNow);



          //  echo $dt . " |";
          }
          $cont=0;
          $dts=[];
          sort($datas);
          foreach ($datas as $key => $value) {

          $dts[$cont]['dia']= $datas[$key]['dia'] ;

          } */

        // return $datas[0]['dia'];    

        return $proximaData;
    }

    function getDataAula($idaula, $diax) {

        $gfm = Gfm::select('gfm.*')->where('gfm.id', $idaula)->first();

        $hrHj = date('H:i');
        $dtHj = date('Y-m-d');

        $ano = substr($dtHj, 0, 4);
        $mes = substr($dtHj, 5, 2);
        $dia = substr($dtHj, 8, 2);
        $ds = date('w', strtotime($dtHj));
        $diasemana = $ds + 1;

        $dias = explode(",", $gfm->diasemana);
        $dif = 0;
        $datas = [];
        $numerodias = sizeof($dias);
        if ($diax == 0) {
            $diax = $diasemana;
        }

        if ($diax >= $diasemana) {
            $difere = ($diax - $diasemana );

            if ($diax == $diasemana) {
                if (date('H:i:s') >= date('H:i:s', strtotime($gfm->hora_inicio))) {
                    //$difere=($diax - $diasemana ) + 7;  
                    $difere = 0;
                }
            }
        } else {
            $difere = ($diax - $diasemana ) + 7;
        }

        //echo $diasemana . " - " . $diax ;
        $proximaData = date('Y-m-d', strtotime("+" . $difere . " days"));


        //echo $proximaData;
        /* foreach ($dias as $key => $value) {
          if ($dias[$key] < $diasemana) {
          if ($numerodias==$key+1){
          $dif = ($dias[0] - $diasemana) + 7;
          }else{
          $dif = ($dias[$key] - $diasemana) + 7;
          }
          }
          if ($dias[$key] > $diasemana) {
          $dif = ($dias[$key] - $diasemana);


          }
          if ($dias[$key] == $diasemana) {
          if (date('H:i:s') <= date('H:i:s', strtotime($gfm->hora_inicio))) {
          $dif = ($dias[$key] - $diasemana);
          } else {
          if ($numerodias==$key+1){
          $dif = ($dias[0] - $diasemana) + 7;
          }else{
          $dif = ($dias[$key] - $diasemana) + 7;
          }

          }
          $hrInicio = $gfm->hora_inicio;
          $hrFim = $horaNova = strtotime("$hrInicio + $gfm->duracao  minutes");
          }

          $dt = date('Y-m-d', strtotime("+" . $dif . " days"));
          $datas[$key]['dia']=$dt;
          $dsz = date('w', strtotime($datas[$key]['dia']));

          $datas[$key]['diasem']=$dsz+1;

          //echo '|  '  . $datas[$key]['dia'] . 'x ' . $datas[$key]['diasem']  .  '|->';



          $dateStart = new \DateTime(date($dt . ' ' . $gfm->hora_inicio));
          $dateNow = new \DateTime(date('Y-m-d H:i:s'));
          $dateDiff = $dateStart->diff($dateNow);



          //  echo $dt . " |";
          }
          $cont=0;
          $dts=[];
          sort($datas);
          foreach ($datas as $key => $value) {

          $dts[$cont]['dia']= $datas[$key]['dia'] ;

          }

          return $datas[0]['dia']; */

        return $proximaData;
    }

    public function esperaAdd($idaula, $idaluno) {

        $idunidade = Gfm::select('idunidade')->where('id', $idaula)->first();

        $inserido = Matriculaturma::select('idaluno')->where('idaula', $idaula)->where('idaluno', $idaluno)->where('stmatricula', 'A')->first();
        if (sizeof($inserido) <= 0):
            $versao = Listaespera::create([ 'idaluno' => $idaluno, 'idgfm' => $idaula, 'idunidade' => $idunidade->idunidade]);

            if (sizeof($versao)) {
                $retorno['cod'] = '0';
                $retorno['text'] = 'Versão atualizada com sucesso!';
            } else {
                $retorno['cod'] = '-1';
                $retorno['text'] = 'Ocorreu um erro ao atualizar versão, tente novamente!';
            }
        else:
            $retorno['cod'] = '-1';
            $retorno['text'] = 'Aluno já matriculado nesse turma!';
        endif;

        return $retorno;
    }

    public function esperaDel($idaula, $idaluno) {

        if (Listaespera::where('idaluno', $idaluno)->where('idgfm', $idaula)->delete()):
            $retorno['cod'] = '0';
            $retorno['text'] = 'Reserva cancelada com sucesso!';
        else:
            $retorno['cod'] = '-1';
            $retorno['text'] = 'Ocorreu um erro ao cancelar reserva, tente novamente!';
        endif;

        return $retorno;
    }

    public function getConfiguraGfm($unidade) {


        $configura = Gfmconfigura::select('gfm_configura.*')->where('idunidade', $unidade)->first();

        return $configura;
    }

    public function reservaAdd($idaula, $idaluno, $dia) {

        $diasemana = 0;

        switch ($dia) {
            case 'Dom':
                $diasemana = 1;
                break;
            case 'Seg':
                $diasemana = 2;
                break;
            case 'Ter':
                $diasemana = 3;
                break;
            case 'Qua':
                $diasemana = 4;
                break;
            case 'Qui':
                $diasemana = 5;
                break;
            case 'Sex':
                $diasemana = 6;
                break;
            case 'Sab':
                $diasemana = 7;
                break;
        }





        $dtAula = $this->getDataAula_App($idaula, $diasemana);


        $dtHj = date('Y-m-d H:i:s');



        $gfm = Gfm::select('gfm.idunidade', 'gfm.tempo_antecedencia', 'gfm.id', 'gfm.idunidade', 'gfm.capacidade', 'gfm.diasemana', 'gfm.hora_inicio', 'gfm.duracao')->where('gfm.id', $idaula)->first();

        //$tempoAntecedencia = $gfm->tempo_antecedencia; // dias pra reservar determinada aula 
        //$unidade = User::select('idunidade')->where('id', $idaluno)->first();
        $unidade = $gfm->idunidade;


        $configura = $this->getConfiguraGfm($unidade); ///configiraçoes  gfm

        $reservado = Logreserva::select('idaluno')->where('idgfm', $idaula)->where('idaluno', $idaluno)->where('dtaula', $dtAula)->first();

        //acumula total ja reservado
        $total = DB::select($sql = "select coalesce(count(*),0) as total from gfm_reserva_log where idgfm = " . $idaula . " and dtaula = '" . $dtAula . "'");

        $termino = date('H:i:s', strtotime("$gfm->hora_inicio + $gfm->duracao minutes"));
        $dtStamp = $dtAula . ' ' . $termino;

        //$unidade = User::select('idunidade')->where('id', $idaluno)->first();
        //$numAulas = $configura->aulasantecipadas; // numero de aulas antecipadas
        $temReserva = Logreserva::select('gfm_reserva_log.dtaula', 'gfm.hora_inicio', 'gfm.duracao')
                        ->leftjoin('gfm', 'gfm.id', 'gfm_reserva_log.idgfm')
                        ->where('idaluno', $idaluno)
                        ->wherenotin('idgfm', [$idaula])
                        ->where('idaluno', '>', 0)->where('dtaula', '=', $dtAula)->get();

        $dtCompara = "";
        $aulasAbertas = 0;

        $podeReservar = "S";
        ////////////diferenca entre data atual e a data da aula
        $dateStart = new \DateTime(date($dtAula));
        $dateNow = new \DateTime(date('Y-m-d'));
        $dateDiff = $dateStart->diff($dateNow);

        if ($configura->diasantecedencia < $dateDiff->d): ///  verifica antecedencia
            $retorno['cod'] = '-3';
            $retorno['text'] = "aula so pode reservada com $configura->diasantecedencia dia(s) de antecedência";

            //echo "aula so pode reservada com $tempoAntecedencia dia(s) de antecedência";
            $podeReservar = "N";
        endif;


        if ($podeReservar == "S"):
            $dtHjx = date('Y-m-d H:i:s');
            if (sizeof($temReserva) > 0): ///verifica se as reservas encontradas ja passaram
                foreach ($temReserva as $key => $value) {

                    $termino = date('H:i:s', strtotime($temReserva[$key]->hora_inicio . "+ " . $temReserva[$key]->duracao . " minutes")); //pega o termnino da aula

                    $temReserva[$key]->termino = $termino;
                    $dtCompara = $temReserva[$key]->dtaula . ' ' . $termino; // data da aula + horario de termino

                    if ($dtHjx < $dtCompara):
                        $temReserva[$key]->passou = 'N';
                        $aulasAbertas++;
                    else:
                        $temReserva[$key]->passou = 'S';
                    endif;
                }
            endif;
            if ($aulasAbertas >= $configura->aulasantecipadas):
                $podeReservar = "N";

                if ($configura->aulasantecipadas > 1):
                    $xx = "Você já possui $configura->aulasantecipadas aulas reservadas em aberto!";
                else:
                    $xx = "Você já possui $configura->aulasantecipadas aula reservada em aberto!";
                endif;
                $retorno['cod'] = '-3';
                $retorno['text'] = $xx; //"Você já possui $configura->aulasantecipadas aula(s) reservada(s) em aberto!";
            endif;
        endif;
        if ($podeReservar == "S"):
            $varx = $gfm->hora_inicio;
            foreach ($temReserva as $key => $value) {
                if ($temReserva[$key]->passou == "N"):


                    $data1 = $varx;
                    $data2 = $temReserva[$key]->hora_inicio;

                    $unix_data1 = strtotime($data1);
                    $unix_data2 = strtotime($data2);
                    $nHoras = ($unix_data1 - $unix_data2) / 60;
                    $minutes = $nHoras;

                    if ($minutes >= 0):
                        $data1 = date('H:i:s', strtotime("$gfm->hora_inicio + $gfm->duracao minutes"));
                        $data2 = $temReserva[$key]->hora_inicio;
                        $unix_data1 = strtotime($data1);
                        $unix_data2 = strtotime($data2);
                        $nHoras = ($unix_data1 - $unix_data2) / 60;
                    else:
                        $data1 = $gfm->hora_inicio;
                        $data2 = $temReserva[$key]->termino;
                        $unix_data1 = strtotime($data1);
                        $unix_data2 = strtotime($data2);
                        $nHoras = ($unix_data1 - $unix_data2) / 60;
                    endif;
                    $minutes = (int) str_replace('-', '', $nHoras);
                    if ($minutes >= $configura->intervalo) {
                        //echo "já pode" . $configura->intervalo . "|" . $temReserva[$key]->hora_inicio . "|" . $varx;
                        $retorno['text'] = 'Intervalo permite reserva!';
                        $retorno['intervalo'] = $configura->intervalo;
                        $retorno['intervalo1'] = $temReserva[$key]->hora_inicio;
                        $retorno['intervalo2'] = $minutes;
                        $retorno['data1'] = $data1;
                        $retorno['data2'] = $data2;
                    } else {

                        $podeReservar = "N";
                        $retorno['cod'] = '-3';
                        $retorno['text'] = 'Intervalo nao permite reserva!';
                        $retorno['intervalo'] = $configura->intervalo;
                        $retorno['intervalo1'] = $temReserva[$key]->hora_inicio;
                        $retorno['intervalo2'] = $varx;
                        $retorno['minutes'] = $minutes;
                        $retorno['data1'] = $data1;
                        $retorno['data2'] = $data2;
                        break;
                    }
                endif;
            }
        endif;

        //finalizar sabado ou segunda



        if ($podeReservar == 'S'):

            if (sizeof($reservado) <= 0) {
                $reserva = Reserva::select('n_reservas', 'id', 'capacidade')->where('idgfm', $idaula)->where('dtaula', $dtAula)->first();

                if (count($reserva) > 0) {
                    if ($reserva->n_reservas > 0) {
                        //$gfm = Gfm::select('gfm.capacidade')->where('gfm.id', $idaula)->first();

                        $resto = intval($reserva->capacidade) - intval($total[0]->total) - 1; //--calcula capacidade menos total reservado
                        if (Reserva::where('idgfm', $idaula)->update(['n_reservas' => $resto])) {
                            $logres = LogReserva::create(['idaluno' => $idaluno, 'idgfm' => $idaula, 'reserva_id' => $reserva->id, 'dtaula' => $dtAula]);
                            $retorno['cod'] = '0';
                            $retorno['text'] = 'Aula reservada com sucesso!';

                            //insere movatividade
                            Movatividade::create(
                                    ['idaluno' => $idaluno,
                                        'idunidade' => $unidade,
                                        'idatividade' => 2, 'qtatividade' => 1, 'dtregistro' => $dtHj,
                                        'stmovatividade' => 'F', 'vinculo_id' => $logres->id
                            ]);


                            $retorno['cod'] = '0';
                            $retorno['text'] = 'Aula reservada com sucesso!' . $logres->id . "|" . sizeof($temReserva);
                        } else {
                            $retorno['cod'] = '-3';
                            $retorno['text'] = 'Ocorreu um erro ao reservar aula, tente novamente!';
                        }
                    } else {
                        $retorno['cod'] = '-3';
                        $retorno['text'] = 'Desculpe, mas infelizmente não há mais vagas para esta aula!';
                    }
                } else {
                    $gfm = Gfm::select('gfm.id', 'gfm.idunidade', 'gfm.capacidade', 'gfm.diasemana')->where('gfm.id', $idaula)->first();
                    $dias = explode(",", $gfm->diasemana);

                    $dtHj = date('Y-m-d');
                    $ano = substr($dtHj, 0, 4);
                    $mes = substr($dtHj, 5, 2);
                    $dia = substr($dtHj, 8, 2);
                    $ds = date("w", mktime($ano, $dia, $mes));
                    $diasemana = $ds + 1;
                    $temAula = "N";
                    foreach ($dias as $key => $value) {
                        if ($dias[$key] == $diasemana):
                            $temAula = "S";
                        endif;
                    }
                    //if ($temAula == "S") { trava p n reservar ffora do dia
                    $dataReserva['idunidade'] = $gfm->idunidade;
                    $dataReserva['idgfm'] = $gfm->id;
                    $dataReserva['n_reservas'] = $gfm->capacidade - 1;
                    $dataReserva['capacidade'] = $gfm->capacidade;
                    $dataReserva['dtaula'] = $dtAula;
                    $reservaNew = Reserva::create($dataReserva);
                    if (sizeof($reservaNew)) {

                        LogReserva::create(['idaluno' => $idaluno, 'idgfm' => $idaula, 'dtaula' => $dtAula, 'reserva_id' => $reservaNew->id]);

                        ///////////////////gera medalha
                        $dadosMedalha['idaluno'] = $idaluno;
                        $dadosMedalha['idunidade'] = $unidade;
                        $dadosMedalha['type_id'] = 15;
                        $dadosMedalha['nomeaula'] = "";

                        $this->medalhaService->setMedalha($dadosMedalha);


                        $retorno['cod'] = '0';
                        $retorno['text'] = 'Aula reservada com sucesso!';
                    } else {
                        $retorno['cod'] = '-3';
                        $retorno['text'] = 'Ocorreu um erro ao reservar aula, tente novamente!';
                    }
                }
            } else {
                $retorno['cod'] = '-3';
                $retorno['text'] = 'Aula já reservada para esse aluno!';
            }



        endif;

        return $retorno;
    }

    public function reservaAddPainel($idaula, $idaluno, $dia) {


        $diasemana = 0;
        switch ($dia) {
            case 'Dom':
                $diasemana = 1;
                break;
            case 'Seg':
                $diasemana = 2;
                break;
            case 'Ter':
                $diasemana = 3;
                break;
            case 'Qua':
                $diasemana = 4;
                break;
            case 'Qui':
                $diasemana = 5;
                break;
            case 'Sex':
                $diasemana = 6;
                break;
            case 'Sab':
                $diasemana = 7;
                break;
            case 'XXX':
                $diasemana = 0;
                break;
        }


        $dtHj = date('Y-m-d');
        $ano = substr($dtHj, 0, 4);
        $mes = substr($dtHj, 5, 2);
        $dia = substr($dtHj, 8, 2);
        $ds = date("w", mktime($ano, $dia, $mes));
        $diasemana = $ds + 1;


        $gfm = Gfm::select('gfm.id', 'gfm.idunidade', 'gfm.capacidade', 'gfm.diasemana', 'gfm.hora_inicio', 'gfm.duracao')->where('gfm.id', $idaula)->first();
        $diasx = explode(",", $gfm->diasemana);

        foreach ($diasx as $key => $value) {
            if ($diasx[$key] < $diasemana) {
                $dif = ($diasx[$key] - $diasemana) + 7;
            }
            if ($diasx[$key] > $diasemana) {
                $dif = ($diasx[$key] - $diasemana);
            }
            if ($diasx[$key] == $diasemana) {
                if (date('H:i:s') <= date('H:i:s', strtotime($gfm->hora_inicio))) {
                    $dif = ($diasx[$key] - $diasemana);
                } else {
                    $dif = ($diasx[$key] - $diasemana) + 7;
                }
            }

            $dt = date('Y-m-d', strtotime("+" . $dif . " days"));

            $datas[$key] = $dt;
        }


        sort($datas);

        $dtAula = $datas[0]; //$this->getDataAula($id, $diasemana);


        $dtStamp = $dtAula . ' ' . $gfm->hora_inicio;

        $unidade = User::select('idunidade')->where('id', $idaluno)->first();

        $temReserva = Logreserva::select('dtaula')->where('idgfm', $idaula)->where('idaluno', $idaluno)->where('idaluno', '>', 0)->where('dtaula', $dtAula)->get();
        $dtCompara = 0;

        $podeReservar = "S";

        $empresa = Unidade::select('numeroaulas')->where('id', $unidade)->first();

        $numAulas = $empresa->numeroaulas;


        echo $numAulas;
        //$hrtermino= strtotime("$gfm->hora_inicio + $gfm->duracao minutes");

        if (sizeof($temReserva) >= $numAulas):
            $dtCompara = $temReserva[0]->dtaula . ' ' . $gfm->hora_inicio;
        endif;

        $dtHjx = date('Y-m-d H:i:s');

        if ($dtHjx < $dtCompara):
            $podeReservar = "N";
        else:
            $podeReservar = "S";
        endif;

        if ($podeReservar == 'S'):
            $reservado = Logreserva::select('idaluno')->where('idgfm', $idaula)->where('idaluno', $idaluno)->where('idaluno', '>', 0)->where('dtaula', $dtAula)->first();
            $total = DB::select($sql = "select coalesce(count(*),0) as total from gfm_reserva_log where idgfm = " . $idaula . " and dtaula = '" . $dtAula . "'");

            if (sizeof($reservado) <= 0) {
                $reserva = Reserva::select('n_reservas', 'id', 'capacidade')->where('idgfm', $idaula)->where('dtaula', $dtAula)->first();

                if (count($reserva) > 0) {
                    if ($reserva->n_reservas > 0) {
                        //$gfm = Gfm::select('gfm.capacidade')->where('gfm.id', $idaula)->first();

                        $resto = intval($reserva->capacidade) - intval($total[0]->total) - 1; //--calcula capacidade menos total reservado
                        if (Reserva::where('idgfm', $idaula)->update(['n_reservas' => $resto])) {
                            $logres = LogReserva::create(['idaluno' => $idaluno, 'idgfm' => $idaula, 'reserva_id' => $reserva->id, 'dtaula' => $dtAula]);
                            $retorno['cod'] = '0';
                            $retorno['text'] = 'Aula reservada com sucesso!';

                            if ($idaluno > 0):
                                //insere movatividade
                                Movatividade::create(['idaluno' => $idaluno, 'idunidade' => $unidade->idunidade,
                                    'idatividade' => 2, 'qtatividade' => 1, 'dtregistro' => $dtHj,
                                    'stmovatividade' => 'F', 'vinculo_id' => $logres->id
                                ]);

                            endif;


                            $retorno['cod'] = '0';
                            $retorno['text'] = 'Aula reservada com sucesso!' . $logres->id;
                        } else {
                            $retorno['cod'] = '-1';
                            $retorno['text'] = 'Ocorreu um erro ao reservar aula, tente novamente!';
                        }
                    } else {
                        $retorno['cod'] = '-1';
                        $retorno['text'] = 'Desculpe, mas infelizmente não há mais vagas para esta aula!';
                    }
                } else {
                    $gfm = Gfm::select('gfm.id', 'gfm.idunidade', 'gfm.capacidade', 'gfm.diasemana')->where('gfm.id', $idaula)->first();
                    $dias = explode(",", $gfm->diasemana);

                    $dtHj = date('Y-m-d');
                    $ano = substr($dtHj, 0, 4);
                    $mes = substr($dtHj, 5, 2);
                    $dia = substr($dtHj, 8, 2);
                    $ds = date("w", mktime($ano, $dia, $mes));
                    $diasemana = $ds + 1;
                    $temAula = "N";
                    foreach ($dias as $key => $value) {
                        if ($dias[$key] == $diasemana):
                            $temAula = "S";
                        endif;
                    }
                    //if ($temAula == "S") { trava p n reservar ffora do dia
                    $dataReserva['idunidade'] = $gfm->idunidade;
                    $dataReserva['idgfm'] = $gfm->id;
                    $dataReserva['n_reservas'] = $gfm->capacidade - 1;
                    $dataReserva['capacidade'] = $gfm->capacidade;
                    $dataReserva['dtaula'] = $dtAula;
                    $reservaNew = Reserva::create($dataReserva);
                    if (sizeof($reservaNew)) {
                        LogReserva::create(['idaluno' => $idaluno, 'idgfm' => $idaula, 'dtaula' => $dtAula, 'reserva_id' => $reservaNew->id]);
                        $retorno['cod'] = '0';
                        $retorno['text'] = 'Aula reservada com sucesso!';
                    } else {
                        $retorno['cod'] = '-1';
                        $retorno['text'] = 'Ocorreu um erro ao reservar aula, tente novamente!';
                    }
                }
            } else {
                $retorno['cod'] = '-1';
                $retorno['text'] = 'Aula já reservada para esse aluno!';
            }
        else:

            $retorno['cod'] = '-3';
            $retorno['text'] = 'Já possui aula reservada!' . $hrtermino;


        endif;
        return $retorno;
    }

    public function matriculaAdd($idaula, $idaluno, $nivel) {


        $idunidade = User::select('idunidade')->where('id', $idaluno)->first();


        $inserido = Matriculaturma::select('idaluno')->where('idaula', $idaula)->where('idaluno', $idaluno)->where('stmatricula', 'A')->first();



        $dataMatricula['idaluno'] = $idaluno;
        $dataMatricula['idunidade'] = $idunidade->idunidade;
        $dataMatricula['idaula'] = $idaula;
        $dataMatricula['idnivel'] = $nivel;
        $dataMatricula['dtmatricula'] = date('Y-m-d H:i:s');
        $dataMatricula['stmatricula'] = "A";  // A=ATIVA / I=INATIVA
        if (sizeof($inserido) <= 0) {
            $reservaNew = Matriculaturma::create($dataMatricula);
        } else {

            $dataMatriculaUp['idaluno'] = $idaluno;
            $dataMatriculaUp['idunidade'] = $idunidade->idunidade;
            $dataMatriculaUp['idnivel'] = $nivel;

            $dataMatriculaUp['idaula'] = $idaula;
            $reservaNew = Matriculaturma::where('idaula', $idaula)->where('idaluno', $idaluno)->update($dataMatriculaUp);
        }
        if (sizeof($reservaNew)) {
            $retorno['cod'] = '0';
            $retorno['text'] = 'Matricula efetuada com sucesso!';
        } else {
            $retorno['cod'] = '-1';
            $retorno['text'] = 'Ocorreu um erro ao matricular aluno, tente novamente!';
        }

        $inserido = Listaespera::select('idaluno')->where('idgfm', $idaula)->where('idaluno', $idaluno)->first();
        if (sizeof($inserido) > 0):

            Listaespera::where('idaluno', $idaluno)->where('idgfm', $idaula)->delete();

        endif;


        return $retorno;
    }
    
    
    

    public function cancelaReservas() {

        $horaFim = strtotime(date('H:i:s') . " + 10 minutes");

        $idAula = 0;
        $idS = 0;
        $reserva = Reserva::select('gfm_reserva.idgfm', 'gfm_reserva.id', 'gfm_reserva.dtaula', 'gfm.idfuncionario', 'gfm_reserva.id', 'gfm.hora_inicio', 'gfm.idunidade', 'programa.nmprograma')
                ->leftJoin('gfm', 'gfm.id', 'gfm_reserva.idgfm')
                ->leftJoin('programa', 'programa.id', 'gfm.idprograma')
                ->where('gfm.hora_inicio', '>=', date('H:i:s'))
                ->where('gfm.hora_inicio', '<=', date("H:i:s", $horaFim))
                //->where('gfm.idunidade', '=', 56)
                ->where('gfm.privado', 'like', '%N%')
                ->where('gfm_reserva.liberouvaga', '=', 'N')
                ->where('dtaula', date('Y-m-d'))
                ->orderBy('gfm_reserva.idgfm')
                ->get();



        foreach ($reserva as $key => $value) {


            $config = $this->getConfiguraGfm($reserva[$key]->idunidade);

            if ($config->remove == "S"):
                $reservaLog = LogReserva::select('gfm_reserva_log.*')
                        ->where('idgfm', $reserva[$key]->idgfm)
                        ->where('stconfirmado', 'N')
                        ->where('dtaula', $reserva[$key]->dtaula)
                        ->get();

                $numeroSobra = 0;
                foreach ($reservaLog as $key1 => $value) {


                    $vReserva['idunidade'] = $reserva[$key]->idunidade;
                    $vReserva['idgfm'] = $reserva[$key]->idgfm;
                    $vReserva['idaluno'] = $reservaLog[$key1]->idaluno;
                    $vReserva['dtaula'] = $reserva[$key]->dtaula;
                    $vReserva['dtcancelamento'] = date('Y-m-d H:i:s');
                    $vReserva['canceladopor'] = 'sistema teste';

                    $reservaNew = LogCancelamento::create($vReserva);
                    if (sizeof($reservaNew)) {
                        
                    } else {
                        
                    }
                    LogReserva::where('id', $reservaLog[$key1]->id)->where('idaluno', $reservaLog[$key1]->idaluno)->delete();

                    Reserva::where('id', $reserva[$key]->id)->where('idunidade', $reserva[$key]->idunidade)->update(['liberouvaga' => 'S']);

                    $dados['sender_id'] = $reserva[$key]->idfuncionario; //$this->pushService->getSenderID($aulasdia[$key]->idunidade);
                    $dados['receiver_id'] = $reservaLog[$key1]->idaluno;
                    $dados['push_type'] = 9;
                    $dados['reserva_id'] = $reservaLog[$key1]->id;
                    $dados['hr_aviso'] = $reserva[$key]->hora_inicio;

                    $dados['push_dtaula'] = $reserva[$key]->dtaula;
                    $dados['push_horario'] = date('H:i', strtotime($reserva[$key]->hora_inicio));
                    $dados['push_aula'] = $reserva[$key]->nmprograma;
                    $this->pushService->enviaPushCancelaAula($dados);
                    $numeroSobra++;
                }
                $ech=$this->ofereceVagas($reserva[$key],$numeroSobra); 



            endif;
        }







        //  $reservaLog = Logreserva::select('id')->where('idaluno', $idaluno)->where('idgfm', $idaula)->where('dtaula', $dtAula)->first();        
        return response()->json(compact('reserva'));
        //return $reserva;
    }
    public function testeReservas() {
        
        $teste=[];
        
        $teste['idgfm']= 603;
        $teste['dtaula'] = '2018-07-23';
        $teste['hora_inicio'] = date('H:i', strtotime('14:00:00'));
        $teste['nmprograma'] = "TESTE TESTE";
     
        $ech=$this->ofereceVagas($teste,5); 

        return $ech;
        
    }
    public function ofereceVagas($reserva, $numeroSobra) {


        ////fazer busca e enviar primeiro para clientes premium
        
        $dtHj = date('Y-m-d');

        $dtIni = date('Y-m-d', strtotime("-30 days"));

        $sql = "SELECT count(*) as total, idaluno from gfm, gfm_reserva_log, users where"
                . " gfm_reserva_log.idgfm = " . $reserva['idgfm'] . " and "
                . " gfm_reserva_log.idgfm = gfm.id and "
                . " gfm_reserva_log.idaluno = users.id and"
                . " gfm.privado like '%N%' "
                . " and users.role not in('prospect') and users.excluido not in ('S')"
                . " and gfm_reserva_log.dtaula between '$dtIni' and '$dtHj'"
                . " group by idaluno order by total desc";


        $reserves = DB::select($sql);

        //$gfm_aula = Reserva::find($reserva->idgfm);

        $gfm_user = Gfm::find($reserva['idgfm']);
        $ttAlunos=0;
        foreach ($reserves as $key => $value) {
            //echo $reserves[$key]->idaluno;
            //    echo $reserves[$key]->idaluno . '-';
            $ttAlunos++;
            if($ttAlunos<=$numeroSobra):
                $dados['sender_id'] = $gfm_user->idfuncionario; //$this->pushService->getSenderID($aulasdia[$key]->idunidade);
                $dados['receiver_id'] = $reserves[$key]->idaluno;
                $dados['push_type'] = 11;
                $dados['reserva_id'] = 0;
                $dados['hr_aviso'] = $gfm_user->hora_inicio;

                $dados['push_dtaula'] = $reserva['dtaula'];
                $dados['push_horario'] = date('H:i', strtotime($reserva['hora_inicio']));
                $dados['push_aula'] = $reserva['nmprograma'];
                $this->pushService->enviaPushOfereceVaga($dados);

            endif;

            
        }
        return $reserves;
    }

    public function reservaDel($idaula, $idaluno, $dias = 'XXX') {


        switch ($dias) {
            case 'Dom':
                $diasemana = 1;
                break;
            case 'Seg':
                $diasemana = 2;
                break;
            case 'Ter':
                $diasemana = 3;
                break;
            case 'Qua':
                $diasemana = 4;
                break;
            case 'Qui':
                $diasemana = 5;
                break;
            case 'Sex':
                $diasemana = 6;
                break;
            case 'Sab':
                $diasemana = 7;
                /* case 'XXX':
                  $diasemana = 0; */
                break;
        }
        $dtHj = date('Y-m-d');
        $ano = substr($dtHj, 0, 4);
        $mes = substr($dtHj, 5, 2);
        $dia = substr($dtHj, 8, 2);
        $ds = date("w", mktime($ano, $dia, $mes));
        $diasem = $ds + 1;

        if ($diasemana == 0) {
            $diasemana = $diasem;
        }


        $dtAula = $this->getDataAula_App($idaula, $diasemana);

        $reserva = Reserva::select('n_reservas', 'capacidade', 'idunidade')->where('idgfm', $idaula)->where('dtaula', $dtAula)->first();
        $reservaLog = Logreserva::select('id')->where('idaluno', $idaluno)->where('idgfm', $idaula)->where('dtaula', $dtAula)->first();
        if (LogReserva::where('idaluno', $idaluno)->where('idgfm', $idaula)->where('dtaula', $dtAula)->delete()) {
            $total = DB::select("select coalesce(count(*),0) as total from gfm_reserva_log where idgfm = " . $idaula . " and dtaula = '" . $dtAula . "'");
            $sobra = intval($reserva->capacidade) - intval($total[0]->total);
            Reserva::where('idgfm', $idaula)->update(['n_reservas' => $sobra]);

            Movatividade::where('idaluno', $idaluno)->where('idatividade', 2)->where('vinculo_id', $reservaLog->id)->delete();

            Rankingconquista::where('idaluno', $idaluno)->where('idconquista', 2)->update(['qtconquista' => $sobra]);

            $vReserva['idunidade'] = $reserva->idunidade;
            $vReserva['idgfm'] = $idaula;
            $vReserva['idaluno'] = $idaluno;
            $vReserva['dtaula'] = $dtAula;
            $vReserva['dtcancelamento'] = date('Y-m-d H:i:s');
            $vReserva['canceladopor'] = 'aluno';

            $reservaNew = LogCancelamento::create($vReserva);
            if (sizeof($reservaNew)) {
                
            } else {
                
            }
            $retorno['text'] = 'Aula cancelada com sucesso!' . $reservaLog->id;
        } else {
            $retorno['text'] = 'Ocorreu um erro ao cancelar aula, tente novamente!';
        }
        return $retorno;
    }

    public function reservaDelPainel($idaula, $id, $dias = 'XXX') {


        $reservaLog = Logreserva::select('id', 'dtaula', 'idaluno')->where('id', $id)->where('idgfm', $idaula)->first();


        $reserva = Reserva::select('n_reservas', 'capacidade')->where('idgfm', $idaula)
                ->where('dtaula', $reservaLog->dtaula)
                ->first();
        if (LogReserva::where('id', $id)->where('idgfm', $idaula)->where('dtaula', $reservaLog->dtaula)->delete()) {
            $total = DB::select("select coalesce(count(*),0) as total from gfm_reserva_log where idgfm = " . $idaula . " and dtaula = '" . $reservaLog->dtaula . "'");
            $sobra = intval($reserva->capacidade) - intval($total[0]->total);
            Reserva::where('idgfm', $idaula)->update(['n_reservas' => $sobra]);

            Movatividade::where('idaluno', $reservaLog->idaluno)->where('idatividade', 2)->where('vinculo_id', $reservaLog->id)->delete();

            Rankingconquista::where('idaluno', $reservaLog->idaluno)->where('idconquista', 2)->update(['qtconquista' => $sobra]);


            $retorno['text'] = 'Aula cancelada com sucesso!' . $reservaLog->id;
        } else {
            $retorno['text'] = 'Ocorreu um erro ao cancelar aula, tente novamente!';
        }
        return $retorno;
    }

    public function matriculaDel($idaula, $idaluno) {

        Matriculaturma::where('idaluno', $idaluno)->where('idaula', $idaula)->update(['stmatricula' => 'I', 'dtbaixa' => date('Y-m-d H:i:s')]);
        //if (Matriculaturma::where('idaluno', $idaluno)->where('idaula', $idaula)->delete()) {
        $retorno['text'] = 'Matricula cancelada com sucesso!';
        // } else {
        //   $retorno['text'] = 'Ocorreu um erro ao cancelar matricula, tente novamente!';
        // }
        return $retorno;
    }

    // FECHA JSON GFM
    // JSON SPM
    public function getAulasDiaSpm_Old($idunidade) {
        $diasemana = date('w', strtotime(date('Y-m-d'))) + 1;
        $spms = Spm::select('spm.id', 'spm.hora_inicio', 'spm.capacidade_max', 'users.name as nmprofessor', 'programa.nmprograma', 'local.nmlocal', 'spm_reserva.n_reservas')
                        ->leftJoin('users', 'users.id', '=', 'spm.idfuncionario')
                        ->leftJoin('programa', 'programa.id', '=', 'spm.idprograma')
                        ->leftJoin('local', 'local.id', '=', 'spm.idlocal')
                        ->leftJoin('spm_reserva', 'spm_reserva.idspm', '=', 'spm.id')
                        ->where('spm.idunidade', $idunidade)
                        ->where('spm.diasemana', $diasemana)->get();




        return $spms;
    }

    public function getAulasDiaSpm($idunidade) {
        $diasemana = date('w', strtotime(date('Y-m-d'))) + 1;
        $spms = Gfm::select('gfm.id', 'gfm.hora_inicio', 'gfm.capacidade as capacidademax', 'users.name as nmprofessor', 'programa.nmprograma', 'local.nmlocal', 'gfm_reserva.n_reservas')
                        ->leftJoin('users', 'users.id', '=', 'gfm.idfuncionario')
                        ->leftJoin('programa', 'programa.id', '=', 'gfm.idprograma')
                        ->leftJoin('local', 'local.id', '=', 'gfm.idlocal')
                        ->leftJoin('gfm_reserva', 'gfm_reserva.idgfm', '=', 'gfm.id')
                        ->where('gfm.idunidade', $idunidade)
                        ->where('gfm.diasemana', $diasemana)->get();

        return $spms;
    }

    public function getAulasDiaSemanaSpm($idunidade, $dia) {
        switch ($dia) {
            case 'Dom':
                $diasemana = 1;
                break;
            case 'Seg':
                $diasemana = 2;
                break;
            case 'Ter':
                $diasemana = 3;
                break;
            case 'Qua':
                $diasemana = 4;
                break;
            case 'Qui':
                $diasemana = 5;
                break;
            case 'Sex':
                $diasemana = 6;
                break;
            case 'Sab':
                $diasemana = 7;
                break;
        }
        $idaluno = 76;


        //'gfm_reserva.n_reservas', 'gfm_reserva.id as reserva_id')
        $spms = Gfm::select('gfm.id', 'gfm.hora_inicio', 'gfm.capacidade as capacidademax', 'users.name as nmprofessor', 'programa.nmprograma', 'local.nmlocal')
                ->leftJoin('users', 'users.id', '=', 'gfm.idfuncionario')
                ->leftJoin('programa', 'programa.id', '=', 'gfm.idprograma')
                ->leftJoin('local', 'local.id', '=', 'gfm.idlocal')
                ->where('gfm.idunidade', $idunidade)
                ->where('gfm.diasemana', 'LIKE', '%' . $diasemana . '%')
                ->wherein('gfm.gfmspm', ['Spm', 'Ambos'])
                ->where('gfm.privado', 'Não')
                ->orderBy('gfm.hora_inicio', 'ASC')
                ->get();

        foreach ($spms as $key => $value) {
            $dtAula = $this->getDataAula($spms[$key]['id']);
            $spms[$key]['dtaula'] = $dtAula;

            $reservas = Reserva::select('n_reservas')->where('idgfm', $spms[$key]['id'])->where('dtaula', $dtAula)->get();
            $spms[$key]['n_reservas'] = isset($reservas[0]['n_reservas']) ? $reservas[0]['n_reservas'] : $spms[$key]['capacidade'];
            $reservou = LogReserva::select('idaluno')->where('idaluno', $idaluno)->where('idgfm', $spms[$key]['id'])->where('dtaula', $dtAula)->get();
            if (sizeof($reservou) > 0) {
                $spms[$key]['selecionou'] = 'S';
            } else {
                $spms[$key]['selecionou'] = 'N';
            }
        }

        /* $spms = Spm::select('spm.id', 'spm.hora_inicio', 'spm.capacidade_max', 'users.name as nmprofessor', 'programa.nmprograma', 'local.nmlocal', 'spm_reserva.n_reservas')
          ->leftJoin('users', 'users.id', '=', 'spm.idfuncionario')
          ->leftJoin('programa', 'programa.id', '=', 'spm.idprograma')
          ->leftJoin('local', 'local.id', '=', 'spm.idlocal')
          ->leftJoin('spm_reserva', 'spm_reserva.idspm', '=', 'spm.id')
          ->where('spm.idunidade', $idunidade)
          ->where('spm.diasemana', 'LIKE', '%' . $diasemana . '%')
          ->where('spm.privado', 'LIKE', '%Não%')
          ->get(); */
        return $spms;
    }

    public function getAulaSpm($idaula, $idaluno) {
        $spm = Spm::select('spm.*', 'users.name as nmprofessor', 'programa.nmprograma', 'local.nmlocal', 'spm_reserva.n_reservas')
                ->leftJoin('users', 'users.id', '=', 'spm.idfuncionario')
                ->leftJoin('programa', 'programa.id', '=', 'spm.idprograma')
                ->leftJoin('local', 'local.id', '=', 'spm.idlocal')
                ->leftJoin('spm_reserva', 'spm_reserva.idspm', '=', 'spm.id')
                ->where('spm.id', $idaula)
                ->first();
        $selecionou = LogReservaSpm::where('idaluno', $idaluno)->where('idspm', $idaula)->get();
        $contador = count($selecionou);
        $spm->selecionou = ($contador > 0) ? 'S' : 'N';

        $diferenca = 0;
        $dtHj = date('Y-m-d');
        $ano = substr($dtHj, 0, 4);
        $mes = substr($dtHj, 5, 2);
        $dia = substr($dtHj, 8, 2);
        $ds = date("w", mktime($ano, $dia, $mes));
        $diasemana = $ds + 1;

        $dias = explode(",", $spm->diasemana);
        $dif = 0;
        $datas = "";
        foreach ($dias as $key => $value) {
            if ($dias[$key] < $diasemana) {
                $dif = ($dias[$key] - $diasemana) + 7;
            }
            if ($dias[$key] > $diasemana) {
                $dif = ($dias[$key] - $diasemana);
            }
            if ($dias[$key] == $diasemana) {
                if (date('H:i:s') <= date('H:i:s', strtotime($spm->hora_inicio))) {
                    $dif = ($dias[$key] - $diasemana);
                } else {
                    $dif = ($dias[$key] - $diasemana) + 7;
                }
            }

            $dt = date('Y-m-d', strtotime("+" . $dif . " days"));

            $dateStart = new \DateTime(date($dt . ' ' . $spm->hora_inicio));
            $dateNow = new \DateTime(date('Y-m-d H:i:s'));
            $dateDiff = $dateStart->diff($dateNow);


            $result = ((($dateDiff->d * 24) * 60) * 60) + (($dateDiff->h * 60) * 60) + ($dateDiff->i * 60) + $dateDiff->s;

            $datas[$key] = $result;
        }

        sort($datas);
        $spm['diferencatempo'] = $datas[0] - 1800; // ---> remover 1800 apara acertar??
        return $spm;
    }

    public function reservaAddSpm($idaula, $idaluno) {
        $dtAula = $this->getDataAula($idaula);

        $reservado = Logreserva::select('idaluno')->where('idgfm', $idaula)->where('idaluno', $idaluno)->where('dtaula', $dtAula)->first();
//        $reservado = Logreserva::select('idaluno')->where('idgfm', $idaula)->where('idaluno', $idaluno)->whereRaw('Date(created_at) = CURDATE()')->first();
        //acumula total ja reservado

        $total = DB::select($sql = "select coalesce(count(*),0) as total from gfm_reserva_log where idgfm = " . $idaula . " and dtaula = '" . $dtAula . "'");

        if (sizeof($reservado) <= 0) {
            $reserva = Reserva::select('n_reservas', 'id', 'capacidade')->where('idgfm', $idaula)->where('dtaula', $dtAula)->first();

            if (count($reserva) > 0) {
                if ($reserva->n_reservas > 0) {
                    //$gfm = Gfm::select('gfm.capacidade')->where('gfm.id', $idaula)->first();

                    $resto = intval($reserva->capacidade) - intval($total[0]->total) - 1; //--calcula capacidade menos total reservado
                    if (Reserva::where('idgfm', $idaula)->update(['n_reservas' => $resto])) {
                        LogReserva::create(['idaluno' => $idaluno, 'idgfm' => $idaula, 'reserva_id' => $reserva->id, 'dtaula' => $dtAula]);
                        $retorno['cod'] = '0';
                        $retorno['text'] = 'Aula reservada com sucesso!';
                    } else {
                        $retorno['cod'] = '-1';
                        $retorno['text'] = 'Ocorreu um erro ao reservar aula, tente novamente!';
                    }
                } else {
                    $retorno['cod'] = '-1';
                    $retorno['text'] = 'Desculpe, mas infelizmente não há mais vagas para esta aula!';
                }
            } else {
                $gfm = Gfm::select('gfm.id', 'gfm.idunidade', 'gfm.capacidade', 'gfm.diasemana')->where('gfm.id', $idaula)->first();
                $dias = explode(",", $gfm->diasemana);

                $dtHj = date('Y-m-d');
                $ano = substr($dtHj, 0, 4);
                $mes = substr($dtHj, 5, 2);
                $dia = substr($dtHj, 8, 2);
                $ds = date("w", mktime($ano, $dia, $mes));
                $diasemana = $ds + 1;
                $temAula = "N";
                foreach ($dias as $key => $value) {
                    if ($dias[$key] == $diasemana):
                        $temAula = "S";
                    endif;
                }
                //if ($temAula == "S") { trava p n reservar ffora do dia
                $dataReserva['idunidade'] = $gfm->idunidade;
                $dataReserva['idgfm'] = $gfm->id;
                $dataReserva['n_reservas'] = $gfm->capacidade - 1;
                $dataReserva['capacidade'] = $gfm->capacidade;
                $dataReserva['dtaula'] = $dtAula;
                $reservaNew = Reserva::create($dataReserva);
                if (sizeof($reservaNew)) {
                    LogReserva::create(['idaluno' => $idaluno, 'idgfm' => $idaula, 'dtaula' => $dtAula, 'reserva_id' => $reservaNew->id]);
                    $retorno['cod'] = '0';
                    $retorno['text'] = 'Aula reservada com sucesso!';
                } else {
                    $retorno['cod'] = '-1';
                    $retorno['text'] = 'Ocorreu um erro ao reservar aula, tente novamente!';
                }
            }
        } else {
            $retorno['cod'] = '-1';
            $retorno['text'] = 'Aula já reservada para esse aluno!';
        }

        return $retorno;

        /* $reserva = ReservaSpm::select('n_reservas')->where('idspm', $idaula)->whereRaw('Date(created_at) = CURDATE()')->first();
          if (count($reserva) > 0) {
          if ($reserva->n_reservas > 0) {
          if (ReservaSpm::where('idspm', $idaula)->update(['n_reservas' => $reserva->n_reservas - 1])) {
          LogReservaSpm::create(['idaluno' => $idaluno, 'idspm' => $idaula]);
          $retorno['text'] = 'Aula reservada com sucesso!';
          } else {
          $retorno['text'] = 'Ocorreu um erro ao reservar aula, tente novamente!';
          }
          } else {
          $retorno['text'] = 'Desculpe, mas infelizmente não há mais vagas para esta aula!';
          }
          } else {
          $spm = Spm::select('spm.id', 'spm.idunidade', 'spm.capacidade_max')->where('spm.id', $idaula)->first();
          if (ReservaSpm::create(['idunidade' => $spm->idunidade, 'idspm' => $spm->id, 'n_reservas' => $spm->capacidade_max - 1])) {
          LogReservaSpm::create(['idaluno' => $idaluno, 'idspm' => $idaula]);
          $retorno['text'] = 'Aula reservada com sucesso!';
          } else {
          $retorno['text'] = 'Ocorreu um erro ao reservar aula, tente novamente!';
          }
          }
          return $retorno; */
    }

    public function reservaDelSpm($idaula, $idaluno) {
        $reserva = ReservaSpm::select('n_reservas')->where('idspm', $idaula)->first();
        if (ReservaSpm::where('idspm', $idaula)->update(['n_reservas' => $reserva->n_reservas + 1])) {
            LogReservaSpm::where('idaluno', $idaluno)->where('idspm', $idaula)->delete();
            $retorno['text'] = 'Aula cancelada com sucesso!';
        } else {
            $retorno['text'] = 'Ocorreu um erro ao cancelar aula, tente novamente!';
        }
        return $retorno;
    }

    // FECHA JSON SPM
    ##################
    #### PERFIL DO USUARIO FOTO
    ##################
    ////////////////////////

    public function gerarProcessosAut() {


        $ret = $this->processoService->geraProcessoAgendamentoRb(); //ok
        $ret = $this->processoService->geraProcessoTreinoVencidoRb(); //ok
        //$ret= $this->processoService->geraProcessoAusenciaRb();
        $ret = $this->processoService->geraProcessoEtapaRb(); //ok
        $ret = $this->processoService->geraProcessoProjetoRb(); //ok
        $ret = $this->processoService->geraProcessoAniversarioRb(); // ok


        return $ret;
    }

    public function enviaClienteTotem($idaluno, $idunidade) {


        //$ret= $this->processoService->geraProcessoAgendamento($idaluno,$idunidade); //OK GERANDO
        //$ret1= $this->processoService->geraProcessoTreinoVencido($idaluno,$idunidade);  // okgerando */
        //$ret3= $this->processoService->geraProcessoAusencia($idaluno,$idunidade);//ok gerando
        //$ret4= $this->processoService->geraProcessoEtapa($idaluno,$idunidade);//ok gerando
        //$ret5= $this->processoService->geraProcessoProjeto($idaluno,$idunidade);//ok gerando
        //$ret3= $this->processoService->geraProcessoAniversario($idaluno,$idunidade);//ok gerando// erro
        //return $ret3;

        $result = Totem::select('totem.idaluno')
                ->where('idunidade', $idunidade)
                ->where('idaluno', $idaluno)
                ->where('dtentrada', date('Y-m-d'))
                ->where('situacao', 'A')
                ->get();


        if (sizeof($result) > 0):
            $retorno['title'] = 'Erro!';
            $retorno['type'] = 'error';
            $retorno['text'] = 'Aluno já está no Totem!';

            return $retorno;
        else:
            $dataTotem['idunidade'] = $idunidade;
            $dataTotem['idaluno'] = $idaluno;
            $dataTotem['tpapp'] = 'T'; //T = TOTEM ---A=APP
            $dataTotem['situacao'] = 'A';
            $dataTotem['dtentrada'] = date('Y-m-d');
            $dataTotem['hrentrada'] = date('H:i:s');

            $totem = new Totem();
            if ($totem->create($dataTotem)):
                $retorno['title'] = 'Sucesso!';
                $retorno['type'] = 'success';
                $retorno['text'] = 'Enviado com sucesso!';
                return $retorno;
            else:
                $retorno['title'] = 'Erro!';
                $retorno['type'] = 'error';
                $retorno['text'] = 'Erro ao enviar para totem!';
                return $retorno;
            endif;
        endif;
    }

    public function listarProcessos($idunidade) {

        $processos = Processoaluno::select('id', 'idaluno', 'idunidade', 'dtstart', 'dtagenda')
                ->where('idunidade', $idunidade)
                ->where('stprocesso', 'P')
                ->get();


        foreach ($processos as $key => $value) {
            $dateStart = new \DateTime(date($processos[$key]->dtagenda));
            $dateNow = new \DateTime(date('Y-m-d H:i:s'));
            $dateDiff = $dateStart->diff($dateNow);

            $processos[$key]->diasatraso = $dateDiff->days;
        }

        return $processos;
    }

    public function getClientesTotem($idunidade) {

        $totem = Totem::select('totem.*', 'users.*')
                        ->leftJoin('users', 'users.id', '=', 'totem.idaluno')
                        ->where('totem.idunidade', $idunidade)
                        ->where('totem.situacao', 'A')
                        ->where('totem.dtentrada', date('Y-m-d'))
                        ->orderBy('totem.dtentrada')
                        ->orderBy('totem.hrentrada')->get();

        $processos = 0;
        $diasatraso = 0;
        $soma_score = 0;
        foreach ($totem as $key => $value) {

            $p_name = explode(' ', $value->name);
            $totem[$key]['name'] = $p_name[0];
            $idaluno = $totem[$key]['idaluno'];

            $path = "avatars/user-$idaluno.jpg";

            if (Storage::disk('uploads')->exists($path)) {
                $totem[$key]['avatar'] = "/uploads/$path";
            } else {
                $totem[$key]['avatar'] = 'http://sistemapcb.com.br/tema_assets/img/a4.jpg';
            }

            //
            $idaluno = $totem[$key]['idaluno'];
            $idunidade = $totem[$key]['idunidade'];


            $totem[$key]['processo'] = $this->getProcessosAluno($totem[$key]['idaluno'], $totem[$key]['idunidade']);
            if (sizeof($totem[$key]) > 0):
                if ($totem[$key]['processo'] == 'S'):
                    $score = $this->temProcessosAluno($idaluno, $idunidade);
                    $soma_score = $soma_score + $score['diasatraso'];
                    $processos++;
                endif;
            endif;
        }

        // se existir processos somo o score
        /* $soma_score = 0; 
          if($processos>0) {
          foreach ($score as $key => $value) {



          //       $soma_score = $soma_score + $value['result'][$key]['diasatraso'];

          }
          } */


        return response()->json(compact('processos', 'soma_score', 'score', 'totem'));
    }

    public function getProcessosAluno($idaluno, $idunidade) {

        $result = Processoaluno::select('id', 'idaluno', 'idunidade', 'dtstart')
                ->where('idunidade', $idunidade)
                ->where('idaluno', $idaluno)
                ->where('stprocesso', 'P')
                ->get();

        if (sizeof($result) > 0):
            return 'S';
        else:
            return 'N';

        endif;
    }

    public function temProcessosAluno($idaluno, $idunidade) {

        /* $sql = "select idaluno from historicotreino where"
          . " idaluno = " . $idaluno
          . " and idunidade = " . $idunidade
          . " and dtfim <= " . date('Y-m-d H:i:s');


          $alunosAbertos = DB::select($sql); */

        $result = "";

        $result = Processoaluno::select('processo.id as processo', 'processo.nmprocesso', 'processo_aluno.id', 'processo_aluno.idaluno', 'processo_aluno.idunidade', 'processo_aluno.dtstart', 'processo_aluno.dtagenda', 'processo_aluno.idprocesso', 'processo_aluno.idregistro')
                ->leftjoin('processo', 'processo.id', 'processo_aluno.idprocesso')
                ->where('processo_aluno.idunidade', $idunidade)
                ->where('processo_aluno.idaluno', $idaluno)
                ->where('stprocesso', 'P')
                ->get();

        $total = 0;

        $diasatraso = 0;
        foreach ($result as $key => $value) {

            if ($result[$key]->processo == 1):
                $result1 = Agendaalunoprograma::select('agenda_alunoprograma.nmagenda')
                        ->where('idunidade', $idunidade)
                        ->where('idaluno', $result[$key]->idregistro)
                        ->where('id', 'P')
                        ->get();
                if (sizeof($result1) > 0):
                    $result[$key]->nmagenda = $result1[0]->nmagenda;
                else:
                    $result[$key]->nmagenda = '';

                endif;


            else :
                $result[$key]->nmagenda = '';
            endif;
            if ($result[$key]->nmagenda == ''):
                $result[$key]->nmagenda = $result[$key]->nmprocesso;
            endif;

            $dateStart = new \DateTime(date($result[$key]->dtagenda));
            $dateNow = new \DateTime(date('Y-m-d H:i:s'));
            $dateDiff = $dateStart->diff($dateNow);

            $result[$key]->diasatraso = $dateDiff->days;
            $diasatraso = $diasatraso + $result[$key]->diasatraso;
        }
        if (sizeof($result) > 0):
            $total = sizeof($result);
        endif;
        ///aqui verificar se o aluno ta fazendo aula opu nao pra contabilizar p score
        return ['total' => $total, 'result' => $result, 'diasatraso' => $diasatraso];
    }

    public function getClienteTotem($idaluno, $idunidade) {
        $totem = Totem::select('totem.*')
                        ->where('totem.idaluno', $idaluno)
                        ->where('totem.idunidade', $idunidade)
                        ->where('totem.dtentrada', date('Y-m-d'))->get();

        return $totem;
    }

    public function deleteClienteTotem($idaluno, $idunidade) {
        if (Totem::where('idaluno', $idaluno)
                        ->where('idunidade', $idunidade)
                        ->where('situacao', 'A')
                        ->where('dtentrada', date('Y-m-d'))
                        ->delete()) {
            $msg = ['title' => 'Sucesso', 'mensagem' => 'Aluno removido com sucesso!'];

            return ['status' => 'success', 'dados' => $msg];
        } else {
            $msg = ['title' => 'Erro!', 'mensagem' => 'Não existem dados para remover!'];

            return ['status' => 'error', 'dados' => $msg];
        }

        //return $retorno;
    }

    //////////////////////////

    public function base64_to_jpeg($base64_string, $user_id) {
        $base64_string = "data:image/jpeg;base64," . $base64_string;
        $output_file = '/uploads/avatars/user-' . $user_id . '.jpg';

        $destination_folder = $_SERVER['DOCUMENT_ROOT'] . '/public' . $output_file;

        //		$destination_folder = $output_file;

        $ifp = fopen($destination_folder, 'wb');

        //Livrar-se de tudo até a última vírgula
        $base64_string = substr($base64_string, 1 + strrpos($base64_string, ','));

        fwrite($ifp, base64_decode($base64_string));
        fclose($ifp);


        // return $output_file;

        $fotinha = "user-$user_id.jpg";
        User::where('id', $user_id)->update(['avatar' => $fotinha]);
        return $fotinha;
    }

    public function base64_to_jpeg_pesagem($base64_string, $user_id) {

//// PEGA ID DA FOTO

        $foto = Fotouser::where('user_id', $user_id)
                ->orderBy('id', 'DESC')
                ->first();


        $id_foto = $foto->id;



        ////////////////////


        $time = $user_id . time();
        $base64_string = "data:image/jpeg;base64," . $base64_string;
        $output_file = '/uploads/pesagem/user-' . $time . '.jpg';

        $atualizar = Fotouser::where('id', $id_foto)->update(['user_foto' => $output_file]);

        $destination_folder = $_SERVER['DOCUMENT_ROOT'] . '/public' . $output_file;
        $ifp = fopen($destination_folder, 'wb');

        //Livrar-se de tudo até a última vírgula
        $base64_string = substr($base64_string, 1 + strrpos($base64_string, ','));

        fwrite($ifp, base64_decode($base64_string));
        fclose($ifp);


        if ($atualizar) {
            return 'sucesso ao salvar foto';
        }
        return 'erro ao salvar a foto';
    }

    public function setData($Dt) {
        //echo (string) $Dt;
        /* $Format =substr($Data, 0, 10);
          $Data = explode('-', $Format[0]);
          $Data = $Data[0] . '-' . $Data[1] . '-' . $Data[2]; */
        return $Dt;
    }

    public function salvarfoto_pesagem(Request $request) {
        $user_id = $request->user_id;
        $base64_string = $request->user_foto;

        return $this->base64_to_jpeg_pesagem($base64_string, $user_id);
    }

    public function salvar_foto(Request $request) {
        $user_id = $request->idaluno;
        $base64_string = $request->user_foto;

        return $this->base64_to_jpeg($base64_string, $user_id);

//        $base64_string = "/9j/4AAQSkZJRgABAQEAYABgAAD//gA7Q1JFQVRPUjogZ2QtanBlZyB2MS4wICh1c2luZyBJSkcgSlBFRyB2ODApLCBxdWFsaXR5ID0gOTAK/9sAQwADAgIDAgIDAwMDBAMDBAUIBQUEBAUKBwcGCAwKDAwLCgsLDQ4SEA0OEQ4LCxAWEBETFBUVFQwPFxgWFBgSFBUU/9sAQwEDBAQFBAUJBQUJFA0LDRQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQU/8AAEQgAyADIAwEiAAIRAQMRAf/EAB8AAAEFAQEBAQEBAAAAAAAAAAABAgMEBQYHCAkKC//EALUQAAIBAwMCBAMFBQQEAAABfQECAwAEEQUSITFBBhNRYQcicRQygZGhCCNCscEVUtHwJDNicoIJChYXGBkaJSYnKCkqNDU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6g4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2drh4uPk5ebn6Onq8fLz9PX29/j5+v/EAB8BAAMBAQEBAQEBAQEAAAAAAAABAgMEBQYHCAkKC//EALURAAIBAgQEAwQHBQQEAAECdwABAgMRBAUhMQYSQVEHYXETIjKBCBRCkaGxwQkjM1LwFWJy0QoWJDThJfEXGBkaJicoKSo1Njc4OTpDREVGR0hJSlNUVVZXWFlaY2RlZmdoaWpzdHV2d3h5eoKDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uLj5OXm5+jp6vLz9PX29/j5+v/aAAwDAQACEQMRAD8A98UkdqC56YxTLmdbORQ0chz3jQkVMgE8YdQdp6bhg0ARFvTNO3lifWkuBIip5UYky4DAnGB3NPCEc9qAIy53Hnim7zyOoqUjHakKjPFAERY4pAWI61JjA6Um0c8EH2oAhY88nmvOfiV8a9L+Hd2tpcxvPdOm/apACj1Nd9q19a6Pp1xfXkwhtoEMju3YCvz5+NHjeTxp4/u78M72iNtgiA5KjpkUAe8v+0jfXMbzxxKIF5VSmCTnpnPIrg9W/aAu73xULi5iEUEa8KudvvjPvXmGhzXd5a3BMdxHFjBZEyQBzxnpUd9JYabIoRJPPKhjJICxHsfr7UAe3T/HfU/Eqw2sCRxRbw3y5yAPWtXwR+0PLockVjqVk7wNMVLqCzKvPPv2/OvnmbWk06CIRzOZGJc7uBUh8UrNC7yKPPQgqVGM460Afe3h74gaP4ngWWwvopCW2+UTtcfUHmuiSQSjg5A71+eumeNL6xuvNtn8sswzchiCv5V7X8Nf2gLvTL+207VpRe6fO+0XQwWj7c+ooA+oARyM/lSEAmoLe6S5jSSJw8bAMGHcVLvFACbcGngALxUUkgA61WuriQwMIWVZSMKW6A0Acl4p+I/9iaq1nb26XOxfnYsRg+lbHhTXrzxBai4ntUto2+7gkkiuJf4a3M1551xfQyb33ScnJGea9EspbWwt44IyAqDAxQBpYz7AUyXZEjMxCqBkk9qjTUoTwCD7ZrnvEOqNfj7LBkQj75H8R9PpQBl6prR1O6PlkrAhwmO59aKlsNEMmDg4ooA9j8rg0ww881YLZpCcdaAKrRZ/hx700w56VaPNNOPSgCmbcjmo2hOOK0NqkdeaPKz2oAynhkyML171h3t7rEVwUhs4QgON8rtz+AU/zrs7Zlt7hJCiyKrA7D0I9DV7xv4q069tluF0230tbdCZGhJw+B3/ACoA+O/2lviBfutp4ZZIopHzPM0MjEFewOQMd+K+d7i8h0e1N55KI8oIWVyGJ9gK7L40+O4fFOt3Fy6LA1xNiMDk4U7Rn16dq8h1MeZ9phMhkgiJ2Oc8fSgDRsfHThJ4EjEqSA7mk659vSsy78RN8plZpZF465rFsLGa5m2wo7ZPGB1rVl8IX0TiXyHzjJGOaAM+51BW+d8szHKqT0qu2q3czhGbaR0VuMfSi6t3s38ueE8n+Lmm28kbOXb1yOOntQBp+Hdflt5JIrglkI2jPatnTdfFvctnII4wo4B9q4+QRl05LSAkjHQCtO0i3yiUynHVlP8ASgD7Q+AvxgsLrS7bR9UvRBcZ2W7SHhh6E9jX034R8EXnjC/Fraywxvt37pXAGK/LrTb7ayGCbbIhzgccivvH9ljxze/E3wu6STeXfafshdgDlvQn8qAO/wDF/hW+8N6pPYzvHJLFjJibcPzrybxX4p1DSL02tvZfaJB1Z5dirn8Ca+irnwPfSAu1xGeMkvkV8+fEDTJV8W6gigTKjqoeI5Q/KOhoA4q58R+J7s/8fVpZD0jjaQ/mSP5VR2avcn/SNdv3J7ROIl/ID+tdINHuJMDywv1NWIvDr5HmSquTgYGTQBB8LvDfiHxTqj2sWuvIioX2Xi7s46DcOR+Rrv7nwtrXhwZvdOkmjHWW0/ej8hz+lXvhbo9v8O9Vupr26E8UkexXWM5HPcV62dX0zWIFa2uopVYZ64P5GgDzrRLD7TbLOkb+WVyMqRRXodpLGLKVUU4CsxfbweT0NFAHPi+RmznmpPt64+8K8N/4WdqtuxDWUUo9mxVm3+LcuQJtNcZ4+Q5FAHtX2sY+8Ka12yjgZrzOz+JtlcECWOaL6rXR6f4p0y9xtvAh/wBqgDdOttG5U2krAfxDGP51v+HvG32K0u7cafDIsww32uAMV4/hPasvTbH7eoMF3E4P+1W7B4L1OQAqiuPZhQBlrIJBkdPSuD+NZlHw/wBSMd2tkdhLSkdsdPxr1/TvDetaZdx3MWnCdkOQJI96/iO9eN/tg6Zqi/CvU9TuLP7ERNEGMcRSNMnB47UAfn3qbWt7fQl9zmIbYlB+br3/ADr1fwb8G7K90JLq+bdLP82wDhRXj2lP9s1O2iKjzN2GLDBAzxX1f4cJj0y3jVeNoAFAHO2Hw10XTHV47dAy9MDtUmr+Eba6ZjGqjjGMcV3LaQ8i5UY74qT+xflIA+agD568UfCuK/ichPmGQpHY+tcVN8JLqFNqKrDbyR/OvqO+0IDOFJ56E4qKPw0jL9wUAfJ8vwo1gCSS3tPOVRztPP4Vytzpl7pVxIs9s8eOCrrjFfdmk6EtvK4VAciua+LfhDTx4Q1PUJbdGmSFsNjkcUAfItjOklwgDpHIQAcda+5f+Cc2iXutfE7U/KUtpjWO6d1GAjA8Z9zk18L6TZosouGcPEN2R39jX6p/8EwjY2Xw/wDFgaSBbs38RJLDeU8vj8M5/WgD6D+MfhWDS/hjr9xC5WVIQQRwfvCvkDStBuvEV+IIdodj1c4Ar7P+PmrW0fwq1tBPGzyLGgUMMkmRa+QNHvDY3G9DtoA30+DtxFZvPNfW0YVC+0ZY/TpXDT2RhvEjJ3ASKP1rubvxRNJbFN5IIxXIs/m6hb/7Uy/zoA3760e5YpGuWPQVct9DurCCEzQPGu0EEjjn3pk0nlSlvQ11kmrPdaXHbsx27R1oAr2VzLHZzR7iU8o8enNFJHJFb20zSsEQqME+maKAPktfFto/J85fwBqzB4m04lWMzLjrujrhQML0oP3aAPVbDxFpLMB9thB/2siuy0TUdKmdQL21Ynt5o/rXz1j5uKTpuoA+1PDFnZXRQxvbsf8AZdTXrOg6GyKGRXye6Of6Gvzx8BaNL4i8baVp32iWKCV90nluVLKBkgYPeuk1NPGXhq+kOl61f+SXZo9t40bIM8ZycGgD9J9H03UYwhiedF+uf55rwb/gond6hZfs13wkZ3SW/t0b5AAOSecY9K+bNP8AjN8YvDq2/wBn8R6tMGGdgmEoXnoc5rmv2h/jD8RfiJ8J9T0/xBrcd5ptheQtNGY1Ds/OMFRzjPNAHzn4C0xtX8TIN2+RdpY19bW7aZoVhBLeXKRPtAVSa+ef2e/D0lwdW1txlYX8tVI+8QP/AK9bviiwt7jfe+INSkjRidkQkwAPQCgD2mPxzpU5IiuUOO4YVKPE8bE+WysPUV8banf+GFv/ALPZ3WppIzYwpYfzr0v4ax3EE0Ultqdzc25+UwXHOKAPd7jWVcZODVKTxjY6cha4mSJR3ZgKw9bhu7WwacAqME57Cvn3xY+n6hebr+5nncEkJG+AOe9AH1LpXxZ8Mtcqr3cOOhO8cV1Wv6TpvjPwldx206T21xGyEqc4yOK+JtBufBk0wixKsx4Pz9K9m8BfadJVJ9F1SSSxLgPbuxI/+tQB883elT+GfEl7pFyuPLkZQT9a+1f+Cfy6zcXXia208TFFghc+VyBhnHP+e1fNnx38OXOleNkupY8RXgEiSYxkgcivuj/glR4ant9B8Za28qCK4kgtVjz8+VDMSR6fPQB6H8Uzqln4TmTUEncSTRqruxAUg56d+leNxOynINfWX7Vsmz4f2cYxmS+QfkrGvk6WJ7aRo5AAw4OGB/UUAPaZyOelLYAPqdpu6eZmmJKi9U4qxayRPdxbEw6knP4UAbF5MpY5bvipLPVnAWNlDKMAlT1rGvZmAzn+I1n6XfSG4GPu5zQB6TqpMuizKhAklRMDPOCaKWyiWfSklkYfaDMqBCPm2AE/lRQB4J/wrzw9cjMV3GQfSQVE/wAJdPl/1Vzx7MK+dbbw94rtrj5J50UHosh5roNQm8V2OmxtHeXEcw6jeaAPYJvg1nmG6yfwqpJ8Gr0A+XOGz615HpXirx2J4Ql/OyE/MSc10kPxB8d2s06LdO+zlSy8EUAd1oHw+13wrr9nqVqUkktnD4J6juK9a8ZHT/EOnwX1qrWd/GoVrQR5DjPII6fjXzpZfGLxyI97QwyHdjDJ1rZ/4Xv4ps1BuNIs5B3OSKAOplkvLSaZW064mXduUiJT36dc1zvjLQ4/EHhy8tPsc1k17KxmVk25YjII/KsSX9sFdPlMd54dSQqcHypcVu2fx903x79m09tAutNmuCRBcPIrJuwePWgDnfgHZmP4eXCBcObyQN+GBVPxz4Ai1S/W7ug8u3/VqGwEPrXdfDHT49O0e7hRdiveSOU9M4rub3RbK+tz5sfQUAfLl58O7ea/F1km4znzC2T6V2XgLwhJYXcUaqxiDbixr0S98Padau3lEY9SOa1NHsIlWNYU2gnJY96ALHiHTGvPD0sHl4yhAOK+b7/4dSWmpG6SMptJ+UjK5r7DtdLGrQpbwsBI6uBkdT1rzzxHZQWF4sMq7Zj99GHBNAHzlpfwptoLqSUBlaZSrAqDweuK9V+Hvgb/AIRiNhazSTQPglZx8wx/Ou20nw1bXrqyqkbGu90fwUsVszhlYR9qAPCP2n7JJ/Bmh3TAJPFeKoYddpU8VY+BvjnxT8K9DvBpGpT6TNNFHKhi4JVzkZz7Ctb9ojRhq/hO1VX8tbS8jlYeq9MfrU9h4Vv/ABD4csZ7SNfuBWLHBIUfKP1NAHoOnfGvxh8SI3sPEmrvqNnbMsqK0agq3TOQMnjNW7uSBpT9mDiMHjeOSPpXGeDvD194furkXkYj81Rtwc5wa6st6cUAKxYDrVjRJC+ofRCaqM+TzVvw6p/tGT2iP86ALGoH92B0wWNU9EjDTqPVhVzUx+7PHY1HoEeZl+tAHeWHiPTbvVZ9FgmZ9QsmEs0flkKqlRtw3Q9aKTQo4Wv52jELMy7nZFG/PTDEc9AOtFAHjuqal4U0HUYhaTw30PUnIrkviTbaZ4ph87T5PsQC8BT1rldQ+F19JZ293aFhGRl9zdKs+I7qz8P6HaxO4aTGGINAHG+FJ7rwtrflahc+ZZMfvGvc/D974X1OAyS3scYA5BIzXz1reoW2tQiKBxv9awvs13pwBEzspPY0AfWk0PguRQ8GoqWxggY4riPF1ro62d0LOZLkeWfnOPlNL8Ovg3/wkXw4vtZa4aOZF3AE8145DDcWLXUFzLI0TsRkGgCpBomlS2LRXFk0tyWOJs8dete4+C/h34f1XSbG8guLERWboV3zNGyzAcnpzXk+laPHe3Vvb27umW3F3Hy4r3nwZo2oQ+FtX1ayhsbkWR3Mlyh2tgHjAPtQBZcRafqTwrIHCoDuHTPtSXWuhIWXcMDua5w3WtSNHdX9vFb206CRPLXgE9snnArH8Tao9tbHZkljigCa41l9SvzHG2VB5IrX/wCE/htHW1+zmMqANxGRx715tpmoTtO6x53FucDNJfaXqQXz42YnO7Hr7UAelr8U5vDlwt5DMqbDnJGQPbFN8VfEOw8f2cFzHb+VqUTEySpkKw+hrx9LjVNUfyTAVZPvBl4zWsseoQQ/c2OAQAo4oA7zQPE3lOArAkdQT0r0LQvHMkayxFt6up4zyK+ZodRvLW+eRiQUI3DPX1r0jQtSbfncwyOQaAO+uNOg8Z3X9n3ESzxuDI0bHGSOn612fha0j0DRILSWSMSgZZFZePbFeKap4hurK8VrOUwuFwGXtmsxdX1CS8iu3uWkuY1MayMASFJyR+OBQB7z4ilR7qHbjHln055qnNNFdSR7IktcKFbaxIY/3jnpmuR8JX13qNg8l3IZHD7VJGOK6AqQepFAF3yEBG5yB2Ixg1o6JCqXc5Vtw8vGfxrAIP8AeJxW/wCGI9qXEh6EYoAZqzfu2z6U7w6vmSrx361HrDDYeO+KveGYSzDA57UAdR4S02CzvLueOMRyXBDSMByxyaK3tN8N3Ogm3FygRp0WVR7UUAeO/FS1tdB8MPLbzLJM5+W3iPQV8ta/dy6ys8dynk7eQCelVovjrNqO/wA+TzB2DGuYu/Gp1m8kYYGcggUAc7DqbadrW0OWjVuRnqK+4vgB4E8FfEbQYmvrYpOBgvjIr4JYrca0sanBZ8H86+9vhJqth4E+FouIZk+2Bd23PJoA7b4hf2H8MtDl0/SLkSRP8rRA9PwrmfhD4M8OeLjK9/ZQzRgFm3DpXjmoeMm13xJc396TJC5OEJ4Fd98PfF0mnyyxWjRxwyqQwXrg0AYPxh1Hw7pE95b6BHHCkJ2MIeoNch8MNd1DQdM1Hz9SaSG/Xb5E8mAOvP61T/aF0y18KW4u7HeJLp90jN3NfPdx8QbqBlHm+YAcj2oA/QK1m0/xn4bGmoYzJFCPKaM9CBkfrXj+r2qvBLHIfnjOPyNeS/Dr48X+mzfuSy7htOBnFelXOujWVa/i3mOVtsnmKF+fvxQBzFx4QvNXsp5rS8msLkZMckD7T/8AXpmkaleWKx2mswyzsnH2uJj8/I5I7Hrmuk0y9KymIckMcAcVc1FVmjDiIhwMlTxzQBh3N7ZBZGt5blyCCqMSMiuS1HRfEPi67SKzu59KsuA7I53P9Pauq82TzjG1m6rj74brXR6cFtYCywnc35mgDkI/C66JJb2XnGcMuDJKcsT7muutYhEAzEIR36YArF1lg2oI33nzwM9Ks32ma3qvhnULvS7OW4t7RS1y0QyY07tjqQO+OlAE2m6iutte3I+55xRcjjAAFe/aN+z1bX/w9tvE/wBvdVa0Ny8HfhckA15V8Fxoj/DyNtQtRLdNcykyNA7cZwOQMV7LoHxguF8JeJfDTaP9lsdM04i3v5JCFm8wMNoU+nH50AcXoUFtFpltLa+aIJ4/NCzEbl5I7fSr6yb2+Y4FUNAZX8PaXhgXS2VHVTkBucitEokRHU57UAOkePjZn6mt/wAPSZs5eeM1zjMVBGK2dHmCWbnPegB2rHdgd81reHCIImlJ4QE1z99fLuG7BGa3tEaPULCSONgN67c0AejWWujWrSK4adWEcIQAt93A6UV5X/Zt9pMFxBbzApCCxc9DRQB+XEa3VrdIh3LuPeus0+ylaVZI246E1H48smTbLbL9zgkVQsNbmayht4VO8HLGgDsP+EPn024TUpBuQ/NmumtfiPcWcCxl2aPoFzxSaJro1mCDTLhgI5FC7vQ16v4e+CenNbwCRPPVxncKAPMY/FP2yzu3T5ZNpIFc/wDD74palFrUcZZgPNCk57Zr2bxv8PdM8J6bcuIxGdhxn09a8k8G+A7jxPq8dtoNnJdzNIDiNc80Aev/AB0t38T+DrfyjvlCBuue1fGN9pl1DdPGUfcGxjFfqn8L/wBjjxNraWc2tKba12ANG3WvV3/4J7eCiftEsW+bqeB1oA/HnQNOv4bMvDFL524BSoPWvq/wfot4PBMcOpwPHM6A/vVxnjqK+2dP/Zl8N+Dmlhm0eOW0bgu0fT3z2ryL49eHdE8MatYWOjXzXsPkF5MkERnPCgigD5g+1vod8plBKq/3xzj61vt4giuEfdgHPY81B4i0sTKSBz3ya4uaxuLRPkl2KDggjNAHZrfIp3BgQT1q1F4mVPkG0gZwM/nXnl1PePFhbiElP9k1V0bTrnVLoG4uGaMfwLwCaAOxtzJr+oiK3OyIvzIfTvivfPh3r+keC7SeXUH+zaeIWic7C27IxjA65rybw/pq2YVAPb6U34zapceH/hlfX8S5iiVt2ASQxG1T+tAFz4U/FTwrp3ghLKXX7G2uFurhjA8m0gGVsdvTFYusz2WvatdXdvrIuvPmAihtZc5BGOlfKPg+e3hcNcT4V4jIwYEgHdxX0D8Dra2uviL4ZEZRybuNhge9AHvvgZEsNLsopkk2xnEiycMTnk1s39wpu5THwucLj0q14tAHiXUSCOZiAB9BWUY5FblCM0APMx2gn0qF9bFpA6bsEn1olDKmMZOOnpXE6xdMbgoxIUNQAzxN8QJNCUtPlo/U+lSfDn402NvdNHLMz2srdc/cNeX/ABT1fEDWsdtJO204YDIFeSeHdUvLGc25hkTc24kg0Afofpfi3Ttc8OajPazq7EEbc8gUV82+FtV1K3tLRoF22sq7ZCBzz3ooA8M1y/hFzLDKo29CPQ1l+HTYNqLw8AHoTXXeLPDNvc6rdtbMHRwSCPWuBTw3eabqMYkRoxI3BxQB7Vp/w3SHw6NWt5C8KuN5H8NfQXghza6DaRNNuUKCHHNef/CaK6f4c6noUts1y92mYJ1GQp9D6V6j+ynoMuqajf2WvwCSO3QiEP3x3oA0NW/Z18U/GPR7280qSOWEx7Ejc4Oat/slfDq9+BnifULXxNpflXiEbHkXt6ivpnwD8QtL0XVpNM/48reCTygTwjvjoK5T41eMrfXL6GS1ljkmhdlJhGSR6ZFAH0DB4+tYbWKY4ELrkEdDXL+Lv2jfDvh2AqrNe3mceREMnPua+S4/Gmr6bo97El7PDp5fcUd9xJ7AHsPpWFa38gg+1SkSOEMrBupJ6AUAen/En4+63r92kQlFhZyHDW8J6DP8Rrx34j6h9r1nchyoQYxWN4ovp7jTXCqfOUeYA3U5POa5fT/FseuP5EzBL2JMFCeoHQigAv4RcHca5680wrn5dxPH1ropZgTgdKrsUOVzz6UAcXfaZE4OVwSewxirWlWQtsGNBgdWNbd3axsQcAfWltguQMce3SgDX0wAFWJ68DFek+HLCz1SyubO9hS4s502PHIAQwry2C6CnAGT2Fb1544Xw9oBSNl+3TApGpbG0d2NAHGePP2c9A8S2l1deD1XTNSMTRSWx4hkAbrgZ2k+3HtWP8HfDd94Y+Kvhy01W3e0CTgEvwjAA9G6V23gbxHJHeLLI5xI205PUV6XPNH59rqULQmaNwyF0DDdjuPX3HNAHI/FvW5NO07U7qzl8tlvU8qRDkgeaBwafrWt6lbxxhrmUhkByx56fSoPiDoV54u8NTiwty13JMsjW+cjKvuO1unvip/FpZkAcFXUAEEc9KAOan169Tn7XIx9zWLc69NeamsMrEjGeaW5ny5FZdijXXiiOFRlig4/GgDqbizt7mFRJGrEjqRU+heANN1C6Rnt0LH2pdUt20+68hxtZcZFdV4MTNxGfU0Addq/hPT9A8EBlt0UjGCBzRW94/YReEYUYcHGQaKAPzv8N+OSmrLJIglAIAR+MDvmrvi/4qaffXQhtrdRInG7bkGvvlf2QfgNanjQtRl7/PqD1ma1+zt8F/DUsVzpXhJDdREOJbu5eXHp8pOD+NAHjH7LKeJtU3Xeq2LaboEaZjZsq1wx74PpX0b4b0+x8P3ct/Y3Mkcrk5LHgA9q4+914RxSeUwSGIbFjRdoHp+FUtE8QSXFmwDbirZoA9DutTiknO8l1DF3Yngt6/WsG+1ZryVgmEjA4C9j61hzarkYYsoHGc96jsNSW4udwk2w24Mkp9h1zQBV8eah+907RIWCZIklZT61D4hnFna2Fiow0rBmz3UdK5fR9Ufxf40vNQbmFW2pg8YH/wCqrfiS7e613e7cQrgcdPagB3iW7ZYSyYAC4zmvBPEmptBrYuIHMcgYDKH7tewXjSzW0yFmYZByeSa8c8RWx/tGbILEtt4xkUAd74S8Qxa3blTIBcpw0fr7ita6ViflU/WvD0ae0mDWszQTA/JhsEGuq0X4l6hYZXUQt2gOBn5X/wAKAO9e2mkYBu1W4LQRjHc1ytx8U9LEJaKKRptuQhIGfxrlNS8e6zrSskLCxQjBWLrj3agDu/EHie08PI8QZZro9I152/WuL/tKbWblnnck4+dx/KsQEfMXy5xzk9/eti0UQ2EkuNrONpzxx7UAdv4Snja384cqOBivRfB2s/bIbmymwdp3Rn1FeU6HdfZoAAny8cA4rqvCt0kd3LPhlHQgHrQB6BZX32HWdjS7I5Tho1PHPGcVrajZwTWxiuws8KdYz99e3ysOf51yF5NyjxHav3ie+K3LuYSpBdiUuWj2uoHUjrQBzuu+AoY5d9pcMsUi70Egzj2J/wDrV5p4+ttV+GN4utXmn3E2n7VH2m2G5VPv6V7fLfRzafCoYEBiFyOaSw1eHUI7i0vglxZSv5TQSjcjDGMYNAHkml+JB4mtIr8b1MgBw55r1X4cFZ7mEHrnGKyvEvwht9MsXvvDEYhhGXayJ4H+5/hVH4WeLbaHVoo7iWONg+3aW5zQB658ZLoWfhiM5wF/worA/aIuZE8EtNGDsEZYsOnSigD0m41Nbe3eaQjaOme5rz/VNYNzd7JXHlyZI56kUnirxTmyu44GJNsxjKA9TjOa4G+1xX+yXSEMGUMM9+x+lAF3XNTjSJXibILYcj17Vk+GtTFtczqSV+bAbqCaztcd7hLwRFQUAcDPJ75FZehagrSzuzFmkCuM9M9D/KgD0XV9YjtLeMs4Dvzjbxmuc8Z66+g+ERAn/H9qzbQOjFM4xVGWRte8TRWocrHGBu3dBXI+L/EEuteNoZIVDQWjCOHd0AA9PWgD0zwNp9r4W8P+bMCCi8hRyzGq11L9oul67jztb+tcTf8Ai7ULu7t7WGYBIvmcIuM1nWviC/8At0oW8lMhJHIBAoA7vUZDBE0mFPB565rynXmd5Jn2gnPOK27vUr6ZHE11I+RwsajP1PpXCeIbye3kJimm+ZgAFwc/WgB72wkXc2F2+3OarNsydqk7eazV1W9B2tOShxncAcCnPqFx3lhUjsU60AXvs/mTKRHFk4ywXnFTPEHQhQGcHBIGOM9Kyk1OeYFikJI7LkAVZh1NmzmJGCYyobk0AX7W3V5Sjr8pPGOR75rYuD5YjiRgfXHp61gW+refeIn2ZyS2AVYZxWjc6tbNOp2SRjody96AOlsNsMS9RjjJ/pXUaYRDprzI2NrAYxXJaXqUD2zfvhgDO1+MVt6Jqqz6fcRmaMlSD8rZzQB19nK9xp5zguD164rTg1D7RpGJHAZGI246CuKs9UeCUHdwTg46fjWtY3pQ3yNJuBUOm6gDftr2NLeOMkMBuK5+lVI7nFpGSOC7Pn/CsCXVtkLOMZVHO4fSmyaqIbGyTzQJJHWNRn25oA9W0XWwscETOVYDIU+lY/iLwBpz6/FrNpbphyPOCLwJPX8a5rSdc23U8judgPbngV6N4bv1urEQvgiX5sdx6GgDzr42+L7/AEvwReWKS7raSEqUbnH0ork/2jrv7PpV1bjmRNqlT35ooA6fxhrkmkajJdFXe1ulEdzH3HPDD3riP7alRJLASiQZM1nJn5XHdfx/nXa+J7dXLW1wJIyRyT8yn3GO1eXavpsuiu6O2+ydtytGclD2ZT/SgDqrnWo54rO5R8efAyOM/wAS1z1jrfk2ww+1y20AHOOf1rBv7uS1V5EcOu4TIy/dZh1wP9ofkawV1ZZb9o43GxZiyZHOCMigD1bRPEJhGr3wb5lUoO5JPHFZFlAYIbKWRfNklZ3b1+n0rA0y/J0PamWe4nwDjnAHP6mupJNneWQkG9YYTlj6n2oAmsYkhu5m53sp3JnJx6VZ0iCa7kYvbiFVbI2qCfzptjK8lnczhQjgEjjnrVvw1IZYNskuCQSx7CgDK8Ry3FtGI4jld+McDPrXAavaT5Z1iYAk4xmu/wDELpFK/wAp4wRXOu5juAoO5FVnIx1zQB52rGQkqrJzjOOlLOHSAM6nB7+1dde6erwjavzKC2R05qr9kWa3VmXIEZXAHXmgDjxeleNxUDjg4FXotQEdsQqA54345qZ7W2Er+ZCDtUEg0txp8VxH/oqkA/w5waALGjSu7s4Zht/izxirk12zMAqkjPQnp9KdpGkCC1DZZWHJHWrkWlM8ErSEKi4YFTzz2oAuaVM0ti6SLkMhzuPb6VX8O3KRm4XBjJwFx8vNPTTpI0AiO5QDjdwfx9axtP1CUS3ELLGqEfMAuDn2oA7BrmeMrLFcSAdCpYYPvzV2x8RzLcRCba6MNu4iuHOovewhYUffE2SHJ6etOOoMZkeJ9o3jKZ/OgD0HVdU8vRWkLDLqFCjvk4rNbVlOtNvkzFp9sWcnp5j9APfFcxqfiApokMbZYKyMT9GzVbSNSEgZ5DteaTz3U/xN/Av0A5oA9Og1MCG0twfnch5cH5h6LXpmkauLR4rVHzeSAFvm4iX/ABrxrw5ejS1W4YeffXDFIIz3bu59hXX6AphmJVzNIW3O4bl2759h6UAavx10BdQ0mLUYohcRsAsv1Heiurntv7Y8OXdpcsxeRN6ccjHPAooA425m1LQrf7PcsNb0qM4QxEGa3B9CeSPauc1i1t9UtjNbypPF2ZOMN/dZexoooA811rzNLLkD9yDkr18snqQPSuNux5GuwTRki3kyMg5CttPFFFAHe+F9ssGnAcYUvtJ4JzWt4j11LeSFJJGXeozt6jnrRRQB0Om6tG+gSuzq3m4RGI5P+TWppEUsentIEAOOnXjvRRQBxniTWP3+3YRuxuO7IArFg1tZL2QkhIyCoz9KKKAJba5W6cQh95Hy9hzVdoz5k6RkABcYAziiigDAuGAvAd4LbQDjirKujTDy3G4MDnoPeiigDb+0bQCABk8D8KvxyIbRQCoXOWGOpoooAJA0QYNnB7jvXNahIYrpY4n+Xksm3n25oooAr3WpvpTLImws6EMT6VXS/wDN1CO2IVRI4dXxjnFFFAFbW7kNpHlbsneoOeoGeat6CYnT7bcEm3iOEGfmkP8Aj6UUUAdlp9zKbgOy7b2UABO1un90e57/AEr0jRxFo1sj4LOP9XEeDI3r9KKKAO68LTT3M6GYOZHGW4+UewooooA//9k=";
//        $output_file  = date('Y_m_d_H_i_s');
//        $this->base64_to_jpeg($base64_string, $output_file);
    }

    public function exportaFrame($id) {
        $nameVideoFile = str_pad($id, 6, "0", STR_PAD_LEFT) . ".mp4";
        $nameImageFile = str_pad($id, 6, "0", STR_PAD_LEFT) . ".jpg";

        FFMpeg::fromDisk('videos')
                ->open($nameVideoFile)
                ->getFrameFromSeconds(1)
                ->export()
                ->toDisk('miniaturas')
                ->save($nameImageFile);

        return 'IMAGEM GERADA COM SUCESSO';
    }

    public function verificaTreinoAbDiaAnterior() {
        $dia = date('Y-m-d ' . '00:00:00');

        $c = Historicotreino::select('idaluno', 'idunidade', 'nrdiatreino', 'idtreino', 'dtinicio')
                ->where('sttreino', 'A')
                ->where('dtinicio', '<', $dia)
                ->orderBy('dtinicio', 'desc')
                ->get();


        if (Historicotreino::where('sttreino', 'A')
                        ->where('sttreino', 'A')
                        //->where('idunidade', 1) coloquei a unidade pra teste
                        ->where('dtinicio', '<', $dia)
                        ->update(['sttreino' => 'F'])) {
            //return 'Registro atualizado com sucesso!';
            //} else {
            //return'Erro ao atualizar dados!';
        }

        if (sizeof($c) > 0) {
            return response()->json(compact('c'));
        } else {
            return "Busca não encontrou clientes para finalizar";
        }
    }

    public function verificarTreinoAberto($idaluno, $idunidade) {


        $c = Historicotreino::select('idaluno', 'idunidade', 'nrdiatreino', 'idtreino', 'dtinicio')
                        ->where('sttreino', 'A')
                        ->where('idunidade', $idunidade)
                        ->where('idaluno', $idaluno)
                        ->orderBy('dtinicio', 'DESC')->first();

        $treino['IDTREINO'] = $c->idtreino;
        $treino['IDALUNO'] = $c->idaluno;
        $treino['IDUNIDADE'] = $c->idunidade;
        $treino['DTINICIO'] = $c->dtinicio;
        $treino['NRDIATREINO'] = $c->nrdiatreino;

        IF ($c->nrdiatreino == 1) {
            $ficha = "A";
        }
        IF ($c->nrdiatreino == 2) {
            $ficha = "B";
        }
        IF ($c->nrdiatreino == 3) {
            $ficha = "C";
        }
        IF ($c->nrdiatreino == 4) {
            $ficha = "D";
        }
        IF ($c->nrdiatreino == 5) {
            $ficha = "E";
        }
        IF ($c->nrdiatreino == 6) {
            $ficha = "F";
        }
        IF ($c->nrdiatreino == 7) {
            $ficha = "G";
        }
        IF ($c->nrdiatreino == 8) {
            $ficha = "H";
        }
        IF ($c->nrdiatreino == 9) {
            $ficha = "I";
        }
        IF ($c->nrdiatreino == 10) {
            $ficha = "J";
        }
        $treino['FICHATREINO'] = $ficha;
        $treino['DTATUAL'] = date('Y-m-d');
        $treino['HRATUAL'] = date('H:i:s');

        $datatime1 = new DateTime($c->dtinicio);
        $datatime2 = new DateTime(date('Y-m-d H:i:s'));
        $data1 = $datatime1->format('Y-m-d H:i:s');
        $data2 = $datatime2->format('Y-m-d H:i:s');
        $diff = $datatime1->diff($datatime2);
        $horas = $diff->h + ($diff->days * 24);
        $tphoras = explode(":", $diff->format($horas . ':%i:%s'));
        $treino['TEMPO'] = sprintf("%02d", $tphoras[0]) . ":" . sprintf("%02d", $tphoras[1]) . ":" . sprintf("%02d", $tphoras[2]);

        return $treino;
    }

    public function primeiroNome($nome) {

        $ret = explode(' ', $nome);
        $ret[0] = strtolower($ret[0]);
        return ucfirst($ret[0]);
    }

    public function getEquipe($unidade) {

        $usuario = User::select('users.avatar', 'users.id', 'name', 'avatar', 'users.idunidade')->whereNotIn('role', ['cliente', 'prospect'])
                        ->Join('user_dados', 'users.id', '=', 'user_dados.user_id')
                        ->where('users.idunidade', $unidade)
                        ->where('user_dados.idunidade', $unidade)
                        ->where('user_dados.situacao', 'A')
                        ->where('users.excluido', '=', 'N')->get();

        foreach ($usuario as $key => $value) {
            $usuario[$key]['avatar'] = $this->verificarArquivo($usuario[$key]['avatar']);
            $usuario[$key]['name'] = $this->primeiroNome($usuario[$key]['name']);
        }
        return $usuario;
    }

    public function getEmpresa($unidade) {

        $usuario = Unidade::select('unidade.id', 'fantasia', 'razao_social', 'endereco', 'bairro', 'cep', 'telefone', 'celular', 'email', 'numero', 'site')
                        ->leftJoin('unidade_dados', 'unidade.id', '=', 'unidade_dados.idunidade')
                        ->where('unidade.id', $unidade)->get();

        foreach ($usuario as $key => $value) {
            $usuario[$key]['cidade'] = 'Toledo';
            $usuario[$key]['estado'] = 'PR';
        }

        return $usuario;
    }

    public function cancelarTreino(Request $request) {

        $aluno = $request->aluno;
        $unidade = $request->unidade;
        $prescricao = $request->prescricao;
        $fichatreino = $request->fichatreino;

        $dia = 0;
        if ($fichatreino == "A") {
            $dia = 1;
        }
        if ($fichatreino == "B") {
            $dia = 2;
        }
        if ($fichatreino == "C") {
            $dia = 3;
        }
        if ($fichatreino == "D") {
            $dia = 4;
        }
        if ($fichatreino == "E") {
            $dia = 5;
        }
        if ($fichatreino == "F") {
            $dia = 6;
        }
        if ($fichatreino == "G") {
            $dia = 7;
        }

        if (Historicotreino::where('STTREINO', 'A')->where('IDTREINO', $prescricao)->where('NRDIATREINO', $dia)->where('IDUNIDADE', $unidade)
                        ->where('IDALUNO', $aluno)->delete()) {
            $retorno['cod'] = '0';
            $retorno['text'] = 'Treino cancelado com sucesso!';
        } else {
            $retorno['cod'] = '-1';
            $retorno['text'] = 'Ocorreu um erro ao cancelar treino, tente novamente!';
        }


        return $retorno;
    }

    public function iniciarTreino($al, $un, $pre, $fic) {

        $aluno = $al;
        $unidade = $un;
        $prescricao = $pre;
        $fichatreino = $fic;
        /*      $aluno = $request->idaluno;
          $unidade = $request->idunidade;
          $prescricao = $request->idtreino;
          $fichatreino = $request->fichatreino; */
        $nrdiatreino = 0;
        if ($fichatreino == "A") {
            $nrdiatreino = 1;
        }
        if ($fichatreino == "B") {
            $nrdiatreino = 2;
        }
        if ($fichatreino == "C") {
            $nrdiatreino = 3;
        }
        if ($fichatreino == "D") {
            $nrdiatreino = 4;
        }
        if ($fichatreino == "E") {
            $nrdiatreino = 5;
        }
        if ($fichatreino == "F") {
            $nrdiatreino = 6;
        }
        if ($fichatreino == "G") {
            $nrdiatreino = 7;
        }



        $i = 0;

        $exercicio = 0;
        $qtserie = 0;
        $temEvolucao = 0;
        $tpEdicao = "";
        $idInserido = 0;
        $cargaAnt = 0;

        $inicio = date('Y-m-d H:i:s');
        $dia_hoje = date('Y-m-d H:i:s');

        /////inicia e grava o historicotreino
        $historicotreino = Historicotreino::create([
                    'IDALUNO' => $aluno,
                    'IDUNIDADE' => $unidade,
                    'IDTREINO' => $prescricao,
                    'NRDIATREINO' => $nrdiatreino,
                    'TPATIVIDADE' => 'M',
                    'DTINICIO' => $inicio,
                    'NRDIASUGESTAO' => $nrdiatreino,
                    'STTREINO' => "A",
                    'DTTREINO' => $inicio
        ]);
        ///pega lista de exercicios(ficha)       
        $sql = "select p.treino_id,l.exercicio_id, l.ficha_letra, l.ficha_id, l.ficha_series, l.ficha_repeticoes from treino_ficha l, "
                . " treino_musculacao p where p.unidade_id = " . $unidade
                . " and p.aluno_id = " . $aluno
                . " and l.treino_id = " . $prescricao
                . " and l.ficha_letra = '" . $fichatreino . "'"
                . " and p.treino_id = l.treino_id";


        $c = DB::select($sql);
        $sql = "";
        $nrlancExer = 0;
        $idInserido = 0;
        $qtSerie = 0;
        foreach ($c as $key => $value) {
            $nrlancExer = $c[$key]->ficha_id;
            $qtSerie = 0;



            $evo = Evolucaotreino::create([
                        'IDALUNO' => $aluno,
                        'IDUNIDADE' => $unidade,
                        'IDPRESCRICAO' => $prescricao,
                        'NRDIATREINO' => $nrdiatreino,
                        'NRLANCAMENTO' => $c[$key]->ficha_id,
                        'QTREPETICAO' => $c[$key]->ficha_repeticoes,
                        'QTSERIE' => $c[$key]->ficha_series,
                        'QTCARGA' => '0',
                        'DTREGISTRO' => $dia_hoje,
                        'IDEXERCICIO' => $c[$key]->exercicio_id
            ]);

            $idInserido = $evo->id;

            ///BUSCA DADOS DA ULTIMA SERIE FEITA PRA CARREGAR COM CARGA
            $cargaAnt = 0;
            $sql2 = "select qtcarga, nrserie,qtrepeticao from serietreino where idaluno = " . $aluno;
            $sql2 .=" and idunidade = " . $unidade;
            $sql2 .=" and idprescricao = " . $prescricao;
            $sql2 .=" and nrdiatreino = " . $nrdiatreino;
            $sql2 .=" and nrlancamento = " . $c[$key]->ficha_id;
            $sql2 .=" and nrlancamentodia = (";
            $sql2 .=" select max(nrlancamentodia) from serietreino ";
            $sql2 .=" where idaluno = " . $aluno;
            $sql2 .=" and idunidade = " . $unidade;
            $sql2 .=" and idprescricao = " . $prescricao;
            $sql2 .=" and nrdiatreino = " . $nrdiatreino;
            $sql2 .=" and nrlancamento = " . $c[$key]->ficha_id;
            $sql2 .=" and nrlancamentodia < " . $idInserido . " )";

            $c2 = DB::select($sql2);
            $contSeries = 0;

            foreach ($c2 as $key1 => $value) {
                $qtSerie = 1;
                $cargaAnt = $c2[$key1]->qtcarga;

                $serie = Serietreino::create([
                            'IDALUNO' => $aluno,
                            'IDUNIDADE' => $unidade,
                            'IDPRESCRICAO' => $prescricao,
                            'NRDIATREINO' => $nrdiatreino,
                            'NRLANCAMENTO' => $c[$key]->ficha_id,
                            'NRLANCAMENTODIA' => $idInserido,
                            'NRSERIE' => $c2[$key1]->nrserie,
                            'QTREPETICAO' => $c2[$key1]->qtrepeticao,
                            'QTCARGA' => $cargaAnt,
                            'DTREGISTRO' => $dia_hoje
                ]);
                /* $sql1 = "insert serietreino (idaluno,idunidade,idprescricao,nrdiatreino,nrlancamento,nrlancamentodia,nrserie,qtrepeticao,qtcarga,dtregistro)";
                  $sql1 .= " values (" . $aluno. "," . $unidade . "," . $prescricao . "," . $nrdiatreino . "," . $c->ficha_id . "," . $idInserido .  ",'" . $c2->nrserie . "','" . $c2->qtrepeticao . "','" . $cargaAnt . "','" . $dia_hoje . "')";
                  $stmt1 = $pdo1->prepare($sql1);
                  $stmt1->execute(array(
                  )); */
                $contSeries++;
            }
            //caso nao exista pega o numero de series da ficha e grava as serie nova com dados em branco
            if ($contSeries < $c[$key]->ficha_series) {
//          if ($qtSerie<1){
                $y = $contSeries + 1;

                while ($y <= $c[$key]->ficha_series) {
                    $serie = Serietreino::create([
                                'IDALUNO' => $aluno,
                                'IDUNIDADE' => $unidade,
                                'IDPRESCRICAO' => $prescricao,
                                'NRDIATREINO' => $nrdiatreino,
                                'NRLANCAMENTO' => $c[$key]->ficha_id,
                                'NRLANCAMENTODIA' => $idInserido,
                                'NRSERIE' => $y,
                                'QTREPETICAO' => $c[$key]->ficha_repeticoes,
                                'QTCARGA' => '0',
                                'DTREGISTRO' => $dia_hoje
                    ]);

                    /* $sql1 = "insert serietreino (idaluno,idunidade,idprescricao,nrdiatreino,nrlancamento,nrlancamentodia,nrserie,qtrepeticao,qtcarga,dtregistro)";
                      $sql1 .= " values (" . $aluno. "," . $unidade . "," . $prescricao . "," . $nrdiatreino . "," . $c->ficha_id . "," . $idInserido .  ",'" . $y . "','" . $c->ficha_repeticoes . "','" . 0 . "','" . $dia_hoje . "')";
                      $stmt1 = $pdo1->prepare($sql1);
                      $stmt1->execute(array(
                      )); */
                    $y++;
                }
            }
        }



        $ret = $this->geraProcessoAgendamento($aluno, $unidade);

        $retorno['MENSAGEM'] = 'Registro efetuado com sucesso';
        return $retorno;
    }

    public function listarHistoricoAtividade(Request $request) {

        $atividade = $request->idatividade;
        $aluno = $request->idaluno;
        $unidade = $request->idunidade;

        $sql = "select idatividade, idaluno, idunidade, dtregistro, qtatividade from movatividade where ";
        $sql .= " idatividade = " . $atividade;
        $sql .= " and idaluno = " . $aluno;
        $sql .= " and idunidade = " . $unidade;
        $sql .= " and stmovatividade = 'F' ";
        $sql .= " order by dtregistro desc";


        $consulta = DB::select($sql);

        foreach ($consulta as $key => $value) {
            $consulta[$key]->IDATIVIDADE = $consulta[$key]->idatividade;
            $consulta[$key]->IDALUNO = $consulta[$key]->idaluno;
            $consulta[$key]->IDUNIDADE = $consulta[$key]->idunidade;
            $consulta[$key]->DTREGISTRO = date("d/m/Y H:i", strtotime($consulta[$key]->dtregistro));


            if ($consulta[$key]->idatividade == 1) {
                $numer = $consulta[$key]->qtatividade;
                $consulta[$key]->QTATIVIDADE = str_replace(".", ",", number_format($numer, 1));
            } else {
                if ($consulta[$key]->idatividade == 3) {
                    $consulta[$key]->QTATIVIDADE = number_format($consulta[$key]->qtatividade, 0, ',', '.');
                } else {
                    if ($consulta[$key]->idatividade == 4) {
                        $consulta[$key]->QTATIVIDADE = number_format($consulta[$key]->qtatividade, 2, ':', ',');
                    } else {
                        if ($consulta[$key]->idatividade == 5) {
                            $consulta[$key]->QTATIVIDADE = number_format($consulta[$key]->qtatividade, 2, ',', '.');
                        } else {
                            if ($consulta[$key]->idatividade == 2) {
                                $consulta[$key]->QTATIVIDADE = number_format($consulta[$key]->qtatividade, 0, ',', '');
                            } else {
                                $consulta[$key]->QTATIVIDADE = number_format($consulta[$key]->qtatividade, 0, '', ',');
                            }
                        }
                    }
                }
            }


            $consulta[$key]->DIASEMANA = $this->diasemanaAbr(date("Y-m-d", strtotime($consulta[$key]->dtregistro)));
        }
        return $consulta;
    }

    function diasemanaAbr($data) {

        $ano = substr("$data", 0, 4);
        $mes = substr("$data", 5, -3);
        $dia = substr("$data", 8, 9);

        $diasemana = date("w", mktime(0, 0, 0, $mes, $dia, $ano));

        switch ($diasemana) {
            case"0": $diasemana = "DOM";
                break;
            case"1": $diasemana = "SEG";
                break;
            case"2": $diasemana = "TER";
                break;
            case"3": $diasemana = "QUA";
                break;
            case"4": $diasemana = "QUI";
                break;
            case"5": $diasemana = "SEX";
                break;
            case"6": $diasemana = "SAB";
                break;
        }

        return $diasemana;
    }

}
