<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Usuariosistema;
use Hash;
use App\Http\Requests\GrupomuscularRequest;

class UsuariosistemaController extends Controller
{
    public function index()
    {
        $headers = ['category' => 'Clientes', 'title' => 'Grupos Musculares'];
        
        $usuariosistemas = Usuariosistema::all();
        return view('admin.gruposmusculares.index', compact('usuariosistemas', 'headers'));
    }

    public function edit($id)
    {
        $headers = ['category' => 'Clientes', 'title' => 'Grupos Musculares'];
        $usuariosistema = Usuariosistema::where('id', $id)->first();
        return view('admin.usuariosistema.edit', compact('usuariosistema', 'headers'));
    }

    public function create()
    {
        $headers = ['category' => 'Clientes', 'title' => 'Grupos Musculares'];
        return view('admin.usuariosistemas.create', compact('headers'));
    }

    public function update(UsuariosistemaRequest $request, $id)
    {
        $data = $request->all();

        Usuariosistema::where('id', $id)->update(['nmgrupo' => $data['nmgrupo']

       ]);
     
        return redirect()->route('admin.clientes.gruposmusculares.index');
    }

    public function store(UsuariosistemaRequest $request)
    {
        $data = $request->all();
       
        Usuariosistema::create(['nmgrupo' => $data['nmgrupo']]);
     
        return redirect()->route('admin.clientes.usuariosistemas.index');
    }
}
