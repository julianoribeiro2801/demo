<?php

//header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");

namespace App\Http\Controllers;

use DateTime;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserDados;
use App\Models\Configura;
use SoapClient;
use App\Models\Unidade;
use DB;
use Hash;
use Mail;
use Redirect;
// Precisa para funcionar o combo
use Auth;
use App\Services\FilialService;

class ApiScaController extends Controller {

    public function __construct(FilialService $filialService) {
        $this->filialService = $filialService;
    }

    public function changeEmp($id_unidade) {
        $this->filialService->changeEmp($id_unidade);

        return redirect(url()->previous());
    }

    public function index() {
        $headers = ['category' => 'Empresa', 'title' => 'Unidades'];

        $unidades = Unidade::with(['parent', 'children', 'matriz'])->get();

        return view('empresas.index', compact('unidades', 'headers'));
    }


    
    public function addUser(Request $request, $id, $nome, $idunidade, $email, $endereco, $bairro, $cep, $telefone, $celular, $sexo, $situacao, $cpf, $datanasc) {
        if ($sexo == "0") {
            $sexoA = "M";
        } else {
            $sexoA = "F";
        }
        if ($situacao == 'ENCERRADA') {
            $stAluno = "I";
        } else {
            $stAluno = "A";
        }


        if (UserDados::where('controle_id', $id)->where('idunidade', $idunidade)->first()) {
            $retorno['cod'] = '-1';
            $retorno['text'] = 'Cliente já existe na base de dados!';
        } else {
            $cliente = User::create(['name' => $nome,
                        'idunidade' => $idunidade,
                        'email' => $email,
                        'password' => '',
                        'role' => 'prospect'
            ]);
            //$xx=str_replace('|',  '',$endereco);
            $clienteDados = UserDados::create(['idunidade' => $idunidade,
                        'user_id' => $cliente->id,
                        'dt_nascimento' => $datanasc,
                        'endereco' => str_replace('|', '', $endereco),
                        'bairro' => $bairro,
                        'cep' => $cep,
                        'telefone' => $telefone,
                        'celular' => $celular,
                        'genero' => $sexoA,
                        'situacao' => $stAluno,
                        'situacaomatricula' => $situacao,
                        'cpf' => $cpf,
                        'profile_type' => 0,
                        'controle_id' => $id
            ]);

            $retorno['cod'] = '0';
            $retorno['text'] = 'Cliente ' . $cliente->id . ' inserido com sucesso!';
        };


        return response()->json(compact('retorno'));
    }

    public function updateMatricula( $idunidade, $aluno, $situacao) {
        
        $role='';
        if ($situacao == 'ENCERRADA') {
            $role='prospect';
            $stAluno = "I";
        } else {
            $role='cliente';
            $stAluno = "A";
        }

        
        $clienteDadosUserDados = UserDados::where('controle_id', $aluno)->where('idunidade', $idunidade)->update([
            'situacao' => $stAluno,
            //'role' => $role,
            'situacaomatricula' => $situacao
        ]);     
        
        $usuario = UserDados::select('user_id')
                ->where('controle_id', $aluno)
                ->where('idunidade', $idunidade)
                ->get();        
        if (sizeof($usuario)>0):
            $clienteUserDados = User::where('id', $usuario[0]['user_id'])->where('idunidade', $idunidade)->update([
                'role' => $role
            ]);     
                
            $retorno = 'ok';
            //$retorno['text'] = 'matricula atualizada com sucesso!';
        else:
            $retorno = 'erro';
            
        endif;        
       


        return $retorno;        
    }
    public function updateUser(Request $request, 
                                    $id, $nome, $idunidade, $email, 
                                    $endereco, $bairro, $cep, $telefone, 
                                    $celular, $sexo, $situacao, $cpf, 
                                    $datanasc) {
        if ($sexo == "0") {
            $sexoA = "M";
        } else {
            $sexoA = "F";
        }
        if ($situacao == 'ENCERRADA') {
            $stAluno = "I";
        } else {
            $stAluno = "A";
        }
       /* $cliente = User::where('controle_id', $id)->where('idunidade', $idunidade)->update([
            'name' => $nome,
            'idunidade' => $idunidade,
            'email' => $email,
            //'password' => '',
            'role' => 'cliente'
        ]);*/
        
        if (UserDados::where('controle_id', $id)->where('idunidade', $idunidade)->first()) {
            $clienteDadosUserDados= UserDados::where('controle_id', $id)->where('idunidade', $idunidade)->update([
                'idunidade' => $idunidade,
                //'user_id' => $cliente->id,
                'dt_nascimento' => $datanasc,
                'endereco' => str_replace('|', '', $endereco),
                'bairro' => $bairro,
                'cep' => $cep,
                'telefone' => $telefone,
                'celular' => $celular,
                'genero' => $sexoA,
                'situacao' => $stAluno,
                'situacaomatricula' => $situacao,
                'cpf' => $cpf
            ]);
        
            
        
        } else {    
            $cliente = User::create(['name' => $nome,
                        'idunidade' => $idunidade,
                        'email' => $email,
                        'password' => '',
                        'role' => 'prospect'
            ]);
            //$xx=str_replace('|',  '',$endereco);
            $clienteDados = UserDados::create(['idunidade' => $idunidade,
                        'user_id' => $cliente->id,
                        'dt_nascimento' => $datanasc,
                        'endereco' => str_replace('|', '', $endereco),
                        'bairro' => $bairro,
                        'cep' => $cep,
                        'telefone' => $telefone,
                        'celular' => $celular,
                        'genero' => $sexoA,
                        'situacao' => $stAluno,
                        'situacaomatricula' => $situacao,
                        'cpf' => $cpf,
                        'profile_type' => 0,
                        'controle_id' => $id
            ]);

            $retorno['cod'] = '0';
            $retorno['text'] = 'Cliente ' . $cliente->id . ' inserido com sucesso!';            
          //  $retorno['cod'] = '-1';
          //  $retorno['text'] = 'Cliente já existe na base de dados!';
        }
        
        
        /*if (Configura::where('idunidade',  $this->unidade)->first()) {
            $configura = Configura::where('idunidade',  $this->unidade)->update(['codigo_atualiza' => 0]);
        } else {
            $configura = Configura::create(['idunidade' => $this->unidade,
                'codigo_atualiza' => 0
            ]);
        };        */
        

        $retorno['cod'] = '0';
        $retorno['text'] = 'Cliente atualizado com sucesso!';


        return response()->json(compact('retorno'));
    }

    public function verifica(Request $request, $idunidade) {
        $configs = Configura::select('stadiciona')->where('idunidade', $idunidade)->first();
        if (sizeof($configs) > 0) {
            if ($configs->stadiciona == 'S') {
                $configura = Configura::where('idunidade', $idunidade)->update(['stadiciona' => 'N']);
                $retorno = $configs->stadiciona;
            } else {
                $retorno = $configs->stadiciona;
            }
        } else {
            $retorno = $configs[0]['stadiciona'];
            $configura = Configura::create(['idunidade' => $idunidade,
                        'stadiciona' => 'S'
            ]);
        };

        return $retorno;
    }
    public function verificaCadastro(Request $request, $idunidade) {
        $configs = Configura::select('codigo_atualiza')->where('idunidade', $idunidade)->first();
        if (sizeof($configs) > 0) {
            if ($configs->codigo_atualiza > 0 ) {
                $configura = Configura::where('idunidade', $idunidade)->update(['codigo_atualiza' => 0]);
                $retorno = $configs->codigo_atualiza;
            } else {
                $retorno = $configs->codigo_atualiza;
            }
        } else {
            $retorno = $configs[0]['codigo_atualiza'];
            $configura = Configura::create(['idunidade' => $idunidade,
                        'codigo_atualiza' => 0
            ]);
        };

        return $retorno;
    }    
    
    
    public function sincronizaSca( $id, $unidade ){

        if (Configura::where('idunidade',  $unidade)->first()) {
            $configura = Configura::where('idunidade',  $unidade)->update(['codigo_atualiza' => $id]);
        } else {
            $configura = Configura::create(['idunidade' => $unidade,
                'codigo_atualiza' => $id
            ]);
        };
        return 'ok';
    }    
    
    
    

}
