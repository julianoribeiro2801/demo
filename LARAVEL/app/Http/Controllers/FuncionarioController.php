<?php

namespace App\Http\Controllers;

use App\Services\ApiFuncionarioService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class FuncionarioController extends ApiCoreController
{
    private $service;

    public function __construct(ApiFuncionarioService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        return $this->service->index();
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->getMessages(), 400);
        }

        return $this->service->register($request);
    }

    public function data($id)
    {
        return $this->service->data($id);
    }

    public function update(Request $request, $id)
    {
        return $this->service->update($request, $id);
    }

    public function show($id)
    {
        return $this->service->show($id);
    }


    public function destroy($id)
    {
        return $this->service->destroy($id);
    }
}
