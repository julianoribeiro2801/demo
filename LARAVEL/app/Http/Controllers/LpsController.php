<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Hope;

use Hash;
use App\Http\Requests\AdminCategoryRequest;

// necessario para funcionar os combos
use App\Services\TrataDadosService;

// Precisa para funcionar o combo
use Auth;
use App\Services\FilialService;

class LpsController extends Controller
{
    public function __construct(
       TrataDadosService $trataDadosService,
      FilialService $filialService
   ) {
        $this->trataDadosService = $trataDadosService;
        $this->filialService = $filialService;
    }

    public function index()
    {
        $headers = ['category' => 'Lp', 'title' => 'Lp editar'];
        $unidades_combo = $this->filialService->unidadesComboTop();

        return view('lps.index', compact('headers', 'unidades_combo'));
    }

    public function lp($cliente)
    {
        $headers = ['category' => 'Clientes', 'title' => $cliente];
//        $categories = 	User::all();
        return view('lps.lp', compact('headers'));
    }
}
