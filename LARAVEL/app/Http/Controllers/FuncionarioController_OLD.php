<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Funcionario;
use App\Models\Pessoa;
use App\Models\User;
use Hash;
use App\Http\Requests\FuncionarioRequest;

use App\Services\TrataDadosService;

class FuncionarioController extends Controller
{
    public function __construct(TrataDadosService $trataDadosService)
    {
        $this->trataDadosService = $trataDadosService;
    }
    
    public function index()
    {
        $headers = ['category' => 'Empresa', 'title' => 'Funcionários'];
        
        $funcionarios = Funcionario::all();
        return view('admin.funcionarios.index', compact('funcionarios', 'headers'));
    }

    public function edit($id)
    {
        $headers = ['category' => 'Empresa', 'title' => 'Funcionários'];
        $funcionario = Funcionario::where('id', $id)->first();
        $nomes =Pessoa::select('id', 'nmpessoa')->get();
        $selectedPessoas = $this->trataDadosService->listToSelectPessoas($nomes);
        $sns=Funcionario::select('id', 'stprofessor')->get();
        $selectedSns = $this->trataDadosService->listToSelectPessoas($sns);
      
        return view('admin.funcionarios.edit', compact('funcionario', 'selectedPessoas', 'selectedSns', 'headers'));
    }

    public function create()
    {
        $headers = ['category' => 'Empresa', 'title' => 'Funcionários'];
        $pessoas =Pessoa::select('id', 'nmpessoa')->get();
        $selectedPessoas = $this->trataDadosService->listToSelectPessoas($pessoas);
        $sns='';
        $selectedSns = $this->trataDadosService->listToSelectSns($sns);

        return view('admin.funcionarios.create', compact('selectedPessoas', 'selectedSns', 'headers'));
    }

    public function update(FuncionarioRequest $request, $id)
    {
        $data = $request->all();

        Funcionario::where('id', $id)->update(['nmpessoa' => $data['nmpessoa'],
        'idunidade' => 1,
        'idpessoa' => $data['idpessoa'],
        'stprofessor' => $data['stprofessor']

       ]);
     
        return redirect()->route('admin.empresa.funcionarios.index');
    }
    public function lista()
    {
        return User::all();
    }

    public function store(FuncionarioRequest $request)
    {
        $data = $request->all();
      
        $verificaemail = User::where('email', $data['email'])->first();
        if (count($verificaemail)>0) {
            return "Email já existe";
        } else {
            DB::beginTransaction();
          
            try {
                $funcionario = User::create(['name' => $data['name'],
                'idunidade' => 1,
                'email' => $data['email'],
                'password' => Hash::make($data['password']),
                'role' => 'funcionario'
              ]);
              
                $idcidade = Cidade::select('id')->where('nome', $data['cidade'])->first();
         
                UserDados::create(['user_id' => $cliente->id,
                'idunidade' => 1,
                'telefoness' => $data['telefone'],
                'idcidade' => $idcidade->id,
                'endereco' => $data['endereco'],
                'numero' => $data['numero'],
                'bairro' => $data['bairro'],
                'dt_nascimento' => $data['dt_nascimento'],
                'CPF' => $data['CPF']
              ]);
              
                DB::commitTransaction();
            } catch (Illuminate\Database\QueryException $e) {
                DB::rollbackTransaction();
                return "Não foi possivel salvar";
            }
        }
      
     
        return "Salvo com sucesso";
    }


    // public function create_user(){

          //  User::create( [
    //          'email' => 'kk@msn.com' ,
    //          'password' => Hash::make( '123456' ) ,
    //          'name' => 'KK Ruiz' ,
    //          'role' => 'admin'
    //         ] );
    // }
}
