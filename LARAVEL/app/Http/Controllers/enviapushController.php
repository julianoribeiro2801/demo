<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

use Hash;
use Auth;
use App\Services\FilialService;
use App\Models\User;

class enviapushController extends Controller
{
    protected $rules = [
        'message' => 'requried|min:10'
    ];
    protected $messages = [
        'required' => ':attribute é obrigatório',
        'min' => ':attribute precisa de pelo menos :min caracteres',
    ];
    public function send(request $request)
    {
        $client = new Client;
        $client->request('POST', 'https://fcm.googleapis.com/fcm/send', [
            'headers' => [
                'Authorization' => 'key=AAAAsk7DCU4:APA91bFZVdiP4uRz43MQJjats2cVUBsDl_sB2a9TZVmgPlKDzGeDCgyMKV8NviiwgunEzPRE1jUXLnjdIe8DqrSakiuojHrJGrf9jrZU6oOy0uww4pG0O2ormIvtuQtWR78RmaNa2QGl'
            ],
            'json' => [
                'to' => '/topics/cliente',
                'notification' => [
                    'title' => $request->input('title'),
                    'body' => $request->input('message'),
                    'icon' => 'ic_stat_chat',
                    'sound' => 'default',
                    'color' => '#301954'
                ]
            ]
        ]);
        //'icon' => 'http://sistemapcb.com.br/icone_push.png'
        return response()->json(['status' => 'success']);
    }

    public function registration(request $request)
    {
        $client = new Client;
        $client->request('POST', 'https://iid.googleapis.com/iid/v1/'. $request->input('token') .'/rel/topics/cliente', [
            'headers' => [
                'Authorization' => 'key=AAAAsk7DCU4:APA91bFZVdiP4uRz43MQJjats2cVUBsDl_sB2a9TZVmgPlKDzGeDCgyMKV8NviiwgunEzPRE1jUXLnjdIe8DqrSakiuojHrJGrf9jrZU6oOy0uww4pG0O2ormIvtuQtWR78RmaNa2QGl'
            ],
            'json' => []
        ]);
        return response()->json(['status' => 'success']);
    }




    ##########
    ###### JULIANO FEZ

    //definir tipos de mensagem


    ///
    public function enviaPush(request $request)
    {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        

        $user_logued= User::select('users.id', 'users.name', 'users.device_id')
                    //->where('users.idunidade', $idunidade)
                    ->where('users.id', $request->header('user'))
                    ->get();
        $users = User::select('users.id', 'users.name', 'users.device_id')
                    ->where('users.idunidade', $idunidade)
                    ->where('users.id', $request->person)
                    ->get();
        
        $client = new Client;
        $client->request('POST', 'https://fcm.googleapis.com/fcm/send', [
            'headers' => [
                'Authorization' => 'key=AAAAsk7DCU4:APA91bFZVdiP4uRz43MQJjats2cVUBsDl_sB2a9TZVmgPlKDzGeDCgyMKV8NviiwgunEzPRE1jUXLnjdIe8DqrSakiuojHrJGrf9jrZU6oOy0uww4pG0O2ormIvtuQtWR78RmaNa2QGl'
            ],
            'json' => [

                'to' =>$users[0]['device_id'],
                'notification' => [
                    'title' => $user_logued[0]['name'],//$request->input('title'),
                    'body' => $request->input('message'),
                    'icon' => 'ic_stat_chat',
                    'user_id' => $users[0]['id'],
                    'sound' => 'default',
                    'color' => '#301954'
                ]
            ]
        ]);
        return response()->json(['status' => 'success']);
    }    
    
    public function enviaPush_old(request $request)
    {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        $users = User::select('users.id', 'users.name', 'users.device_id')
                    ->where('users.idunidade', $idunidade)
                    ->where('users.id', $request->person)
                    ->get();

        $apiKey = "AIzaSyCkzr5eV_YEtkKM4dnF3A6HqhOYgQO-sEY";
        //$regId = "765825583438";
        $proxy=null;
                                           

        $regIds=$users[0]['device_id']; //'APA91bEDGUZu_JDx8hZM3rRztNSXPJGtsJfVeMXA8nBE3mtsfH8VAruIGjIVlFi0HJMR6SnwzUsHXrL_Kfj7h5Yx0MDeHtN6v4gd_3lQNP3QwlpJqcAbwR0xLnC7wjbcZ66yIcQ0PlTC';
        $data='<b>' . $request->message . '</b>';
        //$output='';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://fcm.googleapis.com/fcm/send");
        if (!is_null($proxy)) {
            curl_setopt($ch, CURLOPT_PROXY, $proxy);
        }
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->getHeaders($apiKey));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->getPostFields($regIds, $data));

        $result = curl_exec($ch);
        if ($result === false) {
            throw new \Exception(curl_error($ch));
        }

        curl_close($ch);
        $output = $result;
                
                    
                    
        return $users;
    }

    public function getHeaders($apiKey)
    {
        return [
            'Authorization: key=' . $apiKey,
            'Content-Type: application/json'
        ];
    }
    public function getPostFields($regIds, $data)
    {
        $fields = [
            'registration_ids' => is_string($regIds) ? [$regIds] : $regIds,
            'data'             => is_string($data) ? ['message' => $data] : $data,
        ];

        return json_encode($fields, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
    }
}
