<?php

namespace App\Http\Controllers;

//

use File;
use FFMpeg;
use Storage;
//

use Illuminate\Http\Request;
use App\Models\Local;
use Hash;
use App\Http\Requests\LocalRequest;

class TesteController extends Controller
{


    // public function testeStorage(){
    // $diskvideo = Storage::disk('videos');
    // $diskvideo->put("ola.txt", "ola exemplo");
    //  if($diskvideo->exists("ola.txt")) {
//     return $diskvideo->get("ola.txt");
    //  }
    // }

    // teste video inicio
   
    public function ffmpeg()
    {
        FFMpeg::fromDisk('videos')
        ->open('ABD-SUSPENSO.mp4')
        ->getFrameFromSeconds(1)
        ->export()
        ->toDisk('videos')
        ->save('FrameAt10sec.png');

      

        return  'IMAGEM GERADA COM SUCESSO';
    }
    // fim video inicio


    public function index()
    {
        $headers = ['category' => 'Empresa', 'title' => 'Locais'];
        //$locais = Local::all();
        return view('admin.teste.index');
    }

    public function edit($id)
    {
        $headers = ['category' => 'Empresa', 'title' => 'Locais'];
        $local = Local::where('id', $id)->first();
        return view('admin.locais.edit', compact('local', 'headers'));
    }

    public function create()
    {
        $headers = ['category' => 'Empresa', 'title' => 'Locais'];
        return view('admin.locais.create', compact('headers'));
    }

    public function update(LocalRequest $request, $id)
    {
        $data = $request->all();

        Local::where('id', $id)->update(['nmlocal' => $data['nmlocal'],
                                       'idunidade' => 1
       ]);
     
        return redirect()->route('admin.empresa.locais.index');
    }

    public function store(LocalRequest $request)
    {
        $data = $request->all();
       
        Local::create(['nmlocal' => $data['nmlocal'],
                     'idunidade' => 1
      ]);
        return redirect()->route('admin.empresa.locais.index');
    }


    // public function create_user(){

          //  User::create( [
    //          'email' => 'kk@msn.com' ,
    //          'password' => Hash::make( '123456' ) ,
    //          'name' => 'KK Ruiz' ,
    //          'role' => 'admin'
    //         ] );
    // }
}
