<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Input;
// use Request;
use Image;
use File;
use FFMpeg;
use Storage;
use App\Models\Exercicio;
use App\Models\Grupomuscular;
use App\Models\ExercicioBlacklist;
use Auth;
use App\Services\FilialService;
use Cache;

class ExercicioController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
        setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
        date_default_timezone_set('America/Sao_Paulo');
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function insere() {
        //if (Request::ajax()) {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        $data = Input::all();
        $data['idunidade'] = $idunidade;
        if (isset($data['nmgrupo']) && $data['nmgrupo'] != '') {
            $grupo = Grupomuscular::create(['nmgrupo' => $data['nmgrupo']]);
            $data['idgrupomuscular'] = $grupo->id;
        }

        $teste = 0;
        if (Input::hasFile('video')):
            $file = Input::file('video');

            $teste = filesize($file); //1048576       
        //echo $teste .  'k';
        endif;
        if ($teste <= 1048576) {

            if (Input::hasFile('dsurlminiatura')):
                $file = Input::file('dsurlminiatura');
                $image_name = time() . "-" . $file->getClientOriginalName();
                $file->move('uploads/exercicios', $image_name);
                $image = Image::make(sprintf('uploads/exercicios/%s', $image_name))->resize(260, null, function ($constraint) {
                            $constraint->aspectRatio();
                        })->save();
                $data['dsurlminiatura'] = $image_name;
            endif;



            unset($data['nmgrupo']);
            unset($data['radioInline']);


//        $exercicio = new Exercicio();


            $exercicio = Exercicio::create($data);

            if (sizeof($exercicio) > 0):
                $retorno['title'] = 'Sucesso!';
                $retorno['type'] = 'success';
                $retorno['text'] = 'Exercício <b>' . $data['nmexercicio'] . '</b> cadastrado com sucesso!';
            else:
                $retorno['title'] = 'Erro!';
                $retorno['type'] = 'error';
                $retorno['text'] = 'Erro ao cadastrar exercício <b>' . $data['nmexercicio'] . '</b>!';
            endif;

            if (Input::hasFile('video')):
                //renomeia antes de subir arquivo
                $arquivo_antigo = "uploads/exercicios/" . str_pad($exercicio->id, 6, "0", STR_PAD_LEFT) . ".mp4";
                $arquivo_novo = "uploads/exercicios/" . str_pad($exercicio->id, 6, "0", STR_PAD_LEFT) . "-" . time() . ".mp4_old";
                if (file_exists($arquivo_antigo)) {
                    $ret = rename($arquivo_antigo, $arquivo_novo); // Resultado: TRUE /
                }
                /////////////////////////////////



                $file = Input::file('video');
                $image_name = "" . str_pad($exercicio->id, 6, "0", STR_PAD_LEFT) . ".mp4";
                $file->move('uploads/exercicios', $image_name);
                $data['video'] = $image_name;

                //gerar miniatura
                $nameVideoFile = str_pad($exercicio->id, 6, "0", STR_PAD_LEFT) . ".mp4";
                $nameImageFile = str_pad($exercicio->id, 6, "0", STR_PAD_LEFT) . ".jpg";
                FFMpeg::fromDisk('videos')
                        ->open($nameVideoFile)
                        ->getFrameFromSeconds(1)
                        ->export()
                        ->toDisk('miniaturas')
                        ->save($nameImageFile);
                //miniatura do frame
                $data['dsurlminiatura'] = $nameImageFile;


            endif;
            //$exercicio = new Exercicio();
            if (isset($data['video']) && $data['video'] != '') {
                if (Exercicio::where('id', $exercicio->id)->where('idunidade', $idunidade)->update(['video' => $data['video'], 'dsurlminiatura' => $data['dsurlminiatura']])):
                    $retorno['title'] = 'Sucesso!';
                    $retorno['type'] = 'success';
                    $retorno['text'] = 'Exercício <b>' . $data['nmexercicio'] . '</b> atualizado com sucesso!';
                else:
                    $retorno['title'] = 'Erro!';
                    $retorno['type'] = 'error';
                    $retorno['text'] = 'Erro ao atualizar exercício <b>' . $data['nmexercicio'] . '</b>!';
                endif;
            }
        } else {
            $retorno['title'] = 'Erro!';
            $retorno['type'] = 'error';
            $retorno['text'] = 'Vídeo ultrapassa o tamanho permitido</b>!';
        }

        return $retorno;
    }

    public function gerarMiniatura() {
        $data = Input::all();

        //gerar miniatura
        $nameVideoFile = str_pad($data['id'], 6, "0", STR_PAD_LEFT) . ".mp4";
        $nameImageFile = str_pad($data['id'], 6, "0", STR_PAD_LEFT) . ".jpg";
        FFMpeg::fromDisk('videos')
                ->open($nameVideoFile)
                ->getFrameFromSeconds(1)
                ->export()
                ->toDisk('miniaturas')
                ->save($nameImageFile);
        //miniatura do frame
        $dataX['dsurlminiatura'] = $nameImageFile;
        $exercicio = new Exercicio();
        if ($exercicio->where('id', '=', $data['id'])->update($dataX)):
            $retorno['title'] = 'Sucesso!';
            $retorno['type'] = 'success';
            $retorno['text'] = 'Exercício <b>' . $data['nmexercicio'] . '</b> atualizado com sucesso!';
        else:
            $retorno['title'] = 'Erro!';
            $retorno['type'] = 'error';
            $retorno['text'] = 'Erro ao atualizar exercício <b>' . $data['nmexercicio'] . '</b>!';
        endif;



        return $data;
    }

    public function atualiza() {
        // if (Request::ajax()) {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        $data = Input::all();
        $teste = 0;
        if (Input::hasFile('video')):
            $file = Input::file('video');

            $teste = filesize($file); //1048576       
        //echo $teste .  'k';
        endif;
        if ($teste <= 1048576):




            unset($data['radioInline']);

            //$data['idunidade']=$idunidade;
            $data['idgrupomuscular'] = preg_replace("/[^0-9]/", "", $data['idgrupomuscular']);
            if (isset($data['nmgrupo']) && $data['nmgrupo'] != '') {
                $grupo = Grupomuscular::create(['nmgrupo' => $data['nmgrupo']]);
                $data['idgrupomuscular'] = $grupo->id;
            }

            $exercicio = new Exercicio();
            $dataExercicio = $exercicio->select('dsurlminiatura', 'video')->where('id', $data['id'])->get();
            $imagemAtual = 'uploads/exercicios/' . $dataExercicio[0]['dsurlminiatura'];
            $videoAtual = 'uploads/exercicios/' . $dataExercicio[0]['video'];

            if (Input::hasFile('dsurlminiatura')):
                File::delete($imagemAtual);

                $file = Input::file('dsurlminiatura');
                $image_name = time() . "-" . $file->getClientOriginalName();
                $file->move('uploads/exercicios', $image_name);
                $image = Image::make(sprintf('uploads/exercicios/%s', $image_name))->resize(260, null, function ($constraint) {
                            $constraint->aspectRatio();
                        })->save();
                $data['dsurlminiatura'] = $image_name;
            else:
                unset($data['dsurlminiatura']);
            endif;

            if (Input::hasFile('video')):
                //File::delete($videoAtual);
                $arquivo_antigo = "uploads/exercicios/" . str_pad($data['id'], 6, "0", STR_PAD_LEFT) . ".mp4";
                $arquivo_novo = "uploads/exercicios/" . str_pad($data['id'], 6, "0", STR_PAD_LEFT) . "-" . time() . ".mp4";
                if (file_exists($arquivo_antigo)) {
                    $ret = rename($arquivo_antigo, $arquivo_novo); // Resultado: TRUE /
                }
                /////////////////////////////////
                $file = Input::file('video');





                //$image_name = time()."-".$file->getClientOriginalName();
                $image_name = "" . str_pad($data['id'], 6, "0", STR_PAD_LEFT) . ".mp4";
                $file->move('uploads/exercicios', $image_name);
                $data['video'] = $image_name;


                //gerar miniatura
                $nameVideoFile = str_pad($data['id'], 6, "0", STR_PAD_LEFT) . ".mp4";
                $nameImageFile = str_pad($data['id'], 6, "0", STR_PAD_LEFT) . ".jpg";
                FFMpeg::fromDisk('videos')
                        ->open($nameVideoFile)
                        ->getFrameFromSeconds(1)
                        ->export()
                        ->toDisk('miniaturas')
                        ->save($nameImageFile);
                //miniatura do frame
                $data['dsurlminiatura'] = $nameImageFile;
            else:
                unset($data['video']);
            endif;

            $exercicio = new Exercicio();
            unset($data['SendPostForm']);
            unset($data['files']);
            unset($data['nmgrupo']);
            if ($exercicio->where('id', '=', $data['id'])->update($data)):
                $retorno['title'] = 'Sucesso!';
                $retorno['type'] = 'success';
                $retorno['text'] = 'Exercício <b>' . $data['nmexercicio'] . '</b> atualizado com sucesso!';
            else:
                $retorno['title'] = 'Erro!';
                $retorno['type'] = 'error';
                $retorno['text'] = 'Erro ao atualizar exercício <b>' . $data['nmexercicio'] . '</b>!';
            endif;

        else:
            $retorno['title'] = 'Erro!';
            $retorno['type'] = 'error';
            $retorno['text'] = 'Vídeo ultrapassa o tamanho permitido</b>!';
        endif;
        return $retorno;
        //}
    }

    public function getExercicios() {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }

        $exercicios = Exercicio::select('exercicio.idunidade', 'exercicio.id', 'exercicio.nmexercicio', 'exercicio.idgrupomuscular', 'exercicio.dsurlminiatura', 'grupomuscular.nmgrupo', 'exercicio.video')
                ->leftjoin('grupomuscular', 'grupomuscular.id', 'exercicio.idgrupomuscular')
                ->wherein('exercicio.idunidade', [$idunidade, 0])
                ->whereNotIn('exercicio.id', function($query) {

                    $query->select('exercicio_id')
                    ->from(with(new ExercicioBlacklist)->getTable())
                    ->where(function($query) {
                        $query->where('unidade_id', '=', session()->get('id_unidade')); //--> so
                    });
                })
                ->orderBy('exercicio.id')
                ->paginate(40);



        /* $exercicios=DB::select("select e.idunidade,e.id, e.nmexercicio, e.idgrupomuscular, "
          . "e.dsurlminiatura, g.nmgrupo, e.video from  "
          . " grupomuscular g ,exercicio e "
          . " where e.idgrupomuscular = g.id "
          . " and e.id not in (select exercicio_id from exercicio_blacklist "
          . " where unidade_id = " . $idunidade . ")"
          . " and e.idunidade in( " . $idunidade . ",0)"
          . " order by e.id desc"); */

        if (!Cache::has('gruposmusculares')) {
            Cache::put('gruposmusculares', DB::select("select * from grupomuscular"), 1);
        }

        $gruposmusculares = Cache::get('gruposmusculares');

        return response()->json(compact('exercicios', 'gruposmusculares'));
    }

//  busca
//  
    public function getExerciciosBusca($nome) {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        $exercicios = Exercicio::select('exercicio.idunidade', 'exercicio.id', 'exercicio.nmexercicio', 'exercicio.idgrupomuscular', 'exercicio.dsurlminiatura', 'grupomuscular.nmgrupo', 'exercicio.video')
                ->leftjoin('grupomuscular', 'grupomuscular.id', 'exercicio.idgrupomuscular')
                ->wherein('exercicio.idunidade', [$idunidade, 0])
                ->where('exercicio.nmexercicio', 'like', "%{$nome}%")
                ->whereNotIn('exercicio.id', function($query) {

                    $query->select('exercicio_id')
                    ->from(with(new ExercicioBlacklist)->getTable())
                    ->where(function($query) {
                        $query->where('unidade_id', '=', session()->get('id_unidade')); //--> so
                    });
                })
                ->orderBy('exercicio.id')
                ->paginate(40);

        /* $exercicios=DB::table('exercicio')
          ->where('exercicio.nmexercicio', 'like', "%{$nome}%")
          ->orderby('exercicio.id','desc')->paginate(40); */


        if (!Cache::has('gruposmusculares')) {
            Cache::put('gruposmusculares', DB::select("select * from grupomuscular"), 1);
        }

        $gruposmusculares = Cache::get('gruposmusculares');

        return response()->json(compact('exercicios', 'gruposmusculares'));
    }

    public function deleteExercicio($id) {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }

        //$exer=DB::select("select idunidade from exercicio where id = " . $id );
        $exer = Exercicio::select('id', 'nmexercicio', 'idunidade')->where('id', $id)->get();
        //return response()->json(compact('exer'));

        if ($idunidade == $exer[0]['idunidade']) {
            $exercicio = new Exercicio();
            $exercicio->where('id', '=', $id)->delete();
        } else {
            $dataBlack['unidade_id'] = $idunidade;
            $dataBlack['exercicio_id'] = $id;
            $black = new ExercicioBlacklist();
            if ($black->create($dataBlack)):
                $retorno['title'] = 'Sucesso!';
                $retorno['type'] = 'success';
                $retorno['text'] = 'Exercício removido com sucesso!';
            else:
                $retorno['title'] = 'Erro!';
                $retorno['type'] = 'error';
                $retorno['text'] = 'Erro ao excluir exercício!';
            endif;
        }
    }

}
