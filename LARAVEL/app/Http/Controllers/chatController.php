<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Local;
use Hash;
use App\Http\Requests\LocalRequest;
// Precisa para funcionar o combo
use Auth;
use App\Services\FilialService;

class ChatController extends Controller
{
    public function __construct(FilialService $filialService)
    {
        $this->filialService = $filialService;
    }

    public function index()
    {
        $unidades_combo = $this->filialService->unidadesComboTop();
       
        $headers = ['category' => 'Clientes', 'title' => 'Chat'];
        return view('admin.chat.index', compact('headers', 'unidades_combo'));
    }
}
