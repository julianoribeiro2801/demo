<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Psa;
use App\Models\Aluno;
use App\Models\programa;
use Hash;
use App\Http\Requests\PsaRequest;
use App\Services\TrataDadosService;

class PsaController extends Controller
{
    public function __construct(TrataDadosService $trataDadosService)
    {
        $this->trataDadosService = $trataDadosService;
    }
    
    public function index()
    {
        $headers = ['category' => 'Clientes', 'title' => 'PSA '];
        $psas = Psa::all();
       
        return view('admin.psas.index', compact('psas', 'headers'));
    }

    public function edit($id)
    {
        $headers = ['category' => 'Clientes', 'title' => 'PSA '];
        $psa = Psa::where('id', $id)->first();
        return view('admin.psas.edit', compact('psa', 'headers'));
    }

    public function create()
    {
        $headers = ['category' => 'Clientes', 'title' => 'PSA '];
        $alunos = Aluno::select('id', 'name')->where('role', 'cliente')->orderby('name')->get();
        $selectedAlunos = $this->trataDadosService->listToSelectAlunos($alunos);
 
        $programas = Programa::select('id', 'nmprograma')->get();
        $selectedProgramas = $this->trataDadosService->listToSelectProgramas($programas);
      
        return view('admin.psas.create', compact('selectedAlunos', 'selectedProgramas', 'headers'));
    }

    public function update(PsaRequest $request, $id)
    {
        $data = $request->all();

        Psa::where('id', $id)->update(['nmlocal' => $data['nmlocal'],
        'tpmensagem' => $data['tpmensagem']

       ]);
     
        return redirect()->route('admin.clientes.psas.index');
    }

    public function store(PsaRequest $request)
    {
        $data = $request->all();
       
        Psa::create(['nmlocal' => $data['nmlocal'],
        'tpmensagem' => $data['tpmensagem']]);
     
        return redirect()->route('admin.clientes.psas.index');
    }


    // public function create_user(){

          //  User::create( [
    //          'email' => 'kk@msn.com' ,
    //          'password' => Hash::make( '123456' ) ,
    //          'name' => 'KK Ruiz' ,
    //          'role' => 'admin'
    //         ] );
    // }
}
