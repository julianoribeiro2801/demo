<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// Precisa para funcionar o combo
use Auth;
use App\Services\FilialService;
use App\Services\SendPushService;
use DB;
use Input;
use App\Models\PrescricaoCalendario;
use App\Models\Agendaavalsemana;
use App\Models\Agendaavalaluno;

class AgendamentoController extends Controller {

    public function __construct(FilialService $filialService,SendPushService $pushService) {
        $this->filialService = $filialService;
        $this->pushService = $pushService;
    }
    
    public function index() {
        $headers = ['category' => 'AGENDAMENTO DE AVALIAÇÃO FÍSICA', 'title' => 'AGENDAMENTO DE AVALIAÇÃO FÍSICA'];
        return view('admin.agendamento.index', compact('headers'));
    }

    public function getAgendamentos($dia) {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        $diaAtual = $dia;
        $diasemana = (date('w', strtotime($diaAtual))) + 1;
        //'gfm_reserva.n_reservas', 'gfm_reserva.id as reserva_id')

        $sql = "SELECT aas.id , coalesce(aaa.id) as idagendamento, DATE_FORMAT(aas.horario,'%H:%i') as horario, coalesce(users.name,'') as name FROM agendaavalsemana as aas left join agendaavalaluno as aaa
              ON aaa.iddiasemana = aas.diasemana
              and aaa.idunidade = aas.idunidade
              and aaa.agenda_id = aas.id
              and aas.diasemana = $diasemana
              and aaa.dtagenda ='$diaAtual'
              left join users on aaa.idaluno = users.id 
              where aas.diasemana =$diasemana
              and aas.idunidade =$idunidade
              order by aas.horario asc";
        //echo $sql;

        $agendamentos = DB::select($sql);
        /* $agendamentos = Agendaavalsemana::select('agendaavalsemana.id','agendaavalsemana.horario','agendaavalsemana.idunidade','agendaavalaluno.idaluno','users.name')
          ->leftJoin('agendaavalaluno', 'agendaavalaluno.agenda_id', '=', 'agendaavalsemana.id')
          ->leftJoin('users', 'users.id', '=', 'agendaavalaluno.idaluno')
          ->where('agendaavalsemana.idunidade','=',$idunidade)
          ->where('agendaavalsemana.diasemana','=',$diasemana)
          ->where('agendaavalaluno.dtagenda','=',$diaAtual)
          ->orderBy('agendaavalsemana.horario', 'ASC')
          ->get(); */

        return response()->json(compact('agendamentos'));
    }

    public function addAgendamento() {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }

        $data = Input::all();

        $agendamento = Agendaavalsemana::select('agendaavalsemana.*')
                ->where('idunidade', '=', $idunidade)
                ->where('id', '=', $data['idagenda'])
                ->orderBy('agendaavalsemana.horario', 'ASC')
                ->get();


        $dataReserva['idaluno'] = $data['aluno'];
        $dataReserva['agenda_id'] = $data['idagenda'];
        $dataReserva['idunidade'] = $idunidade;
        $dataReserva['dtagenda'] = $data['dtagenda'];
        $dataReserva['horario'] = date('H:i', strtotime($agendamento[0]['horario']));
        $dataReserva['iddiasemana'] = $agendamento[0]['diasemana'];
        $reservaNew = Agendaavalaluno::create($dataReserva);
        if (sizeof($reservaNew)) {
            $retorno['cod'] = '0';
            $retorno['text'] = 'Avaliação reservada com sucesso!';
        } else {
            $retorno['cod'] = '-1';
            $retorno['text'] = 'Ocorreu um erro ao agendar avaliação, tente novamente!';
        }
        $this->enviaPushNovo($data['aluno'],8,$dataReserva['dtagenda'],$dataReserva['horario']);  /// envia push qdo cria um novo treino
        
        return response()->json(compact('retorno'));
    }
    function enviaPushNovo($idaluno,$tipo,$dataagenda, $horario){
    	$dados['sender_id']= $idprofessor = Auth::id();
    	$dados['receiver_id']=$idaluno;
    	$dados['push_type']=$tipo;
    	$dados['push_dtagenda']=$dataagenda;
    	$dados['push_horario']=$horario;
        
        $this->pushService->enviaPushAgendamento($dados);        
    }

    public function retDiaSemana($id) {
        switch ($id) {
            case 1:
                $dia = 'Domingo';
                # code...
                break;
            case 2:
                $dia = 'Segunda';
                # code...
                break;
            case 3:
                $dia = 'Terça';
                #code...
                break;
            case 4:
                $dia = 'Quarta';
                # code...
                break;
            case 5:
                $dia = 'Quinta';
                # code...
                break;
            case 6:
                $dia = 'Sexta';
                # code...
                break;
            case 7:
                $dia = 'Sábado';
                # code...
                break;
           
        }
         return $dia; 
    }

    public function getHorarios() {
        
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }

        $horarios = Agendaavalsemana::where('idunidade', $idunidade)
        ->orderBy('diasemana','ASC')
        ->orderBy('horario','ASC')->get();

         foreach ($horarios as $value) {
             $value->horario = substr($value->horario, 0,5);
 
            $value->diasemana = $this->retDiaSemana($value->diasemana);
         }
        


        return $horarios;
    }
    public function delHorario($id) {
        
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        
        if (Agendaavalsemana::where('id', $id)
                        ->where('idunidade', $idunidade)
                        ->delete()) {
                $retorno['cod'] = '0';
                $retorno['text'] = 'Horario removido com sucesso!';
            } else {
                $retorno['cod'] = '-1';
                $retorno['text'] = 'Ocorreu um erro ao remover horário, tente novamente!';
        }
        
       return response()->json(compact('retorno'));
        
    }

    public function addHorario(Request $request) {

        // return $request->all();

        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }

        $data = Input::all();


        if (isset($data['duracao'])):


            $existe = Agendaavalsemana::where('diasemana', $data['dia'])
                            ->where('horario', $data['horario'])
                            ->where('idunidade', $idunidade)->get();



            $hora = $data['horario'];
            // Somo 5 minutos (resultado em int)
            $x = $data['duracao'];
            $horaNova = strtotime("$hora + $x minutes");
            // Formato o resultado
            $horaNovaFormatada = date("H:i", $horaNova);


            $iniFim = DB::table('agendaavalsemana')
                            ->whereBetween('horario', [$data['horario'], $horaNovaFormatada])->get();

            if (sizeof($existe) <= 0) {
                $dataReserva['idunidade'] = $idunidade;
                $dataReserva['diasemana'] = $data['dia'];
                $dataReserva['horario'] = $data['horario'];
                $dataReserva['duracao'] = $data['duracao'];
                $reservaNew = Agendaavalsemana::create($dataReserva);
                if (sizeof($reservaNew)) {
                    $retorno['cod'] = '0';
                    $retorno['text'] = 'Horario gravado com sucesso!';
                } else {
                    $retorno['cod'] = '-1';
                    $retorno['text'] = 'Ocorreu um erro ao gravar horário, tente novamente!';
                }
            } else {
                $retorno['cod'] = '-1';
                $retorno['text'] = 'Horário já existe nesse dia da semana!';
            }
        else:
            $retorno['cod'] = '-1';
            $retorno['text'] = 'Informe a duração!';
        endif;


        return response()->json(compact('retorno', 'existe', 'horaNovaFormatada', 'iniFim'));
    }

    public function cancelaAgendamento() {
        $data = Input::all();
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        if (Agendaavalaluno::where('id', $data['id'])
                        ->where('idunidade', $idunidade)
                        ->delete()) {
            $retorno['cod'] = '0';
            $retorno['text'] = 'Agendamento removido com sucesso!';
        } else {
            $retorno['cod'] = '-1';
            $retorno['text'] = 'Não existem agendamentos para remover!';
        }
        return response()->json(compact('retorno'));
    }

    // CALENDARIO
    public function MostreSemanas() {
        $semanas = array(
            'Domingo',
            'Segunda-Feira',
            'Terça-Feira',
            'Quarta-Feira',
            'Quinta-Feira',
            'Sexta-Feira',
            'Sábado'
        );
        for ($i = 0; $i < 7; $i++) {
            echo "<th>" . $semanas[$i] . "</th>";
        }
    }

    public function GetNumeroDias($mes) {
        $numero_dias = array(
            '01' => 31, '02' => 28, '03' => 31, '04' => 30, '05' => 31, '06' => 30,
            '07' => 31, '08' => 31, '09' => 30, '10' => 31, '11' => 30, '12' => 31
        );
        if (((date('Y') % 4) == 0 and ( date('Y') % 100) != 0) or ( date('Y') % 400) == 0) {
            $numero_dias['02'] = 29; // altera o numero de dias de fevereiro se o ano for bissexto
        }
        return $numero_dias[$mes];
    }

    public function GetNomeMes($mes) {
        $meses = array('01' => "Janeiro", '02' => "Fevereiro", '03' => "Março",
            '04' => "Abril", '05' => "Maio", '06' => "Junho",
            '07' => "Julho", '08' => "Agosto", '09' => "Setembro",
            '10' => "Outubro", '11' => "Novembro", '12' => "Dezembro"
        );

        if ($mes >= 01 && $mes <= 12) {
            return $meses[$mes];
        } else {
            return "Mês deconhecido";
        }
    }

    public function MostreCalendario($mes, $ano, $tipo, $client_id) {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }


        $numero_dias = $this->GetNumeroDias($mes); // retorna o número de dias que tem o mês desejado
        $nome_mes = $this->GetNomeMes($mes);
        $diacorrente = 0;
        $diasemana = jddayofweek(cal_to_jd(CAL_GREGORIAN, $mes, "01", $ano), 0); // função que descobre o dia da semana
        // busca desc_prescricao
        $prescicoesAluno = PrescricaoCalendario::where('user_id_aluno', $client_id)->where('tipo_prescricao', $tipo)->where('idunidade', $idunidade)->whereMonth('data_prescricao', $mes)->whereYear('data_prescricao', $ano)->get();
        $arrayDesc = [];
        foreach ($prescicoesAluno as $prescicaoAluno) {
            $arrayDesc += array(substr($prescicaoAluno['data_prescricao'], 8, 2) => $prescicaoAluno['desc_prescricao']);
        }

        echo "<h2 class='pull-left'>" . $nome_mes . " de " . $ano . "</h2><input type='hidden' id='mesatual' value='" . $mes . "'><input type='hidden' id='anoatual' value='" . $ano . "'>";

        echo "<table class='table table-bordered'>";
        echo "<thead>";
        echo "<tr>";
        $this->MostreSemanas(); // função que mostra as semanas aqui
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";
        for ($linha = 0; $linha < 6; $linha++) {
            echo "<tr>";
            for ($coluna = 0; $coluna < 7; $coluna++) {


                if (($diacorrente == (date('d') - 1) && date('m') == $mes)) {
                    echo "<td id='dia_atual'>";
                   
                } else {
                    if (($diacorrente + 1) <= $numero_dias) {
                        if ($coluna < $diasemana && $linha == 0) {
                            echo "<td id='dia_branco' >";
                          
                        } else {
                            echo "<td ";
                            $d = $diacorrente + 1;
                            echo "id='dia_comum' class='col-$ano-$mes-$d' ";
                            echo ">";
                        }
                    }
                }
                
                if ($diacorrente + 1 <= $numero_dias) {
                    if ($coluna < $diasemana && $linha == 0) {
                       
                        echo "</td>";
                    } else {
                        
                        ++$diacorrente;
                        

                        //texto estranho estaria aqui
                        echo "<div onclick=\"pegaDia('$ano-$mes-$diacorrente')\" ng-click=\"pegaDia('$ano-$mes-$diacorrente')\" id=\"btlivre\">  $diacorrente  </div>";

                        // $diacorrente = str_pad($diacorrente, 2, "0", STR_PAD_LEFT);
                        // echo isset($arrayDesc[$diacorrente]) ? $arrayDesc[$diacorrente] : '';
                        // echo "</textarea>";

                        echo "</td>";
                    }
                } else {
                    break;
                }
            }
            echo "</tr>";
        }
        echo "<tbody>";
        echo "</table>";
    }

    public function MostreCalendarioCompleto() {
        echo "<table align = 'center'>";
        $cont = 1;
        for ($j = 0; $j < 4; $j++) {
            echo "<tr>";
            for ($i = 0; $i < 3; $i++) {
                echo "<td>";
                $this->MostreCalendario(($cont < 10) ? "0" . $cont : $cont);
                $cont++;
                echo "</td>";
            }
            echo "</tr>";
        }
        echo "</table>";
    }

    public function Data($Data) {
        $Data = explode('/', $Data);
        $Data = $Data[2] . '-' . $Data[1] . '-' . $Data[0];
        return $Data;
    }

    public function getCaledario(Request $request) {


        $request->mes = str_pad($request->mes, 2, "0", STR_PAD_LEFT);
        return $this->MostreCalendario($request->mes, $request->ano, $request->tipo, $request->client_id);
    }

    // FECHA CALENDARIO


    public function gravaCalendario() {
        //if(Request::ajax()) {
        $data = Input::all();
        $idprofessor = Auth::id();

        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }

        $data['desc_prescricao'] = $data['desc'];
        $data['data_prescricao'] = $this->setData($data['dataatual']);
        $data['tipo_prescricao'] = $data['tipo'];

        // alterar os campos idunidade, user_id_aluno e user_id_prof pra pegar dinamicamente
        $prescicaoAluno = PrescricaoCalendario::where('user_id_aluno', $data['client_id'])->where('tipo_prescricao', $data['tipo'])->where('data_prescricao', $data['data_prescricao'])->get();
        $contPrescricao = $prescicaoAluno->count();
        if ($contPrescricao > 0):
            $id = $prescicaoAluno[0]['id'];
            PrescricaoCalendario::where('id', $id)->update([
                'idunidade' => $idunidade,
                'user_id_aluno' => $data['client_id'],
                'user_id_prof' => $idprofessor,
                'desc_prescricao' => $data['desc_prescricao'],
                'data_prescricao' => $data['data_prescricao'],
                'tipo_prescricao' => $data['tipo_prescricao']
            ]);
            return 'Alterado com sucesso!';
        else:
            PrescricaoCalendario::create([
                'idunidade' => $idunidade,
                'user_id_aluno' => $data['client_id'],
                'user_id_prof' => $idprofessor,
                'desc_prescricao' => $data['desc_prescricao'],
                'data_prescricao' => $data['data_prescricao'],
                'tipo_prescricao' => $data['tipo_prescricao']
            ]);
            return 'Criado com sucesso!';
        endif;
        //}
    }

}
