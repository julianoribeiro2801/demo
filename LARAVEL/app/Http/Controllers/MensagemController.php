<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Mensagem;
use App\Models\Tipomensagem;
use App\Models\User;
use Hash;
use App\Http\Requests\MensagemRequest;
use App\Services\TrataDadosService;

// Precisa para funcionar o combo
use Auth;
use App\Services\FilialService;

class MensagemController extends Controller
{
    public function __construct(
       TrataDadosService $trataDadosService,
    FilialService $filialService
   ) {
        $this->trataDadosService = $trataDadosService;
        $this->filialService = $filialService;
    }
        
    public function index()
    {
        $headers = ['category' => 'Clientes', 'title' => 'Mensagens '];
        $unidades_combo = $this->filialService->unidadesComboTop();

        $mensagens = Mensagem::all();
        // dd($mensagens);
        return view('admin.mensagens.index', compact('mensagens', 'headers', 'unidades_combo'));
    }

    public function edit($id)
    {
        $headers = ['category' => 'Clientes', 'title' => 'Mensagens '];
        $unidades_combo = $this->filialService->unidadesComboTop();

        $mensagem = Mensagem::where('id', $id)->first();
       
        $alunos =User::select('id', 'name')->get();
        $selectedAlunos = $this->trataDadosService->listToSelectAlunos($alunos);

        $tipomensagens =  Tipomensagem::select('id', 'dstipomensagem')->get();
        $selectedTipomensagens = $this->trataDadosService->listToSelectTipomensagens($tipomensagens);
       
        return view('admin.mensagens.edit', compact('mensagem', 'selectedAlunos', 'selectedTipomensagens', 'headers', 'unidades_combo'));
    }

    public function create()
    {
        $headers = ['category' => 'Clientes', 'title' => 'Mensagens '];
        $unidades_combo = $this->filialService->unidadesComboTop();

        $alunos = User::select('id', 'name')->get();
        $tipomensagens = Tipomensagem::select('id', 'dstipomensagem')->get();
       
        $selectedAlunos = $this->trataDadosService->listToSelectAlunos($alunos);
        $selectedTipomensagens = $this->trataDadosService->listToSelectTipomensagens($tipomensagens);

    
        return view('admin.mensagens.create', compact('selectedAlunos', 'selectedTipomensagens', 'headers', 'unidades_combo'));
    }

    public function update(MensagemRequest $request, $id)
    {
        $data = $request->all();

        Mensagem::where('id', $id)->update(['dsmensagem' => $data['dsmensagem'],
                                          'idunidade' => 1,
                                          'idaluno' => $data['id']       ,
                                          'idtipomensagem' => $data['idtipomensagem'],
                                          'stlida' => 'N'
          
       ]);
     
        return redirect()->route('admin.clientes.mensagens.index');
    }

    public function store(MensagemRequest $request)
    {
        $data = $request->all();
       
        Mensagem::create(['dsmensagem' => $data['dsmensagem'],
                        'idunidade' => 1,
                        'idaluno' => $data['idaluno']       ,
                        'idtipomensagem' => $data['idtipomensagem'],
                        'stlida' => 'N'
          
              ]);
     
        return redirect()->route('admin.clientes.mensagens.index');
    }


    // public function create_user(){

          //  User::create( [
    //          'email' => 'kk@msn.com' ,
    //          'password' => Hash::make( '123456' ) ,
    //          'name' => 'KK Ruiz' ,
    //          'role' => 'admin'
    //         ] );
    // }
}
