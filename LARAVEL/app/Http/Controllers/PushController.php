<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Mensagem;
use App\Services\SendPushService;

use App\Models\PushType;


// Precisa para funcionar o combo
use Auth;
use App\Services\FilialService;

class PushController extends Controller
{
    private $sendPushService;

    public function __construct(FilialService $filialService, SendPushService $sendPushService)
    {
        $this->filialService = $filialService;
        $this->sendPushService = $sendPushService;

    }

    // aqui so um exemplo de como enviar o push
      public function exemploPushSend(){
       

        // em qualquer lugar so chamar isso e pasar esse dados para envia ro push
        $dados_push = [ 
	   	'sender_id' => Auth::user()->id,
	    	'receiver_id' => '76',
	    	'push_type' => '1'
	    ];

	  
       return $this->sendPushService->enviaPush($dados_push);

    
    }

     
    public function index()
    {
        $headers = ['category' => 'Push', 'title' => 'Push'];
        $unidades_combo = $this->filialService->unidadesComboTop();
        
        $push_types = PushType::where('status', '<','4')->get();
        $medalhas = PushType::where('status', '4')->get();


        // $this->exemploPushSend();

       return view('push.push', compact('headers', 'unidades_combo', 'push_types', 'medalhas'));
    }
}
