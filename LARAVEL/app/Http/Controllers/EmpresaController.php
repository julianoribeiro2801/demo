<?php

namespace App\Http\Controllers;

//////////////////////////
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Atividade;
use App\Models\AtividadeBlacklist;
use App\Models\Cidade;
use App\Models\Estado;
use App\Models\Funcionario;
use App\Models\Local;
use App\Models\Unidade;
use App\Models\UnidadeDados;
use App\Models\UserDados;
use App\Notifications\NewEmployerRegistred;
use App\Services\FilialService;
use App\Models\Gfmconfigura;
use Auth;
use DB;
use File;
use Image;
use Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Storage;

// use Request;
// Precisa para funcionar o combo

class EmpresaController extends Controller {

    public function __construct(FilialService $filialService) {
        $this->filialService = $filialService;
    }

    public function changeEmp($id_unidade) {
        $this->filialService->changeEmp($id_unidade);
        return redirect(url()->previous());
    }

    public function index() {
        $headers = ['category' => 'Empresa', 'title' => 'Unidades'];

        $unidades = Unidade::with(['parent', 'children', 'matriz'])->get();

        return view('empresas.index', compact('unidades', 'headers'));
    }

    public function getEstados() {
        $estados = Estado::select('id', 'nome', 'uf')->get();
        return $estados;
    }

    public function getCidades() {
        $data = Input::all();

        $cidades = Cidade::select('id', 'nome', 'estado')->where('estado', $data['estado'])->get();
        return $cidades;
    }

    public function getCidadesGet($uf) {


        $cidades = Cidade::select('id', 'nome', 'estado')->where('estado', $uf)->get();
        return $cidades;
    }

    public function getEmpresa() {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }

        $empresa = Unidade::select('unidade.logo', 'unidade.fantasia', 'unidade.razao_social', 'unidade_dados.*')
                        ->leftJoin('unidade_dados', 'unidade_dados.idunidade', '=', 'unidade.id')
                        ->where('unidade.id', $idunidade)
                        ->with(['dados', 'parent', 'children', 'matriz'])->get();
        return $empresa;
    }

    public function getConfiguraGfm() {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }

        $configura = Gfmconfigura::select('gfm_configura.*')
                        ->where('gfm_configura.idunidade', $idunidade)->get();
        return $configura;
    }

    public function remove_http($url) {
        $disallowed = array('http://', 'https://');
        foreach ($disallowed as $d) {
            if (strpos($url, $d) === 0) {
                return str_replace($d, '', $url);
            }
        }
        return $url;
    }

    public function upConfiguraGfm() {

        $data = Input::all();

        
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }

        $config = Gfmconfigura::select('id')->where('idunidade', $idunidade)->get();
        if (sizeof($config) <= 0):
            $dataConfigura['idunidade'] = $idunidade;
            $dataConfigura['diasantecedencia'] = isset($data['diasantecedencia']) ? $data['diasantecedencia'] : 6;//$data['aulasantecipadas'];;
            $dataConfigura['aulasantecipadas'] = isset($data['aulasantecipadas']) ? $data['aulasantecipadas'] : 1;//$data['aulasantecipadas'];
            $dataConfigura['intervalo'] = isset($data['intervalo']) ? $data['intervalo'] : 60;//$data['intervalo'];
            $dataConfigura['remove'] = isset($data['remove']) ? $data['remove'] : 'N';//$data['remove'];
            $dataConfigura['oferecevaga'] = isset($data['oferecevaga']) ? $data['oferecevaga'] : 'N';
            $dataConfigura['premium'] = isset($data['premium']) ? $data['premium'] : 'N';
            $dataConfigura['indice_inicial'] = isset($data['range'][0]) ? $data['range'][0] : '20'; //  $data['range'][0];
            $dataConfigura['indice_final'] = isset($data['range'][1]) ? $data['range'][1] : '40'; //$data['range'][1];
            $dataConfigura['user_update'] =$data['user_update'];
            
            
            if (Gfmconfigura::create($dataConfigura)):
                $retorno['title'] = 'Sucesso!';
                $retorno['type'] = 'success';
                $retorno['text'] = 'Filial cadastrada com sucesso!';
              //  return $retorno;
            else:
                $retorno['title'] = 'Erro!';
                $retorno['type'] = 'error';
                $retorno['text'] = 'Erro ao cadastrar filial!';
               // return $retorno;
            endif;

        else:
            $dataConfigura['idunidade'] = $idunidade;
            if ( isset($data['diasantecedencia'])):
                $dataConfigura['diasantecedencia'] = isset($data['diasantecedencia']) ? $data['diasantecedencia'] : 6;//$data['aulasantecipadas'];;
            endif;
            if ( isset($data['aulasantecipadas'])):
                $dataConfigura['aulasantecipadas'] = isset($data['aulasantecipadas']) ? $data['aulasantecipadas'] : 1;//$data['aulasantecipadas'];
            endif;
            if ( isset($data['intervalo'])):
                $dataConfigura['intervalo'] = isset($data['intervalo']) ? $data['intervalo'] : 60;//$data['intervalo'];
            endif;
            if ( isset($data['remove'])):
                $dataConfigura['remove'] = isset($data['remove']) ? $data['remove'] : 'N';//$data['remove'];
            endif;
            if ( isset($data['oferecevaga'])):
                $dataConfigura['oferecevaga'] = isset($data['oferecevaga']) ? $data['oferecevaga'] : 'N';
                
            endif;
            if ( isset($data['premium'])):
                $dataConfigura['premium'] = isset($data['premium']) ? $data['premium'] : 'N';
                
            endif;
            if ( isset($data['range'][0])):
                $dataConfigura['indice_inicial'] = isset($data['range'][0]) ? $data['range'][0] : '20'; //  $data['range'][0];
            endif;
            if ( isset($data['range'][1])):
                $dataConfigura['indice_final'] = isset($data['range'][1]) ? $data['range'][1] : '40'; //$data['range'][1];
                
            endif;
            
            $dataConfigura['user_update'] =$data['user_update'];
            $configura = new Gfmconfigura();
            if ($configura->where('id', '=', $config[0]->id)->update($dataConfigura)):
                $retorno['title'] = 'Sucesso!';
                $retorno['type'] = 'success';
                $retorno['text'] = 'Configurações atualizadas com sucesso!';
                return $retorno;
            else:
                $retorno['title'] = 'Erro!';
                $retorno['type'] = 'error';
                $retorno['text'] = 'Erro ao atualizar configurações!';
               return $retorno;
            endif;
        endif;



        
        
      //  return $data;
    }

     public function upPasso() {

         $idunidade = Auth::user()->idunidade;
         $id = Auth::user()->id;

        User::where('id', $id)->update(['passo' => '4']);
     }

    public function upEmpresa() {
        $data = Input::all();
        $validator = Validator::make(Input::all(), [
                    'fantasia' => 'required|min:3',
                    'razao_social' => 'required|min:3',
                    'idestado' => 'required|min:1',
                    'idcidade' => 'required|min:1'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors()->getMessages(), 400);
        } else {
            $idunidade = Auth::user()->idunidade;
            if (session()->get('id_unidade')) {
                $idunidade = session()->get('id_unidade');
            }

            $unidade = new Unidade();
            $dataUni = $unidade->select('logo')->where('id', $idunidade)->get();
            $imagemAtual = 'uploads/unidade/' . $dataUni[0]['logo'];

            if (Input::hasFile('logo')):
                File::delete($imagemAtual);
                $file = Input::file('logo');
                $image_name = time() . "-" . $file->getClientOriginalName();
                $file->move('uploads/unidade', $image_name);
                $image = Image::make(sprintf('uploads/unidade/%s', $image_name))->resize(260, null, function ($constraint) {
                            $constraint->aspectRatio();
                        })->save();
                $dataUnidade['logo'] = $image_name;
            endif;
            $dataUnidade['fantasia'] = $data['fantasia'];
            $dataUnidade['razao_social'] = $data['razao_social'];

            $dataUnidadeDados['cnpj'] = $data['cnpj'];
            $dataUnidadeDados['inscricao_estadual'] = $data['inscricao_estadual'];
            $dataUnidadeDados['endereco'] = $data['endereco'];
            $dataUnidadeDados['numero'] = $data['numero'];
            $dataUnidadeDados['cep'] = $data['cep'];
            $dataUnidadeDados['bairro'] = $data['bairro'];
            $dataUnidadeDados['idestado'] = $data['idestado'];
            $dataUnidadeDados['idcidade'] = $data['idcidade'];
            $dataUnidadeDados['telefone'] = $data['telefone'];
            $dataUnidadeDados['celular'] = $data['celular'];
            $dataUnidadeDados['email'] = $data['email'];

            $dataUnidadeDados['site'] = $data['site'];
            // remove e add o https mas com a preocupacao de nao duplicar 'http:// varias vezes
            $dataUnidadeDados['site'] = 'http://' . $this->remove_http($dataUnidadeDados['site']);

            $empresa = new Unidade();
            if ($empresa->where('id', '=', $idunidade)->update($dataUnidade)):
                $empresaDados = new UnidadeDados();
                if ($empresaDados->where('idunidade', '=', $idunidade)->update($dataUnidadeDados)):
                    $retorno['title'] = 'Sucesso!';
                    $retorno['type'] = 'success';
                    $retorno['text'] = 'Empresa atualizada com sucesso!';
                else:
                    $retorno['title'] = 'Erro!';
                    $retorno['type'] = 'error';
                    $retorno['text'] = 'Erro ao atualizar empresa!';
                endif;
            else:
                $retorno['title'] = 'Erro!';
                $retorno['type'] = 'error';
                $retorno['text'] = 'Erro ao atualizar empresa!';
            endif;
            return $retorno;
        }
    }

    // FILIAIS
    public function getFiliais() {
        $idunidade = Auth::user()->idunidade;

        $filiais = Unidade::select('id', 'fantasia', 'razao_social', 'situacao')->where('situacao', 1)->where('parent_id', $idunidade)->get();
        return $filiais;
    }

    public function getFilial() {
        $data = Input::all();

        $filial = Unidade::select('unidade.fantasia', 'unidade.razao_social', 'unidade_dados.*')
                ->leftJoin('unidade_dados', 'unidade_dados.idunidade', '=', 'unidade.id')
                ->where('unidade.id', $data['id'])
                ->get();
        return $filial;
    }

    public function addFilial() {
        // comentei somente para funcionar pq tinha dado erro na migracao do servidor testar habilitar de novo essa validacao do Request
        // if(Request::ajax()) {
        $data = Input::all();
        $validator = Validator::make(Input::all(), [
                    'fantasia' => 'required|min:3',
                    'razao_social' => 'required|min:3',
                    'idestado' => 'required|min:1',
                    'idcidade' => 'required|min:1'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors()->getMessages(), 400);
        } else {
            $idunidade = Auth::user()->idunidade;
            if (session()->get('id_unidade')) {
                $idunidade = session()->get('id_unidade');
            }

            $dataUnidade['user_id'] = $idunidade;
            $dataUnidade['parent_id'] = $idunidade;
            $dataUnidade['fantasia'] = $data['fantasia'];
            $dataUnidade['razao_social'] = $data['razao_social'];
            $dataUnidade['situacao'] = '0';

            $dataUnidadeDados['user_id'] = $idunidade;
            $dataUnidadeDados['cnpj'] = isset($data['cnpj']) ? $data['cnpj'] : '';
            $dataUnidadeDados['inscricao_estadual'] = isset($data['inscricao_estadual']) ? $data['inscricao_estadual'] : '';
            $dataUnidadeDados['endereco'] = isset($data['endereco']) ? $data['endereco'] : '';
            $dataUnidadeDados['numero'] = isset($data['numero']) ? $data['numero'] : '';
            $dataUnidadeDados['cep'] = isset($data['cep']) ? $data['cep'] : '';
            $dataUnidadeDados['bairro'] = isset($data['bairro']) ? $data['bairro'] : '';
            $dataUnidadeDados['idestado'] = isset($data['idestado']) ? $data['idestado'] : '';
            $dataUnidadeDados['idcidade'] = isset($data['idcidade']) ? $data['idcidade'] : '';
            $dataUnidadeDados['telefone'] = isset($data['telefone']) ? $data['telefone'] : '';
            $dataUnidadeDados['celular'] = isset($data['celular']) ? $data['celular'] : '';
            $dataUnidadeDados['email'] = isset($data['email']) ? $data['email'] : '';
            $dataUnidadeDados['site'] = isset($data['site']) ? $data['site'] : '';

            $filial = Unidade::create([
                        'user_id' => $idunidade,
                        'parent_id' => $idunidade,
                        'fantasia' => $data['fantasia'],
                        'razao_social' => $data['razao_social'],
                        'situacao' => '0'
            ]);
            $dataUnidadeDados['idunidade'] = $filial->id;

            $filialDados = new UnidadeDados();
            if ($filialDados->create($dataUnidadeDados)):
                $retorno['title'] = 'Sucesso!';
                $retorno['type'] = 'success';
                $retorno['text'] = 'Filial cadastrada com sucesso!';
                return $retorno;
            else:
                $retorno['title'] = 'Erro!';
                $retorno['type'] = 'error';
                $retorno['text'] = 'Erro ao cadastrar filial!';
                return $retorno;
            endif;
            // }
        }
    }

    public function upFilial() {
        // if(Request::ajax()) {
        // 'email' => 'required|unique:users',


        $data = Input::all();
        $validator = Validator::make(Input::all(), [
                    'fantasia' => 'required|min:3',
                    'razao_social' => 'required|min:3',
                    'idestado' => 'required|min:1',
                    'idcidade' => 'required|min:1'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->getMessages(), 400);
        } else {
            $idunidade = Auth::user()->idunidade;
            if (session()->get('id_unidade')) {
                $idunidade = session()->get('id_unidade');
            }

            $dataUnidade['fantasia'] = $data['fantasia'];
            $dataUnidade['razao_social'] = $data['razao_social'];

            $dataUnidadeDados['cnpj'] = $data['cnpj'];
            $dataUnidadeDados['inscricao_estadual'] = $data['inscricao_estadual'];
            $dataUnidadeDados['endereco'] = $data['endereco'];
            $dataUnidadeDados['numero'] = $data['numero'];
            $dataUnidadeDados['cep'] = $data['cep'];
            $dataUnidadeDados['bairro'] = $data['bairro'];
            $dataUnidadeDados['idestado'] = $data['idestado'];
            $dataUnidadeDados['idcidade'] = $data['idcidade'];
            $dataUnidadeDados['telefone'] = $data['telefone'];
            $dataUnidadeDados['celular'] = $data['celular'];
            $dataUnidadeDados['email'] = $data['email'];
            $dataUnidadeDados['site'] = $data['site'];

            $empresa = new Unidade();
            if ($empresa->where('id', '=', $data['id'])->update($dataUnidade)):
                $empresaDados = new UnidadeDados();
                if ($empresaDados->where('idunidade', '=', $data['id'])->update($dataUnidadeDados)):
                    $retorno['title'] = 'Sucesso!';
                    $retorno['type'] = 'success';
                    $retorno['text'] = 'Filial atualizada com sucesso!';
                else:
                    $retorno['title'] = 'Erro!';
                    $retorno['type'] = 'error';
                    $retorno['text'] = 'Erro ao atualizar filial!';
                endif;
            else:
                $retorno['title'] = 'Erro!';
                $retorno['type'] = 'error';
                $retorno['text'] = 'Erro ao atualizar filial!';
            endif;

            return $retorno;
        }
        // }
    }

    // DELETAR FILIAL
    public function delFilial($id) {
        // Local::where('id', $id)->delete();
        $dataFilial['situacao'] = 0;
        $local = new Unidade();
        $local->where('id', '=', $id)->update($dataFilial);
    }

    // FECHA FILIAIS
    // FILIAIS
    public function getLocais() {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }

        $locais = Local::select('id', 'nmlocal', 'metros', 'situacao')->where('situacao', 'A')->where('idunidade', $idunidade)->get();
        return $locais;
    }

    public function getLocal() {
        $data = Input::all();

        $locais = Local::select('id', 'nmlocal', 'metros', 'situacao')->where('id', $data['id'])->get();
        return $locais;
    }

    public function addLocal() {
        // if(Request::ajax()) {
        $data = Input::all();
        $validator = Validator::make(Input::all(), [
                    'nmlocal' => 'required|min:3',
                    'metros' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors()->getMessages(), 400);
        } else {
            $idunidade = Auth::user()->idunidade;
            if (session()->get('id_unidade')) {
                $idunidade = session()->get('id_unidade');
            }

            $dataLocal['idunidade'] = $idunidade;
            $dataLocal['nmlocal'] = $data['nmlocal'];
            $dataLocal['metros'] = isset($data['metros']) ? $data['metros'] : ''; //$data['metros'];
            $dataLocal['situacao'] = 'A';

            $local = new Local();
            if ($local->create($dataLocal)):
                $retorno['title'] = 'Sucesso!';
                $retorno['type'] = 'success';
                $retorno['text'] = 'Local cadastrado com sucesso!';
                return $retorno;
            else:
                $retorno['title'] = 'Erro!';
                $retorno['type'] = 'error';
                $retorno['text'] = 'Erro ao cadastrar local!';
                return $retorno;
            endif;
        }
    }

    public function upLocal() {
        // if(Request::ajax()) {
        $data = Input::all();
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }

        $dataLocal['nmlocal'] = $data['nmlocal'];
        $dataLocal['metros'] = $data['metros'];

        $local = new Local();
        if ($local->where('id', '=', $data['id'])->update($dataLocal)):
            $retorno['title'] = 'Sucesso!';
            $retorno['type'] = 'success';
            $retorno['text'] = 'Local atualizado com sucesso!';
        else:
            $retorno['title'] = 'Erro!';
            $retorno['type'] = 'error';
            $retorno['text'] = 'Erro ao atualizar local!';
        endif;
        return $retorno;
        // }
    }

    // DELETAR FILIAL
    public function delLocal($id) {
        // Local::where('id', $id)->delete();
        $dataLocal['situacao'] = 'I';
        $local = new Local();
        $local->where('id', '=', $id)->update($dataLocal);
    }

    // FECHA FILIAIS
    // FUNCIONARIOS
    public function setData($Data) {
        if ($Data != null):
            $Format = explode(' ', $Data);
            $Data = explode('/', $Format[0]);
            $Data = $Data[2] . '-' . $Data[1] . '-' . $Data[0];
            return $Data;
        else:
            return '';
        endif;
    }

    public function getData($Data) {
        if ($Data != null):
            $data = date("d/m/Y", strtotime($Data));
            return $data;
        endif;
    }

    // NUTRICIONISTAS
    public function getNutricionistas() {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }

        $nutricionistas = Funcionario::select('users.id', 'users.name', 'user_dados.situacao','users.tipo')
                ->where('users.idunidade', $idunidade)
                ->wherein('users.role', ['coach','nutricionista'])
                ->where('user_dados.situacao', 'A')
                ->leftJoin('user_dados', 'user_dados.user_id', '=', 'users.id')
                ->get();
        foreach ($nutricionistas as $nutricionista) {
            $nutricionista->name = ucwords(strtolower($nutricionista->name));
        }
        return $nutricionistas;
    }

    public function getNutricionista() {
        $data = Input::all();

        $nutricionista = Funcionario::select('users.name', 'users.email', 'users.role', 'user_dados.*','users.tipo')
                ->leftJoin('user_dados', 'user_dados.user_id', '=', 'users.id')
                ->where('users.id', $data['id'])
                ->get();
        $nutriocionistaData = [
            'name' => $nutricionista[0]['name'],
            'email' => $nutricionista[0]['email'],
            'role' => $nutricionista[0]['role'],
            'user_id' => $nutricionista[0]['user_id'],
            'telefone' => $nutricionista[0]['telefone'],
            'dt_nascimento' => ($nutricionista[0]['dt_nascimento'] != '0000-00-00') ? $this->getData($nutricionista[0]['dt_nascimento']) : '',
            'cpf' => $nutricionista[0]['cpf'],
            'origem' => $nutricionista[0]['origem'],
            'endereco' => $nutricionista[0]['endereco'],
            'numero' => $nutricionista[0]['numero'],
            'cep' => $nutricionista[0]['cep'],
            'site' => $nutricionista[0]['site'],
            'bairro' => $nutricionista[0]['bairro'],
            'idestado' => $nutricionista[0]['idestado'],
            'idcidade' => $nutricionista[0]['idcidade'],
            'tipo' => $nutricionista[0]['tipo']
        ];
        return $nutriocionistaData;
    }

    public function addNutricionista() {
        // if(Request::ajax()) {
        $data = Input::all();

        $validator = Validator::make(Input::all(), [
                    'name' => 'required|min:3',
                    'email' => 'required|min:3',
                        /* 'telefone' => 'required|min:3',
                          'dt_nascimento' => 'required|min:3',
                          'cpf' => 'required|min:3',
                          'endereco' => 'required|min:3',
                          'numero' => 'required|min:1',
                          'cep' => 'required|min:3',
                          'bairro' => 'required|min:3',
                          'idestado' => 'required',
                          'idcidade' => 'required' */
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors()->getMessages(), 400);
        } else {
            $idunidade = Auth::user()->idunidade;
            if (session()->get('id_unidade')) {
                $idunidade = session()->get('id_unidade');
            }

            $dataUser['idunidade'] = $idunidade;
            $dataUser['name'] = $data['name'];
            $dataUser['email'] = $data['email'];

            if (isset($data['password']) && $data['password'] != '') {
                $dataUser['password'] = bcrypt($data['password']);
            } else {
                $senha = str_random(9);
                $dataUser['password'] = bcrypt($senha);
            }
            $email = Funcionario::select('id', 'email')->where('email', $data['email'])->where('idunidade', $idunidade)->get();

            if (sizeof($email) <= 0) {
                $dataUser['role'] = 'funcionario';

                $dtNasc = $this->setData(isset($data['dt_nascimento']) ? $data['dt_nascimento'] : '');
                $dataNutricionistaDados['idunidade'] = $idunidade;
                $dataNutricionistaDados['telefone'] = isset($data['telefone']) ? $data['telefone'] : '';
                $dataNutricionistaDados['dt_nascimento'] = isset($dtNasc) ? $dtNasc : '';
                $dataNutricionistaDados['cpf'] = isset($data['cpf']) ? $data['cpf'] : '';
                $dataNutricionistaDados['site'] = isset($data['site']) ? $data['site'] : '';
                $dataNutricionistaDados['endereco'] = isset($data['endereco']) ? $data['endereco'] : '';
                $dataNutricionistaDados['numero'] = isset($data['numero']) ? $data['numero'] : '';
                $dataNutricionistaDados['cep'] = isset($data['cep']) ? $data['cep'] : '';
                $dataNutricionistaDados['bairro'] = isset($data['bairro']) ? $data['bairro'] : '';
                $dataNutricionistaDados['idestado'] = isset($data['idestado']) ? $data['idestado'] : '';
                $dataNutricionistaDados['idcidade'] = isset($data['idcidade']) ? $data['idcidade'] : '';
                $dataNutricionistaDados['tipo'] = isset($data['tipo']) ? $data['tipo'] : '';

                $dataNutricionistaDados['situacao'] = 'A';

                $nutricionista = Funcionario::create([
                            'idunidade' => $idunidade,
                            'name' => $data['name'],
                            'tipo' => $data['tipo'],
                            'email' => $data['email'],
                            'password' => $dataUser['password'],
                            'role' => 'coach'
                ]);
                
                $dataNutricionistaDados['user_id'] = $nutricionista->id;

                $funcionarioDados = new UserDados();
                $nutricionista->notify(new NewEmployerRegistred($dataUser['password']));

                if ($funcionarioDados->create($dataNutricionistaDados)):
                    $retorno['title'] = 'Sucesso!';
                    $retorno['type'] = 'success';
                    $retorno['text'] = 'Nutricionista cadastrado com sucesso!';
                    return $retorno;
                else:
                    $retorno['title'] = 'Erro!';
                    $retorno['type'] = 'error';
                    $retorno['text'] = 'Erro ao cadastrar nutricionista!';
                    return $retorno;
                endif;
            } else {
                $retorno['title'] = 'Erro!';
                $retorno['type'] = 'error';
                $retorno['text'] = 'E-mail já cadastrado!';
                return $retorno;
            }
        }
    }

    public function upNutricionista() {
        // if(Request::ajax()) {
        $data = Input::all();

        $validator = Validator::make(Input::all(), [
                    'name' => 'required|min:3',
                    'email' => 'required|min:3',
                        //'telefone' => 'required|min:3',
                        //'dt_nascimento' => 'required|min:3',
                        //'cpf' => 'required|min:3',
                        //'endereco' => 'required|min:3',
                        //'numero' => 'required|min:1',
                        //'cep' => 'required|min:3',
                        //'bairro' => 'required|min:3',
                        //'idestado' => 'required',
                        //'idcidade' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors()->getMessages(), 400);
        } else {
            $data = Input::all();
            $idunidade = Auth::user()->idunidade;
            if (session()->get('id_unidade')) {
                $idunidade = session()->get('id_unidade');
            }

            $dataUser['name'] = $data['name'];
            $dataUser['email'] = $data['email'];
            $dataUser['tipo'] = isset($data['tipo']) ? $data['tipo'] : '';
            if (isset($data['password']) && $data['password'] != '') {
                $dataUser['password'] = bcrypt($data['password']);
            }

            $dtNasc = $this->setData($data['dt_nascimento']);
            $dataNutricionistaDados['idunidade'] = $idunidade;
            $dataNutricionistaDados['telefone'] = isset($data['telefone']) ? $data['telefone'] : '';
            $dataNutricionistaDados['dt_nascimento'] = isset($dtNasc) ? $dtNasc : ''; //$this->setData($data['dt_nascimento']);
            $dataNutricionistaDados['cpf'] = isset($data['cpf']) ? $data['cpf'] : '';
            $dataNutricionistaDados['site'] = isset($data['site']) ? $data['site'] : '';
            $dataNutricionistaDados['endereco'] = isset($data['endereco']) ? $data['endereco'] : '';
            $dataNutricionistaDados['numero'] = isset($data['numero']) ? $data['numero'] : '';
            $dataNutricionistaDados['cep'] = isset($data['cep']) ? $data['cep'] : '';
            $dataNutricionistaDados['bairro'] = isset($data['bairro']) ? $data['bairro'] : '';
            $dataNutricionistaDados['idestado'] = isset($data['idestado']) ? $data['idestado'] : '';
            $dataNutricionistaDados['idcidade'] = isset($data['idcidade']) ? $data['idcidade'] : '';
            
            $email = Funcionario::select('email')->where('email', $data['email'])->where('idunidade', $idunidade)->where('id', '<>', $data['user_id'])->get();
            if (sizeof($email) <= 0) {
                $nutricionista = new Funcionario();
                if ($nutricionista->where('id', '=', $data['user_id'])->update($dataUser)):
                    $userDados = new UserDados();
                    if ($userDados->where('user_id', '=', $data['user_id'])->update($dataNutricionistaDados)):
                        $retorno['title'] = 'Sucesso!';
                        $retorno['type'] = 'success';
                        $retorno['text'] = 'Nutricionista atualizado com sucesso!';
                    else:
                        $retorno['title'] = 'Erro!';
                        $retorno['type'] = 'error';
                        $retorno['text'] = 'Erro ao atualizar nutricionista!';
                    endif;
                else:
                    $retorno['title'] = 'Erro!';
                    $retorno['type'] = 'error';
                    $retorno['text'] = 'Erro ao atualizar nutricionista!';
                endif;
            } else {
                $retorno['title'] = 'Erro!';
                $retorno['type'] = 'error';
                $retorno['text'] = 'E-mail já cadastrado!';
                return $retorno;
            }

            return $retorno;
        }
    }

    // DELETAR NUTRICIONISTAS
    public function delNutricionista($id) {
        // Funcionario::where('id', $id)->delete();
        // UserDados::where('user_id', $id)->delete();
        $dataNutricionistaDados['situacao'] = 'I';
        $userDados = new UserDados();
        $userDados->where('user_id', '=', $id)->update($dataNutricionistaDados);
    }

    // FECHA NUTRICIONISTAS
    // FUNCIONARIOS
    public function getFuncionarios() {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        $funcionarios = Funcionario::select('users.idunidade', 'users.id', 'users.name', 'users.avatar')
                ->leftJoin('user_dados', 'user_dados.user_id', '=', 'users.id')
                ->where('users.idunidade', $idunidade)
                ->where('users.role', '<>', 'cliente')
                ->where('users.role', '<>', 'prospect')
                ->where('users.role', '<>', 'coach')
                ->where('user_dados.situacao', 'A')
                ->where('users.excluido', 'N')
                ->get();

        foreach ($funcionarios as $funcionario) {
            $funcionario->avatar = $this->verificarArquivo($funcionario->avatar);

            $funcionario->name = ucwords(strtolower($funcionario->name));
        }
        return $funcionarios;
    }

    function verificarArquivo($name_img) {


        $diskimagem = Storage::disk('avatars');

        // SE A FOTO EXISTIR COLOCO
        if ($diskimagem->exists($name_img)) {
            return $name_img;
        } else {
            return "user-a4.jpg";
        }
    }

    public function getFuncionario() {
        $data = Input::all();

        $funcionario = Funcionario::select('users.name', 'users.avatar', 'users.email', 'users.role', 'user_dados.*')
                ->leftJoin('user_dados', 'user_dados.user_id', '=', 'users.id')
                ->where('users.id', $data['id'])
                ->get();



        if (file_exists('uploads/avatars/user-' . $funcionario[0]['user_id'] . '.jpg')) {
            $avatar = $funcionario[0]['avatar'];
        } else {
            $avatar = 'user-a4.jpg';
        }


        $funcionarioData = [
            'avatar' => $avatar,
            'name' => $funcionario[0]['name'],
            'email' => $funcionario[0]['email'],
            'role' => $funcionario[0]['role'],
            'user_id' => $funcionario[0]['user_id'],
            'telefone' => $funcionario[0]['telefone'],
            'dt_nascimento' => ($funcionario[0]['dt_nascimento'] != '0000-00-00') ? $this->getData($funcionario[0]['dt_nascimento']) : '',
            'cpf' => $funcionario[0]['cpf'],
            'origem' => $funcionario[0]['origem'],
            'endereco' => $funcionario[0]['endereco'],
            'numero' => $funcionario[0]['numero'],
            'genero' => $funcionario[0]['genero'],
            'cep' => $funcionario[0]['cep'],
            'bairro' => $funcionario[0]['bairro'],
            'idestado' => $funcionario[0]['idestado'],
            'idcidade' => $funcionario[0]['idcidade']
        ];
        return $funcionarioData;
    }

    public function addFuncionario(Request $request) {
        // if(Request::ajax()) {
        $data = Input::all();

        $validator = Validator::make(Input::all(), [
                    'name' => 'required|min:3',
                    'email' => 'required|min:3',
                    //'telefone' => 'required|min:3',
                    //'dt_nascimento' => 'required|min:3',
                    //'cpf' => 'required|min:3',
                    'genero' => 'required|min:1',
                    'role' => 'required|min:3'
            
                        //'password' => 'required|min:6',
                        //'endereco' => 'required|min:3',
                        //'numero' => 'required|min:1',
                        //'cep' => 'required|min:3',
                        //'bairro' => 'required|min:3',
                        //'idestado' => 'required',
                        //'idcidade' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors()->getMessages(), 400);
        } else {
            $idunidade = Auth::user()->idunidade;
            if (session()->get('id_unidade')) {
                $idunidade = session()->get('id_unidade');
            }

            $dataUser['idunidade'] = $idunidade;
            $dataUser['name'] = $data['name'];
            $dataUser['email'] = $data['email'];

            if (isset($data['password']) && $data['password'] != '') {
                $dataUser['password'] = bcrypt($data['password']);
            } else {
                $senha = str_random(9);
                $dataUser['password'] = bcrypt($senha);
            }

            $emailX = Funcionario::select('users.id', 'users.role','users.email', 'user_dados.situacao')
                            ->join('user_dados', 'users.id', '=', 'user_dados.user_id')
                            ->where('email', $data['email'])
                            ->where('user_dados.idunidade', $idunidade)->get();
            if (sizeof($emailX) <= 0) {
                $dataUser['role'] = 'funcionario';

                $dtNasc = $this->setData(isset($data['dt_nascimento']) ? $data['dt_nascimento'] : '');
                $dataFuncionarioDados['idunidade'] = $idunidade;
                $dataFuncionarioDados['telefone'] = isset($data['telefone']) ? $data['telefone'] : '';
                $dataFuncionarioDados['dt_nascimento'] = isset($dtNasc) ? $dtNasc : '';
                $dataFuncionarioDados['cpf'] = isset($data['cpf']) ? $data['cpf'] : '';
                $dataFuncionarioDados['origem'] = $data['role'];
                $dataFuncionarioDados['genero'] = $data['genero'];
                $dataFuncionarioDados['endereco'] = isset($data['endereco']) ? $data['endereco'] : '';
                $dataFuncionarioDados['numero'] = isset($data['numero']) ? $data['numero'] : '';
                $dataFuncionarioDados['cep'] = isset($data['cep']) ? $data['cep'] : '';
                $dataFuncionarioDados['bairro'] = isset($data['bairro']) ? $data['bairro'] : '';
                $dataFuncionarioDados['idestado'] = isset($data['idestado']) ? $data['idestado'] : '';
                $dataFuncionarioDados['idcidade'] = isset($data['idcidade']) ? $data['idcidade'] : '';
                $dataFuncionarioDados['situacao'] = 'A';

                $funcionario = Funcionario::create([
                            'idunidade' => $idunidade,
                            'name' => $data['name'],
                            'email' => $data['email'],
                            'password' => $dataUser['password'],
                            //'role' => 'funcionario'
                            'role' => $data['role']
                ]);
                $dataFuncionarioDados['user_id'] = $funcionario->id;

                $funcionarioDados = new UserDados();
                $funcionario->notify(new NewEmployerRegistred($dataUser['password']));

                if ($funcionarioDados->create($dataFuncionarioDados)):
                    
                    $retorno['cod'] = '0';
                    $retorno['title'] = 'Sucesso!';
                    $retorno['type'] = 'success';
                    $retorno['text'] = 'Funcionário cadastrado com sucesso!';
                //   return $retorno;
                else:
                    $retorno['cod'] = '0';
                    $retorno['title'] = 'Erro!';
                    $retorno['type'] = 'error';
                    $retorno['text'] = 'Erro ao cadastrar funcionário!';
                //   return $retorno;
                endif;
            } else {

                if ($emailX[0]->situacao == 'I') :
                    $retorno['cod'] = '-1';
                else:
                    $retorno['cod'] = '0';
                endif;
                $retorno['id'] = $emailX[0]->id;
                $retorno['title'] = 'Erro!';
                $retorno['type'] = 'error';
                $retorno['text'] = "E-mail já cadastrado como " .  $emailX[0]->role ."!";
                // return $retorno;
            }
            return $retorno;
        }
    }

    public function upFuncionario(Request $request) {
        // if($request->ajax()) {
        $data = $request->all();
        $validator = Validator::make(Input::all(), [
                    'name' => 'required|min:3',
                    'email' => 'required|min:3',
                    //'telefone' => 'required|min:3',
                    //'dt_nascimento' => 'required|min:3',
                    //'cpf' => 'required',
                    'genero' => 'required|min:1',
                    //'role' => 'required',
                    //'endereco' => 'required|min:3',
                    //'numero' => 'required|min:1',
                    //'cep' => 'required|min:3',
                    //'bairro' => 'required|min:3',
                    //'idestado' => 'required',
                    //'idcidade' => 'required',
                    Rule::unique('users')->ignore($data['user_id'])
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors()->getMessages(), 400);
        } else {
            $idunidade = Auth::user()->idunidade;
            if (session()->get('id_unidade')) {
                $idunidade = session()->get('id_unidade');
            }

            $dataUser['name'] = $data['name'];
            $dataUser['role'] = $data['role'];
            $dataUser['email'] = $data['email'];
            if (isset($data['password']) && $data['password'] != '') {
                $dataUser['password'] = bcrypt($data['password']);
            }

            $dtNasc = $this->setData(isset($data['dt_nascimento']) ? $data['dt_nascimento'] : '');
            $dataFuncionarioDados['idunidade'] = $idunidade;
            $dataFuncionarioDados['telefone'] = isset($data['telefone']) ? $data['telefone'] : '';
            $dataFuncionarioDados['dt_nascimento'] = isset($dtNasc) ? $dtNasc : ''; //$this->setData($data['dt_nascimento']);
            $dataFuncionarioDados['cpf'] = isset($data['cpf']) ? $data['cpf'] : '';
            $dataFuncionarioDados['endereco'] = isset($data['endereco']) ? $data['endereco'] : '';
            $dataFuncionarioDados['numero'] = isset($data['numero']) ? $data['numero'] : '';
            $dataFuncionarioDados['genero'] = $data['genero'];
            $dataFuncionarioDados['cep'] = isset($data['cep']) ? $data['cep'] : '';
            $dataFuncionarioDados['bairro'] = isset($data['bairro']) ? $data['bairro'] : '';
            $dataFuncionarioDados['idestado'] = isset($data['idestado']) ? $data['idestado'] : '';
            $dataFuncionarioDados['idcidade'] = isset($data['idcidade']) ? $data['idcidade'] : '';
            $dataFuncionarioDados['situacao'] = 'A';

            $email = Funcionario::select('email','role')->where('email', $data['email'])->where('idunidade', $idunidade)->where('id', '<>', $data['user_id'])->get();
            if (sizeof($email) <= 0) {
                $funcionario = new Funcionario();
                if ($funcionario->where('id', '=', $data['user_id'])->update($dataUser)):
                    $empresaDados = new UserDados();
                    if ($empresaDados->where('user_id', '=', $data['user_id'])->update($dataFuncionarioDados)):
                        $retorno['title'] = 'Sucesso!';
                        $retorno['type'] = 'success';
                        $retorno['text'] = 'Funcionário atualizado com sucesso!';
                    else:
                        $retorno['title'] = 'Erro!';
                        $retorno['type'] = 'error';
                        $retorno['text'] = 'Erro ao atualizar funcionário!';
                    endif;
                else:
                    $retorno['title'] = 'Erro!';
                    $retorno['type'] = 'error';
                    $retorno['text'] = 'Erro ao atualizar funcionário!';
                endif;
            } else {
                $retorno['title'] = 'Erro!';
                $retorno['type'] = 'error';
                $retorno['text'] = "E-mail já cadastrado como " .   $email[0]->role . "!";
                return $retorno;
            }
            return $retorno;
        }
    }

    // DELETAR FUNCIONARIOS
    public function delFuncionario($id, $situacao) {
        // Funcionario::where('id', $id)->delete();
        // UserDados::where('user_id', $id)->delete();
        $dataFuncionarioDados['situacao'] = $situacao;
        $empresaDados = new UserDados();
        $empresaDados->where('user_id', '=', $id)->update($dataFuncionarioDados);
    }

    // FECHA FUNCIONARIOS
    // MODALIDADES
    public function getModalidades() {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }

        $modalidades = Atividade::select('id', 'nmatividade', 'stmenuapp')->get();
        return $modalidades;
    }

    public function getModalidadesBlackList() {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }

        $blacklist = AtividadeBlacklist::select('atividade_id')->where('unidade_id', $idunidade)->get();
        return $blacklist;
    }

    public function upModalidadesBlackList() {
        $data = Input::all();
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }

        $black = AtividadeBlacklist::where('atividade_id', $data['id'])->where('unidade_id', $idunidade)->get();
        $contBlack = $black->count();
        if ($contBlack > 0):
            AtividadeBlacklist::where('atividade_id', $data['id'])->where('unidade_id', $idunidade)->delete();
            $retorno['title'] = 'Sucesso!';
            $retorno['type'] = 'success';
            $retorno['text'] = 'Modalidade ativada com sucesso!';
        else:
            AtividadeBlacklist::create([
                'atividade_id' => $data['id'],
                'unidade_id' => $idunidade
            ]);
            $retorno['title'] = 'Sucesso!';
            $retorno['type'] = 'success';
            $retorno['text'] = 'Modalidade desativada com sucesso!';
        endif;
        return $retorno;
    }

    // FECHA MODALIDADES
}
