<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Unidade;
use App\Models\Programa;
use App\Models\Objetivotreino;
use App\Models\Nivelhabilidade;

use App\Models\Local;
use App\Models\Moduloacademia;
use App\Models\Programatreinamento;
use App\Models\Agendaprogramatreinamento;
use App\Models\UnidadeDados;
use App\Models\User;
use App\Models\UserDados;
use App\Models\PrescricaoCalendario;
use App\Models\TreinoMusculacao;
use App\Models\TreinoMusculacaopadrao;
use App\Models\TreinoFicha;
use App\Models\TreinoFichapadrao;
use App\Models\TempoEstimado;
use App\Models\TempoEstimadopadrao;
use App\Models\PrescricaoProjeto;
use App\Models\PrescricaoEtapa;
use App\Models\PsaAtividade;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\API\Services\ApiClientService;
use App\Services\ApiEgoiService;
use DB;

class AdminMasterController extends Controller {

    private $service;
    private $egoi;

    public function __construct(ApiClientService $service) {
        $this->service = $service;
    }

    public function index() {
        $headers = ['category' => 'Admin Master', 'title' => 'Admin Master'];

        $unidades = Unidade::select('unidade.*', 'users.name', 'users.email')
                ->JOIN('users', 'users.id', '=', 'unidade.user_id')
                ->where('idmatriz', '0')
                ->where('unidade.parent_id', '0')
                ->orderby('unidade.parent_id')
                ->get();

        foreach ($unidades as $key => $value) {
            $unidades[$key]['filiais'] = Unidade::select('unidade.*')
                    ->where('idmatriz', $unidades[$key]['id'])
                    ->get();
        }


        return view('adminmaster.index', compact('headers', 'unidades'));
    }

    public function getEstados() {
        $estados = Estado::select('id', 'nome', 'uf')->get();
        return $estados;
    }

    public function getCidades() {
        $data = Input::all();

        $cidades = Cidade::select('id', 'nome', 'estado')->where('estado', $data['estado'])->get();
        return $cidades;
    }

    public function getMatriz($id) {

        $unidade = Unidade::select('users.name as name', 'unidade.*', 'unidade_dados.*')
                        ->leftJoin('unidade_dados', 'unidade.id', '=', 'unidade_dados.idunidade')
                        ->leftJoin('users', 'users.id', '=', 'unidade.user_id')
                         ->where('unidade.situacao','>=', 0)
                        ->where('unidade.id', $id)->get();
        return $unidade;
    }

    public function addMatriz() {
        // comentei somente para funcionar pq tinha dado erro na migracao do servidor testar habilitar de novo essa validacao do Request
        // if(Request::ajax()) {
        $data = Input::all();
        $validator = Validator::make(Input::all(), [
                    'fantasia' => 'required|min:3',
                    'razao_social' => 'required|min:3',
                    'idestado' => 'required|min:1',
                    'idcidade' => 'required|min:1'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors()->getMessages(), 400);
        } else {
            $idunidade = Auth::user()->idunidade;
            if (session()->get('id_unidade')) {
                $idunidade = session()->get('id_unidade');
            }




            $adm = User::create([
                        'email' => $data['email'],
                        'password' => Hash::make('123456'),
                        'name' => 'teste',
                        'role' => 'admin'
            ]);


            $dataUnidade['user_id'] = $adm->id;
            $dataUnidade['parent_id'] = $data['parent_id'];
            $dataUnidade['fantasia'] = $data['fantasia'];
            $dataUnidade['razao_social'] = $data['razao_social'];
            $dataUnidade['situacao'] = '0';

            $dataUnidadeDados['user_id'] = $adm->id;
            $dataUnidadeDados['cnpj'] = isset($data['cnpj']) ? $data['cnpj'] : '';
            $dataUnidadeDados['inscricao_estadual'] = isset($data['inscricao_estadual']) ? $data['inscricao_estadual'] : '';
            $dataUnidadeDados['endereco'] = isset($data['endereco']) ? $data['endereco'] : '';
            $dataUnidadeDados['numero'] = isset($data['numero']) ? $data['numero'] : '';
            $dataUnidadeDados['cep'] = isset($data['cep']) ? $data['cep'] : '';
            $dataUnidadeDados['bairro'] = isset($data['bairro']) ? $data['bairro'] : '';
            $dataUnidadeDados['idestado'] = isset($data['idestado']) ? $data['idestado'] : '';
            $dataUnidadeDados['idcidade'] = isset($data['idcidade']) ? $data['idcidade'] : '';
            $dataUnidadeDados['telefone'] = isset($data['telefone']) ? $data['telefone'] : '';
            $dataUnidadeDados['celular'] = isset($data['celular']) ? $data['celular'] : '';
            $dataUnidadeDados['email'] = isset($data['email']) ? $data['email'] : '';
            $dataUnidadeDados['site'] = isset($data['site']) ? $data['site'] : '';

            $filial = Unidade::create([
                        'user_id' => $dataUnidade['user_id'],
                        'parent_id' => $data['parent_id'],
                        'idmatriz' => 0,
                        'fantasia' => $data['fantasia'],
                        'razao_social' => $data['razao_social'],
                        'situacao' => '0'
            ]);
            $dataUnidadeDados['idunidade'] = $filial->id;

            $filialDados = new UnidadeDados();
            if ($filialDados->create($dataUnidadeDados)):
                $retorno['title'] = 'Sucesso!';
                $retorno['type'] = 'success';
                $retorno['text'] = 'Filial cadastrada com sucesso!';
            //return $retorno;
            else:
                $retorno['title'] = 'Erro!';
                $retorno['type'] = 'error';
                $retorno['text'] = 'Erro ao cadastrar filial!';
            //    return $retorno;
            endif;

            echo $filial->id . " -  " . $adm->id;

            $upUser = new User();
            if ($upUser->where('id', '=', $adm->id)->update(['idunidade' => $filial->id])):
                $retorno['title'] = 'Sucesso!';
                $retorno['type'] = 'success';
                $retorno['text'] = 'Filial ssssatualizada com sucesso!';
            else:
                $retorno['title'] = 'Erro!';
                $retorno['type'] = 'error';
                $retorno['text'] = 'Erro ao atualizar filial!';
            endif;
        }
    }

    public function upMatriz() {
        // comentei somente para funcionar pq tinha dado erro na migracao do servidor testar habilitar de novo essa validacao do Request
        // if(Request::ajax()) {
        $data = Input::all();
        $validator = Validator::make(Input::all(), [
                    'fantasia' => 'required|min:3',
                    'razao_social' => 'required|min:3',
                    'idestado' => 'required|min:1',
                    'idcidade' => 'required|min:1'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors()->getMessages(), 400);
        } else {
            $idunidade = Auth::user()->idunidade;
            if (session()->get('id_unidade')) {
                $idunidade = session()->get('id_unidade');
            }

            //$dataUnidade['user_id'] = 1;
            // $dataUnidade['parent_id'] = 0;
            $dataUnidade['fantasia'] = $data['fantasia'];
            $dataUnidade['razao_social'] = $data['razao_social'];
            $dataUnidade['situacao'] = '0';

            //$dataUnidadeDados['user_id'] = 1;
            $dataUnidadeDados['cnpj'] = isset($data['cnpj']) ? $data['cnpj'] : '';
            $dataUnidadeDados['inscricao_estadual'] = isset($data['inscricao_estadual']) ? $data['inscricao_estadual'] : '';
            $dataUnidadeDados['endereco'] = isset($data['endereco']) ? $data['endereco'] : '';
            $dataUnidadeDados['numero'] = isset($data['numero']) ? $data['numero'] : '';
            $dataUnidadeDados['cep'] = isset($data['cep']) ? $data['cep'] : '';
            $dataUnidadeDados['bairro'] = isset($data['bairro']) ? $data['bairro'] : '';
            $dataUnidadeDados['idestado'] = isset($data['idestado']) ? $data['idestado'] : '';
            $dataUnidadeDados['idcidade'] = isset($data['idcidade']) ? $data['idcidade'] : '';
            $dataUnidadeDados['telefone'] = isset($data['telefone']) ? $data['telefone'] : '';
            $dataUnidadeDados['celular'] = isset($data['celular']) ? $data['celular'] : '';
            $dataUnidadeDados['email'] = isset($data['email']) ? $data['email'] : '';
            $dataUnidadeDados['site'] = isset($data['site']) ? $data['site'] : '';

            $unidade = new Unidade();
            if ($unidade->where('id', '=', $data['id'])->update($dataUnidade)):
                $retorno['title'] = 'Sucesso!';
                $retorno['type'] = 'success';
                $retorno['text'] = 'Filial atualizada com sucesso!';
                return $retorno;
            else:
                $retorno['title'] = 'Erro!';
                $retorno['type'] = 'error';
                $retorno['text'] = 'Erro ao atualizar filial!';
                return $retorno;
            endif;

            $filialDados = new UnidadeDados();
            if ($filialDados->where('idunidade', '=', $data['id'])->update($dataUnidadeDados)):
                $retorno['title'] = 'Sucesso!';
                $retorno['type'] = 'success';
                $retorno['text'] = 'Filial atualizada com sucesso!';
                return $retorno;
            else:
                $retorno['title'] = 'Erro!';
                $retorno['type'] = 'error';
                $retorno['text'] = 'Erro ao atualizar filial!';
                return $retorno;
            endif;
        }
    }

    public function form_lead2() {
        $headers = ['category' => 'Form', 'title' => 'Form'];
        return view('adminmaster.form_lead2', compact('headers'));
    }

    public function parabens_lead2($id) {
        $user = User::find($id);
        $headers = ['category' => 'parabens_lead2', 'title' => 'parabens_lead2'];
        return view('adminmaster.parabens_lead2', compact('headers', 'user'));
    }
    

    

    public function cadastrar_lead2(Request $request) {
        
        
        $data = $request->all();

        ///criar usuario
        $user = User::create($request->all());
        $user_id = $user->id;
        $idunidade = $request->idunidade;
        $user_dados = UserDados::create(['celular' => $request->telefone,
                    'user_id' => $user_id,  'origem' => 'form lead']);


        //criar unidade
        $dataUnidadeDados['user_id'] = $user_id;
        $dataUnidadeDados['cnpj'] = $data['cnpj'];
        $dataUnidadeDados['estado'] = $data['estado'];
        $dataUnidadeDados['cidade'] = $data['cidade'];
        $dataUnidadeDados['email'] = $user['email'];
        $dataUnidadeDados['site'] = 'www.meusite.com.br';

        $empresa = Unidade::create([
                    'user_id' => $dataUnidadeDados['user_id'],
                    'parent_id' => 0,
                    'idmatriz' => 0,
                    'logo' => 'gym-padrao.png',
                    'fantasia' => $request->fantasia,
                    'razao_social' => $request->fantasia,
                    'situacao' => '1',
                    'licenca' => 'trial'
        ]);
        
        
  
        
        
          $dataUnidadeDados['telefone'] = $request->telefone;
          $dataUnidadeDados['celular'] = $request->telefone;
          $dataUnidadeDados['idunidade'] = $empresa->id;
          $filialDados = new UnidadeDados();
          if ($filialDados->create($dataUnidadeDados)):
          $retorno['title'] = 'Sucesso!';
          $retorno['type'] = 'success';
          $retorno['text'] = 'Filial cadastrada com sucesso!';
          //return $retorno;
          else:
          $retorno['title'] = 'Erro!';
          $retorno['type'] = 'error';
          $retorno['text'] = 'Erro ao cadastrar filial!';
          //    return $retorno;
          endif;

          $upUser = new User();

        if ($upUser->where('id', '=', $user->id)->update(['idunidade' => $empresa->id, 'role' => 'admin'])):
            $retorno['title'] = 'Sucesso!';
            $retorno['type'] = 'success';
            $retorno['text'] = 'Matriz atualizada com sucesso!';
        else:
            $retorno['title'] = 'Erro!';
            $retorno['type'] = 'error';
            $retorno['text'] = 'Erro ao atualizar matriz!';
        endif;
/////////////////add recepcionista////////////////////////
        $recep = User::create($request->all());
        $recep_id = $recep->id;
        $idunidade = $request->idunidade;
        $recep_dados = UserDados::create(['celular' => $request->telefone,
                    'user_id' => $user_id,  'origem' => 'form lead']);        

        $recepUser = new User();

        if ($recepUser->where('id', '=', $recep->id)->update(['idunidade' => $empresa->id, 'role' => 'recepcionista'])):
            $retorno['title'] = 'Sucesso!';
            $retorno['type'] = 'success';
            $retorno['text'] = 'Matriz atualizada com sucesso!';
        else:
            $retorno['title'] = 'Erro!';
            $retorno['type'] = 'error';
            $retorno['text'] = 'Erro ao atualizar matriz!';
        endif;        
/////////////////////////////////////////////////////////////////////////////////////        
        //$this->egoi->addSubscriber($egoi_dados);
        //$this->service->convertDonoAcademia($user_id);
          $idunidade = $empresa->id;
          ////////////////////////////
          $nome = explode(" ", $user['name']);
          $egoi_dados['nome'] = $nome[0];
          $egoi_dados['sobrenome'] = str_replace($nome[0], '', $user['name']);
          $egoi_dados['email'] = $user['email'];
          $egoi_dados['celular'] = $user_dados['celular'];

          //$this->egoi->addSubscriber($egoi_dados);

        $this->service->convertDonoAcademia($user_id);        
        
        
        
        ///////////////////libera modulo cores
        $modulos = Moduloacademia::select('id')->where('id_modulo',1)->where('id_unidade', $empresa->id)->first();

        $dataAdd['id_unidade'] =$empresa->id;
        $dataAdd['id_modulo'] = 1;
        $dataAdd['status'] = 1;
        $dataAdd['pago'] = 'N';

        if (sizeof($modulos) > 0):
             $addModulo = new Moduloacademia();
            if ($addModulo->where('id', '=', 1)->update($dataAdd)):
                $retorno['title'] = 'Sucesso!';
                $retorno['type'] = 'success';
                
  /*              if ($data['status']==1):
                    // aqui eu tenho q mandar o e-mail
                    $this->enviaEmailAvisando($data['unidade']);

//                    $retorno['text'] = 'Modulo ativado com sucesso!';
                    
                endif;*/
                
            else:
                $retorno['title'] = 'Erro!';
                $retorno['type'] = 'error';
                $retorno['text'] = 'Erro ao atualizar Modulo!';
            endif;

        else:
            $addModulo = new Moduloacademia();
            if ($addModulo->create($dataAdd)):
                $retorno['title'] = 'Sucesso!';
                $retorno['type'] = 'success';
                /*if ($data['status']==0):
                    $retorno['text'] = 'Modulo desativado com sucesso!';
                    
                endif;*/
                
              /*  if ($data['status']==1):
                    // aqui eu tenho q mandar o e-mail
                    $this->enviaEmailAvisando($data['unidade']);
                    $retorno['text'] = 'Modulo ativado com sucesso!';
                    
                endif;*/
            else:
                $retorno['title'] = 'Erro!';
                $retorno['type'] = 'error';
                $retorno['text'] = 'Erro ao adicionar modulo!';
                

            endif;

        endif;
        
        
        
        
        ///criar programa de treinamento 

        Local::create([
                    'nmlocal' => 'Sala de Musculação',
                    'metros' => '100',
                    'situacao' => 'A',
                    'idunidade' => $empresa->id
        ]);
        
        Local::create([
                    'nmlocal' => 'Sala de Ginástica',
                    'metros' => '100',
                    'situacao' => 'A',
                    'idunidade' => $empresa->id
        ]);
        
        

        ///criar programa de treinamento 

        $programa = Programatreinamento::create([
                    'nmprograma' => 'Projeto Verão',
                    'idunidade' => $empresa->id
        ]);


        Agendaprogramatreinamento::create([
            'nmagenda' => 'Avaliação física',
            'idunidade' => $empresa->id,
            'nrdias' => 0,
            'idprogramatreinamento' => $programa->id
        ]);

        Agendaprogramatreinamento::create([
            'nmagenda' => 'Orientação alimentar',
            'idunidade' => $empresa->id,
            'nrdias' => 7,
            'idprogramatreinamento' => $programa->id
        ]);


        ///criar aulas
        Programa::create([
            'nmprograma' => 'Musculação',
            'idunidade' => $empresa->id,
            'stcoletivo' => 'S'
        ]);
        Programa::create([
            'nmprograma' => 'Zumba',
            'idunidade' => $empresa->id,
            'stcoletivo' => 'S'
        ]);
        Programa::create([
            'nmprograma' => 'Bike',
            'idunidade' => $empresa->id,
            'stcoletivo' => 'S'
        ]);

        //criar objetivos musculação
        $objetivo1 = Objetivotreino::create(['nmobjetivo' => 'Hipertrofia','idunidade' => $empresa->id,'idmodalidade' => 1 ]);
        $nivel1 = Nivelhabilidade::create(['nmnivel' => 'Iniciante','idunidade' => $empresa->id,'idmodalidade' => 1,'idobjetivo' => $objetivo1->id ]);
        Nivelhabilidade::create(['nmnivel' => 'Intermediario','idunidade' => $empresa->id, 'idmodalidade' => 1,'idobjetivo' => $objetivo1->id ]);
        Nivelhabilidade::create(['nmnivel' => 'Avançado','idunidade' => $empresa->id,'idmodalidade' => 1,'idobjetivo' => $objetivo1->id]);
                
        $objetivo = Objetivotreino::create(['nmobjetivo' => 'Condicionamento','idunidade' => $empresa->id,'idmodalidade' => 1]);
        Nivelhabilidade::create(['nmnivel' => 'Iniciante','idunidade' => $empresa->id,'idmodalidade' => 1,'idobjetivo' => $objetivo->id ]);
        Nivelhabilidade::create(['nmnivel' => 'Intermediario','idunidade' => $empresa->id, 'idmodalidade' => 1,'idobjetivo' => $objetivo->id ]);
        Nivelhabilidade::create(['nmnivel' => 'Avançado','idunidade' => $empresa->id,'idmodalidade' => 1,'idobjetivo' => $objetivo->id]);
        
        
        $objetivo = Objetivotreino::create(['nmobjetivo' => 'Fortalecimento','idunidade' => $empresa->id,'idmodalidade' => 1]);
        Nivelhabilidade::create(['nmnivel' => 'Iniciante','idunidade' => $empresa->id,'idmodalidade' => 1,'idobjetivo' => $objetivo->id ]);
        Nivelhabilidade::create(['nmnivel' => 'Intermediario','idunidade' => $empresa->id, 'idmodalidade' => 1,'idobjetivo' => $objetivo->id ]);
        Nivelhabilidade::create(['nmnivel' => 'Avançado','idunidade' => $empresa->id,'idmodalidade' => 1,'idobjetivo' => $objetivo->id]);

        $objetivo = Objetivotreino::create(['nmobjetivo' => 'Emagrecimento','idunidade' => $empresa->id,'idmodalidade' => 1]);
        Nivelhabilidade::create(['nmnivel' => 'Iniciante','idunidade' => $empresa->id,'idmodalidade' => 1,'idobjetivo' => $objetivo->id ]);
        Nivelhabilidade::create(['nmnivel' => 'Intermediario','idunidade' => $empresa->id, 'idmodalidade' => 1,'idobjetivo' => $objetivo->id ]);
        Nivelhabilidade::create(['nmnivel' => 'Avançado','idunidade' => $empresa->id,'idmodalidade' => 1,'idobjetivo' => $objetivo->id]);

        //criar objetivos ginastica
        $objetivo2 = Objetivotreino::create(['nmobjetivo' => 'Hipertrofia','idunidade' => $empresa->id,'idmodalidade' => 2 ]);
        $nivel2 = Nivelhabilidade::create(['nmnivel' => 'Iniciante','idunidade' => $empresa->id,'idmodalidade' => 2,'idobjetivo' => $objetivo2->id ]);
        Nivelhabilidade::create(['nmnivel' => 'Intermediario','idunidade' => $empresa->id, 'idmodalidade' => 2,'idobjetivo' => $objetivo2->id ]);
        Nivelhabilidade::create(['nmnivel' => 'Avançado','idunidade' => $empresa->id,'idmodalidade' => 2,'idobjetivo' => $objetivo2->id]);
                
        $objetivo = Objetivotreino::create(['nmobjetivo' => 'Condicionamento','idunidade' => $empresa->id,'idmodalidade' => 2]);
        Nivelhabilidade::create(['nmnivel' => 'Iniciante','idunidade' => $empresa->id,'idmodalidade' => 2,'idobjetivo' => $objetivo->id ]);
        Nivelhabilidade::create(['nmnivel' => 'Intermediario','idunidade' => $empresa->id, 'idmodalidade' => 2,'idobjetivo' => $objetivo->id ]);
        Nivelhabilidade::create(['nmnivel' => 'Avançado','idunidade' => $empresa->id,'idmodalidade' => 2,'idobjetivo' => $objetivo->id]);
        
        
        $objetivo = Objetivotreino::create(['nmobjetivo' => 'Fortalecimento','idunidade' => $empresa->id,'idmodalidade' => 2]);
        Nivelhabilidade::create(['nmnivel' => 'Iniciante','idunidade' => $empresa->id,'idmodalidade' => 2,'idobjetivo' => $objetivo->id ]);
        Nivelhabilidade::create(['nmnivel' => 'Intermediario','idunidade' => $empresa->id, 'idmodalidade' => 2,'idobjetivo' => $objetivo->id ]);
        Nivelhabilidade::create(['nmnivel' => 'Avançado','idunidade' => $empresa->id,'idmodalidade' => 2,'idobjetivo' => $objetivo->id]);

        $objetivo = Objetivotreino::create(['nmobjetivo' => 'Emagrecimento','idunidade' => $empresa->id,'idmodalidade' => 2]);
        Nivelhabilidade::create(['nmnivel' => 'Iniciante','idunidade' => $empresa->id,'idmodalidade' => 2,'idobjetivo' => $objetivo->id ]);
        Nivelhabilidade::create(['nmnivel' => 'Intermediario','idunidade' => $empresa->id, 'idmodalidade' => 2,'idobjetivo' => $objetivo->id ]);
        Nivelhabilidade::create(['nmnivel' => 'Avançado','idunidade' => $empresa->id,'idmodalidade' => 2,'idobjetivo' => $objetivo->id]);

        //criar objetivos natacao
        $objetivo3 = Objetivotreino::create(['nmobjetivo' => 'Hipertrofia','idunidade' => $empresa->id,'idmodalidade' => 3 ]);
        $nivel3 = Nivelhabilidade::create(['nmnivel' => 'Iniciante','idunidade' => $empresa->id,'idmodalidade' => 3,'idobjetivo' => $objetivo3->id ]);
        Nivelhabilidade::create(['nmnivel' => 'Intermediario','idunidade' => $empresa->id, 'idmodalidade' => 3,'idobjetivo' => $objetivo3->id ]);
        Nivelhabilidade::create(['nmnivel' => 'Avançado','idunidade' => $empresa->id,'idmodalidade' => 3,'idobjetivo' => $objetivo3->id]);
                
        $objetivo = Objetivotreino::create(['nmobjetivo' => 'Condicionamento','idunidade' => $empresa->id,'idmodalidade' => 3]);
        Nivelhabilidade::create(['nmnivel' => 'Iniciante','idunidade' => $empresa->id,'idmodalidade' => 3,'idobjetivo' => $objetivo->id ]);
        Nivelhabilidade::create(['nmnivel' => 'Intermediario','idunidade' => $empresa->id, 'idmodalidade' => 3,'idobjetivo' => $objetivo->id ]);
        Nivelhabilidade::create(['nmnivel' => 'Avançado','idunidade' => $empresa->id,'idmodalidade' => 3,'idobjetivo' => $objetivo->id]);
        
        
        $objetivo = Objetivotreino::create(['nmobjetivo' => 'Fortalecimento','idunidade' => $empresa->id,'idmodalidade' => 3]);
        Nivelhabilidade::create(['nmnivel' => 'Iniciante','idunidade' => $empresa->id,'idmodalidade' => 3,'idobjetivo' => $objetivo->id ]);
        Nivelhabilidade::create(['nmnivel' => 'Intermediario','idunidade' => $empresa->id, 'idmodalidade' => 3,'idobjetivo' => $objetivo->id ]);
        Nivelhabilidade::create(['nmnivel' => 'Avançado','idunidade' => $empresa->id,'idmodalidade' => 3,'idobjetivo' => $objetivo->id]);

        $objetivo = Objetivotreino::create(['nmobjetivo' => 'Emagrecimento','idunidade' => $empresa->id,'idmodalidade' => 3]);
        Nivelhabilidade::create(['nmnivel' => 'Iniciante','idunidade' => $empresa->id,'idmodalidade' => 3,'idobjetivo' => $objetivo->id ]);
        Nivelhabilidade::create(['nmnivel' => 'Intermediario','idunidade' => $empresa->id, 'idmodalidade' => 3,'idobjetivo' => $objetivo->id ]);
        Nivelhabilidade::create(['nmnivel' => 'Avançado','idunidade' => $empresa->id,'idmodalidade' => 3,'idobjetivo' => $objetivo->id]);

        //criar objetivos cross
        $objetivo4 = Objetivotreino::create(['nmobjetivo' => 'Hipertrofia','idunidade' => $empresa->id,'idmodalidade' => 4 ]);
        $nivel4 = Nivelhabilidade::create(['nmnivel' => 'Iniciante','idunidade' => $empresa->id,'idmodalidade' => 4,'idobjetivo' => $objetivo4->id ]);
        Nivelhabilidade::create(['nmnivel' => 'Intermediario','idunidade' => $empresa->id, 'idmodalidade' => 4,'idobjetivo' => $objetivo4->id ]);
        Nivelhabilidade::create(['nmnivel' => 'Avançado','idunidade' => $empresa->id,'idmodalidade' => 4,'idobjetivo' => $objetivo4->id]);
                
        $objetivo = Objetivotreino::create(['nmobjetivo' => 'Condicionamento','idunidade' => $empresa->id,'idmodalidade' => 4]);
        Nivelhabilidade::create(['nmnivel' => 'Iniciante','idunidade' => $empresa->id,'idmodalidade' => 4,'idobjetivo' => $objetivo->id ]);
        Nivelhabilidade::create(['nmnivel' => 'Intermediario','idunidade' => $empresa->id, 'idmodalidade' => 4,'idobjetivo' => $objetivo->id ]);
        Nivelhabilidade::create(['nmnivel' => 'Avançado','idunidade' => $empresa->id,'idmodalidade' => 4,'idobjetivo' => $objetivo->id]);
        
        
        $objetivo = Objetivotreino::create(['nmobjetivo' => 'Fortalecimento','idunidade' => $empresa->id,'idmodalidade' => 4]);
        Nivelhabilidade::create(['nmnivel' => 'Iniciante','idunidade' => $empresa->id,'idmodalidade' => 4,'idobjetivo' => $objetivo->id ]);
        Nivelhabilidade::create(['nmnivel' => 'Intermediario','idunidade' => $empresa->id, 'idmodalidade' => 4,'idobjetivo' => $objetivo->id ]);
        Nivelhabilidade::create(['nmnivel' => 'Avançado','idunidade' => $empresa->id,'idmodalidade' => 4,'idobjetivo' => $objetivo->id]);

        $objetivo = Objetivotreino::create(['nmobjetivo' => 'Emagrecimento','idunidade' => $empresa->id,'idmodalidade' => 4]);
        Nivelhabilidade::create(['nmnivel' => 'Iniciante','idunidade' => $empresa->id,'idmodalidade' => 4,'idobjetivo' => $objetivo->id ]);
        Nivelhabilidade::create(['nmnivel' => 'Intermediario','idunidade' => $empresa->id, 'idmodalidade' => 4,'idobjetivo' => $objetivo->id ]);
        Nivelhabilidade::create(['nmnivel' => 'Avançado','idunidade' => $empresa->id,'idmodalidade' => 4,'idobjetivo' => $objetivo->id]);

        //criar objetivos corrida
        $objetivo5 = Objetivotreino::create(['nmobjetivo' => 'Hipertrofia','idunidade' => $empresa->id,'idmodalidade' => 5 ]);
        $nivel5 = Nivelhabilidade::create(['nmnivel' => 'Iniciante','idunidade' => $empresa->id,'idmodalidade' => 5,'idobjetivo' => $objetivo5->id ]);
        Nivelhabilidade::create(['nmnivel' => 'Intermediario','idunidade' => $empresa->id, 'idmodalidade' => 5,'idobjetivo' => $objetivo5->id ]);
        Nivelhabilidade::create(['nmnivel' => 'Avançado','idunidade' => $empresa->id,'idmodalidade' => 5,'idobjetivo' => $objetivo5->id]);
                
        $objetivo = Objetivotreino::create(['nmobjetivo' => 'Condicionamento','idunidade' => $empresa->id,'idmodalidade' => 5]);
        Nivelhabilidade::create(['nmnivel' => 'Iniciante','idunidade' => $empresa->id,'idmodalidade' => 5,'idobjetivo' => $objetivo->id ]);
        Nivelhabilidade::create(['nmnivel' => 'Intermediario','idunidade' => $empresa->id, 'idmodalidade' => 5,'idobjetivo' => $objetivo->id ]);
        Nivelhabilidade::create(['nmnivel' => 'Avançado','idunidade' => $empresa->id,'idmodalidade' => 5,'idobjetivo' => $objetivo->id]);
        
        
        $objetivo = Objetivotreino::create(['nmobjetivo' => 'Fortalecimento','idunidade' => $empresa->id,'idmodalidade' => 5]);
        Nivelhabilidade::create(['nmnivel' => 'Iniciante','idunidade' => $empresa->id,'idmodalidade' => 5,'idobjetivo' => $objetivo->id ]);
        Nivelhabilidade::create(['nmnivel' => 'Intermediario','idunidade' => $empresa->id, 'idmodalidade' => 5,'idobjetivo' => $objetivo->id ]);
        Nivelhabilidade::create(['nmnivel' => 'Avançado','idunidade' => $empresa->id,'idmodalidade' => 5,'idobjetivo' => $objetivo->id]);

        $objetivo = Objetivotreino::create(['nmobjetivo' => 'Emagrecimento','idunidade' => $empresa->id,'idmodalidade' => 5]);
        Nivelhabilidade::create(['nmnivel' => 'Iniciante','idunidade' => $empresa->id,'idmodalidade' => 5,'idobjetivo' => $objetivo->id ]);
        Nivelhabilidade::create(['nmnivel' => 'Intermediario','idunidade' => $empresa->id, 'idmodalidade' => 5,'idobjetivo' => $objetivo->id ]);
        Nivelhabilidade::create(['nmnivel' => 'Avançado','idunidade' => $empresa->id,'idmodalidade' => 5,'idobjetivo' => $objetivo->id]);

        //criar objetivos ciclismo
        $objetivo6 = Objetivotreino::create(['nmobjetivo' => 'Hipertrofia','idunidade' => $empresa->id,'idmodalidade' => 6 ]);
        $nivel6 = Nivelhabilidade::create(['nmnivel' => 'Iniciante','idunidade' => $empresa->id,'idmodalidade' => 6,'idobjetivo' => $objetivo6->id ]);
        Nivelhabilidade::create(['nmnivel' => 'Intermediario','idunidade' => $empresa->id, 'idmodalidade' => 6,'idobjetivo' => $objetivo6->id ]);
        Nivelhabilidade::create(['nmnivel' => 'Avançado','idunidade' => $empresa->id,'idmodalidade' => 6,'idobjetivo' => $objetivo6->id]);
                
        $objetivo = Objetivotreino::create(['nmobjetivo' => 'Condicionamento','idunidade' => $empresa->id,'idmodalidade' => 6]);
        Nivelhabilidade::create(['nmnivel' => 'Iniciante','idunidade' => $empresa->id,'idmodalidade' => 6,'idobjetivo' => $objetivo->id ]);
        Nivelhabilidade::create(['nmnivel' => 'Intermediario','idunidade' => $empresa->id, 'idmodalidade' => 6,'idobjetivo' => $objetivo->id ]);
        Nivelhabilidade::create(['nmnivel' => 'Avançado','idunidade' => $empresa->id,'idmodalidade' => 6,'idobjetivo' => $objetivo->id]);
        
        $objetivo = Objetivotreino::create(['nmobjetivo' => 'Fortalecimento','idunidade' => $empresa->id,'idmodalidade' => 6]);
        Nivelhabilidade::create(['nmnivel' => 'Iniciante','idunidade' => $empresa->id,'idmodalidade' => 6,'idobjetivo' => $objetivo->id ]);
        Nivelhabilidade::create(['nmnivel' => 'Intermediario','idunidade' => $empresa->id, 'idmodalidade' => 6,'idobjetivo' => $objetivo->id ]);
        Nivelhabilidade::create(['nmnivel' => 'Avançado','idunidade' => $empresa->id,'idmodalidade' => 6,'idobjetivo' => $objetivo->id]);

        $objetivo = Objetivotreino::create(['nmobjetivo' => 'Emagrecimento','idunidade' => $empresa->id,'idmodalidade' => 6]);
        Nivelhabilidade::create(['nmnivel' => 'Iniciante','idunidade' => $empresa->id,'idmodalidade' => 6,'idobjetivo' => $objetivo->id ]);
        Nivelhabilidade::create(['nmnivel' => 'Intermediario','idunidade' => $empresa->id, 'idmodalidade' => 6,'idobjetivo' => $objetivo->id ]);
        Nivelhabilidade::create(['nmnivel' => 'Avançado','idunidade' => $empresa->id,'idmodalidade' => 6,'idobjetivo' => $objetivo->id]);

      

          $projeto = PrescricaoProjeto::create([
          'projeto_nome' => 'Perder 5kg',
          'projeto_data' => date('d/m/Y'),
          'projeto_status' => 'A',
          'unidade_id' => $idunidade,
          'aluno_id' => $user_id,
          'professor_id' => $user_id
          ]);

          $arr1 = array(1, 2, 3, 4);

          $nomes[0] = '';
          $nomes[1] = '3x por semana no mínimo';
          $nomes[2] = 'Aumentar cargas';
          $nomes[3] = 'Diminuir o ingestão de carboidratos';
          $nomes[4] = 'Fazer treinos de aeróbicos';

          foreach ($arr1 as $value) {
          $x = 20 * $value;
          $datax = date("d/m/Y", strtotime("+" . $x . " days"));
          $etapas = PrescricaoEtapa::create([
          'projeto_id' => $projeto->id,
          'etapa_nome' => $nomes[$value],
          'etapa_data' => $datax,
          'etapa_status' => 'A',
          'idcategoria' => $value
          ]);
          }




          $maxPadrao = TreinoMusculacaoPadrao::where('unidade_id', 3)->max('treino_id');


          $treinoPadrao = TreinoMusculacaoPadrao::select('treino_nome_padrao', 'treino_qtddias', 'treino_objetivo', 'treino_nivel')
          ->where('unidade_id', 3)
          ->where('treino_id', $maxPadrao)
          ->get();

          $treino = TreinoMusculacao::create([
          'treino_revisao' => date('Y-m-d H:i:s', strtotime("+ 30 days")),
          'treino_nome' => 'Primeiro treino',
          'unidade_id' => $idunidade,
          'aluno_id' => $user_id,
          'treino_objetivo' => $objetivo1->id,
          'treino_nivel' =>  $nivel1->id,
          'treino_atual' => 1,
          'treino_qtddias' => $treinoPadrao[0]->treino_qtddias,
          'professor_id' => $idunidade
          ]);
          $treino_id = $treino->id;

          $treinofichas = TreinoFichapadrao::select('treino_ficha_padrao.*')
          ->where('treino_id', $maxPadrao)
          ->orderby('ficha_letra')
          ->orderby('ord')
          ->get();

          foreach ($treinofichas as $key => $value) {

          TreinoFicha::create([
          'ficha_letra' => $treinofichas[$key]['ficha_letra'],
          'ficha_intervalo' => $treinofichas[$key]['ficha_intervalo'],
          'ficha_series' => $treinofichas[$key]['ficha_series'],
          'ficha_repeticoes' => $treinofichas[$key]['ficha_repeticoes'],
          'medida_duracao' => $treinofichas[$key]['medida_duracao'],
          'medida_intensidade' => $treinofichas[$key]['medida_intensidade'],
          'super_serie' => $treinofichas[$key]['super_serie'],
          'ficha_carga' => $treinofichas[$key]['ficha_carga'],
          'ficha_observacao' => $treinofichas[$key]['ficha_observacao'],
          'treino_id' => $treino_id,
          'ficha_id_vinculo' => $treinofichas[$key]['ficha_id_vinculo'],
          'ord' => $treinofichas[$key]['ord'],
          'exercicio_id' => $treinofichas[$key]['exercicio_id']
          ]);
          }

          $tempo = TempoEstimadopadrao::select('tempo_estimado_padrao.*')
          ->where('treino_id', $maxPadrao)
          ->get();

          foreach ($tempo as $key => $value) {
          TempoEstimado::create([
          'client_id' => $user_id,
          'treino_id' => $treino_id,
          'letra' => $tempo[$key]['letra'],
          'tempoestimado' => $tempo[$key]['tempomusculacao'],
          'tempomusculacao' => $tempo[$key]['tempomusculacao']
          ]);
          }

          ///////////////////////////treino cross padrao
          $arr = array(0, 1, 2, 3, 4, 5, 6, 7);

          foreach ($arr as $value) {

          $value = date('Y-m-d', strtotime("+" . $value . " days"));
          PrescricaoCalendario::create([
          'idunidade' => $idunidade,
          'user_id_aluno' => $user_id,
          'user_id_prof' => $user_id,
          'desc_prescricao' => 'MODELO DE TREINO CROSS
          ----------------------------------------
          FOR TIME
          Corrida: 700 m
          Box jump: 15x
          Clean and jerk: 20x
          Back Squat: 20x
          Jump Squat: 25x
          Abd. Butterfly: 30x',
          'data_prescricao' => $value,
          'tipo_prescricao' => 'cross'
          ]);
          }

          foreach ($arr as $value) {

          $value = date('Y-m-d', strtotime("+" . $value . " days"));
          PrescricaoCalendario::create([
          'idunidade' => $idunidade,
          'user_id_aluno' => $user_id,
          'user_id_prof' => $user_id,
          'desc_prescricao' => 'MODELO DE TREINO DE CORRIDA
          --------------------------------------------------------
          Aquecimento 10min caminhando
          Z1 - 30min 60 á 69% da frequência cardíaca
          máxima.(você consegue conversar tranquilamente)
          Alongamento de MMII',
          'data_prescricao' => $value,
          'tipo_prescricao' => 'corrida'
          ]);
          }
          foreach ($arr as $value) {
          $value = date('Y-m-d', strtotime("+" . $value . " days"));
          PrescricaoCalendario::create([
          'idunidade' => $idunidade,
          'user_id_aluno' => $user_id,
          'user_id_prof' => $user_id,
          'desc_prescricao' => 'MODELO DE TREINO CICLISMO
          --------------------------------------------------------
          Aquecimento 10min giro solto
          Z1 - 30min 60 á 69% da frequência cardíaca
          máxima. Alongamento de MMII',
          'data_prescricao' => $value,
          'tipo_prescricao' => 'ciclismo'
          ]);
          }


          //////////////////gerar psa//////////////////////
          $psa = PsaAtividade::create([
          'idaluno' => $user_id,
          'idprofessor' => $user_id,
          'idunidade' => $idunidade,
          'dom_atv' => '',
          'dom_hora' => '',
          'seg_atv' => '',
          'seg_hora' => '',
          'ter_atv' => '',
          'ter_hora' => '',
          'qua_atv' => '',
          'qua_hora' => '',
          'qui_atv' => '',
          'qui_hora' => '',
          'sex_atv' => '',
          'sex_hora' => '',
          'sab_atv' => '',
          'sab_hora' => ''
          ]);


          PsaAtividade::where('id', $psa->id)->update([
          'dom_atv' => '',
          'dom_hora' => '',
          'seg_atv' => 182,
          'seg_hora' => '16:00',
          'ter_atv' => 165,
          'ter_hora' => '16:00',
          'qua_atv' => 182,
          'qua_hora' => '16:00',
          'qui_atv' => 165,
          'qui_hora' => '16:00',
          'sex_atv' => 166,
          'sex_hora' => '16:00',
          'sab_atv' => '',
          'sab_hora' => '16:00'
          ]);

         

        return ['status' => 'success', 'user_id' => $user->id];
        //return $data;
    }

    public function form_lead() {
        $headers = ['category' => 'Form', 'title' => 'Form'];
        return view('adminmaster.form_lead', compact('headers'));
    }

    public function parabens_lead($id) {
        $user = User::find($id);
        $headers = ['category' => 'parabens_lead', 'title' => 'parabens_lead'];
        return view('adminmaster.parabens_lead', compact('headers', 'user'));
    }

    public function cadastrar_lead(Request $request) {

        $user = User::create($request->all());
        $user_id = $user->id;
        $idunidade = $request->idunidade;
        $user_dados = UserDados::create(['celular' => $request->telefone,
                    'idunidade' => $request->idunidade, 'user_id' => $user_id, 'origem' => 'form lead']);


        $nome = explode(" ", $user['name']);

        $egoi_dados['nome'] = $nome[0];
        $egoi_dados['sobrenome'] = str_replace($nome[0], '', $user['name']);
        $egoi_dados['email'] = $user['email'];
        $egoi_dados['celular'] = $user_dados['celular'];

        //$this->egoi->addSubscriber($egoi_dados);

        $this->service->convertDonoAcademia($user_id);

        $projeto = PrescricaoProjeto::create([
                    'projeto_nome' => 'Perder 5kg',
                    'projeto_data' => date('d/m/Y'),
                    'projeto_status' => 'A',
                    'unidade_id' => $idunidade,
                    'aluno_id' => $user_id,
                    'professor_id' => 8569
        ]);

        $arr1 = array(1, 2, 3, 4);

        $nomes[0] = '';
        $nomes[1] = '3x por semana no mínimo';
        $nomes[2] = 'Aumentar cargas';
        $nomes[3] = 'Diminuir o ingestão de carboidratos';
        $nomes[4] = 'Fazer treinos de aeróbicos';

        foreach ($arr1 as $value) {
            $x = 20 * $value;
            $datax = date("d/m/Y", strtotime("+" . $x . " days"));
            $etapas = PrescricaoEtapa::create([
                        'projeto_id' => $projeto->id,
                        'etapa_nome' => $nomes[$value],
                        'etapa_data' => $datax,
                        'etapa_status' => 'A',
                        'idcategoria' => $value
            ]);
        }




        $maxPadrao = TreinoMusculacaoPadrao::where('unidade_id', $idunidade)->max('treino_id');


        $treinoPadrao = TreinoMusculacaoPadrao::select('treino_nome_padrao', 'treino_qtddias', 'treino_objetivo', 'treino_nivel')
                ->where('unidade_id', $idunidade)
                ->where('treino_id', $maxPadrao)
                ->get();
        $treino = TreinoMusculacao::create([
                    'treino_revisao' => date('Y-m-d H:i:s', strtotime("+ 30 days")),
                    'treino_nome' => 'Primeiro treino',
                    'unidade_id' => $idunidade,
                    'aluno_id' => $user_id,
                    'treino_objetivo' => $treinoPadrao[0]->treino_objetivo,
                    'treino_nivel' => $treinoPadrao[0]->treino_nivel,
                    'treino_atual' => 1,
                    'treino_qtddias' => $treinoPadrao[0]->treino_qtddias,
                    'professor_id' => 8569
        ]);
        $treino_id = $treino->id;

        $treinofichas = TreinoFichapadrao::select('treino_ficha_padrao.*')
                ->where('treino_id', $maxPadrao)
                ->orderby('ficha_letra')
                ->orderby('ord')
                ->get();

        foreach ($treinofichas as $key => $value) {

            TreinoFicha::create([
                'ficha_letra' => $treinofichas[$key]['ficha_letra'],
                'ficha_intervalo' => $treinofichas[$key]['ficha_intervalo'],
                'ficha_series' => $treinofichas[$key]['ficha_series'],
                'ficha_repeticoes' => $treinofichas[$key]['ficha_repeticoes'],
                'medida_duracao' => $treinofichas[$key]['medida_duracao'],
                'medida_intensidade' => $treinofichas[$key]['medida_intensidade'],
                'super_serie' => $treinofichas[$key]['super_serie'],
                'ficha_carga' => $treinofichas[$key]['ficha_carga'],
                'ficha_observacao' => $treinofichas[$key]['ficha_observacao'],
                'treino_id' => $treino_id,
                'ficha_id_vinculo' => $treinofichas[$key]['ficha_id_vinculo'],
                'ord' => $treinofichas[$key]['ord'],
                'exercicio_id' => $treinofichas[$key]['exercicio_id']
            ]);
        }

        $tempo = TempoEstimadopadrao::select('tempo_estimado_padrao.*')
                ->where('treino_id', $maxPadrao)
                ->get();

        foreach ($tempo as $key => $value) {
            TempoEstimado::create([
                'client_id' => $user_id,
                'treino_id' => $treino_id,
                'letra' => $tempo[$key]['letra'],
                'tempoestimado' => $tempo[$key]['tempomusculacao'],
                'tempomusculacao' => $tempo[$key]['tempomusculacao']
            ]);
        }

        ///////////////////////////treino cross padrao
        $arr = array(0, 1, 2, 3, 4, 5, 6, 7);

        foreach ($arr as $value) {

            $value = date('Y-m-d', strtotime("+" . $value . " days"));
            PrescricaoCalendario::create([
                'idunidade' => $idunidade,
                'user_id_aluno' => $user_id,
                'user_id_prof' => 8569,
                'desc_prescricao' => 'MODELO DE TREINO CROSS
----------------------------------------
FOR TIME
Corrida: 700 m
Box jump: 15x
Clean and jerk: 20x
Back Squat: 20x
Jump Squat: 25x
Abd. Butterfly: 30x',
                'data_prescricao' => $value,
                'tipo_prescricao' => 'cross'
            ]);
        }

        foreach ($arr as $value) {

            $value = date('Y-m-d', strtotime("+" . $value . " days"));
            PrescricaoCalendario::create([
                'idunidade' => $idunidade,
                'user_id_aluno' => $user_id,
                'user_id_prof' => 8569,
                'desc_prescricao' => 'MODELO DE TREINO DE CORRIDA
--------------------------------------------------------
Aquecimento 10min caminhando
Z1 - 30min 60 á 69% da frequência cardíaca
máxima.(você consegue conversar tranquilamente) 
Alongamento de MMII',
                'data_prescricao' => $value,
                'tipo_prescricao' => 'corrida'
            ]);
        }
        foreach ($arr as $value) {
            $value = date('Y-m-d', strtotime("+" . $value . " days"));
            PrescricaoCalendario::create([
                'idunidade' => $idunidade,
                'user_id_aluno' => $user_id,
                'user_id_prof' => 8569,
                'desc_prescricao' => 'MODELO DE TREINO CICLISMO
--------------------------------------------------------
Aquecimento 10min giro solto
Z1 - 30min 60 á 69% da frequência cardíaca
máxima. Alongamento de MMII',
                'data_prescricao' => $value,
                'tipo_prescricao' => 'ciclismo'
            ]);
        }


//////////////////gerar psa//////////////////////
        $psa = PsaAtividade::create([
                    'idaluno' => $user_id,
                    'idprofessor' => 8569,
                    'idunidade' => $idunidade,
                    'dom_atv' => '',
                    'dom_hora' => '',
                    'seg_atv' => '',
                    'seg_hora' => '',
                    'ter_atv' => '',
                    'ter_hora' => '',
                    'qua_atv' => '',
                    'qua_hora' => '',
                    'qui_atv' => '',
                    'qui_hora' => '',
                    'sex_atv' => '',
                    'sex_hora' => '',
                    'sab_atv' => '',
                    'sab_hora' => ''
        ]);


        PsaAtividade::where('id', $psa->id)->update([
            'dom_atv' => '',
            'dom_hora' => '',
            'seg_atv' => 182,
            'seg_hora' => '16:00',
            'ter_atv' => 165,
            'ter_hora' => '16:00',
            'qua_atv' => 182,
            'qua_hora' => '16:00',
            'qui_atv' => 165,
            'qui_hora' => '16:00',
            'sex_atv' => 166,
            'sex_hora' => '16:00',
            'sab_atv' => '',
            'sab_hora' => '16:00'
        ]);



        return ['status' => 'success', 'user_id' => $user_id];
    }
    
    public function bloqueiaTrial() {
        
        $unidades = Unidade::select('id','created_at')->where('licenca','trial')->where('situacao','1')->get();        
        
        $datas='';
        $dtHj=date('Y-m-d H:i:s');
        foreach($unidades as $key=>$value){
                $datas=date('Y-m-d', strtotime($unidades[$key]->created_at));
                $data_inicial =$datas;
                $data_final = $dtHj;

                $diferenca = strtotime($data_final) - strtotime($data_inicial);
                
                $dias = floor($diferenca / (60 * 60 * 24));
                
                $unidade = Unidade::select('unidade.*')->where('id',$unidades[$key]->id)->get();    
                if ($dias>=15){
                    ///bloquear aqui 
                    Unidade::where('id', $unidade[0]->id)->update(['situacao' => 0]);                    
                    
                }
            
        }
        
        return $unidades;
        
        
    }        
    public function bloqueiaTrialModulo() {
        
        $modulos= Moduloacademia::select('id_modulo','id_unidade','created_at')->where('pago','N')->where('status','1')->get();        
        
        $datas='';
        $dtHj=date('Y-m-d H:i:s');
        foreach($modulos as $key=>$value){
                $datas=date('Y-m-d', strtotime($modulos[$key]->created_at));
                $data_inicial =$datas;
                $data_final = $dtHj;

                $diferenca = strtotime($data_final) - strtotime($data_inicial);
                
                $dias = floor($diferenca / (60 * 60 * 24));
                
                $unidade = Moduloacademia::select('modulos_has_unidade.*')->where('id_unidade',$modulos[$key]->id_unidade)->where('id_modulo',$modulos[$key]->id_modulo)->get();    
                if ($dias>=7){
                    ///bloquear aqui 
                    Moduloacademia::where('id_unidade', $unidade[0]->id_unidade)->where('id_modulo', $unidade[0]->id_modulo)->update(['status' => 0]);                    
                    
                }
            
        }
        
        return $modulos;
        
        
    }        

}
