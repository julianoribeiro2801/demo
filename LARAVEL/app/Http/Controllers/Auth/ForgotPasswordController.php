<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\ResetPassRequest;
use App\Models\User;
use App\Notifications\NewPass;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showResetAppPasswordForm()
    {
        return view('auth.passwords.app');
    }

    public function resetAppPassword(ResetPassRequest $request)
    {
        try {
            $user = User::where('email', $request->email)->first();
            $newpass = $this->generatePass();

            $user->password = $newpass;
            $user->save();
            $user->notify(new NewPass($newpass));

            return redirect()->back()->with('status', 'Sua nova senha foi enviada para seu email!');
        } catch (\Exception $exception) {
            return redirect()->back()->withErrors($exception);
        }
    }

    private function generatePass($length = 6)
    {
        $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        return substr(str_shuffle(str_repeat($pool, $length)), 0, $length);
    }
}
