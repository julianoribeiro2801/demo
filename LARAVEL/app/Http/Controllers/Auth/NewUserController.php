<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests\NewsUserRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Validator;
use App\Http\Controllers\Controller;

class NewUserController extends Controller
{
    public function welcome(Request $request)
    {
        $hash = $request->get('hash');
        if (!$hash) {
            return view('auth.tokenexpired', compact('user', 'hash'));
            // abort(404);
        }

        $user = User::where('remember_token', $hash)->first();
        if($user) {
            return view('auth.newuser', compact('user', 'hash'));
        } else{
             return view('auth.tokenexpired', compact('user', 'hash'));
        }
        
    }



    public function createPassword(NewsUserRequest $request)
    {
        // AQUI EXECUTA DEPOIS Q PREENCHE A NOVA SENHA
        try {
            $hash = $request->get('_hash');
            if (!$hash) {
                return view('auth.tokenexpired', compact('user', 'hash'));
                // abort(404);
            }
            $user = User::where('remember_token', $hash)->first();

            $hashed = $request->password;
            $user->password = $hashed;
            $user->remember_token = null;

              $update = User::where('email', $user->email)
              ->update(['remember_token' => $user->remember_token , 'password' => $user->password]);

            //$user->save();

            if ($user->role != "cliente") {
               return redirect()->route('admin.index');
            }
            return view('auth.welcome', compact('user'));
        } catch (\Exception $exception) {

            return view('auth.tokenexpired', compact('user', 'hash'));
             
        }
    }
}
