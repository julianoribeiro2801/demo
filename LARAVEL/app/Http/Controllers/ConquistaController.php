<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Conquista;
use App\Models\Tipoconquista;
use App\Models\Atividade;

use Hash;
use App\Http\Requests\ConquistaRequest;

use App\Services\TrataDadosService;

class ConquistaController extends Controller
{
    public function __construct(TrataDadosService $trataDadosService)
    {
        $this->trataDadosService = $trataDadosService;
    }
        
    
    public function index()
    {
        $headers = ['category' => 'Clientes', 'title' => 'Conquistas '];
        $conquistas = Conquista::all();
        return view('admin.conquistas.index', compact('conquistas', 'headers'));
    }

    public function edit($id)
    {
        $headers = ['category' => 'Clientes', 'title' => 'Conquistas '];
        $conquista = Conquista::where('id', $id)->first();
       
        $nomes =  Tipoconquista::select('id', 'dstipoconquista')->get();
        $selectedTipoconquistas = $this->trataDadosService->listToSelectTipoconquistas($nomes);

        $nomes =  Atividade::select('id', 'nmatividade')->get();
        $selectedAtividades = $this->trataDadosService->listToSelectAtividades($nomes);
       
        return view('admin.conquistas.edit', compact('conquista', 'selectedTipoconquistas', 'selectedAtividades', 'headers'));
    }

    public function create()
    {
        $headers = ['category' => 'Clientes', 'title' => 'Conquistas '];
        $tipoconquistas = Tipoconquista::select('id', 'dstipoconquista')->get();
        $selectedTipoconquistas = $this->trataDadosService->listToSelectTipoconquistas($tipoconquistas);
      
        $atividades = Atividade::select('id', 'nmatividade')->get();
        $selectedAtividades = $this->trataDadosService->listToSelectAtividades($atividades);

        return view('admin.conquistas.create', compact('selectedTipoconquistas', 'selectedAtividades', 'headers'));
    }

    public function update(ConquistaRequest $request, $id)
    {
        $data = $request->all();

        Conquista::where('id', $id)->update(['nmconquista' => $data['nmconquista'],
        'idtipoconquista' => $data['idtipoconquista'],
        'idatividade' => $data['idatividade']

       ]);
     
        return redirect()->route('admin.clientes.conquistas.index');
    }

    public function store(ConquistaRequest $request)
    {
        $data = $request->all();
       
        Conquista::create(['nmconquista' => $data['nmconquista'],
        'idtipoconquista' => $data['idtipoconquista'],
        'idatividade' => $data['idatividade']

      ]);
     
        return redirect()->route('admin.clientes.conquistas.index');
    }


    // public function create_user(){

          //  User::create( [
    //          'email' => 'kk@msn.com' ,
    //          'password' => Hash::make( '123456' ) ,
    //          'name' => 'KK Ruiz' ,
    //          'role' => 'admin'
    //         ] );
    // }
}
