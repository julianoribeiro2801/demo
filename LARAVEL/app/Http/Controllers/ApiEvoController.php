<?php

//header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");

namespace App\Http\Controllers;

use DateTime;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserDados;
use App\Models\Configura;
use SoapClient;
use App\Models\Unidade;
use DB;
use Hash;
use Mail;
use Redirect;
use stdClass;
// Precisa para funcionar o combo
use Auth;
use App\Services\FilialService;

class ApiEvoController extends Controller {

    // $SERVER = 'http://webservice.com.br/CatalogoWS.svc?wsdl';  
    //WSDL paths  
    //const ZRW_WS_PRODUTO_SCHEMALOCATION  = 'http://webservice.com.br/CatalogoWS.svc?xsd=xsd2';  
    //const ZRW_WS_PRODUTO_NAMESPACE = 'http://schemas.webservice.Integration.ServiceModel.Produto';
    //default values  
    const INITIAL_PASS = 'xxxx';
    const ABAP_TRUE = 'X';
    const ABAP_FALSE = '';

    /**
     * 
     * @var array SAP Logon Details 
     */
    private $SOAP_OPTS;

    /**
     * 
     * @var SoapClient Soap Object 
     */
    private $client;

    public function getFunctions() {
        $this->client = new SoapClient('http://w12evo.com/Integracao/API/EVO3.svc?wsdl');


        try {
            return $this->client->__getFunctions();
        } catch (SoapFault $e) {
            throw $e;
        }
    }

    public function listarClientesGeralEvo() {
        
        error_reporting(E_ALL);
        ini_set('display_errors', '1');
        $url = "http://w12evo.com/Integracao/API/EVO3.svc?wsdl";

        $client = new SoapClient($url, array("trace" => 1, "exception" => 0));

        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }

         $SOAPCall = "ListarClientesGeral";
        $SoapCallParameters = new stdClass();
        $SoapCallParameters->Chave = 'UpgaIbuEXE9xFXLkJsJzYQ==';
        $SoapCallParameters->IdFilial = 1;
        $SoapCallParameters->IdFilialBoll = true;
        $obj = $client->ListarClientesGeral($SoapCallParameters);

        $clientes = json_encode($obj->ListarClientesGeralResult->VO_ClientesGeral);

        $cli = json_decode($clientes);
        $cont=0;
        Foreach ($cli as $value) {
            $usuario = UserDados::select('controle_id', 'id', 'user_id')
                    ->where('controle_id', $value->ID_CLIENTE)
                    ->where('idunidade', $idunidade)
                    ->get();
            //if ($cont == 0):
            $role = '';
            if ($value->STATUS == 'ATIVO'):
                $role = 'cliente';
            else:
                $role = 'prospect';
            endif;



            $User['role'] = $role;

            if (sizeOf($usuario) > 0):
                $adm = User::where('id',$usuario[0]->user_id)->update(['role' =>  $role]);
                $cont++;
            else:
                // $adm = User::create($User);
            endif;

            $dadosUser['situacaomatricula'] = $value->STATUS;
            if (sizeOf($usuario) > 0):
                $admDados = UserDados::where('id', $usuario[0]->id)->update($dadosUser);
            endif;


            // endif;
          
        }    
        
        
        return $cont;
        
        
        
    }    
    
    public function listarClientes() {
        error_reporting(E_ALL);
        ini_set('display_errors', '1');
        $url = "http://w12evo.com/Integracao/API/EVO3.svc?wsdl";

        $client = new SoapClient($url, array("trace" => 1, "exception" => 0));

        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }

        $SOAPCall = "ListarClientes";
        $SoapCallParameters = new stdClass();
        $SoapCallParameters->Chave = 'UpgaIbuEXE9xFXLkJsJzYQ==';
        $SoapCallParameters->IdFilial = 1;
        $SoapCallParameters->IdFilialBoll = true;
        $obj = $client->ListarClientes($SoapCallParameters);

        $clientes = json_encode($obj->ListarClientesResult->VO_Clientes);

        $cli = json_decode($clientes);
        Foreach ($cli as $value) {

            //$cad=UserDados::select('controle_id','id')->where('controle_id', $id)->where('idunidade', $idunidade)->first() ;
            $usuario = UserDados::select('controle_id', 'id', 'user_id')
                    ->where('controle_id', $cli->ID_CLIENTE)
                    ->where('idunidade', $idunidade)
                    ->get();
            //if ($cont == 0):
            $role = '';
            if ($cli->STATUS == 'ATIVO'):
                $role = 'cliente';
            else:
                $role = 'prospect';
            endif;



            $User['name'] = $cli->NOME;
            $User['email'] = $cli->EMAIL;
            $User['role'] = $role;
            $User['idunidade'] = $idunidade;

            if (sizeOf($usuario) > 0):
            //$adm = User::where('id',$usuario[0]->user_id)->update($User);

            else:
            // $adm = User::create($User);
            endif;

            $dadosUser['idunidade'] = $idunidade;
            $dadosUser['controle_id'] = $cli->ID_CLIENTE;
            $dadosUser['cep'] = $cli->CEP;
            $dadosUser['endereco'] = $cli->ENDERECO;
            $dadosUser['complemento'] = $cli->COMPLEMENTO;
            $dadosUser['numero'] = $cli->NUMERO;
            $dadosUser['bairro'] = $cli->BAIRRO;
            //$dadosUser['cidade'] = $cli->CIDADE;
            //$dadosUser['controle_id'] = $cli->SIGLA;
            //$dadosUser['rg'] = $cli->RG;
            $dadosUser['cpf'] = $cli->CPF;
            $dadosUser['telefone'] = $cli->TELEFONE;
            $dadosUser['genero'] = $cli->SEXO;
            //$dadosUser['controle_id'] = $cli->NM_PAI;
            //$dadosUser['controle_id'] = $cli->NM_MAE;
            //$dadosUser['controle_id'] = $cli->CONTATO_EMERGENCIA;
            //$dadosUser['controle_id'] = $cli->DT_CADASTRO;
            $dadosUser['dt_nascimento'] = $cli->DT_NASCIMENTO;
            $dadosUser['situacaomatricula'] = $cli->STATUS;
            if (sizeOf($usuario) > 0):
                $admDados = UserDados::where('id', $usuario[0]->id)->update($dadosUser);
            else:
                $dadosUser['user_id'] = $adm->id;
                $admDados = UserDados::create($dadosUser);
            endif;


            // endif;
            $cont++;
        }







        return $cli;
    }

    public function atualizaClientes($inicial, $final) {


        $i = $inicial;

        while ($inicial <= $final) {

            $ret = $this->ListarClienteID($inicial);

            $inicial++;
        }
    }

    public function importaClientesEvo() {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }


        $configura = Configura::select('sistema','chave_sistema')->where('idunidade', $idunidade)->first();
        $resultado[] = "";
        if ($configura->sistema == "Evo"):

            $vars = UserDados::select(DB::raw('max(controle_id) as id'))
                    ->where('idunidade', $idunidade)
                    ->get();


            $inicial = $vars[0]->id;
            $final = $inicial + 10;


            while ($inicial <= $final) {

                $ret = $this->ListarClienteID($inicial);
                $resultado[$inicial] = $ret;
                $inicial++;
            }
        endif;
        return $resultado;
    }

    public function ListarClienteID($id) {
        error_reporting(E_ALL);
        ini_set('display_errors', '1');
        $url = "http://w12evo.com/Integracao/API/EVO3.svc?wsdl";

        $client = new SoapClient($url, array("trace" => 1, "exception" => 0));

//var_dump($client->__getFunctions());
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        
        
        $configura = Configura::select('sistema','chave_sistema')->where('idunidade', $idunidade)->first();
        //$resultado[] = "";
        //if ($configura->sistema == "Evo"):        
        
        
        $SOAPCall = "ListarClienteID";
        $SoapCallParameters = new stdClass();
        $SoapCallParameters->Chave = $configura->chave_sistema;
        //$SoapCallParameters->IdFilial = 1;
        $SoapCallParameters->IdCliente = $id;
        $obj = $client->ListarClienteID($SoapCallParameters);
        $clientes = json_encode($obj->ListarClienteIDResult);

        $cli = json_decode($clientes);

        $tot = sizeof($cli);
        $cont = 0;
        $cad = "";
        Foreach ($cli as $value) {

            //echo $cli->ID_CLIENTE;
            if ($cli->ID_CLIENTE > 0) :


                //$cad=UserDados::select('controle_id','id')->where('controle_id', $id)->where('idunidade', $idunidade)->first() ;
                $usuario = UserDados::select('controle_id', 'id', 'user_id')
                        ->where('controle_id', $id)
                        ->where('idunidade', $idunidade)
                        ->get();


                if ($cont == 0):


                    $role = '';
                    if ($cli->STATUS !== 'null'):
                        $role = 'prospect';
                    else:
                        $role = 'prospect';
                    endif;



                    $User['name'] = $cli->NOME;
                    $User['email'] = $cli->EMAIL;
                    $User['role'] = $role;
                    $User['idunidade'] = $idunidade;

                    if (sizeOf($usuario) > 0):
                        $adm = User::where('id', $usuario[0]->user_id)->update($User);
                    //UserDados::where('controle_id', $aluno)->where('idunidade', $idunidade)->update([
                    else:
                        $adm = User::create($User);
                    endif;


                    $dadosUser['idunidade'] = $idunidade;
                    $dadosUser['controle_id'] = $cli->ID_CLIENTE;
                    $dadosUser['cep'] = $cli->CEP;
                    $dadosUser['endereco'] = $cli->ENDERECO;
                    $dadosUser['complemento'] = $cli->COMPLEMENTO;
                    $dadosUser['numero'] = $cli->NUMERO;
                    $dadosUser['bairro'] = $cli->BAIRRO;
                    //$dadosUser['cidade'] = $cli->CIDADE;
                    //$dadosUser['controle_id'] = $cli->SIGLA;
                    //$dadosUser['rg'] = $cli->RG;
                    $dadosUser['cpf'] = $cli->CPF;
                    $dadosUser['telefone'] = $cli->TELEFONE;
                    $dadosUser['genero'] = $cli->SEXO;
                    //$dadosUser['controle_id'] = $cli->NM_PAI;
                    //$dadosUser['controle_id'] = $cli->NM_MAE;
                    //$dadosUser['controle_id'] = $cli->CONTATO_EMERGENCIA;
                    //$dadosUser['controle_id'] = $cli->DT_CADASTRO;
                    $dadosUser['dt_nascimento'] = $cli->DT_NASCIMENTO;
                    $dadosUser['situacaomatricula'] = isset($cli->STATUS) ? $cli->STATUS : 'INATIVA';
                    if (sizeOf($usuario) > 0):
                        $admDados = UserDados::where('id', $usuario[0]->id)->update($dadosUser);
                    else:
                        $dadosUser['user_id'] = $adm->id;
                        $admDados = UserDados::create($dadosUser);
                    endif;


                endif;

            endif;
            $cont++;
        }




        return $cli;
    }

}
