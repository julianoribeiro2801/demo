<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


// Precisa para funcionar o combo
use Auth;
use App\Services\FilialService;

class CrmController extends Controller
{
    public function index()
    {
        $headers = ['category' => 'CRM', 'title' => 'CRM'];
        return view('admin.crm.index', compact('headers'));
    }

    public function operacional()
    {
        $tt = 'CRM Professor: '.Auth::user()->name;
        $headers = ['category' => $tt, 'title' => $tt];
        return view('admin.crm.operacional', compact('headers'));
    }
}
