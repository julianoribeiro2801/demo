<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Input;
use Request;
use App\Models\Spm;
use App\Models\Programa;
use App\Models\Local;
use App\Models\User;
use App\Models\Funcionario;
use App\Models\ReservaSpm;
use App\Models\LogReservaSpm;
use App\Models\Nivelhabilidade;
use App\Services\TrataDadosService;
// Precisa para funcionar o combo
use Auth;
use App\Services\FilialService;

class SpmController extends Controller
{
    public function __construct(TrataDadosService $trataDadosService, FilialService $filialService)
    {
        $this->middleware('auth');
        $this->trataDadosService = $trataDadosService;
        $this->filialService = $filialService;
    }

    public function index()
    {
        $headers = ['category' => 'SPM', 'title' => 'SPM'];
        $unidades_combo = $this->filialService->unidadesComboTop();
        return view('admin.spms.index', compact('headers', 'unidades_combo'));
    }

    public function getNiveis()
    {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        $niveis = Nivelhabilidade::select('id', 'nmnivel')->where('idunidade', $idunidade)->get();
        return $niveis;
    }

    public function getProgramas()
    {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        $programas = Programa::select('id', 'nmprograma')->where('idunidade', $idunidade)->get();
        return $programas;
    }

    public function getLocais()
    {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        $locais = Local::select('id', 'nmlocal')->where('situacao', 'A')->where('idunidade', $idunidade)->get();
        return $locais;
    }

    public function getProfessores()
    {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        $professores = Funcionario::select('id', 'name')->where('role', 'funcionario')->where('idunidade', $idunidade)->orderby('name')->get();
        foreach ($professores as $key => $professor) {
            $professores[$key]->name = ucwords(strtolower($professor->name));
            $reservas = ReservaSpm::select('n_reservas')
                            ->leftJoin('spm', 'spm.id', '=', 'spm_reserva.idspm')
                            ->where('spm.idfuncionario', $professor->id)->get();
            $contReservaSpms = count($reservas);
            $total = 0;
            foreach ($reservas as $reserva) {
                $total += $reserva->n_reservas;
            }
            if (isset($total) && $total != 0) {
                $professor->media = $total / $contReservaSpms;
            } else {
                $professor->media = 0;
            }
        }
        return $professores;
    }

    public function getProfessor()
    {
        $data = Input::all();
        $professor = Funcionario::select('name')->where('id', $data['id'])->get();
        return ucwords(strtolower($professor[0]['name']));
    }

    public function getHora($Hora)
    {
        if ($Hora != null):
            $hora = date("H:i", strtotime($Hora));
        return $hora;
        endif;
    }

    public function getSpms()
    {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        // AQUI TEM QUE PASSAR PERIODO
        $spms = Spm::select('spm.*', 'users.name as nmprofessor', 'programa.nmprograma', 'local.nmlocal', 'nivelhabilidade.nmnivel')
                        ->leftJoin('users', 'users.id', '=', 'spm.idfuncionario')
                        ->leftJoin('programa', 'programa.id', '=', 'spm.idprograma')
                        ->leftJoin('local', 'local.id', '=', 'spm.idlocal')
                        ->leftJoin('nivelhabilidade', 'nivelhabilidade.id', '=', 'spm.nivel')
                        ->where('spm.idunidade', $idunidade)->get();
        foreach ($spms as $spm) {
            $reservas = LogReservaSpm::where('idspm', $spm->id)->get();
            $contReservaSpms = count($reservas);

            if ($contReservaSpms > 0) {
                $spm->media = $spm->capacidade_max / $contReservaSpms;
                if ($spm->privado == 'Sim') {
                    $spm->vacancia = $spm->capacidade_max - $contReservaSpms;
                    // $spm->ocupacao = round($spm->vacancia / $spm->capacidade_max);
                    $spm->ocupacao = $contReservaSpms;
                } else {
                    $spm->vacancia = 0;
                    $spm->ocupacao = 0;
                }

                $diasdasemana = preg_replace("/[^0-9]/", "", $spm->diasemana);
                $diasdasemana = strlen($spm->diasemana);
                if (($diasdasemana * $spm->capacidade_max)> 0):
                    $spm->taxa_frequencia = ($diasdasemana * $spm->vacancia) / ($diasdasemana * $spm->capacidade_max); else:
                    $spm->taxa_frequencia = 0;
                endif;
                $spm->taxa_frequencia = round($spm->taxa_frequencia, 1);
                $spm->revisado = round($spm->taxa_frequencia * $spm->equalizador);
            } else {
                $spm->media = 0;
                $spm->revisado = 0;
                $spm->vacancia = 0;
                $spm->ocupacao = 0;
                $spm->taxa_frequencia = 0;
            }
            $spm->hora_inicio = $this->getHora($spm->hora_inicio);

            $spm->diasemana = explode(',', $spm->diasemana);
            $spm->domingo = in_array("1", $spm->diasemana) ? true : false;
            $spm->segunda = in_array("2", $spm->diasemana) ? true : false;
            $spm->terca = in_array("3", $spm->diasemana) ? true : false;
            $spm->quarta = in_array("4", $spm->diasemana) ? true : false;
            $spm->quinta = in_array("5", $spm->diasemana) ? true : false;
            $spm->sexta = in_array("6", $spm->diasemana) ? true : false;
            $spm->sabado = in_array("7", $spm->diasemana) ? true : false;
        }
        return $spms;
    }

    public function getTaxaOcupacao()
    {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        // AQUI TEM QUE PASSAR PERIODO
        $reservas = ReservaSpm::select('n_reservas')->where('idunidade', $idunidade)->get();
        $contReservaSpms = count($reservas);
        $totalReservaSpms = 0;
        foreach ($reservas as $reserva) {
            $totalReservaSpms += $reserva->n_reservas;
        }
        // AQUI TEM QUE PASSAR PERIODO
        $spms = Spm::select('capacidade_max')->where('idunidade', $idunidade)->get();
        $totalCapacidade = 0;
        foreach ($spms as $spm) {
            $totalCapacidade += $spm->capacidade;
        }
        if ($totalReservaSpms == 0) {
            $media = 0;
        } else {
            // $media = ($totalReservaSpms*100) / $totalCapacidade;
        }
        $taxaOcupacao = [
            'totalReservaSpms' => $totalReservaSpms,
            'totalCapacidade' => $totalCapacidade
        ];
        return $taxaOcupacao;
    }

    public function addSpm()
    {
        $data = Input::all();
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        // SALVA GFM
        $spm = Spm::create(['idunidade' => $idunidade]);
        $spm = ['id' => $spm->id];
        return $spm;
    }

    public function upSpm()
    {
        $data = Input::all();
        $data['domingo'] = ($data['domingo'] == true) ? '1,' : '';
        $data['segunda'] = ($data['segunda'] == true) ? '2,' : '';
        $data['terca'] = ($data['terca'] == true) ? '3,' : '';
        $data['quarta'] = ($data['quarta'] == true) ? '4,' : '';
        $data['quinta'] = ($data['quinta'] == true) ? '5,' : '';
        $data['sexta'] = ($data['sexta'] == true) ? '6,' : '';
        $data['sabado'] = ($data['sabado'] == true) ? '7,' : '';
        $data['diasemana'] = $data['domingo'] . $data['segunda'] . $data['terca'] . $data['quarta'] . $data['quinta'] . $data['sexta'] . $data['sabado'];
        $data['diasemana'] = substr($data['diasemana'], 0, -1);
        unset($data['domingo'], $data['segunda'], $data['terca'], $data['quarta'], $data['quinta'], $data['sexta'], $data['sabado']);

        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }

        // SALVA GFM
        $spm = Spm::where('id', $data['id'])->update([
            'idlocal' => $data['idlocal'],
            'idprograma' => $data['idprograma'],
            'idfuncionario' => $data['idfuncionario'],
            'nivel' => $data['nivel'],
            'privado' => $data['privado'],
            'gfmspm' => $data['gfmspm'],
            'hora_inicio' => $data['hora_inicio'],
            'duracao' => $data['duracao'],
            'diasemana' => $data['diasemana'],
            'valor' => $data['valor'],
            'capacidade_max' => $data['capacidade_max'],
            'capacidade_min' => $data['capacidade_min'],
            'equalizador' => $data['equalizador'],
            'revisado' => $data['revisado']
        ]);
        return $spm;
    }

    public function addAula()
    {
        $data = Input::all();
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        // if (isset($data['nmprograma']) && $data['nmprograma'] != '') {
        //$programas = Programa::select('id')->where('nmprograma', $data['nmprograma'])->first();
        // $contProgramas = count($programas);
        //if ($contProgramas == 0) {
        
                $dataDados['idunidade'] = $idunidade;
                $dataDados['nmprograma'] = strtoupper($data['nmprograma']);

                $programa = new Programa();
                if ($programa->create($dataDados)):        
                    $retorno['id'] = $programa->idunidade;
                    $retorno['title'] = 'Sucesso!';
                    $retorno['type'] = 'success';
                    $retorno['text'] = 'Programa cadastrado com sucesso!';
                    return $retorno;
                else:
                    $retorno['title'] = 'Erro!';
                    $retorno['type'] = 'error';
                    $retorno['text'] = 'Erro ao cadastrar programa!';
                    return $retorno;
                endif;
        
        /*$programa = Programa::create([
                            'idunidade' => $idunidade,
                            'nmprograma' => $data['nmprograma'],
                            'stcoletivo' => 'S'
                ]);*/
        
        
    }

    public function upAula()
    {
        $data = Input::all();
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
            $dataDados['idunidade'] = $idunidade;
            $dataDados['nmprograma'] = strtoupper($data['nmprograma']);
            $local = new Programa();
            if ($local->where('id', '=', $data['id'])->update($dataDados)):
                $retorno['title'] = 'Sucesso!';
                $retorno['type'] = 'success';
                $retorno['text'] = 'Aula atualizado com sucesso!';
            else:
                $retorno['title'] = 'Erro!';
                $retorno['type'] = 'error';
                $retorno['text'] = 'Erro ao atualizar aula!';
            endif;
            return $retorno;        
        /*$programa = Programa::where('id', $data['id'])->update([
                        'idunidade' => $idunidade,
                        'nmprograma' => $data['nmprograma'],
                         'stcoletivo' => 'S'
       ]);
        /*        $programa = Programa::update([
                            'idunidade' => $idunidade,
                            'nmprograma' => $data['nmprograma'],
                            'stcoletivo' => 'S'
                ]);
        echo "pau";*/
        return $programa;
        //}
        //}
    }
    public function delSpm($spm_id)
    {
        Spm::where('id', $spm_id)->delete();
    }

    // GET CLIENTES
    public function getClients()
    {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }

        $clients = User::select('id', 'name')->where('role', 'cliente')->where('idunidade', $idunidade)->get();
        return $clients;
    }

    // GET INSCRIÇOES
    public function getInscricoes()
    {
        $data = Input::all();
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }

        $inscricoes = LogReservaSpm::select('spm_reserva_log.idaluno', 'users.name')
                ->leftJoin('users', 'users.id', '=', 'spm_reserva_log.idaluno')
                ->where('spm_reserva_log.idspm', $data['id'])
                ->get();
        return $inscricoes;
    }

    public function adicionaCliente()
    {
        $data = Input::all();
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }

        $idaula = $data['spmId'];
        $idaluno = $data['clienteId'];

        $reserva = ReservaSpm::select('n_reservas')->where('idspm', $idaula)->whereRaw('Date(created_at) = CURDATE()')->first();
        if (count($reserva) > 0) {
            if ($reserva->n_reservas > 0) {
                $reservado = LogReservaSpm::where('idaluno', $idaluno)->where('idspm', $idaula)->first();
                if (count($reservado) > 0) {
                    $retorno['text'] = 'Cliente já esta adicionado nesta aula!';
                } else {
                    if (ReservaSpm::where('idspm', $idaula)->update(['n_reservas' => $reserva->n_reservas - 1])) {
                        LogReservaSpm::create(['idaluno' => $idaluno, 'idspm' => $idaula]);
                        $retorno['text'] = 'Cliente adicionado com sucesso!';
                    } else {
                        $retorno['text'] = 'Ocorreu um erro ao adicionar cliente, tente novamente!';
                    }
                }
            } else {
                $retorno['text'] = 'Desculpe, mas infelizmente não há mais vagas para esta aula!';
            }
        } else {
            $spm = Spm::select('spm.id', 'spm.idunidade', 'spm.capacidade_max')->where('spm.id', $idaula)->first();
            if (ReservaSpm::create(['idunidade' => $spm->idunidade, 'idspm' => $spm->id, 'n_reservas' => $spm->capacidade_max - 1])) {
                LogReservaSpm::create(['idaluno' => $idaluno, 'idspm' => $idaula]);
                $retorno['text'] = 'Cliente adicionado com sucesso!';
            } else {
                $retorno['text'] = 'Ocorreu um erro ao adicionar cliente, tente novamente!';
            }
        }
        return $retorno;
    }

    public function reservaDelSpm()
    {
        $data = Input::all();
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }

        $idaula = $data['spmId'];
        $idaluno = $data['clienteId'];

        $reserva = ReservaSpm::select('n_reservas')->where('idspm', $idaula)->first();
        if (ReservaSpm::where('idspm', $idaula)->update(['n_reservas' => $reserva->n_reservas + 1])) {
            LogReservaSpm::where('idaluno', $idaluno)->where('idspm', $idaula)->delete();
            $retorno['text'] = 'Cliente removido com sucesso!';
        } else {
            $retorno['text'] = 'Ocorreu um erro ao cancelar aula, tente novamente!';
        }
        return $retorno;
    }
}
