<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Input;
use Request;
use App\Models\Gfm;
use App\Models\Programa;
use App\Models\Local;
use App\Models\User;
use App\Models\Funcionario;
use App\Models\Reserva;
use App\Models\Gfmconfigura;
use App\Models\LogReserva;
use App\Models\Matriculaturma;
use App\Models\Nivelgfm;
use App\Models\Movatividade;


use App\Services\TrataDadosService;
// Precisa para funcionar o combo
use Auth;
use App\Services\FilialService;

class GfmController extends Controller
{
    public function __construct(TrataDadosService $trataDadosService, FilialService $filialService)
    {
        $this->middleware('auth');
        $this->trataDadosService = $trataDadosService;
        $this->filialService = $filialService;
    }

    public function index()
    {
        $headers = ['category' => 'GFM', 'title' => 'GFM'];
        $unidades_combo = $this->filialService->unidadesComboTop();
        return view('admin.gfms.index', compact('headers', 'unidades_combo'));
    }

    public function getProgramas()
    {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        $programas = Programa::select('id', 'nmprograma')->where('idunidade', $idunidade)->get();
        foreach ($programas as $key => $value) {
            $programas[$key]['st']=1;
        }        
        
        return $programas;
    }

    public function getLocais()
    {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        $locais = Local::select('id', 'nmlocal')->where('idunidade', $idunidade)->get();
        return $locais;
    }

    

    
    public function getAtingido($idprofessor)
    {
        
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }        
        $totalAtingido = 0;
        $gfms = Gfm::select('id', 'capacidade')->where('idfuncionario', $idprofessor)->get();
        $contGfms = count($gfms);
        foreach ($gfms as $gfm) {
            $reservas = Reserva::where('idgfm', $gfm->id)->get();
            $contReservas = count($reservas);
            $total = 0;
            foreach ($reservas as $reserva) {
                $total += $reserva->n_reservas;
            }
            if (isset($total) && $total != 0) {
                $gfm->media = $total / $contReservas;
                
                $gfm->atingido = round(($gfm->media * 100) / $gfm->capacidade);
            } else {
                $gfm->media = 0;
                $gfm->atingido = 0;
            }
            $totalAtingido += $gfm->atingido;
        }
        return $totalAtingido;
    }
    
    public function reservaAddPainel($idaula) {
        
        $data = Input::all();
        
        
 
       
        $dtHj = date('Y-m-d');
        $ano = substr($dtHj, 0, 4);
        $mes = substr($dtHj, 5, 2);
        $dia = substr($dtHj, 8, 2);
        $ds = date("w", mktime($ano, $dia, $mes));
        $diasemana = $ds + 1;

        
        $gfm = Gfm::select('gfm.id', 'gfm.idunidade', 'gfm.capacidade', 'gfm.diasemana')->where('gfm.id', $idaula)->first();
        $diasx = explode(",", $gfm->diasemana);
        
        foreach ($diasx as $key => $value) {
            if ($diasx[$key] < $diasemana) {
                $dif = ($diasx[$key] - $diasemana) + 7;
            }
            if ($diasx[$key] > $diasemana) {
                $dif = ($diasx[$key] - $diasemana);
            }
            if ($diasx[$key] == $diasemana) {
                if (date('H:i:s') <= date('H:i:s', strtotime($gfm->hora_inicio))) {
                    $dif = ($diasx[$key] - $diasemana);
                } else {
                    $dif = ($diasx[$key] - $diasemana) + 7;
                }
            }

            $dt = date('Y-m-d', strtotime("+" . $dif . " days"));

            $datas[$key] = $dt;
           
        }        
        

        sort($datas);
        
        $dtAula = $datas[0];//$this->getDataAula($id, $diasemana);

        
        
        
        
        
       /* foreach ($data as $key => $values):
            $idaluno = $data[$key]['id'];
            $registro = $data[$key]['registro'];
            
            $unidade = User::select('idunidade')->where('id', $idaluno)->first();
            $dtHj=date('Y-m-d H:i:s');
            $reservado = Logreserva::select('idaluno')->where('idgfm', $idaula)->where('id', $registro)->where('idaluno','=', $idaluno)->first();
            if (sizeOf($reservado)>0):
                Logreserva::where('id', $registro)->update(['idaluno' => $idaluno]);
                
            endif;
            
        endforeach;    */
        
        foreach ($data as $key => $values):
            $idaluno = $data[$key]['id'];
            //$registro = $data[$key]['registro'];
            
            $unidade = User::select('idunidade')->where('id', $idaluno)->first();
            $dtHj=date('Y-m-d H:i:s');
            $reservado = Logreserva::select('idaluno')->where('idgfm', $idaula)->where('idaluno', $idaluno)->where('idaluno','>', 0)->where('dtaula', $dtAula)->first();
            $total = DB::select($sql = "select coalesce(count(*),0) as total from gfm_reserva_log where idgfm = " . $idaula . " and dtaula = '" . $dtAula . "'");



            if (sizeof($reservado) <= 0) {
                $reserva = Reserva::select('n_reservas', 'id', 'capacidade')->where('idgfm', $idaula)->where('dtaula', $dtAula)->first();

                if (count($reserva) > 0) {
                    if ($reserva->n_reservas > 0) {
                        //$gfm = Gfm::select('gfm.capacidade')->where('gfm.id', $idaula)->first();

                        $resto = intval($reserva->capacidade) - intval($total[0]->total) - 1; //--calcula capacidade menos total reservado
                        if (Reserva::where('idgfm', $idaula)->update(['n_reservas' => $resto])) {
                            
                            
                            
                            
                            $logres = LogReserva::create(['idaluno' => $idaluno, 'idgfm' => $idaula, 'reserva_id' => $reserva->id, 'dtaula' => $dtAula]);
                            $retorno['cod'] = '0';
                            $retorno['text'] = 'Aula reservada com sucesso!';

                            if ($idaluno > 0):
                                //insere movatividade
                                Movatividade::create(['idaluno' => $idaluno, 'idunidade' => $unidade->idunidade,
                                    'idatividade' => 2, 'qtatividade' => 1, 'dtregistro' => $dtHj,
                                    'stmovatividade' => 'F', 'vinculo_id' => $logres->id
                                ]);

                            endif;


                            $retorno['cod'] = '0';
                            $retorno['text'] = 'Aula reservada com sucesso!' . $logres->id;
                        } else {
                            $retorno['cod'] = '-1';
                            $retorno['text'] = 'Ocorreu um erro ao reservar aula, tente novamente!';
                        }
                    } else {
                        $retorno['cod'] = '-1';
                        $retorno['text'] = 'Desculpe, mas infelizmente não há mais vagas para esta aula!';
                    }
                } else {
                    $gfm = Gfm::select('gfm.id', 'gfm.idunidade', 'gfm.capacidade', 'gfm.diasemana')->where('gfm.id', $idaula)->first();
                    $dias = explode(",", $gfm->diasemana);

                    $dtHj = date('Y-m-d');
                    $ano = substr($dtHj, 0, 4);
                    $mes = substr($dtHj, 5, 2);
                    $dia = substr($dtHj, 8, 2);
                    $ds = date("w", mktime($ano, $dia, $mes));
                    $diasemana = $ds + 1;
                    $temAula = "N";
                    foreach ($dias as $key => $value) {
                        if ($dias[$key] == $diasemana):
                            $temAula = "S";
                        endif;
                    }
                    //if ($temAula == "S") { trava p n reservar ffora do dia
                    $dataReserva['idunidade'] = $gfm->idunidade;
                    $dataReserva['idgfm'] = $gfm->id;
                    $dataReserva['n_reservas'] = $gfm->capacidade - 1;
                    $dataReserva['capacidade'] = $gfm->capacidade;
                    $dataReserva['dtaula'] = $dtAula;
                    $reservaNew = Reserva::create($dataReserva);
                    if (sizeof($reservaNew)) {
                        LogReserva::create(['idaluno' => $idaluno, 'idgfm' => $idaula, 'dtaula' => $dtAula, 'reserva_id' => $reservaNew->id]);
                        $retorno['cod'] = '0';
                        $retorno['text'] = 'Aula reservada com sucesso!';
                    } else {
                        $retorno['cod'] = '-1';
                        $retorno['text'] = 'Ocorreu um erro ao reservar aula, tente novamente!';
                    }
                }
            } else {
                $retorno['cod'] = '-1';
                $retorno['text'] = 'Aula já reservada para esse aluno!';
            }
        endforeach;
        return $retorno;
    }
    

    public function getProfessores()
    {
        $idunidade = Auth::user()->idunidade;
        
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }


        $professores = User::select('users.id', 'users.name')
             ->JOIN('user_dados', 'users.id', '=', 'user_dados.user_id')
            ->where('users.idunidade', $idunidade)
            ->wherein('user_dados.situacao', 'A')
            ->wherein('users.role', ['professor','admin','adminfilial','coordenador'])
            ->groupby('users.id')
            ->get();
        
        
        
         foreach ($professores as $key => $professor) {  
             $professores[$key]['total']=$this->getNumeroAlunosProf($professores[$key]['id']);
         }
        
        // foreach ($professores as $key => $professor) {
        //     $professores[$key]->name = ucwords(strtolower($professor->name));
            
        //     $gfms = Gfm::where('idfuncionario', $professor->id)->get();
        //     $professor->totalAulas = count($gfms);
        //     if (isset($professor->totalAulas) && $professor->totalAulas != 0) {
        //         $professor->atingido = number_format( $this->getAtingido($professor->id) / $professor->totalAulas, 2,'.',',');
        //     } else {
        //         $professor->atingido = 0;
        //     }
        // }

        
        return $professores;

    }
    
    
    public function getGfmsGraficoProfs()
    {
       /* $idunidade = Auth::user()->idunidade;
        
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        $reservasArray = [0, 0, 0, 0, 0, 0, 0];

        $professores = User::select('users.name')
            ->JOIN('gfm', 'users.id', '=', 'gfm.idfuncionario')
            ->where('users.idunidade', $idunidade)
            ->groupby('users.id')
            ->get();
        
         foreach ($professores as $key => $value) {  
             $profArray[$key]=$professores[$key]['name'];
         }
        return $profArray;*/
        
        $idunidade = Auth::user()->idunidade;
        
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        $profArray = [];
        $final=date('Y-m-d');
        $inicial = date('Y-m-d', strtotime('-48 days'));
        $sql = "select SUM(gfm_reserva.capacidade - gfm_reserva.n_reservas ) as total, gfm.idfuncionario from gfm_reserva left join gfm on gfm.id = gfm_reserva.idgfm where  gfm.gfmspm =  'Gfm' and  gfm.idunidade = $idunidade and gfm_reserva.dtaula between '$inicial' and '$final' group by gfm.idfuncionario"; 
        $professores = DB::select($sql);

        
         foreach ($professores as $key => $value) {  
             $profArray[$key]=  intVal($professores[$key]->total);
         }
        return $profArray;        

    }    
    public function getGfmsGraficoProfs2()
    {
        $idunidade = Auth::user()->idunidade;
        
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        $reservasArray = [0, 0, 0, 0, 0, 0, 0];

        $capacidade = "select SUM(gfm_reserva.capacidade - gfm_reserva.n_reservas ) as capacidade, gfm.idfuncionario from gfm_reserva left join gfm on gfm.id = gfm_reserva.idgfm where  gfm.gfmspm =  'Gfm' and  gfm.idunidade = $idunidade and gfm_reserva.dtaula between '$inicial' and '$final' group by gfm.idfuncionario"; 
        $capacidade = DB::select($capacidade);
        if ($capacidade[0]->capacidade != null) {
            $reservasArray[4] = (int)$capacidade[0]->capacidade;
        }
        
         foreach ($professores as $key => $value) {  
             $profArray[$key]=$professores[$key]['name'];
         }
        return $profArray;

    }    
    public function getNumeroAlunosProf($id)
    {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        
        $sql = "SELECT count(t.treino_id) as tt FROM treino_musculacao t WHERE t.treino_atual = 1 "
                . " and t.professor_id = " . $id . " and t.unidade_id = " . $idunidade;
        
        $total=DB::select($sql);
        
        $tot=$total[0]->tt;
        
        return $tot;
    }        

    public function getProfessor()
    {
        $data = Input::all();
        $professor = Funcionario::select('name')->where('id', $data['id'])->get();
        return ucwords(strtolower($professor[0]['name']));
    }
        
    public function getAula($id)
    {
        $aula= Programa::select('programa.*')->where('id', $id)->get();
        return response()->json(compact('aula'));
    }
        
    public function getListareserva($id)
    {
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        $reservas = Reserva::select('gfm_reserva.*', 'users.name', 'users.id','gfm_reserva_log.id as idregistro')
                            ->leftJOIN('gfm_reserva_log', 'gfm_reserva_log.idgfm', '=', 'gfm_reserva.idgfm')
                            ->leftJOIN('users', 'users.id', '=', 'gfm_reserva_log.idaluno')
                            ->where('gfm_reserva.idunidade', '=', $idunidade)
                            ->where('gfm_reserva.id', '=', $id)
                            ->orderby('gfm_reserva_log.created_at')
                            ->get();
                
        foreach ($reservas  as $key => $value) {
            if ($reservas[$key]->name==''):
                        $reservas[$key]->name= "ANÔNIMO";
            endif;
        }
                
        return response()->json(compact('reservas'));
    }

    public function getHora($Hora)
    {
        if ($Hora != null):
            $hora = date("H:i", strtotime($Hora));
        return $hora;
        endif;
    }

    public function getGfmsGrafico1()
    {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }

        $reservasArray = [0, 0, 0, 0, 0, 0, 0];

        // Semana Atual
        $inicial = date('Y-m-d', strtotime('-6 days'));
        $final = date('Y-m-d');
        
        
//        $capacidade = "select SUM(gfm_reserva.capacidade - gfm_reserva.n_reservas ) as capacidade from gfm_reserva left join gfm on gfm.id = gfm_reserva.idgfm where gfm.gfmspm =  'Gfm' and gfm.idunidade = $idunidade and gfm_reserva.dtaula between '$inicial' and '$final'";
        $capacidade = "SELECT count(*) as capacidade FROM gfm_reserva gr , gfm_reserva_log grl, gfm g WHERE gr.idgfm = g.id and gr.idgfm = grl.idgfm and gr.dtaula = grl.dtaula and gr.idunidade = $idunidade and gr.dtaula between '$inicial' and '$final' and g.gfmspm = 'Gfm'";
        $capacidade = DB::select($capacidade);
        if ($capacidade[0]->capacidade != null) {
            $reservasArray[6] = (int)$capacidade[0]->capacidade;
        }
     
        // -1 Sem
        $final = date('Y-m-d', strtotime("-1 days", strtotime($inicial)));
        $inicial = date('Y-m-d', strtotime("-6 days", strtotime($final)));
        $capacidade = "SELECT count(*) as capacidade FROM gfm_reserva gr , gfm_reserva_log grl, gfm g WHERE gr.idgfm = g.id and gr.idgfm = grl.idgfm and gr.dtaula = grl.dtaula and gr.idunidade = $idunidade and gr.dtaula between '$inicial' and '$final' and g.gfmspm = 'Gfm'";

        $capacidade = DB::select($capacidade);
        if ($capacidade[0]->capacidade != null) {
            $reservasArray[5] = (int)$capacidade[0]->capacidade;
        }

        
        // -2 Sem
        $final = date('Y-m-d', strtotime("-1 days", strtotime($inicial)));
        $inicial = date('Y-m-d', strtotime("-6 days", strtotime($final)));
        $capacidade = "SELECT count(*) as capacidade FROM gfm_reserva gr , gfm_reserva_log grl, gfm g WHERE gr.idgfm = g.id and gr.idgfm = grl.idgfm and gr.dtaula = grl.dtaula and gr.idunidade = $idunidade and gr.dtaula between '$inicial' and '$final' and g.gfmspm = 'Gfm'";
        $capacidade = DB::select($capacidade);
        if ($capacidade[0]->capacidade != null) {
            $reservasArray[4] = (int)$capacidade[0]->capacidade;
        }

        // -3 Sem
        $final = date('Y-m-d', strtotime("-1 days", strtotime($inicial)));
        $inicial = date('Y-m-d', strtotime("-6 days", strtotime($final)));
        $capacidade = "SELECT count(*) as capacidade FROM gfm_reserva gr , gfm_reserva_log grl, gfm g WHERE gr.idgfm = g.id and gr.idgfm = grl.idgfm and gr.dtaula = grl.dtaula and gr.idunidade = $idunidade and gr.dtaula between '$inicial' and '$final' and g.gfmspm = 'Gfm'";
        $capacidade = DB::select($capacidade);
        if ($capacidade[0]->capacidade != null) {
            $reservasArray[3] = (int)$capacidade[0]->capacidade;
        }

        // -4 Sem
        $final = date('Y-m-d', strtotime("-1 days", strtotime($inicial)));
        $inicial = date('Y-m-d', strtotime("-6 days", strtotime($final)));
        $capacidade = "SELECT count(*) as capacidade FROM gfm_reserva gr , gfm_reserva_log grl, gfm g WHERE gr.idgfm = g.id and gr.idgfm = grl.idgfm and gr.dtaula = grl.dtaula and gr.idunidade = $idunidade and gr.dtaula between '$inicial' and '$final' and g.gfmspm = 'Gfm'";
        $capacidade = DB::select($capacidade);
        if ($capacidade[0]->capacidade != null) {
            $reservasArray[2] = (int)$capacidade[0]->capacidade;
        }

        // -5 Sem
        $final = date('Y-m-d', strtotime("-1 days", strtotime($inicial)));
        $inicial = date('Y-m-d', strtotime("-6 days", strtotime($final)));
        $capacidade = "SELECT count(*) as capacidade FROM gfm_reserva gr , gfm_reserva_log grl, gfm g WHERE gr.idgfm = g.id and gr.idgfm = grl.idgfm and gr.dtaula = grl.dtaula and gr.idunidade = $idunidade and gr.dtaula between '$inicial' and '$final' and g.gfmspm = 'Gfm'";
        $capacidade = DB::select($capacidade);
        if ($capacidade[0]->capacidade != null) {
            $reservasArray[1] = (int)$capacidade[0]->capacidade;
        }

        // -6 Sem
        $final = date('Y-m-d', strtotime("-1 days", strtotime($inicial)));
        $inicial = date('Y-m-d', strtotime("-6 days", strtotime($final)));
        $capacidade = "SELECT count(*) as capacidade FROM gfm_reserva gr , gfm_reserva_log grl, gfm g WHERE gr.idgfm = g.id and gr.idgfm = grl.idgfm and gr.dtaula = grl.dtaula and gr.idunidade = $idunidade and gr.dtaula between '$inicial' and '$final' and g.gfmspm = 'Gfm'";
        $capacidade = DB::select($capacidade);
        if ($capacidade[0]->capacidade != null) {
            $reservasArray[0] = (int)$capacidade[0]->capacidade;
        }
////////////////////////////////////////////////////////////////////////        
        $reservasArray1 = [0, 0, 0, 0, 0, 0, 0];

        // Semana Atual
        $inicial = date('Y-m-d', strtotime('-6 days'));
        $final = date('Y-m-d');
        $capacidade = "select SUM(gfm_reserva.capacidade) as capacidade from gfm left join gfm_reserva  on gfm.id = gfm_reserva.idgfm where  gfm.gfmspm =  'Gfm' and  gfm.idunidade = $idunidade and gfm_reserva.dtaula between '$inicial' and '$final'";
        $capacidade = DB::select($capacidade);

        if ($capacidade[0]->capacidade != null) {
            $reservasArray1[6] = (int)$capacidade[0]->capacidade;
        }

        
        // -1 Sem
        $final = date('Y-m-d', strtotime("-1 days", strtotime($inicial)));
        $inicial = date('Y-m-d', strtotime("-6 days", strtotime($final)));
        $capacidade = "select SUM(gfm_reserva.capacidade) as capacidade from gfm left join gfm_reserva  on gfm.id = gfm_reserva.idgfm where  gfm.gfmspm =  'Gfm' and  gfm.idunidade = $idunidade and gfm_reserva.dtaula between '$inicial' and '$final'";
        $capacidade = DB::select($capacidade);
        if ($capacidade[0]->capacidade != null) {
            $reservasArray1[5] = (int)$capacidade[0]->capacidade;
        }

        // -2 Sem
        $final = date('Y-m-d', strtotime("-1 days", strtotime($inicial)));
        $inicial = date('Y-m-d', strtotime("-6 days", strtotime($final)));
        $capacidade = "select SUM(gfm_reserva.capacidade) as capacidade from gfm left join gfm_reserva  on gfm.id = gfm_reserva.idgfm where  gfm.gfmspm =  'Gfm' and  gfm.idunidade = $idunidade and gfm_reserva.dtaula between '$inicial' and '$final'";
        $capacidade = DB::select($capacidade);
        if ($capacidade[0]->capacidade != null) {
            $reservasArray1[4] = (int)$capacidade[0]->capacidade;
        }

        // -3 Sem
        $final = date('Y-m-d', strtotime("-1 days", strtotime($inicial)));
        $inicial = date('Y-m-d', strtotime("-6 days", strtotime($final)));
        $capacidade = "select SUM(gfm.capacidade) as capacidade from gfm_reserva left join gfm on gfm.id = gfm_reserva.idgfm where  gfm.gfmspm =  'Gfm' and  gfm.idunidade = $idunidade and gfm_reserva.dtaula between '$inicial' and '$final'";
        $capacidade = DB::select($capacidade);
        if ($capacidade[0]->capacidade != null) {
            $reservasArray1[3] = (int)$capacidade[0]->capacidade;
        }

        // -4 Sem
        $final = date('Y-m-d', strtotime("-1 days", strtotime($inicial)));
        $inicial = date('Y-m-d', strtotime("-6 days", strtotime($final)));
        $capacidade = "select SUM(gfm_reserva.capacidade) as capacidade from gfm left join gfm_reserva  on gfm.id = gfm_reserva.idgfm where  gfm.gfmspm =  'Gfm' and  gfm.idunidade = $idunidade and gfm_reserva.dtaula between '$inicial' and '$final'";
        $capacidade = DB::select($capacidade);
        if ($capacidade[0]->capacidade != null) {
            $reservasArray1[2] = (int)$capacidade[0]->capacidade;
        }

        // -5 Sem
        $final = date('Y-m-d', strtotime("-1 days", strtotime($inicial)));
        $inicial = date('Y-m-d', strtotime("-6 days", strtotime($final)));
        $capacidade = "select SUM(gfm_reserva.capacidade) as capacidade from gfm left join gfm_reserva on gfm.id = gfm_reserva.idgfm where  gfm.gfmspm =  'Gfm' and  gfm.idunidade = $idunidade and gfm_reserva.dtaula between '$inicial' and '$final'";
        $capacidade = DB::select($capacidade);
        if ($capacidade[0]->capacidade != null) {
            $reservasArray1[1] = (int)$capacidade[0]->capacidade;
        }

        // -6 Sem
        $final = date('Y-m-d', strtotime("-1 days", strtotime($inicial)));
        $inicial = date('Y-m-d', strtotime("-6 days", strtotime($final)));
        $capacidade = "select SUM(gfm_reserva.capacidade) as capacidade from gfm left join gfm_reserva  on gfm.id = gfm_reserva.idgfm where  gfm.gfmspm =  'Gfm' and  gfm.idunidade = $idunidade and gfm_reserva.dtaula between '$inicial' and '$final'";
        $capacidade = DB::select($capacidade);
        if ($capacidade[0]->capacidade != null) {
            $reservasArray1[0] = (int)$capacidade[0]->capacidade;
        }

        
            //response()->json(compact('exercicios', 'gruposmusculares'));
        return compact('reservasArray', 'reservasArray1');
    }

    public function getGfmsGrafico2()
    {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }

        $reservasArray = [0, 0, 0, 0, 0, 0, 0];

        // Semana Atual
        $inicial = date('Y-m-d', strtotime('-6 days'));
        $final = date('Y-m-d');
        $capacidade = "select SUM(gfm_reserva.capacidade) as capacidade from gfm left join gfm_reserva  on gfm.id = gfm_reserva.idgfm where  gfm.gfmspm =  'Gfm' and  gfm.idunidade = $idunidade and gfm_reserva.dtaula between '$inicial' and '$final'";
        $capacidade = DB::select($capacidade);

        if ($capacidade[0]->capacidade != null) {
            $reservasArray[6] = (int)$capacidade[0]->capacidade;
        }

        
        // -1 Sem
        $final = date('Y-m-d', strtotime("-1 days", strtotime($inicial)));
        $inicial = date('Y-m-d', strtotime("-6 days", strtotime($final)));
        $capacidade = "select SUM(gfm_reserva.capacidade) as capacidade from gfm left join gfm_reserva  on gfm.id = gfm_reserva.idgfm where  gfm.gfmspm =  'Gfm' and  gfm.idunidade = $idunidade and gfm_reserva.dtaula between '$inicial' and '$final'";
        $capacidade = DB::select($capacidade);
        if ($capacidade[0]->capacidade != null) {
            $reservasArray[5] = (int)$capacidade[0]->capacidade;
        }

        // -2 Sem
        $final = date('Y-m-d', strtotime("-1 days", strtotime($inicial)));
        $inicial = date('Y-m-d', strtotime("-6 days", strtotime($final)));
        $capacidade = "select SUM(gfm_reserva.capacidade) as capacidade from gfm left join gfm_reserva  on gfm.id = gfm_reserva.idgfm where  gfm.gfmspm =  'Gfm' and  gfm.idunidade = $idunidade and gfm_reserva.dtaula between '$inicial' and '$final'";
        $capacidade = DB::select($capacidade);
        if ($capacidade[0]->capacidade != null) {
            $reservasArray[4] = (int)$capacidade[0]->capacidade;
        }

        // -3 Sem
        $final = date('Y-m-d', strtotime("-1 days", strtotime($inicial)));
        $inicial = date('Y-m-d', strtotime("-6 days", strtotime($final)));
        $capacidade = "select SUM(gfm.capacidade) as capacidade from gfm_reserva left join gfm on gfm.id = gfm_reserva.idgfm where  gfm.gfmspm =  'Gfm' and  gfm.idunidade = $idunidade and gfm_reserva.dtaula between '$inicial' and '$final'";
        $capacidade = DB::select($capacidade);
        if ($capacidade[0]->capacidade != null) {
            $reservasArray[3] = (int)$capacidade[0]->capacidade;
        }

        // -4 Sem
        $final = date('Y-m-d', strtotime("-1 days", strtotime($inicial)));
        $inicial = date('Y-m-d', strtotime("-6 days", strtotime($final)));
        $capacidade = "select SUM(gfm_reserva.capacidade) as capacidade from gfm left join gfm_reserva  on gfm.id = gfm_reserva.idgfm where  gfm.gfmspm =  'Gfm' and  gfm.idunidade = $idunidade and gfm_reserva.dtaula between '$inicial' and '$final'";
        $capacidade = DB::select($capacidade);
        if ($capacidade[0]->capacidade != null) {
            $reservasArray[2] = (int)$capacidade[0]->capacidade;
        }

        // -5 Sem
        $final = date('Y-m-d', strtotime("-1 days", strtotime($inicial)));
        $inicial = date('Y-m-d', strtotime("-6 days", strtotime($final)));
        $capacidade = "select SUM(gfm_reserva.capacidade) as capacidade from gfm left join gfm_reserva on gfm.id = gfm_reserva.idgfm where  gfm.gfmspm =  'Gfm' and  gfm.idunidade = $idunidade and gfm_reserva.dtaula between '$inicial' and '$final'";
        $capacidade = DB::select($capacidade);
        if ($capacidade[0]->capacidade != null) {
            $reservasArray[1] = (int)$capacidade[0]->capacidade;
        }

        // -6 Sem
        $final = date('Y-m-d', strtotime("-1 days", strtotime($inicial)));
        $inicial = date('Y-m-d', strtotime("-6 days", strtotime($final)));
        $capacidade = "select SUM(gfm_reserva.capacidade) as capacidade from gfm left join gfm_reserva  on gfm.id = gfm_reserva.idgfm where  gfm.gfmspm =  'Gfm' and  gfm.idunidade = $idunidade and gfm_reserva.dtaula between '$inicial' and '$final'";
        $capacidade = DB::select($capacidade);
        if ($capacidade[0]->capacidade != null) {
            $reservasArray[0] = (int)$capacidade[0]->capacidade;
        }

        return $reservasArray;
    }

    public function getGfms($tipo)
            
    {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        
        ///pega configurção gfm
        $configura = Gfmconfigura::select('gfm_configura.*')->where('idunidade', $idunidade)->get();         
        
        
        // AQUI TEM QUE PASSAR PERIODO
        $gfms = Gfm::select('gfm.*', 'users.name as nmprofessor', 'programa.nmprograma', 'local.nmlocal','nivelhabilidade.nmnivel','objetivotreino.nmobjetivo')
        ->leftJoin('users', 'users.id', '=', 'gfm.idfuncionario')
        ->leftJoin('programa', 'programa.id', '=', 'gfm.idprograma')
        ->leftJoin('local', 'local.id', '=', 'gfm.idlocal')
        ->leftJoin('objetivotreino', 'objetivotreino.id', '=', 'gfm.objetivo')
        ->leftJoin('nivelhabilidade', 'nivelhabilidade.id', '=', 'gfm.nivel')                
        ->where('gfm.gfmspm', $tipo)
        ->where('gfm.idunidade', $idunidade)
        ->orderBy('gfm.hora_inicio','asc')->get();
        
        foreach ($gfms as $gfm) {
            //$reservas = Reserva::select('n_reservas')->where('idgfm', $gfm->id)->get();
            $reservas = DB::select('select (capacidade - n_reservas) as n_reservas from gfm_reserva where idgfm = ' . $gfm->id );
            
            $vars=DB::select('select id, nmnivel from nivelhabilidade where idmodalidade = 3'
                                    . ' and idobjetivo = ' . $gfm->objetivo
                                    . ' and idunidade = ' . $gfm->idunidade
                                    );
            /* $gfm->niveis=DB::select('select * from nivel_gfm where idgfm = ' . $gfm->id
                                    . ' and idunidade = ' . $gfm->idunidade);*/
             
             //foreach ($gfm->niveisobj as $nivelobj) {
             $vars1=[];
             foreach ($vars as $key => $value) {
                 $vars1[$key]['id']=$vars[$key]->id;
                 $vars1[$key]['nmnivel']=$vars[$key]->nmnivel;
                 $vars1[$key]['cod']=$vars[$key]->id;
                 
                 $vars2= DB::select('select id from nivel_gfm where idgfm = ' . $gfm->id
                                    . ' and idunidade = ' . $gfm->idunidade
                                    . ' and idnivel = ' . $vars[$key]->id );
                 $vars1[$key]['marcado']=(sizeof($vars2)> 0) ? true : false;
             }
              $gfm->niveisobj=$vars1;
             
             
           // echo $reservas;
            $contReservas = count($reservas);
            $total = 0;
            foreach ($reservas as $reserva) {
                $total += $reserva->n_reservas;
            }
            if (isset($total) && $total != 0) {
                $gfm->media =  number_format( $total / $contReservas, 2,'.',',');
               // $gfm->atingido = round(($gfm->media * 100) / $gfm->capacidade);
                

                $gfm->revisado = $gfm->media * $gfm->fator;
            } else {
                $gfm->media = 0;
                $gfm->atingido = 0;
                $gfm->revisado = 0;
            }
            $gfm->atingido = Matriculaturma::all()->where('idaula',$gfm->id)->where('idunidade',$idunidade)->where('stmatricula', 'A')->count();
            $gfm->hora_inicio = $this->getHora($gfm->hora_inicio);
            $gfm->vacancia = $gfm->capacidade - $gfm->atingido;

            $gfm->diasemana = explode(',', $gfm->diasemana);
            $gfm->domingo = in_array("1", $gfm->diasemana) ? true : false;
            $gfm->segunda = in_array("2", $gfm->diasemana) ? true : false;
            $gfm->terca = in_array("3", $gfm->diasemana) ? true : false;
            $gfm->quarta = in_array("4", $gfm->diasemana) ? true : false;
            $gfm->quinta = in_array("5", $gfm->diasemana) ? true : false;
            $gfm->sexta = in_array("6", $gfm->diasemana) ? true : false;
            $gfm->sabado = in_array("7", $gfm->diasemana) ? true : false;            
            $faixaEtaria = DB::select("SELECT min(u.dt_nascimento) as inicial,max(u.dt_nascimento) as final FROM matricula_turma m,user_dados u
                                    where u.dt_nascimento <> '0000-00-00' and m.idaluno = u.user_id and idaula = " . $gfm->id);
            
            $gfm->faixaetariafim=$this->getIdade($faixaEtaria[0]->inicial);
            $gfm->faixaetariaini=$this->getIdade($faixaEtaria[0]->final);
            
        }
                    
           
        return response()->json(compact('gfms','configura'));            
        //return $gfms;
   }
   
    public function getGfmsPrograma($tipo, $programa){
        
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        
        ///pega configurção gfm
        $configura = Gfmconfigura::select('gfm_configura.*')->where('idunidade', $idunidade)->get();         
        
        
        // AQUI TEM QUE PASSAR PERIODO
        if ($programa !== '0'):
            $gfms = Gfm::select('gfm.*', 'users.name as nmprofessor', 'programa.nmprograma', 'local.nmlocal', 'nivelhabilidade.nmnivel', 'objetivotreino.nmobjetivo')
                            ->leftJoin('users', 'users.id', '=', 'gfm.idfuncionario')
                            ->leftJoin('programa', 'programa.id', '=', 'gfm.idprograma')
                            ->leftJoin('local', 'local.id', '=', 'gfm.idlocal')
                            ->leftJoin('objetivotreino', 'objetivotreino.id', '=', 'gfm.objetivo')
                            ->leftJoin('nivelhabilidade', 'nivelhabilidade.id', '=', 'gfm.nivel')
                            ->where('gfm.gfmspm', $tipo)
                            ->where('gfm.idprograma', '=', $programa )
                            ->where('gfm.idunidade', $idunidade)->get();
        else:
            $gfms = Gfm::select('gfm.*', 'users.name as nmprofessor', 'programa.nmprograma', 'local.nmlocal', 'nivelhabilidade.nmnivel', 'objetivotreino.nmobjetivo')
                            ->leftJoin('users', 'users.id', '=', 'gfm.idfuncionario')
                            ->leftJoin('programa', 'programa.id', '=', 'gfm.idprograma')
                            ->leftJoin('local', 'local.id', '=', 'gfm.idlocal')
                            ->leftJoin('objetivotreino', 'objetivotreino.id', '=', 'gfm.objetivo')
                            ->leftJoin('nivelhabilidade', 'nivelhabilidade.id', '=', 'gfm.nivel')
                            //->where('gfm.gfmspm', $tipo)
                            ->where('gfm.idunidade', $idunidade)->get();

        endif;
        foreach ($gfms as $gfm) {

            
            
            //$reservas = Reserva::select('n_reservas')->where('idgfm', $gfm->id)->get();
            $reservas = DB::select('select (capacidade - n_reservas) as n_reservas from gfm_reserva where idgfm = ' . $gfm->id );
            
            
             $vars=DB::select('select id, nmnivel from nivelhabilidade where idmodalidade = 3'
                                    . ' and idobjetivo = ' . $gfm->objetivo
                                    . ' and idunidade = ' . $gfm->idunidade
                                    );
            
             
            /* $gfm->niveis=DB::select('select * from nivel_gfm where idgfm = ' . $gfm->id
                                    . ' and idunidade = ' . $gfm->idunidade);*/
             
             //foreach ($gfm->niveisobj as $nivelobj) {
             $vars1=[];
             foreach ($vars as $key => $value) {
                 $vars1[$key]['id']=$vars[$key]->id;
                 $vars1[$key]['nmnivel']=$vars[$key]->nmnivel;
                 $vars1[$key]['cod']=$vars[$key]->id;
                 
                 $vars2= DB::select('select id from nivel_gfm where idgfm = ' . $gfm->id
                                    . ' and idunidade = ' . $gfm->idunidade
                                    . ' and idnivel = ' . $vars[$key]->id );
                 $vars1[$key]['marcado']=(sizeof($vars2)> 0) ? true : false;
             }
              $gfm->niveisobj=$vars1;
             
             
           // echo $reservas;
            $contReservas = count($reservas);
            $total = 0;
            foreach ($reservas as $reserva) {
                $total += $reserva->n_reservas;
            }
            if (isset($total) && $total != 0) {
                $gfm->media =  number_format( $total / $contReservas, 2,'.',',');
               // $gfm->atingido = round(($gfm->media * 100) / $gfm->capacidade);
                

                $gfm->revisado = $gfm->media * $gfm->fator;
            } else {
                $gfm->media = 0;
                $gfm->atingido = 0;
                $gfm->revisado = 0;
            }
            $gfm->atingido = Matriculaturma::all()->where('idaula',$gfm->id)->where('idunidade',$idunidade)->where('stmatricula', 'A')->count();
            $gfm->hora_inicio = $this->getHora($gfm->hora_inicio);
            $gfm->vacancia = $gfm->capacidade - $gfm->atingido;

            $gfm->diasemana = explode(',', $gfm->diasemana);
            $gfm->domingo = in_array("1", $gfm->diasemana) ? true : false;
            $gfm->segunda = in_array("2", $gfm->diasemana) ? true : false;
            $gfm->terca = in_array("3", $gfm->diasemana) ? true : false;
            $gfm->quarta = in_array("4", $gfm->diasemana) ? true : false;
            $gfm->quinta = in_array("5", $gfm->diasemana) ? true : false;
            $gfm->sexta = in_array("6", $gfm->diasemana) ? true : false;
            $gfm->sabado = in_array("7", $gfm->diasemana) ? true : false;     
            
            $faixaEtaria = DB::select("SELECT min(u.dt_nascimento) as inicial,max(u.dt_nascimento) as final FROM matricula_turma m,user_dados u
                                    where u.dt_nascimento <> '0000-00-00' and m.idaluno = u.user_id and idaula = " . $gfm->id);
            
            $gfm->faixaetariafim=$this->getIdade($faixaEtaria[0]->inicial);
            $gfm->faixaetariaini=$this->getIdade($faixaEtaria[0]->final);
            
        }
                    
           
        return response()->json(compact('gfms','configura'));            
        //return $gfms;
        
        
    }
   
    
    public function getGfmsDiasemana($tipo){
        
        $data = Input::all();
        
        $diasemana=$data['diasemana'];
        $programa=$data['programa'];
        $professor=$data['professor'];
        $idadeIni=$data['idadeinicial'];
        $idadeFim=$data['idadefinal'];

        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        
        $nascini=$this->getDataNasc($idadeFim);
        $nascfim=$this->getDataNasc($idadeIni);
            
        $dtini=substr( $nascini, 0, 4) . "-" . substr( $nascini, 4, 2) . "-" . substr( $nascini, 6, 2);
        $dtfim=substr( $nascfim, 0, 4) . "-" . substr( $nascfim, 4, 2) . "-" . substr( $nascfim, 6, 2);
        
        $sql1="SELECT distinct(m.idaula) as aula FROM matricula_turma m,user_dados u
                                 where m.idunidade = $idunidade and  u.dt_nascimento between '$dtini' and '$dtfim'";
        

        $aulas= DB::select($sql1);
        
        
     
        

        $pf="";
        foreach($aulas as $key=>$value){
            $pf .= $aulas[$key]->aula;
            if ($key< sizeof($aulas)-1):
                $pf .=",";
            endif;
        }
        ///pega configurção gfm
        $configura = Gfmconfigura::select('gfm_configura.*')->where('idunidade', $idunidade)->get();         
        // AQUI TEM QUE PASSAR PERIODO
        $sql = "select gfm.*, users.name as nmprofessor, programa.nmprograma, local.nmlocal, nivelhabilidade.nmnivel, objetivotreino.nmobjetivo from gfm left join users on users.id = gfm.idfuncionario";
        $sql .=" left join  programa on programa.id = gfm.idprograma ";
        $sql .= " left join local on local.id = gfm.idlocal ";
        $sql .= " left Join  objetivotreino on objetivotreino.id = gfm.objetivo";
        $sql .= " left Join nivelhabilidade on nivelhabilidade.id = gfm.nivel";
        $sql .= " where gfm.gfmspm =  '$tipo'";
        $sql .= " and gfm.idunidade =  $idunidade";
        if ($diasemana !=='T'):
            $sql .= " and gfm.diasemana LIKE '%$diasemana%'"                ;
        endif;
        if ($programa !==0):
            $sql .= " and gfm.idprograma = $programa"                ;
        endif;
        if ($professor !==0):
            $sql .= " and gfm.idfuncionario = $professor"                ;
        endif;
        $sql .= " and gfm.id in($pf)"                ;
        
        $gfms=DB::select($sql);
        foreach ($gfms as $gfm) {

            
            
            //$reservas = Reserva::select('n_reservas')->where('idgfm', $gfm->id)->get();
            $reservas = DB::select('select (capacidade - n_reservas) as n_reservas from gfm_reserva where idgfm = ' . $gfm->id );
            
            
             $vars=DB::select('select id, nmnivel from nivelhabilidade where idmodalidade = 3'
                                    . ' and idobjetivo = ' . $gfm->objetivo
                                    . ' and idunidade = ' . $gfm->idunidade
                                    );
            
             
            /* $gfm->niveis=DB::select('select * from nivel_gfm where idgfm = ' . $gfm->id
                                    . ' and idunidade = ' . $gfm->idunidade);*/
             
             //foreach ($gfm->niveisobj as $nivelobj) {
             $vars1=[];
             foreach ($vars as $key => $value) {
                 $vars1[$key]['id']=$vars[$key]->id;
                 $vars1[$key]['nmnivel']=$vars[$key]->nmnivel;
                 $vars1[$key]['cod']=$vars[$key]->id;
                 
                 $vars2= DB::select('select id from nivel_gfm where idgfm = ' . $gfm->id
                                    . ' and idunidade = ' . $gfm->idunidade
                                    . ' and idnivel = ' . $vars[$key]->id );
                 $vars1[$key]['marcado']=(sizeof($vars2)> 0) ? true : false;
             }
              $gfm->niveisobj=$vars1;
             
             
           // echo $reservas;
            $contReservas = count($reservas);
            $total = 0;
            foreach ($reservas as $reserva) {
                $total += $reserva->n_reservas;
            }
            if (isset($total) && $total != 0) {
                $gfm->media =  number_format( $total / $contReservas, 2,'.',',');
               // $gfm->atingido = round(($gfm->media * 100) / $gfm->capacidade);
                

                $gfm->revisado = $gfm->media * $gfm->fator;
            } else {
                $gfm->media = 0;
                $gfm->atingido = 0;
                $gfm->revisado = 0;
            }
            $gfm->atingido = Matriculaturma::all()->where('idaula',$gfm->id)->where('idunidade',$idunidade)->where('stmatricula', 'A')->count();
            $gfm->hora_inicio = $this->getHora($gfm->hora_inicio);
            $gfm->vacancia = $gfm->capacidade - $gfm->atingido;

            $gfm->diasemana = explode(',', $gfm->diasemana);
            $gfm->domingo = in_array("1", $gfm->diasemana) ? true : false;
            $gfm->segunda = in_array("2", $gfm->diasemana) ? true : false;
            $gfm->terca = in_array("3", $gfm->diasemana) ? true : false;
            $gfm->quarta = in_array("4", $gfm->diasemana) ? true : false;
            $gfm->quinta = in_array("5", $gfm->diasemana) ? true : false;
            $gfm->sexta = in_array("6", $gfm->diasemana) ? true : false;
            $gfm->sabado = in_array("7", $gfm->diasemana) ? true : false;            
            $faixaEtaria = DB::select("SELECT min(u.dt_nascimento) as inicial,max(u.dt_nascimento) as final FROM matricula_turma m,user_dados u
                                    where u.dt_nascimento <> '0000-00-00' and m.idaluno = u.user_id and idaula = " . $gfm->id);
            
            $gfm->faixaetariafim=$this->getIdade($faixaEtaria[0]->inicial);
            $gfm->faixaetariaini=$this->getIdade($faixaEtaria[0]->final);
            
        }
                    
           
        return response()->json(compact('gfms','configura','data','xc'));            
        //return $gfms;
    }
    
    
    public function getIdade($Data) {
        // Declara a data! :P
        // $data = '29/08/2008';
        if ($Data != null):
            // Separa em dia, mês e ano
            list($ano, $mes, $dia) = explode('-', $Data);

            // Descobre que dia é hoje e retorna a unix timestamp
            $hoje = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
            // Descobre a unix timestamp da data de nascimento do fulano
            $nascimento = mktime(0, 0, 0, $mes, $dia, $ano);

            // Depois apenas fazemos o cálculo já citado :)
            $idade = floor((((($hoje - $nascimento) / 60) / 60) / 24) / 365.25);
            return $idade;
        else:
            return 0;

        endif;
        
    }
    
    public function getDataNasc($idade) {
        // Declara a data! :P
        // $data = '29/08/2008';
        
        $idadeX=isset($idade) ? $idade : 1;
        
        $x=$idade * 10000;
        
        $dtHj=date('Ymd');
        
        $dtNasc=$dtHj - $x;
        
                
        return $dtNasc;
        
    }    
    
    public function getTaxaOcupacaoProfs()
    {
        
        
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        
        $sql = "select SUM(gfm_reserva.capacidade) cap,SUM(gfm_reserva.capacidade - gfm_reserva.n_reservas) AS reservas, gfm.idfuncionario, u.name from gfm_reserva left join gfm on gfm.id = gfm_reserva.idgfm left join users u on u.id = gfm.idfuncionario where  gfm.gfmspm =  'Gfm' and  gfm.idunidade = $idunidade group by gfm.idfuncionario"; 
        $reservas = DB::select($sql);
        foreach ($reservas as $key => $value) {
            if ($reservas[$key]->cap>0):
                $reservas[$key]->perc= number_format(($reservas[$key]->reservas * 100) / $reservas[$key]->cap, 2, ',','.') ;
            endif;
            
        }        

            
  
        
        // AQUI TEM QUE PASSAR PERIODO
        /*$reservas = Reserva::select('gfm.idfuncionario','gfm_reserva.capacidade', 'n_reservas')
                            ->leftjoin('gfm','gfm.id','gfm_reserva.idgfm')
                            ->where('gfm_reserva.idunidade', $idunidade)
                            ->groupBy('gfm.idfuncionario')
                            ->get();
        $contReservas = count($reservas);
        $totalReservas = 0;
        $totalCapacidade = 0;
        foreach ($reservas as $reserva) {
            $totalReservas += ( $reserva->capacidade - $reserva->n_reservas);
            $totalCapacidade += $reserva->capacidade;
        }
        // AQUI TEM QUE PASSAR PERIODO
        /*$gfms = Reserva::select('capacidade')->where('idunidade', $idunidade)->get();
        
        foreach ($gfms as $gfm) {
            $totalCapacidade += $gfm->capacidade;
        }*/
/*        $media = ($totalReservas * 100) / $totalCapacidade;
        $taxaOcupacao = [
            'totalReservas' => $totalReservas,
            'totalCapacidade' => $totalCapacidade,
            'media' => round($media)
        ];*/
        
        return $reservas;
        
        
    }
    public function getTaxaOcupacao()
    {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        // AQUI TEM QUE PASSAR PERIODO
        $reservas = Reserva::select('capacidade', 'n_reservas')->where('idunidade', $idunidade)->get();
        $contReservas = count($reservas);
        $totalReservas = 0;
        $totalCapacidade = 0;
        foreach ($reservas as $reserva) {
            $totalReservas += ( $reserva->capacidade - $reserva->n_reservas);
            $totalCapacidade += $reserva->capacidade;
        }
        // AQUI TEM QUE PASSAR PERIODO
        /*$gfms = Reserva::select('capacidade')->where('idunidade', $idunidade)->get();
        
        foreach ($gfms as $gfm) {
            $totalCapacidade += $gfm->capacidade;
        }*/
        if ($totalCapacidade>0):
            $media = ($totalReservas * 100) / $totalCapacidade;
        else:
            $media=0;
        endif;
        
        $taxaOcupacao = [
            'totalReservas' => $totalReservas,
            'totalCapacidade' => $totalCapacidade,
            'media' => round($media)
        ];
        return $taxaOcupacao;
    }

    public function addGfm()
    {
        $data = Input::all();
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        // SALVA GFM
        $tipo = $data['tipo'];
        $gfm = Gfm::create(['idunidade' => $idunidade , 'gfmspm' => $tipo,'privado' => 'Não','capacidade' => 1 ]);
        
        $gfm = ['id' => $gfm->id];

        return $gfm;
    }
    public function getAulasPassadas($id)
    {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }        
        
        $aulas = Reserva::select('id','idgfm','created_at','dtaula')->where('idgfm', $id)->where('idunidade', $idunidade)->get();

        foreach ($aulas as $key => $value) {
            $aulas[$key]['dataAula']=date('d/m/Y', strtotime($aulas[$key]->dtaula));
        }
        return $aulas;
    }
    
    
    public function addAlunos($idaula, $numero)
    {
        
        
        
        
        
        
    }    
    
    
    public function addAnonimos($idaula, $numero)
    {
        $idaluno=0;
        $reserva = Reserva::select('n_reservas', 'id')->where('idgfm', $idaula)->whereRaw('Date(created_at) = CURDATE()')->first();
                
        if (count($reserva) > 0) {
            if ($reserva->n_reservas > 0) {
                if (Reserva::where('idgfm', $idaula)->update(['n_reservas' => $reserva->n_reservas - 1])) {
                    LogReserva::create(['idaluno' => $idaluno, 'idgfm' => $idaula, 'reserva_id' => $reserva->id ]);
                    $retorno['cod'] = '0';
                    $retorno['text'] = 'Aula reservada com sucesso!';
                } else {
                    $retorno['cod'] = '-1';
                    $retorno['text'] = 'Ocorreu um erro ao reservar aula, tente novamente!';
                }
            } else {
                $retorno['cod'] = '-1';
                $retorno['text'] = 'Desculpe, mas infelizmente não há mais vagas para esta aula!';
            }
        } else {
            $gfm = Gfm::select('gfm.id', 'gfm.idunidade', 'gfm.capacidade', 'gfm.diasemana')->where('gfm.id', $idaula)->first();
            $dias=explode(",", $gfm->diasemana);
                      
            $dtHj=date('Y-m-d');
            $ano = substr($dtHj, 0, 4);
            $mes = substr($dtHj, 5, 2);
            $dia = substr($dtHj, 8, 2);
            $ds = date("w", mktime($ano, $dia, $mes));
            $diasemana=$ds+1;
            $temAula="N";
            foreach ($dias as $key=>$value) {
                if ($dias[$key]==$diasemana):
                                $temAula="S";
                endif;
            }
            if ($temAula=="S") {
                $dataReserva['idunidade'] = $gfm->idunidade;
                $dataReserva['idgfm'] = $gfm->id;
                $dataReserva['n_reservas'] =  $gfm->capacidade - 1;
                $reservaNew = Reserva::create($dataReserva);
                if (sizeof($reservaNew)) {
                    LogReserva::create(['idaluno' => $idaluno, 'idgfm' => $idaula,'reserva_id' => $reservaNew->id]);
                    $retorno['cod'] = '0';
                    $retorno['text'] = 'Aula reservada com sucesso!';
                } else {
                    $retorno['cod'] = '-1';
                    $retorno['text'] = 'Ocorreu um erro ao reservar aula, tente novamente!';
                }
            } else {
                $retorno['cod'] = '-1';
                $retorno['text'] = 'Aula nao pode ser iniciada com data de hj!' ;
            };
        }
 
        return  $retorno ;
    }

    public function upGfm()
    {
        $data = Input::all();
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        $data['domingo'] = ($data['domingo'] == true) ? '1,' : '';
        $data['segunda'] = ($data['segunda'] == true) ? '2,' : '';
        $data['terca'] = ($data['terca'] == true) ? '3,' : '';
        $data['quarta'] = ($data['quarta'] == true) ? '4,' : '';
        $data['quinta'] = ($data['quinta'] == true) ? '5,' : '';
        $data['sexta'] = ($data['sexta'] == true) ? '6,' : '';
        $data['sabado'] = ($data['sabado'] == true) ? '7,' : '';
        $data['diasemana'] = $data['domingo'] . $data['segunda'] . $data['terca'] . $data['quarta'] . $data['quinta'] . $data['sexta'] . $data['sabado'];
        $data['diasemana'] = substr($data['diasemana'], 0, -1);
        unset($data['domingo'], $data['segunda'], $data['terca'], $data['quarta'], $data['quinta'], $data['sexta'], $data['sabado']);
        //SALVA GFM
        //unset($data['nmprograma']);
        $gfm = Gfm::where('id', $data['id'])->update([
            'idlocal' => $data['idlocal'],
            'idprograma' => $data['idprograma'],
            'idfuncionario' => $data['idfuncionario'],
            'nivel' => $data['nivel'],
            'objetivo' => $data['objetivo'],
            'privado' => $data['privado'],
            'gfmspm' => $data['gfmspm'],
            'hora_inicio' => $data['hora_inicio'],
            'duracao' => $data['duracao'],
            'diasemana' => $data['diasemana'],
            'valor' => $data['valor'],
            'capacidademin' => $data['capacidademin'],
            'capacidade' => $data['capacidade'],
            'fator' => $data['fator']
        ]);
        
        
        $dtHj=date('Y-m-d');
        $hrHj=date('H:i:s');
//////////////////atualizar aulas abertas/////////////////////////////////////        
        
        if ($hrHj<=$data['hora_inicio']):
            
        
            $aulas = Reserva::select('id','idgfm','created_at','dtaula')
                    ->where('dtaula','>=', $dtHj)->where('idgfm', $data['id'])
                    ->where('idunidade', $idunidade)->update([ 'capacidade' => $data['capacidade'] ]);            
            
        endif;
            

                
        
            
        
        
       /* foreach ($aulas as $key => $value) {
            
             
             $aulas[$key]['dataAula']=date('d/m/Y', strtotime($aulas[$key]->dtaula));
        }*/
        
        
        if (isset($data['niveis'])):

            $niveis = $data['niveis'];
            Nivelgfm::select('id')
                    ->where('idunidade', $idunidade)
                    ->where('idgfm', $data['id'])->delete();


            for ($i = 0; $i < sizeof($niveis); $i++) {

                $fcx = Nivelgfm::create([
                            'idgfm' => $data['id'],
                            'idunidade' => $idunidade,
                            'idnivel' => $niveis[$i]['cod']
                ]);
            }
        endif;

        return $gfm;
    }

    public function delGfm($gfm_id)
    {
        Gfm::where('id', $gfm_id)->delete();
    }
    public function delAula($programa_id)
    {
        Programa::where('id', $programa_id)->delete();
                
        $retorno['cod'] = '0';
                
        $retorno['text'] = 'Aula removida com sucesso!' ;
        return $retorno;
    }
}
