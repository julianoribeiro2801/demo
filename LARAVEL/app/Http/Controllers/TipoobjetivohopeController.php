<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tipoobjetivohope;
use Hash;
use App\Http\Requests\TipoobjetivohopeRequest;

class TipoobjetivohopeController extends Controller
{
    public function index()
    {
        $headers = ['category' => 'Clientes', 'title' => 'Tipo de Objetivo'];
        $tiposobjetivohope = Tipoobjetivohope::all();
        return view('admin.tiposobjetivohope.index', compact('tiposobjetivohope', 'headers'));
    }

    public function edit($id)
    {
        $headers = ['category' => 'Clientes', 'title' => 'Tipo de Objetivo'];
        $tipoobjetivohope = Tipoobjetivohope::where('id', $id)->first();
        return view('admin.tiposobjetivohope.edit', compact('tipoobjetivohope', 'headers'));
    }

    public function create()
    {
        $headers = ['category' => 'Clientes', 'title' => 'Tipo de Objetivo'];
        return view('admin.tiposobjetivohope.create', compact('headers'));
    }

    public function update(TipoobjetivohopeRequest $request, $id)
    {
        $data = $request->all();

        Tipoobjetivohope::where('id', $id)->update(['dstipoobjetivo' => $data['dstipoobjetivo'],
                'idunidade' => 1
       ]);
     
        return redirect()->route('admin.clientes.tiposobjetivohope.index');
    }

    public function store(TipoobjetivohopeRequest $request)
    {
        $data = $request->all();
       
        Tipoobjetivohope::create(['dstipoobjetivo' => $data['dstipoobjetivo'],
                'idunidade' => 1
      ]);
     
        return redirect()->route('admin.clientes.tiposobjetivohope.index');
    }


    // public function create_user(){

          //  User::create( [
    //          'email' => 'kk@msn.com' ,
    //          'password' => Hash::make( '123456' ) ,
    //          'name' => 'KK Ruiz' ,
    //          'role' => 'admin'
    //         ] );
    // }
}
