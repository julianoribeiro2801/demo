<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB;
use Input;

use Image;
use File;

use App\Models\Unidade;
use App\Models\UnidadeDados;
use App\Models\Moduloacademia;

use Mail;

// Precisa para funcionar o combo
use Auth;
use App\Services\FilialService;

class LojaAppController extends Controller
{
    public function __construct(FilialService $filialService)
    {
        $this->filialService = $filialService;
    }

    public function changeEmp($id_unidade)
    {
        $this->filialService->changeEmp($id_unidade);

        return redirect(url()->previous());
    }

    public function index()
    {
        $headers = ['category' => 'Empresa', 'title' => 'Unidades'];

        $unidades = Unidade::with(['parent', 'children', 'matriz'])->get();

        return view('empresas.index', compact('unidades', 'headers'));
    }



    public function getModulos(){
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }

       //$exer=DB::select("select idunidade from exercicio where id = " . $id );
        $modulos = Moduloacademia::select('modulos_has_unidade.*')->where('id_unidade', $idunidade)->get();

        return $modulos;//response()->json(compact('modulos'));        
    }

    public function enviaEmailAvisando($id_unidade) {

        //        dd($request->all());
        $academia = Unidade::select('unidade.fantasia', 'unidade_dados.email','users.name')
                ->where('unidade.id', $id_unidade)
                ->JOIN('unidade_dados', 'unidade_dados.idunidade', '=', 'unidade.id')
                ->JOIN('users', 'users.id', '=', 'unidade_dados.user_id')

                ->first();


        $data = array(
            'name' => $academia->fantasia,
            'email' => $academia->email,
            'nome_cliente' => $academia->name,
            'mensagem' => 'Acabou de Contratar o modulo de personalização de cores',
         
        );

         $dados_use = [
            'academia' => $academia->fantasia,
            'email_cliente' => $academia->email
           
        ];



        Mail::send('formstemplates.contrato_modulo', $data, function ($message) use ($dados_use) {

            $message->from('contato@personalclubbrasil.com.br', $name = 'Personal Club Brasil ');

            $message->replyTo('contato@personalclubbrasil.com.br',$name = 'Personal Club Brasil ');

            $message->to($dados_use['email_cliente'])
            ->cc('karstenschwab@gmail.com')
            ->cc('ruiz@7cliques.com.br')
            ->bcc('julianor1@hotmail.com')
            ->subject('Contratou o modulo de personalização de cores');
        });




        $sucesso = 'Sua mensagem foi enviada com sucesso!';

        return $sucesso;
    }
    
    
    public function addModulo() {
        //if(Request::ajax()) {
        $data = Input::all();
        $modulos = Moduloacademia::select('id')->where('id_modulo', $data['modulo'])->where('id_unidade', $data['unidade'])->first();

        $dataAdd['id_unidade'] = $data['unidade'];
        $dataAdd['id_modulo'] = $data['modulo'];
        $dataAdd['status'] = $data['status'];
        $dataAdd['pago'] = 'N';

        if (sizeof($modulos) > 0):
             $addModulo = new Moduloacademia();
            if ($addModulo->where('id', '=', $modulos->id)->update($dataAdd)):
                $retorno['title'] = 'Sucesso!';
                $retorno['type'] = 'success';
                if ($data['status']==0):
                    $retorno['text'] = 'Modulo desativado com sucesso!';
                    
                endif;
                
                if ($data['status']==1):
                    // aqui eu tenho q mandar o e-mail
                    $this->enviaEmailAvisando($data['unidade']);

                    $retorno['text'] = 'Modulo ativado com sucesso!';
                    
                endif;
                
            else:
                $retorno['title'] = 'Erro!';
                $retorno['type'] = 'error';
                $retorno['text'] = 'Erro ao atualizar Modulo!';
            endif;

        else:
            $addModulo = new Moduloacademia();
            if ($addModulo->create($dataAdd)):
                $retorno['title'] = 'Sucesso!';
                $retorno['type'] = 'success';
                if ($data['status']==0):
                    $retorno['text'] = 'Modulo desativado com sucesso!';
                    
                endif;
                
                if ($data['status']==1):
                    // aqui eu tenho q mandar o e-mail
                    $this->enviaEmailAvisando($data['unidade']);
                    $retorno['text'] = 'Modulo ativado com sucesso!';
                    
                endif;
            else:
                $retorno['title'] = 'Erro!';
                $retorno['type'] = 'error';
                $retorno['text'] = 'Erro ao adicionar modulo!';
                

            endif;

        endif;
        return $retorno;


    }


    // DELETAR PSA
    public function setData($Data)
    {
        $Format = explode(' ', $Data);
        $Data = explode('/', $Format[0]);
        $Data = $Data[2] . '-' . $Data[1] . '-' . $Data[0];
        return $Data;
    }
    public function getData($Data)
    {
        if ($Data != null):
            $data = date("d/m/Y", strtotime($Data));
        return $data;
        endif;
    }
}
