<?php
namespace App\Http\Controllers;

// use App\Http\Requests;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use DB;
use Input;
//use Request;
use File;
use App\Models\User;
use App\Models\UserDados;
use App\Models\Cidade;
use App\Models\Funcionario;
use App\Models\Objetivotreino;
use App\Models\Prescricao;
use App\Models\PrescricaoCalendario;
use App\Models\Exercicio;
use App\Models\Grupomuscular;
use App\Models\TreinoMusculacao;
use App\Models\TreinoMusculacaopadrao;
use App\Models\Historicotreino;
use App\Models\TreinoFicha;
use App\Models\TreinoFichapadrao;
use App\Models\Nutricao;
use App\Models\PrescricaoProjeto;
use App\Models\PrescricaoEtapa;
use App\Models\Programa;
use App\Models\PsaAtividade;
use App\Models\TreinoObjetivo;
use App\Models\TreinoNivel;
use App\Models\Nivelhabilidade;
use App\Models\TempoEstimado;


use App\Services\TrataDadosService;

// Precisa para funcionar o combo
use Auth;
use App\Services\FilialService;

class PrescricaoController extends Controller
{
    public function __construct(TrataDadosService $trataDadosService, FilialService $filialService)
    {
        $this->middleware('auth');

        $this->trataDadosService = $trataDadosService;
        $this->filialService = $filialService;
    }

    public function index()
    {
        $headers = ['category' => 'Prescrição', 'title' => 'Prescrição'];
        $alunos = User::select('id', 'name')->where('role', 'cliente')->get();
        $professores = Funcionario::select('id', 'name')->where('role', 'funcionario')->orderby('name')->get();
        $objetivos = Objetivotreino::select('id', 'nmobjetivo')->orderby('nmobjetivo')->get();
        $treinos = Prescricao::select('id', 'idaluno', 'dtinicio')->orderby('dtinicio')->get();
        $unidades_combo = $this->filialService->unidadesComboTop();

        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }

        return view('prescricao.index', compact('headers', 'alunos', 'professores', 'objetivos', 'treinos', 'unidades_combo', 'idunidade'));
    }

    public function indexId($id)
    {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
            
        $headers = ['category' => 'Prescrição', 'title' => 'Prescrição', 'client_id' => $id];
        $alunos = User::select('id', 'name')
                                ->where('role', 'cliente')
                                ->where('idunidade', $idunidade)
                                ->get();
                
        $professores = Funcionario::select('id', 'name')
                                            ->where('idunidade', $idunidade)
                                            ->where('role', 'funcionario')->orderby('name')->get();
        $objetivos = Objetivotreino::select('id', 'nmobjetivo')
                                            ->where('idunidade', $idunidade)
                                            ->orderby('nmobjetivo')->get();
        $treinos = Prescricao::select('id', 'idaluno', 'dtinicio')
                                            ->where('idunidade', $idunidade)
                                            ->orderby('dtinicio')->get();
        $unidades_combo = $this->filialService->unidadesComboTop();


        return view('prescricao.index', compact('headers', 'alunos', 'professores', 'objetivos', 'treinos', 'unidades_combo', 'idunidade'));
    }

    public function configuracoes()
    {
        $headers = ['category' => 'Configurações', 'title' => 'Configurações'];
        $alunos = User::select('id', 'name')->where('role', 'cliente')->get();
        $professores = Funcionario::select('id', 'name')->where('role', 'funcionario')->orderby('name')->get();
        $objetivos = Objetivotreino::select('id', 'nmobjetivo')->orderby('nmobjetivo')->get();
        $treinos = Prescricao::select('id', 'idaluno', 'dtinicio')->orderby('dtinicio')->get();
        $unidades_combo = $this->filialService->unidadesComboTop();

        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }

        return view('prescricao.configuracoes', compact('headers', 'alunos', 'professores', 'objetivos', 'treinos', 'unidades_combo', 'idunidade'));
    }
        
    public function treinopadrao()
    {
        $headers = ['category' => 'Configurações', 'title' => 'Treino Padrão'];
        $alunos = User::select('id', 'name')->where('role', 'cliente')->get();
        $professores = Funcionario::select('id', 'name')->where('role', 'funcionario')->orderby('name')->get();
        $objetivos = Objetivotreino::select('id', 'nmobjetivo')->orderby('nmobjetivo')->get();
        $treinos = Prescricao::select('id', 'idaluno', 'dtinicio')->orderby('dtinicio')->get();
        $unidades_combo = $this->filialService->unidadesComboTop();

        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }

        return view('prescricao.padrao', compact('headers', 'alunos', 'professores', 'objetivos', 'treinos', 'unidades_combo', 'idunidade'));
    }
        
    public function atualizaOrdemInsertFicha(Request $request, $treino_id, $aba)
    {
        $ficha = "";
        $exercicio = "";
        $series = "";
        $repeticoes = "";
        $carga = "";
        $intervalo = "";

        $ret=TreinoFicha::create([
            'ficha_letra' => $aba,
            'ficha_intervalo' => 0,
            'ficha_series' => 0,
            'ficha_repeticoes' => 0,
            'ficha_carga' =>  '',
            'treino_id' => $treino_id,
            'ord' => 99999,
            'exercicio_id' => '0'
        ]);
        
        
        
        foreach ($request->ficha as $key => $value) {
            $ficha[$key]['id'] = $value;
        }
        foreach ($request->exercicio as $key => $value) {
            $exercicio[$key]['id'] = $value;
        }
        foreach ($request->series as $key => $value) {
            $series[$key]['id'] = $value;
        }
        foreach ($request->repeticoes as $key => $value) {
            $repeticoes[$key]['id'] = $value;
        }
        foreach ($request->carga as $key => $value) {
            $carga[$key]['id'] = $value;
        }
        foreach ($request->intervalo as $key => $value) {
            $intervalo[$key]['id'] = $value;
        }
        if (isset($ficha)) {
            if (sizeof($ficha) > 0) {
                foreach ($ficha as $key => $value) {
                    TreinoFicha::where('ficha_id', $ficha[$key]['id'])
                            ->where('treino_id', $treino_id)
                            ->where('ficha_letra', $aba)
                            ->update([
                                'ord' => $key + 1,
                                'ficha_letra' => $aba,
                                'ficha_intervalo' => $intervalo[$key]['id'],
                                'ficha_series' => $series[$key]['id'],
                                'ficha_repeticoes' => $repeticoes[$key]['id'],
                                'treino_id' => $treino_id,
                                'exercicio_id' => $exercicio[$key]['id']
                    ]);
                }
            }
        }


        
        $fichas = TreinoFicha::select('ficha_id AS ficha', 'ord as ordem', 'ficha_letra AS letra', 'ficha_intervalo AS intervalo', 'ficha_series AS series', 'ficha_repeticoes AS repeticoes', 'ficha_carga AS carga', 'ficha_observacao AS observacao', DB::raw('ifnull(exercicio_id,0) as exercicio'))
                        ->where('treino_id', $treino_id)
                        ->where('ficha_letra', $aba)
                        ->orderby('ficha_letra')
                        ->orderby('ord')
                        ->get();
                
        foreach ($fichas as $key=>$value) {
            $fichas[$key]['seq'] = $key;
        }


        return $fichas;
    }
    public function atualizaOrdemFicha($treino_id, $aba)
    {
        $dataAll = Input::all();
        for ($i = 0; $i < sizeof($dataAll); $i++) {
            TreinoFicha::where('ficha_id', $dataAll[$i])
                                           ->where('treino_id', $treino_id)
                                           ->where('ficha_letra', $aba)
                                           ->update([
                        'ord' => $i + 1
                                           ]);
        }
                           
        return $dataAll;
    }
        

    // BUSCA EXERCICIOS
    public function getExercicios()
    {
        $exercicios = Exercicio::select(DB::raw('CONCAT(grupomuscular.nmgrupo, " - ", exercicio.nmexercicio) AS nome_exercicio'), 'exercicio.idunidade', 'exercicio.id', 'exercicio.dsurlminiatura')
                    ->leftJoin('grupomuscular', 'exercicio.idgrupomuscular', '=', 'grupomuscular.id')
                    ->orderby('exercicio.nmexercicio')->get();
        foreach ($exercicios as $exercicio) {
            $exercicio->dsurlminiatura = (isset($exercicio->dsurlminiatura) && $exercicio->dsurlminiatura != '') ? $exercicio->dsurlminiatura : 'exercicio.png';
        }
        $exercicios[sizeof($exercicios) - 1 ]['id']=0;
        $exercicios[sizeof($exercicios) - 1]['nome_exercicio']='NÃO INFORMADO';
                
        return $exercicios;
    }

    // BUSCA EXERCICIO
    public function getExercicio()
    {
        $data = Input::all();
        
        $exercicio = Exercicio::select('dsurlminiatura')->where('id', $data['id'])->get();
        return $exercicio;
    }

    // CALENDARIO
    public function MostreSemanas()
    {
        $semanas = array(
            'Domingo',
            'Segunda-Feira',
            'Terça-Feira',
            'Quarta-Feira',
            'Quinta-Feira',
            'Sexta-Feira',
            'Sábado'
        );
        for ($i = 0; $i < 7; $i++) {
            echo "<th>".$semanas[$i]."</th>";
        }
    }

    public function GetNumeroDias($mes)
    {
        $numero_dias = array(
            '01' => 31, '02' => 28, '03' => 31, '04' =>30, '05' => 31, '06' => 30,
            '07' => 31, '08' =>31, '09' => 30, '10' => 31, '11' => 30, '12' => 31
        );
        if (((date('Y') % 4) == 0 and (date('Y') % 100)!=0) or (date('Y') % 400)==0) {
            $numero_dias['02'] = 29; // altera o numero de dias de fevereiro se o ano for bissexto
        }
        return $numero_dias[$mes];
    }

    public function GetNomeMes($mes)
    {
        $meses = array(	'01' => "Janeiro", '02' => "Fevereiro", '03' => "Março",
                        '04' => "Abril",   '05' => "Maio",      '06' => "Junho",
                        '07' => "Julho",   '08' => "Agosto",    '09' => "Setembro",
                        '10' => "Outubro", '11' => "Novembro",  '12' => "Dezembro"
                );
         
        if ($mes >= 01 && $mes <= 12) {
            return $meses[$mes];
        } else {
            return "Mês deconhecido";
        }
    }

    public function addExercicioPadrao($treino_id)
    {
        $dataAll = Input::all();

        for ($i = 0; $i < sizeof($dataAll); $i++) {

            /*if(isset($dataAll[$i]['ficha'])) {
                $id = $dataAll[$i]['ficha'];
                if(isset($dataAll[$i]['observacao'])) {
                    TreinoFichapadrao::where('ficha_id', $id)->update([
                        'ficha_letra' => $dataAll[$i]['letra'],
                        'ficha_intervalo' => $dataAll[$i]['intervalo'],
                        'ficha_series' => $dataAll[$i]['series'],
                        'ficha_repeticoes' => $dataAll[$i]['repeticoes'],
                        'ficha_observacao' => $dataAll[$i]['observacao'],
                        'treino_id' => $treino_id,
                        //'ord' => $i + 1,
                        'exercicio_id' => $dataAll[$i]['exercicio']
                    ]);
                } else {
                    TreinoFichapadrao::where('ficha_id', $id)->update([
                        'ficha_letra' => $dataAll[$i]['letra'],
                        'ficha_intervalo' => $dataAll[$i]['intervalo'],
                        'ficha_series' => $dataAll[$i]['series'],
                        'ficha_repeticoes' => $dataAll[$i]['repeticoes'],
                        'treino_id' => $treino_id,
                        //'ord' => $i + 1,
                        'exercicio_id' => $dataAll[$i]['exercicio']
                    ]);
                }
            } else {*/
            if (isset($dataAll[$i]['observacao'])) {
                TreinoFichapadrao::create([
                        'ficha_letra' => $dataAll[$i]['letra'],
                        'ficha_intervalo' => $dataAll[$i]['intervalo'],
                        'ficha_series' => $dataAll[$i]['series'],
                        'ficha_repeticoes' => $dataAll[$i]['repeticoes'],
                        'ficha_observacao' => $dataAll[$i]['observacao'],
                        'treino_id' => $treino_id,
                        'ord' => $i + 1,
                        'exercicio_id' => $dataAll[$i]['exercicio']
                    ]);
            } else {
                TreinoFichapadrao::create([
                        'ficha_letra' => $dataAll[$i]['letra'],
                        'ficha_intervalo' => $dataAll[$i]['intervalo'],
                        'ficha_series' => $dataAll[$i]['series'],
                        'ficha_repeticoes' => $dataAll[$i]['repeticoes'],
                        'treino_id' => $treino_id,
                        'ord' => $i + 1,
                                                'exercicio_id' => $dataAll[$i]['exercicio']
                    ]);
            }
            //}
        }

        return $dataAll;
    }
        
    public function MostreCalendario($mes, $ano, $tipo, $client_id)
    {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
            
            
        $numero_dias = $this->GetNumeroDias($mes); // retorna o número de dias que tem o mês desejado
        $nome_mes = $this->GetNomeMes($mes);
        $diacorrente = 0;
        $diasemana = jddayofweek(cal_to_jd(CAL_GREGORIAN, $mes, "01", $ano), 0); // função que descobre o dia da semana

        // busca desc_prescricao
        $prescicoesAluno = PrescricaoCalendario::where('user_id_aluno', $client_id)->where('tipo_prescricao', $tipo)->where('idunidade', $idunidade)->whereMonth('data_prescricao', $mes)->whereYear('data_prescricao', $ano)->get();
        $arrayDesc = [];
        foreach ($prescicoesAluno as $prescicaoAluno) {
            $arrayDesc += array(substr($prescicaoAluno['data_prescricao'], 8, 2) => $prescicaoAluno['desc_prescricao']);
        }
        echo "<h2 class='pull-left'>".$nome_mes." de ".$ano."</h2><input type='hidden' id='mesatual' value='".$mes."'><input type='hidden' id='anoatual' value='".$ano."'>";
        echo "<table class='table table-bordered'>";
        echo "<thead>";
        echo "<tr>";
        $this->MostreSemanas(); // função que mostra as semanas aqui
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";
        for ($linha = 0; $linha < 6; $linha++) {
            echo "<tr>";
            for ($coluna = 0; $coluna < 7; $coluna++) {
                if (($diacorrente == (date('d') - 1) && date('m') == $mes)) {
                    echo "<td ";
                    echo "id='dia_atual'";
                    echo ">";
                } else {
                    if (($diacorrente + 1) <= $numero_dias) {
                        if ($coluna < $diasemana && $linha == 0) {
                            echo "<td ";
                            echo "id='dia_branco'";
                            echo ">";
                        } else {
                            echo "<td ";
                            echo "id='dia_comum'";
                            echo ">";
                        }
                    }
                }
                if ($diacorrente + 1 <= $numero_dias) {
                    if ($coluna < $diasemana && $linha == 0) {
                        echo " ";
                        echo "</td>";
                    } else {
                        echo "<div class='pull-right numero_calendar'>" . ++$diacorrente . "</div>";
                        echo "<textarea class='form-control calendar_desc' onblur='gravaCalendario(" . $diacorrente . ", this.value, \"$tipo\")' placeholder='Off'>";
                        $diacorrente = str_pad($diacorrente, 2, "0", STR_PAD_LEFT);
                        echo isset($arrayDesc[$diacorrente]) ? $arrayDesc[$diacorrente] : '';
                        echo "</textarea>";
                        echo "</td>";
                    }
                } else {
                    break;
                }
            }
            echo "</tr>";
        }
        echo "<tbody>";
        echo "</table>";
    }

    public function MostreCalendarioCompleto()
    {
        echo "<table align = 'center'>";
        $cont = 1;
        for ($j = 0; $j < 4; $j++) {
            echo "<tr>";
            for ($i = 0; $i < 3; $i++) {
                echo "<td>";
                $this->MostreCalendario(($cont < 10) ? "0".$cont : $cont);
                $cont++;
                echo "</td>";
            }
            echo "</tr>";
        }
        echo "</table>";
    }

    public function Data($Data)
    {
        $Data = explode('/', $Data);
        $Data = $Data[2] . '-' . $Data[1] . '-' . $Data[0];
        return $Data;
    }

    public function getCaledario()
    {
        if (Request::ajax()) {
            $data = Input::all();
            $data['mes'] = str_pad($data['mes'], 2, "0", STR_PAD_LEFT);
            return $this->MostreCalendario($data['mes'], $data['ano'], $data['tipo'], $data['client_id']);
        }
    }
    // FECHA CALENDARIO

    public function setData($Data)
    {
        $Format = explode(' ', $Data);
        $Data = explode('/', $Format[0]);
        $Data = $Data[2] . '-' . $Data[1] . '-' . $Data[0];
        return $Data;
    }

    public function getData($Data)
    {
        if ($Data != null):
            $data = date("d/m/Y", strtotime($Data));
        return $data;
        endif;
    }

    public function gravaCalendario()
    {
        if (Request::ajax()) {
            $data = Input::all();
            $idprofessor = Auth::id();

            $idunidade = Auth::user()->idunidade;
            if (session()->get('id_unidade')) {
                $idunidade = session()->get('id_unidade');
            }

            $data['desc_prescricao'] = $data['desc'];
            $data['data_prescricao'] = $this->setData($data['dataatual']);
            $data['tipo_prescricao'] = $data['tipo'];

            // alterar os campos idunidade, user_id_aluno e user_id_prof pra pegar dinamicamente
            $prescicaoAluno = PrescricaoCalendario::where('user_id_aluno', $data['client_id'])->where('tipo_prescricao', $data['tipo'])->where('data_prescricao', $data['data_prescricao'])->get();
            $contPrescricao = $prescicaoAluno->count();
            if ($contPrescricao > 0):
                $id = $prescicaoAluno[0]['id'];
            PrescricaoCalendario::where('id', $id)->update([
                    'idunidade' => $idunidade,
                    'user_id_aluno' => $data['client_id'],
                    'user_id_prof' => $idprofessor,
                    'desc_prescricao' => $data['desc_prescricao'],
                    'data_prescricao' => $data['data_prescricao'],
                    'tipo_prescricao' => $data['tipo_prescricao']
                ]);
            return 'Alterado com sucesso!'; else:
                PrescricaoCalendario::create([
                    'idunidade' => $idunidade,
                    'user_id_aluno' => $data['client_id'],
                    'user_id_prof' => $idprofessor,
                    'desc_prescricao' => $data['desc_prescricao'],
                    'data_prescricao' => $data['data_prescricao'],
                    'tipo_prescricao' => $data['tipo_prescricao']
                ]);
            return 'Criado com sucesso!';
            endif;
        }
    }

    // CRIA NOVO TREINO
    public function addTreino()
    {
        $data = Input::all();
        $idprofessor = Auth::id();
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }

        // FORMA NOME DO TREINO
        $treinoAluno = TreinoMusculacao::where('aluno_id', $data['aluno_id'])->where('unidade_id', $idunidade)->get();
        $contTreinoAluno = $treinoAluno->count() + 1;
        $treino_nome = 'Treino ' . $contTreinoAluno . ' - ' . date("d/m/Y");

        // SALVA TREINO E RETORNO O ID DO TREINO
        $treino = TreinoMusculacao::create([
            'treino_nome' => $treino_nome,
            'unidade_id' => $idunidade,
            'aluno_id' => $data['aluno_id'],
            'professor_id' => $idprofessor
        ]);
        $treino_id = $treino->id;

        $treinosAluno = [
            'treino_id' => $treino_id,
            'treino_nome' => $treino_nome
        ];

        return $treinosAluno;
    }

    // ATUALIZA TREINO
    public function upTreino()
    {
        $data = Input::all();
        $idprofessor = Auth::id();
        $dadosAluno = UserDados::select('genero', 'objetivo_id')->where('user_id', $data['aluno_id'])->get();
        /*$objetivo = TreinoObjetivo::select('nmobjetivo')->where('id', $dadosAluno[0]['objetivo_id'])->get();
        if(count($objetivo) > 0) {
            $nmobjetivo = (isset($objetivo[0]['nmobjetivo']) && $objetivo[0]['nmobjetivo'] != '') ? $objetivo[0]['nmobjetivo'] : 'S/O';
        } else {
            $nmobjetivo = 'S/O';
        }*/
        $professor = User::select('name as nmprofessor')->where('id', $idprofessor)->get();

        $data['treino_atual'] = false;
        $data['treino_atual'] = ($data['treino_atual'] == true) ? '1' : '0';
        TreinoMusculacao::where('aluno_id', $data['aluno_id'])->update(['treino_atual' => $data['treino_atual']]);

        $data['treino_atual'] = true;
        $treino_id = $data['treino_id'];
        $data['treino_padrao'] = ($data['treino_padrao'] == true) ? 'S' : 'N';
        $data['treino_atual'] = ($data['treino_atual'] == true) ? '1' : '0';

        $treinoAluno = TreinoMusculacao::where('aluno_id', $data['aluno_id'])->get();
        $contTreinoAluno = $treinoAluno->count();
        
        if ($data['treino_padrao'] == 'S'):
            if (isset($data['treino_nome_padrao']) && $data['treino_nome_padrao'] != ''):
                $treino_nome_padrao = $data['treino_nome_padrao'] . ' / Prof. ' . $professor[0]['nmprofessor']; else:
                $treino_nome_padrao = 'Treino ' . $contTreinoAluno . ' / Prof. ' . $professor[0]['nmprofessor'];
        endif; else:
            $treino_nome_padrao = 'Treino ' . $contTreinoAluno . ' / Prof. ' . $professor[0]['nmprofessor'];
        endif;

        TreinoMusculacao::where('treino_id', $treino_id)->update([
            'treino_nome_padrao' => $treino_nome_padrao,
            'treino_qtddias' => $data['treino_qtddias'],
            'treino_revisao' => (isset($data['treino_revisao']) && $data['treino_revisao'] != '') ? $this->setData($data['treino_revisao']) : '',
            'treino_nivel' => $data['treino_nivel'],
//			'treino_objetivo' => $dadosAluno[0]['objetivo_id'],
            'treino_objetivo' => $data['objetivo_id'],
            'treino_sexo' => $dadosAluno[0]['genero'],
            'treino_padrao' => $data['treino_padrao'],
            'treino_atual' => $data['treino_atual']
        ]);
        return 'Alterado com sucesso!';
    }

    public function saveTempoEstimado()
    {
        $data = Input::all();
        $data['client_id'] = $data['client_id'];
        $data['treino_id'] = $data['treino_id'];
        $data['letra'] = $data['letra'];
        $data['tempoestimado'] = $data['tempoestimado'];
        $data['tempomusculacao'] = $data['tempomusculacao'];

        $tempoEstimado = TempoEstimado::where('treino_id', $data['treino_id'])->where('letra', $data['letra'])->get();
        $contTempoEstimado = $tempoEstimado->count();
        if ($contTempoEstimado > 0):
            $id = $tempoEstimado[0]['id'];
        TempoEstimado::where('id', $id)->update([
                'client_id' => $data['client_id'],
                'tempoestimado' => $data['tempoestimado'],
                'tempomusculacao' => $data['tempomusculacao']
            ]);
        return $data ;//'Alterado com sucesso!';
        else:
            TempoEstimado::create([
                'client_id' => $data['client_id'],
                'treino_id' => $data['treino_id'],
                'letra' => $data['letra'],
                'tempoestimado' => $data['tempoestimado'],
                'tempomusculacao' => $data['tempomusculacao']
            ]);
        return $data;
        endif;
    }

    // GET CLIENTES
    public function getClients()
    {
        $data = Input::all();
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }

        $clients = User::select('users.id', 'users.name', 'users.email', 'user_dados.telefone')
                    ->leftJoin('user_dados', 'user_dados.user_id', '=', 'users.id')
                    ->wherein('users.role', ['cliente'])
                    ->where('users.idunidade', $idunidade)
                    ->get();
        return $clients;
    }

    // GET CLIENTE
    public function getClient()
    {
        $data = Input::all();

        $aluno_id = $data['aluno_id'];
        $client = User::select('users.*', 'user_dados.telefone', 'user_dados.endereco', 'user_dados.bairro', 'user_dados.dt_nascimento', 'user_dados.cpf', 'user_dados.genero', 'objetivotreino.nmobjetivo', 'profileType.description', 'profileType.objective', 'profileType.motivation', 'cidade.nome as cidade', 'cidade.estado')
                    ->leftJoin('user_dados', 'user_dados.user_id', '=', 'users.id')
                    ->leftJoin('objetivotreino', 'objetivotreino.id', '=', 'user_dados.objetivo_id')
                    ->leftJoin('profileType', 'profileType.id', '=', 'user_dados.profile_type')
                    ->leftJoin('cidade', 'user_dados.idcidade', '=', 'cidade.id')
                    ->where('users.id', $aluno_id)
                    ->get();
        
        $directoryPath = 'uploads/avatars/user-'.$client[0]['id'].'.jpg';
        if (File::exists($directoryPath)) {
            $client[0]['existe'] = 'sim';
        } else {
            $client[0]['existe'] = 'nao';
        }

        return $client;
    }


    // GET TREINO
    public function getTreino()
    {
        $data = Input::all();
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        $treino_id = $data['treino_id'];
        $treinoAluno = TreinoMusculacao::select('treino_nome_padrao', 'treino_qtddias', 'treino_objetivo', 'treino_revisao', 'treino_nivel', 'treino_padrao', 'treino_atual')
                              ->where('unidade_id', $idunidade)
                              ->where('treino_id', $treino_id)
                              ->get();
                
        $treino_nome_padrao = explode("-", $treinoAluno[0]['treino_nome_padrao']);
        $treino_nome_padrao = trim($treino_nome_padrao[0]);
        $treino = [
            'treino_nome_padrao' => $treino_nome_padrao,
            'treino_qtddias' => $treinoAluno[0]['treino_qtddias'],
            'treino_revisao' => ($treinoAluno[0]['treino_revisao'] != '0000-00-00') ? $this->getData($treinoAluno[0]['treino_revisao']) : '',
            'treino_nivel' => $treinoAluno[0]['treino_nivel'],
            'objetivo_id' => $treinoAluno[0]['treino_objetivo'],
            'treino_padrao' => $treinoAluno[0]['treino_padrao'],
            'treino_atual' => $treinoAluno[0]['treino_atual']
        ];
        return $treino;
    }
    public function getPrescricaoPadrao()
    {
        $data = Input::all();
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        $treino_id = $data['treino_id'];
        $treinoPadrao = TreinoMusculacaoPadrao::select('treino_nome_padrao', 'treino_qtddias', 'treino_objetivo', 'treino_nivel')
                              ->where('unidade_id', $idunidade)
                              ->where('treino_id', $treino_id)
                              ->get();
                
        $treino_nome_padrao = explode("-", $treinoPadrao[0]['treino_nome_padrao']);
        $treino_nome_padrao = trim($treino_nome_padrao[0]);
        $treino = [
            'treino_nome_padrao' => $treino_nome_padrao,
            'treino_qtddias' => $treinoPadrao[0]['treino_qtddias'],
            'treino_nivel' => $treinoPadrao[0]['treino_nivel'],
            'objetivo_id' => $treinoPadrao[0]['treino_objetivo']
        ];
        return $treino;
    }
    public function getTreinoIniciado()
    {
        $data = Input::all();
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        $treinoIniciado = Historicotreino::select('IDALUNO')
                              ->where('IDUNIDADE', $idunidade)
                              ->where('IDALUNO', $data['aluno_id'])
                              ->where('IDTREINO', $data['treino_id'])
                              ->get();
                
                
        if (sizeof($treinoIniciado) > 0):
                    $status="S"; else:
                    $status="N";
        endif;
        return response()->json(compact('status'));
    }

    public function delTreino($treino_id)
    {
        TreinoFicha::where('treino_id', $treino_id)->delete();
        TreinoMusculacao::where('treino_id', $treino_id)->delete();
    }

    public function delExercicioTreino($ficha_id)
    {
        TreinoFicha::where('ficha_id', $ficha_id)->delete();
    }

    // ADD EXERCICIO EM TREINO
    public function addLinhaTreino($treino_id)
    {
        $dataAll = Input::all();
        $ret='';
        for ($i = 0; $i < sizeof($dataAll); $i++) {
            // TreinoFicha::create([
            // 	'ficha_letra' => $dataAll[$i]['letra'],
            // 	'ficha_intervalo' => $dataAll[$i]['intervalo'],
            // 	'ficha_series' => $dataAll[$i]['series'],
            // 	'ficha_repeticoes' => $dataAll[$i]['repeticoes'],
            // 	'ficha_carga' => $dataAll[$i]['carga'],
            // 	'treino_id' => $treino_id,
            // 	'exercicio_id' => $dataAll[$i]['exercicio']
            // ]);
            if (isset($dataAll[$i]['ficha'])) {
                $id = $dataAll[$i]['ficha'];
                if (isset($dataAll[$i]['observacao'])) {
                    TreinoFicha::where('ficha_id', $id)->update([
                        'ficha_letra' => $dataAll[$i]['letra'],
                        'ficha_intervalo' => $dataAll[$i]['intervalo'],
                        'ficha_series' => $dataAll[$i]['series'],
                        'ficha_repeticoes' => $dataAll[$i]['repeticoes'],
                        'ficha_carga' =>  str_replace('kg', '', $dataAll[$i]['carga']),
                                            
                                               
                        'ficha_observacao' => $dataAll[$i]['observacao'],
                        'treino_id' => $treino_id,
                        'exercicio_id' => $dataAll[$i]['exercicio']
                    ]);
                } else {
                    TreinoFicha::where('ficha_id', $id)->update([
                        'ficha_letra' => $dataAll[$i]['letra'],
                        'ficha_intervalo' => $dataAll[$i]['intervalo'],
                        'ficha_series' => $dataAll[$i]['series'],
                        'ficha_repeticoes' => $dataAll[$i]['repeticoes'],
                        'ficha_carga' =>  str_replace('kg', '', $dataAll[$i]['carga']),
                        'treino_id' => $treino_id,
                        'exercicio_id' => $dataAll[$i]['exercicio']
                    ]);
                }
            } else {
                if (isset($dataAll[$i]['observacao'])) {
                    $ret=TreinoFicha::create([
                        'ficha_letra' => $dataAll[$i]['letra'],
                        'ficha_intervalo' => $dataAll[$i]['intervalo'],
                        'ficha_series' => $dataAll[$i]['series'],
                        'ficha_repeticoes' => $dataAll[$i]['repeticoes'],
                        'ficha_carga' =>  str_replace('kg', '', $dataAll[$i]['carga']),
                        'ficha_observacao' => $dataAll[$i]['observacao'],
                        'treino_id' => $treino_id,
                        'ord' => $dataAll[$i]['ordem'],
                        'exercicio_id' => $dataAll[$i]['exercicio']
                    ]);
                } else {
                    $ret=TreinoFicha::create([
                        'ficha_letra' => $dataAll[$i]['letra'],
                        'ficha_intervalo' => $dataAll[$i]['intervalo'],
                        'ficha_series' => $dataAll[$i]['series'],
                        'ficha_repeticoes' => $dataAll[$i]['repeticoes'],
                        'ficha_carga' =>  str_replace('kg', '', $dataAll[$i]['carga']),
                        'treino_id' => $treino_id,
                        'ord' => $dataAll[$i]['ordem'],
                        'exercicio_id' => $dataAll[$i]['exercicio']
                    ]);
                }
            }
        }
             
        return $ret['id'];
    }
    public function addExercicioTreino($treino_id)
    {
        $dataAll = Input::all();

        for ($i = 0; $i < sizeof($dataAll); $i++) {
            // TreinoFicha::create([
            // 	'ficha_letra' => $dataAll[$i]['letra'],
            // 	'ficha_intervalo' => $dataAll[$i]['intervalo'],
            // 	'ficha_series' => $dataAll[$i]['series'],
            // 	'ficha_repeticoes' => $dataAll[$i]['repeticoes'],
            // 	'ficha_carga' => $dataAll[$i]['carga'],
            // 	'treino_id' => $treino_id,
            // 	'exercicio_id' => $dataAll[$i]['exercicio']
            // ]);
            if (isset($dataAll[$i]['ficha'])) {
                $id = $dataAll[$i]['ficha'];
                if (isset($dataAll[$i]['observacao'])) {
                    TreinoFicha::where('ficha_id', $id)->update([
                        'ficha_letra' => $dataAll[$i]['letra'],
                        'ficha_intervalo' => $dataAll[$i]['intervalo'],
                        'ficha_series' => $dataAll[$i]['series'],
                        'ficha_repeticoes' => $dataAll[$i]['repeticoes'],
                        'ficha_carga' =>  str_replace('kg', '', $dataAll[$i]['carga']),
                                            
                                               
                        'ficha_observacao' => $dataAll[$i]['observacao'],
                        'treino_id' => $treino_id,
                        'exercicio_id' => $dataAll[$i]['exercicio']
                    ]);
                } else {
                    TreinoFicha::where('ficha_id', $id)->update([
                        'ficha_letra' => $dataAll[$i]['letra'],
                        'ficha_intervalo' => $dataAll[$i]['intervalo'],
                        'ficha_series' => $dataAll[$i]['series'],
                        'ficha_repeticoes' => $dataAll[$i]['repeticoes'],
                        'ficha_carga' =>  str_replace('kg', '', $dataAll[$i]['carga']),
                        'treino_id' => $treino_id,
                        'exercicio_id' => $dataAll[$i]['exercicio']
                    ]);
                }
            } else {
                if (isset($dataAll[$i]['observacao'])) {
                    TreinoFicha::create([
                        'ficha_letra' => $dataAll[$i]['letra'],
                        'ficha_intervalo' => $dataAll[$i]['intervalo'],
                        'ficha_series' => $dataAll[$i]['series'],
                        'ficha_repeticoes' => $dataAll[$i]['repeticoes'],
                        'ficha_carga' =>  str_replace('kg', '', $dataAll[$i]['carga']),
                        'ficha_observacao' => $dataAll[$i]['observacao'],
                        'treino_id' => $treino_id,
                        'ord' => $dataAll[$i]['ordem'],
                        'exercicio_id' => $dataAll[$i]['exercicio']
                    ]);
                } else {
                    TreinoFicha::create([
                        'ficha_letra' => $dataAll[$i]['letra'],
                        'ficha_intervalo' => $dataAll[$i]['intervalo'],
                        'ficha_series' => $dataAll[$i]['series'],
                        'ficha_repeticoes' => $dataAll[$i]['repeticoes'],
                        'ficha_carga' =>  str_replace('kg', '', $dataAll[$i]['carga']),
                        'treino_id' => $treino_id,
                        'ord' => $dataAll[$i]['ordem'],
                        'exercicio_id' => $dataAll[$i]['exercicio']
                    ]);
                }
            }
        }

        return $dataAll;
    }

    // BUSCA EXERCICIOS DO TREINO
    public function getFicha()
    {
        $data = Input::all();
        //'exercicio_id AS exercicio',
        $ficha = TreinoFicha::select('ficha_id AS ficha', 'ord as ordem', 'ficha_letra AS letra', 'ficha_intervalo AS intervalo', 'ficha_series AS series', 'ficha_repeticoes AS repeticoes', 'ficha_carga AS carga', 'ficha_observacao AS observacao', DB::raw('ifnull(exercicio_id,0) as exercicio'))
                        ->where('treino_id', $data['treino_id'])
                        ->orderby('ficha_letra')
                        ->orderby('ord')
                        ->get();
                
        foreach ($ficha as $key=>$value) {
            $ficha[$key]['seq'] = $key;
        }
        return $ficha;
    }
        
    // BUSCA EXERCICIOS DO TREINO
    public function getFichaAba($treino_id, $ficha_letra)
    {
        $data = Input::all();
        //'exercicio_id AS exercicio',
        $ficha = TreinoFicha::select('ficha_id AS ficha', 'ord as ordem', 'ficha_letra AS letra', 'ficha_intervalo AS intervalo', 'ficha_series AS series', 'ficha_repeticoes AS repeticoes', 'ficha_carga AS carga', 'ficha_observacao AS observacao', DB::raw('ifnull(exercicio_id,0) as exercicio'))
                        ->where('treino_id', $treino_id)
                        ->where('ficha_letra', $ficha_letra)
/*                        ->where('treino_id', $data['treino_id'])
                        ->where('ficha_letra', $data['ficha_letra'])*/
                        ->orderby('ficha_letra')
                        ->orderby('ord')
                        ->get();
                
        foreach ($ficha as $key=>$value) {
            $ficha[$key]['seq'] = $key;
        }
        return $ficha;
    }
    public function getPrescricaoFichaPadrao()
    {
        $data = Input::all();
                
        $ficha = TreinoFichapadrao::select('ord as ordem', 'ficha_id AS ficha', 'ficha_letra AS letra', 'ficha_intervalo AS intervalo', 'ficha_series AS series', 'ficha_repeticoes AS repeticoes', 'ficha_carga AS carga', 'ficha_observacao AS observacao', 'exercicio_id AS exercicio')
                        ->where('treino_id', $data['treino_id'])
                        ->orderby('ficha_letra')
                        ->orderby('ord')
                        ->get();
        return $ficha;
    }

    // BUSCA TREINO
    public function getTreinos()
    {
        $data = Input::all();
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        $treinosAluno = TreinoMusculacao::select('treino_id', 'treino_nome')
                                ->where('unidade_id', $idunidade)
                                ->where('aluno_id', $data['aluno_id'])
                                ->orderBy('treino_id', 'ASC')
                                ->get();
        return $treinosAluno;
    }

    // BUSCA NUTRIÇÕES
    public function getNutricoes()
    {
        $data = Input::all();
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        $nutricoesAluno = Nutricao::select('id', 'nutricao_nome')
                                  ->where('unidade_id', $idunidade)
                                  ->where('aluno_id', $data['aluno_id'])
                                  ->get();
        return $nutricoesAluno;
    }

    // BUSCA NUTRIÇÕES
    public function getNutricao()
    {
        $data = Input::all();
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        $nutricaoAluno = Nutricao::select('id', 'nutricao_descricao', 'nutricao_datainicio', 'nutricao_datatermino', 'nutricao_nivel', 'nutricao_padrao')
                                ->where('unidade_id', $idunidade)
                                ->where('id', $data['nutricao_id'])
                                ->get();
        $nutricaoAluno = [
            'id' => $nutricaoAluno[0]['id'],
            'nutricao_descricao' => $nutricaoAluno[0]['nutricao_descricao'],
            'nutricao_datainicio' => ($nutricaoAluno[0]['nutricao_datainicio'] != '0000-00-00') ? $this->getData($nutricaoAluno[0]['nutricao_datainicio']) : '',
            'nutricao_datatermino' => ($nutricaoAluno[0]['nutricao_datatermino'] != '0000-00-00') ? $this->getData($nutricaoAluno[0]['nutricao_datatermino']) : '',
            'nutricao_nivel' => $nutricaoAluno[0]['nutricao_nivel'],
            'nutricao_padrao' => $nutricaoAluno[0]['nutricao_padrao']
        ];
        return $nutricaoAluno;
    }

    // CRIA NOVA NUTRICAO
    public function addNutricao()
    {
        $data = Input::all();
        $idprofessor = Auth::id();
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }

        // FORMA NOME DA NUTRICAO
        $nutricaoAluno = Nutricao::where('aluno_id', $data['aluno_id'])->get();
        $contNutricaoAluno = $nutricaoAluno->count() + 1;
        $nutricao_nome = 'Nutrição ' . $contNutricaoAluno . ' - ' . date("d/m/Y");

        // SALVA NUTRICAO E RETORNO O ID DA NUTRICAO
        $nutricao = Nutricao::create([
            'nutricao_nome' => $nutricao_nome,
            'aluno_id' => $data['aluno_id'],
            'professor_id' => $idprofessor,
            'unidade_id' => $idunidade,
        ]);
        $nutricao_id = $nutricao->id;

        $nutricaoAluno = [
            'id' => $nutricao_id,
            'nutricao_nome' => $nutricao_nome
        ];

        return $nutricaoAluno;
    }

    // ATUALIZA NUTRICAO
    public function upNutricao()
    {
        $data = Input::all();

        $nutricao_id = $data['nutricao_id'];
        $data['nutricao_padrao'] = ($data['nutricao_padrao'] == true) ? 'S' : 'N';
        Nutricao::where('id', $nutricao_id)->update([
            'nutricao_descricao' => $data['nutricao_descricao'],
            'nutricao_datainicio' => (isset($data['nutricao_datainicio']) && $data['nutricao_datainicio'] != '') ? $this->setData($data['nutricao_datainicio']) : '',
            'nutricao_datatermino' => (isset($data['nutricao_datatermino']) && $data['nutricao_datatermino'] != '') ? $this->setData($data['nutricao_datatermino']) : '',
            'nutricao_nivel' => $data['nutricao_nivel'],
            'nutricao_padrao' => $data['nutricao_padrao']
        ]);
        return 'Alterado com sucesso!';
    }

    // DELETA NUTRIÇÃO
    public function delNutricao($id)
    {
        Nutricao::where('id', $id)->delete();
    }

    // BUSCA PROJETOS
    public function getProjetos()
    {
        $data = Input::all();
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        $projetos = PrescricaoProjeto::select('id', 'projeto_nome', 'projeto_data', 'projeto_status', 'created_at')
                            ->where('unidade_id', $idunidade)
                            ->where('aluno_id', $data['aluno_id'])
                            ->get();
        return $projetos;
    }

    // CRIA NOVO PROJETO
    public function addProjeto()
    {
        $data = Input::all();
        $idprofessor = Auth::id();
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }

        // INATIVA PROJETOS ANTERIORES
        PrescricaoProjeto::where('aluno_id', $data['aluno_id'])->update(['projeto_status' => 'I']);

        // SALVA PROJETO E RETORNO O ID DO PROJETO
        $projeto = PrescricaoProjeto::create([
            'projeto_nome' => '',
            'projeto_data' => date("d/m/Y"),
            'projeto_status' => 'A',
            'unidade_id' => $idunidade,
            'aluno_id' => $data['aluno_id'],
            'professor_id' => $idprofessor
        ]);
        $projeto_id = $projeto->id;
        $projeto_nome = $projeto->projeto_nome;
        $projeto_data = $projeto->projeto_data;
        $projeto_status = $projeto->projeto_status;
        $created_at = $projeto->created_at;

        $projeto = [
            'id' => $projeto_id,
            'projeto_nome' => $projeto_nome,
            'projeto_data' => $projeto_data,
            'projeto_status' => $projeto_status,
            'created_at' => $created_at
        ];

        return $projeto;
    }

    // ATUALIZA PROJETO
    public function upProjeto()
    {
        $data = Input::all();

        $id = $data['id'];
        PrescricaoProjeto::where('id', $id)->update([
            'projeto_nome' => $data['projeto_nome'],
            'projeto_data' => $data['projeto_data']
        ]);
        return 'Alterado com sucesso!';
    }

    // BUSCA ETAPAS
    public function getEtapas()
    {
        $projeto = PrescricaoProjeto::select('id')->orderby('id', 'desc')->get();
        $projeto_id = $projeto[0]['id'];

        $etapas = PrescricaoEtapa::select('id', 'etapa_nome', 'etapa_data', 'etapa_status', 'created_at')->where('projeto_id', $projeto_id)->get();
        return $etapas;
    }

    // DEL PROJETO
    public function delProjeto($projeto_id)
    {
        PrescricaoEtapa::where('projeto_id', $projeto_id)->delete();
        PrescricaoProjeto::where('id', $projeto_id)->delete();
    }

    // CRIA NOVO ETAPA
    public function addEtapa()
    {
        $data = Input::all();

        $projeto = PrescricaoProjeto::select('id', 'projeto_data')->orderby('id', 'desc')->get();
        $projeto_id = $projeto[0]['id'];
        $projeto_data = $projeto[0]['projeto_data'];

        // INATIVA ETAPAS ANTERIORES
        PrescricaoEtapa::where('projeto_id', $projeto_id)->update(['etapa_status' => 'I']);

        // SALVA ETAPA E RETORNO O ID DA ETAPA
        $etapa = PrescricaoEtapa::create([
            'projeto_id' => $projeto_id,
            'etapa_nome' => '',
            'etapa_data' => date("d/m/Y"),
            'etapa_status' => 'A',
            'idcategoria' => '0'
        ]);
        $etapa_id = $etapa->id;
        $etapa_nome = $etapa->etapa_nome;
        $etapa_data = $etapa->etapa_data;
        $etapa_status = $etapa->etapa_status;
        $etapa_categoria = $etapa->etapa_categoria;
        $created_at = $etapa->created_at;

        $etapa = [
            'id' => $etapa_id,
            'projeto_data' => $projeto_data,
            'etapa_nome' => $etapa_nome,
            'etapa_data' => $etapa_data,
            'etapa_status' => $etapa_status,
            'idcategoria' => $etapa_categoria,
            'created_at' => $created_at
        ];

        return $etapa;
    }

    // ATUALIZA PROJETO
    public function upEtapa()
    {
        $data = Input::all();
        $id = $data['id'];

        $projeto = PrescricaoProjeto::select('id', 'projeto_data')->latest()->limit(1)->get();
        $data1 = $this->Data($projeto[0]['projeto_data']);
        $data2 = $this->Data($data['etapa_data']);

        if (strtotime($data1) >= strtotime($data2)):
            PrescricaoEtapa::where('id', $id)->update([
                'etapa_nome' => $data['etapa_nome'],
                'etapa_data' => $data['etapa_data'],
                'idcategoria' => $data['etapa_categoria']
            ]);
        $retorno['title'] = 'Sucesso!';
        $retorno['type'] = 'success';
        $retorno['text'] = 'Alterado com sucesso!'; else:
            $retorno['title'] = 'Erro!';
        $retorno['type'] = 'error';
        $retorno['text'] = 'A data da etapa não pode ser maior que a data do projeto!';
        endif;
        return $retorno;
    }

    // DEL ETAPA
    public function delEtapa($etapa_id)
    {
        PrescricaoEtapa::where('id', $etapa_id)->delete();
    }

    // PSA
    // GET PROGRAMAS
    public function getProgramas()
    {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        $programas = Programa::select('id', 'nmprograma', 'idunidade')->where('idunidade', $idunidade)->orderby('nmprograma')->get();
                
        return $programas;
    }

    // BUSCA PSAS
    public function getPsas()
    {
        $data = Input::all();
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        
        $psas = PsaAtividade::select('id', 'dom_atv', DB::raw('DATE_FORMAT(dom_hora, "%H:%i") as dom_hora'), 'seg_atv', DB::raw('DATE_FORMAT(seg_hora, "%H:%i") as seg_hora'), 'ter_atv', DB::raw('DATE_FORMAT(ter_hora, "%H:%i") as ter_hora'), 'qua_atv', DB::raw('DATE_FORMAT(qua_hora, "%H:%i") as qua_hora'), 'qui_atv', DB::raw('DATE_FORMAT(qui_hora, "%H:%i") as qui_hora'), 'sex_atv', DB::raw('DATE_FORMAT(sex_hora, "%H:%i") as sex_hora'), 'sab_atv', DB::raw('DATE_FORMAT(sab_hora, "%H:%i") as sab_hora'))->where('idaluno', $data['aluno_id'])->where('idunidade', $idunidade)->orderby('id', 'asc')->get();
        return $psas;
    }
        

    // ADICIONAR PSA
    public function addPsa($aluno_id)
    {
        $idprofessor = Auth::id();
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }

        $psa = PsaAtividade::create([
            'idaluno' => $aluno_id,
            'idprofessor' => $idprofessor,
            'idunidade' => $idunidade,
            'dom_atv' => '',
            'dom_hora' => '',
            'seg_atv' => '',
            'seg_hora' => '',
            'ter_atv' => '',
            'ter_hora' => '',
            'qua_atv' => '',
            'qua_hora' => '',
            'qui_atv' => '',
            'qui_hora' => '',
            'sex_atv' => '',
            'sex_hora' => '',
            'sab_atv' => '',
            'sab_hora' => ''
        ]);

        $psa = [
            'id' => $psa->id,
            'dom_atv' => $psa->dom_atv,
            'dom_hora' => $psa->dom_hora,
            'seg_atv' => $psa->seg_atv,
            'seg_hora' => $psa->seg_hora,
            'ter_atv' => $psa->ter_atv,
            'ter_hora' => $psa->ter_hora,
            'qua_atv' => $psa->qua_atv,
            'qua_hora' => $psa->qua_hora,
            'qui_atv' => $psa->qui_atv,
            'qui_hora' => $psa->qui_hora,
            'sex_atv' => $psa->sex_atv,
            'sex_hora' => $psa->sex_hora,
            'sab_atv' => $psa->sab_atv,
            'sab_hora' => $psa->sab_hora
        ];

        return $psa;
    }

    // SALVA PSA
    public function salvaPsa()
    {
        $dataAll = Input::all();
        $idprofessor = Auth::id();
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }

        for ($i = 0; $i < sizeof($dataAll); $i++) {
            $id = $dataAll[$i]['id'];
            PsaAtividade::where('id', $id)->update([
                'dom_atv' => $dataAll[$i]['dom_atv'],
                'dom_hora' => $dataAll[$i]['dom_hora'],
                'seg_atv' => $dataAll[$i]['seg_atv'],
                'seg_hora' => $dataAll[$i]['seg_hora'],
                'ter_atv' => $dataAll[$i]['ter_atv'],
                'ter_hora' => $dataAll[$i]['ter_hora'],
                'qua_atv' => $dataAll[$i]['qua_atv'],
                'qua_hora' => $dataAll[$i]['qua_hora'],
                'qui_atv' => $dataAll[$i]['qui_atv'],
                'qui_hora' => $dataAll[$i]['qui_hora'],
                'sex_atv' => $dataAll[$i]['sex_atv'],
                'sex_hora' => $dataAll[$i]['sex_hora'],
                'sab_atv' => $dataAll[$i]['sab_atv'],
                'sab_hora' => $dataAll[$i]['sab_hora']
            ]);
        }
    }

    // DELETAR PSA
    public function delPsa($id)
    {
        PsaAtividade::where('id', $id)->delete();
    }


    ///////////////////////////////////////////////////////////
    ////////////////////// TREINO PADRAO //////////////////////
    ///////////////////////////////////////////////////////////

    // GET OBJETIVOS
    public function getObjetivos($id)
    {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }

        $objetivos = TreinoObjetivo::select('id', 'nmobjetivo')
                                        ->where('idmodalidade', $id)
                                        ->where('idunidade', $idunidade)
                                        ->get();
        return $objetivos;
    }

    // GET NIVEIS
    public function getNiveis($modalidade, $objetivo)
    {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }

        $niveis = Nivelhabilidade::select('id', 'nmnivel')
                                    ->where('idunidade', $idunidade)
                                    ->where('idmodalidade', $modalidade)
                                    ->where('idobjetivo', $objetivo)
                                    ->orderby('id', 'ASC')
                                    ->get();
        // $niveis = TreinoNivel::select('id', 'nivel_descricao')->where('id_unidade', $idunidade)->where('nivel_status', 'A')->get();
        return $niveis;
    }

    // BUSCA TREINO
    public function getFullTreinosM()
    {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        $treinosUnidade = TreinoMusculacaopadrao::select('treino_id', 'treino_nome_padrao', 'treino_nivel', 'treino_objetivo')->where('treino_sexo', 'M')->where('unidade_id', $idunidade)->get();
        return $treinosUnidade;
    }

    public function getFullTreinosF()
    {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }

        $treinosUnidade = TreinoMusculacaopadrao::select('treino_id', 'treino_nome_padrao', 'treino_nivel', 'treino_objetivo')->where('treino_sexo', 'F')->where('unidade_id', $idunidade)->get();
        return $treinosUnidade;
    }
        

    public function getTreinoPadrao()
    {
        $data = Input::all();

        $treinoPadrao = TreinoMusculacao::select('treino_id', 'treino_nome_padrao')->where('treino_id', $data->id)->first();
        return $treinoPadrao;
    }
        
    public function getFichaPadrao()
    {
        $data = Input::all();
                
                
        $treinofichas = TreinoFichapadrao::select('grupomuscular.nmgrupo as grupo', 'ficha_id AS ficha', 'ord AS ordem', 'ficha_letra AS letra', 'ficha_intervalo AS intervalo', 'ficha_series AS series', 'ficha_repeticoes AS repeticoes', 'ficha_carga AS carga', 'ficha_observacao AS observacao', 'exercicio.nmexercicio as nmexercicio', DB::raw('ifnull(exercicio_id,0) as exercicio'))
                                ->leftJoin('exercicio', 'treino_ficha_padrao.exercicio_id', '=', 'exercicio.id')
                                ->leftJoin('grupomuscular', 'exercicio.idgrupomuscular', '=', 'grupomuscular.id')
                                ->where('treino_id', $data['treino_id'])
                                ->orderby('ficha_letra')
                                ->orderby('ord')
                                ->get();
                
                
        foreach ($treinofichas as $key=>$value) {
            $treinofichas[$key]['seq'] = $key;
        }
         
                
        $treinopadrao = Treinomusculacaopadrao::select('treino_musculacao_padrao.*')
                                ->where('treino_id', $data['treino_id'])->get();

        $treinodia = TreinoFichaPadrao::select('grupomuscular.nmgrupo')
            ->JOIN('exercicio', 'treino_ficha_padrao.exercicio_id', '=', 'exercicio.id')
            ->JOIN('grupomuscular', 'exercicio.idgrupomuscular', '=', 'grupomuscular.id')
            ->where('treino_ficha_padrao.treino_id', $data['treino_id'])
            ->where('treino_ficha_padrao.ficha_letra', 'A')
            ->groupBy('grupomuscular.nmgrupo')
            ->get();
        $ret = '';
        /*foreach ($treinodia as $key => $value){
            $virgula = ($key < (count($treinodia)-1) ) ? ', ' : '';
            $ret .= $value->nmgrupo.$virgula;
        }*/
                
                
        return response()->json(compact('treinofichas', 'treinopadrao', 'ret'));
    }
    public function listarGrupoTreino($id_treino, $letra)
    {
        // $treinodia = TreinoFicha::where('treino_id', $id_treino)->get();
        $treinodia = TreinoFichaPadrao::select('grupomuscular.nmgrupo')
            ->JOIN('exercicio', 'treino_ficha_padrao.exercicio_id', '=', 'exercicio.id')
            ->JOIN('grupomuscular', 'exercicio.idgrupomuscular', '=', 'grupomuscular.id')
            ->where('treino_ficha_padrao.treino_id', $id_treino)
            ->where('treino_ficha_padrao.ficha_letra', $letra)
            ->groupBy('grupomuscular.nmgrupo')
            ->get();
        $ret = '';


        foreach ($treinodia as $key => $value) {
            $virgula = ($key < (count($treinodia)-1)) ? ', ' : '';
            $ret .= $value->nmgrupo.$virgula;
        }

        return response()->json(compact('ret'));
    }

    public function changeNomeTreinoPadrao()
    {
        $data = Input::all();

        TreinoMusculacao::where('treino_id', $data['treino_id'])->update(['treino_nome_padrao' => $data['treino_nome_padrao']]);
        return 'Alterado com sucesso!';
    }

    public function delTreinoPadrao($treino_id)
    {
        TreinoMusculacao::where('treino_id', $treino_id)->update(['treino_padrao' => 'N']);
        return 'Alterado com sucesso!';
    }
        
    public function getPadrao($sexo)
    {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        $treinospadrao = TreinoMusculacaopadrao::select('treino_objetivo', 'objetivotreino.nmobjetivo')
                                       ->leftJoin('objetivotreino', 'treino_musculacao.treino_objetivo', '=', 'objetivotreino.id')
                                       ->where('treino_objetivo', '>', 0)
                                       ->where('treino_padrao', '=', 'S')
                                       ->where('unidade_id', $idunidade)
                                       ->where('treino_sexo', $sexo)
                                       ->groupBy('treino_objetivo', 'objetivotreino.nmobjetivo')
                                       ->distinct()->get();
                
        foreach ($treinospadrao as $key=>$value) {
            $treinospadrao[$key]['niveis']=TreinoMusculacaopadrao::select('treino_nivel', 'nivelhabilidade.nmnivel')
                                              ->leftJoin('nivelhabilidade', 'treino_musculacao.treino_nivel', '=', 'nivelhabilidade.id')
                                              ->where('treino_objetivo', '=', $treinospadrao[$key]['treino_objetivo'])
                                              ->where('treino_nivel', '>', 0)
                                              ->where('treino_padrao', '=', 'S')
                                              ->where('unidade_id', $idunidade)
                                              ->where('treino_sexo', $sexo)
                                              ->groupBy('treino_nivel', 'nivelhabilidade.nmnivel')
                                              ->get();
        }
        foreach ($treinospadrao as $key=>$value) {
            $niveis=$treinospadrao[$key]['niveis'];
            foreach ($niveis as $key1=>$value) {
                $niveis[$key1]['treinos']=TreinoMusculacaopadrao::select('treino_id', 'treino_nome_padrao')
                                              ->where('treino_objetivo', '=', $treinospadrao[$key]['treino_objetivo'])
                                              ->where('treino_nivel', '=', $niveis[$key1]->treino_nivel)
                                              ->where('treino_padrao', '=', 'S')
                                              ->where('treino_sexo', $sexo)
                                              ->where('unidade_id', $idunidade)
                                              ->get();
            }
        }
        return response()->json(compact('treinospadrao'));
    }
}
