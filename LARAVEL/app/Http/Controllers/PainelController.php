<?php

namespace App\Http\Controllers;



use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use SoapClient;
use DB;
use Input;
// use Request;
use Image;
use File;
use \App\Models\Carteiraprofessor;
use App\Services\SendPushService;
use App\Models\Unidade;
use App\Models\Gfm;
use App\Models\Resultadomes;

use App\Models\Logcliente;
use App\Models\User;
use App\Models\UnidadeDados;
use App\Models\Estado;
use App\Models\CVEmpresa;
use App\Models\UserDados;

use App\Models\Reserva;
use App\Models\LogReserva;
use App\Models\TreinoMusculacao;

use App\Models\PrescricaoProjeto;


// Precisa para funcionar o combo



use Auth;
use App\Services\FilialService;

class PainelController extends Controller {
    
    

    public function __construct(FilialService $filialService, SendPushService $pushService) {
        $this->filialService = $filialService;
        $this->pushService = $pushService;
    }

    public function changeEmp($id_unidade ) {
        
        
        
        $this->filialService->changeEmp($id_unidade);

        return redirect(url()->previous());
    }

    public function index() {
        $headers = ['category' => 'Empresa', 'title' => 'Unidades'];

        $unidades = Unidade::with(['parent', 'children', 'matriz'])->get();

        return view('empresas.index', compact('unidades', 'headers'));
    }
    


    public function difDatas($dia) {

        $diferenca = strtotime(date('Y-m-d')) - strtotime($dia);

        $dias = floor($diferenca / (60 * 60 * 24));

        return ($dias * -1);

    }



    public function projetosAtrasados() {
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }

        $projetos = PrescricaoProjeto::select('prescricao_projeto.*', 'users.name')
        ->where('unidade_id',$idunidade)
        ->join('users', 'users.id','=' ,'prescricao_projeto.aluno_id')
        ->where('projeto_status','A')
        ->where('users.role','cliente')
        ->where('projeto_data','<>', '')
        ->get();

        //convertenda a data para en
        foreach($projetos as $key => $value){
          $value->projeto_data =  implode('-', array_reverse(explode('/', $value->projeto_data)));
          $value->dia_atraso =  $this->difDatas($value->projeto_data);

        }

 
        // orenar array pela data
        $sorted = $projetos->sortBy('projeto_data')->values()->all();

        //voltando a data para pt-br
        foreach($projetos as $key => $value){
          $value->projeto_data =  implode('/', array_reverse(explode('-', $value->projeto_data)));
            
        }

       
        return  $sorted;
    }

    public function getProfessores() {
        $idunidade = Auth::user()->idunidade;

        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }


        $profs = User::select('users.id', 'users.name','users.avatar')
                ->leftJoin('user_dados', 'users.id', 'user_dados.user_id')
                ->where('users.idunidade', $idunidade)
                ->wherein('users.role', ['professor', 'admin', 'coordenador','adminfilial'])
                ->wherenotin('users.excluido', ['S'])
                ->where('user_dados.situacao', 'A')
                ->groupby('users.id')
                ->get();

        $professores = [];
        $professores[0]['id'] = 0;
        $professores[0]['name'] = 'Alunos sem carteira';
        $pfs = [];

        foreach ($profs as $key => $value) {
            $pfs[$key] = $profs[$key]['id'];
            $professores[$key + 1] = $profs[$key];
        }
        foreach ($professores as $key => $value) {
            $professores[$key]['total'] = $this->getNumeroAlunosProf($professores[$key]['id'], $pfs);
        }

        // orenar array pela data

        $sorted = collect($professores)->sortByDesc('total')->values()->all();

        return $sorted;
    }
    
    public function getIndicadorProjeto() {
        $idunidade = Auth::user()->idunidade;

        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }

        $sql="SELECT coalesce(count(*),0) as total, professor_id, pp.unidade_id, u.name,u.avatar FROM"
                . " prescricao_projeto as pp , prescricao_etapa as pe, "
                . "users as u WHERE pe.projeto_id = pp.id and pe.etapa_status = 'A' "
                . "and pp.unidade_id = $idunidade and pp.professor_id = u.id "
                . "group by pp.professor_id, pp.unidade_id, u.name order by pp.professor_id";
        $profs=DB::select($sql);
///////////////////////////////////////////////////////////////////////////////////////////////
        $sql="SELECT coalesce(count(*),0) as ttatraso, professor_id, pp.unidade_id FROM"
                . " prescricao_projeto as pp , prescricao_etapa as pe, "
                . "users as u WHERE pe.projeto_id = pp.id and pe.etapa_status = 'A' "
                . "and pp.unidade_id = $idunidade and pp.professor_id = u.id "
                . "and pe.etapa_data < " . date('Y-m-d') . " group by pp.professor_id, pp.unidade_id, u.name order by pp.professor_id";
        $profsAtraso=DB::select($sql);
        
        foreach ($profs as $key=>$value){
                $profs[$key]->percentual=($profsAtraso[$key]->ttatraso * 100) / $profs[$key]->total;
                $profs[$key]->ttatraso=$profsAtraso[$key]->ttatraso;
        }

        return $profs;
    }  
    
    
    public function getRotatividade() {
        $idunidade = Auth::user()->idunidade;

        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }

        $sql="SELECT count(*) as total ,cli.idaluno, u.name FROM cliente_log cli , users u WHERE cli.idunidade = 80 and cli.idunidade =u.idunidade and cli.idaluno =u.id group by u.id ";
        
        
        $profs=DB::select($sql);
///////////////////////////////////////////////////////////////////////////////////////////////
        $sql="SELECT coalesce(count(*),0) as ttatraso, professor_id, pp.unidade_id FROM"
                . " prescricao_projeto as pp , prescricao_etapa as pe, "
                . "users as u WHERE pe.projeto_id = pp.id and pe.etapa_status = 'A' and u.role not in('prospect','cliente') "
                . "and pp.unidade_id = $idunidade and pp.professor_id = u.id "
                . "and pe.etapa_data < " . date('Y-m-d') . " group by pp.professor_id, pp.unidade_id, u.name order by pp.professor_id";
        $profsAtraso=DB::select($sql);
        
        foreach ($profs as $key=>$value){
                //$profs[$key]->percentual=($profsAtraso[$key]->ttatraso * 100) / $profs[$key]->total;
                //$profs[$key]->ttatraso=$profsAtraso[$key]->ttatraso;
        }

        return $profs;
    }     
    
    public function getDesativados() {
        
        $idunidade = Auth::user()->idunidade;

        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        
        $desativados = Logcliente::select('users.id', 'users.name','cliente_log.idunidade','cliente_log.motivo','cliente_log.dtalteracao', 'p.name as professor')
                ->leftJoin('users', 'cliente_log.idaluno', 'users.id')
                ->leftJoin('users as p', 'cliente_log.idprofessor', 'p.id')
                ->where('cliente_log.idunidade', $idunidade)
                ->whereMonth('cliente_log.dtalteracao', '=', date('m'))
                ->whereYear('cliente_log.dtalteracao', '=', date('Y'))
                ->distinct('users.id')
                ->get();

       
        foreach($desativados as $key=>$value){
            $desativados[$key]->name=  ucwords(strtolower($desativados[$key]->name));
            $desativados[$key]->dtalteracao=  date('d/m/y', strtotime($desativados[$key]->dtalteracao));
            
            $professor = explode(" ", $desativados[$key]->professor);
            $desativados[$key]->professor= $professor[0]; 
            
        }
        
        return $desativados;
    }

    public function GetNomeMes($mes) {

        switch ($mes) {
            case '01':
                $extenso = 'Jan';
                break;
            case '02':
                $extenso = 'Fev';
                break;
            case '03':
                $extenso = 'Mar';
                break;
            case '04':
                $extenso = 'Abr';
                break;
            case '05':
                $extenso = 'Mai';
                break;
            case '06':
                $extenso = 'Jun';
                break;
            case '07':
                $extenso = 'Jul';
                break;
            case '08':
                $extenso = 'Ago';
                break;
            case '09':
                $extenso = 'Set';
                break;
            case '10':
                $extenso = 'Out';
                break;
            case '11':
                $extenso = 'Nov';
                break;
            case '12':
                $extenso = 'Dez';
                break;
        }


        return $extenso;
    }

    public function getGraficosC3() {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }



        $dts = [];
        $dtx = date('Y-m');
        $dts[0] = substr($dtx, 0, 4) . "-" . substr($dtx, 5, 2);
        $i = 1;
        $encerra = [];
        $ativa = [];
        $meses = [];
        while ($i <= 13) {

            /*$totalEncerra = DB::select("SELECT coalesce(count(id),0) as total  "
                            . "FROM cliente_log "
                            . " where situacao = 'ENCERRADA'"
                            . " and idunidade = " . $idunidade
                            . " and year(dtalteracao) = " . substr($dtx, 0, 4)
                            . " and month(dtalteracao) = " . substr($dtx, 5, 2)
                            . " and idunidade = " . $idunidade
                            . " group by year(dtalteracao),month(dtalteracao)");*/

            $totalEncerra = DB::select("SELECT coalesce(SUM(desativado),0) as total  "
                            . "FROM resultado_mes "
                            . " where "
                            . " idunidade = " . $idunidade
                            . " and ano = " . substr($dtx, 0, 4)
                            . " and mes = " . substr($dtx, 5, 2)
                            . " group by ano,mes");

            if (isset($totalEncerra[0]->total)):
                $encerra[$i] = $totalEncerra[0]->total;
            else:
                $encerra[$i] = 0;
            endif;

            $totalAtiva = DB::select("SELECT coalesce(SUM(ativado),0) as total  "
                            . "FROM resultado_mes "
                            . " where "
                            . " idunidade = " . $idunidade
                            . " and ano = " . substr($dtx, 0, 4)
                            . " and mes = " . substr($dtx, 5, 2)
                            . " group by ano,mes");
/*            $totalAtiva = DB::select("SELECT count(id) AS total , year(dtalteracao) ano, month(dtalteracao) "
                            . "FROM cliente_log"
                            . "  where situacao = 'ATIVA'"
                            . " and idunidade = " . $idunidade
                            . " and year(dtalteracao) = " . substr($dtx, 0, 4)
                            . " and month(dtalteracao) = " . substr($dtx, 5, 2)
                            . " group by year(dtalteracao),month(dtalteracao)");*/
            if (isset($totalAtiva[0]->total)):
                $ativa[$i] = $totalAtiva[0]->total;
            else:
                $ativa[$i] = 0;
            endif;

            $dts[$i] = date('Y-m', strtotime("-1 month", strtotime($dtx)));
            $meses[$i] = $this->GetNomeMes(substr($dtx, 5, 2));
            $dtx = $dts[$i];


            $i++;
        }

        /* $totalEncerra=DB::select("SELECT count(id) as total , year(dtalteracao) ano, month(dtalteracao) "
          . "FROM cliente_log "
          . " where situacao = 'ENCERRADA'"
          . " and idunidade = " . $idunidade
          . " and year(dtalteracao) = 2018"// . substr ( $dtx , 0 ,4 )
          . " and month(dtalteracao) = 04" //. substr ( $dtx , 5 ,2 )
          . " and idunidade = " . $idunidade
          . " group by year(dtalteracao),month(dtalteracao)"); */



        $ttDesativa = [];
        $resultado = [];

        foreach ($ativa as $key => $value) {
            $tt[$key] = $encerra[$key] * -1;
            $resultado[$key] = $ativa[$key] - $encerra[$key];
        }

        $dados1Array = ['data1', $tt[12], $tt[12], $tt[11], $tt[10], $tt[9], $tt[8], $tt[7], $tt[6], $tt[5], $tt[4], $tt[3], $tt[2], $tt[1]];
        $dados2Array = ['data2', $ativa[13], $ativa[12], $ativa[11], $ativa[10], $ativa[9], $ativa[8], $ativa[7], $ativa[6], $ativa[5], $ativa[4], $ativa[3], $ativa[2], $ativa[1]];
        $dados3Array = ['data3', $resultado[13], $resultado[12], $resultado[11], $resultado[10], $resultado[9], $resultado[8], $resultado[7], $resultado[6], $resultado[5], $resultado[4], $resultado[3], $resultado[2], $resultado[1]];
        $dados5Array = [ $meses[13], $meses[12], $meses[11], $meses[10], $meses[9], $meses[8], $meses[7], $meses[6], $meses[5], $meses[4], $meses[3], $meses[2], $meses[1]];


        return compact('dados1Array', 'dados2Array', 'dados3Array', 'dados5Array');
    }

    public function getGraficosC32() {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }

        $dts = [];
        $dtx = date('Y-m');
        $dts[0] = substr($dtx, 0, 4) . "-" . substr($dtx, 5, 2);
        $i = 1;
        $encerra = [];
        $ativa = [];
        $meses = [];


        /*$profs = User::select('users.id', 'users.name')
                ->leftJoin('user_dados', 'users.id', 'user_dados.user_id')
                ->where('users.idunidade', $idunidade)
                ->wherein('users.role', ['professor', 'admin', 'adminfilial'])
                ->wherenotin('users.excluido', ['S'])
                ->where('user_dados.situacao', 'A')
                ->groupby('users.id')
                ->get();*/

        $sql="SELECT distinct(idprofessor) as id, coalesce(u.name,'Professor sem carteira') as name, ud.situacao FROM "
                . " carteira_professor c left join users u on c.idprofessor = u.id "
                . " left join user_dados ud on ud.user_id =u.id and u.idunidade = ud.idunidade"
                . " and ud.situacao in('A') where c.idunidade = " . $idunidade
                . " group by u.name order by c.idprofessor ";
        
        $profs=DB::select($sql);
        $pfs = [];
        foreach ($profs as $key => $value) {
            $pfs[$key] = $profs[$key]->name;
        }
        
        
        
        
        $resultado = [];

        foreach ($ativa as $key => $value) {
            $ttx[$key] = $encerra[$key] * -1;
            $resultado[$key] = $ativa[$key] - $encerra[$key];
        }

        foreach ($profs as $key => $value) {
            $sql = "SELECT (ativado - desativado ) as total ,ano,mes,idprofessor  "
                    . "FROM resultado_mes "
                    . " where "
                    . " idprofessor =  " . $profs[$key]->id
                    . " group by ativo,ano,mes,idprofessor"
                    . " order by ano desc, mes desc";

            $ttx = DB::select($sql);
            $tt = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
            foreach ($ttx as $key1 => $value) {
                $tt[$key1] = $ttx[$key1]->total;
            }

            $dados1Array[$key] = [ucwords(strtolower($profs[$key]->name)), $tt[12], $tt[11], $tt[10], $tt[9], $tt[8], $tt[7], $tt[6], $tt[5], $tt[4], $tt[3], $tt[2], $tt[1], $tt[0]];
        }

        return compact('dados1Array', 'tt');
    }

    public function ajustaResultado($unidade) {


        $profs = User::select('users.id', 'users.name')
                ->leftJoin('user_dados', 'users.id', 'user_dados.user_id')
                ->where('users.idunidade', $unidade)
                ->wherein('users.role', ['professor', 'admin', 'coordenador','adminfilial'])
               // ->where('users.id', 114)
                ->wherenotin('users.excluido', ['S'])
                ->where('user_dados.situacao', 'A')
                ->groupby('users.id')
                ->get();

        $professores = [];
        $professores[0]['id'] = 0;
        $professores[0]['name'] = 'Alunos sem carteira';
        $pfs = [];
        foreach ($profs as $key2 => $value) {

            $prof = $profs[$key2]->id;
            
            $sql = "SELECT count(*) as tt , year(dtinclusao) as ano ,month(dtinclusao) as mes,idprofessor FROM"
                    . " carteira_professor where idprofessor = " . $prof
                    . " group by year(dtinclusao),month(dtinclusao),"
                    . " idprofessor order by year(dtinclusao),month(dtinclusao)";
            
            $lista = DB::select($sql);
            
            $ant=0;
            foreach ($lista as $k => $value1) {
                $lista[$k]->ativado=$lista[$k]->tt;
                $lista[$k]->tt = $lista[$k]->tt + $ant;
//                $lista[$k]->tt = $lista[$k]->tt + $ant;
                $ant = $lista[$k]->tt;
                //echo $ant . "|";
                $sql = "SELECT * from resultado_mes where"
                        . " idunidade = " . $unidade
                        . " and ano = " . $lista[$k]->ano
                        . " and mes = " . $lista[$k]->mes
                        . " and idprofessor = " . $lista[$k]->idprofessor;

                $tts = DB::select($sql);
                if (sizeof($tts) <= 0) :
                    $tot = 1;
                    $dataCvempresa['idunidade'] = $unidade;
                    $dataCvempresa['ano'] = $lista[$k]->ano;
                    $dataCvempresa['mes'] = $lista[$k]->mes;
                    $dataCvempresa['idprofessor'] = $lista[$k]->idprofessor;
                    $dataCvempresa['ativo'] = $ant;
                    $dataCvempresa['ativado'] = $lista[$k]->ativado;
                    $cvempresa = new Resultadomes();
                    if ($cvempresa->create($dataCvempresa)):
                    else:
                    endif;
                else:
                    Resultadomes::where('idprofessor', $lista[$k]->idprofessor)->where('ano', $lista[$k]->ano)->where('mes', $lista[$k]->mes)->update(['ativo' => $ant,'ativado' => $lista[$k]->ativado]);
                endif;




                //echo $lista[$k]->tt . "|";
            }

           /* $sql = "SELECT idunidade, idaluno, idprofessor, month(dtinclusao) mes, year(dtinclusao) ano FROM carteira_professor where"
                    . " idunidade = " . $unidade . " and idprofessor = " . $prof . " order by"
                    . " year(dtinclusao), month(dtinclusao)";
            $total = DB::select($sql);
            $tot = 0;
            $vss = "";
            $vss1 = "";

            foreach ($total as $key1 => $value1) {
                $total[$key1]->idprofessor = $prof;
                $sql = "select ativo from resultado_mes where id = (SELECT max(id) ult from resultado_mes where"
                        . " idunidade = " . $unidade
                        . " and idprofessor = " . $prof . ")";
//                    . " and idprofessor = " . $total[$key1]->idprofessor . ")";



                $anterior = DB::select($sql);
                $ant = 0;
                if (sizeof($anterior) > 0) {
                    $ant = $anterior[0]->ativo;
                }

                $sql = "SELECT * from resultado_mes where"
                        . " idunidade = " . $unidade
                        . " and ano = " . $total[$key1]->ano
                        . " and mes = " . $total[$key1]->mes
                        . " and idprofessor = " . $prof;

                $tts = DB::select($sql);


                if (sizeof($tts) <= 0) :
                    $tot = 1;
                    $dataCvempresa['idunidade'] = $unidade;
                    $dataCvempresa['ano'] = $total[$key1]->ano;
                    $dataCvempresa['mes'] = $total[$key1]->mes;
                    $dataCvempresa['idprofessor'] = $prof;
                    $dataCvempresa['ativo'] = $tot;
                    $cvempresa = new Resultadomes();
                    if ($cvempresa->create($dataCvempresa)):
                    else:
                    endif;
                else:
                    Resultadomes::where('idprofessor', $prof)->where('ano', $total[$key1]->ano)->where('mes', $total[$key1]->mes)->update(['ativo' => $tts[0]->ativo + 1]);
                endif;
            }*/
        }

    }

    public function getNumeroAlunosProf($id, $profs) {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }


//        if ($id > 0):
        $sql = "SELECT count(t.id) as tt FROM carteira_professor t, users u WHERE"
                . " u.id = t.idaluno and u.excluido not in('S') and u.role in('cliente') and t.idprofessor = " . $id . " and t.idunidade = " . $idunidade;
        /*            $sql = "SELECT count(t.treino_id) as tt FROM treino_musculacao t, users u WHERE t.treino_atual = 1 "
          . " and u.id = t.aluno_id and u.excluido not in('S') and t.professor_id = " . $id . " and t.unidade_id = " . $idunidade; */

        /*      else:
          $sql = "SELECT count(id) as tt  FROM users where role in ('cliente') and idunidade =  $idunidade and"
          . " id not in (select aluno_id from treino_musculacao where treino_atual = '1' "
          . " and idunidade = $idunidade)";
          /*            $sql = "SELECT count(id) as tt  FROM users where role in ('cliente') and idunidade =  $idunidade and"
          . " id not in (select aluno_id from treino_musculacao where treino_atual = '1' "
          . " and idunidade = $idunidade)";

          endif; */



        $total = DB::select($sql);

        $tot = $total[0]->tt;

        return $tot;
    }

    public function getTotalProspects() {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        $totalProsps = DB::select(
                        "select coalesce(count(id),0) as ttcli, role from users  "
                        . " where excluido not in('S') and idunidade = " . $idunidade
                        . " group by role "
        );
        $ttcli = 0;
        foreach ($totalProsps as $totalProsp) {
            if ($totalProsp->role == 'prospect') {
                $ttcli = $totalProsp->ttcli;
            }
            if ($totalProsp->role == 'cliente') {
                $ttcli1 = $totalProsp->ttcli;
            }
        }



        $dadosProspects = DB::select(
                        "select count(id) as totalprospects from users  "
                        . " where excluido not in('S') and idunidade = " . $idunidade
                        . " and role in('prospect','cliente')"
                        . " and EXTRACT(YEAR FROM created_at) = EXTRACT(YEAR FROM CURDATE()) "
                        . " and EXTRACT(MONTH FROM created_at) = EXTRACT(MONTH FROM CURDATE()) "
        );
        foreach ($dadosProspects as $dadosProspect) {
            $totais['totalprospects'] = $dadosProspect->totalprospects;
            $totais['perc'] = number_format(($dadosProspect->totalprospects * 100) / ($ttcli + $ttcli1), 2, ',', '.');
            $totais['total'] = $ttcli + $ttcli1;
        }
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $dadosClientes = DB::select("select count(id) total from users  "
                        . " where excluido not in('S') and idunidade = " . $idunidade
                        . " and role in('cliente')"
                        . " and EXTRACT(YEAR FROM created_at) = EXTRACT(YEAR FROM CURDATE()) "
                        . " and EXTRACT(MONTH FROM created_at) = EXTRACT(MONTH FROM CURDATE()) ");
        foreach ($dadosClientes as $dadosCliente) {
            $totalclientes['total'] = $dadosCliente->total;
            $totalclientes['perc'] = number_format(($dadosCliente->total * 100) / $ttcli1, 2, ',', '.');
            $totalclientes['totalcli'] = $ttcli1;
        }


        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $totalDesats = DB::select(
                        "select coalesce(count(id),0) as ttdes from cliente_log  "
                        . " where idunidade = " . $idunidade
                        . " and EXTRACT(YEAR FROM dtalteracao) = EXTRACT(YEAR FROM CURRENT_DATE()) "
                        . " and EXTRACT(MONTH FROM dtalteracao) = EXTRACT(MONTH FROM CURRENT_DATE()) "
                );
                        

        $ttdes = 0;
        foreach ($totalDesats as $totalDesat) {
            $ttdes = $totalDesat->ttdes;
        }


        $dadosDesativados = DB::select("select count(u.id) total from cliente_log as u "
                        . " where u.idunidade = " . $idunidade
                        . " and u.situacao = 'ENCERRADA'"
                        . " and EXTRACT(YEAR FROM u.dtalteracao) = EXTRACT(YEAR FROM CURRENT_DATE()) "
                        . " and EXTRACT(MONTH FROM u.dtalteracao) = EXTRACT(MONTH FROM CURRENT_DATE()) ");
        foreach ($dadosDesativados as $dadosDesativado) {
            $totaldesativados['total'] = $dadosDesativado->total;
            $totaldesativados['perc'] = number_format(($dadosDesativado->total * 100) / $ttcli1, 2, ',', '.');
        }

        return response()->json(compact('totais', 'totalclientes', 'totaldesativados'));
    }

    public function getTotalLogins() {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        $dtHj = date('Y-m-d');
        $dtSemana = date('Y-m-d', strtotime("-7 days"));
        $totalogins = DB::select("select count(id) total from logusuario  "
                        . " where dtlogin between '" . $dtSemana
                        . "' and '" . $dtHj
                        . "' and tplogin = 2"
                        . " and idunidade = " . $idunidade);

        return response()->json(compact('totalogins'));
    }

    public function getTurmasProf($id) {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }



        $turma = [];
        if ($id > 0):
            $sql = "SELECT x.name as nomeprof , t.professor_id, t.aluno_id, u.name, DATE_FORMAT(t.treino_revisao, '%d/%m/%y') as treino_revisao FROM treino_musculacao t , "
                    . "users u, users x WHERE t.treino_atual =1 and u.id = t.aluno_id "
                    . " and t.professor_id = x.id"
                    . " and u.excluido not in ('S') "
                    . " and u.role not in ('prospect') "
                    . " and t.professor_id =  $id and unidade_id = $idunidade order by u.name";
            $turma = DB::select($sql);

        endif;



        $turma1 = [];
        if ($id == 0) {

            $sql = "SELECT id as aluno_id, name  FROM users where role not in ('prospect') and u.excluido not in ('S') and idunidade =  $idunidade and"
                    . " id not in (select aluno_id from treino_musculacao where treino_atual = '1' "
                    . " and idunidade = $idunidade) order by name";

            $turma1 = DB::select($sql);
        }


        $turma = array_merge($turma, $turma1);

        
        




        foreach ($turma as $key => $value) {
            $turma[$key]->url = 'admin/prescricao/nova/' . $turma[$key]->aluno_id;
        }



        return response()->json(compact('turma'));
    }

    public function getCarteiraProfessor($id) {        
        
        
        
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        
        
        $turma = [];
        if ($id > 0) { 
            
            $sql = "SELECT x.name as nomeprof , t.idprofessor, t.idaluno as aluno_id, u.name, u.role  FROM carteira_professor t , "
                    . "users u, users x WHERE "
                    . " t.idprofessor = x.id"
                    . " and t.idaluno = u.id"
                    . " and u.excluido not in ('S') "
                    . " and u.role not in ('prospect') "
                    . " and t.idprofessor =  $id and t.idunidade = $idunidade order by u.name";

            $turma = DB::select($sql);
        }

        $turma1 = [];
        if ($id == 0) {

            $sql = "SELECT CASE WHEN t.idprofessor = 0 THEN 'Alunos sem carteira' WHEN t.idprofessor  > 0 THEN 'ok' END as nomeprof, t.idprofessor, t.idaluno as aluno_id, u.name , u.role FROM users u , "
                    . " carteira_professor t "
                    . " WHERE "
                    . " t.idaluno = u.id"
                    . " and u.excluido not in ('S') "
                    . " and u.role not in ('prospect') "
                    . " and t.idprofessor =  $id and t.idunidade = $idunidade order by u.name";

            $turma1 = DB::select($sql);
        }


        $turma = array_merge($turma, $turma1);

        foreach ($turma as $key => $value) {
            $turma[$key]->url = 'admin/prescricao/nova/' . $turma[$key]->aluno_id;
        }

        return response()->json(compact('turma'));
    }

    public function upTurma($id, $professor) {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }

        if (Carteiraprofessor::where('idaluno', $id)
                        ->where('idunidade', $idunidade)
                        ->update(['idprofessor' => $professor])) {
            $retorno['cod'] = '0';
            $retorno['text'] = 'Turma atualizada com sucesso!';
        } else {
            $retorno['cod'] = '-1';
            $retorno['text'] = 'Ocorreu um erro ao atualizar aula, tente novamente!';
        }
        /*        if (TreinoMusculacao::where('aluno_id', $id)
          ->where('unidade_id', $idunidade)
          ->where('treino_atual', 1)
          ->update(['professor_id' => $professor])) {
          $retorno['cod'] = '0';
          $retorno['text'] = 'Turma atualizada com sucesso!';
          } else {
          $retorno['cod'] = '-1';
          $retorno['text'] = 'Ocorreu um erro ao atualizar aula, tente novamente!';
          } */





        return response()->json(compact('retorno'));
    }

    public function getRevisaoTreino() {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }



        $revisao = DB::select("select u.id, u.name,DATE_FORMAT(tm.treino_revisao, '%d/%m') as treino_revisao, coalesce(fun.name ,'') as professor,carteira_professor.idprofessor as professor1,  fun.id as idprofessor, "
                        . "  DATEDIFF(tm.treino_revisao,current_date) as dtf from treino_musculacao tm left join users u  "
                        . " on u.id = tm.aluno_id "
                        . " and u.role in ('cliente')"
                        . " and u.excluido not in ('S')"
                        . " and u.idunidade = tm.unidade_id"
                        . " left join carteira_professor on"
                        . " carteira_professor.idaluno = tm.aluno_id and"
                        . " carteira_professor.idunidade = tm.unidade_id"
                        . " left join users fun on"
                        . " fun.id = carteira_professor.idprofessor and"
                        . " fun.role not in('cliente')"
                        . " where u.id = tm.aluno_id"
                        . " and tm.treino_atual = 1 and tm.unidade_id = " . $idunidade
                        . " and tm.treino_revisao between '0001-01-01' and '" . date('Y-m-d', strtotime('+10 days')) . "'"
                        . " order by tm.treino_revisao ");
        //  $revisao = $revis->toArray();

        foreach ($revisao as $key => $value) {
            $revisao[$key]->url = 'admin/prescricao/nova/' . $revisao[$key]->id;
        }


        return response()->json(compact('revisao'));
    }

    public function getTurmas() {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        $revisao = DB::select("select u.id, u.name,DATE_FORMAT(tm.treino_revisao, '%d/%m') as treino_revisao, coalesce(fun.name ,'') as professor, fun.id as idprofessor, "
                        . "  DATEDIFF(tm.treino_revisao,current_date) as dtf from treino_musculacao tm left join users u  "
                        . " on u.id = tm.aluno_id and "
                        . " u.idunidade = tm.unidade_id"
                        . " left join users fun on"
                        . " fun.id = tm.professor_id and"
                        . " fun.role not in('cliente')"
                        . " where u.id = tm.aluno_id"
                        . " and treino_atual = 1 and unidade_id = " . $idunidade
                        . " and tm.treino_revisao between '0001-01-01' and '" . date('Y-m-d', strtotime('+10 days')) . "'"
                        . " order by tm.treino_revisao ");
        //  $revisao = $revis->toArray();


        foreach ($revisao as $key => $value) {
            $revisao[$key]->url = 'admin/prescricao/nova/' . $revisao[$key]->id;
        }



        return response()->json(compact('revisao'));
    }

    public function getAgendaPrograma() {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        $agendas = DB::select("select tm.stagenda, u.id, u.name,DATE_FORMAT(tm.updated_at, '%d/%m/%y') as realizado_d,DATE_FORMAT(tm.updated_at, '%H:%i') as realizado_h ,DATE_FORMAT(tm.dtagenda, '%d/%m/%y') as dtagenda,TIME_FORMAT(tm.hragenda, '%H:%i') as hragenda, tm.nmagenda ,"
                        . "  DATEDIFF(tm.dtagenda ,current_date) as dtf from agenda_alunoprograma tm left join users u  "
                        . " on u.id = tm.idaluno and "
                        . " u.idunidade = tm.idunidade"
                        . " and u.role not in ('prospect')"
                        . " where u.id = tm.idaluno and"
                        . " tm.idunidade = " . $idunidade
                        . " order by tm.dtagenda ");

        $vet = Treinomusculacao::select('users.name')
                ->leftJoin('users', 'users.id', '=', 'treino_musculacao.professor_id')
                ->where('treino_musculacao.aluno_id', 76)
                ->where('treino_musculacao.treino_atual', 1)
                ->first();

        foreach ($agendas as $key => $value) {
            $agendas[$key]->url = 'admin/prescricao/nova/' . $agendas[$key]->id;

            $vet = Treinomusculacao::select('users.name as nomeprof')
                    ->leftJoin('users', 'users.id', '=', 'treino_musculacao.professor_id')
                    ->where('treino_musculacao.aluno_id', $agendas[$key]->id)
                    ->where('treino_musculacao.treino_atual', '1')
                    ->first();
            $professor = explode(" ", $vet['nomeprof']);
            $agendas[$key]->nomeprof = $professor[0];
        }
        return response()->json(compact('agendas', 'vet'));
    }

    function getDataAula($idaula) {
        $gfm = Gfm::select('gfm.*')
                        ->where('gfm.id', $idaula)->first();

        $hrHj = date('H:i');
        $dtHj = date('Y-m-d');
        $ano = substr($dtHj, 0, 4);
        $mes = substr($dtHj, 5, 2);
        $dia = substr($dtHj, 8, 2);
        $ds = date("w", mktime($ano, $dia, $mes));
        $diasemana = $ds + 1;

        $dias = explode(",", $gfm->diasemana);
        $dif = 0;
        
        $datas = [];
        foreach ($dias as $key => $value) {
            if ($dias[$key] < $diasemana) {
                $dif = (intval($dias[$key]) - intVal($diasemana)) + 7;
            }
            if ($dias[$key] > $diasemana) {
                $dif = ($dias[$key] - $diasemana);
            }
            if ($dias[$key] == $diasemana) {
                if (date('H:i:s') <= date('H:i:s', strtotime($gfm->hora_inicio))) {
                    $dif = ($dias[$key] - $diasemana);
                } else {
                    $dif = ($dias[$key] - $diasemana) + 7;
                }
                $hrInicio = $gfm->hora_inicio;
                $hrFim = $horaNova = strtotime("$hrInicio + $gfm->duracao  minutes");
            }

            $dt = date('Y-m-d', strtotime("+" . $dif . " days"));

            $dateStart = new \DateTime(date($dt . ' ' . $gfm->hora_inicio));
            $dateNow = new \DateTime(date('Y-m-d H:i:s'));
            $dateDiff = $dateStart->diff($dateNow);
        }

        return $dt;
    }

    public function getAulasDiaSemana() {

        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        $diaAtual = date('Y-m-d');
        $diasemana = (date('w', strtotime($diaAtual))) + 1;

        //'gfm_reserva.n_reservas', 'gfm_reserva.id as reserva_id')
        $gfms = Gfm::select('gfm.id', 'gfm.diasemana', 'gfm.hora_inicio', 'gfm.privado', 
            'gfm.capacidade', 'users.name as nmprofessor', 'programa.nmprograma', 'local.nmlocal')
                ->leftJoin('users', 'users.id', '=', 'gfm.idfuncionario')
                ->leftJoin('programa', 'programa.id', '=', 'gfm.idprograma')
                ->leftJoin('local', 'local.id', '=', 'gfm.idlocal')
                ->where('gfm.idunidade', $idunidade)
                ->orderBy('gfm.hora_inicio', 'ASC')
                ->get();

        foreach ($gfms as $key => $value) {
            $dtAula = $this->getDataAula($gfms[$key]['id']);
            $gfms[$key]['dtaula'] = $dtAula;
            $gfms[$key]['hora_inicio'] = date('H:i', strtotime($gfms[$key]['hora_inicio']));
            $reservas = Reserva::select('n_reservas')->where('idgfm', $gfms[$key]['id'])->where('dtaula', $dtAula)->get();
            $gfms[$key]['n_reservas'] = isset($reservas[0]['n_reservas']) ? $reservas[0]['n_reservas'] : $gfms[$key]['capacidade'];
            $gfms[$key]['ocupacao'] = $gfms[$key]['capacidade'] - $gfms[$key]['n_reservas'];
            $reservou = LogReserva::select('idaluno')->where('idaluno', 0)->where('idgfm', $gfms[$key]['id'])->where('dtaula', $dtAula)->get();
            if (sizeof($reservou) > 0) {
                $gfms[$key]['selecionou'] = 'S';
            } else {
                $gfms[$key]['selecionou'] = 'N';
            }
        }

        return response()->json(compact('gfms', 'diasemana'));
    }

    public function getEmpresa($id) {
        $cvempresa = CVEmpresa::select('id', 'nmempresa', 'cnpj', 'percdesconto')->where('id', $id)->get();

        return response()->json(compact('cvempresa'));
    }

    public function addEmpresa() {
        //if(Request::ajax()) {
        $data = Input::all();
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        $dataCvempresa['idunidade'] = $idunidade;
        $dataCvempresa['nmempresa'] = $data['nmempresa'];
        $dataCvempresa['cnpj'] = $data['cnpj'];
        $dataCvempresa['percdesconto'] = $data['percdesconto'];

        $cvempresa = new CVEmpresa();
        if ($cvempresa->create($dataCvempresa)):
            $retorno['title'] = 'Sucesso!';
            $retorno['type'] = 'success';
            $retorno['text'] = 'Empresa cadastrada com sucesso!';
            return $retorno;
        else:
            $retorno['title'] = 'Erro!';
            $retorno['type'] = 'error';
            $retorno['text'] = 'Erro ao cadastrar empresa!';
            return $retorno;
        endif;
        //}
    }

    public function upEmpresa() {
        if (Request::ajax()) {
            $data = Input::all();
            $idunidade = Auth::user()->idunidade;
            if (session()->get('id_unidade')) {
                $idunidade = session()->get('id_unidade');
            }

            $dataCvempresa['idunidade'] = $idunidade;
            $dataCvempresa['id'] = $data['cvempresa_id'];
            $dataCvempresa['nmempresa'] = $data['nmempresa'];
            $dataCvempresa['cnpj'] = $data['cnpj1'];
            $dataCvempresa['percdesconto'] = $data['percdesconto'];

            $local = new CVEmpresa();
            if ($local->where('id', '=', $data['cvempresa_id'])->update($dataCvempresa)):
                $retorno['title'] = 'Sucesso!';
                $retorno['type'] = 'success';
                $retorno['text'] = 'Empresa atualizada com sucesso!';
            else:
                $retorno['title'] = 'Erro!';
                $retorno['type'] = 'error';
                $retorno['text'] = 'Erro ao atualizar empresa!';
            endif;
            return $retorno;
        }
    }

    public function getListas() {
        $params = array(
            'apikey' => '3f25ba1ab90e1197d71ad95f3998e1d813cd642e'
        );
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }

        $client = new SoapClient('http://api.e-goi.com/v2/soap.php?wsdl');
        $listas = $client->getLists($params);




        return response()->json(compact('listas'));
    }

    // DELETAR PSA
    public function deleteCvempresa($id) {
        // Local::where('id', $id)->delete();

        $local = new CVEmpresa();
        $local->where('id', '=', $id)->delete();
    }

}
