<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Programa;
use Hash;
use App\Http\Requests\ProgramaRequest;
use App\Services\TrataDadosService;

class ProgramaController extends Controller
{
    public function __construct(TrataDadosService $trataDadosService)
    {
        $this->trataDadosService = $trataDadosService;
    }
    public function index()
    {
        $headers = ['category' => 'Empresa', 'title' => 'programas'];
        $programas = Programa::all();
        return view('admin.programas.index', compact('programas', 'headers'));
    }

    public function edit($id)
    {
        $headers = ['category' => 'Empresa', 'title' => 'programas'];
        $programa = Programa::where('id', $id)->first();
        $sns=Programa::select('id', 'stcoletivo')->get();
        $selectedSns = $this->trataDadosService->listToSelectPessoas($sns);
       
        return view('admin.programas.edit', compact('programa', 'selectedSns', 'headers'));
    }

    public function create()
    {
        $headers = ['category' => 'Empresa', 'title' => 'programas'];
        $sns='';
        $selectedSns = $this->trataDadosService->listToSelectSns($sns);
      
        return view('admin.programas.create', compact('selectedSns', 'headers'));
    }

    public function update(ProgramaRequest $request, $id)
    {
        $data = $request->all();

        Programa::where('id', $id)->update(['nmprograma' => $data['nmprograma'],
          'stcoletivo' => $data['stcoletivo'],
          'idunidade' => 1
       ]);
     
        return redirect()->route('admin.programas.index');
    }

    public function store(ProgramaRequest $request)
    {
        $data = $request->all();
       
        Programa::create(['nmprograma' => $data['nmprograma'],
          'stcoletivo' => $data['stcoletivo'],
          'idunidade' => 1
              
      ]);
     
        return redirect()->route('admin.programas.index');
    }


    // public function create_user(){

          //  User::create( [
    //          'email' => 'kk@msn.com' ,
    //          'password' => Hash::make( '123456' ) ,
    //          'name' => 'KK Ruiz' ,
    //          'role' => 'admin'
    //         ] );
    // }
}
