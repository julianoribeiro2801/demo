<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Mensagem;


// Precisa para funcionar o combo
use Auth;
use App\Services\FilialService;

class PagamentosController extends Controller
{
    public function __construct(FilialService $filialService)
    {
        $this->filialService = $filialService;
    }
     
    public function index()
    {
        $headers = ['category' => 'Push', 'title' => 'Push'];
        $unidades_combo = $this->filialService->unidadesComboTop();
      

        return view('push.push', compact('headers', 'unidades_combo'));
    }
}
