<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
//use App\Http\Requests\NivelHabilidadeRequest;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use DB;
use Input;
// use Request;
use Image;
use File;

use App\Models\Unidade;
use App\Models\Processo;
use App\Services\SendPushService;

// Precisa para funcionar o combo
use Auth;
use App\Services\FilialService;


class ProcessoController extends Controller
{
    public function __construct(FilialService $filialService, SendPushService $pushService)
    {
        $this->filialService = $filialService;
        $this->pushService = $pushService;      
        
    }

    public function changeEmp($id_unidade)
    {
        $this->filialService->changeEmp($id_unidade);

        return redirect(url()->previous());
    }

    public function index()
    {
        $headers = ['category' => 'Empresa', 'title' => 'Unidades'];

        $unidades = Unidade::with(['parent', 'children', 'matriz'])->get();

        return view('empresas.index', compact('unidades', 'headers'));
    }


    public function getProcessos()
            
    {
        
        $idunidade = Auth::user()->idunidade;
        
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        $processos = Processo::select('id', 'nmprocesso', 'statusprocesso')->where('idunidade', $idunidade)->orderby('nmprocesso')->get();
        
        return $processos;
        
    }
    public function upProcesso() {
            //if(Request::ajax()) {
        $data = Input::all();

        $idunidade = Auth::user()->idunidade;

        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        $processo = Processo::select('id', 'nmprocesso', 'statusprocesso')
                    ->where('idunidade', $idunidade)
                    ->where('id', $data['processo'])
                    ->orderby('nmprocesso')->get();
        
        
        $stProc="";
        if($processo[0]->statusprocesso=='S'):
          $stProc='N';  
        else:
          $stProc='S';  
            
        endif;
        
        $dataProcesso['id']=$data['processo'];
        $dataProcesso['idunidade']=$idunidade;
        $dataProcesso['statusprocesso']=$stProc;
        $processo = new Processo();
        if ($processo->where('id', '=', $dataProcesso['id'])->update($dataProcesso)):
            $retorno['title'] = 'Sucesso!';
            $retorno['type'] = 'success';
            $retorno['text'] = 'Processo atualizado com sucesso!';
          return $retorno;
        else:
            $retorno['title'] = 'Erro!';
            $retorno['type'] = 'error';
            $retorno['text'] = 'Erro ao atualizar processo!';
           return $retorno;                    
        endif;
    }
}
