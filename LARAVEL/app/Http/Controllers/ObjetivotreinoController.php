<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB;
use Input;
// use Request;
use Image;
use File;

use App\Models\Unidade;
use App\Models\UnidadeDados;
use App\Models\Objetivotreino;
use App\Models\Nivelhabilidade;
use App\Models\UserDados;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

// Precisa para funcionar o combo
use Auth;
use App\Services\FilialService;

class ObjetivotreinoController extends Controller
{
    public function __construct(FilialService $filialService)
    {
        $this->filialService = $filialService;
    }

    public function changeEmp($id_unidade)
    {
        $this->filialService->changeEmp($id_unidade);

        return redirect(url()->previous());
    }

    public function index()
    {
        $headers = ['category' => 'Empresa', 'title' => 'Unidades'];

        $unidades = Unidade::with(['parent', 'children', 'matriz'])->get();

        return view('empresas.index', compact('unidades', 'headers'));
    }


    /*public function getObjetivosss($id) {
        $idunidade = Auth::user()->idunidade;
        if(session()->get('id_unidade')){
            $idunidade = session()->get('id_unidade');
        }
        $empresas=DB::select("select coalesce(sum(cva.vlrcompra),0) as totalvendas , cve.*,DATE_FORMAT(cve.dtvencimento, '%d/%m/%Y') as vencimento, s.nmsegmento, s.dsurlminiatura "
                             . " from cv_empresa as cve "
                             . " left join cv_alunocompra as cva "
                                                         . " on cva.idempresa = cve.id"
                                                         . " and cva.idunidade = cve.idunidade"
                                                         . " left join segmento s on cve.idsegmento = s.id"
                                                         . " where cve.idunidade = " . $idunidade
                                                         . " group by cve.id asc" );


        return response()->json(compact('empresas'));
    }*/


    public function getObjetivos($id)
    {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        $objetivotreino = Objetivotreino::select('id', 'nmobjetivo', 'idunidade')
                                ->where('idmodalidade', $id)
                                ->where('idunidade', $idunidade)->with('niveis')
                                ->get();
                
        //$objetivotreino = $objetivotreino->toArray();
                
        foreach ($objetivotreino as $key=>$value) {
            $objetivotreino[$key]['niveis'] = collect($objetivotreino[$key]['niveis'])->where('idunidade', $idunidade)->sortBy('id')->reverse()->toArray();
        }
                
                
        //return response()->json(compact('objetivotreino'));
        return  $objetivotreino;
    }
    public function getObjetivo($id)
    {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        $objetivotreino = Objetivotreino::select('id', 'nmobjetivo')
                                ->where('id', $id)
                                ->where('idunidade', $idunidade)->with('niveis')
                                ->get();
        return response()->json(compact('objetivotreino'));
        // return $objetivotreino;
        return response()->json(compact('objetivotreino'));
    }
       
    public function addObjetivo() {
        //if(Request::ajax()) {
        $data = Input::all();

        $validator = Validator::make(Input::all(), [
                    'nmobjetivo' => 'required|min:3'
                        ], [
                    'nmobjetivo.required' => 'O campo nome é obrigatório'

                        ]
        );
        if ($validator->fails()) {
            return response()->json($validator->errors()->getMessages(), 400);
        } else {
            $idunidade = Auth::user()->idunidade;
            if (session()->get('id_unidade')) {
                $idunidade = session()->get('id_unidade');
            }
            $dataObjetivo['idunidade'] = $idunidade;
            $dataObjetivo['nmobjetivo'] = $data['nmobjetivo'];
            $dataObjetivo['idmodalidade'] = $data['idmodalidade'];

            $objetivo = new Objetivotreino();
            if ($objetivo->create($dataObjetivo)):
                $retorno['title'] = 'Sucesso!';
                $retorno['type'] = 'success';
                $retorno['text'] = 'Objetivo cadastrado com sucesso!';
                return $retorno;
            else:
                $retorno['title'] = 'Erro!';
                $retorno['type'] = 'error';
                $retorno['text'] = 'Erro ao cadastrar objetivo!';
                return $retorno;
            endif;
        }
    }
    public function upObjetivo() {
        //if(Request::ajax()) {
        $data = Input::all();

        $validator = Validator::make(Input::all(), [
                    'nmobjetivo' => 'required|min:3'
                        ], [
                    'nmobjetivo.required' => 'O campo nome é obrigatório'

                        ]
        );
        if ($validator->fails()) {
            return response()->json($validator->errors()->getMessages(), 400);
        } else {
            $idunidade = Auth::user()->idunidade;
            if (session()->get('id_unidade')) {
                $idunidade = session()->get('id_unidade');
            }

            $dataObjetivo['nmobjetivo'] = $data['nmobjetivo'];

            $objetivo = new Objetivotreino();
            if ($objetivo->where('id', '=', $data['id'])->update($dataObjetivo)):
                $retorno['title'] = 'Sucesso!';
                $retorno['type'] = 'success';
                $retorno['text'] = 'Objetivo atualizado com sucesso!';
                return $retorno;
            else:
                $retorno['title'] = 'Erro!';
                $retorno['type'] = 'error';
                $retorno['text'] = 'Erro ao atualizar objetivo!';
                return $retorno;
            endif;
        }
    }

    public function upEmpresa()
    {
        //if(Request::ajax()) {
        $data = Input::all();
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
                        
        $dataCvempresa['idunidade'] = $idunidade;
        $dataCvempresa['id'] = $data['cvempresa_id'];
        $dataCvempresa['nmempresa'] = $data['nmempresa'];
        $dataCvempresa['cnpj'] = $data['cnpj1'];
        $dataCvempresa['percdesconto'] = $data['percdesconto'];
        $dataCvempresa['idsegmento'] = $data['idsegmento'];
        $dataCvempresa['dtvencimento'] =  $this->setData($data['dtvencimento']);
            
        $local = new CVEmpresa();
        if ($local->where('id', '=', $data['cvempresa_id'])->update($dataCvempresa)):
                $retorno['title'] = 'Sucesso!';
        $retorno['type'] = 'success';
        $retorno['text'] = 'Empresa atualizada com sucesso!'; else:
                $retorno['title'] = 'Erro!';
        $retorno['type'] = 'error';
        $retorno['text'] = 'Erro ao atualizar empresa!';
        endif;
        return $retorno;
        //}
    }

    // DELETAR objetivo
    public function deleteObjetivo($id)
    {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        
        $local1 = new Nivelhabilidade();
        $local1->where('idobjetivo', '=', $id)->where('idunidade','=',$idunidade)->delete();
        
        
        $local = new Objetivotreino();
        $local->where('id', '=', $id)->where('idunidade','=',$idunidade)->delete();
    }
}
