<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use App\Models\Atividade;
use App\Models\AtividadeBlacklist;
use App\Models\Cidade;
use App\Models\Estado;
use App\Models\Funcionario;
use App\Models\Local;
use App\Models\Unidade;
use App\Models\UnidadeDados;
use App\Models\UserDados;
use App\Models\User;
use App\Notifications\NewEmployerRegistred;
use App\Services\FilialService;

use Hash;
use App\Models\Moduloacademia;

use Auth;
use DB;
use File;
use Image;
use Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use Storage;

class EmpresasController extends Controller
{

    public function __construct()
    {

        setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
        date_default_timezone_set('America/Sao_Paulo');

    }
    public function getEstados() {
        $estados = Estado::select('id', 'nome', 'uf')->get();
        return $estados;
    }

    public function getCidades() {
        $data = Input::all();

        $cidades = Cidade::select('id', 'nome', 'estado')->where('estado', $data['estado'])->get();
        return $cidades;
    }

    public function getEmpresa() {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }

        $empresa = Unidade::select('unidade.logo', 'unidade.fantasia', 'unidade.razao_social', 'unidade_dados.*')
                        ->leftJoin('unidade_dados', 'unidade_dados.idunidade', '=', 'unidade.id')
                        ->where('unidade.id', $idunidade)
                        ->with(['dados', 'parent', 'children', 'matriz'])->get();
        return $empresa;
    }
    
    
    public function getmatriz($id) {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }

        $empresa = Unidade::select('users.name','unidade.logo', 'unidade.fantasia', 'unidade.razao_social', 'unidade_dados.*')
                        ->leftJoin('unidade_dados', 'unidade_dados.idunidade', '=', 'unidade.id')
                        ->leftJoin('users', 'users.id', '=', 'unidade.user_id')
                        ->where('unidade.id', $idunidade)
                        ->with(['dados', 'parent', 'children', 'matriz'])->get();
        return $empresa;
    }    
    

    public function modulos_ativos($id_unidade){
       
        $modulo_cores= Moduloacademia::where('id_unidade', $id_unidade)
        ->where('id_modulo',1)->where('status',1)->first(); 
        
        
        return (count($modulo_cores) > 0)  ? 'S': 'N';
       
    }


    public function getEmpresas() {
        $unidades = Unidade::select('unidade.*',  'users.name', 'users.email')
                ->JOIN('users', 'users.id', '=', 'unidade.user_id')
                ->where('unidade.parent_id', '0')
                ->where('unidade.situacao','>=', 0)
                ->orderby('unidade.parent_id')
                ->get();

        foreach ($unidades as $key => $value) {
            
            $unidades[$key]['cores'] = $this->modulos_ativos($unidades[$key]['id']);

            $unidades[$key]['dtcriacao']= date('d/m/Y', strtotime($unidades[$key]['created_at']));
            $unidades[$key]['ativos']= DB::table('users')->where('role','cliente')->where('idunidade', $unidades[$key]['id'])->count();
            $unidades[$key]['filiais'] = Unidade::select('unidade.*')
                    ->where('parent_id', $unidades[$key]['id'])
                    ->get();
            
            foreach ($unidades[$key]['filiais'] as $key1 => $value) {
                
                 $unidades[$key]['filiais'][$key1]['cores'] = $this->modulos_ativos($unidades[$key]['filiais'][$key1]['id']);

                 $unidades[$key]['filiais'][$key1]['dtcriacao']= date('d/m/Y', strtotime($unidades[$key]['filiais'][$key1]['created_at']));
                 $unidades[$key]['filiais'][$key1]['ativos']= DB::table('users')->where('role','cliente')
                 ->where('idunidade',  $unidades[$key]['filiais'][$key1]['id'])->count();
            }
            
            
        }
        
        return $unidades;
    }    
    
    
    public function mudaStatus() {
        
             $dataAll = Input::all(); 

        
            $empresa = new Unidade();
            if ($empresa->where('id', '=', $dataAll['id'])->update(['situacao' => $dataAll['situacao'] ])):
                $retorno['title'] = 'Sucesso!';
                $retorno['type'] = 'success';
                $retorno['text'] = 'Matriz atualizada com sucesso!';
            else:
                $retorno['title'] = 'Erro!';
                $retorno['type'] = 'error';
                $retorno['text'] = 'Erro ao atualizar matriz!';
            endif;
    
        return $retorno;
    }      
    
    public function mudaLicenca() {
        
             $dataAll = Input::all(); 

        
            $empresa = new Unidade();
            if ($empresa->where('id', '=', $dataAll['id'])->update(['licenca' => $dataAll['licenca'] ])):
                $retorno['title'] = 'Sucesso!';
                $retorno['type'] = 'success';
                $retorno['text'] = 'Licença atualizada com sucesso!';
            else:
                $retorno['title'] = 'Erro!';
                $retorno['type'] = 'error';
                $retorno['text'] = 'Erro ao atualizar licença!';
            endif;
    
        return $dataAll;
    }     
    
    
    public function upAdminMatriz() {
         // comentei somente para funcionar pq tinha dado erro na migracao do servidor testar habilitar de novo essa validacao do Request
        // if(Request::ajax()) {
        $data = Input::all();
        $validator = Validator::make(Input::all(), [
                    'fantasia' => 'required|min:3',
                    'razao_social' => 'required|min:3',
                    'idestado' => 'required|min:1',
                    'idcidade' => 'required|min:1'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors()->getMessages(), 400);
        } else {
            $idunidade = Auth::user()->idunidade;
            if (session()->get('id_unidade')) {
                $idunidade = session()->get('id_unidade');
            }

            $dataUnidade['fantasia'] = $data['fantasia'];
            $dataUnidade['razao_social'] = $data['razao_social'];
            //$dataUnidade['situacao'] = '0';


            $dataUnidadeDados['cnpj'] = isset($data['cnpj']) ? $data['cnpj'] : '';
            $dataUnidadeDados['inscricao_estadual'] = isset($data['inscricao_estadual']) ? $data['inscricao_estadual'] : '';
            $dataUnidadeDados['endereco'] = isset($data['endereco']) ? $data['endereco'] : '';
            $dataUnidadeDados['numero'] = isset($data['numero']) ? $data['numero'] : '';
            $dataUnidadeDados['cep'] = isset($data['cep']) ? $data['cep'] : '';
            $dataUnidadeDados['bairro'] = isset($data['bairro']) ? $data['bairro'] : '';
            $dataUnidadeDados['idestado'] = isset($data['idestado']) ? $data['idestado'] : '';
            $dataUnidadeDados['idcidade'] = isset($data['idcidade']) ? $data['idcidade'] : '';
            $dataUnidadeDados['telefone'] = isset($data['telefone']) ? $data['telefone'] : '';
            $dataUnidadeDados['celular'] = isset($data['celular']) ? $data['celular'] : '';
            $dataUnidadeDados['email'] = isset($data['email']) ? $data['email'] : '';
            $dataUnidadeDados['site'] = isset($data['site']) ? $data['site'] : '';

        $unidade = new Unidade();
        if ($unidade->where('id', '=', $data['id'])->update($dataUnidade)):            
                $retorno['title'] = 'Sucesso!';
                $retorno['type'] = 'success';
                $retorno['text'] = 'Empresa atualizada com sucesso!';
                return $retorno;
            else:
                $retorno['title'] = 'Erro!';
                $retorno['type'] = 'error';
                $retorno['text'] = 'Erro ao atualizar Empresa!';
                //return $retorno;
            endif;            

            $filialDados = new UnidadeDados();
            if ($filialDados->where('idunidade', '=', $data['id'])->update($dataUnidadeDados)):
                $retorno['title'] = 'Sucesso!';
                $retorno['type'] = 'success';
                $retorno['text'] = 'Empresa atualizada com sucesso!';
                return $retorno;
            else:
                $retorno['title'] = 'Erro!';
                $retorno['type'] = 'error';
                $retorno['text'] = 'Erro ao atualizar filial!';
                return $retorno;
            endif;
        }
      //  return $data;



    }    
    
    public function addAdminMatriz() {
        // comentei somente para funcionar pq tinha dado erro na migracao do servidor testar habilitar de novo essa validacao do Request
        // if(Request::ajax()) {
        $data = Input::all();
        $validator = Validator::make(Input::all(), [
                    'name' => 'required|min:3'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors()->getMessages(), 400);
        } else {

  
            
            $adm = User::create([
                        'email' => $data['email'],
                        'password' => Hash::make('123456'),
                        'name' => 'teste',
                        'role' => 'admin'
                    ]);


            $dataUnidade['user_id'] = $adm->id;
            $dataUnidade['parent_id'] = $data['parent_id'];
            $dataUnidade['fantasia'] = $data['fantasia'];
            $dataUnidade['razao_social'] = $data['razao_social'];
            $dataUnidade['situacao'] = '0';

            $dataUnidadeDados['user_id'] = $adm->id;
            $dataUnidadeDados['cnpj'] = isset($data['cnpj']) ? $data['cnpj'] : '';
            $dataUnidadeDados['inscricao_estadual'] = isset($data['inscricao_estadual']) ? $data['inscricao_estadual'] : '';
            $dataUnidadeDados['endereco'] = isset($data['endereco']) ? $data['endereco'] : '';
            $dataUnidadeDados['numero'] = isset($data['numero']) ? $data['numero'] : '';
            $dataUnidadeDados['cep'] = isset($data['cep']) ? $data['cep'] : '';
            $dataUnidadeDados['bairro'] = isset($data['bairro']) ? $data['bairro'] : '';
            $dataUnidadeDados['idestado'] = isset($data['idestado']) ? $data['idestado'] : '';
            $dataUnidadeDados['idcidade'] = isset($data['idcidade']) ? $data['idcidade'] : '';
            $dataUnidadeDados['telefone'] = isset($data['telefone']) ? $data['telefone'] : '';
            $dataUnidadeDados['celular'] = isset($data['celular']) ? $data['celular'] : '';
            $dataUnidadeDados['email'] = isset($data['email']) ? $data['email'] : '';
            $dataUnidadeDados['site'] = isset($data['site']) ? $data['site'] : '';

            $filial = Unidade::create([
                        'user_id' => $dataUnidade['user_id'],
                        'parent_id' => $data['parent_id'],
                        'idmatriz' => 0,
                        'logo' => 'gym-padrao.png',
                        'fantasia' => $data['fantasia'],
                        'razao_social' => $data['razao_social'],
                        'situacao' => '0'
            ]);
            $dataUnidadeDados['idunidade'] = $filial->id;

            $filialDados = new UnidadeDados();
            if ($filialDados->create($dataUnidadeDados)):
                $retorno['title'] = 'Sucesso!';
                $retorno['type'] = 'success';
                $retorno['text'] = 'Filial cadastrada com sucesso!';
                //return $retorno;
            else:
                $retorno['title'] = 'Erro!';
                $retorno['type'] = 'error';
                $retorno['text'] = 'Erro ao cadastrar filial!';
            //    return $retorno;
            endif;
            
            $upUser = new User();
            if ($upUser->where('id', '=', $adm->id)->update(['idunidade' => $filial->id])):
                $retorno['title'] = 'Sucesso!';
                $retorno['type'] = 'success';
                $retorno['text'] = 'Matriz atualizada com sucesso!';
            else:
                $retorno['title'] = 'Erro!';
                $retorno['type'] = 'error';
                $retorno['text'] = 'Erro ao atualizar matriz!';
            endif;
            
        }
        
        return $retorno;


    }
    
    
    


}
