<?php

//header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");

namespace App\Http\Controllers;

use DateTime;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserDados;
use App\Models\Configura;
use App\Models\Temaacademia;
use SoapClient;
use App\Models\Unidade;
use App\Models\Telatemaacademia;
use DB;
use Hash;
use Mail;
use Redirect;
use stdClass;
// Precisa para funcionar o combo
use Auth;
use App\Services\FilialService;

class ApiPersonalizaController extends Controller {

    public function __construct(FilialService $filialService) {
        $this->filialService = $filialService;
    }

    public function changeEmp($id_unidade) {
        $this->filialService->changeEmp($id_unidade);

        return redirect(url()->previous());
    }
    
function hexToRgb($hex, $alpha = false) {
   $hex      = str_replace('#', '', $hex);
   $length   = strlen($hex);
   $rgb['r'] = hexdec($length == 6 ? substr($hex, 0, 2) : ($length == 3 ? str_repeat(substr($hex, 0, 1), 2) : 0));
   $rgb['g'] = hexdec($length == 6 ? substr($hex, 2, 2) : ($length == 3 ? str_repeat(substr($hex, 1, 1), 2) : 0));
   $rgb['b'] = hexdec($length == 6 ? substr($hex, 4, 2) : ($length == 3 ? str_repeat(substr($hex, 2, 1), 2) : 0));
   if ( $alpha ) {
      $rgb['a'] = $alpha;
   }
   return $rgb;
}    

    public function salvaArquivoCss($css_texto, $nome_arquivo, $pasta) {
        
        
        $pastaName = $_SERVER['DOCUMENT_ROOT'] . '/public/uploads/temas/' . $pasta;
        if (is_dir($pastaName)) {
            
        } else {
            mkdir($pastaName, 0755);
        }

        
        $destination_folder = $pastaName . '/' . $nome_arquivo;

        $ifp = fopen($destination_folder, 'wb');

        fwrite($ifp, $css_texto);
        fclose($ifp);
    }

    public function homeCss($unidade, $telaNome) {

        $home = Telatemaacademia::select('tela_tema_academia.cor_background as bg_overlay', 'tela_tema_academia.cor_topo as header', 'tela_tema_academia.cor_rodape as footer', 'tela_tema_academia.cor_texto_topo as header_texto', 'tela_tema_academia.cor_texto_rodape as footer_texto', 'tela_tema_academia.cor_nome_aluno as foto_texto','tela_tema_academia.transparencia')
                ->where('idunidade', $unidade)
                ->where('nometela', $telaNome)
                ->first();

        $css_texto = "";
        
        
        
            if (sizeOf($home)) {
           
               $home_header=$this->hexToRgb($home->bg_overlay,$home->transparencia);
               $x="rgba({$home_header['r']}, {$home_header['g']}, {$home_header['b']},0.{$home_header['a']})";
                
            $css_texto = "
          
            .navigation-bar {
              background: {$home->header};
            }

            .navigation-bar__title {
              color: {$home->header_texto} !important;
            }

            .toolbar-button,
            .toolbar-button--outline,
            .toolbar-button--quiet
            {
                  color: {$home->header_texto} !important;

            }
            
            /* bt voltar*/
            ons-toolbar div.left i {
                color: {$home->header_texto}  !important;
            }

            .barra_col {
              background-color: {$home->footer}; /*rodape*/
              color: #906de1;
            }

            .profile-name {
              color: {$home->foto_texto};
            }

            .conteudo-overlay {
              background-color: {$x} ;

            }

            .bg-transroxo {
              background: {$x};
            }

           .rodape_versao, .rodape_versao i, .rodape_versao a{
              color: {$home->footer_texto};
           
             text-shadow: 1px 1px 2px rgba(0, 0, 0, 1);
             text-decoration: none;
            }


            .totem_rodape{
            color: {$home->footer_texto};

            }";
        }
        $this->salvaArquivoCss($css_texto, 'home.css', $unidade);

        return ['sucess' => 'Arquivo salvo com sucesso'];
    }
    
    public function conquistasCss($unidade, $telaNome) {
        
        
        $conquistas = Telatemaacademia::select('tela_tema_academia.cor_background as bg', 'tela_tema_academia.cor_topo as header', 'tela_tema_academia.cor_texto_topo as header_texto', 'tela_tema_academia.cor_textoconquistas', 'tela_tema_academia.cor_fundoconquistas')
                ->where('idunidade', $unidade)
                ->where('nometela', $telaNome)
                ->first();
        

        $css_texto = "";
        if (sizeOf($conquistas)) {     
            
        
        $css_texto = ".corllll {
                background-color: {$conquistas->cor_fundoconquistas};
                    border-left: rgb(104, 48, 177);
                }

                .corllll h2 {
                    color: {$conquistas->cor_textoconquistas} !important; 
                }";
        }
        $this->salvaArquivoCss($css_texto, 'conquistas.css', $unidade);

        return ['sucess' => 'Arquivo salvo com sucesso'];        
        
    }     
    public function reservaCss($unidade, $telaNome) {
        
        
        $reserva = Telatemaacademia::select('tela_tema_academia.cor_background as bg', 'tela_tema_academia.cor_topo as header', 'tela_tema_academia.cor_texto_topo as header_texto', 'tela_tema_academia.cor_fundoreserva', 'tela_tema_academia.cor_textoreserva','tela_tema_academia.cor_botao_reserva')
                ->where('idunidade', $unidade)
                ->where('nometela', $telaNome)
                ->first();
        

        $css_texto = "";
        if (sizeOf($reserva)) {   
            
               $linha=$this->hexToRgb($reserva->cor_fundoreserva,45);
               $linhax="rgba({$linha['r']}, {$linha['g']}, {$linha['b']},0.{$linha['a']})";            
            
            
        
        $css_texto = "/* quadro q tem os bonequinhos dentro */
.reserva .linha_reserva {
    color: {$reserva->cor_textoreserva};
    background: {$linhax};
    padding: 15px 0;
}

/* bt reservar e cancelar */
.reserva ons-button.login-button {
    background: {$reserva->cor_botaoreserva};
    color: #fff;
}

/*  lista participantes cor texto */
.reserva .empresas h2 {
 color: #fff;
}

/* Reservar */
.reserva .outraspos {
    background: rgba(47, 31, 83, 0.68);
}

/* ciruclo laranja */
.reserva .empresas .mybt {
    background: #ff741f;
    color: #2f1f53;
}";

        }
        $this->salvaArquivoCss($css_texto, 'reserva.css', $unidade);

        return ['sucess' => 'Arquivo salvo com sucesso'];
        
    }    

    public function menuCss($unidade, $telaNome) {

        $menu = Telatemaacademia::select('tela_tema_academia.cor_fundomenu', 'tela_tema_academia.cor_textomenu', 'tela_tema_academia.cor_iconemenu')
                ->where('idunidade', $unidade)
                ->where('nometela', $telaNome)
                ->first();


        $cor = '#3f305f';



        $css_texto = ".toolbar-button,
        .toolbar-button--outline,
        .toolbar-button--quiet {
          color: #fff;
        }
        
        /*cor do fundo menu laterl */
        .menu-lateral {
          background: {$menu->cor_fundomenu};

        }

        .menu-list,
        .menu-item:first-child,
        .menu-item
        {
            color: #fff;
        }

        /* cor do icone  */
        .menu-list span, i {
            color: {$menu->cor_iconemenu};
        }
        .menu-list span.cortexto {
          color: {$menu->cor_textomenu};
        }

        ons-list-item.menu-item.list__item.ons-list-item-inner {
          border-bottom: 1px solid #ccc;
        }

        .frist_line {
            border-top: 1px solid #ccc !important;
        }";


        $this->salvaArquivoCss($css_texto, 'menu.css', $unidade);

        return ['sucess' => 'Arquivo salvo com sucesso'];
    }
    

    public function gfmCss($unidade, $telaNome) {
        $gfm = Telatemaacademia::select('tela_tema_academia.cor_background as bg', 'tela_tema_academia.cor_topo as header', 'tela_tema_academia.cor_texto_topo as header_texto', 'tela_tema_academia.cor_gfm_corsim as linha', 'tela_tema_academia.cor_textogfm as texto', 'tela_tema_academia.cor_fundodiagfm as nav', 'tela_tema_academia.cor_gfm_cornao as linhat')
                ->where('idunidade', $unidade)
                ->where('nometela', $telaNome)
                ->first();


        $css_texto = "";
        if (sizeOf($gfm)) {
            ///aqui vai o css
            
               $linha=$this->hexToRgb($gfm->linha,45);
               
               $linhax="rgba({$linha['r']}, {$linha['g']}, {$linha['b']},0.{$linha['a']})";            

               $linhat=$this->hexToRgb($gfm->linhat,$gfm->transparencia);
               $linhatx="rgba({$linhat['r']}, {$linhat['g']}, {$linhat['b']},0.{$linha['a']})";             
            
     
               
               
                $css_texto = "
              .nav_gfm li a {
                    background: {$gfm->nav} !important;
                    border: 1px solid #302154;
                }

                .nav_gfm > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus {
                    background-color: #000 !important;
                    color: #fff !important;
                  
                }

                .linhatbg_gfm {
                    color: {$gfm->texto};
                    background: {$linhax};
                }
                
                .treino-list .linhat_gfm {
                    color: {$gfm->texto};
                    background: {$linhatx};
                }

                /* cor do check */
                .check-Nok {
                  color: #e4d7ff;
                }
                .check-ok {
                  color: #c8703f !important;
                }

            ";
        }

        $this->salvaArquivoCss($css_texto, 'ginastica.css', $unidade);
        return ['sucess' => 'Arquivo salvo com sucesso'];
    }

    public function natacaoCss($unidade, $telaNome) {
        $natacao = Telatemaacademia::select('tela_tema_academia.cor_background as bg', 'tela_tema_academia.cor_topo as header', 'tela_tema_academia.cor_texto_topo as header_texto', 'tela_tema_academia.cor_spm_corsim as linha', 'tela_tema_academia.cor_textospm as texto', 'tela_tema_academia.cor_fundodiaspm as nav', 'tela_tema_academia.cor_spm_cornao as linhat')
                ->where('idunidade', $unidade)
                ->where('nometela', $telaNome)
                ->first();



        $css_texto = "";
        if (sizeOf($natacao)) {
            ///aqui vai o css
            
               $linha=$this->hexToRgb($natacao->linha,45);
               $linhax="rgba({$linha['r']}, {$linha['g']}, {$linha['b']},0.{$linha['a']})";            

               $linhat=$this->hexToRgb($natacao->linhat,45);
               $linhatx="rgba({$linhat['r']}, {$linhat['g']}, {$linhat['b']},0.{$linha['a']})";                   
          
               $css_texto = "
                
                .nav-tabs li a {
                    background: {$natacao->nav};
                    border: 1px solid #302154;
                }
                
                .nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus {
                    background-color: #000 !important;
                    color: #fff !important;
                  
                }

                .linhatbg {
                    color: {$natacao->texto};
                    background: {$linhax};
                }
                
                .treino-list .linhat {
                    color: {$natacao->texto};
                    background: {$linhatx};
                }
               
            ";
        }
        
        
        
        

        $this->salvaArquivoCss($css_texto, 'natacao.css', $unidade);
        return ['sucess' => 'Arquivo salvo com sucesso'];
    }



     public function selecionaTreinoCss($unidade, $telaNome) {
        $seleciona = Telatemaacademia::select('tela_tema_academia.cor_background as bg', 'tela_tema_academia.cor_topo as header',
                                        'tela_tema_academia.cor_texto_topo as header_texto', 'tela_tema_academia.cor_iconeseleciona','tela_tema_academia.cor_fundoiconeseleciona','tela_tema_academia.cor_selecionaimpar','tela_tema_academia.cor_selecionapar')
                ->where('idunidade', $unidade)
                ->where('nometela', $telaNome)
                ->first();


               $linhaimpar=$this->hexToRgb($seleciona->cor_selecionaimpar,52);
               $linhaimparx="rgba({$linhaimpar['r']}, {$linhaimpar['g']}, {$linhaimpar['b']},0.{$linhaimpar['a']})";         
        
               $linhapar=$this->hexToRgb($seleciona->cor_selecionapar,52);
               $linhaparx="rgba({$linhapar['r']}, {$linhapar['g']}, {$linhapar['b']},0.{$linhapar['a']})";         
        
        
        $css_texto = "";
        if (sizeOf($seleciona)) {
            
            
            
            
        $css_texto=".overlay_modadidades {
  background-color: {$linhaimparx};

}

.overlay_modadidades_par {
  background-color: {$linhaparx};
}

.seleciona_treino .listatvNewsa {
  background-color: {$seleciona->cor_fundoiconeseleciona};
}

.seleciona_treino .listatvNewsa i {
  color: {$seleciona->cor_iconeseleciona} !important;
}

.seleciona_treino .trophy_inside {
  margin-top: -5px;
}

.seleciona_treino .trophy_inside span {
  font-size: 72px;
  color: {$seleciona->cor_iconeseleciona};
}

.seleciona_treino .cor_linha {
  background-color: {$seleciona->cor_selecionaimpar};
  padding-top: 80px;
  height: 100.2%;
}

.seleciona_treino .cor_linha_par {
  background-color: {$seleciona->cor_selecionapar};
}

.seleciona_treino .icone_Cancel {
  color: #695288;
  font-size: 24px;  
}


";
            
            

            /*$css_texto = ".listatvNewsa {
  background-color: {$seleciona->cor_fundoiconeseleciona};
}

.listatvNewsa i {
  color: {$seleciona->cor_iconeseleciona} !important; 
  font-size: 24px;
}
            ";*/
        }
        
        
        

        $this->salvaArquivoCss($css_texto, 'seleciona_treino.css', $unidade);
        return ['sucess' => 'Arquivo salvo com sucesso'];
    }
    
    
     public function planejamentoCss($unidade, $telaNome) {
        $planejamento = Telatemaacademia::select('tela_tema_academia.cor_background as bg', 'tela_tema_academia.cor_topo as header',
                                        'tela_tema_academia.cor_texto_topo as header_texto', 'tela_tema_academia.cor_textoplan',
                                        'tela_tema_academia.cor_fundoplan',
                                        'tela_tema_academia.cor_fundoplan',
                                        'tela_tema_academia.cor_diaplan',
                                        'tela_tema_academia.cor_horaplan',
                                        'tela_tema_academia.cor_atvplan')
                ->where('idunidade', $unidade)
                ->where('nometela', $telaNome)
                ->first();


        $css_texto = "";
        if (sizeOf($planejamento)) {
            ///aqui vai o css
          /*  $css_texto = "
                          .fundo_plan {
                                background-color: {$planejamento->cor_fundoplan}; 
                          }
                          .texto_plan {
                               color: {$planejamento->cor_textoplan}; 
                          }
                          .dia_semana {
                                background-color: {$planejamento->cor_diaplan};  
                          }
                          .hora_semana{
                                background-color: {$planejamento->cor_horaplan}; 
                            
                          }
                          .atv_semana{
                                background-color: {$planejamento->cor_atvplan};  
                          }
            ";*/
            $css_texto = "            
            .diahoje {
  background: #ff9537 !important;
  color: #fff !important;
}

.dia_linha_psa {
  border-bottom: 1px solid #ccc;
  box-shadow: 2px 2px 4px #888888;
  background: {$planejamento->cor_atvplan};
}

.dia_semana {
  background: {$planejamento->cor_diaplan};
  color: {$planejamento->cor_textoplan};
}

.dia_linha_psa ons-col.hora_semana {
  background: {$planejamento->cor_horaplan};
  text-align: center;
  color: #333;
  font-size: 14px;
}";


        }

        $this->salvaArquivoCss($css_texto, 'planejamento.css', $unidade);
        return ['sucess' => 'Arquivo salvo com sucesso'];
    }    

    public function mensagemCss($unidade, $telaNome) {
        $mensagem = Telatemaacademia::select('tela_tema_academia.cor_background as bg', 'tela_tema_academia.cor_topo as header', 'tela_tema_academia.cor_texto_topo as header_texto','tela_tema_academia.transparencia', 'tela_tema_academia.cor_mensagem_corsim as linha', 'tela_tema_academia.cor_texto_telamensagem as texto', 'tela_tema_academia.cor_fundodiagfm as nav', 'tela_tema_academia.cor_mensagem_cornao as linhat')
                ->where('idunidade', $unidade)
                ->where('nometela', $telaNome)
                ->first();

        $css_texto = "";
        if (sizeOf($mensagem)) {
            ///aqui vai o css
            
           
               $linha=$this->hexToRgb($mensagem->linha,45);
               $linhax="rgba({$linha['r']}, {$linha['g']}, {$linha['b']},0.{$linha['a']})";            

               $linhat=$this->hexToRgb($mensagem->linhat,45);
               $linhatx="rgba({$linhat['r']}, {$linhat['g']}, {$linhat['b']},0.{$linha['a']})";            
            
            $css_texto = "
            
            .objetivo_mensagem {

              background: {$linhax};
              border-left: 4px solid #8364cd;
              color: #fff;
            }

            .bgmaisEsMensg_impar {
                background: {$linhatx};
            }

            .objetivo_mensagem h2 {
 
               color: #fff;
              
             }

            ";
        }
 
        

        $this->salvaArquivoCss($css_texto, 'mensagem.css', $unidade);
        return ['sucess' => 'Arquivo salvo com sucesso'];
    }

    public function clubeCss($unidade, $telaNome) {
        $clube = Telatemaacademia::select('tela_tema_academia.cor_background as bg', 'tela_tema_academia.cor_topo as header', 'tela_tema_academia.cor_texto_topo as header_texto', 'tela_tema_academia.cor_impar_clube as linha', 'tela_tema_academia.cor_texto_clube as texto', 'tela_tema_academia.cor_par_clube as linhat', 'tela_tema_academia.cor_cab_clube')
                ->where('idunidade', $unidade)
                ->where('nometela', $telaNome)
                ->first();
        $css_texto = "";
        if (sizeOf($clube)) {
            
               $linha=$this->hexToRgb($clube->linha,45);
               $linhax="rgba({$linha['r']}, {$linha['g']}, {$linha['b']},0.{$linha['a']})";            

               $linhat=$this->hexToRgb($clube->linhat,45);
               $linhatx="rgba({$linhat['r']}, {$linhat['g']}, {$linhat['b']},0.{$linha['a']})";                   
            
            
            
            ///aqui vai o css
            $css_texto = ".clube_vantagens {
  background: {$clube->cor_cab_clube};
  color: {$clube->texto};
}

/*  textos */
.clube_vantagens h2 {
      color: {$clube->texto};
}
.labelname_clube {
    color: {$clube->texto};
}
.clube_vantagens  .nome_mes {
    color: {$clube->texto};
}
/* linhas */
.empresas ons-row.cornoSim {
      background: {$linhax};
       color: {$clube->texto};
 }
.empresas ons-row.cornoNao {
  background:  {$linhatx};
  color: {$clube->texto};
}

/* porcentagem de desconto na linka */
.empresas .mybt {
    background: {$clube->cor_cab_clube};
    color: {$clube->texto};
}

.empresas .seta_laraja {
     color: #ff741f;
}

.clube_vantagens .dinheiro_clube {
  color: {$clube->texto};
}

.clube_vantagens .icon_dinheiro {
 color: {$clube->texto};
}

            ";
        }
        

        $this->salvaArquivoCss($css_texto, 'clube_vantagens.css', $unidade);
        return ['sucess' => 'Arquivo salvo com sucesso'];        
        
        
        
    }    
    
    public function crossCss($unidade, $telaNome) {
        $cross = Telatemaacademia::select('tela_tema_academia.cor_background as bg', 'tela_tema_academia.cor_topo as header', 'tela_tema_academia.cor_texto_topo as header_texto', 'tela_tema_academia.cor_cross_corsim as linha', 'tela_tema_academia.cor_textocross as texto', 'tela_tema_academia.cor_cross_cornao as linhat', 'tela_tema_academia.cor_botao_partiu', 'tela_tema_academia.cor_botao_iniciar')
                ->where('idunidade', $unidade)
                ->where('nometela', $telaNome)
                ->first();
        $css_texto = "";
        if (sizeOf($cross)) {
            ///aqui vai o css
            
               $linha=$this->hexToRgb($cross->linha,45);
               $linhax="rgba({$linha['r']}, {$linha['g']}, {$linha['b']},0.{$linha['a']})";            

               $linhat=$this->hexToRgb($cross->linhat,45);
               $linhatx="rgba({$linhat['r']}, {$linhat['g']}, {$linhat['b']},0.{$linha['a']})";              
            
               $linhatdesc=$this->hexToRgb($cross->texto,45);
               $linhatxdesc="rgba({$linhatdesc['r']}, {$linhatdesc['g']}, {$linhatdesc['b']},0.{$linhatdesc['a']})";              
            
            $css_texto = "
         
            .crossfit .descricao {
                background: {$linhatxdesc};
            }

            .historicoList .linha_cross_par {
                background-color: $linhax;
                border-bottom: 1px solid #624a94;
            }
            .historicoList .linha_cross_imp {
                background-color: $linhatx;
                border-bottom: 1px solid #624a94;
            }
            ";
        }
        

        $this->salvaArquivoCss($css_texto, 'crossfit.css', $unidade);
        return ['sucess' => 'Arquivo salvo com sucesso'];
    }

    public function ciclismoCss($unidade, $telaNome) {
        
        
        $ciclismo = Telatemaacademia::select('tela_tema_academia.cor_background as bg','tela_tema_academia.cor_fundodescciclismo', 'tela_tema_academia.cor_topo as header', 'tela_tema_academia.cor_texto_topo as header_texto', 'tela_tema_academia.cor_ciclismo_corsim as linha', 'tela_tema_academia.cor_textociclismo as texto', 'tela_tema_academia.cor_ciclismo_cornao as linhat', 'tela_tema_academia.cor_botao_partiuciclismo', 'tela_tema_academia.cor_botao_iniciarciclismo')
                ->where('idunidade', $unidade)
                ->where('nometela', $telaNome)
                ->first();  
        
        
        
        
               $fundodesc=$this->hexToRgb($ciclismo->cor_fundodescciclismo,70);
               $fundodescx="rgba({$fundodesc['r']}, {$fundodesc['g']}, {$fundodesc['b']},0.{$fundodesc['a']})";            

               $linhaimpar=$this->hexToRgb($ciclismo->linha,70);
               $linhaimparx="rgba({$linhaimpar['r']}, {$linhaimpar['g']}, {$linhaimpar['b']},0.{$linhaimpar['a']})";            

               $linhapar=$this->hexToRgb($ciclismo->linhat,70);
               $linhaparx="rgba({$linhapar['r']}, {$linhapar['g']}, {$linhapar['b']},0.{$linhapar['a']})";            
        
        $css_texto = "";

 
        
        if (sizeOf($ciclismo)) {

            $css_texto = ".ciclismo_bg .descricao {
    background-color: {$fundodescx};
     color: {$ciclismo->texto};
}
.ciclismo_bg .historicoList .linha_ciclismo_par {
  background: {$linhaimparx};
  border-bottom: 1px solid #624a94;
}

.ciclismo_bg .historicoList .linha_ciclismo_imp {
  background: {$linhaparx};
  border-bottom: 1px solid #624a94;
}

.ciclismo_bg .um_larnja {
  background: #ff7e31;
  color: #3f305f;
}

/* cor do texto na frente do circulo */
.ciclismo_bg.txt_larnja_nofloat {
  color: #ff7e31;
}

.ciclismo_bg .check-ok {
  color: #c8703f !important;
}
.ciclismo_bg .ico_check i {
  color: #c8703f ;
}

";
 
            ///aqui vai o css
        }

        $this->salvaArquivoCss($css_texto, 'ciclismo.css', $unidade);
        return ['sucess' => 'Arquivo salvo com sucesso'];
    }
    public function corridaCss($unidade, $telaNome) {
        
        
        $corrida = Telatemaacademia::select('tela_tema_academia.cor_background as bg','tela_tema_academia.cor_fundodesccorrida', 'tela_tema_academia.cor_topo as header', 'tela_tema_academia.cor_texto_topo as header_texto', 'tela_tema_academia.cor_corrida_corsim as linha', 'tela_tema_academia.cor_textocorrida as texto', 'tela_tema_academia.cor_corrida_cornao as linhat', 'tela_tema_academia.cor_botao_partiucorrida', 'tela_tema_academia.cor_botao_iniciarcorrida')
                ->where('idunidade', $unidade)
                ->where('nometela', $telaNome)
                ->first();  
        
        
        
        
               $fundodesc=$this->hexToRgb($corrida->cor_fundodesccorrida,70);
               $fundodescx="rgba({$fundodesc['r']}, {$fundodesc['g']}, {$fundodesc['b']},0.{$fundodesc['a']})";            

               $linhaimpar=$this->hexToRgb($corrida->linha,70);
               $linhaimparx="rgba({$linhaimpar['r']}, {$linhaimpar['g']}, {$linhaimpar['b']},0.{$linhaimpar['a']})";            

               $linhapar=$this->hexToRgb($corrida->linhat,70);
               $linhaparx="rgba({$linhapar['r']}, {$linhapar['g']}, {$linhapar['b']},0.{$linhapar['a']})";            
        
              $css_texto = "";

 
        
        if (sizeOf($corrida)) {

            $css_texto = ".corrida_bg .descricao {
    background-color: {$fundodescx};
     color: {$corrida->texto};
}
.corrida_bg .historicoList .linha_corrida_par {
  background: {$linhaimparx};
  border-bottom: 1px solid #624a94;
}

.corrida_bg .historicoList .linha_corrida_imp {
  background: {$linhaparx};
  border-bottom: 1px solid #624a94;
}

.corrida_bg .um_larnja {
  background: #ff7e31;
  color: #3f305f;
}

/* cor do texto na frente do circulo */
.corrida_bg. txt_larnja_nofloat {
  color: #ff7e31;
}

.corridabg .check-ok {
  color: #c8703f !important;
}
.ciclismo_bg .ico_check i {
  color: #c8703f ;
}

";
 
            ///aqui vai o css
        }

        $this->salvaArquivoCss($css_texto, 'corrida.css', $unidade);
        return ['sucess' => 'Arquivo salvo com sucesso'];
    }

    public function musculacaoCss($unidade, $telaNome) {
        $musculacao = Telatemaacademia::select('tela_tema_academia.cor_background as bg', 'tela_tema_academia.cor_topo as header', 'tela_tema_academia.cor_texto_musc', 'tela_tema_academia.cor_ativo_musc', 'tela_tema_academia.cor_inativo_musc', 'tela_tema_academia.cor_partiu_musc', 'tela_tema_academia.cor_iniciar_musc')
                ->where('idunidade', $unidade)
                ->where('nometela', $telaNome)
                ->first();


        $css_texto = "";
        if (sizeOf($musculacao)) {
           

            
               $linha=$this->hexToRgb($musculacao->bg,45);
               $linhax="rgba({$linha['r']}, {$linha['g']}, {$linha['b']},0.{$linha['a']})";            

               $linhat=$this->hexToRgb($musculacao->bg,45);
               $linhatx="rgba({$linhat['r']}, {$linhat['g']}, {$linhat['b']},0.{$linha['a']})";               
               
               
            ///aqui vai o css
            $css_texto = "
               /* essa é a linha preta q tb os btn dentro */
                .musculacao .linha_treino {
                  background: #000;
                }

                .musculacao .linhagbg {
                  background : {$linhax};

                }
                
                .musculacao .linha_par {
                  background: rgb(10, 218, 255, 0.62);
                }
                
                .circulo-laranja {
                    background: {$musculacao->cor_ativo_musc};
                     color: #302154;    
                }
                .circulo-roxo {
                  background: {$musculacao->cor_inativo_musc} !important;;
                }

                .musculacao .linhagbg1 {
                  background: {$linhax};
                }
            
                .musculacao .linha_impar
                {
                  background: {$linhatx};
                }



            ";
        }

        $this->salvaArquivoCss($css_texto, 'musculacao.css', $unidade);
        return ['sucess' => 'Arquivo salvo com sucesso'];
    }


/*

 */

    public function selecionarTreinoCss($unidade, $telaNome) {
        
        $treino = Telatemaacademia::select('tela_tema_academia.cor_background as bg', 'tela_tema_academia.cor_topo as header', 'tela_tema_academia.cor_texto_treino', 'tela_tema_academia.cor_treino_cornao', 'tela_tema_academia.cor_treino_corsim', 'tela_tema_academia.cor_treino_corselec')
                ->where('idunidade', $unidade)
                ->where('nometela', $telaNome)
                ->first();
        $css_texto = "";
        if (sizeOf($treino)) {
            ///aqui vai o css
            $css_texto = ".listatvNewsa {
  background-color: #321954;  /* cor do fundo do icone */
}

.listatvNewsa i {
  color: #695288 !important;  /* cor do icone*/
  font-size: 24px;
}
            ";
        }


        $this->salvaArquivoCss($css_texto, 'seleciona_treino.css', $unidade);
        return ['sucess' => 'Arquivo salvo com sucesso'];
        
        
    }
    
    public function exercicioCss($unidade, $telaNome) {
        $exercicio = Telatemaacademia::select('tela_tema_academia.cor_background as bg', 'tela_tema_academia.cor_topo as header', 
                'tela_tema_academia.cor_textoexer', 'tela_tema_academia.cor_fundoexer', 
                'tela_tema_academia.cor_imparexer', 'tela_tema_academia.cor_parexer')
                ->where('idunidade', $unidade)
                ->where('nometela', $telaNome)
                ->first();
        $css_texto = "";
        if (sizeOf($exercicio)) {
            ///aqui vai o css
            $css_texto = "

                .treino-exerc .linhat {
                    background: {$exercicio->cor_imparexer};

                }
 
                .treino-exerc .linhatbg {
                    background: {$exercicio->cor_parexer};
                }

                .treino-list .linhatbg {
                    background: {$exercicio->cor_fundoexer};
                }
            ";
        }


        $this->salvaArquivoCss($css_texto, 'exercicio.css', $unidade);
        return ['sucess' => 'Arquivo salvo com sucesso'];
    }




    // Treino

    public function treinoCss($unidade, $telaNome) {
        $treino = Telatemaacademia::select('tela_tema_academia.cor_background as bg', 'tela_tema_academia.cor_topo as header', 'tela_tema_academia.cor_texto_treino', 'tela_tema_academia.cor_treino_cornao', 'tela_tema_academia.cor_treino_corsim', 'tela_tema_academia.cor_treino_corselec')
                ->where('idunidade', $unidade)
                ->where('nometela', $telaNome)
                ->first();
        $css_texto = "";
        if (sizeOf($treino)) {
            ///aqui vai o css
            
               $linha=$this->hexToRgb($treino->cor_treino_corsim,45);
               $linhax="rgba({$linha['r']}, {$linha['g']}, {$linha['b']},0.{$linha['a']})";            

               $linhat=$this->hexToRgb($treino->cor_treino_cornao,45);
               $linhatx="rgba({$linhat['r']}, {$linhat['g']}, {$linhat['b']},0.{$linha['a']})";            

               $linhasel=$this->hexToRgb($treino->cor_treino_corselec,45);
               $linhaselx="rgba({$linhasel['r']}, {$linhasel['g']}, {$linhasel['b']},0.{$linhasel['a']})";            
            
            
            $css_texto = "
            /* linha par */
            .treino-list .linhat_treino {
              color: {$treino->cor_texto_treino};
              background: {$linhax};
              padding: 20px 0;
            }

            /* linha impar */
            .treino-list .linhatbg_treino {
              color: {$treino->cor_texto_treino};
              background: {$linhatx};
              padding: 20px 0 10px 0;
            }

            /* linha ativa */
            .treino-list  .activeList {
              background: {$linhaselx} !important;
            }

            /* cor do icone da seta */
            .treino-list .seta_laraja {
                  color: {$treino->cor_texto_treino};
            }

            ";
                }


        $this->salvaArquivoCss($css_texto, 'treino.css', $unidade);
        return ['sucess' => 'Arquivo salvo com sucesso'];
    }

    public function chatCss($unidade, $telaNome) {
        $chat = Telatemaacademia::select('tela_tema_academia.cor_background as bg', 'tela_tema_academia.cor_topo as header', 'tela_tema_academia.cor_texto_chat', 'tela_tema_academia.cor_fundo_chat', 'tela_tema_academia.cor_fundo_enviada', 'tela_tema_academia.cor_fundo_recebida')
                ->where('idunidade', $unidade)
                ->where('nometela', $telaNome)
                ->first();

        $css_texto = "";
        if (sizeOf($chat)) {
            ///aqui vai o css
            $css_texto = "/* balao remetente */
                .conversas .personal_mensagem 
                {
                        background: {$chat->cor_fundo_enviada};
                }

                /* balao meu balao */
                    .conversas .cliente_mensagem {
                    background: {$chat->cor_fundo_recebida};
                }

                .envia_mensagem{
                    background: {$chat->cor_fundo_chat};
                    color: #fff;
                }

                /* fundo bt cartinha */
                    .envia_mensagem .button{
                    background-color: #000;
                }

                .envia_mensagem .mensagem-aviao {
                    background: #ff9034;
                }

                .envia_mensagem .textarea-chat {
                    border: solid 1px  {$chat->cor_fundo_chat};
                }
                .envia_mensagem .button i{
                    color: #fff;
                }                

                ";
        }

        $this->salvaArquivoCss($css_texto, 'chat.css', $unidade);
        return ['sucess' => 'Arquivo salvo com sucesso'];
    }


            
    public function contatoCss($unidade, $telaNome) {
        $contato = Telatemaacademia::select('tela_tema_academia.cor_background as bg', 'tela_tema_academia.cor_topo as header', 'tela_tema_academia.cor_texto_contato', 'tela_tema_academia.cor_fundo_contato')
                ->where('idunidade', $unidade)
                ->where('nometela', $telaNome)
                ->first();

        $css_texto = "";
        if (sizeOf($contato) > 0) {
            $css_texto = "
            .contato-form .button {
                background-color: #0F0;
                color: #fff;
            }";
        }

        $this->salvaArquivoCss($css_texto, 'contato.css', $unidade);
        return ['sucess' => 'Arquivo salvo com sucesso'];
    }
    
    public function coachCss($unidade, $telaNome) {
        $coach = Telatemaacademia::select('tela_tema_academia.cor_background as bg', 'tela_tema_academia.cor_topo as header', 'tela_tema_academia.cor_texto_coach', 'tela_tema_academia.cor_fundo_coach', 'tela_tema_academia.transparencia_coach')
                ->where('idunidade', $unidade)
                ->where('nometela', $telaNome)
                ->first();

        $css_texto = "";
        
               $coach_header=$this->hexToRgb($coach->cor_fundo_coach,$coach->transparencia_coach);
               $x="rgba({$coach_header['r']}, {$coach_header['g']}, {$coach_header['b']},0.{$coach_header['a']})";        
        
        if (sizeOf($coach) > 0) {
            $css_texto = ".coach .listatvNewsa {
                           background-color: #321954;
                         }

                        .coach .tableModalidade tr td {
                            color : $coach->cor_texto_coach ;
                            background-color:  {$x};
                        }";
        }

        $this->salvaArquivoCss($css_texto, 'coach.css', $unidade);
        return ['sucess' => 'Arquivo salvo com sucesso'];
    }    

    public function empresaCss($unidade, $telaNome) {
        $empresa = Telatemaacademia::select('tela_tema_academia.cor_background as bg', 'tela_tema_academia.cor_topo as header', 'tela_tema_academia.cor_textoempresa', 'tela_tema_academia.cor_fundoempresa', 'tela_tema_academia.cor_cabecempresa')
                ->where('idunidade', $unidade)
                ->where('nometela', $telaNome)
                ->first();

        
               $fundoempresa=$this->hexToRgb($empresa->cor_fundoempresa,45);
               $fundoempresax="rgba({$fundoempresa['r']}, {$fundoempresa['g']}, {$fundoempresa['b']},0.{$fundoempresa['a']})";         

               $cabecempresa=$this->hexToRgb($empresa->cor_cabecempresa,45);
               $cabecempresax="rgba({$cabecempresa['r']}, {$cabecempresa['g']}, {$cabecempresa['b']},0.{$cabecempresa['a']})";         
        
        $css_texto = "";
        if (sizeOf($empresa)) {

            ///aqui vai o css
            $css_texto = ".ver_empresa .empresa {
  background: {$empresa->cor_cabecempresa};
}

.ver_empresa .empresa .labelname_clube {
  color: {$empresa->cor_textoempresa};
}

.ver_empresa .equipe {
  color: {$empresa->cor_textoempresa};
}

.ver_empresa .contatenos {
  color: {$empresa->cor_textoempresa};
}

.ver_empresa .link_branco {
  color: {$empresa->cor_textoempresa};
}

.ver_empresa i {
  color: #fff;
}

.ver_empresa .historicoList ons-row {
  background: {$fundoempresax};
  border-bottom: 1px solid #624a94;
}

";
        }


        $this->salvaArquivoCss($css_texto, 'empresa.css', $unidade);
        return ['sucess' => 'Arquivo salvo com sucesso'];
    }

    public function getTemaAcademia($unidade) {
        
        $empresa = Temaacademia::select('tema_academia.*')
                ->where('idunidade', $unidade)
                ->first();        

        $logo = Telatemaacademia::select('tela_tema_academia.*')
                ->where('idunidade', $unidade)
                ->where('nometela', 'Home')
                ->first();        

        $total =0;
        
        if (sizeOf($empresa) > 0) {
            if (sizeOf($logo) > 0):
                $empresa->logomarca_home = $logo->logomarca_home;
            endif;
            if ($empresa->idtema == 1):
                $total = 0;
            else:
                $total = 1;
            endif;
        }

        return ['total'=>$total, 'dados' => $empresa];
        
    }    
    
    public function geraCss($unidade) {
    
        $x=$this->getCoresApp($unidade,"Home");
        $x=$this->getCoresApp($unidade,"Gfm");
        $x=$this->getCoresApp($unidade,"Natacao");
        $x=$this->getCoresApp($unidade,"Mensagem");
        $x=$this->getCoresApp($unidade,"Cross");
        $x=$this->getCoresApp($unidade,"Ciclismo");
        $x=$this->getCoresApp($unidade,"Corrida");
        $x=$this->getCoresApp($unidade,"Menu");
        $x=$this->getCoresApp($unidade,"Musculacao");
        $x=$this->getCoresApp($unidade,"Treino");
        $x=$this->getCoresApp($unidade,"Chat");
        $x=$this->getCoresApp($unidade,"Contato");
        $x=$this->getCoresApp($unidade,"Empresa");
        $x=$this->getCoresApp($unidade,"Selecionar_Treino");
        $x=$this->getCoresApp($unidade,"Exercicio");
        $x=$this->getCoresApp($unidade,"Clube");
        $x=$this->getCoresApp($unidade,"Reserva");
        $x=$this->getCoresApp($unidade,"Planejamento");
        $x=$this->getCoresApp($unidade,"Conquistas");
        $x=$this->getCoresApp($unidade,"Coach");
        
        
        return 'ok';
    }    
    
    public function getCoresApp($unidade, $telaNome) {
        
        
        if ($telaNome == "Home"):
            return $this->homeCss($unidade, $telaNome);
        endif;
        if ($telaNome == "Gfm"):
            return $this->gfmCss($unidade, $telaNome);

        endif;
        if ($telaNome == "Natacao"):
            return $this->natacaoCss($unidade, $telaNome);
        endif;
        if ($telaNome == "Mensagem"):
            return $this->mensagemCss($unidade, $telaNome);
        endif;
        if ($telaNome == "Cross"):
            return $this->crossCss($unidade, $telaNome);
        endif;
        if ($telaNome == "Ciclismo"):
            return $this->ciclismoCss($unidade, $telaNome);
        endif;
        if ($telaNome == "Corrida"):
            return $this->corridaCss($unidade, $telaNome);
        endif;
        if ($telaNome == "Menu"):
            return $this->menuCss($unidade, $telaNome);
        endif;
        if ($telaNome == "Musculacao"):
            return $this->musculacaoCss($unidade, $telaNome);
        endif;
        if ($telaNome == "Treino"):
            return $this->treinoCss($unidade, $telaNome);
        endif;
        if ($telaNome == "Chat"):
            return $this->chatCss($unidade, $telaNome);
        endif;
        if ($telaNome == "Contato"):
            return $this->contatoCss($unidade, $telaNome);
        endif;
        if ($telaNome == "Empresa"):
            return $this->empresaCss($unidade, $telaNome);
        endif;
        if ($telaNome == "Selecionar_Treino"):
            return $this->selecionaTreinoCss($unidade, $telaNome);
        endif;
        if ($telaNome == "Exercicio"):
            return $this->exercicioCss($unidade, $telaNome);
        endif;
        if ($telaNome == "Clube"):
            return $this->clubeCss($unidade, $telaNome);
        endif;
        if ($telaNome == "Reserva"):
            return $this->reservaCss($unidade, $telaNome);
        endif;
        if ($telaNome == "Planejamento"):
            return $this->planejamentoCss($unidade, $telaNome);
        endif;
        if ($telaNome == "Conquistas"):
            return $this->conquistasCss($unidade, $telaNome);
        endif;
        if ($telaNome == "Coach"):
            return $this->coachCss($unidade, $telaNome);
        endif;
        
        
     
    }
   
    
    

}
