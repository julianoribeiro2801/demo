<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use Illuminate\Database\Connection;
use DB;
use App\Models\CVAlunoMercado;
use App\Models\CVEmpresa;
use App\Models\User;
use App\Http\Requests\ClubeVantagensRequest;
use App\Services\TrataDadosService;

// Precisa para funcionar o combo
use Auth;
use App\Services\FilialService;

class ClubeVantagensController extends Controller
{
    public function __construct(FilialService $filialService)
    {
        $this->filialService = $filialService;
    }

    public function index()
    {
        $headers = ['category' => 'Clube Vantagens', 'title' => 'Clube Vantagens '];
        $unidades_combo = $this->filialService->unidadesComboTop();

        $compras = CVAlunoMercado::All();
        // dd($compras->toArray());
        return view('admin.clube.index', compact('compras', 'headers', 'unidades_combo'));
    }

    public function create()
    {
    }
}
