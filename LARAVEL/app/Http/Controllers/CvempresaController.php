<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB;
use Input;

use Image;
use File;

use App\Models\Unidade;
use App\Models\UnidadeDados;
use App\Models\Estado;
use App\Models\CVEmpresa;
use App\Models\Segmento;
use App\Models\UserDados;

// Precisa para funcionar o combo
use Auth;
use App\Services\FilialService;

class CvempresaController extends Controller
{
    public function __construct(FilialService $filialService)
    {
        $this->filialService = $filialService;
    }

    public function changeEmp($id_unidade)
    {
        $this->filialService->changeEmp($id_unidade);

        return redirect(url()->previous());
    }

    public function index()
    {
        $headers = ['category' => 'Empresa', 'title' => 'Unidades'];

        $unidades = Unidade::with(['parent', 'children', 'matriz'])->get();

        return view('empresas.index', compact('unidades', 'headers'));
    }


    public function getEmpresas()
    {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        $empresas=DB::select("select coalesce(sum(cva.vlrcompra),0) as totalvendas , cve.*,DATE_FORMAT(cve.dtvencimento, '%d/%m/%Y') as vencimento, s.nmsegmento, s.dsurlminiatura "
                             . " from cv_empresa as cve "
                             . " left join cv_alunocompra as cva "
                                                         . " on cva.idempresa = cve.id"
                                                         . " and cva.idunidade = cve.idunidade"
                                                         . " left join segmento s on cve.idsegmento = s.id"
                                                         . " where cve.idunidade = " . $idunidade
                                                         . " and cve.excluido not in('S') group by cve.id asc");
                

        return response()->json(compact('empresas'));
    }


    public function getEmpresa($id)
    {
        $cvempresaData = CVEmpresa::select('id', 'nmempresa', 'cnpj', 'percdesconto', 'idsegmento', 'dtvencimento', DB::raw("ifnull(dtvencimento,'false') as indet"))->where('id', $id)->get();
                
        $cvempresa = [
                    
            'id' => $cvempresaData[0]['id'],
            'nmempresa' => $cvempresaData[0]['nmempresa'],
            'cnpj' => $cvempresaData[0]['cnpj'],
            'percdesconto' => $cvempresaData[0]['percdesconto'],
            'idsegmento' => $cvempresaData[0]['idsegmento'],
            'indet' => ($cvempresaData[0]['indet'] != 'false') ? $cvempresaData[0]['indet'] : 'true',
            'dtvencimento' => ($cvempresaData[0]['dtvencimento'] != '0000-00-00') ? $this->getData($cvempresaData[0]['dtvencimento']) : ''
        ];
                
        return response()->json(compact('cvempresa'));
    }
        
    public function getSegmentos()
    {
        $segmentos = Segmento::select('id', 'nmsegmento')->orderby('nmsegmento')->get();
        
        return response()->json(compact('segmentos'));
    }
    
    public function addEmpresa()
    {
        //if(Request::ajax()) {
        $data = Input::all();
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        if (isset($data['value1'])==false) {
            $dataCvempresa['dtvencimento'] =  $this->setData($data['dtvencimento']);
        };
        $dataCvempresa['idunidade'] = $idunidade;
        $dataCvempresa['nmempresa'] = $data['nmempresa'];
        $dataCvempresa['cnpj'] = $data['cnpj'];
        $dataCvempresa['percdesconto'] = $data['percdesconto'];
        $dataCvempresa['idsegmento'] = $data['idsegmento'];
                        
        $cvempresa = new CVEmpresa();
        if ($cvempresa->create($dataCvempresa)):
                $retorno['title'] = 'Sucesso!';
        $retorno['type'] = 'success';
        $retorno['text'] = 'Empresa cadastrada com sucesso!';
        return $retorno; else:
                $retorno['title'] = 'Erro!';
        $retorno['type'] = 'error';
        $retorno['text'] = 'Erro ao cadastrar empresa!';
        return $retorno;
        endif;
        //}
    }

    public function upEmpresa()
    {
        //if(Request::ajax()) {
        $data = Input::all();
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
                        
                        
                        
        $dataCvempresa['idunidade'] = $idunidade;
        $dataCvempresa['id'] = $data['cvempresa_id'];
        $dataCvempresa['nmempresa'] = $data['nmempresa'];
        $dataCvempresa['cnpj'] = $data['cnpj1'];
        $dataCvempresa['percdesconto'] = $data['percdesconto'];
        $dataCvempresa['idsegmento'] = $data['idsegmento'];
        if (isset($data['value1'])==false) {
            $dataCvempresa['dtvencimento'] =  $this->setData($data['dtvencimento']);
        } else {
            $dataCvempresa['dtvencimento'] = null;
        };
            
        $local = new CVEmpresa();
        if ($local->where('id', '=', $data['cvempresa_id'])->update($dataCvempresa)):
            $retorno['title'] = 'Sucesso!';
            $retorno['type'] = 'success';
            $retorno['text'] = 'Empresa atualizada com sucesso!';
        else:
            $retorno['title'] = 'Erro!';
            $retorno['type'] = 'error';
            $retorno['text'] = 'Erro ao atualizar empresa!';
        endif;
        return $retorno;
        //}
    }

    // DELETAR PSA
    public function deleteCvempresa($id)
    {
        // Local::where('id', $id)->delete();
        
        $dataCvempresa['excluido']='S';
        
        $local = new CVEmpresa();
        if ($local->where('id', '=', $id)->update($dataCvempresa)):
            $retorno['title'] = 'Sucesso!';
            $retorno['type'] = 'success';
            $retorno['text'] = 'Empresa atualizada com sucesso!';
        else:
            $retorno['title'] = 'Erro!';
            $retorno['type'] = 'error';
            $retorno['text'] = 'Erro ao atualizar empresa!';
        endif;     

//        $local = new CVEmpresa();
  //      $local->where('id', '=', $id)->delete();
        
        
        
    }
    public function setData($Data)
    {
        $Format = explode(' ', $Data);
        $Data = explode('/', $Format[0]);
        $Data = $Data[2] . '-' . $Data[1] . '-' . $Data[0];
        return $Data;
    }
    public function getData($Data)
    {
        if ($Data != null):
            $data = date("d/m/Y", strtotime($Data));
        return $data;
        endif;
    }
}
