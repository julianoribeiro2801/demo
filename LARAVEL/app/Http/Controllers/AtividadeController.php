<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Atividade;
use Hash;
use DB;
use App\Http\Requests\AtividadeRequest;

class AtividadeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $headers = ['category' => 'Empresa', 'title' => 'Atividades '];
        $atividades = Atividade::all();
        return view('admin.atividades.index', compact('atividades', 'headers'));
    }

    public function edit($id)
    {
        $atividade = Atividade::where('id', $id)->first();
        return view('admin.atividades.edit', compact('atividade', 'headers'));
    }

    public function create()
    {
        $headers = ['category' => 'Empresa', 'title' => 'Atividades '];
        return view('admin.atividades.create', compact('headers'));
    }

    public function update(AtividadeRequest $request, $id)
    {
        $data = $request->all();

        Atividade::where('id', $id)->update(['nmatividade' => $data['nmatividade'],
                                           'qtgastocalorico' => $data['qtgastocalorico'],
                                           'tpatividade' => $data['tpatividade'],
                                           'stmenuapp' => $data['stmenuapp']

       ]);
     
        return redirect()->route('admin.empresa.atividades.index');
    }

    public function store(AtividadeRequest $request)
    {
        $data = $request->all();
       
        Atividade::create(['nmatividade' => $data['nmatividade'],
                         'qtgastocalorico' => $data['qtgastocalorico'],
                         'tpatividade' => $data['tpatividade'],
                         'stmenuapp' => $data['stmenuapp']
      ]);
        return redirect()->route('admin.empresa.atividades.index');
    }
}
