<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers;

use \App\Models\User;
use \App\Models\Programa;

use App\Services\TrataDadosService;

class MetahopeController extends Controller
{
    public function __construct(TrataDadosService $trataDadosService)
    {
        $this->trataDadosService = $trataDadosService;
    }
    
    public function index()
    {
        $headers = ['category' => 'Psa', 'title' => 'Psa'];
        $alunos =User::select('id', 'name')->where('role', 'cliente')->orderby('name')->get();

        $programas = Programa::select('id', 'nmprograma')->get();
        $selectedProgramas = $this->trataDadosService->listToSelectProgramas($programas);
        
        return view('admin.psas.index', compact('headers', 'alunos', 'programas'));
    }
    public function create()
    {
        $headers = ['category' => 'Psa', 'title' => 'Psa'];
        $alunos =User::select('id', 'name')->where('role', 'cliente')->orderby('name')->get();

        $programas = Programa::select('id', 'nmprograma')->get();
        $selectedProgramas = $this->trataDadosService->listToSelectProgramas($programas);
        
        return view('admin.psas.create', compact('headers', 'alunos', 'programas'));
    }
}
