<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Temapadraoapp;
use App\Models\Telatemaacademia;
use App\Models\Temaacademia;
use App\Models\Telatemaapp;
use App\Models\Telatemapadraoapp;

use App\Services\SendPushService;
use DB;

use App\Models\PushType;

use Input;
use Image;

// Precisa para funcionar o combo
use Auth;
use App\Services\FilialService;


class coresAppController extends Controller
{
    private $sendPushService;

    public function __construct(FilialService $filialService, SendPushService $sendPushService)
    {
        $this->filialService = $filialService;
        $this->sendPushService = $sendPushService;

    }

    


  public function getTemaAcademia($unidade) {
        
        $empresa = Temaacademia::select('tema_academia.*')
                ->where('idunidade', $unidade)
                ->first();        

        $logo = Telatemaacademia::select('tela_tema_academia.*')
                ->where('idunidade', $unidade)
                ->where('nometela', 'Home')
                ->first();        

        $total =0;
        
        if (sizeOf($empresa)> 0){
           if (sizeOf($logo)> 0){ 
            $empresa->logomarca_home= $logo->logomarca_home;
           } 
           if ($empresa->idtema==1):
               $total =0;
           else:
               $total=1;
           endif;
        }    
        
        return ['total'=>$total, 'dados' => $empresa];
        
    }    
    
     
    public function index($tela = 'Home') {
        
        $headers = ['category' => 'Cores App', 'title' => 'Cores App'];

        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }

        //validar se academia tem tema
        $tema = $this->getTemaAcademia($idunidade);


        
        return view('coresApp.index', compact('headers',  'tela', 'tema'));
    }




     public function uploadFile(Request $request) {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        
        

        if ($request->hasFile('image')) {
            
               $destinationPath = 'uploads/logomarcas';
               
               $file = $request->image;
               
               $image_name = time().".".$file->getClientOriginalExtension();
                // 
               //$image_name = "logo_" . str_pad($idunidade, 5, '0',STR_PAD_LEFT)  . "." .$file->getClientOriginalExtension();
               
               //
               $file->move( $destinationPath, $image_name);
               $image = Image::make(sprintf('uploads/logomarcas/%s', $image_name))->resize(600, null, function($constraint){$constraint->aspectRatio();})->save();
               
               $dados['logomarca_home']=$image_name ;
               
               
               
               Telatemaacademia::where('nometela', 'Home')->where('idunidade',$idunidade)->update($dados);
               $retorno['title'] = 'Sucesso!';
               $retorno['type'] = 'success';
               $retorno['text'] = 'Tema atualizado com sucesso!';               
               
               return ['status'=> 'success', 'image_name' => $image_name ];
        }
        else{
            unset($data['image']);
            return ['status'=> 'error'];

        }
        
        

    }

   
    public function getTemas()
    {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }

        $temas=Temapadraoapp::select('tema_padrao_app.*')
                ->get();
        
        /*foreach ($agendas as $key => $value) {
*/
        return response()->json(compact('temas'));
    }  
    
    
    public function addTema(){
        $data = Input::All();
        
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }        
        $data['idunidade']=$idunidade;
        
        
        $cod=isset($data['id']) ? $data['id'] : 0;
              
        
        
        $telaHome=Telatemaacademia::select('tela_tema_academia.*')->where('nometela','Home')
                                                         ->where('tema_id',$data['tema_id'])
                                                         ->where('idunidade',$idunidade  )->first();        
        
        $dataAlt['idunidade']=$idunidade;
        $dataAlt['tema_id']=$data['tema_id'];

        
        
        $dataAlt['nometela']=isset($data['nometela'] ) ? $data['nometela'] : '';
        $dataAlt['cor_background']=isset($data['cor_background'] ) ? $data['cor_background'] : $telaHome['cor_background'];
        $dataAlt['cor_nome_aluno']=isset($data['cor_nome_aluno'] ) ? $data['cor_nome_aluno'] : $telaHome['cor_nome_aluno'];
        $dataAlt['cor_rodape']=isset($data['cor_rodape'] ) ? $data['cor_rodape'] : $telaHome['cor_rodape'];
        $dataAlt['cor_texto_rodape']=isset($data['cor_texto_rodape'] ) ? $data['cor_texto_rodape'] : $telaHome['cor_texto_rodape'];
        $dataAlt['cor_texto_topo']=isset($data['cor_texto_topo'] ) ? $data['cor_texto_topo'] : $telaHome['cor_texto_topo'];
        $dataAlt['cor_topo']=isset($data['cor_topo'] ) ? $data['cor_topo'] : $telaHome['cor_topo'];

        $dataAlt['cor_texto_telamensagem']=isset($data['cor_texto_telamensagem'] ) ? $data['cor_texto_telamensagem'] : $telaHome['cor_texto_telamensagem'];
        $dataAlt['cor_mensagem_corsim']=isset($data['cor_mensagem_corsim'] ) ? $data['cor_mensagem_corsim'] : $telaHome['cor_mensagem_corsim'];
        $dataAlt['cor_mensagem_cornao']=isset($data['cor_mensagem_cornao'] ) ? $data['cor_mensagem_cornao'] : $telaHome['cor_mensagem_cornao'];

        
        $dataAlt['cor_gfm_corsim']=isset($data['cor_gfm_corsim'] ) ? $data['cor_gfm_corsim'] : $telaHome['cor_gfm_corsim'];
        $dataAlt['cor_gfm_cornao']=isset($data['cor_gfm_cornao'] ) ? $data['cor_gfm_cornao'] : $telaHome['cor_gfm_cornao'];
        $dataAlt['cor_textogfm']=isset($data['cor_textogfm'] ) ? $data['cor_textogfm'] : $telaHome['cor_textogfm'];
        $dataAlt['cor_fundodiagfm']=isset($data['cor_fundodiagfm'] ) ? $data['cor_fundodiagfm'] : $telaHome['cor_fundodiagfm'];
        $dataAlt['cor_icone_gfm']=isset($data['cor_icone_gfm'] ) ? $data['cor_icone_gfm'] : $telaHome['cor_icone_gfm'];
      
        ///natavcao
        $dataAlt['cor_spm_corsim']=isset($data['cor_spm_corsim'] ) ? $data['cor_spm_corsim'] : $telaHome['cor_spm_corsim'];
        $dataAlt['cor_spm_cornao']=isset($data['cor_spm_cornao'] ) ? $data['cor_spm_cornao'] : $telaHome['cor_spm_cornao'];
        $dataAlt['cor_textospm']=isset($data['cor_textospm'] ) ? $data['cor_textospm'] : $telaHome['cor_textospm'];
        $dataAlt['cor_fundodiaspm']=isset($data['cor_fundodiaspm'] ) ? $data['cor_fundodiaspm'] : $telaHome['cor_fundodiaspm'];
        $dataAlt['cor_icone_spm']=isset($data['cor_icone_spm'] ) ? $data['cor_icone_spm'] : $telaHome['cor_icone_spm'];
      
        //tela cross
        $dataAlt['cor_textocross'] =isset($data['cor_textocross'] ) ? $data['cor_textocross'] : $telaHome['cor_textocross'];
        $dataAlt['cor_fundodesccross'] =isset($data['cor_fundodesccross'] ) ? $data['cor_fundodesccross'] : $telaHome['cor_fundodesccross'];
        $dataAlt['cor_botao_partiu'] =isset($data['cor_botao_partiu'] ) ? $data['cor_botao_partiu'] : $telaHome['cor_botao_partiu'];
        $dataAlt['cor_botao_iniciar'] =isset($data['cor_botao_iniciar'] ) ? $data['cor_botao_iniciar'] : $telaHome['cor_botao_iniciar'];
        $dataAlt['cor_cross_corsim'] =isset($data['cor_cross_corsim'] ) ? $data['cor_cross_corsim'] : $telaHome['cor_cross_corsim'];
        $dataAlt['cor_cross_cornao'] =isset($data['cor_cross_cornao'] ) ? $data['cor_cross_cornao'] : $telaHome['cor_cross_cornao'];
        
        
        ////menu
        $dataAlt['cor_textomenu'] =isset($data['cor_textomenu'] ) ? $data['cor_textomenu'] : $telaHome['cor_textomenu'];
        $dataAlt['cor_fundomenu'] =isset($data['cor_fundomenu'] ) ? $data['cor_fundomenu'] : $telaHome['cor_fundomenu'];
        $dataAlt['cor_iconemenu'] =isset($data['cor_iconemenu'] ) ? $data['cor_iconemenu'] : $telaHome['cor_iconemenu'];

        $dataAlt['transparencia'] =isset($data['transparencia'] ) ? $data['transparencia'] : $telaHome['transparencia'];
        
        //////        musculacao
        $dataAlt['cor_texto_musc'] =isset($data['cor_texto_musc'] ) ? $data['cor_texto_musc'] : $telaHome['cor_texto_musc'];
        $dataAlt['cor_ativo_musc'] =isset($data['cor_ativo_musc'] ) ? $data['cor_ativo_musc'] : $telaHome['cor_ativo_musc'];
        $dataAlt['cor_inativo_musc'] =isset($data['cor_inativo_musc'] ) ? $data['cor_inativo_musc'] : $telaHome['cor_inativo_musc'];
        $dataAlt['cor_partiu_musc'] =isset($data['cor_partiu_musc'] ) ? $data['cor_partiu_musc'] : $telaHome['cor_partiu_musc'];
        $dataAlt['cor_iniciar_musc'] =isset($data['cor_iniciar_musc'] ) ? $data['cor_iniciar_musc'] : $telaHome['cor_iniciar_musc'];
        ///treino
        $dataAlt['cor_texto_treino'] =isset($data['cor_texto_treino'] ) ? $data['cor_texto_treino'] : $telaHome['cor_texto_treino'];
        $dataAlt['cor_treino_corsim'] =isset($data['cor_treino_corsim'] ) ? $data['cor_treino_corsim'] : $telaHome['cor_treino_corsim'];
        $dataAlt['cor_treino_cornao'] =isset($data['cor_treino_cornao'] ) ? $data['cor_treino_cornao'] : $telaHome['cor_treino_cornao'];
        $dataAlt['cor_treino_corselec'] =isset($data['cor_treino_corselec'] ) ? $data['cor_treino_corselec'] : $telaHome['cor_treino_corselec'];

        /////chat
        $dataAlt['cor_texto_chat'] =isset($data['cor_texto_chat'] ) ? $data['cor_texto_chat'] : $telaHome['cor_texto_chat'];
        $dataAlt['cor_fundo_chat'] =isset($data['cor_fundo_chat'] ) ? $data['cor_fundo_chat'] : $telaHome['cor_fundo_chat'];
        $dataAlt['cor_fundo_enviada'] =isset($data['cor_fundo_enviada'] ) ? $data['cor_fundo_enviada'] : $telaHome['cor_fundo_enviada'];
        $dataAlt['cor_fundo_recebida'] =isset($data['cor_fundo_recebida'] ) ? $data['cor_fundo_recebida'] : $telaHome['cor_fundo_recebida'];
        ////
        /////contato
        $dataAlt['cor_texto_contato'] =isset($data['cor_texto_contato'] ) ? $data['cor_texto_contato'] : $telaHome['cor_texto_contato'];
        $dataAlt['cor_fundo_contato'] =isset($data['cor_fundo_contato'] ) ? $data['cor_fundo_contato'] : $telaHome['cor_fundo_contato'];
        $dataAlt['cor_iniciar_contato'] =isset($data['cor_iniciar_contato'] ) ? $data['cor_iniciar_contato'] : $telaHome['cor_iniciar_contato'];
        
        ////clube
        $dataAlt['cor_texto_clube'] =isset($data['cor_texto_clube'] ) ? $data['cor_texto_clube'] : $telaHome['cor_texto_clube'];
        $dataAlt['cor_cab_clube'] =isset($data['cor_cab_clube'] ) ? $data['cor_cab_clube'] : $telaHome['cor_cab_clube'];
        $dataAlt['cor_impar_clube'] =isset($data['cor_impar_clube'] ) ? $data['cor_impar_clube'] : $telaHome['cor_impar_clube'];
        $dataAlt['cor_par_clube'] =isset($data['cor_par_clube'] ) ? $data['cor_par_clube'] : $telaHome['cor_par_clube'];        
        ////////ciclismo
        $dataAlt['cor_textociclismo'] =isset($data['cor_textociclismo'] ) ? $data['cor_textociclismo'] : $telaHome['cor_textociclismo'];
        $dataAlt['cor_fundodescciclismo'] =isset($data['cor_fundodescciclismo'] ) ? $data['cor_fundodescciclismo'] : $telaHome['cor_fundodescciclismo'];
        $dataAlt['cor_botao_partiuciclismo'] =isset($data['cor_botao_partiuciclismo'] ) ? $data['cor_botao_partiuciclismo'] : $telaHome['cor_botao_partiuciclismo'];
        $dataAlt['cor_botao_iniciarciclismo'] =isset($data['cor_botao_iniciarciclismo'] ) ? $data['cor_botao_iniciarciclismo'] : $telaHome['cor_botao_iniciarciclismo'];
        $dataAlt['cor_ciclismo_corsim'] =isset($data['cor_ciclismo_corsim'] ) ? $data['cor_ciclismo_corsim'] : $telaHome['cor_ciclismo_corsim'];
        $dataAlt['cor_ciclismo_cornao'] =isset($data['cor_ciclismo_cornao'] ) ? $data['cor_ciclismo_cornao'] : $telaHome['cor_ciclismo_cornao'];
        
        ////////corrida
        $dataAlt['cor_textocorrida'] =isset($data['cor_textocorrida'] ) ? $data['cor_textocorrida'] : $telaHome['cor_textocorrida'];
        $dataAlt['cor_fundodesccorrida'] =isset($data['cor_fundodesccorrida'] ) ? $data['cor_fundodesccorrida'] : $telaHome['cor_fundodesccorrida'];
        $dataAlt['cor_botao_partiucorrida'] =isset($data['cor_botao_partiucorrida'] ) ? $data['cor_botao_partiucorrida'] : $telaHome['cor_botao_partiucorrida'];
        $dataAlt['cor_botao_iniciarcorrida'] =isset($data['cor_botao_iniciarcorrida'] ) ? $data['cor_botao_iniciarcorrida'] : $telaHome['cor_botao_iniciarcorrida'];
        $dataAlt['cor_corrida_corsim'] =isset($data['cor_corrida_corsim'] ) ? $data['cor_corrida_corsim'] : $telaHome['cor_corrida_corsim'];
        $dataAlt['cor_corrida_cornao'] =isset($data['cor_corrida_cornao'] ) ? $data['cor_corrida_cornao'] : $telaHome['cor_corrida_cornao'];
        
        ////reserva
        $dataAlt['cor_textoreserva'] =isset($data['cor_textoreserva'] ) ? $data['cor_textoreserva'] : $telaHome['cor_textoreserva'];
        $dataAlt['cor_fundoreserva'] =isset($data['cor_fundoreserva'] ) ? $data['cor_fundoreserva'] : $telaHome['cor_fundoreserva'];
        $dataAlt['cor_botao_reserva'] =isset($data['cor_botao_reserva'] ) ? $data['cor_botao_reserva'] : $telaHome['cor_botao_reserva'];
        $dataAlt['cor_boneco_reserva'] =isset($data['cor_boneco_reserva'] ) ? $data['cor_boneco_reserva'] : $telaHome['cor_boneco_reserva'];
        ///exercicio
        $dataAlt['cor_textoexer'] =isset($data['cor_textoexer'] ) ? $data['cor_textoexer'] : $telaHome['cor_textoexer'];
        $dataAlt['cor_fundoexer'] =isset($data['cor_fundoexer'] ) ? $data['cor_fundoexer'] : $telaHome['cor_fundoexer'];
        $dataAlt['cor_imparexer'] =isset($data['cor_imparexer'] ) ? $data['cor_imparexer'] : $telaHome['cor_imparexer'];
        $dataAlt['cor_parexer'] =isset($data['cor_parexer'] ) ? $data['cor_parexer'] : $telaHome['cor_parexer'];
        
        ///////seleciona treino
        $dataAlt['cor_iconeseleciona'] =isset($data['cor_iconeseleciona'] ) ? $data['cor_iconeseleciona'] : $telaHome['cor_iconeseleciona'];
        $dataAlt['cor_fundoiconeseleciona'] =isset($data['cor_fundoiconeseleciona'] ) ? $data['cor_fundoiconeseleciona'] : $telaHome['cor_fundoiconeseleciona'];
        $dataAlt['cor_selecionaimpar'] =isset($data['cor_selecionaimpar'] ) ? $data['cor_selecionaimpar'] : $telaHome['cor_selecionaimpar'];
        $dataAlt['cor_selecionapar'] =isset($data['cor_selecionapar'] ) ? $data['cor_selecionapar'] : $telaHome['cor_selecionapar'];
        //empresa
        $dataAlt['cor_textoempresa'] =isset($data['cor_textoempresa'] ) ? $data['cor_textoempresa'] : $telaHome['cor_textoempresa'];
        $dataAlt['cor_fundoempresa'] =isset($data['cor_fundoempresa'] ) ? $data['cor_fundoempresa'] : $telaHome['cor_fundoempresa'];
        $dataAlt['cor_cabecempresa'] =isset($data['cor_cabecempresa'] ) ? $data['cor_cabecempresa'] : $telaHome['cor_cabecempresa'];
        //planejamento
        $dataAlt['cor_textoplan'] =isset($data['cor_textoplan'] ) ? $data['cor_textoplan'] : $telaHome['cor_textoplan'];
        $dataAlt['cor_fundoplan'] =isset($data['cor_fundoplan'] ) ? $data['cor_fundoplan'] : $telaHome['cor_fundoplan'];
        $dataAlt['cor_diaplan'] =isset($data['cor_diaplan'] ) ? $data['cor_diaplan'] : $telaHome['cor_diaplan'];
        $dataAlt['cor_horaplan'] =isset($data['cor_horaplan'] ) ? $data['cor_horaplan'] : $telaHome['cor_horaplan'];
        $dataAlt['cor_atvplan'] =isset($data['cor_atvplan'] ) ? $data['cor_atvplan'] : $telaHome['cor_atvplan'];
        
        ///conquistas
        $dataAlt['cor_textoconquistas'] =isset($data['cor_textoconquistas'] ) ? $data['cor_textoconquistas'] : $telaHome['cor_textoconquistas'];
        $dataAlt['cor_fundoconquistas'] =isset($data['cor_fundoconquistas'] ) ? $data['cor_fundoconquistas'] : $telaHome['cor_fundoconquistas'];
        ///coach
        $dataAlt['cor_fundo_coach'] =isset($data['cor_fundo_coach'] ) ? $data['cor_fundo_coach'] : $telaHome['cor_fundo_coach'];
        $dataAlt['cor_texto_coach'] =isset($data['cor_texto_coach'] ) ? $data['cor_texto_coach'] : $telaHome['cor_texto_coach'];
        $dataAlt['transparencia_coach'] =isset($data['transparencia_coach'] ) ? $data['transparencia_coach'] : $telaHome['transparencia_coach'];
        
        
        $temas=Telatemaacademia::select('tela_tema_academia.*')->where('nometela',$data['nometela'])
                ->where('tema_id',$dataAlt['tema_id'])->first();

        if (sizeof($temas)>0):
            
            Telatemaacademia::where('id', $temas['id'])->update($dataAlt);
            $retorno['title'] = 'Sucesso!';
            $retorno['type'] = 'success';
            $retorno['text'] = 'Tema atualizado com sucesso!';
            
        //    return $retorno;
            
        else:

            Telatemaacademia::create($dataAlt);
            $retorno['title'] = 'Erro!';
            $retorno['type'] = 'error';
            $retorno['text'] = 'Tema criado com sucesso!';
         //  return $retorno;

        endif;
        
        $temaAlt['idunidade']=$idunidade;
        $temaAlt['idtema']=$data['tema_id'];
        
        $temaAcad=Temaacademia::select('tema_academia.*')->where('idunidade',$idunidade)->first()    ;    
        if (sizeof($temaAcad)>0):
            Temaacademia::where('id', $temaAcad['id'])->update(['idtema' => $data['tema_id'] ]);
            $retorno['title'] = 'Sucesso!';
            $retorno['type'] = 'success';
            $retorno['text'] = 'Tema academia atualizado com sucesso!';
           return $retorno;
        else:
            Temaacademia::create($temaAlt);
            $retorno['title'] = 'Sucesso!';
            $retorno['type'] = 'success';
            $retorno['text'] = 'Tema academia criado com sucesso!';
            return $retorno;
        endif;
        
        return $data;
        
    }

    public function getTelasTema()
    {
        $data = Input::All();
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }

        $telas=  Telatemaapp::select('tela_tema_app.*')
                //->where('idunidade',$idunidade)
                //->where('tema_id',$data['id'])
                ->orderby('id')
                ->get();
        /*$telas=  Telatemaacademia::select('tela_tema_academia.*')
                ->where('idunidade',$idunidade)
                ->where('tema_id',$data['id'])
                ->get();*/
        
        return response()->json(compact('telas'));
    }    
    
    public function getTelaTema(Request $request)
    {
        $data = Input::All();
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        
        
        $sql = "select * from tela_tema_academia where idunidade = {$idunidade} and tema_id = {$data['idtema']} and nometela = '{$data['nometela']}' ";
        
        $tela = DB::select($sql);
        $sql = "select cor_background as cor_background_home,cor_topo as cor_topo_home,"
                . " cor_rodape as cor_rodape_home,cor_texto_topo as cor_texto_topo_home,"
                . " cor_texto_rodape as cor_texto_rodape_home from tela_tema_academia where idunidade = {$idunidade} and tema_id = {$data['idtema']} and nometela = 'Home' ";

        $home = DB::select($sql);
        
        return response()->json(compact('home','tela'));
    }      

    
    // Criei uma função nova somente para o App Pq senao estava dando prombelema 
    // e a unidade tem q passar por post mesmo pq como esta vindo do app nao tem como ver por session
    public function getTelaTemaApp(Request $request)
    {
        
        $idunidade = $request->idunidade;

        $tela=  Telatemapadraoapp::select('tela_tema_padrao.*')
                ->where('tema_id', $request->tema_id)
                ->where('id', $request->id)
                ->get();
        
        return response()->json(compact('tela'));
    }     
    
    
    public function gerarTema($unidade) {

        $data = "";
        $telas = Telatemaapp::select('tela_tema_app.*')->get();



        foreach ($telas as $key => $value) {

            $dataAlt['idunidade'] = $unidade;
            $dataAlt['tema_id'] = 2;

            $telasP = Telatemapadraoapp::select('tela_tema_padrao.*')->where('nometela', $telas[$key]['nometela'])->get();
            if (sizeof($telasP) > 0):

                $dataAlt['nometela'] = isset($telasP[0]['nometela']) ? $telasP[0]['nometela'] : '';
                $dataAlt['cor_background'] = isset($telasP[0]['cor_background']) ? $telasP[0]['cor_background'] : '';
                $dataAlt['cor_nome_aluno'] = isset($telasP[0]['cor_nome_aluno']) ? $telasP[0]['cor_nome_aluno'] : '';
                $dataAlt['cor_rodape'] = isset($telasP[0]['cor_rodape']) ? $telasP[0]['cor_rodape'] : '';
                $dataAlt['cor_texto_rodape'] = isset($telasP[0]['cor_texto_rodape']) ? $telasP[0]['cor_texto_rodape'] : '';
                $dataAlt['cor_texto_topo'] = isset($telasP[0]['cor_texto_topo']) ? $telasP[0]['cor_texto_topo'] : '';
                $dataAlt['cor_topo'] = isset($telasP[0]['cor_topo']) ? $telasP[0]['cor_topo'] : '';

                $dataAlt['cor_texto_telamensagem'] = isset($telasP[0]['cor_texto_telamensagem']) ? $telasP[0]['cor_texto_telamensagem'] : '';
                $dataAlt['cor_mensagem_corsim'] = isset($telasP[0]['cor_mensagem_corsim']) ? $telasP[0]['cor_mensagem_corsim'] : '';
                $dataAlt['cor_mensagem_cornao'] = isset($telasP[0]['cor_mensagem_cornao']) ? $telasP[0]['cor_mensagem_cornao'] : '';


                $dataAlt['cor_gfm_corsim'] = isset($telasP[0]['cor_gfm_corsim']) ? $telasP[0]['cor_gfm_corsim'] : '';
                $dataAlt['cor_gfm_cornao'] = isset($telasP[0]['cor_gfm_cornao']) ? $telasP[0]['cor_gfm_cornao'] : '';
                $dataAlt['cor_textogfm'] = isset($telasP[0]['cor_textogfm']) ? $telasP[0]['cor_textogfm'] : '';
                $dataAlt['cor_fundodiagfm'] = isset($telasP[0]['cor_fundodiagfm']) ? $telasP[0]['cor_fundodiagfm'] : '';

                ///natavcao
                $dataAlt['cor_spm_corsim'] = isset($telasP[0]['cor_spm_corsim']) ? $telasP[0]['cor_spm_corsim'] : '';
                $dataAlt['cor_spm_cornao'] = isset($telasP[0]['cor_spm_cornao']) ? $telasP[0]['cor_spm_cornao'] : '';
                $dataAlt['cor_textospm'] = isset($telasP[0]['cor_textospm']) ? $telasP[0]['cor_textospm'] : '';
                $dataAlt['cor_fundodiaspm'] = isset($telasP[0]['cor_fundodiaspm']) ? $telasP[0]['cor_fundodiaspm'] : '';

                //tela cross
                $dataAlt['cor_textocross'] = isset($telasP[0]['cor_textocross']) ? $telasP[0]['cor_textocross'] : '';
                $dataAlt['cor_fundodesccross'] = isset($telasP[0]['cor_fundodesccross']) ? $telasP[0]['cor_fundodesccross'] : '';
                $dataAlt['cor_botao_partiu'] = isset($telasP[0]['cor_botao_partiu']) ? $telasP[0]['cor_botao_partiu'] : '';
                $dataAlt['cor_botao_iniciar'] = isset($telasP[0]['cor_botao_iniciar']) ? $telasP[0]['cor_botao_iniciar'] : '';
                $dataAlt['cor_cross_corsim'] = isset($telasP[0]['cor_cross_corsim']) ? $telasP[0]['cor_cross_corsim'] : '';
                $dataAlt['cor_cross_cornao'] = isset($telasP[0]['cor_cross_cornao']) ? $telasP[0]['cor_cross_cornao'] : '';


                ////menu
                $dataAlt['cor_textomenu'] = isset($telasP[0]['cor_textomenu']) ? $telasP[0]['cor_textomenu'] : '';
                $dataAlt['cor_fundomenu'] = isset($telasP[0]['cor_fundomenu']) ? $telasP[0]['cor_fundomenu'] : '';
                $dataAlt['cor_iconemenu'] = isset($telasP[0]['cor_iconemenu']) ? $telasP[0]['cor_iconemenu'] : '';

                $dataAlt['transparencia'] = isset($telasP[0]['transparencia']) ? $telasP[0]['transparencia'] : '';

                //////        musculacao
                $dataAlt['cor_texto_musc'] = isset($telasP[0]['cor_texto_musc']) ? $telasP[0]['cor_texto_musc'] : '';
                $dataAlt['cor_ativo_musc'] = isset($telasP[0]['cor_ativo_musc']) ? $telasP[0]['cor_ativo_musc'] : '';
                $dataAlt['cor_inativo_musc'] = isset($telasP[0]['cor_inativo_musc']) ? $telasP[0]['cor_inativo_musc'] : '';
                $dataAlt['cor_partiu_musc'] = isset($telasP[0]['cor_partiu_musc']) ? $telasP[0]['cor_partiu_musc'] : '';
                $dataAlt['cor_iniciar_musc'] = isset($telasP[0]['cor_iniciar_musc']) ? $telasP[0]['cor_iniciar_musc'] : '';
                ///treino
                $dataAlt['cor_texto_treino'] = isset($telasP[0]['cor_texto_treino']) ? $telasP[0]['cor_texto_treino'] : '';
                $dataAlt['cor_treino_corsim'] = isset($telasP[0]['cor_treino_corsim']) ? $telasP[0]['cor_treino_corsim'] : '';
                $dataAlt['cor_treino_cornao'] = isset($telasP[0]['cor_treino_cornao']) ? $telasP[0]['cor_treino_cornao'] : '';
                $dataAlt['cor_treino_corselec'] = isset($telasP[0]['cor_treino_corselec']) ? $telasP[0]['cor_treino_corselec'] : '';

                /////chat
                $dataAlt['cor_texto_chat'] = isset($telasP[0]['cor_texto_chat']) ? $telasP[0]['cor_texto_chat'] : '';
                $dataAlt['cor_fundo_chat'] = isset($telasP[0]['cor_fundo_chat']) ? $telasP[0]['cor_fundo_chat'] : '';
                $dataAlt['cor_fundo_enviada'] = isset($telasP[0]['cor_fundo_enviada']) ? $telasP[0]['cor_fundo_enviada'] : '';
                $dataAlt['cor_fundo_recebida'] = isset($telasP[0]['cor_fundo_recebida']) ? $telasP[0]['cor_fundo_recebida'] : '';
                ////
                /////contato
                $dataAlt['cor_texto_contato'] = isset($telasP[0]['cor_texto_contato']) ? $telasP[0]['cor_texto_contato'] : '';
                $dataAlt['cor_fundo_contato'] = isset($telasP[0]['cor_fundo_contato']) ? $telasP[0]['cor_fundo_contato'] : '';

                ////clube
                $dataAlt['cor_texto_clube'] = isset($telasP[0]['cor_texto_clube']) ? $telasP[0]['cor_texto_clube'] : '';
                $dataAlt['cor_cab_clube'] = isset($telasP[0]['cor_cab_clube']) ? $telasP[0]['cor_cab_clube'] : '';
                $dataAlt['cor_impar_clube'] = isset($telasP[0]['cor_impar_clube']) ? $telasP[0]['cor_impar_clube'] : '';
                $dataAlt['cor_par_clube'] = isset($telasP[0]['cor_par_clube']) ? $telasP[0]['cor_par_clube'] : '';
                ////////ciclismo
                $dataAlt['cor_textociclismo'] = isset($telasP[0]['cor_textociclismo']) ? $telasP[0]['cor_textociclismo'] : '';
                $dataAlt['cor_fundodescciclismo'] = isset($telasP[0]['cor_fundodescciclismo']) ? $telasP[0]['cor_fundodescciclismo'] : '';
                $dataAlt['cor_botao_partiuciclismo'] = isset($telasP[0]['cor_botao_partiuciclismo']) ? $telasP[0]['cor_botao_partiuciclismo'] : '';
                $dataAlt['cor_botao_iniciarciclismo'] = isset($telasP[0]['cor_botao_iniciarciclismo']) ? $telasP[0]['cor_botao_iniciarciclismo'] : '';
                $dataAlt['cor_ciclismo_corsim'] = isset($telasP[0]['cor_ciclismo_corsim']) ? $telasP[0]['cor_ciclismo_corsim'] : '';
                $dataAlt['cor_ciclismo_cornao'] = isset($telasP[0]['cor_ciclismo_cornao']) ? $telasP[0]['cor_ciclismo_cornao'] : '';

                ////////corrida
                $dataAlt['cor_textocorrida'] = isset($telasP[0]['cor_textocorrida']) ? $telasP[0]['cor_textocorrida'] : '';
                $dataAlt['cor_fundodesccorrida'] = isset($telasP[0]['cor_fundodesccorrida']) ? $telasP[0]['cor_fundodesccorrida'] : '';
                $dataAlt['cor_botao_partiucorrida'] = isset($telasP[0]['cor_botao_partiucorrida']) ? $telasP[0]['cor_botao_partiucorrida'] : '';
                $dataAlt['cor_botao_iniciarcorrida'] = isset($telasP[0]['cor_botao_iniciarcorrida']) ? $telasP[0]['cor_botao_iniciarcorrida'] : '';
                $dataAlt['cor_corrida_corsim'] = isset($telasP[0]['cor_corrida_corsim']) ? $telasP[0]['cor_corrida_corsim'] : '';
                $dataAlt['cor_corrida_cornao'] = isset($telasP[0]['cor_corrida_cornao']) ? $telasP[0]['cor_corrida_cornao'] : '';

                ////reserva
                $dataAlt['cor_textoreserva'] = isset($telasP[0]['cor_textoreserva']) ? $telasP[0]['cor_textoreserva'] : '';
                $dataAlt['cor_fundoreserva'] = isset($telasP[0]['cor_fundoreserva']) ? $telasP[0]['cor_fundoreserva'] : '';
                $dataAlt['cor_botao_reserva'] = isset($telasP[0]['cor_botao_reserva']) ? $telasP[0]['cor_botao_reserva'] : '';
                ///exercicio
                $dataAlt['cor_textoexer'] = isset($telasP[0]['cor_textoexer']) ? $telasP[0]['cor_textoexer'] : '';
                $dataAlt['cor_fundoexer'] = isset($telasP[0]['cor_fundoexer']) ? $telasP[0]['cor_fundoexer'] : '';
                $dataAlt['cor_imparexer'] = isset($telasP[0]['cor_imparexer']) ? $telasP[0]['cor_imparexer'] : '';
                $dataAlt['cor_parexer'] = isset($telasP[0]['cor_parexer']) ? $telasP[0]['cor_parexer'] : '';

                ///////seleciona treino
                $dataAlt['cor_iconeseleciona'] = isset($telasP[0]['cor_iconeseleciona']) ? $telasP[0]['cor_iconeseleciona'] : '';
                $dataAlt['cor_fundoiconeseleciona'] = isset($telasP[0]['cor_fundoiconeseleciona']) ? $telasP[0]['cor_fundoiconeseleciona'] : '';
                //empresa
                $dataAlt['cor_textoempresa'] = isset($telasP[0]['cor_textoempresa']) ? $telasP[0]['cor_textoempresa'] : '';
                $dataAlt['cor_fundoempresa'] = isset($telasP[0]['cor_fundoempresa']) ? $telasP[0]['cor_fundoempresa'] : '';
                $dataAlt['cor_cabecempresa'] = isset($telasP[0]['cor_cabecempresa']) ? $telasP[0]['cor_cabecempresa'] : '';

                
            else:
                $dataAlt['nometela'] = isset($data['nometela']) ? $data['nometela'] : '';
                $dataAlt['cor_background'] = isset($data['cor_background']) ? $data['cor_background'] : '';
                $dataAlt['cor_nome_aluno'] = isset($data['cor_nome_aluno']) ? $data['cor_nome_aluno'] : '';
                $dataAlt['cor_rodape'] = isset($data['cor_rodape']) ? $data['cor_rodape'] : '';
                $dataAlt['cor_texto_rodape'] = isset($data['cor_texto_rodape']) ? $data['cor_texto_rodape'] : '';
                $dataAlt['cor_texto_topo'] = isset($data['cor_texto_topo']) ? $data['cor_texto_topo'] : '';
                $dataAlt['cor_topo'] = isset($data['cor_topo']) ? $data['cor_topo'] : '';

                $dataAlt['cor_texto_telamensagem'] = isset($data['cor_texto_telamensagem']) ? $data['cor_texto_telamensagem'] : '';
                $dataAlt['cor_mensagem_corsim'] = isset($data['cor_mensagem_corsim']) ? $data['cor_mensagem_corsim'] : '';
                $dataAlt['cor_mensagem_cornao'] = isset($data['cor_mensagem_cornao']) ? $data['cor_mensagem_cornao'] : '';


                $dataAlt['cor_gfm_corsim'] = isset($data['cor_gfm_corsim']) ? $data['cor_gfm_corsim'] : '';
                $dataAlt['cor_gfm_cornao'] = isset($data['cor_gfm_cornao']) ? $data['cor_gfm_cornao'] : '';
                $dataAlt['cor_textogfm'] = isset($data['cor_textogfm']) ? $data['cor_textogfm'] : '';
                $dataAlt['cor_fundodiagfm'] = isset($data['cor_fundodiagfm']) ? $data['cor_fundodiagfm'] : '';

                ///natavcao
                $dataAlt['cor_spm_corsim'] = isset($data['cor_spm_corsim']) ? $data['cor_spm_corsim'] : '';
                $dataAlt['cor_spm_cornao'] = isset($data['cor_spm_cornao']) ? $data['cor_spm_cornao'] : '';
                $dataAlt['cor_textospm'] = isset($data['cor_textospm']) ? $data['cor_textospm'] : '';
                $dataAlt['cor_fundodiaspm'] = isset($data['cor_fundodiaspm']) ? $data['cor_fundodiaspm'] : '';

                //tela cross
                $dataAlt['cor_textocross'] = isset($data['cor_textocross']) ? $data['cor_textocross'] : '';
                $dataAlt['cor_fundodesccross'] = isset($data['cor_fundodesccross']) ? $data['cor_fundodesccross'] : '';
                $dataAlt['cor_botao_partiu'] = isset($data['cor_botao_partiu']) ? $data['cor_botao_partiu'] : '';
                $dataAlt['cor_botao_iniciar'] = isset($data['cor_botao_iniciar']) ? $data['cor_botao_iniciar'] : '';
                $dataAlt['cor_cross_corsim'] = isset($data['cor_cross_corsim']) ? $data['cor_cross_corsim'] : '';
                $dataAlt['cor_cross_cornao'] = isset($data['cor_cross_cornao']) ? $data['cor_cross_cornao'] : '';


                ////menu
                $dataAlt['cor_textomenu'] = isset($data['cor_textomenu']) ? $data['cor_textomenu'] : '';
                $dataAlt['cor_fundomenu'] = isset($data['cor_fundomenu']) ? $data['cor_fundomenu'] : '';
                $dataAlt['cor_iconemenu'] = isset($data['cor_iconemenu']) ? $data['cor_iconemenu'] : '';

                $dataAlt['transparencia'] = isset($data['transparencia']) ? $data['transparencia'] : '';

                //////        musculacao
                $dataAlt['cor_texto_musc'] = isset($data['cor_texto_musc']) ? $data['cor_texto_musc'] : '';
                $dataAlt['cor_ativo_musc'] = isset($data['cor_ativo_musc']) ? $data['cor_ativo_musc'] : '';
                $dataAlt['cor_inativo_musc'] = isset($data['cor_inativo_musc']) ? $data['cor_inativo_musc'] : '';
                $dataAlt['cor_partiu_musc'] = isset($data['cor_partiu_musc']) ? $data['cor_partiu_musc'] : '';
                $dataAlt['cor_iniciar_musc'] = isset($data['cor_iniciar_musc']) ? $data['cor_iniciar_musc'] : '';
                ///treino
                $dataAlt['cor_texto_treino'] = isset($data['cor_texto_treino']) ? $data['cor_texto_treino'] : '';
                $dataAlt['cor_treino_corsim'] = isset($data['cor_treino_corsim']) ? $data['cor_treino_corsim'] : '';
                $dataAlt['cor_treino_cornao'] = isset($data['cor_treino_cornao']) ? $data['cor_treino_cornao'] : '';
                $dataAlt['cor_treino_corselec'] = isset($data['cor_treino_corselec']) ? $data['cor_treino_corselec'] : '';

                /////chat
                $dataAlt['cor_texto_chat'] = isset($data['cor_texto_chat']) ? $data['cor_texto_chat'] : '';
                $dataAlt['cor_fundo_chat'] = isset($data['cor_fundo_chat']) ? $data['cor_fundo_chat'] : '';
                $dataAlt['cor_fundo_enviada'] = isset($data['cor_fundo_enviada']) ? $data['cor_fundo_enviada'] : '';
                $dataAlt['cor_fundo_recebida'] = isset($data['cor_fundo_recebida']) ? $data['cor_fundo_recebida'] : '';
                ////
                /////contato
                $dataAlt['cor_texto_contato'] = isset($data['cor_texto_contato']) ? $data['cor_texto_contato'] : '';
                $dataAlt['cor_fundo_contato'] = isset($data['cor_fundo_contato']) ? $data['cor_fundo_contato'] : '';

                ////clube
                $dataAlt['cor_texto_clube'] = isset($data['cor_texto_clube']) ? $data['cor_texto_clube'] : '';
                $dataAlt['cor_cab_clube'] = isset($data['cor_cab_clube']) ? $data['cor_cab_clube'] : '';
                $dataAlt['cor_impar_clube'] = isset($data['cor_impar_clube']) ? $data['cor_impar_clube'] : '';
                $dataAlt['cor_par_clube'] = isset($data['cor_par_clube']) ? $data['cor_par_clube'] : '';
                ////////ciclismo
                $dataAlt['cor_textociclismo'] = isset($data['cor_textociclismo']) ? $data['cor_textociclismo'] : '';
                $dataAlt['cor_fundodescciclismo'] = isset($data['cor_fundodescciclismo']) ? $data['cor_fundodescciclismo'] : '';
                $dataAlt['cor_botao_partiuciclismo'] = isset($data['cor_botao_partiuciclismo']) ? $data['cor_botao_partiuciclismo'] : '';
                $dataAlt['cor_botao_iniciarciclismo'] = isset($data['cor_botao_iniciarciclismo']) ? $data['cor_botao_iniciarciclismo'] : '';
                $dataAlt['cor_ciclismo_corsim'] = isset($data['cor_ciclismo_corsim']) ? $data['cor_ciclismo_corsim'] : '';
                $dataAlt['cor_ciclismo_cornao'] = isset($data['cor_ciclismo_cornao']) ? $data['cor_ciclismo_cornao'] : '';

                ////////corrida
                $dataAlt['cor_textocorrida'] = isset($data['cor_textocorrida']) ? $data['cor_textocorrida'] : '';
                $dataAlt['cor_fundodesccorrida'] = isset($data['cor_fundodesccorrida']) ? $data['cor_fundodesccorrida'] : '';
                $dataAlt['cor_botao_partiucorrida'] = isset($data['cor_botao_partiucorrida']) ? $data['cor_botao_partiucorrida'] : '';
                $dataAlt['cor_botao_iniciarcorrida'] = isset($data['cor_botao_iniciarcorrida']) ? $data['cor_botao_iniciarcorrida'] : '';
                $dataAlt['cor_corrida_corsim'] = isset($data['cor_corrida_corsim']) ? $data['cor_corrida_corsim'] : '';
                $dataAlt['cor_corrida_cornao'] = isset($data['cor_corrida_cornao']) ? $data['cor_corrida_cornao'] : '';

                ////reserva
                $dataAlt['cor_textoreserva'] = isset($data['cor_textoreserva']) ? $data['cor_textoreserva'] : '';
                $dataAlt['cor_fundoreserva'] = isset($data['cor_fundoreserva']) ? $data['cor_fundoreserva'] : '';
                $dataAlt['cor_botao_reserva'] = isset($data['cor_botao_reserva']) ? $data['cor_botao_reserva'] : '';
                ///exercicio
                $dataAlt['cor_textoexer'] = isset($data['cor_textoexer']) ? $data['cor_textoexer'] : '';
                $dataAlt['cor_fundoexer'] = isset($data['cor_fundoexer']) ? $data['cor_fundoexer'] : '';
                $dataAlt['cor_imparexer'] = isset($data['cor_imparexer']) ? $data['cor_imparexer'] : '';
                $dataAlt['cor_parexer'] = isset($data['cor_parexer']) ? $data['cor_parexer'] : '';

                ///////seleciona treino
                $dataAlt['cor_iconeseleciona'] = isset($data['cor_iconeseleciona']) ? $data['cor_iconeseleciona'] : '';
                $dataAlt['cor_fundoiconeseleciona'] = isset($data['cor_fundoiconeseleciona']) ? $data['cor_fundoiconeseleciona'] : '';
                //empresa
                $dataAlt['cor_textoempresa'] = isset($data['cor_textoempresa']) ? $data['cor_textoempresa'] : '';
                $dataAlt['cor_fundoempresa'] = isset($data['cor_fundoempresa']) ? $data['cor_fundoempresa'] : '';
                $dataAlt['cor_cabecempresa'] = isset($data['cor_cabecempresa']) ? $data['cor_cabecempresa'] : '';

            endif;
            $telaacad = Telatemaacademia::select('tela_tema_academia.*')->where('nometela', $telas[$key]->nometela)->where('tema_id', 2)->where('idunidade', $unidade)->first();

            if (sizeof($telaacad) > 0):

            /* Telatemaacademia::where('id', $telaacad['id'])->update($dataAlt);
              $retorno['title'] = 'Sucesso!';
              $retorno['type'] = 'success';
              $retorno['text'] = 'Tela atualizado com sucesso!';

              // return $retorno; */

            else:
             // $dataAlt['nometela']=$telas[$key]->nometela;
             Telatemaacademia::create($dataAlt);
             $retorno['title'] = 'Erro!';
              $retorno['type'] = 'error';
             $retorno['text'] = 'Tela criado com sucesso!';
             // return $retorno;

            endif;
        }


        return $telas;
    }

}
