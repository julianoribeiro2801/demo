<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Local;
use Hash;
use App\Http\Requests\LocalRequest;
// Precisa para funcionar o combo
use Auth;
use App\Services\FilialService;

class TipomensagemController extends Controller
{
    public function __construct(FilialService $filialService)
    {
        $this->filialService = $filialService;
    }

    public function index()
    {
        $headers = ['category' => 'Clientes', 'title' => 'Tipo de Conquistas'];
        $unidades_combo = $this->filialService->unidadesComboTop();

        $tipomensagens = Local::all();
        return view('admin.tipomensagens.index', compact('tipomensagens', 'headers', 'unidades_combo'));
    }

    public function edit($id)
    {
        $headers = ['category' => 'Clientes', 'title' => 'Tipo de Conquistas'];
        $unidades_combo = $this->filialService->unidadesComboTop();

        $tipomensagem = Local::where('id', $id)->first();
        return view('admin.tipomensagens.edit', compact('tipomensagem', 'headers', 'unidades_combo'));
    }

    public function create()
    {
        $headers = ['category' => 'Clientes', 'title' => 'Tipo de Conquistas'];
        $unidades_combo = $this->filialService->unidadesComboTop();

        return view('admin.tipomensagens.create', compact('headers', 'unidades_combo'));
    }

    public function update(LocalRequest $request, $id)
    {
        $data = $request->all();

        Local::where('id', $id)->update(['nmlocal' => $data['nmlocal'],
        'tpmensagem' => $data['tpmensagem']

       ]);
     
        return redirect()->route('admin.clientes.tipomensagens.index');
    }

    public function store(LocalRequest $request)
    {
        $data = $request->all();
       
        Local::create(['nmlocal' => $data['nmlocal'],
        'tpmensagem' => $data['tpmensagem']]);
     
        return redirect()->route('admin.clientes.tipomensagens.index');
    }


    // public function create_user(){

          //  User::create( [
    //          'email' => 'kk@msn.com' ,
    //          'password' => Hash::make( '123456' ) ,
    //          'name' => 'KK Ruiz' ,
    //          'role' => 'admin'
    //         ] );
    // }
}
