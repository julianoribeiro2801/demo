<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers;

use \App\Models\User;
use \App\Models\Programa;

use App\Services\TrataDadosService;

// Precisa para funcionar o combo
use Auth;
use App\Services\FilialService;

class PsaController extends Controller
{
    public function __construct(
    
        TrataDadosService $trataDadosService,
        FilialService $filialService
    
    ) {
        $this->trataDadosService = $trataDadosService;
        $this->filialService = $filialService;
    }
    
    public function index()
    {
        $headers = ['category' => 'Psa', 'title' => 'Psa'];
        $unidades_combo = $this->filialService->unidadesComboTop();

        $alunos =User::select('id', 'name')->where('role', 'cliente')->orderby('name')->get();

        $programas = Programa::select('id', 'nmprograma')->get();
        $selectedProgramas = $this->trataDadosService->listToSelectProgramas($programas);
        
        return view('admin.psas.index', compact('headers', 'alunos', 'programas', 'unidades_combo'));
    }
    public function create()
    {
        $headers = ['category' => 'Psa', 'title' => 'Psa'];
        $alunos =User::select('id', 'name')->where('role', 'cliente')->orderby('name')->get();
        $unidades_combo = $this->filialService->unidadesComboTop();
        

        $programas = Programa::select('id', 'nmprograma')->get();
        $selectedProgramas = $this->trataDadosService->listToSelectProgramas($programas);
        
        return view('admin.psas.create', compact('headers', 'alunos', 'programas', 'unidades_combo'));
    }
}
