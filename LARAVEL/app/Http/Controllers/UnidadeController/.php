<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Unidade;
use App\Models\UnidadeDados;
use Hash;
use App\Http\Requests\UnidadeRequest;

// Precisa para funcionar o combo
use Auth;
use App\Services\FilialService;

class UnidadeController extends Controller
{
    
     public function __construct(FilialService $filialService) {
        $this->filialService = $filialService;
    }

    public function index(){
       $headers = ['category' => 'Empresa', 'title' => 'Unidades'];
       $unidades_combo = $this->filialService->unidadesComboTop();
       $idunidade = Auth::user()->idunidade;

       $unidades = Unidade::all();
       return view('admin.unidades.index',compact('unidades','headers','unidades_combo', 'idunidade'));
    }

  

    public function lista(){
      return Unidade::all();
    }
    
    public function getUnidade(){    
       $idunidade = Auth::user()->idunidade;
       
       return response()->json(compact('idunidade'));
    }

    public function empresas(){
       $headers = ['category' => 'Empresa', 'title' => 'Unidades'];
       $unidades = Unidade::all();
       $unidades_combo = $this->filialService->unidadesComboTop();
       $idunidade = Auth::user()->idunidade;
//       dd($unidades_combo->toArray());
       return view('admin.empresa.index',compact('unidades','headers','unidades_combo','idunidade'));
    }
    

    public function edit($id){
    	//$headers = ['category' => 'Empresa', 'title' => 'Unidades'];
       $unidade = Unidade::select('unidade.fantasia','unidade.razao_social','unidade.parent_id',
                                    'unidade_dados.cnpj','unidade_dados.telefone','unidade_dados.celular',
                                    'unidade_dados.email','unidade_dados.site')
                    ->join('unidade_dados','unidade_dados.idunidade','=','unidade.id')
                    ->where('unidade.id', $id)->first();
       return $unidade;
    }

    public function create(){
      $headers = ['category' => 'Empresa', 'title' => 'Unidades'];
       $unidades_combo = $this->filialService->unidadesComboTop();

       return view('admin.unidades.create',compact('headers','unidades_combo'));
    }

    /*public function update(UnidadeRequest $request, $id){  

      $data = $request->all();

      Unidade::where('id', $id)->update(['nmunidade' => $data['nmunidade'],
        'dsemail' => $data['dsemail']

       ]);
     
      return redirect()->route('admin.unidades.index');
      

    }*/

   // public function store(UnidadeRequest $request){  


      /*$data = $request->all();

      $unidade = Unidade::create(['razao_social' => $data['razao_social'],
        'fantasia' => $data['fantasia'],
        'user_id' => 0,
        'parent_id' => $data['tipo'],
        'situacao' => "false"

      ]);
        
        UnidadeDados::create(['user_id' => 0,
            'idunidade' => $unidade->id,
            'telefone' => $data['telefone'],
            'idcidade' =>  1,
            'endereco' => $data['endereco'],
            'numero' => $data['numero'],
            'cnpj' => $data['cnpj'],
            'bairro' => $data['bairro'],
            'metros' => $data['metros'],
            'site' => $data['site'],
            'celular' => $data['celular']
          ]);
     
      return "Salvo com sucesso";
      */

    //}



    
   
}
