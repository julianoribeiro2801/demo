<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;

class AdminBaseController extends Controller
{
    protected $idUnidade;

    public function __construct()
    {
        $this->idUnidade = Session::get('id_unidade');
    }
}
