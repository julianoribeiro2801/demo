<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// Precisa para funcionar o combo
use Auth;
use App\Services\FilialService;

use App\Models\User;
use App\Models\Moduloacademia;
use App\Models\Modulos;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct(FilialService $filialService)
    {
        $this->filialService = $filialService;


    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $headers = ['category' => 'PAINEL', 'title' => 'PAINEL'];
        $unidades_combo = $this->filialService->unidadesComboTop();

        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }

        return view('home', compact('headers', 'unidades_combo', 'idunidade'));
    }


    public function passos()
    {   
        $tela = 'Home';
        $headers = ['category' => 'PAINEL', 'title' => 'PAINEL'];
        $unidades_combo = $this->filialService->unidadesComboTop();


        $id_user = Auth::user()->id;
        $idunidade = Auth::user()->idunidade;

         // dd( Auth::user());
        

        $user = User::select('users.*', 'unidade.logo', 'unidade.fantasia',
            'unidade_dados.cnpj','unidade_dados.celular' , 'unidade_dados.idestado', 'unidade_dados.idcidade')
          ->leftjoin('unidade', 'users.idunidade', '=', 'unidade.id')
          ->leftjoin('unidade_dados', 'unidade_dados.id', '=', 'unidade.id')
          ->where('users.id',$id_user)
          ->where('users.idunidade',$idunidade )
          ->where('users.excluido', '<>', 'S')
          ->where('users.role', '<>', 'prospect')
          ->where('users.parent_id', 0)->first();

        return view('passos_form_led2', compact('headers', 'unidades_combo', 'idunidade',  'tela', 'user'));
    }


    public function passosparabens()
    {   
        $tela = 'Home';
        $headers = ['category' => 'PAINEL', 'title' => 'PAINEL'];
        $unidades_combo = $this->filialService->unidadesComboTop();


        $id_user = Auth::user()->id;
        $idunidade = Auth::user()->idunidade;
        

        $user = User::select('users.*', 'unidade.logo', 'unidade.fantasia',
            'unidade_dados.cnpj','unidade_dados.celular' , 'unidade_dados.idestado', 'unidade_dados.idcidade')
      ->leftjoin('unidade', 'users.idunidade', '=', 'unidade.id')
      ->leftjoin('unidade_dados', 'unidade_dados.id', '=', 'unidade.id')

      ->where('users.id',$id_user)
      ->where('users.idunidade',$idunidade )
      ->where('users.excluido', '<>', 'S')
      ->where('users.role', '<>', 'prospect')
      ->where('users.parent_id', 0)->first();
       

        return view('passos_form_led2_parabens', compact('headers', 'unidades_combo', 'idunidade',  'tela', 'user'));
    }

    public function homeAdmin()
    {
        $headers = ['category' => 'PAINEL', 'title' => 'PAINEL'];
        $unidades_combo = $this->filialService->unidadesComboTop();

        $idunidade = Auth::user()->idunidade;

        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }

        // so mudar passo = 1
        if (Auth::user()->role == 'admin' && Auth::user()->passo < 3) {
             return redirect('admin/passos');
        }


        return view('home_admin', compact('headers', 'unidades_combo', 'idunidade'));
    }



    public function difDatas($dia) {

        $diferenca = strtotime(date('Y-m-d')) - strtotime($dia);

        $dias = floor($diferenca / (60 * 60 * 24));


        return $dias;


    }

     public function modulosSetModulos($idunidade){
        $modulos = Modulos::where('modulos.status',1)->get();

      
        foreach ($modulos as $value) {

           $modulos_has_unidade = Moduloacademia::where('id_unidade',$idunidade)
           ->where('id_modulo',$value->id)
           ->get();
           if(count($modulos_has_unidade)>0){

           } else{
            $status = 1;
            // se for o modulo de cores vem desativado e restante ativa tudo
            if($value->id == 1) { $status = 0; }
            $insert = ['id_unidade' => $idunidade,  'id_modulo' => $value->id ,
            'pago', 'N', 'status' => $status];

            Moduloacademia::create( $insert);
           }
          
        }
     }

    public function apps()
    {
        $headers = ['category' => 'Apps', 'title' => 'Apps'];
        $unidades_combo = $this->filialService->unidadesComboTop();

        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }

        // se o modulo nao existir ele cria
        $this->modulosSetModulos($idunidade);

        // modulos comprados
        $modulos = Modulos::select('modulos.*', 'mu.created_at as dt_compra', 'mu.pago')
        ->leftjoin('modulos_has_unidade as mu', 'mu.id_modulo', '=', 'modulos.id')
        ->where('mu.id_unidade', $idunidade )
        ->where('modulos.status',1)->get();

        // calculo dias trial
        foreach($modulos as $value){
        
          $value->dia_atraso =  $this->difDatas($value->dt_compra) - $value->trial;
          $value->dt_compra = substr($value->dt_compra,0,10);
          $value->dt_compra =  implode('/', array_reverse(explode('-', $value->dt_compra)));
         

        }

     

        return view('admin.apps.index', compact('headers', 'unidades_combo', 'idunidade',  'modulos'));


    }


    public function trial(){


        return view('trial.index');


    }
}
