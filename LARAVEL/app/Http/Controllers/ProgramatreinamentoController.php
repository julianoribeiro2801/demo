<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Input;
// use Request;
use Illuminate\Database\QueryException;
use App\Models\Unidade;
use App\Models\UnidadeDados;
use App\Models\Programatreinamento;
use App\Models\Agendaprogramatreinamento;
use App\Models\Agendaalunoprograma;
use App\Models\Processoaluno;
// Precisa para funcionar o combo
use Auth;

//////////////
use App\Services\FilialService;
use App\Services\ProcessoService;
use Illuminate\Support\Facades\Validator;


class ProgramatreinamentoController extends Controller {

    public function __construct(FilialService $filialService,ProcessoService $processoService) {
        $this->filialService = $filialService;
        $this->processoService = $processoService;
    }

    public function changeEmp($id_unidade) {
        $this->filialService->changeEmp($id_unidade);

        return redirect(url()->previous());
    }

    public function getProgramastreinamento() {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        $programas = Programatreinamento::select('id', 'nmprograma')
                ->where('idunidade', $idunidade)
                ->orderby('id', 'desc')->with('agendas')->orderby('id')
                ->get();
        foreach ($programas as $key => $value) {
            $programas[$key]['st']=1;
        }
        
        return response()->json(compact('programas'));
    }

    public function getProgramatreinamento($id) {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        $programa = Programatreinamento::select('id', 'nmprograma')
                ->where('idunidade', $idunidade)
                ->where('id', $id)->with('agendas')
                ->get();


        return response()->json(compact('programa'));
    }

    public function getAgendasProgramatreinamento($id) {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        $agendas = Agendaprogramatreinamento::select('id', 'idunidade', 'nrdias')
                ->where('idunidade', $idunidade)
                ->where('idprogramatreinamento', $id)
                ->get();


        return response()->json(compact('agendas'));
    }

    public function getAgendas($idprograma, $idaluno) {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        $agendas = Agendaalunoprograma::select('id', 'stagenda', 'idunidade', 'idaluno', DB::raw('DATE_FORMAT(dtagenda, "%Y-%m-%d") as dtagenda'), 'hragenda', 'nmagenda', 'idprogramatreinamento')
                ->where('idunidade', $idunidade)
                ->where('idaluno', $idaluno)
                ->where('idprogramatreinamento', $idprograma)
                ->orderBy('id')
                ->get();

        if (sizeof($agendas) <= 0) {
            $agendas = Agendaprogramatreinamento::select('id', 'nmagenda', 'idunidade', 'nrdias', 'idprogramatreinamento')
                    ->where('idunidade', $idunidade)
                    ->where('idprogramatreinamento', $idprograma)
                    ->orderBy('id')
                    ->get();
            $agendas = $agendas->toArray();
            $dtCalc = date('Y-m-d');
            $soma = 0;
            foreach ($agendas as $key => $value) {
                $soma = $soma + $agendas[$key]['nrdias'];
                $agendas[$key]['hragenda'] = date('H:i');
                $agendas[$key]['idaluno'] = $idaluno;
                $agendas[$key]['id'] = 0;
                $dtCalc = date('d/m/Y', strtotime("+" . $soma . " days"));
                $agendas[$key]['dtagenda'] = $dtCalc;
                $agendas[$key]['datas'] = $this->getDataAgendada($this->setData($agendas[$key]['dtagenda']));
                $agendas[$key]['diasemana'] = $this->setDiaSemana($dtCalc);
            }
        } else {
            foreach ($agendas as $key => $value) {
                if ($key == 0) {
                    $k = $key;
                } else {
                    $k = $key - 1;
                }

                if ($key > 0) {
                    $agendas[$key]['key'] = $this->setData($agendas[$k]['dtagenda']);
                } else {
                    $agendas[$key]['key'] = $agendas[$key]['dtagenda'];
                }
                $dateNow = new \DateTime(date('Y-m-d', strtotime($agendas[$key]['dtagenda'])));
                $dateStart = new \DateTime(date('Y-m-d', strtotime($agendas[$key]['key'])));
                $dateDiff = $dateStart->diff($dateNow);
                $agendas[$key]['dateStart'] = $agendas[$key]['key'];
                $agendas[$key]['dateNow'] = $dateNow;
                $agendas[$key]['nrdias'] = $dateDiff->d;
                $agendas[$key]['hragenda'] = date('H:i', strtotime($agendas[$key]['hragenda']));
                $agendas[$key]['dtagenda'] = date('d/m/Y', strtotime($agendas[$key]['dtagenda']));
                $agendas[$key]['diasemana'] = $this->setDiaSemana($agendas[$key]['dtagenda']);
                $agendas[$key]['datas'] = $this->getDataAgendada($this->setData($agendas[$key]['dtagenda']));
            }
        }

        return response()->json(compact('agendas'));
    }

    public function getAgendaData($ind) {
        $dts = Input::all();
        foreach ($dts as $key => $value) {
            if ($key == 0) {
                $k = $key;
            } else {
                $k = $key - 1;
            }
            if ($key > $ind) {
                $dt = setData($dts[$k]['dtagenda']);
                $dts[$key]['dtagenda'] = date('d/m/Y', strtotime("+" . $dts[$key]['nrdias'] . " days", strtotime($dt)));
            }
            $dts[$key]['nrdias'] = $dts[$key]['nrdias'];
            $dts[$key]['datas'] = $this->getDataAgendada($this->setData($dts[$key]['dtagenda']));
            $dts[$key]['diasemana'] = $this->setDiaSemana($dts[$key]['dtagenda']);
        }

        return response()->json(compact('dts'));
    }

    public function getProgramasAluno($idaluno) {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        $programasaluno = DB::select("SELECT distinct(ap.idprogramatreinamento),p.nmprograma"
                        . " FROM agenda_alunoprograma ap left join programatreinamento p"
                        . " on p.id = ap.idprogramatreinamento"
                        . " where ap.idaluno = " . $idaluno
                        . " and ap.idunidade = " . $idunidade  . " order by ap.id asc " );
        
        foreach ($programasaluno as $key => $value) {
            $programasaluno[$key]->nome = 'teste ' ;
        }
        return response()->json(compact('programasaluno'));
    }

    public function validaProgramaAberto($id) {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }

        $abertos = DB::select("SELECT count(*) as abertos"
                        . " FROM agenda_alunoprograma as ap"
                        . " where ap.idaluno = " . $id
                        . " and ap.dtagenda > '" . date('Y-m-d') . "'"
                        . " and stagenda = 'P' and ap.idunidade = " . $idunidade);

        return response()->json(compact('abertos'));
    }

    public function addProgramatreinamento() {
        //if(Request::ajax()) {
        $data = Input::all();
        $validator = Validator::make(Input::all(), [
                    'nmprograma' => 'required|min:3'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors()->getMessages(), 400);
        } else {

            try {

                $idunidade = Auth::user()->idunidade;
                if (session()->get('id_unidade')) {
                    $idunidade = session()->get('id_unidade');
                }
                $dataPrograma['idunidade'] = $idunidade;
                $dataPrograma['nmprograma'] = strtoupper($data['nmprograma']);

                $programa = new Programatreinamento();
                if ($programa->create($dataPrograma)):
                    $retorno['id'] = $programa->idunidade;
                    $retorno['title'] = 'Sucesso!';
                    $retorno['type'] = 'success';
                    $retorno['text'] = 'Programa cadastrado com sucesso!';
                    return $retorno;
                else:
                    $retorno['title'] = 'Erro!';
                    $retorno['type'] = 'error';
                    $retorno['text'] = 'Erro ao cadastrar programa!';
                    return $retorno;
                endif;
            } catch (Illuminate\Database\QueryException $e) {
                return "Não foi possivel salvar";
            }
        }
    }

    public function addAgenda() {
        $data = Input::all();

        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        $dataPrograma['idunidade'] = $idunidade;
        $dataPrograma['idprogramatreinamento'] = $data['idprogramatreinamento'];
        $dataPrograma['nrdias'] = $data['nrdias'];
        $dataPrograma['nmagenda'] = $data['nmagenda'];
        $dataPrograma['st'] = 1;

        $programa = new Agendaprogramatreinamento();
        if ($programa->create($dataPrograma)):
            $retorno['title'] = 'Sucesso!';
            $retorno['type'] = 'success';
            $retorno['text'] = 'Agenda cadastrada com sucesso!';
        else:
            $retorno['title'] = 'Erro!';
            $retorno['type'] = 'error';
            $retorno['text'] = 'Erro ao cadastrar programa!';

        endif;
        return response()->json(compact('retorno'));
    }

    public function upAgenda() {
        $data = Input::all();
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        $dataAgenda['id'] = $data['agenda_id'];
        $dataAgenda['nrdias'] = $data['nrdias'];
        $dataAgenda['nmagenda'] = $data['nmagenda'];

        $agenda = new Agendaprogramatreinamento();
        if ($agenda->where('id', '=', $data['agenda_id'])
                        ->where('idunidade', '=', $idunidade)
                        ->update($dataAgenda)):
            $retorno['title'] = 'Sucesso!';
            $retorno['type'] = 'success';
            $retorno['text'] = 'Empresa atualizada com sucesso!';
        else:
            $retorno['title'] = 'Erro!';
            $retorno['type'] = 'error';
            $retorno['text'] = 'Erro ao atualizar empresa!';
        endif;
        return response()->json(compact('retorno'));
    }

    public function atualizaAgenda($id, $st) {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }

        $dataAgenda['id'] = $id;
        $dataAgenda['stagenda'] = $st;
        $agenda = new Agendaalunoprograma();
        if ($agenda->where('id', '=', $id)
                        ->where('idunidade', '=', $idunidade)
                        ->update($dataAgenda)):
            $retorno['title'] = 'Sucesso!';
            $retorno['type'] = 'success';
            $retorno['text'] = 'Agenda atualizada com sucesso!';
        else:
            $retorno['title'] = 'Erro!';
            $retorno['type'] = 'error';
            $retorno['text'] = 'Erro ao atualizar agenda!';
        endif;
        if ($st=='R'):
            
            
             $ret=$this->processoService->baixaProcesso(0,$idunidade,$id,1);//ok gerando
        
        
             return $ret;


            
        endif;
        
        ////////////////////////////////
        
        
        
        
        return response()->json(compact('retorno'));
    }

    public function validaAgenda() {
        $data = Input::all();

        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        $ret = DB::select("SELECT count(*) as conta FROM agenda_alunoprograma as ap"
                        . " where ap.idaluno = " . $data['aluno']
                        . " and ap.idprogramatreinamento = " . $data['programa']
                        . " and ap.dtagenda < '" . $this->setData($data['dtagenda']) . "'"
                        . " and ap.stagenda = 'P'"
                        . " and ap.idunidade = " . $idunidade);


        return response()->json(compact('ret'));
    }

    public function addAgendaAluno() {
        $data = Input::all();
       /* $validator = Validator::make(Input::all(), [
                    'nmprograma' => 'required|min:3'
        ]);*/
        /*if ($validator->fails()) {
            return response()->json($validator->errors()->getMessages(), 400);
        } else {*/
            $idunidade = Auth::user()->idunidade;
            if (session()->get('id_unidade')) {
                $idunidade = session()->get('id_unidade');
            }
            $retorno = [];
            try {
                for ($i = 0; $i < sizeof($data); $i++) {
                    $dataPrograma['idunidade'] = $idunidade;
                    $dataPrograma['idaluno'] = $data[$i]['idaluno'];
                    $dataPrograma['idprogramatreinamento'] = $data[$i]['idprogramatreinamento'];
                    $dataPrograma['dtagenda'] = $this->setData($data[$i]['dtagenda']);
                    $dataPrograma['hragenda'] = $data[$i]['hragenda'];
                    $dataPrograma['nmagenda'] = $data[$i]['nmagenda'];

                    $programa = new Agendaalunoprograma();  //--> adiciona
                    if ($data[$i]['id'] == 0):
                        $dataPrograma['stagenda'] = 'P';
                        if ($programa->create($dataPrograma)):
                            $retorno['title'] = 'Sucesso!';
                            $retorno['type'] = 'success';
                            $retorno['text'] = 'Agenda cadastrada com sucesso!';
                        else:
                            $retorno['title'] = 'Erro!';
                            $retorno['type'] = 'error';
                            $retorno['text'] = 'Erro ao cadastrar programa!';
                        endif;
                    else:
                        if ($programa->where('id', '=', $data[$i]['id'])->where('idunidade', '=', $idunidade)->update($dataPrograma)):
                           
                            $retorno['title'] = 'Sucesso';
                            $retorno['type'] = 'success';
                            $retorno['text'] = 'Agenda atualizada com sucesso!';
                        else:
                            $retorno['title'] = 'Erro!';
                            $retorno['type'] = 'error';
                            $retorno['text'] = 'Erro ao atualizar empresa!';
                        endif;


                    endif;
                }
                //DB::commitTransaction();
                return $retorno;
            } catch (Illuminate\Database\QueryException $e) {

                // DB::rollbackTransaction();
                return $retorno;
            }
       // }
    }

    public function upProgramatreinamento() {
        //if(Request::ajax()) {
        $data = Input::all();

        $validator = Validator::make(Input::all(), [
                    'nmprograma' => 'required|min:3'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors()->getMessages(), 400);
        } else {

            $idunidade = Auth::user()->idunidade;
            if (session()->get('id_unidade')) {
                $idunidade = session()->get('id_unidade');
            }
            $dataPrograma['idunidade'] = $idunidade;
            $dataPrograma['nmprograma'] = strtoupper($data['nmprograma']);

            $local = new Programatreinamento();
            if ($local->where('id', '=', $data['id'])->update($dataPrograma)):
                $retorno['title'] = 'Sucesso!';
                $retorno['type'] = 'success';
                $retorno['text'] = 'Programa atualizado com sucesso!';
            else:
                $retorno['title'] = 'Erro!';
                $retorno['type'] = 'error';
                $retorno['text'] = 'Erro ao atualizar programa!';
            endif;
            return $retorno;
        }
    }

    // DELETAR PROGRAMA
    public function deleteProgramatreinamento($id) {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }

        $local = new Programatreinamento();
        if ($local->where('id', '=', $id)->where('idunidade', '=', $idunidade)->delete()):
            $retorno['title'] = 'Sucesso!';
            $retorno['type'] = 'success';
            $retorno['text'] = 'Programa excluido com sucesso!';
        else:
            $retorno['title'] = 'Erro!';
            $retorno['type'] = 'error';
            $retorno['text'] = 'Erro ao excluir programa!';
        endif;
        return $retorno;
    }

    public function deleteAgenda($id) {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }

        $local = new Agendaprogramatreinamento();
        if ($local->where('idunidade', '=', $idunidade)->where('id', '=', $id)->delete()):
            $retorno['title'] = 'Sucesso!';
            $retorno['type'] = 'success';
            $retorno['text'] = 'Agenda excluida com sucesso!';
        else:
            $retorno['title'] = 'Erro!';
            $retorno['type'] = 'error';
            $retorno['text'] = 'Erro ao excluir agenda!';
        endif;
        return $retorno;
    }

    public function delProgramaaluno($idprograma, $idaluno) {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }

        $local = new Agendaalunoprograma();
        if ($local->where('idunidade', '=', $idunidade)->where('idprogramatreinamento', '=', $idprograma)->where('idaluno', '=', $idaluno)->delete()):
            $retorno['title'] = 'Sucesso!';
            $retorno['type'] = 'success';
            $retorno['text'] = 'Agenda excluida com sucesso!';
        else:
            $retorno['title'] = 'Erro!';
            $retorno['type'] = 'error';
            $retorno['text'] = 'Erro ao excluir agenda!';
        endif;
        return $retorno;
    }

    public function getDataAgendada($Data) {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }

        $datas = Agendaalunoprograma::select('agenda_alunoprograma.idaluno', 'agenda_alunoprograma.dtagenda', 'agenda_alunoprograma.hragenda', 'users.name')
                ->leftjoin('users', 'agenda_alunoprograma.idaluno', '=', 'users.id')
                ->where('agenda_alunoprograma.idunidade', $idunidade)
                ->where('agenda_alunoprograma.dtagenda', $Data)
                ->orderBy('agenda_alunoprograma.hragenda')
                ->get();

        foreach ($datas as $key => $value) {
            $datas[$key]['hragenda'] = date('H:i', strtotime($datas[$key]['hragenda']));
        }

        return $datas;
    }

    public function setData($Data) {
        $Format = explode(' ', $Data);
        $Data = explode('/', $Format[0]);
        $Data = $Data[2] . '-' . $Data[1] . '-' . $Data[0];
        return $Data;
    }

    public function setDiaSemana($datas) {
        $ano = substr("$datas", 6, 4);
        $mes = substr("$datas", 3, 2);
        $dia = substr("$datas", 0, 2);

        $diasemana = date("w", mktime(0, 0, 0, $mes, $dia, $ano));

        switch ($diasemana) {
            case"0": $diasemana = "Domingo";
                break;
            case"1": $diasemana = "Segunda-feira";
                break;
            case"2": $diasemana = "Terça-feira";
                break;
            case"3": $diasemana = "Quarta-feira";
                break;
            case"4": $diasemana = "Quinta-feira";
                break;
            case"5": $diasemana = "Sexta-feira";
                break;
            case"6": $diasemana = "Sábado";
                break;
        }

        return "$diasemana";
    }

    public function getData($Data) {
        if ($Data != null):
            $data = date("d/m/Y", strtotime($Data));
            return $data;
        endif;
    }

}
