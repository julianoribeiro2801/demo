<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pessoa;
use App\Models\User;
use App\Models\Cidade;
use App\Models\UserDados;
use Illuminate\Support\Facades\DB;
use Hash;
use App\Http\Requests\PessoaRequest;

class PessoaController extends Controller
{
    public function index()
    {
        $headers = ['category' => 'Empresa', 'title' => 'Pessoas'];
        $pessoas = Pessoa::all();
        return view('admin.pessoas.index', compact('pessoas', 'headers'));
    }

    public function edit($id)
    {
        $headers = ['category' => 'Empresa', 'title' => 'Pessoas'];
        $pessoa = User::where('id', $id)->first();
        return view('admin.pessoas.edit', compact('pessoa', 'headers'));
    }

    public function create()
    {
        $headers = ['category' => 'Empresa', 'title' => 'Pessoas'];
        return view('admin.pessoas.create', compact('headers'));
    }

    public function update(PessoaRequest $request, $id)
    {
        $data = $request->all();

        Pessoa::where('id', $id)->update(['nmpessoa' => $data['nmpessoa'],
        'dtnascimento' => date('Y-m-d', strtotime($data['dtnascimento'])),
        'idunidade' => 1

       ]);
     
        return redirect()->route('admin.empresa.pessoas.index');
    }

    public function lista($tipo)
    {
        return User::where('role', $tipo)->get();
    }

    public function remove($id)
    {
        DB::beginTransaction();
          
        try {
            User::where('id', $id)->delete();
            UserDados::where('userid', $id)->delete();

            DB::commit();
            return "Removido com sucesso";
        } catch (Exception $e) {
            DB::rollback();
            return "Não foi possivel remover";
        }
    }

    public function store(PessoaRequest $request)
    {
        $data = $request->all();
      
        $verificaemail = User::where('email', $data['email'])->first();
        if (count($verificaemail)>0) {
            return "Email já existe";
        } else {
            DB::beginTransaction();
          
            try {
                $cliente = User::create(['name' => $data['name'],
                'idunidade' => 1,
                'email' => $data['email'],
                'password' => Hash::make($data['senha']),
                'role' => $data['tipo']
              ]);
              
                $idcidade = Cidade::select('id')->where('nome', $data['cidade'])->first();
         
                UserDados::create(['user_id' => $cliente->id,
                'idunidade' => 1,
                'telefone' => $data['telefone'],
                'idcidade' => $idcidade->id,
                'endereco' => $data['endereco'],
                'numero' => $data['numero'],
                'bairro' => $data['bairro'],
                'dt_nascimento' => $data['dt_nascimento'],
                'cpf' => $data['cpf']
              ]);
              
                DB::commit();
                return "Salvo com sucesso";
            } catch (Exception $e) {
                DB::rollback();
                return "Não foi possivel salvar";
            }
        }
    }


    // public function create_user(){

          //  User::create( [
    //          'email' => 'kk@msn.com' ,
    //          'password' => Hash::make( '123456' ) ,
    //          'name' => 'KK Ruiz' ,
    //          'role' => 'admin'
    //         ] );
    // }
}
