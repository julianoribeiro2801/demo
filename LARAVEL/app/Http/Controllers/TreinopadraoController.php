<?php

namespace App\Http\Controllers;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Input;
//use Request;
use File;
use App\Models\User;
use App\Models\UserDados;
use App\Models\Cidade;
use App\Models\Funcionario;
use App\Models\Objetivotreino;
use App\Models\Prescricao;
use App\Models\Exercicio;
use App\Models\Grupomuscular;
use App\Models\TreinoMusculacaopadrao;
use App\Models\PrescricaoCalendarioPadrao;
use App\Models\PrescricaoModalidadePadrao;
use App\Models\Historicotreino;
use App\Models\TreinoFichapadrao;
use App\Models\Nutricao;
use App\Models\Nutricaopadrao;
use App\Models\TreinoNivel;
use App\Models\Nivelhabilidade;
use App\Models\TempoEstimadopadrao;
use App\Services\TrataDadosService;
// Precisa para funcionar o comboF
use Auth;
use App\Services\FilialService;

class TreinopadraoController extends Controller {

    public function __construct(TrataDadosService $trataDadosService, FilialService $filialService) {
        $this->middleware('auth');

        $this->trataDadosService = $trataDadosService;
        $this->filialService = $filialService;
    }

    public function treinopadrao() {
        $headers = ['category' => 'Configurações', 'title' => 'Treino Padrão'];
        $alunos = User::select('id', 'name')->where('role', 'cliente')->get();
        $professores = Funcionario::select('id', 'name')->where('role', 'funcionario')->orderby('name')->get();
        $objetivos = Objetivotreino::select('id', 'nmobjetivo')->orderby('nmobjetivo')->get();
        $treinos = Prescricao::select('id', 'idaluno', 'dtinicio')->orderby('dtinicio')->get();
        $unidades_combo = $this->filialService->unidadesComboTop();

        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }

        return view('prescricao.padrao', compact('headers', 'alunos', 'professores', 'objetivos', 'treinos', 'unidades_combo', 'idunidade'));
    }

    public function addTreinoModalidadePadrao() {
        
        $data = Input::all();
        $idprofessor = Auth::id();
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }

        $treino = PrescricaoModalidadePadrao::create([
                    'idunidade' => $idunidade,
                    'idobjetivo' => $data['idobjetivo'],
                    'idnivel' => $data['idnivel'],
                    'nome_treino' => $data['nome_treino'],
                    'tipo_prescricao' => $data['tipo_prescricao']
            
        ]);
        $treinos = $treino->id;

        return response()->json(compact('treinos'));        
        
    }
    public function upTreinoModalidadePadrao() {
        
        $data = Input::all();
        $idprofessor = Auth::id();
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }

        $treino = PrescricaoModalidadePadrao::where('id', $data['id_treino'])->where('idunidade', $idunidade)->update([
            'idunidade' => $idunidade,
            'treino_sexo' => $data['sexo_treino'],
            'idobjetivo' => $data['objetivo_treino'],
            'idnivel' => $data['nivel_treino'],
            'nome_treino' => $data['nome_treino']
        ]);

        return response()->json(compact('data'));        
        
    }    
    
    // CRIA NOVO TREINO
    
    
    public function addTreinoPadrao() {
        $data = Input::all();
        $idprofessor = Auth::id();
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }

        $treino = TreinoMusculacaopadrao::create([
                    'unidade_id' => $idunidade,
                    'treino_nome_padrao' => $data['treino_nome_padrao'],
                    'treino_qtddias' => $data['treino_qtddias'],
                    'treino_nivel' => $data['treino_nivel'],
                    'treino_objetivo' => $data['objetivo_id'],
                    'treino_observacao' => $data['treino_observacao'],
                    'professor_id' => $idprofessor,
                    'treino_sexo' => $data['treino_sexo']
        ]);
        $treinos = $treino->id;

        return response()->json(compact('treinos'));
    }
    // ATUALIZA TREINO
    public function upTreinoPadrao() {
        $data = Input::all();

        $idprofessor = Auth::id();

        $treino_id = $data['treino_id'];

        TreinoMusculacaopadrao::where('treino_id', $treino_id)->update([
            'treino_nome_padrao' => ucwords($data['treino_nome_padrao']),
            'treino_qtddias' => $data['treino_qtddias'],
            'treino_nivel' => $data['treino_nivel'],
            'treino_objetivo' => $data['objetivo_id'],
            'treino_observacao' => $data['treino_observacao'],
            'professor_id' => $idprofessor,
            'treino_sexo' => $data['treino_sexo']
        ]);
        return 'Alterado com sucesso!';
    }


    public function getFichaPadraoCross($treino_id) {
        $data = Input::all();
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }

        $calendario = PrescricaoCalendarioPadrao::select('prescricao_calendario_padrao.*')
                ->where('idtreino', $treino_id)
                ->where('idunidade', $idunidade)
                ->orderby('id')
                ->get();

      /*  foreach ($calendario as $key => $value) {
            $calendario[$key]['seq'] = $key;
            $treinofichas[$key]['superserie'] = TreinoFichapadrao::select('grupomuscular.nmgrupo as grupo', 'medida_duracao', 'medida_intensidade', 'ficha_id_vinculo', 'super_serie', 'ficha_id AS ficha', 'ord AS ordem', 'ficha_letra AS letra', 'ficha_intervalo AS intervalo', 'ficha_series AS series', 'ficha_repeticoes AS repeticoes', 'ficha_carga AS carga', 'ficha_observacao AS observacao', 'exercicio.nmexercicio as nmexercicio', DB::raw('ifnull(exercicio_id,0) as exercicio'))
                    ->leftJoin('exercicio', 'treino_ficha_padrao.exercicio_id', '=', 'exercicio.id')
                    ->leftJoin('grupomuscular', 'exercicio.idgrupomuscular', '=', 'grupomuscular.id')
                    ->where('treino_id', $data['treino_id'])
                    ->where('ficha_id_vinculo', $treinofichas[$key]['ficha'])
                    ->orderby('ficha_letra')
                    ->orderby('ord')
                    ->get();
            foreach ($treinofichas[$key]['superserie'] as $key1 => $value) {
                $treinofichas[$key]['superserie'][$key1]['id'] = $key1 + 1;
            }
        }*/


        $treinopadrao = PrescricaoModalidadePadrao::select('prescricao_modalidade_padrao.*','nivelhabilidade.nmnivel')
                        ->leftJoin('nivelhabilidade', 'prescricao_modalidade_padrao.idnivel', '=', 'nivelhabilidade.id')
                        ->where('prescricao_modalidade_padrao.idunidade', $idunidade)
                        ->where('prescricao_modalidade_padrao.id', $treino_id)->get();


        return response()->json(compact('calendario', 'treinopadrao', 'ret'));
    }    
    
    public function getPadrao($sexo) {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        
        $treinospadrao = TreinoMusculacaopadrao::select('treino_objetivo', 'objetivotreino.nmobjetivo')
                        ->leftJoin('objetivotreino', 'treino_musculacao_padrao.treino_objetivo', '=', 'objetivotreino.id')
                        ->where('treino_objetivo', '>', 0)
                        ->where('unidade_id', $idunidade)
                        ->where('treino_sexo', $sexo)
                        ->groupBy('treino_objetivo', 'objetivotreino.nmobjetivo')
                        ->distinct()->get();

        foreach ($treinospadrao as $key => $value) {
            $treinospadrao[$key]['niveis'] = TreinoMusculacaopadrao::select('treino_nivel', 'nivelhabilidade.nmnivel')
                    ->leftJoin('nivelhabilidade', 'treino_musculacao_padrao.treino_nivel', '=', 'nivelhabilidade.id')
                    ->where('treino_objetivo', '=', $treinospadrao[$key]['treino_objetivo'])
                    ->where('treino_nivel', '>', 0)
                    ->where('unidade_id', $idunidade)
                    ->where('treino_sexo', $sexo)
                    ->groupBy('treino_nivel', 'nivelhabilidade.nmnivel')
                    ->get();
        }
        foreach ($treinospadrao as $key => $value) {
            $niveis = $treinospadrao[$key]['niveis'];
            foreach ($niveis as $key1 => $value) {
                $niveis[$key1]['treinos'] = TreinoMusculacaopadrao::select('treino_id', 'treino_sexo', 'treino_nome_padrao')
                        ->where('treino_objetivo', '=', $treinospadrao[$key]['treino_objetivo'])
                        ->where('treino_nivel', '=', $niveis[$key1]->treino_nivel)
                        ->where('unidade_id', $idunidade)
                        ->where('treino_sexo', $sexo)
                        ->orderBy('treino_nome_padrao')
                        ->get();
            }
        }
        return response()->json(compact('treinospadrao'));
    }

    public function getPadraoModalidade($sexo,$modalidade) {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        $treinospadrao = PrescricaoModalidadePadrao::select('idobjetivo', 'objetivotreino.nmobjetivo')
                        ->leftJoin('objetivotreino', 'prescricao_modalidade_padrao.idobjetivo', '=', 'objetivotreino.id')
                        ->where('idobjetivo', '>', 0)
                        ->where('prescricao_modalidade_padrao.idunidade', $idunidade)
                        ->where('treino_sexo', $sexo)
                        ->where('prescricao_modalidade_padrao.tipo_prescricao', $modalidade)
                        ->groupBy('idobjetivo', 'objetivotreino.nmobjetivo')
                        ->distinct()->get();

        foreach ($treinospadrao as $key => $value) {
            $treinospadrao[$key]['niveis'] = PrescricaoModalidadePadrao::select('idnivel', 'nivelhabilidade.nmnivel')
                    ->leftJoin('nivelhabilidade', 'prescricao_modalidade_padrao.idnivel', '=', 'nivelhabilidade.id')
                    ->where('prescricao_modalidade_padrao.idobjetivo', '=', $treinospadrao[$key]['idobjetivo'])
                    ->where('prescricao_modalidade_padrao.idnivel', '>', 0)
                    ->where('prescricao_modalidade_padrao.idunidade', $idunidade)
                    ->where('prescricao_modalidade_padrao.treino_sexo', $sexo)
                    ->where('prescricao_modalidade_padrao.tipo_prescricao', $modalidade)
                    ->groupBy('prescricao_modalidade_padrao.idnivel', 'nivelhabilidade.nmnivel')
                    ->get();
        }
        foreach ($treinospadrao as $key => $value) {
            $niveis = $treinospadrao[$key]['niveis'];
            foreach ($niveis as $key1 => $value) {
                $niveis[$key1]['treinos'] = PrescricaoModalidadePadrao::select('id', 'treino_sexo', 'nome_treino')
                        ->where('idobjetivo', '=', $treinospadrao[$key]['idobjetivo'])
                        ->where('idnivel', '=', $niveis[$key1]->idnivel)
                        ->where('idunidade', $idunidade)
                        ->where('treino_sexo', $sexo)
                        ->where('tipo_prescricao', $modalidade)
                        ->orderBy('nome_treino')
                        ->get();
            }
        }
        return response()->json(compact('treinospadrao'));
    }    
    // ATUALIZA ordem
    public function atualizaOrdemInsert(Request $request, $treino_id, $aba) {
        $ficha = "";
        $exercicio = "";
        $series = "";
        $repeticoes = "";
        $carga = "";
        $intervalo = "";
        foreach ($request->ficha as $key => $value) {
            $ficha[$key]['id'] = $value;
        }
        foreach ($request->exercicio as $key => $value) {
            $exercicio[$key]['id'] = $value;
        }
        foreach ($request->series as $key => $value) {
            $series[$key]['id'] = $value;
        }
        foreach ($request->repeticoes as $key => $value) {
            $repeticoes[$key]['id'] = $value;
        }
        foreach ($request->carga as $key => $value) {
            $carga[$key]['id'] = $value;
        }
        foreach ($request->intervalo as $key => $value) {
            $intervalo[$key]['id'] = $value;
        }

        if (sizeof($ficha) > 0) {
            foreach ($ficha as $key => $value) {
                TreinoFichapadrao::where('ficha_id', $ficha[$key]['id'])
                        ->where('treino_id', $treino_id)
                        ->where('ficha_letra', $aba)
                        ->update([
                            'ord' => $key + 1,
                            'ficha_letra' => $aba,
                            'ficha_intervalo' => $intervalo[$key]['id'],
                            'ficha_series' => $series[$key]['id'],
                            'ficha_repeticoes' => $repeticoes[$key]['id'],
                            'treino_id' => $treino_id,
                            'exercicio_id' => $exercicio[$key]['id']
                ]);
            }
        }


        return $treino_id;
    }

    public function atualizaOrdem($treino_id, $aba) {
        $dataAll = Input::all();
        for ($i = 0; $i < sizeof($dataAll); $i++) {
            TreinoFichapadrao::where('ficha_id', $dataAll[$i])
                    ->where('treino_id', $treino_id)
                    ->where('ficha_letra', $aba)
                    ->update([
                        'ord' => $i + 1
            ]);
        }
        return $dataAll;
    }

    public function addExercicioTreinoPadrao($treino_id) {

        $dataAll = Input::all();

        for ($i = 0; $i < sizeof($dataAll); $i++) {

            $ficha = isset($dataAll[$i]['ficha']) ? $dataAll[$i]['ficha'] : '';
            if ($ficha != '') {

                $medida_duracao = 'rep';
                $medida_intensidade = 'kg';
                $super_serie = 1;
                $observacao = '';
                if (isset($dataAll[$i]['medida_duracao'])) {
                    $medida_duracao = $dataAll[$i]['medida_duracao'];
                }
                if (isset($dataAll[$i]['medida_intensidade'])) {
                    $medida_intensidade = $dataAll[$i]['medida_intensidade'];
                }
                if (isset($dataAll[$i]['super_serie'])) {
                    $super_serie = $dataAll[$i]['super_serie'];
                }
                if (isset($dataAll[$i]['observacao'])) {
                    $observacao = $dataAll[$i]['observacao'];
                }
                $intervalo = $dataAll[$i]['intervalo'];
                if (isset($dataAll[$i]['super_serie'])) {
                    if ($dataAll[$i]['super_serie'] > 1) {
                        $intervalo = 0;
                    }
                }
                $id = $dataAll[$i]['ficha'];

                TreinoFichapadrao::where('ficha_id', $id)->update([
                    'ficha_letra' => $dataAll[$i]['letra'],
                    'ficha_intervalo' => $intervalo,
                    'ficha_series' => $dataAll[$i]['series'],
                    'ficha_repeticoes' => $dataAll[$i]['repeticoes'],
                    'medida_duracao' => $medida_duracao,
                    'medida_intensidade' => $medida_intensidade,
                    'super_serie' => $super_serie,
                    'ficha_carga' => str_replace('kg', '', $dataAll[$i]['carga']),
                    'ficha_observacao' => $observacao,
                    'ord' => $i + 1,
                    'treino_id' => $treino_id,
                    'exercicio_id' => $dataAll[$i]['exercicio']
                ]);
                ///atualiza superserie
                if (isset($dataAll[$i]['superserie'])):
                    for ($k = 0; $k < sizeof($dataAll[$i]['superserie']); $k++) {


                        $ficha_super = isset($dataAll[$i]['superserie'][$k]['ficha']) ? $dataAll[$i]['superserie'][$k]['ficha'] : '';
                        $observacao_superserie = '';
                        if (isset($dataAll[$i]['superserie'][$k]['observacao'])) {
                            $observacao_superserie = $dataAll[$i]['superserie'][$k]['observacao'];
                        }                        

                        if ($ficha_super != '') {
                            TreinoFichapadrao::where('ficha_id', $dataAll[$i]['superserie'][$k]['ficha'])->update([
                                'ficha_letra' => $dataAll[$i]['superserie'][$k]['letra'],
                                'ficha_intervalo' => $dataAll[$i]['superserie'][$k]['intervalo'],
                                'ficha_series' => $dataAll[$i]['superserie'][$k]['series'],
                                'ficha_repeticoes' => $dataAll[$i]['superserie'][$k]['repeticoes'],
                                'medida_duracao' => $medida_duracao,
                                'medida_intensidade' => $medida_intensidade,
                                'super_serie' => $super_serie,
                                'ficha_carga' => str_replace('kg', '', isset($dataAll[$i]['superserie'][$k]['carga']) ? $dataAll[$i]['superserie'][$k]['carga'] : '0'),
                                'ficha_observacao' => $observacao_superserie,
                                'treino_id' => $treino_id,
                                'ord' => 0,
                                'exercicio_id' => $dataAll[$i]['superserie'][$k]['exercicio']
                            ]);
                        } else {
                            TreinoFichapadrao::create([
                                'ficha_letra' => $dataAll[$i]['superserie'][$k]['letra'],
                                'ficha_intervalo' => $dataAll[$i]['superserie'][$k]['intervalo'],
                                'ficha_series' => $dataAll[$i]['superserie'][$k]['series'],
                                'ficha_repeticoes' => $dataAll[$i]['superserie'][$k]['repeticoes'],
                                'medida_duracao' => $medida_duracao,
                                'medida_intensidade' => $medida_intensidade,
                                'super_serie' => $super_serie,
                                'ficha_carga' => str_replace('kg', '', isset($dataAll[$i]['superserie'][$k]['carga']) ? $dataAll[$i]['superserie'][$k]['carga'] : '0'),
                                'ficha_observacao' => $observacao,
                                'treino_id' => $treino_id,
                                'ficha_id_vinculo' => $id,
                                'ord' => 0,
                                'exercicio_id' => $dataAll[$i]['superserie'][$k]['exercicio']
                            ]);
                        }
                    }
                endif;
            } else {
                $medida_duracao = 'rep';
                $medida_intensidade = 'kg';
                $super_serie = 1;
                $observacao = '';
                if (isset($dataAll[$i]['medida_duracao'])) {
                    $medida_duracao = $dataAll[$i]['medida_duracao'];
                }
                if (isset($dataAll[$i]['medida_intensidade'])) {
                    $medida_intensidade = $dataAll[$i]['medida_intensidade'];
                }
                if (isset($dataAll[$i]['super_serie'])) {
                    $super_serie = $dataAll[$i]['super_serie'];
                }
                if (isset($dataAll[$i]['observacao'])) {
                    $observacao = $dataAll[$i]['observacao'];
                }
                $intervalo = $dataAll[$i]['intervalo'];
                if (isset($dataAll[$i]['super_serie'])) {
                    if ($dataAll[$i]['super_serie'] > 1) {
                        $intervalo = null;
                    }
                }
//                
                $fch = TreinoFichapadrao::create([
                            'ficha_letra' => $dataAll[$i]['letra'],
                            'ficha_intervalo' => $intervalo,
                            'ficha_series' => $dataAll[$i]['series'],
                            'ficha_repeticoes' => $dataAll[$i]['repeticoes'],
                            'medida_duracao' => $medida_duracao,
                            'medida_intensidade' => $medida_intensidade,
                            'super_serie' => $super_serie,
                            'ficha_carga' => str_replace('kg', '', $dataAll[$i]['carga']),
                            'ficha_observacao' => $observacao,
                            'treino_id' => $treino_id,
                            'ord' => $i + 1,
                            'exercicio_id' => $dataAll[$i]['exercicio']
                ]);
                if (isset($dataAll[$i]['superserie'])):
                    for ($k = 0; $k < sizeof($dataAll[$i]['superserie']); $k++) {
                        
                        $observacao_superserie = '';
                        if (isset($dataAll[$i]['superserie'][$k]['observacao'])) {
                            $observacao_superserie = $dataAll[$i]['superserie'][$k]['observacao'];
                        }                                 
                        
                        TreinoFichapadrao::create([
                            'ficha_letra' => $dataAll[$i]['superserie'][$k]['letra'],
                            'ficha_intervalo' => $dataAll[$i]['superserie'][$k]['intervalo'],
                            'ficha_series' => $dataAll[$i]['superserie'][$k]['series'],
                            'ficha_repeticoes' => $dataAll[$i]['superserie'][$k]['repeticoes'],
                            'medida_duracao' => $medida_duracao,
                            'medida_intensidade' => $medida_intensidade,
                            'super_serie' => $super_serie,
                            'ficha_carga' => str_replace('kg', '', isset($dataAll[$i]['superserie'][$k]['carga']) ? $dataAll[$i]['superserie'][$k]['carga'] : '0'),
                            'ficha_observacao' => $observacao_superserie,
                            'treino_id' => $treino_id,
                            'ficha_id_vinculo' => $fch->id,
                            'ord' => 0,
                            'exercicio_id' => $dataAll[$i]['superserie'][$k]['exercicio']
                        ]);
                    }
                endif;
            }
        }
        return $dataAll;
    }

    public function saveTempoEstimadoPadrao() {
        $data = Input::all();
        $data['treino_id'] = $data['treino_id'];
        $data['letra'] = $data['letra'];
        $data['tempoestimado'] = $data['tempoestimado'];
        $data['tempomusculacao'] = $data['tempomusculacao'];

        $tempoEstimado = TempoEstimadopadrao::where('treino_id', $data['treino_id'])->where('letra', $data['letra'])->get();
        $contTempoEstimado = $tempoEstimado->count();
        if ($contTempoEstimado > 0):
            $id = $tempoEstimado[0]['id'];
            TempoEstimadopadrao::where('id', $id)->update([
                'tempoestimado' => $data['tempoestimado'],
                'tempomusculacao' => $data['tempomusculacao']
            ]);
            return 'Alterado com sucesso!';
        else:
            TempoEstimadopadrao::create([
                'treino_id' => $data['treino_id'],
                'letra' => $data['letra'],
                'tempoestimado' => $data['tempoestimado'],
                'tempomusculacao' => $data['tempomusculacao']
            ]);
            return 'Criado com sucesso!';
        endif;
    }

    public function delTreinoPadrao($treino_id) {
        TreinoFichapadrao::where('treino_id', $treino_id)->delete();
        TreinoMusculacaopadrao::where('treino_id', $treino_id)->delete();
    }

    public function delExercicioTreinoPadrao($ficha_id) {
        TreinoFichapadrao::where('ficha_id', $ficha_id)->delete();
    }

    public function delExercicioSuperSeriePadrao($ficha_id, $treino_id, $ficha_super, $super_serie) {

        TreinoFichapadrao::where('ficha_id', $ficha_super)->where('treino_id', $treino_id)
                ->update([
                    'super_serie' => $super_serie
        ]);

        TreinoFichapadrao::where('ficha_id_vinculo', $ficha_super)->where('treino_id', $treino_id)->where('ficha_id', $ficha_id)->delete();
    }

    public function getCaledarioPadrao() {
        //if(Request::ajax()) {
        $data = Input::all();
        $data['mes'] = str_pad($data['mes'], 2, "0", STR_PAD_LEFT);
        return $this->MostreCalendario($data['mes'], $data['ano'], $data['tipo'], $data['treino_id']);
        //}
    }

        // CALENDARIO
    public function MostreSemanas() {
        $semanas = array(
            'Domingo',
            'Segunda-Feira',
            'Terça-Feira',
            'Quarta-Feira',
            'Quinta-Feira',
            'Sexta-Feira',
            'Sábado'
        );
        for ($i = 0; $i < 7; $i++) {
            echo "<th>" . $semanas[$i] . "</th>";
        }
    }

    public function MostreCalendario($mes, $ano, $tipo, $treino_id) {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }


        $numero_dias = $this->GetNumeroDias($mes); // retorna o número de dias que tem o mês desejado
        $nome_mes = $this->GetNomeMes($mes);
        $diacorrente = 0;
        $diasemana = jddayofweek(cal_to_jd(CAL_GREGORIAN, $mes, "01", $ano), 0); // função que descobre o dia da semana
        // busca desc_prescricao
        $prescicoesAluno = PrescricaoCalendarioPadrao::where('tipo_prescricao', $tipo)->where('idunidade', $idunidade)->where('idtreino', $treino_id)->whereMonth('data_prescricao', $mes)->whereYear('data_prescricao', $ano)->get();
        $arrayDesc = [];
        foreach ($prescicoesAluno as $prescicaoAluno) {
            $arrayDesc += array(substr($prescicaoAluno['data_prescricao'], 8, 2) => $prescicaoAluno['desc_prescricao']);
        }
        echo "<h2 class='pull-left'>" . $nome_mes . " de " . $ano . "</h2><input type='hidden' id='mesatual' value='" . $mes . "'><input type='hidden' id='anoatual' value='" . $ano . "'>";
        echo "<table class='table table-bordered'>";
        echo "<thead>";
        echo "<tr>";
        $this->MostreSemanas(); // função que mostra as semanas aqui
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";
        for ($linha = 0; $linha < 6; $linha++) {
            echo "<tr>";
            for ($coluna = 0; $coluna < 7; $coluna++) {
                if (($diacorrente == (date('d') - 1) && date('m') == $mes)) {
                    echo "<td ";
                    echo "id='dia_atual'";
                    echo ">";
                } else {
                    if (($diacorrente + 1) <= $numero_dias) {
                        if ($coluna < $diasemana && $linha == 0) {
                            echo "<td ";
                            echo "id='dia_branco'";
                            echo ">";
                        } else {
                            echo "<td ";
                            echo "id='dia_comum'";
                            echo ">";
                        }
                    }
                }
                if ($diacorrente + 1 <= $numero_dias) {
                    if ($coluna < $diasemana && $linha == 0) {
                        echo " ";
                        echo "</td>";
                    } else {
                        echo "<div class='pull-right numero_calendar'>" . ++$diacorrente . "</div>";
                        echo "<textarea class='form-control calendar_desc' onblur='gravaCalendarioPadrao(" . $diacorrente . ", this.value,nome_treino_cross.value, \"$tipo\" , treino_id_pad.value)' placeholder='Off'>";
                        $diacorrente = str_pad($diacorrente, 2, "0", STR_PAD_LEFT);
                        echo isset($arrayDesc[$diacorrente]) ? $arrayDesc[$diacorrente] : '';
                        echo "</textarea>";
                        echo "</td>";
                    }
                } else {
                    break;
                }
            }
            echo "</tr>";
        }
        echo "<tbody>";
        echo "</table>";
        
    }
    
    public function GetNumeroDias($mes) {
        $numero_dias = array(
            '01' => 31, '02' => 28, '03' => 31, '04' => 30, '05' => 31, '06' => 30,
            '07' => 31, '08' => 31, '09' => 30, '10' => 31, '11' => 30, '12' => 31
        );
        if (((date('Y') % 4) == 0 and ( date('Y') % 100) != 0) or ( date('Y') % 400) == 0) {
            $numero_dias['02'] = 29; // altera o numero de dias de fevereiro se o ano for bissexto
        }
        return $numero_dias[$mes];
    }

    public function GetNomeMes($mes) {
        $meses = array('01' => "Janeiro", '02' => "Fevereiro", '03' => "Março",
            '04' => "Abril", '05' => "Maio", '06' => "Junho",
            '07' => "Julho", '08' => "Agosto", '09' => "Setembro",
            '10' => "Outubro", '11' => "Novembro", '12' => "Dezembro"
        );

        if ($mes >= 01 && $mes <= 12) {
            return $meses[$mes];
        } else {
            return "Mês deconhecido";
        }
    }
    
    public function setData($Data) {
        $Format = explode(' ', $Data);
        $Data = explode('/', $Format[0]);
        $Data = $Data[2] . '-' . $Data[1] . '-' . $Data[0];
        return $Data;
    }    
    public function gravaCalendarioPadrao() {
        //if(Request::ajax()) {
        $data = Input::all();
        $idprofessor = Auth::id();

        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }

        
        $data['treino_id'] = $data['id_treino'];
        $data['desc_prescricao'] = $data['desc'];
        $data['nome_treino'] = $data['nome_treino'];
        $data['data_prescricao'] = $this->setData($data['dataatual']);
        $data['tipo_prescricao'] = $data['tipo'];

        // alterar os campos idunidade, user_id_aluno e user_id_prof pra pegar dinamicamente
        $prescicaoAluno = PrescricaoCalendarioPadrao::where('tipo_prescricao', $data['tipo'])->where('data_prescricao', $data['data_prescricao'])->get();
        $contPrescricao = $prescicaoAluno->count();
        if ($contPrescricao > 0):
            $id = $prescicaoAluno[0]['id'];
            PrescricaoCalendarioPadrao::where('id', $id)->update([
                'idunidade' => $idunidade,
                'idtreino' => $data['treino_id'],
                'user_id_prof' => $idprofessor,
                'desc_prescricao' => $data['desc_prescricao'],
                'nome_treino' => $data['nome_treino'],
                'data_prescricao' => $data['data_prescricao'],
                'tipo_prescricao' => $data['tipo_prescricao']
            ]);
            return 'Alterado com sucesso!';
        else:
            if (sizeof($data['desc_prescricao'])> 0){
                PrescricaoCalendarioPadrao::create([
                    'idunidade' => $idunidade,
                    'idtreino' => $data['treino_id'],                
                    'user_id_prof' => $idprofessor,
                    'desc_prescricao' => $data['desc_prescricao'],
                    'nome_treino' => $data['nome_treino'],
                    'data_prescricao' => $data['data_prescricao'],
                    'tipo_prescricao' => $data['tipo_prescricao']
                ]);
            }    
            return 'Criado com sucesso!';
        endif;
        //}
    }
    
    public function getData($Data)
{
    if ($Data != '0000-00-00 00:00:00'):
        $data = date("d/m/Y", strtotime($Data));
    return $data;
    endif;
}

    // BUSCA NUTRIÇÕES
    public function getNutricoesPadrao() {
        $data = Input::all();
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        $nutricoesAluno = Nutricao::select('id', 'nutricao_nome')
                ->where('unidade_id', $idunidade)
                ->where('aluno_id', $data['aluno_id'])
                ->get();
        return $nutricoesAluno;
    }

    // BUSCA NUTRIÇÕES
    public function getNutricaoPadrao(Request $request) {

        
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }

        $nutricaoAluno = Nutricao::select('id', 'nutricao_descricao', 'nutricao_datainicio', 'nutricao_datatermino', 'nutricao_nivel', 'nutricao_padrao')
                ->where('unidade_id', $idunidade)
                ->where('id', $request->nutricao_id)
                ->first();
      
        $nutricaoAluno = [
            'id' => $nutricaoAluno->id,
            'nutricao_descricao' => $nutricaoAluno->nutricao_descricao,
            'nutricao_datainicio' => ($nutricaoAluno->nutricao_datainicio != '0000-00-00') ? $this->getData($nutricaoAluno->nutricao_datainicio) : '',
            'nutricao_datatermino' => ($nutricaoAluno->nutricao_datatermino != '0000-00-00') ? $this->getData($nutricaoAluno->nutricao_datatermino) : '',
            'nutricao_nivel' => $nutricaoAluno->nutricao_nivel,
            'nutricao_padrao' => $nutricaoAluno->nutricao_padrao
        ];

        return $nutricaoAluno;
    }

    // CRIA NOVA NUTRICAO
    public function addNutricaoPadrao() {
        $data = Input::all();
        $idprofessor = Auth::id();
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }

        // FORMA NOME DA NUTRICAO
        //$nutricaoAluno = Nutricao::where('unidade_id', $data['aluno_id'])->get();
        /*$contNutricaoAluno = $nutricaoAluno->count() + 1;*/
        $nutricao_nome = 'Nutrição Padrão ' . date("d/m/Y");

        // SALVA NUTRICAO E RETORNO O ID DA NUTRICAO
        $nutricao = Nutricao::create([
                    'nutricao_nome' => $nutricao_nome,
                    'professor_id' => $idprofessor,
                    'unidade_id' => $idunidade,
        ]);
        

        /*$nutricaoAluno = [
            'id' => $nutricao_id,
            'nutricao_nome' => $nutricao_nome
        ];*/

        return $nutricao;
    }

    // ATUALIZA NUTRICAO
    public function upNutricaoPadrao() {

        // return Input::all();

        $data = Input::all();

        $nutricao_id = $data['nutricao_id'];
        $data['nutricao_padrao'] = ($data['nutricao_padrao'] == true) ? 'S' : 'N';
        
        Nutricao::where('id', $nutricao_id)->update([
            'nutricao_descricao' => $data['nutricao_descricao'],
            'nutricao_datainicio' => (isset($data['nutricao_datainicio']) && $data['nutricao_datainicio'] != '') ? $this->setData($data['nutricao_datainicio']) : '',
            'nutricao_datatermino' => (isset($data['nutricao_datatermino']) && $data['nutricao_datatermino'] != '') ? $this->setData($data['nutricao_datatermino']) : '',
            'nutricao_nivel' => $data['nutricao_nivel'],
            'aluno_id' => $data['aluno_id'],
            'nutricao_padrao' => $data['nutricao_padrao']
        ]);

        return 'Alterado com sucesso!';
    }

    // DELETA NUTRIÇÃO
    public function delNutricaoPadrao($id) {
        Nutricao::where('id', $id)->delete();
    }

    
}
