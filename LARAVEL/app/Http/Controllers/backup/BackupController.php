<?php 
 
namespace App\Http\Controllers\Backup; 
 
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use Mail; 
use Storage;
use File ;

class BackupController extends Controller 
{   


    public function __construct() {
       
        setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
        date_default_timezone_set('America/Sao_Paulo');

        
        
    }

  public function listarArquivos() {    
      $arrayRemessa = array_map('pathinfo', File::files('../database/backup'));
      return $arrayRemessa;
  }

  public function delArquivoAntigos(){
      $arquivos = $this->listarArquivos();
      $data30dias = date("Ymd", strtotime( '-30 days' ) );
     // $data30dias = date('Ymd');
     
      foreach($arquivos as $item) {
        $data_arquivo = substr($item['basename'], 11, 8);
        if($data_arquivo < $data30dias) {
            Storage::disk('backup')->delete($item['basename']);
        }

      }
  }


  private function validaHora(){
     
    $hora = date('H:i');
    if( $hora =='00:30') {
      return 'fazerbackup';
    } else if($hora =='12:30'){
      return 'fazerbackup';

    } else {
      return 'naofazer';

    }
  }

    public function backup($senha){ 

      $vhora = $this->validaHora();
      
      $this->delArquivoAntigos();
      
      if($senha == '909003983' && $vhora == 'fazerbackup') {
          $dbhost = env('DB_HOST'); 
          $dbuser = env('DB_USERNAME'); 
          $dbpass = env('DB_PASSWORD'); 
          $dbname = env('DB_DATABASE'); 
          $backupfile = 'Autobackup_' . date("Ymd-H-i-s") . '.sql'; 
          $backupzip = $backupfile . '.tar.gz'; 
          $caminho = database_path('backup'); 
       
        system("mysqldump $dbname > $caminho/$backupfile -h sistemapcb.net.br -u $dbuser --pass=.]2~p6Tjxe+F"); 

        //@todo zip
        // system("tar -czvf $backupzip $backupfile"); 
        // Storage::disk('local')->put($backupfile.'zip', $backupzip);

      
        $data = array('dbname' => $dbname );
      
     // so descomentar as linhas abaixo para  enviar e-mail
          Mail::send('backup.send_backup', $data, function ($message) use ($data)  {   
              $message->to('ruiz@7cliques.com.br') 
              ->cc('karstenschwab@gmail.com')
              ->bcc('karsten80@hotmail.com')
              ->subject('Backup -  backup gerado com sucesso!!') ;
              // ->attach($backupzip); 
          }); 

          return "Backup gerado com sucesso.";

      } else {
          return "Acesso negado.";

      }
    
    } 


} 