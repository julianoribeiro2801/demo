<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tipoconquista;
use Hash;
use App\Http\Requests\TipoconquistaRequest;
// Precisa para funcionar o combo
use Auth;
use App\Services\FilialService;

class TipoconquistaController extends Controller
{
    public function __construct(FilialService $filialService)
    {
        $this->filialService = $filialService;
    }

    public function index()
    { 
        $headers = ['category' => 'Clientes', 'title' => 'Tipo de Conquistas'];
        $unidades_combo = $this->filialService->unidadesComboTop();

        $tipoconquistas = Tipoconquista::all();
        return view('admin.tipoconquistas.index', compact('tipoconquistas', 'headers', 'unidades_combo'));
    }


    public function index_novo() 
    {
        $headers = ['category' => 'Desafios (v. 0.2 Beta)', 'title' => 'Desafios'];
        $unidades_combo = $this->filialService->unidadesComboTop();
     
        return view('admin.desafios.index', compact('headers', 'unidades_combo'));
    }

    public function edit($id)
    {
        $headers = ['category' => 'Clientes', 'title' => 'Tipo de Conquistas'];
        $unidades_combo = $this->filialService->unidadesComboTop();

        $tipoconquista = Tipoconquista::where('id', $id)->first();
        return view('admin.tipoconquistas.edit', compact('tipoconquista', 'headers', 'unidades_combo'));
    }

    public function create()
    {
        $headers = ['category' => 'Clientes', 'title' => 'Tipo de Conquistas'];
        $unidades_combo = $this->filialService->unidadesComboTop();

        return view('admin.tipoconquistas.create', compact('headers', 'unidades_combo'));
    }

    public function update(TipoconquistaRequest $request, $id)
    {
        $data = $request->all();

        Tipoconquista::where('id', $id)->update(['dstipoconquista' => $data['dstipoconquista'],
        'tpmedida' => $data['tpmedida']

       ]);
     
        return redirect()->route('admin.clientes.tipoconquistas.index');
    }

    public function store(TipoconquistaRequest $request)
    {
        $data = $request->all();
       
        Tipoconquista::create(['dstipoconquista' => $data['dstipoconquista'],
        'tpmedida' => $data['tpmedida']]);
     
        return redirect()->route('admin.clientes.tipoconquistas.index');
    }


    // public function create_user(){

          //  User::create( [
    //          'email' => 'kk@msn.com' ,
    //          'password' => Hash::make( '123456' ) ,
    //          'name' => 'KK Ruiz' ,
    //          'role' => 'admin'
    //         ] );
    // }
}
