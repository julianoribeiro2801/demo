<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// Precisa para funcionar o combo
// use App\Services\FilialService;
// use App\Services\SendPushService;
use DB;
use App\Models\PrescricaoCalendario;
use App\Models\Agendaavalsemana;
use App\Models\Agendaavalaluno;
use App\Models\Historicotreino;
class FrequenciaController extends Controller {

   
    public function retDiaSemana($id) {
        switch ($id) {
            case 1:
                $dia = 'D';
                # code...
                break;
            case 2:
                $dia = 'S';
                # code...
                break;
            case 3:
                $dia = 'T';
                #code...
                break;
            case 4:
                $dia = 'Q';
                # code...
                break;
            case 5:
                $dia = 'Q';
                # code...
                break;
            case 6:
                $dia = 'S';
                # code...
                break;
            case 7:
                $dia = 'S';
                # code...
                break;
           
        }
         return $dia; 
    }

   

    // CALENDARIO
    public function MostreSemanas() {
        $semanas = array(
            'D',
            'S',
            'T',
            'Q',
            'Q',
            'S',
            'S'
        );
        for ($i = 0; $i < 7; $i++) {
            echo "<th>" . $semanas[$i] . "</th>";
        }
    }

    public function GetNumeroDias($mes) {
        $numero_dias = array(
            '01' => 31, '02' => 28, '03' => 31, '04' => 30, '05' => 31, '06' => 30,
            '07' => 31, '08' => 31, '09' => 30, '10' => 31, '11' => 30, '12' => 31
        );
        if (((date('Y') % 4) == 0 and ( date('Y') % 100) != 0) or ( date('Y') % 400) == 0) {
            $numero_dias['02'] = 29; // altera o numero de dias de fevereiro se o ano for bissexto
        }
        return $numero_dias[$mes];
    }

    public function GetNomeMes($mes) {
        $meses = array('01' => "Janeiro", '02' => "Fevereiro", '03' => "Março",
            '04' => "Abril", '05' => "Maio", '06' => "Junho",
            '07' => "Julho", '08' => "Agosto", '09' => "Setembro",
            '10' => "Outubro", '11' => "Novembro", '12' => "Dezembro"
        );

        if ($mes >= 01 && $mes <= 12) {
            return $meses[$mes];
        } else {
            return "Mês deconhecido";
        }
    }
    
    
    public function marcaPresenca($dia , $mes, $ano, $idunidade, $client_id) {
        
        $presencasAluno = Historicotreino::select('ID')->where('IDALUNO', $client_id)
        	->where('IDUNIDADE', $idunidade)
        	->whereMonth('DTINICIO', $mes)
        	->whereYear('DTINICIO', $ano)
                ->whereDay('DTINICIO', $dia)->get();
        
        
       if (sizeof($presencasAluno)>0):
           return "presenca";
       else:     
           return "naoveio";
       endif;
       
    }

    public function MostreCalendario($mes, $ano, $idunidade, $client_id) {
       

        $numero_dias = $this->GetNumeroDias($mes); // retorna o número de dias que tem o mês desejado
        $nome_mes = $this->GetNomeMes($mes);
        $diacorrente = 0;
        $diasemana = jddayofweek(cal_to_jd(CAL_GREGORIAN, $mes, "01", $ano), 0); // função que descobre o dia da semana

        // busca desc_prescricao
        $prescicoesAluno = Historicotreino::where('IDALUNO', $client_id)
        	->where('IDUNIDADE', $idunidade)
        	->whereMonth('DTINICIO', $mes)
        	->whereYear('DTINICIO', $ano)->get();
        $arrayDesc = [];
        foreach ($prescicoesAluno as $prescicaoAluno) {
            
            $arrayDesc += array(substr($prescicaoAluno['DTINICIO'], 8, 2) => $prescicaoAluno['DTINICIO']);
            
        }

        echo "<h3 class='mesatual' >" . $nome_mes . " de " . $ano . "</h3>
        <input type='hidden' id='anoatual' value='" . $ano . "'>";

        echo '<table class="calendario">' ;
        echo "<thead>";
        echo "<tr>";
        $this->MostreSemanas(); // função que mostra as semanas aqui
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";
        for ($linha = 0; $linha < 6; $linha++) {
            echo "<tr>";
            for ($coluna = 0; $coluna < 7; $coluna++) {
                
                
                $class=$this->marcaPresenca($diacorrente, $mes, $ano, $idunidade, $client_id);
                
                if (($diacorrente == (date('d') - 1) && date('m') == $mes)) {
                    echo "<td id='$class'>";
                } else {
                    if (($diacorrente + 1) <= $numero_dias) {
                        if ($coluna < $diasemana && $linha == 0) {
                            echo "<td>";
                          
                        } else {
                            $d = $diacorrente + 1;
                            echo "<td ";
                            echo "class='col-$ano-$mes-$d'";
                            echo ">";
                        }
                    }
                }
                
                if ($diacorrente + 1 <= $numero_dias) {
                    if ($coluna < $diasemana && $linha == 0) {   
                        echo "</td>";
                    } else {
                        
                         ++$diacorrente;
                        
                        // se o cara veio $class
                       echo "<div class='$class'>  $diacorrente </div>";
                       


                        echo "</td>";
                    }
                } else {
                    break;
                }
            }
            echo "</tr>";
        }
        echo "<tbody>";
        echo "</table>";
    }

    public function MostreCalendarioCompleto() {
        echo "<table align = 'center'>";
        $cont = 1;
        for ($j = 0; $j < 4; $j++) {
            echo "<tr>";
            for ($i = 0; $i < 3; $i++) {
                echo "<td>";
                $this->MostreCalendario(($cont < 10) ? "0" . $cont : $cont);
                $cont++;
                echo "</td>";
            }
            echo "</tr>";
        }
        echo "</table>";
    }

    public function Data($Data) {
        $Data = explode('/', $Data);
        $Data = $Data[2] . '-' . $Data[1] . '-' . $Data[0];
        return $Data;
    }

    public function getCaledario($id_user, $idunidade, $mes,$ano) {	
		
        $mes = str_pad($mes, 2, "0", STR_PAD_LEFT);

        return $this->MostreCalendario($mes, $ano,$idunidade, $id_user);
    }

    // FECHA CALENDARIO



}
