<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use Illuminate\Database\Connection;
use DB;
use App\Models\User;
use App\Models\UserDados;
use App\Models\Cidade;
use Hash;
use App\Http\Requests\AlunoRequest;
use App\Services\TrataDadosService;
use App\Models\Configura;
// Precisa para funcionar o combo
use Auth;


class AlunoController extends AdminBaseController
{
    public function __construct(TrataDadosService $trataDadosService)
    {
        parent::__construct();
        $this->trataDadosService = $trataDadosService;
       
    }

    public function search(Request $request)
    {
        $query = $request->query('search');

        $result = User::allByUnidade()->where('name', 'like', "%{$query}%")->where('role', '<>', 'admin')
        ->orderBy('role')->orderBy('name')->paginate(50);

        $headers = ['category' => 'Clientes', 'title' => 'Pesquisar '];

        return view('admin.alunos.search', compact('result', 'headers', 'query'));
    }

    public function index()
    {
        $headers = ['category' => 'Clientes', 'title' => 'Alunos '];

        return view('admin.alunos.index', compact('headers'));
    }


     public function crmsegmenta()
    {
        $headers = ['category' => 'Segmentação de Público', 'title' => 'Segmentação de Público'];

        return view('admin.alunos.crmsegmenta', compact( 'headers'));
    }

    public function crm()
    {
        $headers = ['category' => 'CRM', 'title' => 'CRM'];

        return view('admin.alunos.crm', compact( 'headers'));
    }


    public function alunobunny()
    {
        $headers = ['category' => 'Clientes', 'title' => 'Clientes'];
        $alunos = User::allByUnidade();

        return view('admin.alunos.alunobunny', compact('alunos', 'headers'));
    }

    public function edit($id)
    {
        $headers = ['category' => 'Clientes', 'title' => 'Aluno Bunny'];
        $aluno = User::select('users.*', 'user_dados.telefone', 'user_dados.endereco', 'cidade.nome as cidade', 'cidade.estado', 'user_dados.bairro', 'dt_nascimento', 'cpf')
                        ->JOIN('user_dados', 'user_dados.user_id', '=', 'users.id')
                        ->JOIN('cidade', 'user_dados.idcidade', '=', 'cidade.id')->where('users.id', $id)->first();

        $nomes = User::select('id', 'name')->get();
        $selectedPessoas = $this->trataDadosService->listToSelectPessoas($nomes);
        return view('admin.alunos.edit', compact('aluno', 'selectedPessoas', 'headers'));
    }

    public function create()
    {
        $headers = ['category' => 'Clientes', 'title' => 'Aluno Bunny'];
        $pessoas = User::select('id', 'name')->get();
        $selectedPessoas = $this->trataDadosService->listToSelectPessoas($pessoas);

        return view('admin.alunos.create', compact('selectedPessoas', 'headers'));
    }

    public function update(AlunoRequest $request, $id)
    {
        $data = $request->all();

        if (isset($data['password']) && $data['password'] != '') {
            $data['password'] = bcrypt($data['password']);
        }

        User::where('id', $id)->update(['name' => $data['name'],
            'password' => $data['password'],
            'email' => $data['email']
        ]);

        UserDados::where('user_id', $id)->update(['endereco' => $data['endereco'],
            'telefone' => $data['telefone'],
            'numero' => $data['numero']
        ]);

        return redirect()->route('admin.clientes.alunos.index');
    }
  
    public function excluirAluno($id)
    {
        
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }        

        User::where('id', $id)->where('idunidade', $idunidade)->update(['excluido' => 'S'
                
        ]);
        
        return 'ok';
        
    }
    
    public function clientePremium($cliente,$categoria) {
     



        $user = ["idcategoria" => $categoria ];

        $update = User::where('id', $cliente)
        ->update($user);



        return $user; 
        
    }    

    public function vincularUpdateDepenteAluno(Request $request, $id_responsavel) {
     



        $user = ["name" => $request->name,
            "email" => $request->email,
            "parent_id"=> $id_responsavel ];

        $update = User::where('id', $request->id)
        ->update($user);

      

          $dados_user = [
            'telefone' => $request['dados']['celular'],
            'celular' => $request['dados']['celular'],
            'dt_nascimento' => $request['dados']['dt_nascimento'],
            'cpf' => $request['dados']['cpf'],
            'site' => $request['dados']['site'],
            'origem' => $request['dados']['origem'],
            'endereco' => $request['dados']['celular'],
            'numero' => $request['dados']['numero'],
            // 'cep' => $request['dados']['cep'],
            'bairro' => $request['dados']['bairro'],
            'idestado' => $request['dados']['idestado'],
            'idcidade' => $request['dados']['idcidade'],
            'genero' => $request['dados']['genero'],
            'controle_id' => $request['dados']['controle_id'],
            'profile_type' => $request['dados']['profile_type'],
            'complemento' => $request['dados']['complemento'],
            'objetivo_id' => $request['dados']['objetivo_id'],
            'situacao' => $request['dados']['situacao'],
            'situacaomatricula'  => $request['dados']['situacaomatricula']
          ];
          

        $dados_update = UserDados::where('user_id', $request->id)
        ->update($dados_user);

        return $dados_update; 
        
    }

 
    public function vincularDepenteAluno(Request $request, $id_responsavel) {
        
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }  
        
        $user = ["name" => $request->name,
          "email" => $request->email,
           "idunidade" => $idunidade,
            "role"=>'prospect',
            "excluido" => "N", "avatar" => "user-a4.jpg"];

        $inserir = User::create($user);
        $id_depente = $inserir->id;

        $dados_user = ["celular" =>  $request->celular,
          "genero" => $request->genero,
          "origem" => $request->origem,
          "user_id" =>  $id_depente,
          "idunidade" => $idunidade];
          

        $isert_user = UserDados::create($dados_user);

        $update = User::where('id', $dados_user['user_id'])
        ->update(['parent_id' => $id_responsavel]);

        return $update; 

        
    }


    public function store(AlunoRequest $request)
    {
        $data = $request->all();

        $verificaemail = User::where('email', $data['email'])->first();

        if (count($verificaemail) > 0) {
            return "Email já existe";
        } else {
            DB::beginTransaction();
            try {
                if (isset($data['password']) && $data['password'] != '') {
                    $data['password'] = bcrypt($data['password']);
                } else {
                    $senha = str_random(9);
                    $data['password'] = bcrypt($senha);
                }

                $cliente = User::create(['name' => $data['name'],
                            'idunidade' => $this->idUnidade,
                            'email' => $data['email'],
                            'password' => $data['password'],
                            'role' => 'cliente'
                ]);

                $idcidade = Cidade::select('id')->where('nome', $data['cidade'])->first();

                UserDados::create(['user_id' => $cliente->id,
                    'idunidade' => $this->idUnidade,
                    'telefone' => $data['telefone'],
                    'idcidade' => $idcidade->id,
                    'endereco' => $data['endereco'],
                    'numero' => $data['numero'],
                    'bairro' => $data['bairro'],
                    'dt_nascimento' => $data['dt_nascimento'],
                    'CPF' => $data['CPF']
                ]);

                DB::commitTransaction();
            } catch (Illuminate\Database\QueryException $e) {
                DB::rollbackTransaction();
                return "Não foi possivel salvar";
            }
        }

        return redirect()->route('admin.clientes.alunos.index');
    }
}
