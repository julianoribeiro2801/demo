<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Hope;

use Hash;
use App\Http\Requests\AdminCategoryRequest;

// necessario para funcionar os combos
use App\Services\TrataDadosService;

class AdminController extends Controller
{
    public function __construct(TrataDadosService $trataDadosService)
    {
        $this->trataDadosService = $trataDadosService;
    }

    public function index()
    {
        $headers = ['category' => 'Clientes', 'title' => 'Usuarios'];
        $categories = 	User::all();
        return view('admin.categories.index', compact('categories', 'headers'));
    }

    public function edit($id)
    {
        $headers = ['category' => 'Clientes', 'title' => 'Usuarios'];
        $category = User::where('id', $id)->first();
        $nomes =User::select('id', 'name')->get();
        $selectedCategories = $this->trataDadosService->listToSelect($nomes);

        return view('admin.categories.edit', compact('category', 'selectedCategories', 'headers'));
    }

    public function create()
    {
        $headers = ['category' => 'Clientes', 'title' => 'Usuarios'];
        $categories =User::select('id', 'name')->get();
        $selectedCategories = $this->trataDadosService->listToSelect($categories);

        return view('admin.categories.create', compact('selectedCategories', 'headers'));
    }

    public function update(AdminCategoryRequest $request, $id)
    {
        $data = $request->all();

        User::where('id', $id)->update(['name' => $data['name'],
        'email' => $data['email']

       ]);
     
        return redirect()->route('admin.categories.index');
    }

    public function store(AdminCategoryRequest $request)
    {
        $data = $request->all();
//      dd($request->all());
        $pass = $data['password'];
       
        User::create(['name' => $data['name'],
        'email' => $data['email'],
        'password' => Hash::make($pass),
        'role' => 'admin' ]);
     
        return redirect()->route('admin.categories.index');
    }


    // public function create_user(){

          //  User::create( [
    //          'email' => 'kk@msn.com' ,
    //          'password' => Hash::make( '123456' ) ,
    //          'name' => 'KK Ruiz' ,
    //          'role' => 'admin'
    //         ] );
    // }
}
