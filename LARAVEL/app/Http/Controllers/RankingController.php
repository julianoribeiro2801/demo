<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB;
use Input;
// use Request;
use Image;
use File;
use App\Models\Notificacao;

use Auth;
use App\Services\FilialService;
use Storage;
class RankingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $headers = ['category' => 'Empresa', 'title' => 'Atividades '];
        $atividades = Atividade::all();
        return view('admin.desafios.index', compact('atividades', 'headers'));
    }
    public function getDenuncias()
    {
        $data = Input::all();
        $denuncias=DB::select("select dc.dtdenuncia,dc.idalunodenuncia, dc.idunidade, dc.idconquista,u.name "
                 . " from denunciaconquista dc,  users u where u.id =dc.idalunodenuncia  ");
        return response()->json(compact('denuncias'));
    }
    public function addNotificacao()
    {
        //if(Request::ajax()) {
        $data = Input::all();
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
                        
        $dataNotifica['idunidade'] = $idunidade;
        $dataNotifica['idaluno'] = $data['aluno_id'];
        $dataNotifica['idconquista'] = $data['conquista_id'];
        $dataNotifica['dsnotificacao'] = $data['dsnotificacao'];
        $notifica = new Notificacao();
        if ($notifica->create($dataNotifica)):
                $retorno['title'] = 'Sucesso!';
        $retorno['type'] = 'success';
        $retorno['text'] = 'Notificacao gravada com sucesso!'; else:
                $retorno['title'] = 'Erro!';
        $retorno['type'] = 'error';
        $retorno['text'] = 'Erro ao atualizar empresa!';
        endif;
        return $retorno;
        //}
    }
    
    
    function verificarArquivo($name_img) {


        $diskimagem = Storage::disk('avatars');

        // SE A FOTO EXISTIR COLOCO
        if ($diskimagem->exists($name_img)) {
            return $name_img;
        } else {
            return "user-a4.jpg";
        }
    }       
    
    
    public function getRanking($id)
    {
        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
      
        /*$sql = "select count(dc.id) as qtdenuncia,tp.tpmedida,rk.nrposicao, rk.idaluno, rk.idunidade, rk.idconquista, rk.qtconquista, a.name";
        $sql .= " from  conquista as c,tipoconquista as tp, users as a,rankingconquista as rk left JOIN";
        $sql .= " denunciaconquista dc on dc.idunidade = rk.idunidade and";
        $sql .= " dc.idaluno = rk.idaluno and dc.idconquista = rk.idconquista";
        $sql .= " where rk.idunidade = " . $idunidade;
        $sql .= " and rk.idconquista = 1";
        $sql .= " and rk.idunidade = a.idunidade";
        $sql .= " and c.id = rk.idconquista";
        $sql .= " and c.idtipoconquista = tp.id";
        $sql .= " and a.id = rk.idaluno order by rk.nrposicao asc";*/
        $sql1 = "select tp.tpmedida,rk.nrposicao, rk.idaluno, rk.idunidade, rk.idconquista, rk.qtconquista, a.name";
        $sql1 .= " from rankingconquista as rk,conquistausuario as cqu, conquista as c,tipoconquista as tp, users as a";
        $sql1 .= " where rk.idunidade = " . $idunidade;
        $sql1 .= " and rk.idunidade = cqu.idunidade";
        $sql1 .= " and rk.idaluno = cqu.idaluno";
        $sql1 .= " and rk.idconquista = cqu.idconquista";
        $sql1 .= " and cqu.stparticipa = 'S'";
        $sql1 .= " and rk.idconquista = " . $id;
        $sql1 .= " and rk.idunidade = a.idunidade";
        $sql1 .= " and c.id = rk.idconquista";
        $sql1 .= " and c.idtipoconquista = tp.id and rk.qtconquista > 0 ";
        $sql1 .= " and a.id = rk.idaluno order by rk.nrposicao asc";        
        $rks = DB::select($sql1);
        $musculacaos = [];
        foreach ($rks as $key => $value) {
            $musculacaos[$key]['FOTOALUNO'] = 'http://sistemapcb.com.br/uploads/avatars/' . $this->verificarArquivo('user-' . $rks[$key]->idaluno . '.jpg');

            /*if(file_exists('uploads/avatars/user-' . $rks[$key]->idaluno . '.jpg')){
                 $musculacaos[$key]['FOTOALUNO']=$musculacaos[$key]['FOTOALUNO'];
            } else{
                 $musculacaos[$key]['FOTOALUNO']='http://sistemapcb.com.br/uploads/avatars/a4.jpg';
                
            }*/
            $musculacaos[$key]['IDALUNO'] = $rks[$key]->idaluno;
            $musculacaos[$key]['NMALUNO'] = $rks[$key]->name;
            $musculacaos[$key]['IDUNIDADE'] = $rks[$key]->idunidade;
            $musculacaos[$key]["IDCONQUISTA"] = $rks[$key]->idconquista;
            //if ($zera == 'S') {
             //   $musculacaos[$key]["NRPOSICAO"] = 0;
            //} else {
                $musculacaos[$key]["NRPOSICAO"] = $rks[$key]->nrposicao;
            //}

            $numer = $rks[$key]->qtconquista;

            if ($rks[$key]->tpmedida == "ton") {
                $musculacaos[$key]["UNIDADE"] = $rks[$key]->tpmedida;
                $numer = $rks[$key]->qtconquista;
                $musculacaos[$key]["QTCONQUISTA"] = str_replace(".", ",", number_format($numer, 1));
            } else {
                if ($rks[$key]->tpmedida == "m") {
                    $musculacaos[$key]["UNIDADE"] = $rks[$key]->tpmedida;
                    $musculacaos[$key]["QTCONQUISTA"] = number_format($rks[$key]->qtconquista, 0, ',', '.');
                } else {
                    if ($rks[$key]->tpmedida == "min") {
                        $musculacaos[$key]["UNIDADE"] = $rks[$key]->tpmedida;
                        $musculacaos[$key]["QTCONQUISTA"] = number_format($rks[$key]->qtconquista, 2, ':', ',');
                    } else {
                        if ($rks[$key]->tpmedida == "km") {
                            $musculacaos[$key]["UNIDADE"] = $rks[$key]->tpmedida;
                            $musculacaos[$key]["QTCONQUISTA"] = number_format($rks[$key]->qtconquista, 2, ',', '.');
                        } else {
                            $musculacaos[$key]["UNIDADE"] = " " . $rks[$key]->tpmedida;
                            $musculacaos[$key]["QTCONQUISTA"] = number_format($rks[$key]->qtconquista, 0, '', ',');
                        }
                    }
                }
            }
        }
        

       /* $sql = "select count(dc.id) as qtdenuncia,tp.tpmedida,rk.nrposicao, rk.idaluno, rk.idunidade, rk.idconquista, rk.qtconquista, a.name";
        $sql .= " from  conquista as c,tipoconquista as tp, users as a,rankingconquista as rk left JOIN";
        $sql .= " denunciaconquista dc on dc.idunidade = rk.idunidade and";
        $sql .= " dc.idaluno = rk.idaluno and dc.idconquista = rk.idconquista";
        $sql .= " where rk.idunidade = " . $idunidade;
        $sql .= " and rk.idconquista = 2";
        $sql .= " and rk.idunidade = a.idunidade";
        $sql .= " and c.id = rk.idconquista";
        $sql .= " and c.idtipoconquista = tp.id";
        $sql .= " and a.id = rk.idaluno order by rk.nrposicao asc";
        $ginasticas = DB::select($sql);

        $sql = "select count(dc.id) as qtdenuncia,tp.tpmedida,rk.nrposicao, rk.idaluno, rk.idunidade, rk.idconquista, rk.qtconquista, a.name";
        $sql .= " from  conquista as c,tipoconquista as tp, users as a,rankingconquista as rk left JOIN";
        $sql .= " denunciaconquista dc on dc.idunidade = rk.idunidade and";
        $sql .= " dc.idaluno = rk.idaluno and dc.idconquista = rk.idconquista";
        $sql .= " where rk.idunidade = " . $idunidade;
        $sql .= " and rk.idconquista = 3";
        $sql .= " and rk.idunidade = a.idunidade";
        $sql .= " and c.id = rk.idconquista";
        $sql .= " and c.idtipoconquista = tp.id";
        $sql .= " and a.id = rk.idaluno order by rk.nrposicao asc";
        $natacaos = DB::select($sql);

        $sql = "select count(dc.id) as qtdenuncia,tp.tpmedida,rk.nrposicao, rk.idaluno, rk.idunidade, rk.idconquista, rk.qtconquista, a.name";
        $sql .= " from  conquista as c,tipoconquista as tp, users as a,rankingconquista as rk left JOIN";
        $sql .= " denunciaconquista dc on dc.idunidade = rk.idunidade and";
        $sql .= " dc.idaluno = rk.idaluno and dc.idconquista = rk.idconquista";
        $sql .= " where rk.idunidade = " . $idunidade;
        $sql .= " and rk.idconquista = 4";
        $sql .= " and rk.idunidade = a.idunidade";
        $sql .= " and c.id = rk.idconquista";
        $sql .= " and c.idtipoconquista = tp.id";
        $sql .= " and a.id = rk.idaluno order by rk.nrposicao asc";
        $crosss = DB::select($sql);

        $sql = "select count(dc.id) as qtdenuncia,tp.tpmedida,rk.nrposicao, rk.idaluno, rk.idunidade, rk.idconquista, rk.qtconquista, a.name";
        $sql .= " from  conquista as c,tipoconquista as tp, users as a,rankingconquista as rk left JOIN";
        $sql .= " denunciaconquista dc on dc.idunidade = rk.idunidade and";
        $sql .= " dc.idaluno = rk.idaluno and dc.idconquista = rk.idconquista";
        $sql .= " where rk.idunidade = " . $idunidade;
        $sql .= " and rk.idconquista = 5";
        $sql .= " and rk.idunidade = a.idunidade";
        $sql .= " and c.id = rk.idconquista";
        $sql .= " and c.idtipoconquista = tp.id";
        $sql .= " and a.id = rk.idaluno order by rk.nrposicao asc";
        $corridas = DB::select($sql);

        $sql = "select count(dc.id) as qtdenuncia,tp.tpmedida,rk.nrposicao, rk.idaluno, rk.idunidade, rk.idconquista, rk.qtconquista, a.name";
        $sql .= " from  conquista as c,tipoconquista as tp, users as a,rankingconquista as rk left JOIN";
        $sql .= " denunciaconquista dc on dc.idunidade = rk.idunidade and";
        $sql .= " dc.idaluno = rk.idaluno and dc.idconquista = rk.idconquista";
        $sql .= " where rk.idunidade = " . $idunidade;
        $sql .= " and rk.idconquista = 6";
        $sql .= " and rk.idunidade = a.idunidade";
        $sql .= " and c.id = rk.idconquista";
        $sql .= " and c.idtipoconquista = tp.id";
        $sql .= " and a.id = rk.idaluno order by rk.nrposicao asc";
      
        $ciclismos = DB::select($sql);
        return compact('musculacaos', 'ginasticas', 'natacaos', 'crosss', 'corridas', 'ciclismos');*/
        return compact('musculacaos');
        //return $rankings;
    }
}
