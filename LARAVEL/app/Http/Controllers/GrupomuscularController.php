<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Grupomuscular;
use Hash;
use App\Http\Requests\GrupomuscularRequest;

class GrupomuscularController extends Controller
{
    public function index()
    {
        $headers = ['category' => 'Clientes', 'title' => 'Grupos de Exercicíos'];
        $gruposmusculares = Grupomuscular::all();
        return view('admin.gruposmusculares.index', compact('gruposmusculares', 'headers'));
    }

    public function edit($id)
    {
        $headers = ['category' => 'Clientes', 'title' => 'Grupos de Exercicíos'];
        $grupomuscular = Grupomuscular::where('id', $id)->first();
        return view('admin.gruposmusculares.edit', compact('grupomuscular', 'headers'));
    }

    public function create()
    {
        $headers = ['category' => 'Clientes', 'title' => 'Grupos de Exercicíos'];
        return view('admin.gruposmusculares.create', compact('headers'));
    }

    public function update(GrupomuscularRequest $request, $id)
    {
        $data = $request->all();

        Grupomuscular::where('id', $id)->update(['nmgrupo' => $data['nmgrupo']

       ]);
     
        return redirect()->route('admin.clientes.gruposmusculares.index');
    }

    public function store(GrupomuscularRequest $request)
    {
        $data = $request->all();
       
        Grupomuscular::create(['nmgrupo' => $data['nmgrupo']]);
     
        return redirect()->route('admin.clientes.gruposmusculares.index');
    }


    // public function create_user(){

          //  User::create( [
    //          'email' => 'kk@msn.com' ,
    //          'password' => Hash::make( '123456' ) ,
    //          'name' => 'KK Ruiz' ,
    //          'role' => 'admin'
    //         ] );
    // }
}
