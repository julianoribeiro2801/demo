<?php


Route::group(['prefix' => 'core'], function () {
    Route::get('', 'CoreController@index');
});

Route::get('/recuperaSenha/{email}', 'ClientController@recuperaSenha');


Route::group(['prefix' => 'cliente'], function () {
    Route::get('', 'ClientController@index');
    Route::put('refresh/password/{user}', 'ClientController@refreshPassword');
    Route::get('search/{query}', 'ClientController@search');
    Route::get('search', 'ClientController@search');
    Route::get('client/{id}', 'ClientController@show');
    Route::post('', 'ClientController@register');
    Route::put('{id}', 'ClientController@update');
    Route::get('{id}', 'ClientController@atualizar');
    Route::patch('{id}/{motivo}', 'ClientController@convert');
    Route::delete('', 'ClientController@deleteCli');
    Route::get('crm/{id}', 'CrmController@show');
    Route::get('crm', 'CrmController@index');
    
});

Route::group(['prefix' => 'projeto'], function () {
    Route::get('', 'ProjetoController@index');
    Route::get('projeto/{id}', 'projetoController@show');
    Route::post('', 'ProjetoController@register');
    Route::put('{id}', 'ProjetoController@update');
    Route::delete('', 'ProjetoController@destroy');
});
Route::group(['prefix' => 'prescricao'], function () {
    //   Route::get('','PrescricaoController@index');
//   Route::get('prescricao/{id}','PrescricaoController@show');
//   Route::post('', 'PrescricaoController@register');
//   Route::put('{id}', 'PrescricaoController@update');
//   Route::delete('', 'PrescricaoController@destroy');
});
Route::group(['prefix' => 'exercicio'], function () {
    Route::get('', 'ExercicioController@index');
    Route::get('exercicio/{id}', 'ExercicioController@show');
    Route::post('', 'ExercicioController@register');
    Route::put('{id}', 'ExercicioController@update');
    Route::delete('{id}', 'ExercicioController@destroy');
});

Route::group(['prefix' => 'grupomuscular'], function () {
    Route::get('', 'GrupomuscularController@index');
    Route::get('grupomuscular/{id}', 'GrupomuscularController@show');
    Route::post('', 'GrupomuscularController@register');
    Route::put('{id}', 'GrupomuscularController@update');
    Route::delete('{id}', 'GrupomuscularController@destroy');
});

/*Route::group(['prefix' => 'local'],function(){
   Route::get('','LocalController@index');
   Route::get('local/{id}','LocalController@show');
   Route::post('', 'LocalController@register');
   Route::put('{id}', 'LocalController@update');
   Route::delete('{id}', 'LocalController@destroy');
});

Route::group(['prefix' => 'modalidade'],function(){
   Route::get('','ModalidadeController@index');
   Route::get('modalidade/{id}','ModalidadeController@show');
   Route::post('', 'ModalidadeController@register');
   Route::put('{id}', 'ModalidadeController@update');
   Route::delete('{id}', 'ModalidadeController@destroy');
});*/

/*Route::group(['prefix' => 'funcionario'],function(){
   Route::get('','FuncionarioController@index');
   Route::get('funcionario/{id}','FuncionarioController@show');
   Route::post('', 'FuncionarioController@register');
   Route::put('{id}', 'FuncionarioController@update');
   Route::delete('{id}', 'FuncionarioController@destroy');
});*/

/*Route::group(['prefix' => 'unidade'],function(){
   Route::get('','UnidadeController@index');
   Route::get('unidade/{id}','UnidadeController@show');
   Route::get('cidade/{id}','UnidadeController@carregaCidade');
   Route::get('matriz/','UnidadeController@matriz');
   Route::post('', 'UnidadeController@register');
   Route::put('{id}', 'UnidadeController@update');
   Route::delete('{id}', 'UnidadeController@destroy');
});*/


Route::group(['prefix' => 'cvempresa'], function () {
    Route::get('', 'CvempresaController@index');
    Route::get('cvempresa/{id}', 'CvempresaController@show');
    Route::post('', 'CvempresaController@register');
    Route::put('{id}', 'CvempresaController@update');
    Route::delete('{id}', 'CvempresaController@destroy');
});

/*Route::group(['prefix' => 'gfm'],function(){
   Route::get('','GfmController@index');
   Route::get('gfm/{id}','GfmController@show');
   Route::get('indicadores/{id}','GfmController@indicadores');
   Route::get('grafico/{id}','GfmController@grafico');
   Route::post('', 'GfmController@register');
   Route::put('{id}', 'GfmController@update');
   Route::delete('{id}', 'GfmController@destroy');
});*/



Route::group(['prefix' => 'objectives'], function () {
    Route::post('', 'ObjectiveController@register');
});

Route::group(['prefix' => 'contact'], function () {
    Route::post('', 'ContactController@register');
});

Route::post('upload', 'UploadController@register');
