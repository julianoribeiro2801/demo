<?php
/**
 * Created by PhpStorm.
 * User: jeandro
 * Date: 04/04/17
 * Time: 22:28
 */

namespace App\API\Http\Controllers;

use App\API\Services\ObjectiveService;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ObjectiveController extends Controller
{
    private $objectiveService;

    public function __construct(ObjectiveService $objectiveService)
    {
        $this->objectiveService = $objectiveService;
    }

    public function register(Request $request)
    {
        return $this->objectiveService->register($request);
    }
}
