<?php
namespace App\API\Http\Controllers;

use App\Http\Controllers\Controller;

class ApiCoreController extends Controller
{
    protected $idUnidade;

    public function __construct()
    {
        $this->idUnidade = session('id_unidade');
    }
}
