<?php
/**
 * Created by PhpStorm.
 * User: jeandro
 * Date: 04/04/17
 * Time: 23:45
 */

namespace App\API\Http\Controllers;

use App\API\Services\ApiContactService;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    private $contactService;

    public function __construct(ApiContactService $contactService)
    {
        $this->contactService = $contactService;
    }

    public function register(Request $request)
    {
        return $this->contactService->register($request);
    }
}
