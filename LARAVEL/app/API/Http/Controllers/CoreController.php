<?php
/**
 * Created by PhpStorm.
 * User: jeandro
 * Date: 09/03/17
 * Time: 21:27
 */

namespace App\API\Http\Controllers;

use App\API\Services\CoreApiService;

class CoreController extends ApiCoreController
{
    private $service;
    public function __construct(CoreApiService $coreApiService)
    {
        $this->service = $coreApiService;
    }

    public function index()
    {
        return $this->service->index();
    }
}
