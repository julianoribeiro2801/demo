<?php

namespace App\API\Http\Controllers;

use App\API\Services\ApiProjetoService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProjetoController extends ApiCoreController
{
    private $service;

    public function __construct(ApiProjetoService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        return $this->service->index();
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'required|unique:users',
            'origem' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors()->getMessages(), 400);
        }
        return $this->service->register($request);
    }

    public function data($id)
    {
        return $this->service->data($id);
    }

    public function convert($id)
    {
        return $this->service->convert($id);
    }

    public function update(Request $request, $id)
    {
        return $this->service->update($request, $id);
    }

    public function show($id)
    {
        return $this->service->show($id);
    }

    public function destroy($id)
    {
    }
}
