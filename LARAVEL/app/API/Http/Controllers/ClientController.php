<?php

namespace App\API\Http\Controllers;

use App\API\Services\ApiClientService;
use App\Http\Requests\AlunoUpdateRequest;
use App\Models\User;
use App\Notifications\NewPass;
use Illuminate\Foundation\Testing\HttpException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ClientController extends ApiCoreController
{
    private $service;

    public function __construct(ApiClientService $service)
    {
        parent::__construct();
        $this->service = $service;
    }

    public function index()
    {
        return $this->service->index();
    }

    public function atualiza()
    {
        return $this->service->index();
    }    
    
    public function search($query = '')
    {
        return $this->service->search($query);
    }

    public function register(Request $request)
    {
        $request->merge(['idunidade' => session('id_unidade')]);
        // 'email' => 'required|unique:users',
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'required',
            'origem' => 'required'
        ]);

        if ($validator->fails()) {
          return response()->json($validator->errors()->getMessages(), 400);
        }

        return $this->service->register($request);
    }

    public function data($id)
    {
        return $this->service->data($id);
    }

    public function convert($id,$motivo)
    {
        try {

            return $this->service->convert($id,$motivo);
        } catch (\Exception $exception) {
            return response()->json($exception->getMessage(), 400);
        }
    }

    public function update(AlunoUpdateRequest $request, $id)
    {
        return $this->service->update($request, $id);
    }
    
    public function atualizar($id)
    {
        return $this->service->atualizar($id);
    }

    public function show($id){
        return $this->service->show($id);
    }
    public function deleteCli($id){
        return $this->service->deleteCli($id);
    }

    public function recuperaSenha($email)
    {
        $validar = User::where('email',$email)->first();
        if($validar) {
            $this->refreshPassword( $validar);
            return ['status'=>'success','msg'=>'Email para recuperação de senha enviado com sucesso!'];
        }

        return ['status'=>'error','msg'=>'Email não cadastrado'];
    }


    public function refreshPassword(User $user)
    {
        abort_unless($user,400,'Não foi encontado usuario');

        try {
            return response()->json($this->resetAppPassword($user));
        } catch (\Exception $exception) {
            return response()->json($exception->getMessage(), 400);
        }
    }

    public function resetAppPassword(User $user)
    {
        $newpass = $this->generatePass();

        $user->password = $newpass;
        $user->setRememberToken($this->generatePass(32) . time() . uniqid());
        $user->save();

        return $user->notify(new NewPass($newpass));
    }

    private function generatePass($length = 6)
    {
        $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        return substr(str_shuffle(str_repeat($pool, $length)), 0, $length);
    }
}
