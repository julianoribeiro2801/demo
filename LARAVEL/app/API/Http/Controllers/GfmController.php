<?php
namespace App\API\Http\Controllers;

use App\API\Services\ApiGfmService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class GfmController extends ApiCoreController
{
    private $service;

    public function __construct(ApiGfmService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        return $this->service->index();
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'idlocal' => 'required|max:10000'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors()->getMessages(), 400);
        }
        return $this->service->register($request);
    }

    public function data($id)
    {
        return $this->service->data($id);
    }

    public function update(Request $request, $id)
    {
        return $this->service->update($request, $id);
    }

    public function show($id)
    {
        return $this->service->show($id);
    }

    public function grafico($id)
    {
        return $this->service->grafico($id);
    }

    public function indicadores($dias)
    {
        return $this->service->indicadores($dias);
    }

    public function destroy($id)
    {
        return $this->service->destroy($id);
    }
}
