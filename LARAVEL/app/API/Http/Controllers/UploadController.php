<?php
/**
 * Created by PhpStorm.
 * User: jeandro
 * Date: 07/04/17
 * Time: 01:56
 */

namespace App\API\Http\Controllers;

use App\API\Services\ApiUploadService;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UploadController extends Controller
{
    protected $service;
    public function __construct(ApiUploadService $apiUploadService)
    {
        $this->service = $apiUploadService;
    }

    public function register(Request $request)
    {
        $fileAddress = $this->service->register($request);

        if ($fileAddress) {
            return response()->json($this->service->updateUserAvatar($request->client, $fileAddress));
        }
    }
}
