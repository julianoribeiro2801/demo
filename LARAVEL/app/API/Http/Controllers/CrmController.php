<?php
/**
 * Created by PhpStorm.
 * User: jeandro
 * Date: 09/03/17
 * Time: 21:27
 */

namespace App\API\Http\Controllers;

use App\API\Services\CrmClientService;
use App\Http\Controllers\Controller;

class CrmController extends Controller
{
    private $service;

    public function __construct(CrmClientService $crmClientService)
    {
        $this->service = $crmClientService;
    }
    public function index()
    {
        return $this->service->index();
    }

    public function show($id = null)
    {
        return $this->service->show($id);
    }
}
