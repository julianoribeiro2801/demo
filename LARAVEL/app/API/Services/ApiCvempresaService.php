<?php
namespace App\API\Services;

use App\Models\Unidade;
use App\Models\CVEmpresa;
use App\Services\CvempresaService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use League\Flysystem\Exception;

class ApiCvempresaService
{
    private $model;
    private $default = 'prospect';
    private $request;
    private $unidade;
    private $usuario;
    private $crm;
    public function __construct(CVEmpresa $cvempresa, Request $request)
    {
        $this->model = $cvempresa;
        $this->request = $request;
        $this->unidade = $this->getUnidade($this->request->header('unidade'));
    }

    public function index()
    {
        $cvempresas= $this->model->where('id', '>', 0)->orderBy('nmempresa', 'ASC')->get();

        return response()->json(compact('cvempresas'));
    }

    public function all()
    {
        $cvempresas = $this->model->where('id', '>', 0)->orderBy('nmempresa', 'ASC')->get();

        return response()->json(compact('cvempresas'));
    }
    public function register(Request $request)
    {
        try {
            $cvempresa = $this->model->create($this->formatUser($request));
            $cvempresa->dados()->create($request->all());
            return response()->json($cvempresa);
        } catch (Exception $exception) {
            return response()->json($exception->getMessage(), $exception->getCode());
        }
    }

    public function show($id)
    {
        $cvempresa = $this->model->where('id', $id)->first();
        return response()->json($cvempresa);
    }


    public function update(Request $request, $id)
    {
        try {
            $cvempresa = $this->model->find($id)->update($request->all());
            if ($this->model->find($id)->update($this->formatData($request))) {
                return response()->json('Cadastro atualizado com sucesso!!!');
            }
            $this->model->find($id)->dados()->create($this->formatData($request));
        } catch (Exception $e) {
            return response()->json($e->getMessage(), $e->getCode());
        }
    }

    public function data($id)
    {
        $client = $this->model->find($id)->with('dados');
    }

    public function destroy($id)
    {
        try {
            $cvempresa = $this->model->find($id)->delete();
            return response()->json('Cadastro removido com sucesso!!!');
        } catch (Exception $e) {
            return response()->json($e->getMessage(), $e->getCode());
        }
    }

    public function profiles()
    {
        $profiles = ProfileType::all();
        return $profiles;
    }
    private function formatUser(Request $request)
    {
        $data = $request->all();
        //$data['role'] = $request->role ? $request->role : $this->default;
        //$data['password'] = $request->password ? $request->password : bcrypt($this->default);

        return $data;
    }

    private function formatData(Request $request)
    {
        $data = collect($request->dados)->except('id', 'user_id', 'type', 'objetivo');

        $data['idunidade'] = $request->idunidade or 0;
        if (isset($request->dados['cidade'])) {
            $data['idcidade'] = $this->getCidadeId($data['cidade'], $data['estado'])['cidade'];
        }
        if (isset($request->dados['dt_nascimento']) && $request->dados['dt_nascimento']) {
            $data['dt_nascimento'] = Carbon::parse($request->dados['dt_nascimento']);
        }

        $data['profile_type'] = isset($data['profile_type']) ? $data['profile_type'] : 0 ;

        return $data->except('cidade', 'estado')->toArray();
    }

    private function getUser($id)
    {
        return User::find($id);
    }

    private function getUnidade($id)
    {
        return Unidade::find($id);
    }

    private function getCidadeId($cidade, $uf)
    {
        try {
            $estado = Estado::where('nome', 'like', $uf)->orWhere('uf', 'like', $uf)->first();
            $cidade = Cidade::where('nome', $cidade)->where('estado', $estado->id)->first();
            return ['cidade' => $cidade->id, 'estado' => $estado->id];
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    private function parseCidade($idCidade)
    {
        $cidade = Cidade::find($idCidade);
        $estado = Estado::find($cidade->estado);
        return ['cidade' => $cidade->nome, 'estado' => $estado->uf];
    }
}
