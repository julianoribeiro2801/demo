<?php
/**
 * Created by PhpStorm.
 * User: jeandro
 * Date: 04/04/17
 * Time: 23:45
 */

namespace App\API\Services;

use App\Models\CrmContato;
use App\Services\CrmService;
use Illuminate\Http\Request;

class ApiContactService
{
    protected $crm;
    private $usuario;
    public function __construct(CrmService $crmService, Request $request)
    {
        $this->usuario = $request->header('user');
        $this->crm = $crmService;
    }

    public function register(Request $request)
    {
        $data = $request->all();
        $data['idunidade'] = $request->header('unidade');

        try {
           
            $contato = CrmContato::create($data);
            $this->crm->register($this->usuario, $data['iduser'], $data['crmacao'], 'Criou um contato');
            return response()->json(['message' => 'Contato registrado com sucesso','contato' => $contato]);
        } catch (\Exception $exception) {
            dd($exception);
            return response()->json($exception->getMessage(), $exception->getCode());
        }
    }
}
