<?php
namespace App\API\Services;

use App\Models\Unidade;
use App\Models\UnidadeDados;

use App\Models\Cidade;
use App\Models\Estado;

use App\Services\UnidadeService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

use App\Services\TrataDadosService;

//use Auth;
use Illuminate\Support\Facades\Auth;
use League\Flysystem\Exception;

class ApiUnidadeService
{
    private $model;
    private $default = 'prospect';
    private $request;
    private $unidade;
    private $unidadedados;
   
    private $crm;
    public function __construct(Unidade $unidade, UnidadeDados $unidadedados, Request $request, TrataDadosService $trataDadosService)
    {
        $this->model = $unidadedados;
        $this->trataDadosService = $trataDadosService;
        $this->model = $unidade;
        $this->request = $request;
    }

    public function index()
    {
        $idunidade = Auth::user()->idunidade;
        $unidades= $this->model->where('id', '>', 0)->orderBy('fantasia', 'ASC')->get();
//        $empresa = $this->model->where('tipo','=','M')->orderBy('tipo', 'ASC')->get();

        $estados = Estado::select('id', 'nome')->get();
        $selectedEstado = $this->trataDadosService->listToSelectProgramas($estados);
        
        return response()->json(compact('unidades', 'estados', 'idunidade'));
    }

    public function all()
    {
        //$unidades = $this->model->where('id','>',0)->orderBy('fantasia', 'ASC')->get();
        $unidades =DB::select("select u.id ,u.fantasia, u.razao_social, u.user_id, u.parent_id,"
                   . " u.created_at, u.updated_at, u.situacao ,"
                   . " ud.id as iddados, ud.user_id, ud.idunidade, ud.telefone, ud.idcidade, ud.endereco, "
                   . " ud.numero, ud.cnpj, ud.email, ud.bairro, ud.created_at, ud.updated_at,"
                   . " ud.metros, ud.site, ud.celular from unidade u, unidade_dados ud"
                   . " where u.id = ud.idunidade  and u.id = " . 1);
        //$empresa = $this->model->where('tipo','=','M')->orderBy('tipo', 'ASC')->get();

        return response()->json(compact('unidades'));
    }
    public function matriz()
    {
        $user= Auth::user();
            
        $empresa = DB::select("select u.id ,u.fantasia, u.razao_social, u.user_id, u.parent_id,"
                   . " u.created_at, u.updated_at, u.situacao ,"
                   . " ud.id as iddados, ud.user_id, ud.idunidade, ud.telefone, ud.idcidade, ud.endereco, "
                   . " ud.numero, ud.cnpj, ud.email, ud.bairro, ud.created_at, ud.updated_at,"
                   . " ud.metros, ud.site, ud.celular from unidade u, unidade_dados ud"
                   . " where u.id = ud.idunidade and u.parent_id = '0' and u.id = " . $idunidade);
    

        return response()->json(compact('empresa', 'unidade'));
    }
    public function register(Request $request)
    {
        try {
            $unidade = $this->model->create($request->all());
            Unidadedados::create($this->insereId($request, $unidade->id));
            return response()->json($unidade->id);
        } catch (Exception $exception) {
            return response()->json($exception->getMessage(), $exception->getCode());
        }
    }

    public function show($id)
    {
        $unidade = $this->model->where('id', $id)->first();
        return response()->json($unidade);
    }
    public function carregaCidade($id)
    {
        $cidades = Cidade::select('id', 'nome')->where('estado', '=', $id)->get();
        $selectedCidades = $this->trataDadosService->listToSelectProgramas($cidades);
        return response()->json(compact('cidades'));
    }


    public function update(Request $request, $id)
    {
        try {
            $unidade = $this->model->find($id)->update($request->all());
            //Unidade::find($id)->update($request->all());
            Unidadedados::find($request->iddados)->update($request->all());
            //return response()->json('Cadastro atualizado com sucesso!!!');
        } catch (Exception $e) {
            return response()->json($e->getMessage(), $e->getCode());
        }
    }

    public function data($id)
    {
        $client = $this->model->find($id)->with('dados');
    }

    public function destroy($id)
    {
        try {
            $unidade = $this->model->find($id)->delete();
            return response()->json('Cadastro removido com sucesso!!!');
        } catch (Exception $e) {
            return response()->json($e->getMessage(), $e->getCode());
        }
    }

    
    private function insereId(Request $request, $id)
    {
        $data = $request->all();
        $data['idunidade'] = $request->idunidade ? $request->idunidade : $id;

        return $data;
    }
    private function formatUser(Request $request)
    {
        $data = $request->all();
        $data['role'] = $request->role ? $request->role : $this->default;
        $data['password'] = $request->password ? $request->password : bcrypt($this->default);

        return $data;
    }

    private function getUnidade($id)
    {
        return Unidade::find($id);
    }

    private function getCidadeId($cidade, $uf)
    {
        try {
            $estado = Estado::where('nome', 'like', $uf)->orWhere('uf', 'like', $uf)->first();
            $cidade = Cidade::where('nome', $cidade)->where('estado', $estado->id)->first();
            return ['cidade' => $cidade->id, 'estado' => $estado->id];
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    private function parseCidade($idCidade)
    {
        $cidade = Cidade::find($idCidade);
        $estado = Estado::find($cidade->estado);
        return ['cidade' => $cidade->nome, 'estado' => $estado->uf];
    }
}
