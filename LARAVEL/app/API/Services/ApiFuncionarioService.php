<?php
namespace App\API\Services;

use App\Models\Funcionario;
use App\Models\Unidade;
use App\Models\User;
use App\Models\UserDados;
use App\Services\FuncionarioService;
use Illuminate\Http\Request;
use League\Flysystem\Exception;

class ApiFuncionarioService
{
    private $model;
    private $request;
    private $unidade;
    private $default = 'prospect';

    public function __construct(Funcionario $funcionario, Request $request)
    {
        $this->model = $funcionario;
        $this->request = $request;
        $this->unidade = $this->getUnidade($this->request->header('unidade'));
    }

    public function index()
    {
        $funcionarios= $this->model->where('id', '>', 0)->orderBy('name', 'ASC')->get();

        return response()->json(compact('funcionarios'));
    }

    public function all()
    {
        $funcionarios = $this->model->where('role', '=', 'funcionario')->orderBy('name', 'ASC')->get();

        return response()->json(compact('funcionarios'));
    }

    public function register(Request $request)
    {
        try {
            $funcionario = $this->model->create($this->formatUser($request));
            UserDados::create($this->insereId($request, $funcionario->id));

            return response()->json($funcionario);
        } catch (Exception $exception) {
            return response()->json($exception->getMessage(), $exception->getCode());
        }
    }

    private function insereId(Request $request, $id)
    {
        $data = $request->all();
        $data['user_id'] = $request->user_id ? $request->user_id : $id;

        return $data;
    }

    public function show($id)
    {
        $funcionario = $this->model->where('id', $id)->first();
        return response()->json($funcionario);
    }

    public function update(Request $request, $id)
    {
        try {
            $funcionario = $this->model->find($id)->update($request->all());
            return response()->json('Cadastro atualizado com sucesso!!!');
        } catch (Exception $e) {
            return response()->json($e->getMessage(), $e->getCode());
        }
    }

    public function data($id)
    {
        $client = $this->model->find($id)->with('dados');
    }

    public function destroy($id)
    {
        try {
            $funcionario = $this->model->find($id)->delete();
            // if ($this->model->find($id)->update($this->formatData($request))){
            return response()->json('Cadastro removido com sucesso!!!');
            //}
        } catch (Exception $e) {
            return response()->json($e->getMessage(), $e->getCode());
        }
    }

    private function formatUser(Request $request)
    {
        $data = $request->all();
        $data['role'] = $request->role ? $request->role : 'funcionario';
        $data['password'] = $request->password ? $request->password : bcrypt($this->default);

        return $data;
    }

    private function getUser($id)
    {
        return User::find($id);
    }

    private function getUnidade($id)
    {
        return Unidade::find($id);
    }

    private function getCidadeId($cidade, $uf)
    {
        try {
            $estado = Estado::where('nome', 'like', $uf)->orWhere('uf', 'like', $uf)->first();
            $cidade = Cidade::where('nome', $cidade)->where('estado', $estado->id)->first();
            return ['cidade' => $cidade->id, 'estado' => $estado->id];
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    private function parseCidade($idCidade)
    {
        $cidade = Cidade::find($idCidade);
        $estado = Estado::find($cidade->estado);
        return ['cidade' => $cidade->nome, 'estado' => $estado->uf];
    }
}
