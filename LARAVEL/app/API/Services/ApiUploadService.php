<?php

namespace App\API\Services;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use League\Flysystem\Exception;

class ApiUploadService
{
    private $local = 'local';

    public function register(Request $request)
    {
        try {
            //$image = $request->file('file')->store('', 'avatars');
            $image1 = $request->file('file')->storeAs(
                '','user-' . $request->client . ".jpg",'avatars'
            );            
            return $image1;
        } catch (Exception $exception) {
            return response()->json($exception->getMessage(), $exception->getCode());
        }
    }

    public function updateUserAvatar($id, $fileAddress)
    {
        $user = User::find($id);

        $this->deleteFIle($user->avatar);
        $user->avatar = $fileAddress;
        return $user->save();
    }

    private function deleteFIle($file)
    {
        try {
            Storage::delete($file);
        } catch (\Exception $e) {
        }
    }
}
