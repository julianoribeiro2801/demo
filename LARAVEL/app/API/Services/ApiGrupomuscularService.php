<?php
namespace App\API\Services;

use App\Models\Grupomuscular;
use App\Models\Unidade;

use App\Services\GrupomuscularService;
use Carbon\Carbon;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use League\Flysystem\Exception;

class ApiGrupomuscularService
{
    private $model;
    private $default = 'prospect';
    private $request;
    private $unidade;
    private $usuario;
    private $crm;

    public function __construct(Grupomuscular $grupomuscular, Request $request)
    {
        $this->model = $grupomuscular;
        $this->request = $request;
        $this->unidade = $this->getUnidade($this->request->header('unidade'));
    }

    public function index()
    {
        $gruposmusculares= $this->model->where('id', '>', 0)->orderBy('nmgrupo', 'ASC')->get();
        return response()->json(compact('gruposmusculares'));
    }

    public function all()
    {
        $gruposmusculares = $this->model->where('id', '>', 0)->orderBy('nmgrupo', 'ASC')->get();
        return response()->json(compact('gruposmusculares'));
    }

    public function register(Request $request)
    {
        $acao = 1;
        try {
            $grupomuscular = $this->model->create($request->all());
            return response()->json($grupomuscular);
        } catch (Exception $exception) {
            return response()->json($exception->getMessage(), $exception->getCode());
        }
    }

    public function show($id)
    {
        $grupomuscular = $this->model->where('id', $id)->first();
        return response()->json($grupomuscular);
    }

    public function update(Request $request, $id)
    {
        try {
            $grupomuscular = $this->model->find($id)->update($request->all());
            return response()->json('Cadastro atualizado com sucesso!!!');
        } catch (Exception $e) {
            return response()->json($e->getMessage(), $e->getCode());
        }
    }

    public function data($id)
    {
        $client = $this->model->find($id)->with('dados');
    }

    public function destroy($id)
    {
        try {
            $grupomuscular = $this->model->find($id)->delete();
            return response()->json('Cadastro removido com sucesso!!!');
        } catch (Exception $e) {
            return response()->json($e->getMessage(), $e->getCode());
        }
    }

    private function formatUser(Request $request)
    {
        $data = $request->all();
        $data['role'] = $request->role ? $request->role : $this->default;
        $data['password'] = $request->password ? $request->password : bcrypt($this->default);
        return $data;
    }

    private function getUser($id)
    {
        return User::find($id);
    }

    private function getUnidade($id)
    {
        return Unidade::find($id);
    }

    private function getCidadeId($cidade, $uf)
    {
        try {
            $estado = Estado::where('nome', 'like', $uf)->orWhere('uf', 'like', $uf)->first();
            $cidade = Cidade::where('nome', $cidade)->where('estado', $estado->id)->first();
            return ['cidade' => $cidade->id, 'estado' => $estado->id];
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    private function parseCidade($idCidade)
    {
        $cidade = Cidade::find($idCidade);
        $estado = Estado::find($cidade->estado);
        return ['cidade' => $cidade->nome, 'estado' => $estado->uf];
    }
}
