<?php
namespace App\API\Services;

use App\Events\Prospect;
use App\Models\Crm;
use App\Models\User;
use Illuminate\Http\Request;
use League\Flysystem\Exception;

class CrmClientService
{
    private $model;

    public function __construct(Crm $crm)
    {
        $this->model = $crm;
    }

    public function index()
    {
        $data = $this->model->get();

        return response()->json($this->formatData($data));
    }

    public function show($id = null, $client = null)
    {
        $data = $this->model->where('para', $id)->get();
        return response()->json($this->formatData($data));
    }

    private function formatData($data)
    {
        $dataFormated = [];
        foreach ($data as $key => $dt) {
            $dataFormated = array_prepend($dataFormated, ['text' => $dt->getText(), 'date' => $dt->getDate()]);
        }
        return $dataFormated;
    }
}
