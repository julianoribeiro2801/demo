<?php
namespace App\API\Services;

use App\Models\Metahope;
use App\Models\Unidade;
use App\Services\ProjetoService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use League\Flysystem\Exception;

class ApiProjetoService
{
    private $model;
    private $default = 'prospect';
    private $request;
    private $unidade;
    private $usuario;
    private $crm;
    public function __construct(Metahope $projeto, Request $request)
    {
        $this->model = $projeto;
        $this->request = $request;
        $this->unidade = $this->getUnidade($this->request->header('unidade'));
        ///   $this->usuario = $this->getUser($this->request->header('user'));
    }

    public function index()
    {
        $projetos = $this->model->where('id', '>', 0)->orderBy('dsmeta', 'ASC')->get();

        return response()->json(compact('projetos'));
    }

    public function all()
    {
        $projetos = $this->model->where('id', '>', 0)->orderBy('dsmeta', 'ASC')->get();

        return response()->json(compact('projetos'));
    }
    public function register(Request $request)
    {
        $acao = 1;
        try {
            $projeto = $this->model->create($this->formatUser($request));
            $projeto->dados()->create($request->all());
            return response()->json($projeto);
        } catch (Exception $exception) {
            return response()->json($exception->getMessage(), $exception->getCode());
        }
    }

    public function show($id)
    {
        $projeto = $this->model->where('id', $id)->first();
        //$client->profiles = $this->profiles();

        //if ($client->dados && $client->dados->idcidade){
//            $client->dados->cidade = $this->parseCidade($client->dados->idcidade)['cidade'];
        //          $client->dados->estado = $this->parseCidade($client->dados->idcidade)['estado'];
        //    }

        return response()->json($projeto);
    }

    public function convert($id)
    {
        $acao = 2;
        try {
            $cliente = $this->model->find($id)->update(['role' => 'cliente']);
            $this->crm->register($this->usuario->id, $id, $acao, 'Converteu prospect em cliente');

            return response()->json($cliente);
        } catch (Exception $exception) {
            return response()->json($exception->getMessage(), 400);
        }
    }



    public function update(Request $request, $id)
    {
        try {
            $client = $this->model->find($id)->update($request->all());
            if ($this->model->find($id)->dados()->update($this->formatData($request))) {
                return response()->json('Cadastro atualizado com sucesso!!!');
            }
            $this->model->find($id)->dados()->create($this->formatData($request));
        } catch (Exception $e) {
            return response()->json($e->getMessage(), $e->getCode());
        }
    }

    public function data($id)
    {
        $client = $this->model->find($id)->with('dados');
    }

    public function delete()
    {
    }

    public function profiles()
    {
        $profiles = ProfileType::all();
        return $profiles;
    }
    private function formatUser(Request $request)
    {
        $data = $request->all();
        $data['role'] = $request->role ? $request->role : $this->default;
        $data['password'] = $request->password ? $request->password : bcrypt($this->default);

        return $data;
    }

    private function formatData(Request $request)
    {
        $data = collect($request->dados)->except('id', 'user_id', 'type', 'objetivo');

        $data['idunidade'] = $request->idunidade or 0;
        if (isset($request->dados['cidade'])) {
            $data['idcidade'] = $this->getCidadeId($data['cidade'], $data['estado'])['cidade'];
        }
        if (isset($request->dados['dt_nascimento']) && $request->dados['dt_nascimento']) {
            $data['dt_nascimento'] = Carbon::parse($request->dados['dt_nascimento']);
        }

        $data['profile_type'] = isset($data['profile_type']) ? $data['profile_type'] : 0 ;

        return $data->except('cidade', 'estado')->toArray();
    }

    private function getUser($id)
    {
        return User::find($id);
    }

    private function getUnidade($id)
    {
        return Unidade::find($id);
    }

    private function getCidadeId($cidade, $uf)
    {
        try {
            $estado = Estado::where('nome', 'like', $uf)->orWhere('uf', 'like', $uf)->first();
            $cidade = Cidade::where('nome', $cidade)->where('estado', $estado->id)->first();
            return ['cidade' => $cidade->id, 'estado' => $estado->id];
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    private function parseCidade($idCidade)
    {
        $cidade = Cidade::find($idCidade);
        $estado = Estado::find($cidade->estado);
        return ['cidade' => $cidade->nome, 'estado' => $estado->uf];
    }
}
