<?php
namespace App\API\Services;

use App\Models\Gfm;
use App\Models\Local;
use App\Models\Programa;
use App\Models\Diasemana;
use App\Models\User;
use App\Models\Unidade;
use App\Services\GfmService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

use App\Services\TrataDadosService;
use League\Flysystem\Exception;

class ApiGfmService
{
    private $model;
    private $default = 'prospect';
    private $request;
    private $unidade;
    private $usuario;
    private $crm;
    public function __construct(Gfm $gfm, Request $request, TrataDadosService $trataDadosService)
    {
        $this->trataDadosService = $trataDadosService;
        $this->model = $gfm;
        $this->request = $request;
        $this->unidade = $this->getUnidade($this->request->header('unidade'));
    }

    public function index()
    {
        $gfms = DB::select("SELECT coalesce(vw_mediagfm.total,0) as presencas,coalesce(vwt.numaulas,0) as numeroaulas,gfm.*,coalesce(vw_mediagfm.total,0) as total,"
                           . " ((coalesce(vw_mediagfm.total,0) * 100) / (gfm.qtcapacidade *  vwt.numaulas)) as perc,"
                           . " (coalesce(vw_mediagfm.total,0) / coalesce(vwt.numaulas,0)) as media,"
                           . " coalesce(((coalesce(vw_mediagfm.total,0) / coalesce(vwt.numaulas,0)) * gfm.vlfator),0) as revisado"
                           . " FROM gfm left join vw_mediagfm on gfm.id = vw_mediagfm.idgfm "
                           . " left join view_totalaulas as vwt on gfm.id = vwt.id");
        $locais = Local::select('id', 'nmlocal')->get();
        $selectedLocais = $this->trataDadosService->listToSelectProgramas($locais);

        $programas = Programa::select('id', 'nmprograma')->get();
        $selectedProgramas = $this->trataDadosService->listToSelectProgramas($programas);

        $professores = User::select('id', 'name')->get();
        $selectedProfessores = $this->trataDadosService->listToSelectProgramas($professores);

        $diassemana = Diasemana::select('id', 'nmdiasemana')->get();
        $selectedDiassemana = $this->trataDadosService->listToSelectProgramas($diassemana);
        return response()->json(compact('locais', 'programas', 'professores', 'gfms', 'diassemana', 'percgeral', 'mediaprof'));
    }

    public function grafico($dias)
    {
        $dias = 50;
        $dataatual = date('Y-m-d');
        $dataini = date('Y-m-d', strtotime('-' . $dias . ' days'));
        $audiencia=DB::select("select coalesce(sum(r.qtcapacidade),0) as totcap, coalesce(sum(r.qtalunopresente),0) as totaluno "
                             . " from resultadogfm as r "
                             . " where r.dtaula between '" . $dataini . "'  and '" . $dataatual . "'"
                             . " and r.idunidade = 1");

        $teste=[28, 48, 40, 25, 50, 25, 13];
        $capacidade=[45, 50, 50, 55, 50, 45, 60];

        return response()->json(compact('audiencia', 'capacidade', 'teste'));
    }

    public function indicadores($dias)
    {
        $dataatual = date('Y-m-d');
        $dataini = date('Y-m-d', strtotime('-' . $dias . ' days'));

        $mediaprof=DB::select("select coalesce(sum(r.qtcapacidade),0) as totcap, coalesce(sum(r.qtalunopresente),0) as totaluno ,"
                             . " u.id, u.idunidade, u.name , coalesce((coalesce(sum(r.qtalunopresente),0) / (select count(rg.id) from resultadogfm rg"
                             . " where rg.idfuncionario = u.id  )),0) as media, u.id from users as u left join resultadogfm as r on u.id = r.idfuncionario"
                             . " and r.dtaula between '" . $dataini . "'  and '" . $dataatual . "'"
                             . " and u.idunidade = 1"
                             . " group by u.id, u.idunidade, u.name order by media desc");
        $percgeral=DB::select("select coalesce(sum(r.qtcapacidade),0) as totcap, coalesce(sum(r.qtalunopresente),0) as totaluno ,"
                             . " u.idunidade, coalesce(((coalesce(sum(r.qtalunopresente),0) * 100)  / (coalesce(sum(r.qtcapacidade),0)) ),0) as perc "
                             . " from users as u left join resultadogfm as r on u.id = r.idfuncionario"
                             . " and r.dtaula between '" . $dataini . "'  and '" . $dataatual . "'"
                             . " and u.idunidade = 1"
                             . " group by u.idunidade");
        return response()->json(compact('percgeral', 'mediaprof'));
    }

    public function all()
    {
        $gfms = DB::select("SELECT gfm.*,coalesce(vw_mediagfm.total,0) as total FROM gfm left join vw_mediagfm on gfm.id = vw_mediagfm.idgfm");
        $indicadores=DB::select("select vwt.numaulas,(vwt.numaulas * gfm.qtdcapacidade) as captotal from gfm , join view_totalaulas as vwt where vwt.idgfm = gfm.id ");
        return response()->json(compact('gfms', 'indicadores'));
    }

    public function register(Request $request)
    {
        try {
            $gfm = $this->model->create($this->formatUser($request));
            return response()->json($gfm);
        } catch (Exception $exception) {
            return response()->json($exception->getMessage(), $exception->getCode());
        }
    }

    private function insereId(Request $request, $id)
    {
        $data = $request->all();
        $data['idexercicioanimacao'] = $request->idexercicioanimacao ? $request->idexercicioanimacao : $id;
        $data['dsurlminiatura'] = $request->dsurlminiatura ? $request->dsurlminiaturao : '';
        return $data;
    }

    public function show($id)
    {
        $gfm = $this->model->where('id', $id)->first();
        return response()->json($gfm);
    }

    public function update(Request $request, $id)
    {
        try {
            $client = $this->model->find($id)->update($request->all());
            return response()->json('Cadastro atualizado com sucesso!!!');
        } catch (Exception $e) {
            return response()->json($e->getMessage(), $e->getCode());
        }
    }

    public function data($id)
    {
        $client = $this->model->find($id)->with('dados');
    }

    public function destroy($id)
    {
        try {
            $exercicio = $this->model->find($id)->delete();
            return response()->json('Cadastro removido com sucesso!!!');
        } catch (Exception $e) {
            return response()->json($e->getMessage(), $e->getCode());
        }
    }

    public function profiles()
    {
        $profiles = ProfileType::all();
        return $profiles;
    }

    private function formatUser(Request $request)
    {
        $data = $request->all();
        return $data;
    }

    private function formatData(Request $request)
    {
        $data = collect($request->dados)->except('id', 'user_id', 'type', 'objetivo');
        $data['idunidade'] = $request->idunidade or 0;
        if (isset($request->dados['cidade'])) {
            $data['idcidade'] = $this->getCidadeId($data['cidade'], $data['estado'])['cidade'];
        }
        if (isset($request->dados['dt_nascimento']) && $request->dados['dt_nascimento']) {
            $data['dt_nascimento'] = Carbon::parse($request->dados['dt_nascimento']);
        }
        $data['profile_type'] = isset($data['profile_type']) ? $data['profile_type'] : 0;
        return $data->except('cidade', 'estado')->toArray();
    }

    public function listToSelectProgramas($programas)
    {
        foreach ($programas as $programa) {
            $selectedProgramas[$programa->id] = $programa->nmprograma;
        }
        return $selectedProgramas;
    }

    private function getUser($id)
    {
        return User::find($id);
    }

    private function getUnidade($id)
    {
        return Unidade::find($id);
    }

    private function getCidadeId($cidade, $uf)
    {
        try {
            $estado = Estado::where('nome', 'like', $uf)->orWhere('uf', 'like', $uf)->first();
            $cidade = Cidade::where('nome', $cidade)->where('estado', $estado->id)->first();
            return ['cidade' => $cidade->id, 'estado' => $estado->id];
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    private function parseCidade($idCidade)
    {
        $cidade = Cidade::find($idCidade);
        $estado = Estado::find($cidade->estado);
        return ['cidade' => $cidade->nome, 'estado' => $estado->uf];
    }
}
