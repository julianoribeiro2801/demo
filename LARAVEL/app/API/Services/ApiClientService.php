<?php
namespace App\API\Services;


use App\Models\Cidade;
use App\Models\Estado;
use App\Models\ProfileType;
use App\Models\User;
use App\Models\UserDados;
use App\Models\Configura;
use App\Models\Logcliente;

use App\Notifications\ClienteConverted;
use App\Services\CrmService;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\HttpException;
use Illuminate\Http\Request;
use League\Flysystem\Exception;
use Storage;
class ApiClientService {

    private $model;
    private $default = 'prospect';
    private $request;
    private $unidade;
    private $usuario;
    private $crm;

    public function __construct(User $cliente,UserDados $dados, Request $request, CrmService $crm)
    {
        $this->model = $cliente;
        $this->dados = $dados;
        $this->request = $request;
        $this->unidade = $request->header('unidade');
        $this->usuario = $this->request->header('user');
        $this->crm = $crm;
    }

    public function index(){
        $persons = $this->model->whereIn('role', ['cliente', 'prospect'])->where('idunidade', $this->unidade)->where('excluido', 'N')->orderBy('id', 'DESC')->with('dados')->paginate(50);
        foreach ($persons as $key => $value) {
            
            $persons[$key]['avatar']=$this->verificarArquivo($persons[$key]['avatar']);
            
            $persons[$key]['name'] = ucwords(strtolower($persons[$key]['name']));
        }
        
        return response()->json(compact('persons'));
    }
    function verificarArquivo($name_img) {


        $diskimagem = Storage::disk('avatars');

        // SE A FOTO EXISTIR COLOCO
        if ($diskimagem->exists($name_img)) {
            return $name_img;
        } else {
            return "user-a4.jpg";
        }
    }    
    

    public function search($q)
    {
        if (Configura::where('idunidade',  $this->unidade)->first()) {
            $configura = Configura::where('idunidade',  $this->unidade)->update(['stadiciona' => 'S']);
        } else {
            $configura = Configura::create(['idunidade' => $this->unidade,
                        'stadiciona' => 'S'
            ]);
        };
        
        $persons = $this->model->newQuery()
            ->whereIn('role',['cliente', 'prospect'])
            ->where('idunidade', $this->unidade)
            ->where('excluido', 'N')
            ->with('dados')
            ->where(function($query) use ($q) {
                $query->orWhere('name', 'like', "%{$q}%")
                ->orWhere('email', 'like', "%{$q}%");
        })->orderBy('id', 'DESC')->paginate(50);
        
        foreach ($persons as $key => $value) {
            
            $persons[$key]['avatar']=$this->verificarArquivo($persons[$key]['avatar']);

            $persons[$key]['name'] = ucwords(strtolower($persons[$key]['name']));
        }
        
       return response()->json(compact('persons'));
    }

    public function register(Request $request)
    {
        $data = $request->all();

        $verificaemail = User::select('users.*','user_dados.genero')->JOIN('user_dados', 'user_dados.user_id', '=', 'users.id')
        ->where('users.email', $data['email'])->where('users.idunidade',$this->unidade)->first();

        if (count($verificaemail) > 0) {
            $retorno['cod']='-1';
            $retorno['text']="Email já existente";
            $retorno['client'] = $verificaemail ;
            
            
            return $retorno;
        } else {
        
        try{
            
        
            $cliente = $this->model->create($this->formatUser($request));
            $cliente->dados()->create($request->all());

            $this->crm->register($this->usuario, $cliente->id, 1, 'Cadastrou um novo prospect');

            return response()->json($cliente);
        }catch (Exception $exception){
            return response()->json($exception->getMessage(), $exception->getCode());
        }
      }  
       //return $client;

    }

    public function show($id){
        
        
        if (Configura::where('idunidade',  $this->unidade)->first()) {
            $configura = Configura::where('idunidade',  $this->unidade)->update(['codigo_atualiza' => $id]);
        } else {
            $configura = Configura::create(['idunidade' => $this->unidade,
                'codigo_atualiza' => $id
            ]);
        };        
        
        
        
        $client = $this->model->where('id', $id)->with('dados')->first();
        $client->profiles = $this->profiles();

        if ($client->dados && $client->dados->idcidade){
            $client->dados->cidade = $this->parseCidade($client->dados->idcidade)['cidade'];
            $client->dados->estado = $this->parseCidade($client->dados->idcidade)['estado'];
            
        }
        
        $client->dados->dt_nascimento= $this->getData($client->dados->dt_nascimento);
       // echo $datax;
        return response()->json($client);
    }

     public function convertDonoAcademia($id)
    {
        $client = $this->model->findOrFail($id);
        $client->notify(new ClienteConverted());
    }

    public function convert($id,$motivo)
    {
        $client = $this->model->findOrFail($id);
        $datos = $this->dados->where('user_id', $id)->firstOrFail();
        $datos['motivo']=$motivo;
        
                abort_unless($client->dados->genero, 400, 'Ante de converter, é obrigatório informar um genero');

        try{

            switch ($client->role) {
                case 'cliente':
                    $this->convertToOportunit($client,$datos);
                    break;
                case 'prospect':
                case 'oportunidade':
                    $this->convertToClient($client,$datos);
                    break;
                default:
                    $this->convertToClient($client,$datos);
                    break;
            }

            return response()->json($client);
        }catch (Exception $exception){
            return response()->json($exception->getMessage(), 400);
        }
    }

    private function convertToOportunit(User $client,UserDados $dados)
    {
        $client->update(['role' => 'prospect']);
        $dados->update(['situacaomatricula' => 'ENCERRADA','motivo' => $dados['motivo']]);
        $this->crm->register($this->usuario, $client->id, 2, 'Converteu o cliente em uma oportunidade');
        
        ////registra log
       /* $log = Logcliente::create(['idunidade' => $client->idunidade,
                'idaluno' => $client->id,
                'idprofessor' => 0,
                'dtalteracao' => date('Y-m-d'),
                'situacao' => 'ENCERRADA',
                'motivo' => 'dd'
            
        ]);*/
        
        $client->notify(new ClienteConverted());
    }

    private function convertToClient(User $client, UserDados $dados)
    {
        $client->update(['role' => 'cliente']);
        $dados->update(['situacaomatricula' => 'ATIVA']);
        $this->crm->register($this->usuario, $client->id, 2, 'Converteu oportunidade em cliente');
        $client->notify(new ClienteConverted());
    }

    public function update(Request $request, $id) {


        $verificaemail = User::select('users.*','user_dados.genero')
        ->JOIN('user_dados', 'user_dados.user_id', '=', 'users.id')
        ->where('users.email', $request->email)->where('users.idunidade',$this->unidade)->first();

        $valida = 0;

        if (count($verificaemail) > 0) {

            if ($id == $verificaemail->id) {
                $retorno['cod'] = '-2';
                $retorno['text'] = "Email não existente " . $verificaemail->id;   
                $retorno['client'] = $verificaemail ;             
                $valida = 0;
            } else {

                $valida = 1;
                $retorno['cod'] = '-1';
                $retorno['text'] = "Email já existente " ;
                $retorno['client'] = $verificaemail ; 

                return $retorno;
            }

        } 

        if ($valida == 0) {
            try {

                $client = $this->model->find($id)->update($request->only('idunidade', 'name', 'email', 'role', 'avatar'));

                if ($this->model->find($id)->dados()->update($this->formatData($request))) {
                    $client = $this->model->where('id', $id)->with('dados')->first();
                    $client->profiles = $this->profiles();

                    if ($client->dados && $client->dados->idcidade) {
                        $client->dados->cidade = $this->parseCidade($client->dados->idcidade)['cidade'];
                        $client->dados->estado = $this->parseCidade($client->dados->idcidade)['estado'];
                    }
                    $client->dados->dt_nascimento = $this->getData($client->dados->dt_nascimento);
                    return response()->json($client);
                }

                $this->model->find($id)->dados()->create($this->formatData($request));
            } catch (Exception $e) {
                return response()->json($e->getMessage(), $e->getCode());
            }
        } else {
            return $retorno;
        }
    }

    public function atualizar( $id){

        
        if (Configura::where('idunidade',  $this->unidade)->first()) {
            $configura = Configura::where('idunidade',  $this->unidade)->update(['codigo_atualiza' => $id]);
        } else {
            $configura = Configura::create(['idunidade' => $this->unidade,
                'codigo_atualiza' => $id
            ]);
        };

    }
    public function data($id)
    {
        $client = $this->model->find($id)->with('dados');
    }

    public function deleteCli($id){
        try {
            $cliente = $this->model->find($id)->delete();
            // if ($this->model->find($id)->update($this->formatData($request))){
            return response()->json('Cadastro removido com sucesso!!!');
            //}
        } catch (Exception $e) {
            return response()->json($e->getMessage(), $e->getCode());
        }        

    }

    public function profiles()
    {
        $profiles = ProfileType::all();
        return $profiles;
    }
    private function formatUser(Request $request)
    {
     
        $data = $request->all();
        $data['name'] = ucwords(strtolower($request->name));
        $data['role'] = $request->role ? $request->role : $this->default;
        $data['password'] = $request->password ? $request->password : $this->default;
        $data['idunidade'] = $this->unidade;
        return $data;
    }

    private function formatData(Request $request)
    {
        $data = collect($request->dados)->except('id','user_id','type','objective', 'objetivo');

        $data['idunidade'] = $this->unidade;

        if(isset($request->dados['cidade']) && isset($request->dados['estado'])){
            $data['idcidade'] = $this->getCidadeId($data['cidade'], $data['estado'])['cidade'];
        }
        if(isset($request->dados['dt_nascimento']) && $request->dados['dt_nascimento']){
            $data['dt_nascimento'] = $this->setData($request->dados['dt_nascimento']); //Carbon::parse($request->dados['dt_nascimento']);
        }

        $data['profile_type'] = isset($data['profile_type']) ? $data['profile_type'] : 0 ;

        return $data->except('cidade','estado')->toArray();
    }

    private function getCidadeId($cidade, $uf)
    {
        try{
            $estado = Estado::where('nome','like',$uf)->orWhere('uf', 'like', $uf)->first();
            $cidade = Cidade::where('nome', $cidade)->where('estado', $estado->id)->first();
            return ['cidade' => $cidade->id, 'estado' => $estado->id];
        }catch (Exception $e){
            return $e->getMessage();
        }
    }
    function setData($Data) {
		$Format = explode(' ', $Data);
		$Data = explode('/', $Format[0]);
		$Data = $Data[2] . '-' . $Data[1] . '-' . $Data[0];
		return $Data;
    }
    function getData($Data) {
        if ($Data != NULL):
            $Format = explode(' ', $Data);
            $Data = explode('-', $Format[0]);
            $Data = $Data[2] . '/' . $Data[1] . '/' . $Data[0];
            return $Data;        
        endif;    
	/*if ($Data != NULL):
		$data = date("d/m/Y", strtotime($Data));
		return $data;
	endif;*/
    }      
    private function parseCidade($idCidade)
    {
        $cidade = Cidade::find($idCidade);
        $estado = Estado::find($cidade->estado);
        return ['cidade' => $cidade->nome, 'estado' => $estado->uf];
    }
}