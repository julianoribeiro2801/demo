<?php
namespace App\API\Services;

use App\Models\Cidade;
use App\Models\CrmAcao;
use App\Models\Objetivotreino;
use App\Models\ProfileType;
use Mockery\Exception;

class CoreApiService
{
    public function __construct()
    {
    }

    public function index()
    {
        try {
            $objectives = Objetivotreino::all();
            $profiles = ProfileType::all();
            $crm_acoes = CrmAcao::where('status', "1")->orderBy('titulo')->get();

            return response()->json(compact('objectives', 'profiles', 'crm_acoes'));
        } catch (Exception $e) {
            return response()->json($e->getMessage(), $e->getCode());
        }
    }
}
