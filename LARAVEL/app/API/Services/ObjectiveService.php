<?php
namespace App\API\Services;

use App\Models\Objetivotreino;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class ObjectiveService
{
    public function register(Request $request)
    {
        $data = [];
        $data['nmobjetivo'] = $request->input('nmobjetivo');
//        $data['idunidade'] = $request->header('unidade',1);

        try {
            $objective = Objetivotreino::create($data);
            return response()->json($objective);
        } catch (Exception $exception) {
            return response()->json(['error' => $exception->getMessage()], $exception->getCode());
        }
    }
}
