<?php
namespace App\API\Services;

use App\Models\Exercicio;
use App\Models\Exercicioanimacao;
use App\Models\Grupomuscular;
use App\Models\Unidade;
use App\Models\TreinoFicha;

use App\Services\ExercicioService;
use Carbon\Carbon;
use File;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

use App\Services\TrataDadosService;
use League\Flysystem\Exception;

class ApiExercicioService
{
    private $model;
    private $default = 'prospect';
    private $request;
    private $unidade;
    private $usuario;
    private $crm;
    
    public function __construct(Exercicio $exercicio, Exercicioanimacao $animacao, Request $request, TrataDadosService $trataDadosService)
    {
        $this->model1 = $animacao;
        $this->trataDadosService = $trataDadosService;
        $this->model = $exercicio;
        $this->request = $request;
        $this->unidade = $this->getUnidade($this->request->header('unidade'));
    }

    public function index()
    {
        $exercicios = DB::select("select e.id, e.nmexercicio, e.idgrupomuscular, e.dsurlminiatura, g.nmgrupo from exercicio e, grupomuscular g where e.idgrupomuscular = g.id order by e.nmexercicio asc");
        $gruposmusculares = Grupomuscular::select('id', 'nmgrupo')->get();
        $selectedGruposmusculares = $this->trataDadosService->listToSelectProgramas($gruposmusculares);
        return response()->json(compact('exercicios', 'gruposmusculares'));
    }

    public function all()
    {
        $exercicios = DB::select("select e.id, e.nmexercicio, e.idgrupomuscular, e.dsurlminiatura, g.nmgrupo from exercicio e, grupomuscular g where e.idgrupomuscular = g.id order by e.nmexercicio asc");
        return response()->json(compact('exercicios'));
    }

    public function register(Request $request)
    {
        try {
            $animacao = $this->model1->create($this->formatUser($request));
            $exercicio = $this->model->create($this->insereId($request, $animacao->id));
            return response()->json($exercicio);
        } catch (Exception $exception) {
            return response()->json($exception->getMessage(), $exception->getCode());
        }
    }

    private function insereId(Request $request, $id)
    {
        $data = $request->all();
        $data['idexercicioanimacao'] = $request->idexercicioanimacao ? $request->idexercicioanimacao : $id;
        $data['dsurlminiatura'] = $request->dsurlminiatura ? $request->dsurlminiaturao : '';
        return $data;
    }

    public function show($id)
    {
        $exercicio = $this->model->where('id', $id)->first();
        return response()->json($exercicio);
    }

    public function update(Request $request, $id)
    {
        try {
            $client = $this->model->find($id)->update($request->all());
            return response()->json('Cadastro atualizado com sucesso!!!');
        } catch (Exception $e) {
            return response()->json($e->getMessage(), $e->getCode());
        }
    }

    public function data($id)
    {
        $client = $this->model->find($id)->with('dados');
    }

    public function destroy($id)
    {
        try {
            $treino = TreinoFicha::where('exercicio_id', $id)->get();
            $contTreino = $treino->count();
            if ($contTreino > 0) {
                return false;
            } else {
                $exercicio = new Exercicio();
                $dataExercicio = $exercicio->select('dsurlminiatura', 'video')->where('id', $id)->get();
                $imagemAtual = 'uploads/exercicios/' . $dataExercicio[0]['dsurlminiatura'];
                $videoAtual = 'uploads/exercicios/' . $dataExercicio[0]['video'];
                File::delete($imagemAtual);
                File::delete($videoAtual);

                $exercicio = $this->model->find($id)->delete();
                return response()->json('Cadastro removido com sucesso!!!');
            }
        } catch (Exception $e) {
            return response()->json($e->getMessage(), $e->getCode());
        }
    }

    public function profiles()
    {
        $profiles = ProfileType::all();
        return $profiles;
    }
    
    private function formatUser(Request $request)
    {
        $data = $request->all();
        return $data;
    }

    private function formatData(Request $request)
    {
        $data = collect($request->dados)->except('id', 'user_id', 'type', 'objetivo');

        $data['idunidade'] = $request->idunidade or 0;
        if (isset($request->dados['cidade'])) {
            $data['idcidade'] = $this->getCidadeId($data['cidade'], $data['estado'])['cidade'];
        }
        if (isset($request->dados['dt_nascimento']) && $request->dados['dt_nascimento']) {
            $data['dt_nascimento'] = Carbon::parse($request->dados['dt_nascimento']);
        }

        $data['profile_type'] = isset($data['profile_type']) ? $data['profile_type'] : 0 ;
        return $data->except('cidade', 'estado')->toArray();
    }

    public function listToSelectProgramas($programas)
    {
        foreach ($programas as $programa) {
            $selectedProgramas[$programa->id] = $programa->nmprograma;
        }
        return $selectedProgramas;
    }
    
    private function getUser($id)
    {
        return User::find($id);
    }

    private function getUnidade($id)
    {
        return Unidade::find($id);
    }

    private function getCidadeId($cidade, $uf)
    {
        try {
            $estado = Estado::where('nome', 'like', $uf)->orWhere('uf', 'like', $uf)->first();
            $cidade = Cidade::where('nome', $cidade)->where('estado', $estado->id)->first();
            return ['cidade' => $cidade->id, 'estado' => $estado->id];
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    private function parseCidade($idCidade)
    {
        $cidade = Cidade::find($idCidade);
        $estado = Estado::find($cidade->estado);
        return ['cidade' => $cidade->nome, 'estado' => $estado->uf];
    }
}
