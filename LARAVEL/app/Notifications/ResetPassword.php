<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\Models\User;

class ResetPassword extends Notification
{
    use Queueable;

    public $token;


    public function __construct($token)
    {
        $this->token = $token;
    }


    public function via($notifiable)
    {
        return ['mail'];
    }


    public function toMail($notifiable)
    {
        // dd($notifiable->toArray());
        $user = User::where('id', $notifiable->id)->update(['remember_token' => $this->token]);

        // $idunidade = User::select('idunidade')->where('id', $notifiable->id)->first();

        // $academia = Unidade::select('unidade.fantasia', 'unidade_dados.email')
        //         ->where('unidade.id', $idunidade)
        //         ->first();


        return (new MailMessage)
             ->from(config('mail.from.address'), $notifiable->unidade->fantasia)
             ->replyTo($notifiable->unidade->dados->email, $notifiable->unidade->fantasia)
            ->subject('Recuperar senha')
            ->line('Você está recebendo este email por que nós recebemos uma solicitação para recuperar sua senha.')
            ->action('Recuperar senha', url('/novo-usuario?hash='. $this->token))
            ->line('Caso você não tenha solicitado uma nova senha, desconsidere este email.');

            //  ->action('Recuperar senha', $this->token)
    }
}
