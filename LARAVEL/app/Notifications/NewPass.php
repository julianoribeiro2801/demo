<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NewPass extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    private $args;
    public function __construct($args)
    {
        $this->args = $args;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }


    public function toMail($user)
    {
        return (new MailMessage)
            ->from(config('mail.from.address'), $user->unidade->fantasia)
            ->subject('Atualize sua senha')
            ->replyTo($user->unidade->dados->email, $user->unidade->fantasia)
            ->greeting('Olá '. $user->name)
            ->line('Para ter acesso so sistema, você deve cadastrar uma nova senha')
            ->action('Cadastrar nova senha', url('/novo-usuario?hash='.$user->remember_token));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
