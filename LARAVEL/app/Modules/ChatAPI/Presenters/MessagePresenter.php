<?php
namespace App\Modules\ChatAPI\Presenters;

use App\Modules\ChatAPI\Transformers\MessageTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

class MessagePresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new MessageTransformer();
    }
}
