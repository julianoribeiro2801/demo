<?php
namespace App\Modules\ChatAPI\Entities;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Traits\PresentableTrait;

class Message extends Model
{
    use PresentableTrait;

    protected $fillable = [
        'sender_id',
        'receiver_id',
        'message',
        'active',
        'opened',
        'push_types',
        'reserva_id',
        'quando',
        'hr_aviso'
    ];
    

    protected $casts = [
        'active' => 'boolean',
        'opened' => 'boolean'
    ];

    public $timestamps = true;

    public function sender()
    {
        return $this->hasOne(User::class, 'sender_id');
    }

    public function receiver()
    {
        return $this->hasOne(User::class, 'receiver_id');
    }
}
