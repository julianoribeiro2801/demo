<?php

Route::get('', function() {
   return 'API de chat';
});

Route::get('unity', 'UnityController@index');
Route::get('unity/teachers', 'UnityController@teachers');
Route::get('unity/allUser/{allUser}', 'UnityController@allUser');
Route::get('unity/totalmsg', 'UnityController@totalmsg');
Route::get('unity/atualizarListaChat/{sender}/{unidade}', 'UnityController@atualizarListaChat');

Route::get('person/{person}/messages', 'PersonController@messages');
Route::get('person/{person}/messagesAll', 'PersonController@messagesAll');
Route::get('person/{person}/messagesAllCRM', 'PersonController@messagesAllCRM');

Route::get('person/{person}/messagesApp', 'PersonController@messagesApp');

Route::get('person/{person}/news', 'PersonController@news');
Route::get('person', 'PersonController@index');

Route::post('messages', 'ChatController@store');
Route::get('searchMessages/{quantidade}', 'ChatController@searchMessages');
//Route::put('messages/read', 'ChatController@setRead');


Route::post('messages/setRead', 'ChatController@setRead');
//Route::post('messages/setRead', 'ChatController@setReadAll');

