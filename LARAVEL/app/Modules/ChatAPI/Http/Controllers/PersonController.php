<?php
namespace App\Modules\ChatAPI\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\ChatAPI\Repositories\Contracts\MessageRepository;
use Illuminate\Http\Request;
use App\Modules\ChatAPI\Entities\Message;
use Illuminate\Support\Facades\DB;

 use App\Models\CrmContato;

class PersonController extends Controller
{
    private $unity;
    private $user;

    private $messageRepository;

    public function __construct(Request $request, MessageRepository $messageRepository)
    {
        $this->unity = $request->header('unity');
        $this->user = $request->header('user');
        $this->messageRepository = $messageRepository;
    }
    
    public function show($id)
    {

    }

    public function messages($id)
    {
        
      //  return response()->json(compact('respostax'));
       $sql = "select p.id as push_type,p.quando as title,b.name as receiver_name, a.name as sender_name,m.confirmado, m.message, m.id, m.created_at,m.updated_at, m.sender_id, m.opened,"
                . " m.receiver_id,         m.reserva_id,         m.hr_aviso from messages m "
                . " left join push_types p on m.push_types = p.id left join users a on a.id = m.sender_id "
                . " left join users b on a.id = m.receiver_id 
            where (m.receiver_id = $this->user And m.sender_id = $id) OR
            (m.sender_id = $this->user And m.receiver_id = $id) order by m.created_at asc";
        $resposta = DB::select($sql);

        foreach ($resposta as $key => $value) {     
            $dts=date('d/m/Y H:i:s', strtotime( $resposta[$key]->created_at));
            $resposta[$key]->updated_at=date('d/m - H:i', strtotime( $resposta[$key]->updated_at));
            $resposta[$key]->created_at=date('d/m - H:i', strtotime( $resposta[$key]->created_at));
        }
        
        
        return response()->json(compact('resposta'), 200);  

    }
    
    

    public function messagesAllCRM($id)
    {
       
      

       $sql = " SELECT 
                 crm.id ,
                 crm.idunidade,
                 crm.iduser as receiver_id, 
                 crm.sender_id, 
                 crm_acao.descricao as acao,
                 crm.assunto as title,
                 crm.efetuado,
                 crm.reagendar,
                 crm.hot, 
                 crm.observacao as message,
                 crm.created_at , 
                 crm.updated_at,
                 users.name as sender_name
                FROM crm_contato as crm
                left join users  on users.id = crm.sender_id 
                                left join crm_acao  on crm.crmacao = crm_acao.id 
                WHERE iduser = $id";

       $crm = DB::select($sql);


       $sql = "select p.id as push_type,p.quando as title,b.name as receiver_name, a.name as sender_name,m.confirmado, m.message, m.id, m.created_at,m.updated_at, m.sender_id, m.opened,"
                . " m.receiver_id,         m.reserva_id,         m.hr_aviso from messages m "
                . " left join push_types p on m.push_types = p.id left join users a on a.id = m.sender_id "
                . " left join users b on a.id = m.receiver_id 
            where  m.receiver_id = $id  order by m.created_at desc";   

        $chat = DB::select($sql);


        $resposta = array_merge($crm, $chat);

        $resposta = collect($resposta)->sortBy('created_at')->toArray();

        foreach ($resposta as $key => $value) {     
            $dts=date('d/m/Y H:i:s', strtotime( $resposta[$key]->created_at));
            $resposta[$key]->created_at=date('d/m/Y - H:i', strtotime( $resposta[$key]->created_at));
        }        


        return response()->json(compact('resposta'), 200);  

    }    

    
    public function messagesAll($id)
    {
       
       $sql = "select vw.dtultima, p.id as push_type,p.quando as title,b.name as receiver_name, a.name as sender_name,m.confirmado, m.message, m.id, m.created_at,m.updated_at, m.sender_id, m.opened,"
                . " m.receiver_id,         m.reserva_id,         m.hr_aviso from messages m  "
                . " left join push_types p on m.push_types = p.id left join users a on a.id = m.sender_id "
                . " left join users b on b.id = m.receiver_id "
                . " left join vw_ultima_msg vw on vw.sender_id = b.id  and vw.sender_id = $id"    
                . " where  m.receiver_id = $id  order by vw.dtultima asc";   

       

        $resposta = DB::select($sql);
        
        foreach ($resposta as $key => $value) {     
            $dts=date('d/m/Y H:i:s', strtotime( $resposta[$key]->created_at));
            $resposta[$key]->created_at=date('d/m - H:i', strtotime( $resposta[$key]->created_at));
        }        

        return response()->json(compact('resposta'), 200);  

    }    

      public function messagesApp($id) {



        $sql = "select p.id as push_type,p.quando as title, a.name,m.confirmado, m.message, m.id, m.created_at,m.updated_at, m.sender_id, m.opened,"
                . " m.receiver_id,         m.reserva_id,         m.hr_aviso from messages m "
                . " left join push_types p on m.push_types = p.id left join users a on a.id = m.sender_id "
                . " left join users b on a.id = m.receiver_id 
            where (m.receiver_id = $this->user And m.sender_id = $id) OR
            (m.sender_id = $this->user And m.receiver_id = $id)  order by m.created_at ";   

        $resposta = DB::select($sql);

        return response()->json(compact('resposta'), 200);
    }

    public function news($id)
    {
        return response()->json($this->messageRepository->findNewsTalkByPeople($this->user, $id), 200);
    }
}
