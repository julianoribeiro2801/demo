<?php
namespace App\Modules\ChatAPI\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\ChatAPI\Repositories\Contracts\MessageRepository;
use Illuminate\Http\Request;

use App\Models\User;
use App\Modules\ChatAPI\Entities\Message;

use DB;

use Storage;
use App\Services\ChatService;

class ChatController extends Controller
{
    private $unity;
    private $user;
    /**
     * @var MessageRepository
     */
    private $messageRepository;

    public function __construct(Request $request, MessageRepository $messageRepository,ChatService $chatService)
    {
        setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
        date_default_timezone_set('America/Sao_Paulo');
        $this->unity = $request->header('unity');
        $this->user = $request->header('user');
        $this->messageRepository = $messageRepository;
        $this->chatService = $chatService;
    }

    public function store(Request $request)
    {
        return response()->json($this->messageRepository->storeMessage($this->user, $request->input('person'), $request->input('message')), 200);
    }

    public function setRead(Request $request)
    {
        
       /* $sql="update messages set opened = true ";
        $sql .= " where receiver_id = " . $this->user;
        $sql .= " and sender_id = " .  $request->person;       
        
        
        $update = DB::update($sql);*/
        
        $update = Message::where('sender_id', $request->person)
        ->where('receiver_id',$this->user) 
        ->update(['opened' => true]);
        return  ['status', 'success' ];
             
        
        //return response()->json($this->messageRepository->setRead($request->input('message')));
    }

    public function setReadAll(Request $request)
    {   

        $update = Message::where('sender_id', $request->person)
        ->where('receiver_id',$this->user) 
        ->update(['opened' => true]);
         return  ['status', 'success'];
        
    }

    public function searchMessages($quantidade) {

        $sql = "select count(m.id) as naolidas from messages m where m.receiver_id = " . $this->user;

        $total =DB::select($sql);

        $total[0]->quant=$quantidade;
        

        
        return $total;

    }   
    
    
      function verificarArquivo($name_img) {


        $diskimagem = Storage::disk('avatars');

        // SE A FOTO EXISTIR COLOCO
        if ($diskimagem->exists($name_img)) {
            return $name_img;
        } else { 
            return "user-a4.jpg";
        }
    }   

}
