<?php

namespace App\Modules\ChatAPI\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Modules\ChatAPI\Repositories\Contracts\UnityRepository;
use App\Modules\ChatAPI\Transformers\UnityTransformer;
use Illuminate\Http\Request;
use Fractal;
use DB;
use Storage;
use App\Services\ChatService;


class UnityController extends Controller {

    private $unity;
    private $user;
    private $unityRepository;
    public function __construct(Request $request, UnityRepository $unityRepository,ChatService $chatService) {
        $this->unity = $request->header('unity');
        $this->user = $request->header('user');
        $this->unityRepository = $unityRepository;
        $this->chatService = $chatService;

    }

    public function index() {

        
        $sql= "select vw.dtultima, u.excluido,u.id, u.name ,u.avatar from users u left join
                vw_ultima_msg as vw on vw.sender_id = u.id
                and vw.receiver_id = $this->user
                where u.excluido not in('S')
                and u.idunidade = $this->unity
                and u.role ='cliente' order by vw.dtultima desc";
        
        $unity = DB::select($sql);
        foreach ($unity as $key => $value) {

            $sql1="select count(*) as tt from messages where receiver_id =  $this->user and opened = 0 and sender_id = " .  $unity[$key]->id ;
            
            $unity1 = DB::select($sql1);
            $unity[$key]->naolidas=$unity1[0]->tt;

            $unity[$key]->name = ucwords(strtolower($unity[$key]->name));
            $unity[$key]->avatar = $this->chatService->verificarArquivo($unity[$key]->avatar);
            $unity[$key]->dtultima = date('d/m/Y H:i:s', strtotime($unity[$key]->dtultima)) ;

        }


        return response()->json(compact('unity'));


        //return response()->json(Fractal::collection($unidade)->transformWith(new UnityTransformer()));
    }
    
    public function atualizarListaChat($receiver, $unidade){
        
        $sql= "select vw.dtultima, u.excluido,u.id, u.name ,u.avatar from users u left join
                vw_ultima_msg as vw on vw.sender_id = u.id
                and vw.receiver_id = $receiver
                where u.excluido not in('S')
                and u.idunidade =  $unidade
                and u.role ='cliente' order by vw.dtultima desc";
        
        
        $unity = DB::select($sql);
        foreach ($unity as $key => $value) {

            $sql1="select count(*) as tt from messages where receiver_id = $receiver and opened = 0 and sender_id = " .  $unity[$key]->id ;
            
            $unity1 = DB::select($sql1);
            $unity[$key]->naolidas=$unity1[0]->tt;
            $unity[$key]->name = ucwords(strtolower($unity[$key]->name));
            $unity[$key]->avatar = $this->verificarArquivo($unity[$key]->avatar);
            $unity[$key]->dtultima = date('d/m/Y H:i:s', strtotime($unity[$key]->dtultima));
        }


       // return $unity;
        return response()->json(compact('unity'));    
        
    }    
    
  function verificarArquivo($name_img) {


        $diskimagem = Storage::disk('avatars');

        // SE A FOTO EXISTIR COLOCO
        if ($diskimagem->exists($name_img)) {
            return $name_img;
        } else { 
            return "user-a4.jpg";
        }
    }   

    public function teachers() {

         $sql= " SELECT u.id, u.name ,u.avatar, count(m.id) 
        as conversa_iniciada, max(m.created_at) 
        as created_at , 
        (select count(*) as total  FROM messages 
        WHERE receiver_id = $this->user and sender_id = u.id and opened =0 ) as naolidas 
        FROM users u INNER join messages m on m.sender_id = u.id 
        WHERE u.idunidade = $this->unity and m.receiver_id = $this->user 
        group by u.id, u.name ,u.avatar ORDER BY naolidas desc ";
    

        $unity = DB::select($sql);
        foreach ($unity as $key => $value){
            $unity[$key]->name=ucwords(strtolower($value->name));
            $unity[$key]->avatar=$this->verificarArquivo($value->avatar);
            $unity[$key]->created_at =  date('d/m/Y H:i:s', strtotime( $unity[$key]->created_at));
        }
        return $unity;


    }

 public function allUser($termo) {

        $sql= "  SELECT u.id, u.name ,u.avatar, role, idunidade
        FROM users u 
        WHERE u.idunidade = $this->unity  
            AND role <> 'prospect'  
            AND excluido = 'N'
            AND id <>$this->user 
            AND u.name like '%$termo%'
        group by u.id, u.name ,u.avatar ORDER BY u.name";

         $unity = DB::select($sql);
        // return $unity[0]->avatar;
      
        foreach ($unity as $key => $value){
            $unity[$key]->name = ucwords(strtolower($value->name));
            $unity[$key]->avatar = $this->verificarArquivo($value->avatar);
            // $unity[$key]->created_at =  date('d/m/Y H:i:s', strtotime( $unity[$key]->created_at));
        
        }
    

        return $unity;
    }

     public function totalmsg() {

        
        $sql= "select  count(*) as total FROM messages 
            WHERE receiver_id = $this->user AND opened = 0";        
        
        
        return DB::select($sql);


    }


}
