<?php

namespace App\Modules\ChatAPI\Providers;

use App\Modules\ChatAPI\Repositories\Contracts\MessageRepository;
use App\Modules\ChatAPI\Repositories\Contracts\UnityRepository;
use App\Modules\ChatAPI\Repositories\Contracts\UserRepository;
use App\Modules\ChatAPI\Repositories\MessageRepositoryEloquent;
use App\Modules\ChatAPI\Repositories\UnityRepositoryEloquent;
use App\Modules\ChatAPI\Repositories\UserRepositoryEloquent;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->mapWebRoutes();

        $this->app->bind(UserRepository::class, UserRepositoryEloquent::class);
        $this->app->bind(MessageRepository::class, MessageRepositoryEloquent::class);
        $this->app->bind(UnityRepository::class, UnityRepositoryEloquent::class);
    }

    private function mapWebRoutes()
    {
        Route::group([
            'prefix'     => 'api/chat',
            'as'         => 'api.',
            'middleware' => ['chat'],
            'namespace'  => 'App\Modules\ChatAPI\Http\Controllers'
        ], function ($router) {
            require app_path('Modules/ChatAPI/Http/routes.php');
        });
    }
}
