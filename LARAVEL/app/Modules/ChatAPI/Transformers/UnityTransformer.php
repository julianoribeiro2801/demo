<?php

namespace App\Modules\ChatAPI\Transformers;

use App\Models\Unidade;
use App\Modules\ChatAPI\Repositories\Contracts\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use League\Fractal\TransformerAbstract;
use Fractal;

class UnityTransformer extends TransformerAbstract
{
    public function transform($unidade)
    {
        return [
            'id'        => (int) $unidade->id,
            'logo'      => $unidade->logo,
            'fantasia'  => $unidade->fantasia,
            'persons'  => $this->getPersons($unidade->id)
        ];
    }

    private function getPersons($unidade)
    {
        $repository = App::make(UserRepository::class);
        $persons = $repository->findUsersByUnidade($unidade);

        return Fractal::collection($persons)->transformWith(new PersonTransformer());
    }
}
