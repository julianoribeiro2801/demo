<?php

namespace App\Modules\ChatAPI\Transformers;

use App\Models\Unidade;
use League\Fractal\TransformerAbstract;

class PersonTransformer extends TransformerAbstract
{
    public function transform($person)
    {
        return [
            'id'        => (int) $person->id,
            'name'      => $person->name,
            'email'     => $person->email,
            'avatar'    => url('/') . $person->avatar,
            'role'      => $person->role,
            'new_message' => 0
        ];
    }
}
