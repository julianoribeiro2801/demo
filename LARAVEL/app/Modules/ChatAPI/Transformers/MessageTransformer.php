<?php

namespace App\Modules\ChatAPI\Transformers;

use App\Models\User;
use App\Modules\ChatAPI\Entities\Message;
use League\Fractal\TransformerAbstract;

class MessageTransformer extends TransformerAbstract
{

    public function transform(Message $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */
            'sender_id' => $model->sender_id,
            'receiver_id' => $model->receiver_id,
            'message' => $model->message,
            'active' => $model->active,
            'opened' => $model->opened,

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at,
            'sender' => User::find($model->sender_id),
            'receiver' => User::find($model->receiver_id)
        ];
    }
}
