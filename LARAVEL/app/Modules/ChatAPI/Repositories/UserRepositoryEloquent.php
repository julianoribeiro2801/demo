<?php

namespace App\Modules\ChatAPI\Repositories;

use App\Models\User;
use App\Modules\ChatAPI\Repositories\Contracts\UserRepository;
use Prettus\Repository\Eloquent\BaseRepository;

class UserRepositoryEloquent extends BaseRepository implements UserRepository
{
    public function model()
    {
        return User::class;
    }

    public function findUsersByUnidade($unidade, $idusuario = null)
    {
        $data = $this->scopeQuery(function ($q) use ($idusuario, $unidade) {
            return $q->where('idunidade', $unidade)
               ->where('id', '<>', $idusuario);
        })->all();

        return $data;
    }
}
