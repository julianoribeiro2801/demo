<?php

namespace App\Modules\ChatAPI\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

interface UnityRepository extends RepositoryInterface
{
}
