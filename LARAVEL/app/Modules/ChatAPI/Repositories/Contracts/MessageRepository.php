<?php

namespace App\Modules\ChatAPI\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

interface MessageRepository extends RepositoryInterface
{
    public function findLastTalk($user, $unidade);
    public function findTalkByPeople($user, $talker);
    public function findNewsTalkByPeople($user, $talker);
    public function storeMessage($sender, $receiver, $message);
    public function setRead($message);
}
