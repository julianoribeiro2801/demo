<?php

namespace App\Modules\ChatAPI\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

interface UserRepository extends RepositoryInterface
{
    public function findUsersByUnidade($unidade, $idusuario);
}
