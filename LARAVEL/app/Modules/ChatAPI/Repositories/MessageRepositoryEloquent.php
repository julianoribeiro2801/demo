<?php

namespace App\Modules\ChatAPI\Repositories;

use App\Models\User;
use App\Modules\ChatAPI\Entities\Message;
use App\Modules\ChatAPI\Presenters\MessagePresenter;
use App\Modules\ChatAPI\Repositories\Contracts\MessageRepository;
use App\Modules\ChatAPI\Repositories\Contracts\UserRepository;
use Prettus\Repository\Eloquent\BaseRepository;

class MessageRepositoryEloquent extends BaseRepository implements MessageRepository
{
    public function presenter()
    {
        return MessagePresenter::class;
    }

    public function model()
    {
        return Message::class;
    }
    
    public function findLastTalk($user, $unidade)
    {
    }

    public function findTalkByPeople($user, $talker)
    {
        $data = $this->scopeQuery(function ($q) use ($user, $talker) {
               return $q->where('sender_id', $user)
                ->where('receiver_id', $talker)
                ->orWhere(function ($sub) use ($user, $talker) {
                    return $sub->where('sender_id', $talker)
                        ->where('receiver_id', $user);
                });
        //})->orderBy('created_at', 'ASC')->get();
                })->orderBy('id', 'ASC')->get();                

        return $data;
    }


     public function findTalkByPeoplev2($user, $talker)
    {
         $data = $this->scopeQuery(function ($q) use ($user, $talker) {
               return $q->where('sender_id', $user)
                ->where('receiver_id', $talker)
                ->orWhere(function ($sub) use ($user, $talker) {
                    return $sub->where('sender_id', $talker)
                        ->where('receiver_id', $user);
                });
        })->orderBy('id', 'ASC')->get();

        return $data;
    }

    public function findNewsTalkByPeople($user, $talker)
    {
        $data = $this->scopeQuery(function ($q) use ($user, $talker) {
            return $q->where('sender_id', $talker)
                    ->where('receiver_id', $user)
                    ->where('sender_id','<>', $user)
                    ->where('opened', false);

        })->get();

        return $data;
    }

    public function storeMessage($sender, $receiver, $message)
    {
        $data = $this->create([
           'sender_id' => $sender,
           'receiver_id' => $receiver,
           'message' => $message,
        ]);

        return $this->findTalkByPeople($sender, $receiver);
    }

    public function setRead($message)
    {
       return Message::find($message)->update(['opened' => true]);
    }
}
