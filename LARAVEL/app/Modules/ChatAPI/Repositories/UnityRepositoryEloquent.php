<?php

namespace App\Modules\ChatAPI\Repositories;

use App\Models\Unidade;
use App\Modules\ChatAPI\Repositories\Contracts\UnityRepository;
use Prettus\Repository\Eloquent\BaseRepository;

class UnityRepositoryEloquent extends BaseRepository implements UnityRepository
{
    public function model()
    {
        return Unidade::class;
    }
}
