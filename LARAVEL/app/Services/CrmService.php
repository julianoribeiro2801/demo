<?php
/**
 * Created by PhpStorm.
 * User: jeandro
 * Date: 09/03/17
 * Time: 23:01
 */

namespace App\Services;

use App\Models\Crm;
use League\Flysystem\Exception;

class CrmService
{
    public function __construct()
    {
    }

    public function register($from, $to, $action, $description = null)
    {
        try {
            return Crm::create([
                'de' => $from,
                'para' => $to,
                'acao' => $action,
                'descricao' => $description
            ]);
        } catch (Exception $exception) {
            return [];
        }
    }
}
