<?php 
namespace App\Services;

/**
*
*/
class TrataDadosService
{
    // Transforma para dar certo listar no drop dow (Select)
    public function listToSelect($categories)
    {
        $selectCategories = array();

        foreach ($categories as $category) {
            $selectedCategories[$category->id] = $category->name;
        }
        return $selectedCategories;
    }

    
    public function listToSelectPessoas($pessoas)
    {
        $selectPessoas = array();

        foreach ($pessoas as $pessoa) {
            $selectedPessoas[$pessoa->id] = $pessoa->name;
        }
        return $selectedPessoas;
    }
    public function listToSelectAlunos($alunos)
    {
        foreach ($alunos as $aluno) {
            $selectedAlunos[$aluno->id] = $aluno->name;
        }
        return $selectedAlunos;
    }

    public function listToSelectTipoconquistas($tipoconquistas)
    {
        foreach ($tipoconquistas as $tipoconquista) {
            $selectedTipoconquistas[$tipoconquista->id] = $tipoconquista->dstipoconquista;
        }
        return $selectedTipoconquistas;
    }
    
    public function listToSelectTipomensagens($tipomensagens)
    {
        foreach ($tipomensagens as $tipomensagem) {
            $selectedTipomensagens[$tipomensagem->id] = $tipomensagem->dstipomensagem;
        }
        return $selectedTipomensagens;
    }

    public function listToSelectAtividades($atividades)
    {
        foreach ($atividades as $atividade) {
            $selectedAtividades[$atividade->id] = $atividade->nmatividade;
        }
        return $selectedAtividades;
    }
    
    public function listToSelectProgramas($programas)
    {
        foreach ($programas as $programa) {
            $selectedProgramas[$programa->id] = $programa->nmprograma;
        }
        return $selectedProgramas;
    }
    public function listToSelectProfessores($professores)
    {
        foreach ($professores as $professor) {
            $selectedProfessores[$professor->id] = $professor->name;
        }
        return $selectedProfessores;
    }
    
    public function listToSelectTipoatividades($tipoatividades)
    {
        foreach ($tipoatividades as $tipoatividade) {
            $selectedTipoatividades[$tipoatividade->id] = $tipoatividade->dstipoatividade;
        }
        return $selectedTipoatividades;
    }
    
    public function listToSelectSns($sns)
    {
        $selectedSns['S'] = 'SIM';
        $selectedSns['N'] = 'NÃO';
        
        return $selectedSns;
    }
    
    public function listToSelectGruposmusculares($gruposmusculares)
    {
        foreach ($gruposmusculares as $grupomuscular) {
            $selectedGruposmusculares[$grupomuscular->id] = $grupomuscular->nmgrupo;
        }
        return $selectedGruposmusculares;
    }
}
