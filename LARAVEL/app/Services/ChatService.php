<?php

namespace App\Services;

use App\Models\Unidade;
use App\Models\Moduloacademia;
use Auth;
use DB;
use Session;
use Storage;

class ChatService {
    
    public function verificarArquivo($name_img) {


        $diskimagem = Storage::disk('avatars');

        // SE A FOTO EXISTIR COLOCO
        if ($diskimagem->exists($name_img)) {
            return $name_img;
        } else { 
            return "user-a4.jpg";
        }
    }     



}
