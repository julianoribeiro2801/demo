<?php
namespace App\Services;

use App\Models\Gfm;
use App\Models\Local;
use App\Models\Programa;
use App\Models\Diasemana;
use App\Models\User;
use App\Models\Unidade;
use App\Services\GfmService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Auth;

use App\Services\FilialService;

use App\Services\TrataDadosService;
use League\Flysystem\Exception;

class ApiGfmService
{
    private $model;
    private $default = 'prospect';
    private $request;
    private $unidade;
    private $usuario;
    private $crm;
    public function __construct(Gfm $gfm, Request $request, TrataDadosService $trataDadosService, FilialService $filialService)
    {
        $this->filialService = $filialService;
        $this->trataDadosService = $trataDadosService;
        $this->model = $gfm;
        $this->request = $request;
        // $this->unidade = $this->getUnidade($this->request->header('unidade'));
    }

    public function index()
    {
        $headers = ['category' => 'Empresa', 'title' => 'Gfm'];
        $unidades_combo = $this->filialService->unidadesComboTop();

        $idunidade = Auth::user()->idunidade;
        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }


        return view('admin.gfms.index', compact('unidades_combo', 'headers', 'idunidade'));
    }

    public function grafico($dias)
    {
        $idunidade = Auth::user()->idunidade;

        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        
        $dias=50;
        $dataatual= date('Y-m-d');
        $dataini = date('Y-m-d', strtotime('-' . $dias . ' days'));
        $audiencia=DB::select("select coalesce(sum(r.qtcapacidade),0) as totcap, coalesce(sum(r.qtalunopresente),0) as totaluno "
                             . " from resultadogfm as r "
                             . " where r.dtaula between '" . $dataini . "'  and '" . $dataatual . "'"
                             . " and r.idunidade = " . $idunidade);
        
        //andamento SQL
        $teste=[28, 48, 40, 25, 50, 25, 13];
        $capacidade=[45, 50, 50, 55, 50, 45, 60];
                
        return response()->json(compact('audiencia', 'capacidade', 'teste'));
    }
    
    
    public function indicadores($dias)
    {
        $idunidade = Auth::user()->idunidade;

        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }
        
       
        $dataatual= date('Y-m-d');
        $dataini = date('Y-m-d', strtotime('-' . $dias . ' days'));
        
        $mediaprof=DB::select("select coalesce(sum(r.qtcapacidade),0) as totcap, coalesce(sum(r.qtalunopresente),0) as totaluno ,"
                             . " u.id, u.idunidade, u.name , coalesce((coalesce(sum(r.qtalunopresente),0) / (select count(rg.id) from resultadogfm rg"
                             . " where rg.idfuncionario = u.id and rg.idunidade = " . $idunidade . " )),0) as media, u.id from users as u left join resultadogfm as r on u.id = r.idfuncionario"
                             . " and r.dtaula between '" . $dataini . "'  and '" . $dataatual . "'"
                             . " and u.idunidade = " . $idunidade
                             . " and r.idunidade = " . $idunidade
                             . " and u.role = 'funcionario'"
                             . " where u.idunidade = " . $idunidade
                             . " group by u.id, u.idunidade, u.name order by media desc");
        
        $percgeral=DB::select("select coalesce(sum(r.qtcapacidade),0) as totcap, coalesce(sum(r.qtalunopresente),0) as totaluno ,"
                             . " u.idunidade, coalesce(((coalesce(sum(r.qtalunopresente),0) * 100)  / (coalesce(sum(r.qtcapacidade),0)) ),0) as perc "
                             . " from users as u left join resultadogfm as r on u.id = r.idfuncionario"
                             . " and r.dtaula between '" . $dataini . "'  and '" . $dataatual . "'"
                             . " and u.idunidade = " . $idunidade
                             . " where u.idunidade = " . $idunidade
                             . " group by u.idunidade");
        return response()->json(compact('percgeral', 'mediaprof'));
    }
    
    public function all()
    {
        //    $unidades_combo = $this->filialService->unidadesComboTop();
        $idunidade = Auth::user()->idunidade;

        if (session()->get('id_unidade')) {
            $idunidade = session()->get('id_unidade');
        }

        $gfms = DB::select("SELECT coalesce(vw_mediagfm.total,0) as presencas,coalesce(vwt.numaulas,0) as numeroaulas,gfm.*,coalesce(vw_mediagfm.total,0) as total,"
                           . " ((coalesce(vw_mediagfm.total,0) * 100) / (gfm.qtcapacidade *  vwt.numaulas)) as perc,"
                           . " (coalesce(vw_mediagfm.total,0) / coalesce(vwt.numaulas,0)) as media,"
                           . " coalesce(((coalesce(vw_mediagfm.total,0) / coalesce(vwt.numaulas,0)) * gfm.vlfator),0) as revisado"
                           . " FROM gfm left join vw_mediagfm on gfm.id = vw_mediagfm.idgfm "
                           . " left join view_totalaulas as vwt on gfm.id = vwt.id"
                           . " where gfm.idunidade = " . $idunidade);
        
        $locais=DB::select("select * from local where idunidade = " . $idunidade . " order by nmlocal asc ");
        $selectedLocais = $this->trataDadosService->listToSelectLocais($locais);
        

        $programas =  DB::select("select id,nmprograma from programa where idunidade = " . $idunidade . " order by nmprograma asc ");
        $selectedProgramas = $this->trataDadosService->listToSelectProgramas($programas);

        $professores = DB::select("select id, name from users where idunidade = " . $idunidade . " and role = 'funcionario' order by name asc ");
        $selectedProfessores = $this->trataDadosService->listToSelectProfessores($professores);

        $diassemana = Diasemana::select('id', 'nmdiasemana')->get();
        $selectedDiassemana = $this->trataDadosService->listToSelectProgramas($diassemana);

        return response()->json(compact('locais', 'programas', 'professores', 'gfms', 'diassemana'));
    }
    public function register(Request $request)
    {
        try {
            $gfm = $this->model->create($request->all());
            return response()->json($gfm);
        } catch (Exception $exception) {
            return response()->json($exception->getMessage(), $exception->getCode());
        }
    }

    public function show($id)
    {
        $gfm = $this->model->where('id', $id)->first();
        return response()->json($gfm);
    }


    public function update(Request $request, $id)
    {
        try {
            $client = $this->model->find($id)->update($request->all());
            //if ($this->model->find($id)->update($this->formatData($request))){
            return response()->json('Cadastro atualizado com sucesso!!!');
            // }
        } catch (Exception $e) {
            return response()->json($e->getMessage(), $e->getCode());
        }
    }

    public function data($id)
    {
        $client = $this->model->find($id)->with('dados');
    }

    public function destroy($id)
    {
        try {
            $exercicio = $this->model->find($id)->delete();
            // if ($this->model->find($id)->update($this->formatData($request))){
            return response()->json('Cadastro removido com sucesso!!!');
            //}
        } catch (Exception $e) {
            return response()->json($e->getMessage(), $e->getCode());
        }
    }
}
