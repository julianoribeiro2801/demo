<?php

namespace App\Services;

use App\Models\Unidade;
use App\Models\User;
use App\Models\PushType;
use App\Modules\ChatAPI\Entities\Message;
use App\Models\Habilidade;
use Auth;
use GuzzleHttp\Client;

class SendPushService {

    public function getSenderID($unidade) {
        // verifica se tem repecionista se nao tiver pegar o menor id 
        $sender_id = User::wherein('role', ['recepcionista','admin','adminfilial'])
                        ->where('idunidade', $unidade)
                        ->orderBy('role','desc')
                        ->get();
        
        return $sender_id[0]['id'];
        
    }
    public function enviaPushProgressoSpm($dados) {
        
       $sender_user = User::find($dados['sender_id']);
       $receiver_habilidade = Habilidade::find($dados['push_habilidade']);

       $receiver_user = User::find($dados['receiver_id']);
       $nome=$receiver_user['name'];
       
        //        $receiver_user = User::where('id', $dados['sender_id'])->get();
        if ($receiver_user->parent_id > 0):
            $receiver_user = User::find($receiver_user->parent_id); // User::where('id', $receiver_user->parent_id)->get();
        endif;

        $push_type = PushType::find($dados['push_type']);
        $push_dtreserva = date('d/m/Y', strtotime($dados['push_dtaula']));
        $push_horario = $dados['push_horario'];
        $push_aula = '';//$dados['push_aula'];
        $push_hr_aviso = $dados['hr_aviso'];
        $push_reserva_id = $dados['reserva_id'];

        $receiver_name = explode(" ", $nome);
        $sender_name = explode(" ", $sender_user['name']);
        //APOS VALIDAR SE USUARIO JA PODE RECEBER PUSH ENVIO MENSAGEM
        
        
        $mensagem = str_replace('+++++',ucwords(strtolower($receiver_habilidade['nmhabilidade'])), str_replace('YYYYY', ucwords(strtolower($sender_name[0])),str_replace('$$$$$', $push_aula, str_replace('##:##', $push_horario, str_replace('xx/xx/xxxx', $push_dtreserva, str_replace('*****', ucwords(strtolower($receiver_name[0])), $push_type->mensagem))))));
        
        $dados_log = [
            'sender_id' => $dados['sender_id'],
            'receiver_id' => $dados['receiver_id'],
            'message' => $mensagem,
            'reserva_id' => $dados['reserva_id'],
            'hr_aviso' =>$dados['hr_aviso'],
            
            'push_types' => $dados['push_type']
        ];

        $this->gravaLogMensagem($dados_log);

        if ($receiver_user->device_id) {
            $client = new Client;
            $client->request('POST', 'https://fcm.googleapis.com/fcm/send', [
                'headers' => [
                    'Authorization' => 'key=AAAAsk7DCU4:APA91bFZVdiP4uRz43MQJjats2cVUBsDl_sB2a9TZVmgPlKDzGeDCgyMKV8NviiwgunEzPRE1jUXLnjdIe8DqrSakiuojHrJGrf9jrZU6oOy0uww4pG0O2ormIvtuQtWR78RmaNa2QGl'
                ],
                'json' => [

                    'to' => $receiver_user->device_id,
                    'notification' => [
                        'title' => $sender_name[0],
                        'body' => "Enviou uma nova mensagem",
                        'icon' => 'ic_stat_chat',
                        'user_id' => $receiver_user->id,
                        'sound' => 'default',
                        'color' => '#301954'
                    ]
                ]
            ]);


        /*    return response()->json(['status' => 'success']);
        } else {
            return response()->json(['status' => 'error', 'mensagem' => 'receiver_user nao possui device_id']);*/
        }        
        

        
    }    

    public function enviaPushReserva($dados) {

        $sender_user = User::find($dados['sender_id']);
        $receiver_user = User::find($dados['receiver_id']);
        $push_type = PushType::find($dados['push_type']);
        $push_dtreserva = date('d/m/Y', strtotime($dados['push_dtaula']));
        $push_horario = $dados['push_horario'];
        $push_aula = $dados['push_aula'];
        $push_hr_aviso = $dados['hr_aviso'];
        $push_reserva_id = $dados['reserva_id'];

        $receiver_name = explode(" ", $receiver_user['name']);
        $sender_name = explode(" ", $sender_user['name']);
        //APOS VALIDAR SE USUARIO JA PODE RECEBER PUSH ENVIO MENSAGEM
        $mensagem =  str_replace('YYYYY', ucwords(strtolower($sender_name[0])),str_replace('$$$$$', $push_aula, str_replace('##:##', $push_horario, str_replace('xx/xx/xxxx', $push_dtreserva, str_replace('*****', ucwords(strtolower($receiver_name[0])), $push_type->mensagem)))));
        
        $dados_log = [
            'sender_id' => $dados['sender_id'],
            'receiver_id' => $dados['receiver_id'],
            'message' => $mensagem,
            'reserva_id' => $dados['reserva_id'],
            'hr_aviso' =>$dados['hr_aviso'],
            
            'push_types' => $dados['push_type']
        ];

        $this->gravaLogMensagem($dados_log);

        if ($receiver_user->device_id) {
            $client = new Client;
            $client->request('POST', 'https://fcm.googleapis.com/fcm/send', [
                'headers' => [
                    'Authorization' => 'key=AAAAsk7DCU4:APA91bFZVdiP4uRz43MQJjats2cVUBsDl_sB2a9TZVmgPlKDzGeDCgyMKV8NviiwgunEzPRE1jUXLnjdIe8DqrSakiuojHrJGrf9jrZU6oOy0uww4pG0O2ormIvtuQtWR78RmaNa2QGl'
                ],
                'json' => [

                    'to' => $receiver_user->device_id,
                    'notification' => [
                        'title' => $sender_name[0],
                        'body' => "Enviou uma nova mensagem",
                        'icon' => 'ic_stat_chat',
                        'user_id' => $receiver_user->id,
                        'sound' => 'default',
                        'color' => '#301954'
                    ]
                ]
            ]);


        /*    return response()->json(['status' => 'success']);
        } else {
            return response()->json(['status' => 'error', 'mensagem' => 'receiver_user nao possui device_id']);*/
        }        

    }
    
    
    public function enviaPushOfereceVaga($dados) {

        $sender_user = User::find($dados['sender_id']);
        $receiver_user = User::find($dados['receiver_id']);
        $push_type = PushType::find($dados['push_type']);
        $push_dtreserva = date('d/m/Y', strtotime($dados['push_dtaula']));
        $push_horario = $dados['push_horario'];
        $push_aula = $dados['push_aula'];
        $push_hr_aviso = $dados['hr_aviso'];
        $push_reserva_id = $dados['reserva_id'];

        $receiver_name = explode(" ", $receiver_user['name']);
        $sender_name = explode(" ", $sender_user['name']);
        //APOS VALIDAR SE USUARIO JA PODE RECEBER PUSH ENVIO MENSAGEM
        $mensagem =  str_replace('YYYYY', ucwords(strtolower($sender_name[0])),str_replace('$$$$$', $push_aula, str_replace('##:##', $push_horario, str_replace('xx/xx/xxxx', $push_dtreserva, str_replace('*****', ucwords(strtolower($receiver_name[0])), $push_type->mensagem)))));
        
        $dados_log = [
            'sender_id' => $dados['sender_id'],
            'receiver_id' => $dados['receiver_id'],
            'message' => $mensagem,
            'reserva_id' => $dados['reserva_id'],
            'hr_aviso' =>$dados['hr_aviso'],
            
            'push_types' => $dados['push_type']
        ];

        $this->gravaLogMensagem($dados_log);

        if ($receiver_user->device_id) {
            $client = new Client;
            $client->request('POST', 'https://fcm.googleapis.com/fcm/send', [
                'headers' => [
                    'Authorization' => 'key=AAAAsk7DCU4:APA91bFZVdiP4uRz43MQJjats2cVUBsDl_sB2a9TZVmgPlKDzGeDCgyMKV8NviiwgunEzPRE1jUXLnjdIe8DqrSakiuojHrJGrf9jrZU6oOy0uww4pG0O2ormIvtuQtWR78RmaNa2QGl'
                ],
                'json' => [

                    'to' => $receiver_user->device_id,
                    'notification' => [
                        'title' => $sender_name[0],
                        'body' => "Enviou uma nova mensagem",
                        'icon' => 'ic_stat_chat',
                        'user_id' => $receiver_user->id,
                        'sound' => 'default',
                        'color' => '#301954'
                    ]
                ]
            ]);


        /*    return response()->json(['status' => 'success']);
        } else {
            return response()->json(['status' => 'error', 'mensagem' => 'receiver_user nao possui device_id']);*/
        }        

    }    
    
    public function enviaPushCancelaAula($dados) {

        $sender_user = User::find($dados['sender_id']);
        $receiver_user = User::find($dados['receiver_id']);
        $push_type = PushType::find($dados['push_type']);
        $push_dtreserva = date('d/m/Y', strtotime($dados['push_dtaula']));
        $push_horario = $dados['push_horario'];
        $push_aula = $dados['push_aula'];
        $push_hr_aviso = $dados['hr_aviso'];
        $push_reserva_id = $dados['reserva_id'];

        $receiver_name = explode(" ", $receiver_user['name']);
        $sender_name = explode(" ", $sender_user['name']);
        //APOS VALIDAR SE USUARIO JA PODE RECEBER PUSH ENVIO MENSAGEM
        $mensagem =  str_replace('YYYYY', ucwords(strtolower($sender_name[0])),str_replace('$$$$$', $push_aula, str_replace('##:##', $push_horario, str_replace('xx/xx/xxxx', $push_dtreserva, str_replace('*****', ucwords(strtolower($receiver_name[0])), $push_type->mensagem)))));
        
        $dados_log = [
            'sender_id' => $dados['sender_id'],
            'receiver_id' => $dados['receiver_id'],
            'message' => $mensagem,
            'reserva_id' => $dados['reserva_id'],
            'hr_aviso' =>$dados['hr_aviso'],
            
            'push_types' => $dados['push_type']
        ];

        $this->gravaLogMensagem($dados_log);
        
        if ($receiver_user->device_id) {
            $client = new Client;
            $client->request('POST', 'https://fcm.googleapis.com/fcm/send', [
                'headers' => [
                    'Authorization' => 'key=AAAAsk7DCU4:APA91bFZVdiP4uRz43MQJjats2cVUBsDl_sB2a9TZVmgPlKDzGeDCgyMKV8NviiwgunEzPRE1jUXLnjdIe8DqrSakiuojHrJGrf9jrZU6oOy0uww4pG0O2ormIvtuQtWR78RmaNa2QGl'
                ],
                'json' => [

                    'to' => $receiver_user->device_id,
                    'notification' => [
                        'title' => $sender_name[0],
                        'body' => "Enviou uma nova mensagem",
                        'icon' => 'ic_stat_chat',
                        'user_id' => $receiver_user->id,
                        'sound' => 'default',
                        'color' => '#301954'
                    ]
                ]
            ]);


        }        

    }    
    
    public function enviaPushPersonalizado($dados) {

        $sender_user = User::find($dados['sender_id']);
        $receiver_user = User::find($dados['receiver_id']);
        $push_type = PushType::find($dados['push_type']);
        $push_dtreserva = date('d/m/Y', strtotime($dados['push_dtagenda']));
        $push_horario = $dados['push_horario'];
        $push_hr_aviso = $dados['hr_aviso'];
        $push_reserva_id = $dados['reserva_id'];

        $receiver_name = explode(" ", $receiver_user['name']);
        $sender_name = explode(" ", $sender_user['name']);
        //APOS VALIDAR SE USUARIO JA PODE RECEBER PUSH ENVIO MENSAGEM
        $mensagem = str_replace('YYYYY',ucwords(strtolower($sender_name[0])),str_replace('##:##', $push_horario, str_replace('xx/xx/xxxx', 'hoje', str_replace('*****', ucwords(strtolower($receiver_name[0])), $push_type->mensagem))));

        
        $dados_log = [
            'sender_id' => $dados['sender_id'],
            'receiver_id' => $dados['receiver_id'],
            'message' => $mensagem,
            'reserva_id' => $dados['reserva_id'],
            'hr_aviso' =>$dados['hr_aviso'],
            'push_types' => $dados['push_type']
        ];
        $this->gravaLogMensagem($dados_log);
        if ($receiver_user->device_id) {
            $client = new Client;
            $client->request('POST', 'https://fcm.googleapis.com/fcm/send', [
                'headers' => [
                    'Authorization' => 'key=AAAAsk7DCU4:APA91bFZVdiP4uRz43MQJjats2cVUBsDl_sB2a9TZVmgPlKDzGeDCgyMKV8NviiwgunEzPRE1jUXLnjdIe8DqrSakiuojHrJGrf9jrZU6oOy0uww4pG0O2ormIvtuQtWR78RmaNa2QGl'
                ],
                'json' => [

                    'to' => $receiver_user->device_id,
                    'notification' => [
                        'title' => $sender_name[0],
                        'body' => "Enviou uma nova mensagem",
                        'icon' => 'ic_stat_chat',
                        'user_id' => $receiver_user->id,
                        'sound' => 'default',
                        'color' => '#301954'
                    ]
                ]
            ]);


        /*    return response()->json(['status' => 'success']);
        } else {
            return response()->json(['status' => 'error', 'mensagem' => 'receiver_user nao possui device_id']);*/
        }         

    }    
    
    public function enviaPushAvisoAval($dados) {

        $sender_user = User::find($dados['sender_id']);
        $receiver_user = User::find($dados['receiver_id']);
        $push_type = PushType::find($dados['push_type']);
        $push_dtreserva = date('d/m/Y', strtotime($dados['push_dtagenda']));
        $push_horario = $dados['push_horario'];
        $push_hr_aviso = $dados['hr_aviso'];
        $push_reserva_id = $dados['reserva_id'];

        $receiver_name = explode(" ", $receiver_user['name']);
        $sender_name = explode(" ", $sender_user['name']);
        //APOS VALIDAR SE USUARIO JA PODE RECEBER PUSH ENVIO MENSAGEM
        $mensagem = str_replace('YYYYY',ucwords(strtolower($sender_name[0])),str_replace('##:##', $push_horario, str_replace('xx/xx/xxxx', 'hoje', str_replace('*****', ucwords(strtolower($receiver_name[0])), $push_type->mensagem))));


        
        $dados_log = [
            'sender_id' => $dados['sender_id'],
            'receiver_id' => $dados['receiver_id'],
            'message' => $mensagem,
            'reserva_id' => $dados['reserva_id'],
            'hr_aviso' =>$dados['hr_aviso'],
            'push_types' => $dados['push_type']
        ];

        $ret = $this->gravaLogMensagem($dados_log);

        if ($receiver_user->device_id) {
            $client = new Client;
            $client->request('POST', 'https://fcm.googleapis.com/fcm/send', [
                'headers' => [
                    'Authorization' => 'key=AAAAsk7DCU4:APA91bFZVdiP4uRz43MQJjats2cVUBsDl_sB2a9TZVmgPlKDzGeDCgyMKV8NviiwgunEzPRE1jUXLnjdIe8DqrSakiuojHrJGrf9jrZU6oOy0uww4pG0O2ormIvtuQtWR78RmaNa2QGl'
                ],
                'json' => [

                    'to' => $receiver_user->device_id,
                    'notification' => [
                        'title' => $sender_name[0],
                        'body' => "Enviou uma nova mensagem",
                        'icon' => 'ic_stat_chat',
                        'user_id' => $receiver_user->id,
                        'sound' => 'default',
                        'color' => '#301954'
                    ]
                ]
            ]);

            return $ret . 'teste';
        /*    return response()->json(['status' => 'success']);
        } else {
            return response()->json(['status' => 'error', 'mensagem' => 'receiver_user nao possui device_id']);*/
        }        

    }

    public function haveMessagemToMe($user_id) {

        $dados = Message::select('push_types.quando as title',
         'users.name',
         'messages.message',
         'messages.sender_id',
         'messages.reserva_id',
         'messages.hr_aviso',
         'messages.push_types')
                        ->JOIN('users', 'users.id', '=', 'messages.sender_id')
                        ->JOIN('push_types', 'push_types.id', '=', 'messages.push_types')
                        ->where('receiver_id', $user_id)
                        ->where('push_types', '>', '0')
                        ->where('opened', '0')->get();


        if ($dados) {
            return ['status' => 'success', 'dados' => $dados];
        }
        return ['status', 'error'];
    }

    public function gravaLogMensagem($dados) {
        $insert = Message::create($dados);
        if ($insert) {
            return ['status', 'success'];
        }
        return ['status', 'error'];
    }

    public function enviaPushAgendamento($dados) {

        $sender_user = User::find($dados['sender_id']);
        $receiver_user = User::find($dados['receiver_id']);
        $push_type = PushType::find($dados['push_type']);
        
        
        $dias = date('w', strtotime(date('Y-m-d',  strtotime($dados['push_dtagenda'])))) ;
        
        $dt=date('Y-m-d');
        //$dtHj =$dados['push_dtagenda'];
        //$ano = substr($dados['push_dtagenda'], 0, 4);
        //$mes = substr($dados['push_dtagenda'], 5, 2);
        //$dia = substr($dados['push_dtagenda'], 8, 2);
        
        //$dias = date("w", mktime( $mes, $dia, $ano));
        switch ($dias) {
            case '0':
                $diasemana = 'domingo';
                break;
            case '1':
                $diasemana = 'segunda-feira';
                break;
            case '2':
                $diasemana = 'terça-feira';
                break;
            case '3':
                $diasemana = 'quarta-feira';
                break;
            case '4':
                $diasemana = 'quinta-feira';
                break;
            case '5':
                $diasemana = 'sexta-feira';
                break;
            case '6':
                $diasemana = 'sábado';
                break;
        }    


        if ( $dados['push_dtagenda'] !== $dt){
            $push_dtagenda =  $diasemana . ', ' . date('d/m/Y', strtotime($dados['push_dtagenda']));
        }else{
            $push_dtagenda = 'hoje';
        }
        
       // $push_dtagenda = date('d/m/Y', strtotime($dados['push_dtagenda']));
        $push_horario = $dados['push_horario'];


        $receiver_name = explode(" ", $receiver_user['name']);
        $sender_name = explode(" ", $sender_user['name']);
        //APOS VALIDAR SE USUARIO JA PODE RECEBER PUSH ENVIO MENSAGEM
        $mensagem = str_replace('YYYYY',$sender_name[0], str_replace('##:##', $push_horario, str_replace('xx/xx/xxxx', $push_dtagenda, str_replace('*****',ucwords(strtolower($receiver_name[0])), $push_type->mensagem))));
        $dados_log = [
            'sender_id' => $dados['sender_id'],
            'receiver_id' => $dados['receiver_id'],
            'message' => $mensagem,
            'push_types' => $dados['push_type']
        ];


        if ($receiver_user->device_id) {
            $this->gravaLogMensagem($dados_log);

            $client = new Client;
            $client->request('POST', 'https://fcm.googleapis.com/fcm/send', [
                'headers' => [
                    'Authorization' => 'key=AAAAsk7DCU4:APA91bFZVdiP4uRz43MQJjats2cVUBsDl_sB2a9TZVmgPlKDzGeDCgyMKV8NviiwgunEzPRE1jUXLnjdIe8DqrSakiuojHrJGrf9jrZU6oOy0uww4pG0O2ormIvtuQtWR78RmaNa2QGl'
                ],
                'json' => [

                    'to' => $receiver_user->device_id,
                    'notification' => [
                        'title' => $sender_name[0],
                        'body' => "Enviou uma nova mensagem",
                        'icon' => 'ic_stat_chat',
                        'user_id' => $receiver_user->id,
                        'sound' => 'default',
                        'color' => '#301954'
                    ]
                ]
            ]);


            return response()->json(['status' => 'success']);
        } else {
            return response()->json(['status' => 'error', 'mensagem' => 'receiver_user nao possui device_id']);
        }
    }

    public function enviaPush($dados) {
        $sender_user = User::find($dados['sender_id']);
        $receiver_user = User::find($dados['receiver_id']);
        $push_type = PushType::find($dados['push_type']);

        $push_unidade = Unidade::find($sender_user->idunidade);
        

        $receiver_name = explode(" ", $receiver_user['name']);
        $sender_name = explode(" ", $sender_user['name']);
        //APOS VALIDAR SE USUARIO JA PODE RECEBER PUSH ENVIO MENSAGEM

        $mensagem = str_replace('#####', $push_unidade->fantasia, str_replace('YYYYY',  $sender_name[0], str_replace('*****', $receiver_name[0], $push_type->mensagem)));//str_replace('YYYYY',  $sender_name[0], str_replace('*****', $receiver_name[0], $push_type->mensagem)); //'Enviou uma nova mensagem.',;
        $dados_log = [
            'sender_id' => $dados['sender_id'],
            'receiver_id' => $dados['receiver_id'],
            'message' => $mensagem,
            'push_types' => $push_type->id
        ];
        $this->gravaLogMensagem($dados_log);
        
        if ($receiver_user->device_id) {   

            $client = new Client;
            $client->request('POST', 'https://fcm.googleapis.com/fcm/send', [
                'headers' => [
                    'Authorization' => 'key=AAAAsk7DCU4:APA91bFZVdiP4uRz43MQJjats2cVUBsDl_sB2a9TZVmgPlKDzGeDCgyMKV8NviiwgunEzPRE1jUXLnjdIe8DqrSakiuojHrJGrf9jrZU6oOy0uww4pG0O2ormIvtuQtWR78RmaNa2QGl'
                ],
                'json' => [

                    'to' => $receiver_user->device_id,
                    'notification' => [
                        'title' => $sender_name[0],
                        'body' => 'Enviou uma nova mensagem.',
                        'icon' => 'ic_stat_chat',
                        'user_id' => $receiver_user->id,
                        'sound' => 'default',
                        'color' => '#301954'
                    ]
                ]
            ]);


            return response()->json(['status' => 'success']);
        } else {
            return response()->json(['status' => 'error', 'mensagem' => 'receiver_user nao possui device_id']);
        }
    }

    
}
