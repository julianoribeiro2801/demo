<?php

namespace App\Services;

use App\Models\Unidade;
use App\Models\Moduloacademia;
use Auth;
use DB;
use Session;

class FilialService {

    public function modulos_ativos($id_unidade) {

        $modulo_cores = Moduloacademia::where('id_unidade', $id_unidade)
                        ->where('id_modulo', 1)->where('status', 1)->first();

        (count($modulo_cores) > 0) ? session()->put('modulo_cores', 'S') : session()->put('modulo_cores', 'N');
    }

    public function totalmsg() {

        $user = auth()->user();

        if (isset($user)) {
            if (Session::get('id_unidade')) {
                $id_unidade = Session::get('id_unidade');
                $this->modulos_ativos($id_unidade);


                $sql = "SELECT  count(*) as total
            FROM messages m
            INNER join users u on m.receiver_id = u.id
            WHERE receiver_id = $user->id AND opened = 0
            AND  u.idunidade =   $id_unidade";

                $msg = DB::select($sql);

                return $msg;
            }
        }
    }

    public function unidadesComboTop() {
        if (auth()->check()) {
            return $this->getUnitiesForUserAuth();
        }
        return [];
    }


 public function getLicenca($id, $campo) {


        $ret = Unidade::select('licenca', 'updated_at')->where('id', $id)->get();

        $data_inicio = new \DateTime(date('Y-m-d', strtotime($ret[0]->updated_at)));
        $data_fim = new \DateTime(date('Y-m-d'));

        // Resgata diferença entre as datas
        $dateInterval = $data_inicio->diff($data_fim);
        $dias = $dateInterval->days;


        if ($campo == 'dias'):
            return $dias;
        else:
            return strtoupper($ret[0]->licenca);
        endif;
    }

    public function changeEmp($id_unidade) {


        $retdias = $this->getLicenca($id_unidade, 'dias');
        $retlicenca = $this->getLicenca($id_unidade, 'licenca');

        session()->put('dias', $retdias);
        session()->put('licenca', $retlicenca);
        session()->put('id_unidade', $id_unidade);

    }

    public function getUnitiesForUserAuth() {
        $user = auth()->user();

        if ($user->role === 'admin') {
            return $this->getUnitiesByAdmin($user->id);
        }
        $ret = $this->getLicenca($user->idunidade,'licenca');
        session()->put('licenca', $ret);
        $ret = $this->getLicenca($user->idunidade,'dias');
        session()->put('dias', $ret);



        return $this->getUnitiesByEmployer($user->idunidade);
    }

   

    public function getUnitiesByAdmin($id) {
        return Unidade::where('user_id', $id)
                        ->where('situacao', 1)
                        ->orderBy('parent_id', 'ASC', 'fantasia', 'ASC')
                        ->get();
    }

    public function getUnitiesByEmployer($unity) {
        return Unidade::where('id', $unity)
                        ->where('situacao', 1)
                        ->orWhere('parent_id', $unity)
                        ->orderBy('parent_id', 'ASC', 'fantasia', 'ASC')
                        ->get();
    }

}
