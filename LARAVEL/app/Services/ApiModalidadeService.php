<?php
namespace App\Services;

use App\Models\Atividade;
use App\Models\Unidade;
use App\Services\ModalidadeService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use League\Flysystem\Exception;
use Auth;
use App\Services\FilialService;

class ApiModalidadeService
{
    private $model;
    private $default = 'prospect';
    private $request;
    private $unidade;
    private $usuario;
    private $crm;
    public function __construct(Atividade $modalidade, Request $request)
    {
        $this->model = $modalidade;
        $this->request = $request;
        $this->unidade = $this->getUnidade($this->request->header('unidade'));
    }

    public function index()
    {
        $modalidades= $this->model->where('id', '>', 0)->orderBy('nmatividade', 'ASC')->get();

        return response()->json(compact('modalidades'));
    }

    public function all()
    {
        $modalidades = $this->model->where('id', '>', 0)->orderBy('nmatividade', 'ASC')->get();

        return response()->json(compact('modalidades'));
    }
    public function register(Request $request)
    {
        $acao = 1;
        try {
            $modalidade = $this->model->create($this->formatUser($request));
            $modalidade->dados()->create($request->all());
            return response()->json($modalidade);
        } catch (Exception $exception) {
            return response()->json($exception->getMessage(), $exception->getCode());
        }
    }

    public function show($id)
    {
        $modalidade = $this->model->where('id', $id)->first();
        return response()->json($modalidade);
    }


    public function update(Request $request, $id)
    {
        try {
            $modalidade = $this->model->find($id)->update($request->all());
            // if ($this->model->find($id)->update($this->formatData($request))){
            return response()->json(compact('modalidade'));
            // }
           // $this->model->find($id)->dados()->create($this->formatData($request));
        } catch (Exception $e) {
            return response()->json($e->getMessage(), $e->getCode());
        }
    }

    public function data($id)
    {
        $client = $this->model->find($id)->with('dados');
    }

    public function destroy($id)
    {
        try {
            $modalidade = $this->model->find($id)->delete();
            // if ($this->model->find($id)->update($this->formatData($request))){
            return response()->json('Cadastro removido com sucesso!!!');
            //}
        } catch (Exception $e) {
            return response()->json($e->getMessage(), $e->getCode());
        }
    }

    private function formatUser(Request $request)
    {
        $data = $request->all();
        $data['role'] = $request->role ? $request->role : $this->default;
        $data['password'] = $request->password ? $request->password : bcrypt($this->default);

        return $data;
    }



    private function getUser($id)
    {
        return User::find($id);
    }

    private function getUnidade($id)
    {
        return Unidade::find($id);
    }

    private function getCidadeId($cidade, $uf)
    {
        try {
            $estado = Estado::where('nome', 'like', $uf)->orWhere('uf', 'like', $uf)->first();
            $cidade = Cidade::where('nome', $cidade)->where('estado', $estado->id)->first();
            return ['cidade' => $cidade->id, 'estado' => $estado->id];
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    private function parseCidade($idCidade)
    {
        $cidade = Cidade::find($idCidade);
        $estado = Estado::find($cidade->estado);
        return ['cidade' => $cidade->nome, 'estado' => $estado->uf];
    }
}
