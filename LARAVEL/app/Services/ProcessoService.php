<?php

namespace App\Services;

use \App\Models\PrescricaoProjeto;
use App\Models\UserDados;
use App\Models\Unidade;
use App\Models\User;
use App\Models\PushType;
use App\Modules\ChatAPI\Entities\Message;
use App\Models\Habilidade;
use App\Models\Processoaluno;
use App\Models\Processo;
use App\Models\Agendaalunoprograma;
use App\Models\Historicotreino;
use Auth;
use GuzzleHttp\Client;
use DB;

class ProcessoService {

    public function baixaProcesso($idaluno, $idunidade, $id, $tipo) {


        $dataProcesso['stprocesso'] = 'R';
        $agenda = new Processoaluno();
        if ($agenda->where('idregistro', '=', $id)
                        //->where('idaluno', '=', $idaluno)
                        ->where('idprocesso', '=', $tipo)
                        ->where('idunidade', '=', $idunidade)
                        ->update($dataProcesso)):
            $retorno['title'] = 'Sucesso!';
            $retorno['type'] = 'success';
            $retorno['text'] = 'Processo atualizada com sucesso!';
            return $retorno;
        else:
            $retorno['title'] = 'Erro!';
            $retorno['type'] = 'error';
            $retorno['text'] = 'Erro ao atualizar agenda!';
            return $retorno;
        endif;
    }

    public function geraProcessoTreinoVencido($idaluno, $idunidade) { // ok
        $idprocesso = 3;

        $revisao = DB::select("select treino_id from treino_musculacao"
                        . " where aluno_id = " . $idaluno
                        . " and unidade_id = " . $idunidade
                        . " and treino_revisao < " . date('Y-m-d')
                        . " and treino_id =( select max(treino_id) "
                        . " from treino_musculacao where aluno_id = " . $idaluno
                        . " and unidade_id = " . $idunidade . " )");


        if (sizeof($revisao) > 0):

            $processo = Processoaluno::select('id')
                    ->where('idunidade', $idunidade)
                    ->where('idregistro', $revisao[0]->treino_id)
                    ->where('idaluno', $idaluno)
                    ->where('idprocesso', $idprocesso)
                    ->get();

            if (sizeof($processo) <= 0):
                $dataProc['idunidade'] = $idunidade;
                $dataProc['idaluno'] = $idaluno;
                $dataProc['idprocesso'] = $idprocesso;
                $dataProc['dtstart'] = date('Y-m-d H:i:s');
                $dataProc['dtagenda'] = date('Y-m-d');
                $dataProc['dtbaixa'] = '';
                $dataProc['idprofessor'] = 0;
                $dataProc['stprocesso'] = 'P';
                $dataProc['idregistro'] = $revisao[0]->treino_id;

                $processo = new Processoaluno();
                if ($processo->create($dataProc)):
                    $retorno['title'] = 'Sucesso!';
                    $retorno['type'] = 'success';
                    $retorno['text'] = 'Processo gerado com sucesso!';
                //return $retorno;
                else:
                    $retorno['title'] = 'Erro!';
                    $retorno['type'] = 'error';
                    $retorno['text'] = 'Erro ao gerar processo!';
                // return $retorno;
                endif;
            endif;
        endif;



        return $revisao;
    }

    public function verificaProcessoAtivo($id) { // ok
        $processo = Processo::select('statusprocesso')
                ->where('id', $id)
                ->get();

        return $processo[0]->statusprocesso;
    }

    public function geraProcessoTreinoVencidoRb() { // ok
        $idprocesso = 3;

        $ret = $this->verificaProcessoAtivo($idprocesso);

        if ($ret == 'S'):


            $revisao = DB::select("select tm.treino_id,tm.aluno_id,tm.unidade_id from treino_musculacao tm"
                            . " where tm.treino_revisao < " . date('Y-m-d')
                            . " and tm.treino_id =( select max(tx.treino_id) "
                            . " from treino_musculacao tx where tx.aluno_id = tm.aluno_id"
                            . " and tx.unidade_id = tm.unidade_id )");


            foreach ($revisao as $key => $value):

                $processo = Processoaluno::select('id')
                        ->where('idunidade', $revisao[$key]->unidade_id)
                        ->where('idregistro', $revisao[$key]->treino_id)
                        ->where('idaluno', $revisao[$key]->aluno_id)
                        ->where('idprocesso', $idprocesso)
                        ->get();

                if (sizeof($processo) <= 0):
                    $dataProc['idunidade'] = $revisao[$key]->unidade_id;
                    $dataProc['idaluno'] = $revisao[$key]->aluno_id;
                    $dataProc['idprocesso'] = $idprocesso;
                    $dataProc['dtstart'] = date('Y-m-d H:i:s');
                    $dataProc['dtagenda'] = date('Y-m-d');
                    $dataProc['dtbaixa'] = '';
                    $dataProc['idprofessor'] = 0;
                    $dataProc['stprocesso'] = 'P';
                    $dataProc['idregistro'] = $revisao[$key]->treino_id;

                    $processo = new Processoaluno();
                    if ($processo->create($dataProc)):
                        $retorno['title'] = 'Sucesso!';
                        $retorno['type'] = 'success';
                        $retorno['text'] = 'Processo gerado com sucesso!';
                    //return $retorno;
                    else:
                        $retorno['title'] = 'Erro!';
                        $retorno['type'] = 'error';
                        $retorno['text'] = 'Erro ao gerar processo!';
                    // return $retorno;
                    endif;
                endif;
            endforeach;


        endif;
    }

    public function geraProcessoAniversario($idaluno, $idunidade) {

        $idprocesso = 5;
        $ret = $this->verificaProcessoAtivo($idprocesso);

        if ($ret == 'S'):

            $dataAniver = UserDados::selectRaw('
                                MONTH(dt_nascimento) AS month,
                                DAY(dt_nascimento) AS DAY,dt_nascimento')
                    ->where('idunidade', $idunidade)
                    ->where('user_id', $idaluno)
                    ->where('user_id', $idaluno)
                    ->get();

            $diaHj = date('d');
            $mesHj = date('m');

            if ($diaHj == $dataAniver[0]->DAY && $mesHj == $dataAniver[0]->month):
                $processo = Processoaluno::select('id')
                        ->where('idunidade', $idunidade)
                        ->where('idaluno', $idaluno)
                        ->where('idprocesso', $idprocesso)
                        ->where('dtagenda', date('Y-m-d'))
                        ->get();

                if (sizeof($processo) <= 0):
                    $dataProc['idunidade'] = $idunidade;
                    $dataProc['idaluno'] = $idaluno;
                    $dataProc['idprocesso'] = $idprocesso;
                    $dataProc['dtstart'] = date('Y-m-d H:i:s');
                    $dataProc['dtagenda'] = date('Y-m-d');
                    $dataProc['dtbaixa'] = '';
                    $dataProc['idprofessor'] = 0;
                    $dataProc['stprocesso'] = 'P';
                    $dataProc['idregistro'] = 0;

                    $processo = new Processoaluno();
                    if ($processo->create($dataProc)):
                        $retorno['title'] = 'Sucesso!';
                        $retorno['type'] = 'success';
                        $retorno['text'] = 'Processo gerado com sucesso!';
                    //return $retorno;
                    else:
                        $retorno['title'] = 'Erro!';
                        $retorno['type'] = 'error';
                        $retorno['text'] = 'Erro ao gerar processo!';
                    // return $retorno;
                    endif;
                else:
                    $retorno['title'] = 'Erro 1!';
                    $retorno['type'] = 'error';
                    $retorno['text'] = 'processo de aniversário já gerado!';
                endif;

            else:
                $retorno['title'] = 'Erro 2!';
                $retorno['type'] = 'error';
                $retorno['text'] = 'aluno não esta de aniversário!';
            endif;

            return $retorno;

        endif;
    }

    public function geraProcessoAniversarioRb() {
        $idprocesso = 5;
        $ret = $this->verificaProcessoAtivo($idprocesso);

        if ($ret == 'S'):

            $sql = "select dt_nascimento, user_id as idaluno, idunidade from user_dados where DAY(dt_nascimento) = " . date('d')
                    . " and MONTH(dt_nascimento) = " . date('m');

            $dataAniver = DB::select($sql);
            /* $dataAniver = UserDados::selectRaw('EXTRACT
              MONTH(dt_nascimento) AS month,
              DAY(dt_nascimento) AS DAY,dt_nascimento')
              ->where('DAY(dt_nascimento)', date('m'))
              //->where('DAY(dt_nascimento)', date('m'))
              ->get(); */

            $diaHj = date('d');
            $mesHj = date('m');
            foreach ($dataAniver as $key => $value) {
                //if($diaHj==$dataAniver[0]->DAY && $mesHj==$dataAniver[$key]->month):
                $processo = Processoaluno::select('id')
                        ->where('idunidade', $dataAniver[$key]->idunidade)
                        ->where('idaluno', $dataAniver[$key]->idaluno)
                        ->where('idprocesso', $idprocesso)
                        ->where('dtagenda', date('Y-m-d'))
                        ->get();

                if (sizeof($processo) <= 0):
                    $dataProc['idunidade'] = $dataAniver[$key]->idunidade;
                    $dataProc['idaluno'] = $dataAniver[$key]->idaluno;
                    $dataProc['idprocesso'] = $idprocesso;
                    $dataProc['dtstart'] = date('Y-m-d H:i:s');
                    $dataProc['dtagenda'] = date('Y-m-d');
                    $dataProc['dtbaixa'] = '';
                    $dataProc['idprofessor'] = 0;
                    $dataProc['stprocesso'] = 'P';
                    $dataProc['idregistro'] = 0;

                    $processo = new Processoaluno();
                    if ($processo->create($dataProc)):
                        $retorno['title'] = 'Sucesso!';
                        $retorno['type'] = 'success';
                        $retorno['text'] = 'Processo gerado com sucesso!';
                    //return $retorno;
                    else:
                        $retorno['title'] = 'Erro!';
                        $retorno['type'] = 'error';
                        $retorno['text'] = 'Erro ao gerar processo!';
                    // return $retorno;
                    endif;
                else:
                    $retorno['title'] = 'Erro 1!';
                    $retorno['type'] = 'error';
                    $retorno['text'] = 'processo de aniversário já gerado!';
                endif;
            }

            return $retorno;
        endif;
    }

    public function geraProcessoTreinoNovo($idaluno, $idunidade, $idtreino) {


        $idprocesso = 2;
        $idprocvencido = 3;
        $ret = $this->verificaProcessoAtivo($idprocesso);

        if ($ret == 'S'):

            //aqui ler processo e ver se ja foi gerado p esse agendamento
            $processo = Processoaluno::select('id')
                    ->where('idunidade', $idunidade)
                    ->where('idregistro', $idtreino)
                    ->where('idaluno', $idaluno)
                    ->where('idprocesso', $idprocesso)
                    ->get();


            ///pega processo treino vewncido em aberto
            $vct = Processoaluno::where('idaluno', $idaluno)
                    ->where('idunidade', $idunidade)
                    ->where('idprocesso', $idprocvencido)
                    ->where('stprocesso', 'P')
                    ->update(['stprocesso' => 'R']);



            if (sizeof($processo) <= 0):
                $dataProc['idunidade'] = $idunidade;
                $dataProc['idaluno'] = $idaluno;
                $dataProc['idprocesso'] = $idprocesso;
                $dataProc['dtstart'] = date('Y-m-d H:i:s');
                $dataProc['dtagenda'] = date('Y-m-d');
                $dataProc['dtbaixa'] = '';
                $dataProc['idprofessor'] = 0;
                $dataProc['stprocesso'] = 'P';
                $dataProc['idregistro'] = $idtreino;

                $processo = new Processoaluno();
                if ($processo->create($dataProc)):
                    $retorno['title'] = 'Sucesso!';
                    $retorno['type'] = 'success';
                    $retorno['text'] = 'Processo gerado com sucesso!';
                    return $retorno;
                else:
                    $retorno['title'] = 'Erro!';
                    $retorno['type'] = 'error';
                    $retorno['text'] = 'Erro ao gerar processo!';
                    return $retorno;
                endif;
            endif;
            return $vct;
        endif;
    }

    public function geraProcessoEtapa($idaluno, $idunidade) {  // ok
        $idprocesso = 6;
        $ret = $this->verificaProcessoAtivo($idprocesso);

        if ($ret == 'S'):
            $etapas = DB::select("select pe.id as etapa, pp.id as projeto, pe.etapa_data from prescricao_etapa as pe, prescricao_projeto as pp where"
                            . " pp.aluno_id = $idaluno "
                            . " and pp.unidade_id = $idunidade"
                            . " and pe.etapa_status = 'A'"
                            . " and pe.etapa_data < " . date('Y-m-d')
                            . " and pp.id = pe.projeto_id");

            if (sizeof($etapas) > 0):



                foreach ($etapas as $key => $value) {

                    $processo = Processoaluno::select('id')
                            ->where('idunidade', $idunidade)
                            ->where('idregistro', $etapas[$key]->etapa)
                            ->where('idaluno', $idaluno)
                            ->where('idprocesso', $idprocesso)
                            ->get();

                    if (sizeof($processo) <= 0):
                        $dataProc['idunidade'] = $idunidade;
                        $dataProc['idaluno'] = $idaluno;
                        $dataProc['idprocesso'] = $idprocesso;
                        $dataProc['dtstart'] = date('Y-m-d H:i:s');
                        $dataProc['dtagenda'] = date('Y-m-d', strtotime($etapas[$key]->etapa_data)); //$etapas[$key]->etapa_data;
                        $dataProc['dtbaixa'] = '';
                        $dataProc['idprofessor'] = 0;
                        $dataProc['stprocesso'] = 'P';
                        $dataProc['idregistro'] = $etapas[$key]->etapa;

                        $processo = new Processoaluno();
                        if ($processo->create($dataProc)):
                            $retorno['title'] = 'Sucesso!';
                            $retorno['type'] = 'success';
                            $retorno['text'] = 'Processo gerado com sucesso!';
                            return $etapas;
                        else:
                            $retorno['title'] = 'Erro!';
                            $retorno['type'] = 'error';
                            $retorno['text'] = 'Erro ao gerar processo!';
                            return $etapas;
                        endif;
                    endif;
                    return $etapas;
                }
            else:
                $retorno['title'] = 'Erro!';
                $retorno['type'] = 'error';
                $retorno['text'] = 'Sem agendamentos !';
                return $etapas;
            endif;


            return $etapas;
        endif;
    }

    public function geraProcessoEtapaRb() {  // ok
        $idprocesso = 6;
        $ret = $this->verificaProcessoAtivo($idprocesso);

        if ($ret == 'S'):

            $etapas = DB::select("select pe.id as etapa, pp.id as projeto, pe.etapa_data, pp.aluno_id,pp.unidade_id from prescricao_etapa as pe, prescricao_projeto as pp where"
                            . " pe.etapa_status = 'A'"
                            . " and pe.etapa_data < " . date('Y-m-d')
                            . " and pp.id = pe.projeto_id");

            if (sizeof($etapas) > 0):



                foreach ($etapas as $key => $value) {

                    $processo = Processoaluno::select('id')
                            ->where('idunidade', $etapas[$key]->unidade_id)
                            ->where('idregistro', $etapas[$key]->etapa)
                            ->where('idaluno', $etapas[$key]->aluno_id)
                            ->where('idprocesso', $idprocesso)
                            ->get();

                    if (sizeof($processo) <= 0):
                        $dataProc['idunidade'] = $etapas[$key]->unidade_id;
                        $dataProc['idaluno'] = $etapas[$key]->aluno_id;
                        $dataProc['idprocesso'] = $idprocesso;
                        $dataProc['dtstart'] = date('Y-m-d H:i:s');
                        $dataProc['dtagenda'] = date('Y-m-d', strtotime($etapas[$key]->etapa_data)); //$etapas[$key]->etapa_data;
                        $dataProc['dtbaixa'] = '';
                        $dataProc['idprofessor'] = 0;
                        $dataProc['stprocesso'] = 'P';
                        $dataProc['idregistro'] = $etapas[$key]->etapa;

                        $processo = new Processoaluno();
                        if ($processo->create($dataProc)):
                            $retorno['title'] = 'Sucesso!';
                            $retorno['type'] = 'success';
                            $retorno['text'] = 'Processo gerado com sucesso!';
                            return $etapas;
                        else:
                            $retorno['title'] = 'Erro!';
                            $retorno['type'] = 'error';
                            $retorno['text'] = 'Erro ao gerar processo!';
                            return $etapas;
                        endif;
                    endif;
                    return $etapas;
                }
            else:
                $retorno['title'] = 'Erro!';
                $retorno['type'] = 'error';
                $retorno['text'] = 'Sem agendamentos !';
                return $etapas;
            endif;


            return $etapas;
        endif;
    }

    public function geraProcessoProjeto($idaluno, $idunidade) {  // ok
        $idprocesso = 7;
        $ret = $this->verificaProcessoAtivo($idprocesso);

        if ($ret == 'S'):
            $etapas = DB::select("select pp.id as projeto, pp.projeto_data from prescricao_projeto as pp where"
                            . " pp.aluno_id = $idaluno "
                            . " and pp.unidade_id = $idunidade"
                            . " and pp.projeto_status = 'A'"
                            . " and pp.projeto_data < " . date('Y-m-d')
            );

            if (sizeof($etapas) > 0):



                foreach ($etapas as $key => $value) {

                    $processo = Processoaluno::select('id')
                            ->where('idunidade', $idunidade)
                            ->where('idregistro', $etapas[$key]->projeto)
                            ->where('idaluno', $idaluno)
                            ->where('idprocesso', $idprocesso)
                            ->get();

                    if (sizeof($processo) <= 0):
                        $dataProc['idunidade'] = $idunidade;
                        $dataProc['idaluno'] = $idaluno;
                        $dataProc['idprocesso'] = $idprocesso;
                        $dataProc['dtstart'] = date('Y-m-d H:i:s');
                        $dataProc['dtagenda'] = date('Y-m-d', strtotime($etapas[$key]->projeto_data)); //$etapas[$key]->etapa_data;
                        $dataProc['dtbaixa'] = '';
                        $dataProc['idprofessor'] = 0;
                        $dataProc['stprocesso'] = 'P';
                        $dataProc['idregistro'] = $etapas[$key]->projeto;

                        $processo = new Processoaluno();
                        if ($processo->create($dataProc)):
                            $retorno['title'] = 'Sucesso!';
                            $retorno['type'] = 'success';
                            $retorno['text'] = 'Processo gerado com sucesso!';
                            return $etapas;
                        else:
                            $retorno['title'] = 'Erro!';
                            $retorno['type'] = 'error';
                            $retorno['text'] = 'Erro ao gerar processo!';
                            return $etapas;
                        endif;
                    endif;
                    return $etapas;
                }
            else:
                $retorno['title'] = 'Erro!';
                $retorno['type'] = 'error';
                $retorno['text'] = 'Sem agendamentos !';
                return $etapas;
            endif;


            return $etapas;
        endif;
    }

    public function geraProcessoProjetoRb() {  // ok
        $idprocesso = 7;
        $ret = $this->verificaProcessoAtivo($idprocesso);

        if ($ret == 'S'):
            $etapas = DB::select("select pp.id as projeto, pp.projeto_data, pp.aluno_id, pp.unidade_id from prescricao_projeto as pp where"
                            . " pp.projeto_status = 'A'"
                            . " and pp.projeto_data < " . date('Y-m-d')
            );

            if (sizeof($etapas) > 0):



                foreach ($etapas as $key => $value) {

                    $processo = Processoaluno::select('id')
                            ->where('idunidade', $etapas[$key]->unidade_id)
                            ->where('idregistro', $etapas[$key]->projeto)
                            ->where('idaluno', $etapas[$key]->aluno_id)
                            ->where('idprocesso', $idprocesso)
                            ->get();

                    if (sizeof($processo) <= 0):
                        $dataProc['idunidade'] = $etapas[$key]->unidade_id;
                        $dataProc['idaluno'] = $etapas[$key]->aluno_id;
                        $dataProc['idprocesso'] = $idprocesso;
                        $dataProc['dtstart'] = date('Y-m-d H:i:s');
                        $dataProc['dtagenda'] = date('Y-m-d', strtotime($etapas[$key]->projeto_data)); //$etapas[$key]->etapa_data;
                        $dataProc['dtbaixa'] = '';
                        $dataProc['idprofessor'] = 0;
                        $dataProc['stprocesso'] = 'P';
                        $dataProc['idregistro'] = $etapas[$key]->projeto;

                        $processo = new Processoaluno();
                        if ($processo->create($dataProc)):
                            $retorno['title'] = 'Sucesso!';
                            $retorno['type'] = 'success';
                            $retorno['text'] = 'Processo gerado com sucesso!';
                            return $etapas;
                        else:
                            $retorno['title'] = 'Erro!';
                            $retorno['type'] = 'error';
                            $retorno['text'] = 'Erro ao gerar processo!';
                            return $etapas;
                        endif;
                    endif;
                    return $etapas;
                }
            else:
                $retorno['title'] = 'Erro!';
                $retorno['type'] = 'error';
                $retorno['text'] = 'Sem agendamentos !';
                return $etapas;
            endif;


            return $etapas;
        endif;
    }

    public function geraProcessoAusencia($idaluno, $idunidade) {  // ok
        $idprocesso = 4;
        $ret = $this->verificaProcessoAtivo($idprocesso);

        if ($ret == 'S'):
            $processo = DB::select("select max(dtinicio) as ultimadata from historicotreino where idaluno = $idaluno and idunidade = $idunidade");

            $dateStart = new \DateTime(date($processo[0]->ultimadata));
            $dateNow = new \DateTime(date('Y-m-d'));
            $dateDiff = $dateStart->diff($dateNow);

            if (sizeof($processo)):
                if ($dateDiff->d > 10):
                    $dataProc['idunidade'] = $idunidade;
                    $dataProc['idaluno'] = $idaluno;
                    $dataProc['idprocesso'] = $idprocesso;
                    $dataProc['dtstart'] = date('Y-m-d H:i:s');
                    $dataProc['dtagenda'] = $processo[0]->ultimadata;
                    $dataProc['dtbaixa'] = '';
                    $dataProc['idprofessor'] = 0;
                    $dataProc['stprocesso'] = 'P';
                    $dataProc['idregistro'] = 0;

                    $processo = new Processoaluno();
                    if ($processo->create($dataProc)):
                        $retorno['title'] = 'Sucesso!';
                        $retorno['type'] = 'success';
                        $retorno['text'] = 'Processo gerado com sucesso!';
                        return $retorno;
                    else:
                        $retorno['title'] = 'Erro!';
                        $retorno['type'] = 'error';
                        $retorno['text'] = 'Erro ao gerar processo!';
                        return $retorno;
                    endif;

                endif;
            endif;

            return $dateDiff->d;
        endif;
    }

    public function geraProcessoAgendamento($idaluno, $idunidade) {  // ok
        $idprocesso = 1;
        $ret = $this->verificaProcessoAtivo($idprocesso);

        if ($ret == 'S'):

            $result = Agendaalunoprograma::select('id', 'nmagenda', 'idaluno', 'idunidade', 'dtagenda')
                    ->where('idunidade', $idunidade)
                    ->where('idaluno', $idaluno)
                    ->where('dtagenda', '<=', date('Y-m-d'))
                    ->where('stagenda', 'P')
                    ->get();

            if (sizeof($result) > 0):



                foreach ($result as $key => $value) {

                    //aqui ler processo e ver se ja foi gerado p esse agendamento
                    $processo = Processoaluno::select('id')
                            ->where('idunidade', $idunidade)
                            ->where('idregistro', $result[$key]->id)
                            ->where('idaluno', $idaluno)
                            ->where('idprocesso', $idprocesso)
                            ->get();

                    if (sizeof($processo) <= 0):
                        $dataProc['idunidade'] = $idunidade;
                        $dataProc['idaluno'] = $idaluno;
                        $dataProc['idprocesso'] = $idprocesso;
                        $dataProc['dtstart'] = date('Y-m-d H:i:s');
                        $dataProc['dtagenda'] = $result[$key]->dtagenda;
                        $dataProc['dtbaixa'] = '';
                        $dataProc['idprofessor'] = 0;
                        $dataProc['stprocesso'] = 'P';
                        $dataProc['idregistro'] = $result[$key]->id;

                        $processo = new Processoaluno();
                        if ($processo->create($dataProc)):
                            $retorno['title'] = 'Sucesso!';
                            $retorno['type'] = 'success';
                            $retorno['text'] = 'Processo gerado com sucesso!';
                            return $retorno;
                        else:
                            $retorno['title'] = 'Erro!';
                            $retorno['type'] = 'error';
                            $retorno['text'] = 'Erro ao gerar processo!';
                            return $retorno;
                        endif;
                    endif;
                    return $processo;
                }
            else:
                $retorno['title'] = 'Erro!';
                $retorno['type'] = 'error';
                $retorno['text'] = 'Sem agendamentos !';
                return $retorno;
            endif;

        endif;
    }

    public function geraProcessoAgendamentoRb() {  // ok
        $idprocesso = 1;
        $ret = $this->verificaProcessoAtivo($idprocesso);

        if ($ret == 'S'):
            $result = Agendaalunoprograma::select('id', 'nmagenda', 'idaluno', 'idunidade', 'dtagenda')
                    ->where('dtagenda', '<', date('Y-m-d'))
                    ->where('stagenda', 'P')
                    ->get();

            if (sizeof($result) > 0):



                foreach ($result as $key => $value) {

                    //aqui ler processo e ver se ja foi gerado p esse agendamento
                    $processo = Processoaluno::select('id')
                            ->where('idunidade', $result[$key]->idunidade)
                            ->where('idregistro', $result[$key]->id)
                            ->where('idaluno', $result[$key]->idaluno)
                            ->where('idprocesso', $idprocesso)
                            ->get();

                    if (sizeof($processo) <= 0):
                        $dataProc['idunidade'] = $result[$key]->idunidade;
                        $dataProc['idaluno'] = $result[$key]->idaluno;
                        $dataProc['idprocesso'] = $idprocesso;
                        $dataProc['dtstart'] = date('Y-m-d H:i:s');
                        $dataProc['dtagenda'] = $result[$key]->dtagenda;
                        $dataProc['dtbaixa'] = '';
                        $dataProc['idprofessor'] = 0;
                        $dataProc['stprocesso'] = 'P';
                        $dataProc['idregistro'] = $result[$key]->id;

                        $processo = new Processoaluno();
                        if ($processo->create($dataProc)):
                            $retorno['title'] = 'Sucesso!';
                            $retorno['type'] = 'success';
                            $retorno['text'] = 'Processo gerado com sucesso!';
                            return $retorno;
                        else:
                            $retorno['title'] = 'Erro!';
                            $retorno['type'] = 'error';
                            $retorno['text'] = 'Erro ao gerar processo!';
                            return $retorno;
                        endif;
                    endif;
                    return $processo;
                }
            else:
                $retorno['title'] = 'Erro!';
                $retorno['type'] = 'error';
                $retorno['text'] = 'Sem agendamentos !';
                return $retorno;
            endif;
        endif;
    }

}
