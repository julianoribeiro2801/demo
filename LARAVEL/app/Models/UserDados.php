<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class UserDados extends Model
{
    protected $table = 'user_dados';
    public $timestamps = ['created_at', 'updated_at'];
    //public $casts = ['dt_nascimento' => 'date'];

    protected $fillable = [
        'user_id',
        'idunidade',
        'telefone',
        'celular',
        'dt_nascimento',
        'cpf',
        'site',
        'origem',
        'endereco',
        'numero',
        'cep',
        'bairro',
        'idestado',
        'idcidade',
        'genero',
        'controle_id',
        'profile_type',
        'complemento',
        'objetivo_id',
        'situacao',
        'situacaomatricula',
        'motivo'  // motivo , usado pra fazer update pela trigger
            
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function setDtNascimentoAttribute($value)
    {
        $this->attributes['dt_nascimento'] = $value;
        //		$this->attributes['dt_nascimento'] = Carbon::parse($value);
    }

    public function objective()
    {
        return $this->hasOne(Objetivotreino::class, 'id', 'objetivo_id');
    }

    public function type()
    {
        return $this->hasOne(ProfileType::class, 'id', 'profile_type');
    }

    public function genre()
    {
        return $this->genero ? ($this->genero == 'M' ? 'Maculino' : 'Feminino') : 'Não informado';
    }
}
