<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Historicotreino extends Model
{
    protected $table = 'historicotreino';
    protected $fillable = [  'IDALUNO','IDUNIDADE','IDTREINO', 'TPATIVIDADE','DTINICIO',
                                 'DTFIM','NRDIASUGESTAO','STTREINO','DTREGISTRO', 'QTTON',
                                 'DURACAO','TEMPOGASTO','DIFERENCATEMPO','NRDIATREINO',
                                 'TEMPO_PARADO'
                              ];
}
