<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Nivelgfm extends Model
{
    protected $table = 'nivel_gfm';
    
    protected $fillable = [
        'idgfm','idunidade','idnivel'
        
    ];
}
