<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Configura extends Model
{
    
    protected $table = 'configura';
    
    protected $fillable = ['idunidade','stadiciona','codigo_atualiza','sistema','chave_sistema'];
    
    
}
