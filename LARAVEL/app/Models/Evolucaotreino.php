<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Evolucaotreino extends Model
{
    protected $table = 'evolucaotreino';
    
    protected $fillable = [
            'IDALUNO' ,  'IDUNIDADE' ,  'IDPRESCRICAO' ,  'NRDIATREINO' ,  'NRLANCAMENTO' ,
            'NRLANCAMENTODIA' ,  'QTREPETICAO' ,  'QTSERIE' ,  'QTCARGA' ,  'QTINTERVALO' ,
            'IDEXERCICIO',  'QTDIA' ,  'SEQEXERCICIO' ,  'STFIXO' ,  'QTINTERVALO_1' ,  'DTREGISTRO' ,
            'QTAEROBIO' ,  'STCONCLUIDO' ,  'QTTON'
    ];
}
