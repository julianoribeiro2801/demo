<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TreinoNivel extends Model
{
    protected $table = 'treino_nivel';
    protected $fillable = ['id_unidade','nivel_descricao','nivel_status'];
}
