<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Telatemapadraoapp extends Model
{
    protected $table = 'tela_tema_padrao';
    protected $fillable = [ 'tema_id',  'nometela',  'cor_background', 
                            'cor_botao','cor_escrita_botao','cor_escrita_senha','logomarca'];
}
