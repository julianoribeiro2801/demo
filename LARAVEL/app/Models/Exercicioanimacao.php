<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Exercicioanimacao extends Model
{
    protected $table = 'exercicioanimacao';
    
    protected $fillable = [
       'idexercicioanimacao','bianimacaobs64', 'dsurl'
        
    ];
}
