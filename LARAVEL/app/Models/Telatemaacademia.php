<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Telatemaacademia extends Model
{
    protected $table = 'tela_tema_academia';
    protected $fillable = [   'nometela' ,
                              'idunidade' ,
                              'tema_id' ,
                              'cor_background' ,
                              'cor_topo' ,
                              'cor_rodape' ,
                              'cor_texto_topo' ,
                              'cor_texto_rodape',
                              'cor_nome_aluno' ,
                              'cor_texto_telamensagem',
                              'cor_mensagem_corsim',
                              'cor_mensagem_cornao',
        
                              'cor_gfm_corsim',
                              'cor_textogfm',
                              'cor_fundodiagfm',
                              'cor_gfm_cornao',
                              'cor_icone_gfm',
        
                              'cor_spm_corsim',
                              'cor_textospm',
                              'cor_fundodiaspm',
                              'cor_spm_cornao',
                              'cor_icone_spm',
        
                              'cor_textocross',
                              'cor_botao_partiu',
                              'cor_botao_iniciar',
                              'cor_cross_corsim',
                              'cor_cross_cornao',
                              'cor_fundodesccross',
                              'cor_textomenu',
                              'cor_fundomenu',
                              'cor_iconemenu',
                              'logomarca_home',
                              'transparencia',
                              'cor_texto_musc',
                              'cor_ativo_musc',
                              'cor_inativo_musc',
                              'cor_partiu_musc',
                              'cor_iniciar_musc',
                              'cor_texto_treino',
                              'cor_treino_cornao',
                              'cor_treino_corsim',
                              'cor_treino_corselec',
                              'cor_texto_chat',
                              'cor_fundo_chat',
                              'cor_fundo_enviada',
                              'cor_fundo_recebida',
                              'cor_texto_contato',
                              'cor_fundo_contato',
                              'cor_iniciar_contato',
                              'cor_texto_empresa',
                              'cor_fundo_empresa',
                              //clube
                              'cor_texto_clube',
                              'cor_cab_clube',
                              'cor_impar_clube',
                              'cor_par_clube',
                              //ciclisno
                              'cor_textociclismo',
                              'cor_fundodescciclismo',
                              'cor_botao_partiuciclismo',
                              'cor_botao_iniciarciclismo',
                              'cor_ciclismo_corsim',
                              'cor_ciclismo_cornao',
                              //corrida
                              'cor_textocorrida',
                              'cor_fundodesccorrida',
                              'cor_botao_partiucorrida',
                              'cor_botao_iniciarcorrida',
                              'cor_corrida_corsim',
                              'cor_corrida_cornao',
                              //reserva
                              'cor_textoreserva',
                              'cor_fundoreserva',
                              'cor_botao_reserva',
                              'cor_boneco_reserva',
                              //exercicio
                              'cor_textoexer',
                              'cor_fundoexer',
                              'cor_imparexer',
                              'cor_parexer',
                              //seleciona treino
                              'cor_iconeseleciona',
                              'cor_fundoiconeseleciona',
                              'cor_selecionaimpar',
                              'cor_selecionapar',
                              ///empresa
                              'cor_textoempresa',
                              'cor_fundoempresa',
                              'cor_cabecempresa',
                              //planejamento
                              'cor_textoplan',
                              'cor_fundoplan',
                              'cor_diaplan',
                              'cor_horaplan',
                              'cor_atvplan',
                              ///conquistas
                              'cor_textoconquista',
                              'cor_fundoconquista',
                              ///coach
                              'cor_fundo_coach',
                              'cor_texto_coach',
                              'transparencia_coach'

        
        
                
        ];
}
