<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Nivelhabilidade extends Model
{
    protected $table = 'nivelhabilidade';
    protected $fillable = ['idmodalidade', 'nmnivel','dscriterio','idunidade','idobjetivo'];
        
    //    public function niveis(){
     //       return $this->hasOne(Objetivotreino::class, 'idobjetivo')->with(['id', 'idobjetivo']);
     //   }
}
