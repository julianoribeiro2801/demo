<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PrescricaoCalendario extends Model {
	protected $table = 'prescricao_calendario'; 
	protected $fillable = ['idunidade', 'user_id_aluno', 'user_id_prof', 'desc_prescricao', 'data_prescricao', 'tipo_prescricao','treino_padrao_id'];
}