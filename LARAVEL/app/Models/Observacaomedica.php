<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Observacaomedica extends Model
{
    protected $table = 'observacaomedica';
    protected $fillable = ['idaluno','idunidade','observacao'];
}
