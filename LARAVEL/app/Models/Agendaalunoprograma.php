<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Agendaalunoprograma extends Model
{
    protected $table = 'agenda_alunoprograma';
    protected $fillable = ['idunidade','idaluno', 'dtagenda', 'hragenda','idprogramatreinamento','nmagenda','stagenda'];
}
