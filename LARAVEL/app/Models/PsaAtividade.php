<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PsaAtividade extends Model
{
    protected $table = 'psa_atividade';
    protected $fillable = ['idaluno','idprofessor','idunidade','dom_atv','dom_hora','seg_atv','seg_hora','ter_atv','ter_hora','qua_atv','qua_hora','qui_atv','qui_hora','sex_atv','sex_hora','sab_atv','sab_hora'];
}
