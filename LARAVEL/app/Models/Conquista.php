<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Conquista extends Model
{
    protected $table = 'conquista';
    
    protected $fillable = [
        'nmconquista','idtipoconquista','idatividade'
    ];
}
