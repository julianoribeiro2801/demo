<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Fotouser extends Model
{
    protected $table = 'foto_user';
    protected $fillable = ['user_id','unidade_id','user_foto','user_peso','tipo_foto'];
}
