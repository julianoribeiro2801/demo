<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Prescricao extends Model
{
    protected $table = 'prescricao';
    
    protected $fillable = [
          'idaluno','idunidade', 'idobjetivotreino', 'idprescricao',
          'idperiodizacao','dtprescricao','dtinicio','dtreavaliacao',
          'nrtreinos','stativo','idprofessor'
    ];
}
