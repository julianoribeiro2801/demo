<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Gfm extends Model
{
    protected $table = 'gfm';
    protected $fillable = ['idunidade', 'idlocal', 'idprograma', 'idfuncionario', 'hora_inicio','nivel','objetivo','gfmspm', 'duracao', 'diasemana', 'valor', 'capacidade', 'fator','privado'];
}
