<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rankingconquista extends Model
{
    protected $table = 'rankingconquista';
    protected $fillable = [
                            'idunidade',  'idaluno',  'idconquista',  'nrposicao',
                            'qtconquista',  'dtregistro'
                          ];
}
