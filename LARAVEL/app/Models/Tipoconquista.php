<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tipoconquista extends Model
{
    protected $table = 'tipoconquista';
    
    protected $fillable = [
        'dstipoconquista','tpmedida'
    ];
}
