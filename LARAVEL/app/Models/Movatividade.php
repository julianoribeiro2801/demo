<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Movatividade extends Model
{
    protected $table = 'movatividade';
    
    protected $fillable = [
              'idunidade' , 'idaluno' ,  'idatividade' ,  'qtatividade',  'dtregistro',
              'nrlancprescricaocross',  'dtdia',  'stmovatividade' ,  'vinculo_id'
    ];
}
