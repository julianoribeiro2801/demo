<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TempoEstimado extends Model
{
    protected $table = 'tempo_estimado';
    protected $fillable = ['client_id', 'treino_id', 'letra', 'tempoestimado', 'tempomusculacao'];
}
