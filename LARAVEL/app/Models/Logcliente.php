<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Logcliente extends Model
{
    protected $table = 'cliente_log';
    
    protected $fillable = [
        'id','idunidade','idaluno','idprofessor','dtalteracao','situacao', 'motivo'
        
    ];
}
