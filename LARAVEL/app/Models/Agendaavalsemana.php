<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Agendaavalsemana extends Model
{
    
    protected $table = 'agendaavalsemana';
    protected $fillable = ['diasemana','duracao', 'horario','idunidade'];
    
}
