<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Gfmconfigura extends Model
{
    protected $table = 'gfm_configura';
    protected $fillable = [
        'idunidade', 'diasantecedencia', 'aulasatecipadas',
        'intervalo', 'remove', 'oferecevaga', 'premium',
        'indice_inicial','indice_final','user_update'
    ];
}
