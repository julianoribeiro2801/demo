<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Serietreino extends Model
{
    protected $table = 'serietreino';
    
    protected $fillable = [
            'NRSERIE' ,  'IDALUNO' ,  'IDUNIDADE' ,  'IDPRESCRICAO',  'NRDIATREINO' ,
            'NRLANCAMENTO',  'NRLANCAMENTODIA',  'QTREPETICAO' ,  'DTREGISTRO' ,  'QTCARGA'
       ];
}
