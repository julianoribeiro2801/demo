<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Moduloacademia extends Model
{
    protected $table = 'modulos_has_unidade';
    
    protected $fillable = [
        'id_modulo','id_unidade','status','pago'
    ];
}
