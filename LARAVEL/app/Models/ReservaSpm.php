<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReservaSpm extends Model
{
    protected $table = 'spm_reserva';
    protected $fillable = ['idunidade', 'idspm', 'n_reservas'];
}
