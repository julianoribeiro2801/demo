<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Agendaavalaluno extends Model
{
    protected $table = 'agendaavalaluno';
    protected $fillable = ['idunidade','agenda_id', 'idaluno', 'iddiasemana', 'dtagenda', 'horario'];
}
