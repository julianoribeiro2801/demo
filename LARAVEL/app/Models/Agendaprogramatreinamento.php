<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Agendaprogramatreinamento extends Model
{
    protected $table = 'agenda_programatreinamento';
    protected $fillable = ['idunidade', 'idprogramatreinamento','nmagenda','nrdias','st'];
}
