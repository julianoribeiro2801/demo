<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExercicioBlacklist extends Model
{
    protected $table = 'exercicio_blacklist';
    protected $fillable = ['exercicio_id','unidade_id','stexercicio'];
}
