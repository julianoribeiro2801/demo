<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notificacao extends Model
{
    protected $table = 'notificacaoconquista';
    
    protected $fillable = [
        'idaluno','idunidade','idconquista'
    ];
}
