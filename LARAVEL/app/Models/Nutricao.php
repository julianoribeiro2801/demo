<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Nutricao extends Model
{
    protected $table = 'nutricao';
    protected $fillable = ['nutricao_nome', 'nutricao_descricao', 'nutricao_datainicio', 'nutricao_datatermino', 'nutricao_nivel', 'nutricao_padrao', 'aluno_id', 'professor_id', 'unidade_id'];
}
