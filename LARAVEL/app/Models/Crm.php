<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Crm extends Model
{
    protected $table = 'crm';

    protected $fillable = [
        'de',
        'para',
        'acao',
        'descricao'
    ];

    public function __toString()
    {
        return "{$this->getUsuario()->name} {$this->getAcao()->descricao} {$this->getCliente()->name}\n\r {$this->getDate()}";
    }

    public function getText()
    {
        return "{$this->getUsuario->name} {$this->getAcao->descricao} {$this->getCliente->name}\n\r";
    }

    public function getAcao()
    {
        return $this->hasOne(CrmAcao::class, 'id', 'acao');
    }

    public function getDate()
    {
        return Carbon::parse($this->created_at)->format('d/m/y H:i');
    }

    public function getUsuario()
    {
        return $this->hasOne(User::class, 'id', 'de');
    }

    public function getCliente()
    {
        return $this->hasOne(User::class, 'id', 'para');
    }
}
