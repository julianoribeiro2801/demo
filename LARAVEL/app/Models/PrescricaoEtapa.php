<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PrescricaoEtapa extends Model
{
    protected $table = 'prescricao_etapa';
    protected $fillable = ['projeto_id','etapa_nome','etapa_data','etapa_status','idcategoria'];
}
