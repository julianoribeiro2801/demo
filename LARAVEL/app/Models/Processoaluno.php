<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Processoaluno extends Model
{
    protected $table = 'processo_aluno';
    protected $fillable = [  'id' ,
                             'idunidade' ,
                             'idaluno' ,
                             'idprocesso',
                             'dtstart',
                             'dtagenda',
                             'dtbaixa',
                             'idprofessor',
                             'idregistro',
                             'stprocesso'
                            ];
}
