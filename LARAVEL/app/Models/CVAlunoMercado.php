<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CVAlunoMercado extends Model
{
    protected $table = 'cv_alunocompra';
    
    protected $fillable = [
       'idaluno','idempresa','dtcompra','vlrcompra','percdesconto','vlrdesconto'
        
    ];
}
