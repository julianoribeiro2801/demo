<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Spm extends Model
{
    protected $table = 'spm';
    protected $fillable = ['idunidade', 'idlocal', 'idprograma', 'idfuncionario', 'nivel', 'privado', 'gfmspm', 'hora_inicio', 'duracao', 'diasemana', 'valor', 'capacidade_max', 'capacidade_min', 'equalizador', 'revisado'];
}
