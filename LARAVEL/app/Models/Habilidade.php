<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Habilidade extends Model {
    
    protected $table = 'habilidade';     
    protected $fillable = ['idunidade','idnivel','nmhabilidade'];
    
}