<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Nutricaopadrao extends Model
{
    protected $table = 'nutricao_padrao';
    protected $fillable = ['nutricao_nome', 'nutricao_descricao', 'nutricao_datainicio', 'nutricao_datatermino', 'nutricao_nivel', 'nutricao_padrao', 'professor_id', 'unidade_id'];
}
