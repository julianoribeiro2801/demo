<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class CrmAcao extends Model
{
    protected $table = 'crm_acao';

    protected $fillable = [
        'descricao',
        'icone',
        'titulo',
        'status'
    ];
}
