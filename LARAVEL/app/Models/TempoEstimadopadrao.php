<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TempoEstimadopadrao extends Model
{
    protected $table = 'tempo_estimado_padrao';
    protected $fillable = ['treino_id', 'letra', 'tempoestimado', 'tempomusculacao'];
}
