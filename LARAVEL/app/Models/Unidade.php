<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Unidade extends Model
{
    protected $table = 'unidade';

    protected $fillable = ['logo','idmatriz','user_id','parent_id','fantasia','razao_social','situacao','licenca'];

    public function children()
    {
        return $this->hasMany(Unidade::class, 'parent_id');
    }

    public function parent()
    {
        return $this->belongsTo(Unidade::class, 'parent_id');
    }

    public function matriz()
    {
        return $this->belongsTo(Unidade::class, 'idmatriz');
    }

    public function dados()
    {
        return $this->hasOne(UnidadeDados::class, 'idunidade');
    }
}
