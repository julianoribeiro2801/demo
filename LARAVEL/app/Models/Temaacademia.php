<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Temaacademia extends Model
{
    protected $table = 'tema_academia';
    protected $fillable = [
        'idunidade','idtema'
    ];
}
