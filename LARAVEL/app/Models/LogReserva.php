<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LogReserva extends Model
{
    
    protected $table = 'gfm_reserva_log';
    protected $fillable = ['idaluno', 'idgfm','reserva_id','dtaula'];
}
