<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LogReservaSpm extends Model
{
    protected $table = 'spm_reserva_log';
    protected $fillable = ['idaluno', 'idspm'];
}
