<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LogCancelamento extends Model
{
    
    protected $table = 'gfm_cancelamento_log';
    protected $fillable = ['idaluno', 'idgfm','idunidade','dtaula','dtcancelamento','canceladopor'];
}
