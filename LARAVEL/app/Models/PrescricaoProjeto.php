<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PrescricaoProjeto extends Model
{
    protected $table = 'prescricao_projeto';
    protected $fillable = ['projeto_nome','projeto_data','projeto_status','unidade_id','aluno_id','professor_id'];
}
