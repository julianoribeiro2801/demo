<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Temapadraoapp extends Model
{
    protected $table = 'tema_app';
    protected $fillable = ['idunidade','nome'];
}
