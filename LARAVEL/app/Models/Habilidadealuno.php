<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Habilidadealuno extends Model {
    protected $table = 'aluno_habilidade';     
    protected $fillable = ['idunidade','idhabilidade','idaluno','checado'];
}