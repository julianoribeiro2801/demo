<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Logusuario extends Model
{
    protected $table = 'logusuario';
    protected $fillable = ['dsemail' , 'idaluno', 'idunidade', 'dtlogin','hrlogin','tplogin',
                               'dshash' ];
}
