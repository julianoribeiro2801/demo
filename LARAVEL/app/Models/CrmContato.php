<?php
/**
 * Created by PhpStorm.
 * User: jeandro
 * Date: 05/04/17
 * Time: 23:38
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CrmContato extends Model
{
    public $table = 'crm_contato';

    public $fillable = [
        'idunidade',
        'iduser',
        'crmacao',
        'assunto',
        'efetuado',
        'reagendar',
        'observacao',
        'sender_id',
        'hot'
    ];

    public $casts = [
      'efetuado' => 'boolean'
    ];
}
