<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Carteiraprofessor extends Model
{
    protected $table = 'carteira_professor';
    protected $fillable = [
        'idaluno','idprofessor','idunidade','dtinclusao','dtbaixa','situacao'
    ];
}
