<?php

namespace App\Models;

use App\Notifications\ResetPassword;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Usuariosistema extends Model
{
    use Notifiable;
    protected $table = 'usuariosistema';
    
    protected $fillable = [
       'nmlogin','idunidade','idpessoa','dssenha','stativo'
        
    ];

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token));
    }
}
