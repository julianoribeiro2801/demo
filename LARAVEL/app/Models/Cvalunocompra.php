<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cvalunocompra extends Model
{
    protected $table = 'cv_alunocompra';
    
    protected $fillable = [
       'idaluno', 'idunidade', 'idempresa', 'dtcompra','vlrcompra','percdesconto', 'vlrdesconto'
    ];
}
