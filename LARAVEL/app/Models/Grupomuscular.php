<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Grupomuscular extends Model
{
    protected $table = 'grupomuscular';
    protected $fillable = ['nmgrupo'];
}
