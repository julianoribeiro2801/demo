<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Matriculaturma extends Model
{
    protected $table = 'matricula_turma';

    protected $fillable = [
        'idaluno' ,
        'idaula' ,
        'idunidade' ,
        'stmatricula',
        'dtmatricula',
        'dtbaixa' ,
        'idnivel'
    ];
}
