<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Segmento extends Model
{
    protected $table = 'segmento';
    protected $fillable = ['nmsegmento','dsurlminiatura'];
}
