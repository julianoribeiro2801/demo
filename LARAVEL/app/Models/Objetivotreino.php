<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Services\FilialService;

class Objetivotreino extends Model
{
    protected $table = 'objetivotreino';

    protected $fillable = [
        'nmobjetivo','idunidade','idmodalidade'
    ];
    
    public function niveis()
    {
        return $this->hasMany(Nivelhabilidade::class, 'idobjetivo');
    }
}
