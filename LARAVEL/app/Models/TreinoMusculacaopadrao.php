<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TreinoMusculacaopadrao extends Model
{
    protected $table = 'treino_musculacao_padrao';
    protected $fillable = ['treino_nome_padrao','treino_qtddias','treino_nivel','treino_objetivo','treino_sexo','unidade_id','professor_id','treino_observacao'];
}
