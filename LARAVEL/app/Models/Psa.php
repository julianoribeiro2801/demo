<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Psa extends Model
{
    protected $table = 'psa';
    
    protected $fillable = [
        'idaluno','idunidade','idprescricao',
        'iddiasemana',
        'qtgastocalorico',
        'qtingestaodiaria',
        'qtoperdagordura'

    ];
}
