<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mensagem extends Model
{
    protected $table = 'mensagemaluno';
    
    protected $fillable = [
        'dsmensagem','idaluno','idunidade','idtipomensagem','stlida'
    ];
}
