<?php
namespace App\Models;

use App\Notifications\ResetPassword;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Funcionario extends Model
{
    use Notifiable;

    protected $table = 'users';
    protected $fillable = ['idunidade','name','email','password','role', 'avatar','tipo'];

    public function unidade()
    {
        return $this->belongsTo(Unidade::class, 'idunidade');
    }

    public function getAvatarAttribute($value)
    {
        $file = $this->attributes['avatar'];
        $avatarpadrao="user-a4.jpg";
        if ($file) {
            return $file;
        }else{
            return $avatarpadrao;
        }
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token));
    }
}
