<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reserva extends Model
{
    protected $table = 'gfm_reserva';
    protected $fillable = ['idunidade', 'idgfm', 'n_reservas','capacidade','dtaula','liberouvaga'];
}
