<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Totem extends Model
{
    protected $table = 'totem';
    protected $fillable = ['idaluno','idunidade','tpapp','situacao','dtentrada','hrentrada'];
}
