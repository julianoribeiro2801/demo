<?php
namespace App\Models\Scopes;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class ClientScope implements Scope
{
    public function apply(Builder $builder, Model $model)
    {
        $tenant = request()->session()->get('id_unidade');
        $builder->where('idunidade', $tenant);
    }
}
