<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PrescricaoCalendarioPadrao extends Model {
	
        protected $table = 'prescricao_calendario_padrao'; 
        
	protected $fillable = ['idunidade','idtreino', 'user_id_prof', 'desc_prescricao', 'data_prescricao', 'tipo_prescricao','nome_treino'];
}