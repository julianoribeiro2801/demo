<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Conquistausuario extends Model
{
    protected $table = 'conquistausuario';
    protected $fillable = ['idunidade', 'idaluno', 'idconquista', 'stconquista', 'dtconquista', 'qtconquista', 'stparticipa', 'dtregistro'];
}
