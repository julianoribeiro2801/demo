<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProfileType extends Model
{
    protected $table = 'profileType';
    
    protected $fillable = [
         'objective',
        'description',
        'motivation'
    ];
}
