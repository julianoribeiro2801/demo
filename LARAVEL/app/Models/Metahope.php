<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Metahope extends Model
{
    protected $table = 'metahope';
    
    protected $fillable = [

          'idaluno', 'idunidade', 'dtprazo','dtlancamento','dsmeta' ,'stmeta'
           
    ];
}
