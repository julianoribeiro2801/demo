<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TreinoMusculacao extends Model
{
    protected $table = 'treino_musculacao';
    protected $fillable = ['treino_nome', 'treino_nome_padrao',
                           'treino_qtddias','treino_revisao','treino_nivel',
                           'treino_objetivo','treino_sexo','unidade_id',
                           'aluno_id','professor_id','treino_padrao',
                           'treino_atual', 'treino_observacao'
                          ];
}
