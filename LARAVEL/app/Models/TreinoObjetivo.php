<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TreinoObjetivo extends Model
{
    protected $table = 'objetivotreino';
    protected $fillable = ['nmobjetivo','idunidade'];
}
