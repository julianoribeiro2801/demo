<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Messages extends Model {

    protected $table = 'messages';
    protected $fillable = [

        'push_types',
        'reserva_id',
        'opened',
        'sender_id',
        'receiver_id',
        'message',
        'active',
        'hr_aviso',
        'confirmado'
        
    ];

}
