<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AtividadeBlacklist extends Model
{
    protected $table = 'atividade_blacklist';
    protected $fillable = ['atividade_id','unidade_id'];
}
