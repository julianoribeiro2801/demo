<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PrescricaoModalidadePadrao extends Model {
	
        protected $table = 'prescricao_modalidade_padrao'; 
        
	protected $fillable = ['id' ,'idunidade', 'idobjetivo', 'idnivel','nome_treino' ,'treino_sexo','tipo_prescricao'];
}