<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Resultadogfmaluno extends Model
{
    protected $table = 'resultadogfmaluno';
    
    protected $fillable = [
      'dtaula', 'idgfm', 'idunidade', 'idaluno', 'dtentrada', 'dtregistro'
        
        
    ];
}
