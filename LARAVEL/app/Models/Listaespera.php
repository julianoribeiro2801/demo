<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Listaespera extends Model
{
    protected $table = 'lista_espera_gfm';
    protected $fillable = [
        'idaluno','idgfm','idunidade'
    ];
}
