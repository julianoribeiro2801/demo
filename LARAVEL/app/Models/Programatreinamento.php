<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Programatreinamento extends Model
{
    protected $table = 'programatreinamento';
    protected $fillable = ['idunidade', 'nmprograma'];
        
    //public function agendas(){
//            return $this->hasOne(Agendaprogramatreinamento::class, 'idprogramatreino')->with(['id', 'idprogramatreino']);
    //      }
    public function agendas()
    {
        //return $this->has(Agendaprogramatreinamento::class, 'idprogramatreinamento');
        return $this->hasMany(Agendaprogramatreinamento::class, 'idprogramatreinamento');
    }
}
