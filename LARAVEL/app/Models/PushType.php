<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PushType extends Model
{
    protected $table = 'push_types';
    protected $fillable = ['quando','mensagem', 'tooltip', 'status'];
}
