<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UnidadeDados extends Model
{
    protected $table = 'unidade_dados';
    
    protected $fillable = ['user_id','idunidade','cnpj','inscricao_estadual','endereco','numero','cep','bairro','idestado','idcidade','telefone','celular','email','site','metros'];
}
