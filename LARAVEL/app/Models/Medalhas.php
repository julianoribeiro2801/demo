<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Medalhas extends Model
{
    protected $table = 'medalhas';
    protected $fillable = ['idunidade','type_id', 'user_id', 'status'];
}
