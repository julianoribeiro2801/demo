<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CVEmpresa extends Model
{
    protected $table = 'cv_empresa';
    
    protected $fillable = [
       'idunidade','nmempresa', 'idempresa','percdesconto','cnpj','idsegmento','dtvencimento','excluido'
        
    ];
}
