<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TreinoFichapadrao extends Model
{
    
    
    protected $table = 'treino_ficha_padrao';
    protected $fillable = ['ficha_id_vinculo','super_serie','medida_duracao','medida_intensidade', 'ficha_intervalo','ficha_letra', 'medida', 'ficha_intervalo','ficha_series','ficha_repeticoes','ficha_carga','ficha_observacao','treino_id','exercicio_id','ord'];
}
