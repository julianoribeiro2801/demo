<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Resultadomes extends Model
{
    protected $table = 'resultado_mes';
    
    protected $fillable = [
        'idunidade','idprofessor','mes','ano','ativo'
    ];
}
