

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Diasemana extends Model
{
    protected $table = 'diasemana';
    protected $fillable = [
        'nmdiasemana'
    ];
}
