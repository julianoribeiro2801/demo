<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Versaoapp extends Model {
    protected $table = 'historico_versaoapp';     
    protected $fillable = ['versao','tipo'];
}