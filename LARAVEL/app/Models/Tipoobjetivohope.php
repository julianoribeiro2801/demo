<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tipoobjetivohope extends Model
{
    protected $table = 'tipoobjetivohope';
    
    protected $fillable = [
        'dstipoobjetivo','idunidade'
    ];
}
