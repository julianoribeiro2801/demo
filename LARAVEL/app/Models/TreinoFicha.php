<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TreinoFicha extends Model
{
    protected $table = 'treino_ficha';
    protected $fillable = ['ficha_letra','super_serie', 'ficha_id_vinculo','medida_duracao','medida_intensidade', 'ficha_intervalo','ficha_series','ficha_repeticoes','ficha_carga','ficha_observacao','treino_id','exercicio_id','ord'];
}
