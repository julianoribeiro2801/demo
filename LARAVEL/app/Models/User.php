<?php

namespace App\Models;

use App\Notifications\ResetPassword;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Storage;

class User extends Authenticatable
{
    use Notifiable;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role', 'passo', 'idunidade','device_id', 'avatar',
        'excluido',  'parent_id',
        'remember_token', 'tipo','idcategoria'
    ];

    protected $hidden = [
        'password', 
    ];

    public function dados()
    {
        return $this->hasOne(UserDados::class, 'user_id')->with(['type', 'objective']);
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }

    public function scopeAllByUnidade($query)
    {
        $query->where('idunidade', session('id_unidade'));
    }


    public function getAvatarAttribute($value)
    {
        $file = $this->attributes['avatar'];
        $avatarpadrao="user-a4.jpg";
        if ($file) {
            return $file;
        }else{
            return $avatarpadrao;
        }
    }
    

    public function unidade()
    {
        return $this->belongsTo(Unidade::class, 'idunidade');
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token));
    }
}
