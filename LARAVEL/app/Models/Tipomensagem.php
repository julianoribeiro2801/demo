<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tipomensagem extends Model
{
    protected $table = 'tipomensagem';
    
    protected $fillable = [
        'dstipomensagem','tpmensagem'
    ];
}
