<?php

namespace App\Listeners;

use App\Events\SomeEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Session;
use App\Models\Logusuario;

class UserEventSubscriber {

    /**
     * Handle user login events.
     */
    public function onUserLogin($event) {
        Session::put('id_unidade', $event->user->idunidade);

        $dataLog['dsemail'] = $event->user->email;
        $dataLog['idunidade'] = $event->user->idunidade;
        $dataLog['idaluno'] = $event->user->id;
        $dataLog['dtlogin'] = date('Y-m-d');
        $dataLog['hrlogin'] = date('H:i:s');
        if ($event->user->role == 'cliente') {
            $dataLog['tplogin'] = 2;  //1 = sistema // 2 = app
        } else {
            $dataLog['tplogin'] = 1;  //1 = sistema // 2 = app
        }
        $log = new Logusuario();
        if ($log->create($dataLog)):
        else:
        endif;
    }

    public function onUserLogout($event) {
        
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events) {
        $events->listen(
                'Illuminate\Auth\Events\Login', 'App\Listeners\UserEventSubscriber@onUserLogin'
        );

        $events->listen(
                'Illuminate\Auth\Events\Logout', 'App\Listeners\UserEventSubscriber@onUserLogout'
        );
    }

}
