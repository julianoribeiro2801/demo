<?php
namespace App\Observers;

use App\Models\Unidade;
use App\Models\User;
use App\Services\SendPushService;
use Auth;

class PushObserver
{
    private $sendPushService;
   
    public function __construct(SendPushService $sendPushService)
    {
        $this->sendPushService = $sendPushService;
    }

    public function boasVindas(User $user)
    {
    	return 'teste boasVindas. Observer';
        // $user->cod_ativacao = $this->generateConfimationCode();
        // $user->save();

        // $this->smsService->send($user);
    }

   
}