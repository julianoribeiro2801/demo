<?php

Route::get('ffmpeg', 'TesteController@ffmpeg');

Route::get('/totem', function () {
    return redirect('https://www.sistemapcb.com.br/public/totem/');
});

Route::get('/', function () {
    Session::forget('id_unidade');
    return view('auth/login');
})->middleware('guest')->name('admin.index');

Route::get('/acesso_negado_dono', function () {
    Session::forget('id_unidade');
    return view('acesso_negado_dono');
});

Auth::routes();

Route::get('novo-usuario', 'Auth\NewUserController@welcome')->name('new-user');
// Route::get('tokenexpired', 'Auth\NewUserController@tokenexpired')->name('tokenexpired');

Route::post('novo-usuario', 'Auth\NewUserController@createPassword')->name('createPassword');
Route::get('password/app/reset', 'Auth\ForgotPasswordController@showResetAppPasswordForm')->name('show-reset-app-password');
Route::post('password/app/reset', 'Auth\ForgotPasswordController@resetAppPassword')->name('reset-app-password');

Route::get('/home', 'HomeController@index');
Route::get('/apps', 'HomeController@apps');

// BACKUP
Route::get('/backup/{senha}', 'Backup\BackupController@backup');
Route::get('/lp/{cliente}', 'LpsController@lp');
Route::get('/lpadmin', 'LpsController@index');

################ INICIO DA ADMINMASTER ################
// CADASTRO LEAD
Route::get('/form_lead', 'AdminMasterController@form_lead');
Route::get('/parabens_lead/{id}', 'AdminMasterController@parabens_lead');
Route::post('/cadastrar_lead', 'AdminMasterController@cadastrar_lead');

Route::get('/form_lead2', 'AdminMasterController@form_lead2');
Route::get('/parabens_lead2/{id}', 'AdminMasterController@parabens_lead2');
Route::post('/cadastrar_lead2', 'AdminMasterController@cadastrar_lead2');
Route::get('/bloqueiaTrial', 'AdminMasterController@bloqueiaTrial');
Route::get('/bloqueiaTrialModulo', 'AdminMasterController@bloqueiaTrialModulo');

Route::group(['prefix' => 'adminmaster', 'middleware' => ['auth', 'adminmasteraccess'], 'as' => 'adminmaster.'], function () {

    Route::get('/', 'AdminMasterController@index');
    Route::get('/empresas', 'AdminMaster\EmpresasController@index');

    Route::post('/addMatriz', 'AdminMasterController@addMatriz');

    Route::post('/addAdminMatriz', 'EmpresasController@addAdminMatriz');

    Route::post('/upAdminMatriz', 'EmpresasController@upAdminMatriz');
    Route::post('/getEmpresas', 'EmpresasController@getEmpresas');
    Route::post('/mudaStatus', 'EmpresasController@mudaStatus');
    Route::post('/mudaLicenca', 'EmpresasController@mudaLicenca');

    Route::post('/upMatriz', 'AdminMasterController@upMatriz');
    Route::get('/getMatriz/{id}', 'AdminMasterController@getMatriz');
});



################ FIM DA ADMINMASTER ################
#
#
#
#
#
#
#
#









#
#
############################################
################ INICIO DA ADMIN NORMAL ################
// depois te q fazer um midware para validar se está logado
Route::group(['prefix' => 'admin', 'middleware' => ['adminaccess'], 'as' => 'admin.'], function () {

    Route::get('/', 'HomeController@homeAdmin');
    Route::get('/passos', 'HomeController@passos');
    Route::get('/passos/parabens', 'HomeController@passosparabens');

// trial
    Route::get('/trial', 'HomeController@trial');


// changeEmp
    Route::get('/changeEmp/{id_unidade}', 'EmpresaController@changeEmp');

    Route::group(['prefix' => 'clube', 'as' => 'clube.'], function () {
        Route::get('/', ['as' => 'index', 'uses' => 'ClubeVantagensController@index']);
        Route::get('/create', ['as' => 'create', 'uses' => 'ClubeVantagensController@create']);
        Route::get('/edit', ['as' => 'edit', 'uses' => 'ClubeVantagensController@create']);
    });

    // cores app
    Route::group(['prefix' => 'coresApp'], function () {
        Route::get('/{tela?}', 'coresAppController@index');
    });

    // PUSH
    Route::group(['prefix' => 'push'], function () {

        Route::get('/', 'PushController@index');
    });

    // AGENDAMENTO
    Route::group(['prefix' => 'agendamento'], function () {

        Route::get('/', 'AgendamentoController@index');
        // CALENDARIO
        Route::post('/getCaledario', 'AgendamentoController@getCaledario');
        Route::post('/gravaCalendario', 'AgendamentoController@gravaCalendario');
    });

    // CRM
    Route::group(['prefix' => 'crm'], function () {

        Route::get('/', 'CrmController@index');
        Route::get('/operacional', 'CrmController@operacional');
    });

    Route::post('/insere', 'ExercicioController@insere');
    Route::post('/atualiza', 'ExercicioController@atualiza');

    // GENESSON
    // PRESCRIÇÃO
    Route::group(['prefix' => 'prescricao', 'as' => 'prescricao.'], function () {
        Route::get('/nova', ['as' => 'prescricao.index', 'uses' => 'PrescricaoController@index']);

        Route::get('/nova/{id}/{tipo?}', ['as' => 'prescricao.index', 'uses' => 'PrescricaoController@indexId']);
        Route::post('/nova/{id}', ['as' => 'prescricao.index', 'uses' => 'PrescricaoController@indexIdPost']);
        Route::get('/configuracoes', ['as' => 'prescricao.index', 'uses' => 'PrescricaoController@configuracoes']);
        Route::get('/treinopadrao', ['as' => 'prescricao.index', 'uses' => 'PrescricaoController@treinopadrao']);

        // CRUD EXERCICIOS
        Route::post('/insere', 'ExercicioController@insere');
        Route::post('/atualiza', 'ExercicioController@atualiza');
        Route::get('/getExercicios', 'ExercicioController@getExercicios');
        Route::get('/getExerciciosBusca/{busca}', 'ExercicioController@getExerciciosBusca');

        Route::post('/deleteExercicio/{id}', 'ExercicioController@deleteExercicio');
        Route::post('/gerarMiniatura', 'ExercicioController@gerarMiniatura');

        ///insere nivel habilidade
        Route::post('/addNivel', 'NivelhabilidadeController@addNivel');
        Route::post('/upNivel', 'NivelhabilidadeController@upNivel');
        Route::post('/deleteNivel/{id}', 'NivelhabilidadeController@deleteNivel');
        Route::get('/getNiveis/{id}', 'NivelhabilidadeController@getNiveis');
        Route::get('/getModalidades', 'EmpresaController@getModalidades'); // modalidades para incluir niveis
        //insere objetivo de treino
        Route::post('/addObjetivo', 'ObjetivotreinoController@addObjetivo');
        Route::post('/upObjetivo', 'ObjetivotreinoController@upObjetivo');
        Route::get('/getObjetivos/{id}', 'ObjetivotreinoController@getObjetivos'); // modalidades para incluir niveis
        Route::get('/getObjetivo/{id}', 'ObjetivotreinoController@getObjetivo'); // modalidades para incluir niveis
        Route::post('/deleteObjetivo/{id}', 'ObjetivotreinoController@deleteObjetivo');

        //insere objetivo de treino
        Route::post('/addHabilidade', 'NivelhabilidadeController@addHabilidade');
        Route::get('/getHabilidadeAluno/{id}/{nivel}', 'NivelhabilidadeController@getHabilidadeAluno'); // modalidades para incluir niveis
        Route::get('/getHabilidade/{id}', 'NivelhabilidadeController@getHabilidade'); // modalidades para incluir niveis
        Route::get('/delHabilidade/{id}', 'NivelhabilidadeController@delHabilidade'); // modalidades para incluir niveis

        Route::get('/getProcessos', 'ProcessoController@getProcessos'); // processos
        Route::post('/upProcesso', 'ProcessoController@upProcesso'); // processos
        
        
        Route::get('/addHabilidade/{id}', 'NivelhabilidadeController@delHabilidade'); // modalidades para incluir niveis

        Route::post('/addHabilidadeAluno', 'NivelhabilidadeController@addHabilidadeAluno');

//programa treinamento
        Route::post('/addProgramatreinamento', 'ProgramatreinamentoController@addProgramatreinamento');
        Route::post('/upProgramatreinamento', 'ProgramatreinamentoController@upProgramatreinamento');
        Route::post('/deleteProgramatreinamento/{id}', 'ProgramatreinamentoController@deleteProgramatreinamento');
        Route::post('/addAgenda', 'ProgramatreinamentoController@addAgenda');

        Route::post('/upAgenda', 'ProgramatreinamentoController@upAgenda');
        Route::post('/deleteAgenda/{id}', 'ProgramatreinamentoController@deleteAgenda');

        Route::get('/getProgramasAluno/{idaluno}', 'ProgramatreinamentoController@getProgramasAluno');
        Route::get('/getProgramastreinamento', 'ProgramatreinamentoController@getProgramastreinamento');
        Route::get('/getProgramatreinamento/{id}', 'ProgramatreinamentoController@getProgramatreinamento');
        Route::get('/getAgendasProgramatreinamento/{id}', 'ProgramatreinamentoController@getAgendasProgramatreinamento');
        Route::get('/validaProgramaAberto/{id}', 'ProgramatreinamentoController@validaProgramaAberto');
    });

    // EMPRESAS
    Route::group(['prefix' => 'empresas', 'as' => 'empresas.'], function () {
        Route::get('/', 'EmpresaController@index');
        Route::get('/unidades', 'EmpresaController@index');
    });

    // GFMS
    Route::group(['prefix' => 'gfms', 'as' => 'gfms.'], function () {
        Route::get('/', ['as' => 'gfms.index', 'uses' => 'GfmController@index']);
    });

    // SPMS
    Route::group(['prefix' => 'spms', 'as' => 'spms.'], function () {
        Route::get('/', ['as' => 'spms.index', 'uses' => 'SpmController@index']);
    });

// API
    //
    Route::group(['prefix' => 'api'], function () {

        // API DE UNIDADE
        Route::group(['prefix' => 'unidade'], function () {
            // EMPRESA/UNIDADES
            Route::post('/getEstados', 'EmpresaController@getEstados');
            Route::post('/getCidades', 'EmpresaController@getCidades');

            Route::get('/getEstados', 'EmpresaController@getEstados');
            Route::get('/getCidadesGet/{uf}', 'EmpresaController@getCidadesGet');

            Route::post('/getEmpresa', 'EmpresaController@getEmpresa');
            Route::post('/upEmpresa', 'EmpresaController@upEmpresa');
            Route::post('/upPasso', 'EmpresaController@upPasso');


            // FILIAIS
            Route::post('/getFiliais', 'EmpresaController@getFiliais');
            Route::post('/getFilial', 'EmpresaController@getFilial');
            Route::post('/addFilial', 'EmpresaController@addFilial');
            Route::post('/upFilial', 'EmpresaController@upFilial');
            Route::post('/delFilial/{id}', 'EmpresaController@delFilial');

            // LOCAIS
            Route::post('/getLocais', 'EmpresaController@getLocais');
            Route::post('/getLocal', 'EmpresaController@getLocal');
            Route::post('/addLocal', 'EmpresaController@addLocal');
            Route::post('/upLocal', 'EmpresaController@upLocal');
            Route::post('/delLocal/{id}', 'EmpresaController@delLocal');

            // FUNCIONARIOS
            Route::post('/getFuncionarios', 'EmpresaController@getFuncionarios');
            Route::post('/getFuncionario', 'EmpresaController@getFuncionario');
            Route::post('/addFuncionario', 'EmpresaController@addFuncionario');
            Route::post('/upFuncionario', 'EmpresaController@upFuncionario');
            Route::post('/delFuncionario/{id}/{status}', 'EmpresaController@delFuncionario');

            // NUTRICIONISTAS
            Route::post('/getNutricionistas', 'EmpresaController@getNutricionistas');
            Route::post('/getNutricionista', 'EmpresaController@getNutricionista');
            Route::post('/addNutricionista', 'EmpresaController@addNutricionista');
            Route::post('/upNutricionista', 'EmpresaController@upNutricionista');
            Route::post('/delNutricionista/{id}', 'EmpresaController@delNutricionista');

            // MODALIDADES
            Route::post('/getModalidades', 'EmpresaController@getModalidades');
            Route::post('/getModalidadesBlackList', 'EmpresaController@getModalidadesBlackList');
            Route::post('/upModalidadesBlackList', 'EmpresaController@upModalidadesBlackList');

            /////////////configura gfm
            Route::post('/upConfiguraGfm', 'EmpresaController@upConfiguraGfm');
            Route::post('/getConfiguraGfm', 'EmpresaController@getConfiguraGfm');

        });

        // API DE PRESCRICAO
        Route::group(['prefix' => 'prescricao'], function () {
            // EXERCICIOS
            Route::post('/getExercicio', 'PrescricaoController@getExercicio');
            Route::post('/getExercicios', 'PrescricaoController@getExercicios');

            // CLIENTE
            Route::post('/getClients', 'PrescricaoController@getClients');
            Route::post('/getFuncionarios', 'PrescricaoController@getFuncionarios');

            Route::post('/getClient', 'PrescricaoController@getClient');
            Route::post('/getClientUnidade', 'PrescricaoController@getClientUnidade');
            Route::get('/getAgendaPrograma/{idprograma}/{idaluno}', 'ProgramatreinamentoController@getAgendas');
            Route::post('/getAgendaData/{indice}', 'ProgramatreinamentoController@getAgendaData');
            Route::post('/delProgramaaluno/{programa}/{aluno}', 'ProgramatreinamentoController@delProgramaaluno');

            Route::post('/addAgendaAluno', 'ProgramatreinamentoController@addAgendaAluno');
            Route::get('/atualizaAgenda/{id}/{st}', 'ProgramatreinamentoController@atualizaAgenda');
            Route::post('/validaAgenda', 'ProgramatreinamentoController@validaAgenda');

            // TREINOS

            Route::post('/getTreinosCross', 'PrescricaoController@getTreinosCross');
            Route::post('/getTreinosCorrida', 'PrescricaoController@getTreinosCorrida');

            Route::post('/getTreinos', 'PrescricaoController@getTreinos');
            Route::post('/getTreino', 'PrescricaoController@getTreino');
            Route::post('/addTreino', 'PrescricaoController@addTreino');
            Route::post('/addObservacao', 'PrescricaoController@addObservacao');
            Route::post('/getObservacao', 'PrescricaoController@getObservacao');
            Route::post('/upTreino', 'PrescricaoController@upTreino');
            Route::post('/delTreino/{id}', 'PrescricaoController@delTreino');
            Route::post('/saveTempoEstimado', 'PrescricaoController@saveTempoEstimado');
            Route::post('/getTreinoIniciado', 'PrescricaoController@getTreinoIniciado');
            Route::post('/atualizaOrdemFicha/{id}/{aba}', 'PrescricaoController@atualizaOrdemFicha');
            Route::post('/addExercicioPadrao/{id}', 'PrescricaoController@addExercicioPadrao');
            Route::post('/atualizaOrdemInsertFicha/{id}/{aba}', 'PrescricaoController@atualizaOrdemInsertFicha');

            Route::post('/getTreinoCrossPadrao', 'PrescricaoController@getTreinoCrossPadrao');
            Route::post('/getTreinoCorridaPadrao', 'PrescricaoController@getTreinoCorridaPadrao');
            Route::post('/salvarTreinoCross', 'PrescricaoController@salvarTreinoCross');

            //TREINO PADRAO
            Route::post('/addTreinoModalidadePadrao', 'TreinopadraoController@addTreinoModalidadePadrao');
            Route::post('/upTreinoModalidadePadrao', 'TreinopadraoController@upTreinoModalidadePadrao');

            Route::post('/addTreinoPadrao', 'TreinopadraoController@addTreinoPadrao');
            Route::post('/upTreinoPadrao', 'TreinopadraoController@upTreinoPadrao');
            Route::post('/saveTempoEstimadoPadrao', 'TreinopadraoController@saveTempoEstimadoPadrao');
            Route::post('/getPrescricaoPadrao', 'PrescricaoController@getPrescricaoPadrao');
            Route::post('/getPrescricaoFichaPadrao', 'PrescricaoController@getPrescricaoFichaPadrao');
            Route::post('/atualizaOrdem/{id}/{aba}', 'TreinopadraoController@atualizaOrdem');
            Route::post('/atualizaOrdemInsert/{id}/{aba}', 'TreinopadraoController@atualizaOrdemInsert');
            Route::get('/delExercicioSuperSeriePadrao/{ficha}/{treino}/{ficha_super}/{super_serie}', 'TreinopadraoController@delExercicioSuperSeriePadrao');

            // FICHA DE TREINO
            Route::post('/addExercicioTreino/{id}', 'PrescricaoController@addExercicioTreino');
            Route::post('/addLinhaTreino/{id}', 'PrescricaoController@addLinhaTreino');
            Route::post('/delExercicioTreino/{ficha}/{treino}', 'PrescricaoController@delExercicioTreino');
            Route::post('/delExercicioTreinoSuperSerie/{ficha}/{treino}/{ficha_super}/{super_serie}', 'PrescricaoController@delExercicioTreinoSuperSerie');
            Route::post('/getFicha', 'PrescricaoController@getFicha');
            Route::get('/getFichaAba/{treino}/{aba}', 'PrescricaoController@getFichaAba');

            //FICHA DE TREINO PADRAO
            Route::post('/addExercicioTreinoPadrao/{id}', 'TreinopadraoController@addExercicioTreinoPadrao');
            Route::post('/delExercicioTreinoPadrao/{id}', 'TreinopadraoController@delExercicioTreinoPadrao');
            Route::post('/addNutricaoPadrao', 'TreinopadraoController@addNutricaoPadrao');

            // CALENDARIO
            Route::post('/getCaledarioPadrao', 'TreinopadraoController@getCaledarioPadrao');
            Route::post('/gravaCalendarioPadrao', 'TreinopadraoController@gravaCalendarioPadrao');

            // CALENDARIO
            Route::post('/getCaledario', 'PrescricaoController@getCaledario');
            Route::post('/gravaCalendario', 'PrescricaoController@gravaCalendario');

            // NUTRICAO
            Route::post('/getNutricao', 'PrescricaoController@getNutricao');
            Route::post('/getNutricoes', 'PrescricaoController@getNutricoes');
            Route::post('/addNutricao', 'PrescricaoController@addNutricao');
            Route::post('/upNutricao', 'PrescricaoController@upNutricao');
            Route::post('/delNutricao/{id}', 'PrescricaoController@delNutricao');

            // NUTRICAO
            Route::post('/getNutricao', 'TreinopadraoController@getNutricaoPadrao');
            Route::post('/getNutricoes', 'TreinopadraoController@getNutricoesPadrao');
            Route::post('/addNutricao', 'TreinopadraoController@addNutricaoPadrao');
            Route::post('/upNutricao', 'TreinopadraoController@upNutricaoPadrao');
            Route::post('/delNutricao/{id}', 'TreinopadraoController@delNutricaoPadrao');
            // PROJETO
            Route::post('/getProjeto', 'PrescricaoController@getProjeto');
            Route::post('/getProjetos', 'PrescricaoController@getProjetos');
            Route::post('/addProjeto', 'PrescricaoController@addProjeto');
            Route::post('/upProjeto', 'PrescricaoController@upProjeto');
            Route::post('/delProjeto/{id}', 'PrescricaoController@delProjeto');
            Route::post('/getEtapas', 'PrescricaoController@getEtapas');
            Route::post('/addEtapa', 'PrescricaoController@addEtapa');
            Route::post('/upEtapa', 'PrescricaoController@upEtapa');
            Route::post('/delEtapa/{id}', 'PrescricaoController@delEtapa');

            // GSA
            Route::post('/getProgramas', 'PrescricaoController@getProgramas');
            Route::post('/getPsas', 'PrescricaoController@getPsas');
            Route::post('/addPsa/{id}', 'PrescricaoController@addPsa');
            Route::post('/salvaPsa', 'PrescricaoController@salvaPsa');
            Route::post('/delPsa/{id}', 'PrescricaoController@delPsa');

            // TREINO PADRAO
            Route::post('/getObjetivos/{id}', 'PrescricaoController@getObjetivos');
            Route::post('/getNiveis/{idmodalidade}/{idobjetivo}', 'PrescricaoController@getNiveis');
            Route::post('/getFullTreinosM', 'PrescricaoController@getFullTreinosM');
            Route::post('/getFullTreinosF', 'PrescricaoController@getFullTreinosF');
            Route::post('/getTreinoPadrao', 'PrescricaoController@getTreinoPadrao');
            Route::post('/getFichaPadrao', 'PrescricaoController@getFichaPadrao');
            Route::post('/getFichaPadraoCross/{idtreino}', 'TreinopadraoController@getFichaPadraoCross');
            Route::get('/listarGrupoTreino/{id}/{letra}', 'PrescricaoController@listarGrupoTreino');

            Route::post('/changeNomeTreinoPadrao', 'PrescricaoController@etchangeNomeTreinoPadrao');
            Route::post('/delTreinoPadrao/{id}', 'TreinopadraoController@delTreinoPadrao');
            Route::get('/getPadrao/{sexo}', 'TreinopadraoController@getPadrao');
            Route::get('/getPadraoModalidade/{sexo}/{modalidade}', 'TreinopadraoController@getPadraoModalidade');

            //push teste
            Route::post('/enviaPush', 'enviapushController@enviaPush');

            Route::post('/enviaPushTreinoAtualizado', 'PrescricaoController@enviaPushTreinoAtualizado');
        });

        // API DE GFM
        Route::group(['prefix' => 'gfm'], function () {
            // GFMS

            Route::post('/getGfm', 'GfmController@getGfm');
            Route::post('/getGfms/{tipo}', 'GfmController@getGfms');

            //filtro dia da semana
            Route::post('/getGfmsDiasemana/{tipo}', 'GfmController@getGfmsDiasemana');            
            
            Route::get('/getGfmsGrafico1', 'GfmController@getGfmsGrafico1');
            Route::get('/getGfmsGrafico2', 'GfmController@getGfmsGrafico2');
            Route::post('/addGfm', 'GfmController@addGfm');
            Route::post('/upGfm', 'GfmController@upGfm');
            Route::post('/delGfm/{id}', 'GfmController@delGfm');
            Route::get('/getListareserva/{id}', 'GfmController@getListareserva');
            Route::get('/getAulasPassadas/{id}', 'GfmController@getAulasPassadas');
            Route::post('/addAnonimos/{id}/{numero}', 'GfmController@addAnonimos');

            Route::post('/reservaAddPainel/{idaula}', 'GfmController@reservaAddPainel');

            Route::post('/delAula/{id}', 'GfmController@delAula');
            Route::get('/getAula/{id}', 'GfmController@getAula');

            // PROGRAMAS
            Route::post('/getTaxaOcupacao', 'GfmController@getTaxaOcupacao');
            Route::post('/getTaxaOcupacaoProfs', 'GfmController@getTaxaOcupacaoProfs');
            Route::post('/getProgramas', 'GfmController@getProgramas');

            // LOCAIS
            Route::post('/getLocais', 'GfmController@getLocais');
            Route::post('/addLocal', 'GfmController@addLocal');
            Route::post('/getProfessores', 'GfmController@getProfessores');
            Route::get('/getGfmsGraficoProfs', 'GfmController@getGfmsGraficoProfs');
            Route::post('/getProfessor', 'GfmController@getProfessor');
        });

        //temas do app

        Route::group(['prefix' => 'temas'], function () {
            // GFMS
            Route::get('/getTemas', 'coresAppController@getTemas');
            Route::post('/getTelasTema', 'coresAppController@getTelasTema');
            Route::post('/getTelaTema', 'coresAppController@getTelaTema');
            Route::post('/addTema', 'coresAppController@addTema');

            Route::post('/addLogomarca', 'coresAppController@uploadFile');

            Route::get('/gerarTema/{unidade}', 'coresAppController@gerarTema');

        });

        // API DE SPM
        Route::group(['prefix' => 'spm'], function () {
            // SPM
            Route::post('/getSpm', 'SpmController@getSpm');
            Route::post('/getSpms', 'SpmController@getSpms');
            Route::post('/addSpm', 'SpmController@addSpm');
            Route::post('/upSpm', 'SpmController@upSpm');
            Route::post('/getClients', 'SpmController@getClients');
            Route::post('/getInscricoes', 'SpmController@getInscricoes');
            Route::post('/adicionaCliente', 'SpmController@adicionaCliente');
            Route::post('/reservaDelSpm', 'SpmController@reservaDelSpm');
            Route::post('/delSpm/{id}', 'SpmController@delSpm');

            // PROGRAMAS
            Route::post('/getTaxaOcupacao', 'SpmController@getTaxaOcupacao');
            Route::post('/getProgramas', 'SpmController@getProgramas');
            Route::post('/getNiveis', 'SpmController@getNiveis');
            Route::post('/addAula', 'SpmController@addAula');
            Route::post('/upAula', 'SpmController@upAula');

            // LOCAIS
            Route::post('/getLocais', 'SpmController@getLocais');
            Route::post('/addLocal', 'SpmController@addLocal');
            Route::post('/getProfessores', 'SpmController@getProfessores');
            Route::post('/getProfessor', 'SpmController@getProfessor');
        });

        Route::group(['prefix' => 'cvempresa'], function () {
            Route::post('/addEmpresa', 'CvempresaController@addEmpresa');
            Route::get('/getEmpresa/{id}', 'CvempresaController@getEmpresa');
            Route::get('/getEmpresas', 'CvempresaController@getEmpresas');
            Route::get('/getSegmentos', 'CvempresaController@getSegmentos');
            Route::post('/deleteCvempresa/{id}', 'CvempresaController@deleteCvempresa');
            Route::post('/upEmpresa', 'CvempresaController@upEmpresa');
        });
        //api ranking conquistas
        Route::group(['prefix' => 'conquistas'], function () {
            Route::get('/getRanking/{id}', 'RankingController@getRanking');
            Route::get('/getDenuncias', 'RankingController@getDenuncias');
            Route::post('/addNotificacao', 'RankingController@addNotificacao');
        });

        //api painel
        Route::group(['prefix' => 'painel'], function () {
            Route::get('/getListas', 'PainelController@getListas');
            Route::get('/getTotalProspects', 'PainelController@getTotalProspects');
            Route::get('/getTotalLogins', 'PainelController@getTotalLogins');
            Route::get('/getAulasDiaSemana', 'PainelController@getAulasDiaSemana');
            Route::get('/getRevisaoTreino', 'PainelController@getRevisaoTreino');
            Route::get('/getAgendaPrograma', 'PainelController@getAgendaPrograma');
            Route::get('/getTurmasProf/{professor}', 'PainelController@getTurmasProf');
            Route::get('/getCarteiraProfessor/{professor}', 'PainelController@getCarteiraProfessor');
            Route::get('/upTurma/{aluno}/{professor}', 'PainelController@upTurma');

            Route::get('/projetosAtrasados', 'PainelController@projetosAtrasados');
            Route::get('/getIndicadorProjeto', 'PainelController@getIndicadorProjeto');
            Route::get('/getRotatividade', 'PainelController@getRotatividade');



            Route::post('/getProfessores', 'PainelController@getProfessores');
            Route::get('/getGraficosC3', 'PainelController@getGraficosC3');
            Route::get('/getGraficosC32', 'PainelController@getGraficosC32');

            Route::get('/getDesativados', 'PainelController@getDesativados');

            Route::get('/ajustaResultado/{unidade}', 'PainelController@ajustaResultado');

        });
        Route::group(['prefix' => 'loja'], function () {
            Route::post('/addModulo', 'LojaAppController@addModulo');
            Route::post('/getModulos', 'LojaAppController@getModulos');
            
        });

        //api painel
        Route::group(['prefix' => 'agendamento'], function () {
            Route::get('/getAgendamentos/{dia}', 'AgendamentoController@getAgendamentos');
            Route::post('/addAgendamento', 'AgendamentoController@addAgendamento');
            Route::post('/addHorario', 'AgendamentoController@addHorario');
            Route::get('/getHorarios', 'AgendamentoController@getHorarios');
            Route::post('/cancelaAgendamento', 'AgendamentoController@cancelaAgendamento');
            Route::get('/delHorario/{id}', 'AgendamentoController@delHorario');
        });

        //api egoi
        Route::group(['prefix' => 'egoi'], function () {
            Route::get('/getListas', 'ApiEgoiController@getListas');
            Route::post('/addSubscriber', 'ApiEgoiController@addSubscriber');
            Route::get('/createCampaignFax', 'ApiEgoiController@createCampaignFax');
        });
        //api egoi
    });

    // INICIO PRESCRICAO
    Route::group(['prefix' => 'desafios', 'middleware' => 'auth', 'as' => 'desafios.'], function () {
        Route::get('/', ['as' => 'desafios.index_novo', 'uses' => 'TipoconquistaController@index_novo']);
    });

    // INICIO conqusitas
    Route::group(['prefix' => 'conquistas', 'middleware' => 'auth', 'as' => 'conquistas.'], function () {
        Route::get('/', ['as' => 'conquistas.index', 'uses' => 'TipoconquistaController@index']);
    });

    Route::group(['prefix' => 'funcionario'], function () {
        Route::get('', 'FuncionarioController@index');
        Route::get('funcionario/{id}', 'FuncionarioController@show');
        Route::post('', 'FuncionarioController@register');
        Route::put('{id}', 'FuncionarioController@update');
        Route::delete('{id}', 'FuncionarioController@destroy');
    });
    Route::group(['prefix' => 'local'], function () {
        Route::get('', 'LocalController@index');
        Route::get('local/{id}', 'LocalController@show');
        Route::post('', 'LocalController@register');
        Route::put('{id}', 'LocalController@update');
        Route::delete('{id}', 'LocalController@destroy');
    });
    Route::group(['prefix' => 'modalidade'], function () {
        Route::get('', 'ModalidadeController@index');
        Route::get('modalidade/{id}', 'ModalidadeController@show');
        Route::post('', 'ModalidadeController@register');
        Route::put('{id}', 'ModalidadeController@update');
        Route::delete('{id}', 'ModalidadeController@destroy');
    });

// INICIO GRUPO CLIENTES
    Route::group(['prefix' => 'clientes', 'middleware' => 'auth', 'as' => 'clientes.'], function () {

        //tipoconquista
        Route::get('/tipoconquistas', ['as' => 'tipoconquistas.index', 'uses' => 'TipoconquistaController@index']);
        Route::get('/tipoconquistas/edit/{id}', ['as' => 'tipoconquistas.edit', 'uses' => 'TipoconquistaController@edit']);
        Route::get('tipoconquistas/create', ['as' => 'tipoconquistas.create', 'uses' => 'TipoconquistaController@create']);
        Route::post('tipoconquistas/update/{id}', ['as' => 'tipoconquistas.update', 'uses' => 'TipoconquistaController@update']);
        Route::post('tipoconquistas/store', ['as' => 'tipoconquistas.store', 'uses' => 'TipoconquistaController@store']);

        //grupomuscular
        Route::get('/gruposmusculares', ['as' => 'gruposmusculares.index', 'uses' => 'GrupomuscularController@index']);
        Route::get('/gruposmusculares/edit/{id}', ['as' => 'gruposmusculares.edit', 'uses' => 'GrupomuscularController@edit']);
        Route::get('gruposmusculares/create', ['as' => 'gruposmusculares.create', 'uses' => 'GrupomuscularController@create']);
        Route::post('gruposmusculares/update/{id}', ['as' => 'gruposmusculares.update', 'uses' => 'GrupomuscularController@update']);
        Route::post('gruposmusculares/store', ['as' => 'gruposmusculares.store', 'uses' => 'GrupomuscularController@store']);

        //tipomensagem
        Route::get('/tipomensagens', ['as' => 'tipomensagens.index', 'uses' => 'TipomensagemController@index']);
        Route::get('/tipomensagens/edit/{id}', ['as' => 'tipomensagens.edit', 'uses' => 'TipomensagemController@edit']);
        Route::get('tipomensagens/create', ['as' => 'tipomensagens.create', 'uses' => 'TipomensagemController@create']);
        Route::post('tipomensagens/update/{id}', ['as' => 'tipomensagens.update', 'uses' => 'TipomensagemController@update']);
        Route::post('tipomensagens/store', ['as' => 'tipomensagens.store', 'uses' => 'TipomensagemController@store']);

        //conquista
        Route::get('/conquistas', ['as' => 'conquistas.index', 'uses' => 'ConquistaController@index']);
        Route::get('/conquistas/edit/{id}', ['as' => 'conquistas.edit', 'uses' => 'ConquistaController@edit']);
        Route::get('conquistas/create', ['as' => 'conquistas.create', 'uses' => 'ConquistaController@create']);
        Route::post('conquistas/update/{id}', ['as' => 'conquistas.update', 'uses' => 'ConquistaController@update']);
        Route::post('conquistas/store', ['as' => 'conquistas.store', 'uses' => 'ConquistaController@store']);

        //alunos
        Route::get('/clientes', ['as' => 'clientes.index', 'uses' => 'AlunoController@alunobunny']);
        Route::get('/clientes/buscar', ['as' => 'clientes.search', 'uses' => 'AlunoController@search']);

        Route::get('/crm', ['as' => 'crm.index', 'uses' => 'AlunoController@crm']);
        Route::get('/crmsegmenta', ['as' => 'crm.index', 'uses' => 'AlunoController@crmsegmenta']);

        Route::get('/alunos', ['as' => 'alunos.index', 'uses' => 'AlunoController@index']);
        Route::get('/alunos/edit/{id}', ['as' => 'alunos.edit', 'uses' => 'AlunoController@edit']);
        Route::get('alunos/create', ['as' => 'alunos.create', 'uses' => 'AlunoController@create']);
        Route::post('alunos/update/{id}', ['as' => 'alunos.update', 'uses' => 'AlunoController@update']);
        Route::post('alunos/store', ['as' => 'alunos.store', 'uses' => 'AlunoController@store']);

        //exercicios
        Route::get('/exercicios', ['as' => 'exercicios.index', 'uses' => 'ExercicioController@index']);
        Route::get('/exercicios/edit/{id}', ['as' => 'exercicios.edit', 'uses' => 'ExercicioController@edit']);
        Route::get('exercicios/create', ['as' => 'exercicios.create', 'uses' => 'ExercicioController@create']);
        Route::post('exercicios/update/{id}', ['as' => 'exercicios.update', 'uses' => 'ExercicioController@update']);
        Route::post('exercicios/store', ['as' => 'exercicios.store', 'uses' => 'ExercicioController@store']);

        //tiposobjetivohope
        Route::get('/tiposobjetivohope', ['as' => 'tiposobjetivohope.index', 'uses' => 'TipoobjetivohopeController@index']);
        Route::get('/tiposobjetivohope/edit/{id}', ['as' => 'tiposobjetivohope.edit', 'uses' => 'TipoobjetivohopeController@edit']);
        Route::get('tiposobjetivohope/create', ['as' => 'tiposobjetivohope.create', 'uses' => 'TipoobjetivohopeController@create']);
        Route::post('tiposobjetivohope/update/{id}', ['as' => 'tiposobjetivohope.update', 'uses' => 'TipoobjetivohopeController@update']);
        Route::post('tiposobjetivohope/store', ['as' => 'tiposobjetivohope.store', 'uses' => 'TipoobjetivohopeController@store']);

        //tiposmensagem
        Route::get('/tiposmensagem', ['as' => 'tiposmensagem.index', 'uses' => 'TipomensagemController@index']);

        Route::get('/chat', ['as' => 'chat.index', 'uses' => 'ChatController@index']);

        Route::get('/tiposmensagem/edit/{id}', ['as' => 'tiposmensagem.edit', 'uses' => 'TipomensagemController@edit']);
        Route::get('tiposmensagem/create', ['as' => 'tiposmensagem.create', 'uses' => 'TipomensagemController@create']);
        Route::post('tiposmensagem/update/{id}', ['as' => 'tiposmensagem.update', 'uses' => 'TipomensagemController@update']);
        Route::post('tiposmensagem/store', ['as' => 'tiposmensagem.store', 'uses' => 'TipomensagemController@store']);
        //mensagens
        Route::get('/mensagens', ['as' => 'mensagens.index', 'uses' => 'MensagemController@index']);
        Route::get('/mensagens/edit/{id}', ['as' => 'mensagens.edit', 'uses' => 'MensagemController@edit']);
        Route::get('mensagens/create', ['as' => 'mensagens.create', 'uses' => 'MensagemController@create']);
        Route::post('mensagens/update/{id}', ['as' => 'mensagens.update', 'uses' => 'MensagemController@update']);
        Route::post('mensagens/store', ['as' => 'mensagens.store', 'uses' => 'MensagemController@store']);

        //PSA
        Route::get('/psas', ['as' => 'psas.index', 'uses' => 'PsaController@index']);
        Route::get('psas/create', ['as' => 'psas.create', 'uses' => 'PsaController@create']);
    });
}); // fim do admin




################ FIM DA admin ################
#
#
#
#
#
#
#
#
#
#

################ INCIO DA API ################
Route::group(['prefix' => 'api', 'as' => 'api.'], function () {

// cores
    Route::group(['prefix' => 'temas'], function () {

        Route::post('/getTelaTema', 'coresAppController@getTelaTemaApp');
    });

    // API DE UNIDADE
    Route::group(['prefix' => 'unidade'], function () {
        // EMPRESA/UNIDADES
        Route::post('/getEstados', 'EmpresaController@getEstados');
        Route::post('/getCidades', 'EmpresaController@getCidades');

        Route::get('/getEstados', 'EmpresaController@getEstados');
        Route::get('/getCidadesGet/{uf}', 'EmpresaController@getCidadesGet');

    });

    //push teste
    Route::post('/enviaPushNew/send', 'enviapushController@send');
    Route::post('/enviaPushNew/register', 'enviapushController@registration');
    Route::get('/haveMessagemToMe/{id}', 'ApiController@haveMessagemToMe');
    Route::get('/getUsers', 'ApiController@getUsers');
    Route::get('/qtdUserInAcademia/{email}', 'ApiController@qtdUserInAcademia');
    Route::post('/login', 'ApiController@authenticate');
    Route::post('/loginAppNovo', 'ApiController@authenticateAppNovo');

    Route::get('/meusdados/{user_id}', 'ApiController@meusdados');

    Route::get('/pushBoasVindas/{user_id}/{idunidade}', 'ApiController@pushBoasVindas');

    Route::post('/authenticateToten', 'ApiController@authenticateToten');

    // FREQUENCIA
    Route::group(['prefix' => 'frequencia'], function () {

        Route::get('/{id_user}/{id_unidade}/{mes}/{ano}', 'FrequenciaController@getCaledario');
        // CALENDARIO
        Route::post('/getCaledario', 'FrequenciaController@getCaledario');
        // Route::post('/gravaCalendario', 'FrequenciaController@gravaCalendario');
    });
    
    
    


    Route::post('/prescricaoSalvar', 'ApiController@prescricaoSalvar');
    Route::get('/prescricaoCalendario/{atividade}/{idaluno}', 'ApiTabletController@prescricaoCalendario');
    Route::get('/tempoEstimado/{treino_id}/{letra}', 'ApiTabletController@tempoEstimado');
    Route::post('/enviarcontato', 'ApiTabletController@enviarContato');
    Route::post('/enviarqueroentraremforma', 'ApiTabletController@enviarqueroentraremforma');
    //////////////////////
    Route::get('/getCoachs/{unidade}/{id_aluno?}', 'ApiTabletController@getCoachs');
    Route::get('/getCoachPrescricao/{id}', 'ApiTabletController@getCoachPrescricao');

    
    
    //atualiza so e versao app cliente
    Route::get('/atualizaVersaoApp/{id}/{versaoapp}/{versaoso}', 'ApiTabletController@atualizaVersaoApp');
    Route::get('/verificarVersaoApp/{id}', 'ApiTabletController@verificarVersaoApp');
    Route::get('/gravarVersaoApp/{versao}/{tipo}', 'ApiTabletController@gravarVersaoApp');

    Route::get('/getVersaoApp/{aluno}/{tipo}', 'ApiTabletController@getVersaoApp');
    Route::get('/getTemaApp/{idunidade}', 'ApiTabletController@getTemaApp');

    Route::get('/psalist/{idaluno}', 'ApiTabletController@psaList');
    Route::get('/projetolist/{idaluno}', 'ApiTabletController@projetoList');
    Route::get('/projetolistetapa/{idprojeto}', 'ApiTabletController@projetoListEtapa');
    Route::get('/getModalidades/{idunidade}/{idaluno}', 'ApiTabletController@getModalidades');

    // JSON GFM
    Route::get('/getAulasDia/{idunidade}', 'ApiTabletController@getAulasDia');
    Route::get('/getAulasDiaSemana/{idunidade}/{dia}', 'ApiTabletController@getAulasDiaSemana');
    Route::get('/getAulasDiaSemana_New/{idunidade}/{dia}/{idaluno}/{tipo?}', 'ApiTabletController@getAulasDiaSemana_New');
    Route::get('/getAula/{idaula}/{idaluno}/{dia}', 'ApiTabletController@getAula');

    Route::get('/corrigeSaldo/{idaula}/{dia}', 'ApiTabletController@corrigeSaldo');

    Route::get('/reservaAdd/{idaula}/{idaluno}/{diasemana?}', 'ApiTabletController@reservaAdd');
    Route::get('/reservaAddPainel/{idaula}/{idaluno}/{diasemana?}', 'ApiTabletController@reservaAddPainel');
    Route::get('/reservaDel/{idaula}/{idaluno}/{dia?}', 'ApiTabletController@reservaDel');
    Route::get('/reservaDelPainel/{idaula}/{id}/{dia?}', 'ApiTabletController@reservaDelPainel');
    Route::get('/getListareserva/{idaula}/{dia?}', 'ApiTabletController@getListareserva');
    Route::get('/getListareservaPainel/{idaula}/{dia?}', 'ApiTabletController@getListareservaPainel');

    Route::get('/getListaespera/{idgfm}', 'ApiTabletController@getListaespera');
    Route::get('/esperaAdd/{idgfm}/{aluno}', 'ApiTabletController@esperaAdd');
    Route::get('/esperaDel/{idgfm}/{aluno}', 'ApiTabletController@esperaDel');

    Route::get('/cancelaReservas', 'ApiTabletController@cancelaReservas');
    Route::get('/ofereceVagas/{idgfm}/{numero}', 'ApiTabletController@ofereceVagas');
    Route::get('/testeReservas', 'ApiTabletController@testeReservas');

    Route::get('/getReservasAluno/{aluno}', 'ApiTabletController@getReservasAluno');

    Route::get('/geraRegistroAula', 'ApiTabletController@geraRegistroAula');

    Route::get('/matriculaAdd/{idaula}/{idaluno}/{idnivel}', 'ApiTabletController@matriculaAdd');
    Route::get('/getListamatricula/{idaula}/{dataaula?}', 'ApiTabletController@getListamatricula');
    Route::get('/matriculaDel/{idaula}/{idaluno}', 'ApiTabletController@matriculaDel');

    // JSON SPM
    Route::get('/getAulasDiaSpm/{idunidade}', 'ApiTabletController@getAulasDiaSpm');
    Route::get('/getAulasDiaSemanaSpm/{idunidade}/{dia}', 'ApiTabletController@getAulasDiaSemanaSpm');
    Route::get('/getAulaSpm/{idaula}/{idaluno}', 'ApiTabletController@getAulaSpm');
    Route::get('/reservaAddSpm/{idaula}/{idaluno}', 'ApiTabletController@reservaAddSpm');
    Route::get('/reservaDelSpm/{idaula}/{idaluno}', 'ApiTabletController@reservaDelSpm');
    
    //registra atividade cross
    Route::post('/registraAtividade', 'ApiTabletController@registraAtividade');
    Route::post('/listarHistoricoAtividade', 'ApiTabletController@listarHistoricoAtividade');
    

    Route::post('/psaSalvar', 'ApiController@psaSalvar');
    Route::post('/gfmSalvar', 'ApiController@gfmSalvar');
    Route::get('/getPsa', 'ApiController@getPsa');
    Route::get('/getMetahope', 'ApiController@getMetahope');

    // Route::post('/prescricaoSalvar/{id}', 'ApiController@prescricaoSalvar'); update
    Route::post('/projetoSalvar', 'ApiController@projetoSalvar');
    Route::post('/projetoFinalizar', 'ApiController@projetoFinalizar');
    Route::get('/getProjetos', 'ApiController@getProjetos');

    ///////////////api cpnquistas
    //Route::get('/listarHistoricoAtividade/{idunidade}/{idaluno}/{idatividade}', 'ApiController@listarHistoricoAtividade');

    /////////////api clube vantagens
    Route::get('/listarEmpresasClube/{idunidade}', 'ApiController@listarEmpresasClube');
    Route::get('/listarTotalDesconto/{idunidade}/{idaluno}', 'ApiController@listarTotalDesconto');
    Route::get('/gravarCompra/{idunidade}/{idaluno}/{idempresa}/{valorcompra}', 'ApiController@gravarCompra');

    /////////////api musculação
    Route::get('/listarFichaTreino/{idunidade}/{idaluno}', 'ApiController@listarFichaTreino');

    /////api foto pesagem
    Route::get('/gravarFotoPesagem/{idaluno}/{idunidade}/{foto}/{peso}/{tipo}', 'ApiController@gravarFotoPesagem');
    Route::get('/deletaFotoPesagem/{id}', 'ApiController@deletaFotoPesagem');
    Route::get('/listarFotoPesagem/{idaluno}/{idunidade}', 'ApiController@listarFotoPesagem');

//// NUTRICIONISTA
    Route::get('/nutricionista/{idunidade}', 'ApiTabletController@getNutricionista');
//    PEGAR DADOS DA ACADEMIA
    Route::get('/getAcademia/{idunidade}', 'ApiTabletController@getAcademia');

    //SALVAR FOTO
    Route::post('/salvarfoto', 'ApiTabletController@salvar_foto');
    Route::post('/salvarfoto_pesagem', 'ApiTabletController@salvarfoto_pesagem');

    Route::get('/listarPesagemGrafico/{aluno}/{unidade}', 'ApiTabletController@listarPesagemGrafico');
    Route::get('/listarTonelagemGrafico/{aluno}/{unidade}', 'ApiTabletController@listarTonelagemGrafico');

    // incio TABLET
    Route::get('/getClientes/{unidade}/{name}', 'ApiTabletController@getClientes');
    Route::get('/listarDiaTreino/{id}', 'ApiTabletController@listarDiaTreino');
    Route::get('/agruparSeries/{idficha}/{st}', 'ApiTabletController@agruparSeries');

    Route::get('/atualizaUpdateAt/{idaluno}/{idunidade}', 'ApiTabletController@atualizaUpdateAt');

    Route::get('/listarTreino/{id}/{letra}', 'ApiTabletController@listarTreino');
    Route::get('/listarSeries/{idaluno}/{idunidade}/{idprescricao}/{fichatreino}/{idexerc1icio}/{nrlancamento}', 'ApiTabletController@listarSeries');
    Route::get('/listarEvolucao/{ficha}', 'ApiTabletController@listarEvolucao');

    Route::get('/iniciarTreino/{aluno}/{unidade}/{treino}/{ficha}', 'ApiTabletController@iniciarTreino');
    Route::get('/verificarTreinoAberto/{aluno}/{unidade}', 'ApiTabletController@verificarTreinoAberto');
    Route::get('/verificaTreinoAbDiaAnterior', 'ApiTabletController@verificaTreinoAbDiaAnterior');

    Route::post('/cancelarTreino', 'ApiTabletController@cancelarTreino');

    Route::post('/verificarArquivo', 'ApiTabletController@verificarArquivo');
    Route::post('/pushAvisaAvaliacao', 'ApiTabletController@pushAvisaAvaliacao');
    Route::get('/pushAvisaAula', 'ApiTabletController@pushAvisaAula');
    Route::get('/pushAvisaAvaliacao', 'ApiTabletController@pushAvisaAvaliacao');
    Route::get('/pushAvisaPersonalizado', 'ApiTabletController@pushAvisaPersonalizado');
    Route::get('/confirmaAvalReserva/{idmsg}/{idtab}/{idaluno}/{tipo}/{stat?}', 'ApiTabletController@confirmaAvalReserva');

    Route::get('/getMedalhas/{idaluno}/{idunidade}', 'ApiController@getMedalhas');

    Route::get('/atualizaAvatar/{unidade}', 'ApiTabletController@atualizaAvatar');

    Route::get('/getEquipe/{unidade}', 'ApiTabletController@getEquipe');
    Route::get('/getEmpresa/{unidade}', 'ApiTabletController@getEmpresa');

    Route::get('/executaAvisos', 'ApiTabletController@executaAvisos');

    //
    Route::get('/enviaClienteTotem/{idaluno}/{idunidade}', 'ApiTabletController@enviaClienteTotem');
    Route::get('/getClientesTotem/{idunidade}', 'ApiTabletController@getClientesTotem');
    Route::get('/getClienteTotem/{aluno}/{idunidade}', 'ApiTabletController@getClienteTotem');
    Route::get('/deleteClienteTotem/{idaluno}/{idunidade}', 'ApiTabletController@deleteClienteTotem');

    
    //
    Route::get('/listarProcessos/{idunidade}', 'ApiTabletController@listarProcessos');
    Route::get('/temProcessosAluno/{aluno}/{idunidade}', 'ApiTabletController@temProcessosAluno');
    Route::get('/gerarProcessosAut', 'ApiTabletController@gerarProcessosAut');

    ///atualiza id do dispositivo
    Route::post('/setDeviceIdGet/{id}', 'ApiTabletController@setDeviceIdGet');
    Route::post('/setDeviceId', 'ApiTabletController@setDeviceId');
    Route::post('/exibirRanking', 'ApiTabletController@exibirRanking');
    Route::post('/exibirPosicao', 'ApiTabletController@exibirPosicao');
    Route::post('/pausarTempo', 'ApiTabletController@pausarTempo');
    Route::post('/getPausado', 'ApiTabletController@getPausado');
//    Route::get('/participarRanking/{aluno}/{unidade}/{atividade}/{participa}', 'ApiTabletController@participarRanking');
    Route::post('/participarRanking', 'ApiTabletController@participarRanking');

    //atividades cross/ciclismo..
    Route::get('/exibirPrescricaoAtividade/{aluno}/{unidade}/{atividade}', 'ApiTabletController@exibirPrescricaoAtividade');

    Route::get('/testeVideo', 'ApiTabletController@testeVideo');
    Route::get('/excluirAluno/{id}', 'AlunoController@excluirAluno');

    // Route::get('/validaDepenteAluno/{id}/{email}', 'AlunoController@validaDepenteAluno');
    Route::post('/vincularDepenteAluno/{id_responsavel}', 'AlunoController@vincularDepenteAluno');
    Route::post('/vincularUpdateDepenteAluno/{id_responsavel}', 'AlunoController@vincularUpdateDepenteAluno');

    Route::post('/clientePremium/{cliente}/{categoria}', 'AlunoController@clientePremium');
    
    //api
    Route::post('/addUser/{id}/{nome}/{unidade}/{email}/{endereco}/{bairro}/{cep}/{telefone}/{celular}/{sexo}/{situacao}/{cpf}/{nasc}', 'ApiScaController@addUser');
    Route::post('/updateMatricula/{unidade}/{aluno}/{situacao}', 'ApiScaController@updateMatricula');
    
    //somente para testes  // remover Route::get('/updateMatricula
    Route::get('/updateMatricula/{unidade}/{aluno}/{situacao}', 'ApiScaController@updateMatricula');

    
    Route::post('/updateUser/{id}/{nome}/{unidade}/{email}/{endereco}/{bairro}/{cep}/{telefone}/{celular}/{sexo}/{situacao}/{cpf}/{nasc}', 'ApiScaController@updateUser');
    Route::post('/verifica/{idunidade}', 'ApiScaController@verifica');
    Route::post('/verificaCadastro/{idunidade}', 'ApiScaController@verificaCadastro');
    Route::post('/atualizaVerificar', 'ApiScaController@atualizaVerificar');

    Route::get('/sincronizaSca/{id}/{unidade}', 'ApiScaController@sincronizaSca');

    Route::get('/importaBase/{unidade}', 'MigracaoController@importaBase');

    

    Route::get('/exportaFrame/{id}', 'ApiTabletController@exportaFrame');

    ////////////////////api evo
    Route::get('/getFunctions', 'ApiEvoController@getFunctions');
    Route::get('/ListarClientes', 'ApiEvoController@ListarClientes');
    Route::get('/ListarClienteID/{id}', 'ApiEvoController@ListarClienteID');
    Route::get('/importaClientes/{inicial}/{final}', 'ApiEvoController@importaClientes');
    Route::get('/listarClientesGeralEvo', 'ApiEvoController@listarClientesGeralEvo');

    Route::get('/importaClientesEvo', 'ApiEvoController@importaClientesEvo');
    
    
    
    
    /////////////////personalização
    Route::get('/getCoresApp/{unidade}/{tela}', 'ApiPersonalizaController@getCoresApp');
    Route::get('/getTemaAcademia/{unidade}', 'ApiPersonalizaController@getTemaAcademia');
    Route::get('/geraCss/{unidade}', 'ApiPersonalizaController@geraCss');

    Route::get('/homeCss', 'ApiPersonalizaController@menuCss');

    
    

    
});

################ FIM DA API ################
#
Auth::routes();
